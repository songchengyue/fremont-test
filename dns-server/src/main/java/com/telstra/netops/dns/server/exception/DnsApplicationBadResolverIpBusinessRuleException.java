package com.telstra.netops.dns.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Cannot be proceeded with the Entered IP")
public class DnsApplicationBadResolverIpBusinessRuleException extends RuntimeException {
	
	public DnsApplicationBadResolverIpBusinessRuleException(String message) {
		super(message);
	}

}
