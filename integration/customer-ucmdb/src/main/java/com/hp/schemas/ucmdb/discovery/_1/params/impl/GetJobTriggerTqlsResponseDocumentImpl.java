/*
 * An XML document type.
 * Localname: getJobTriggerTqlsResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getJobTriggerTqlsResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetJobTriggerTqlsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponseDocument
{
    
    public GetJobTriggerTqlsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETJOBTRIGGERTQLSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getJobTriggerTqlsResponse");
    
    
    /**
     * Gets the "getJobTriggerTqlsResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponse getGetJobTriggerTqlsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponse)get_store().find_element_user(GETJOBTRIGGERTQLSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getJobTriggerTqlsResponse" element
     */
    public void setGetJobTriggerTqlsResponse(com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponse getJobTriggerTqlsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponse)get_store().find_element_user(GETJOBTRIGGERTQLSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponse)get_store().add_element_user(GETJOBTRIGGERTQLSRESPONSE$0);
            }
            target.set(getJobTriggerTqlsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getJobTriggerTqlsResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponse addNewGetJobTriggerTqlsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsResponse)get_store().add_element_user(GETJOBTRIGGERTQLSRESPONSE$0);
            return target;
        }
    }
}
