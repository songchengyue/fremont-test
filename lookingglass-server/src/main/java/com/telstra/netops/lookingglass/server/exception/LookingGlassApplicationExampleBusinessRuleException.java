package com.telstra.netops.lookingglass.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Create an exception like this if you want the user to be informed of something that
 * happen on the server. DO NOT USE THIS EXCEPTION DIRECTLY, IT IS ONLY AN EXAMPLE.
 * 
 * You should only do this if it's possible for the user to correct their action! Usually
 * this shouldn't be necessary as the UI should prevent the user from sending incorrect
 * information to the server.
 * 
 * For your own exception you should set value = HttpStatus.UNPROCESSABLE_ENTITY and add
 * a reason that the user can understand so they can correct their action.
 * 
 * @author d772392
 *
 */
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Explain to the user what went wrong here!")
public class LookingGlassApplicationExampleBusinessRuleException extends RuntimeException {
	
	public LookingGlassApplicationExampleBusinessRuleException(String message) {
		super(message);
	}

}
