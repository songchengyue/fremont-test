/*
 * XML Type:  CIsAndRelationsUpdates
 * Namespace: http://schemas.hp.com/ucmdb/1/types/update
 * Java type: com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.update;


/**
 * An XML CIsAndRelationsUpdates(@http://schemas.hp.com/ucmdb/1/types/update).
 *
 * This is a complex type.
 */
public interface CIsAndRelationsUpdates extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CIsAndRelationsUpdates.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("cisandrelationsupdates1feftype");
    
    /**
     * Gets the "CIsForUpdate" element
     */
    com.hp.schemas.ucmdb._1.types.CIs getCIsForUpdate();
    
    /**
     * True if has "CIsForUpdate" element
     */
    boolean isSetCIsForUpdate();
    
    /**
     * Sets the "CIsForUpdate" element
     */
    void setCIsForUpdate(com.hp.schemas.ucmdb._1.types.CIs cIsForUpdate);
    
    /**
     * Appends and returns a new empty "CIsForUpdate" element
     */
    com.hp.schemas.ucmdb._1.types.CIs addNewCIsForUpdate();
    
    /**
     * Unsets the "CIsForUpdate" element
     */
    void unsetCIsForUpdate();
    
    /**
     * Gets the "relationsForUpdate" element
     */
    com.hp.schemas.ucmdb._1.types.Relations getRelationsForUpdate();
    
    /**
     * True if has "relationsForUpdate" element
     */
    boolean isSetRelationsForUpdate();
    
    /**
     * Sets the "relationsForUpdate" element
     */
    void setRelationsForUpdate(com.hp.schemas.ucmdb._1.types.Relations relationsForUpdate);
    
    /**
     * Appends and returns a new empty "relationsForUpdate" element
     */
    com.hp.schemas.ucmdb._1.types.Relations addNewRelationsForUpdate();
    
    /**
     * Unsets the "relationsForUpdate" element
     */
    void unsetRelationsForUpdate();
    
    /**
     * Gets the "referencedCIs" element
     */
    com.hp.schemas.ucmdb._1.types.CIs getReferencedCIs();
    
    /**
     * True if has "referencedCIs" element
     */
    boolean isSetReferencedCIs();
    
    /**
     * Sets the "referencedCIs" element
     */
    void setReferencedCIs(com.hp.schemas.ucmdb._1.types.CIs referencedCIs);
    
    /**
     * Appends and returns a new empty "referencedCIs" element
     */
    com.hp.schemas.ucmdb._1.types.CIs addNewReferencedCIs();
    
    /**
     * Unsets the "referencedCIs" element
     */
    void unsetReferencedCIs();
    
    /**
     * Gets the "referencedRelations" element
     */
    com.hp.schemas.ucmdb._1.types.Relations getReferencedRelations();
    
    /**
     * True if has "referencedRelations" element
     */
    boolean isSetReferencedRelations();
    
    /**
     * Sets the "referencedRelations" element
     */
    void setReferencedRelations(com.hp.schemas.ucmdb._1.types.Relations referencedRelations);
    
    /**
     * Appends and returns a new empty "referencedRelations" element
     */
    com.hp.schemas.ucmdb._1.types.Relations addNewReferencedRelations();
    
    /**
     * Unsets the "referencedRelations" element
     */
    void unsetReferencedRelations();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates newInstance() {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
