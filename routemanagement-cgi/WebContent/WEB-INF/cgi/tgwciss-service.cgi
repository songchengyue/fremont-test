#!/usr/local/bin/perl

#
# Written by: Richard Chew, rchew@telstra.net
# TGW Custdata Service script
#
# $Id: tgwciss-service.cgi,v 1.4 2004/01/15 11:16:33 rchew Exp $
# 

no warnings;
use lib './usr/local/www/www/gia-reach/tgwcustdata/control';
use lib './tmp';
use Time::Local;
use RM_NETOPS_OLSS;
use OS_PARA;
use lib "$OS_PARA::values{rm_netops_modules}{value}";
use lib "$OS_PARA::values{olss_modules}{value}";
use lib "$OS_PARA::values{rm_netops_common_modules}{value}";
use JSON::PP;
use POSIX ":sys_wait_h";


#IPT_ph5
use CGI;
use File::Basename;
use Log::Log4perl;
use CGI::Carp qw ( fatalsToBrowser );
use OAuth2Token;
my $req = new CGI;
my @colors = $req->param("route");
my $input = decode_json($req->param('POSTDATA'));
            

# Initialize Logger
my $log_conf =$OS_PARA::values{logfile}{value}; 
Log::Log4perl::init($log_conf);
our $logger = Log::Log4perl->get_logger();
 #sample logging statement
$logger->debug("this debug's from Route Registry perl log this is an debug log message");


# check authorization

sub oauth2_error($) {
    my ($message) = @_;
    print "Status: 401 Unauthorized\n";
    print "Content-type: application/json\n\n";
    my %error = ('error' => $message);
    my $json = encode_json \%error;
    print $json;
    $logger->error($message);
}

# validate token
my $token = $req->http('OAuth2-Bearer-Token');
$logger->debug("TOKEN>>>>>>> $token");

if (!$token) {
    oauth2_error("Missing HTTP header \"OAuth2-Bearer-Token\"");
    exit;
}

my $invalid = OAuth2Token::invalid($token);
if ($invalid) {
    oauth2_error("Invalid token $token: $invalid");
    exit;
} else {
    $logger->debug("OAuth2 token check successful");
}

open(DEBUG,">/data1/tmp_log/Ph5_DEBUG");
my $code = $input->{'code'};
$logger->debug("code for RR is>>>>>>> $code");
my $svc =  $input->{'svc'};
$logger->debug("RR svc is >>>>>> $svc");
my $customerId =$input->{'customerId'};
$logger->debug("RR customerId is>>>>>>>$customerId  ");
my $level = $input->{'level'};
$logger->debug("RR level is>>>>>$level");
my $et = $input->{'et'};
my $route = $input->{'route'};
$logger->debug("RR route entered is >>>>>$route");
my $ip_version = $input->{'ip_version'};
$logger->debug("RR ip version is>>>>>>> $ip_version");
my $submit = $input->{'submit'};
$logger->debug("RR submit value is>>>>>>>>$submit");
my $input_file = $input->{'input_file'};
chop($input_file);
my $role = $input->{'role'};
$logger->debug("RR role is value is>>>>>>>>$submit");
#end IPT_ph5

##Added by Karuna on 17th nov 2010
my $debug = 1;
#Uncomment the following line to disable debug statements; and vice versa
#$debug = 0;

if($debug){open (IPT_RM,">>/data1/tmp_log/servicecgi.log");}
use CGI::Carp qw/fatalsToBrowser/;
print "\n";
require "$OS_PARA::values{libfile}{value}";
# Referrer re-write URLNAME
if($ENV{HTTP_HOST} =~ /myservices.telstra-global.com|pccw-services.pccw.com|134.159.2.123/) {
        $urlname = $ENV{HTTP_HOST};
        $rmask = 1; #used for remasking as DocRoot is reseller-telstra/reseller-pccw
}

our %formels;
our $custfrom = &parse_form_data(*formels);

#IPT_ph5
open(DEBUG2,">>/data1/tmp_log/Ph5_DEBUG2");
print DEBUG2 " before setting formels{'level'}:$formels{'level'}\n";
my $route_te = $input->{'route'};
if ($formels{'level'} eq ""){
	$formels{'level'} = $level;
	$formels{'code'} = $code;
	$formels{'svc'} = $svc;
	$formels{'et'} = $et;
    $formels{'submit'} = $submit;
#All formels
$formels{'customerId'} 	= $customerId;
$formels{'router'} = $router;
$formels{'serviceid'} = $serviceid;
$formels{'loginout'} = $loginout;
$formels{'status'} = $status;
$formels{'def_interface'} = $def_interface;
$formels{'def_vrfname'} = $def_vrfname;
$formels{'def_ipaddress'} = $def_ipaddress;
$formels{'def_mapname'} = $def_mapname;
$formels{'command'} = $command;
$formels{'parameter'} = $parameter;
$formels{'execute'} = $execute;
$formels{'syntax'} = $syntax;
$formels{'screen'} = $screen;
$formels{'scrollpos'} = $scrollpos;
$formels{'clear'} = $clear;
$formels{'search'} = $search;
$formels{'sch_serviceid'} = $sch_serviceid;
$formels{'sch_interface'} = $sch_interface;
$formels{'svcinfo'} = $svcinfo;
$formels{'submit'} = $submit;
$formels{'evpl_end'} = $evpl_end;
$formels{'DOM'} = $DOM;
$formels{'PIP'} = $PIP;
$formels{'add'} = $add;
$formels{'view'} = $view;
$formels{'incominghostname'} = $incominghostname;
$formels{'outgoinghostname'} = $outgoinghostname;
$formels{'pathalias'} = $pathalias;
$formels{'type'} = $type;
$formels{'bandwidth'} = $bandwidth;
print DEBUG2 " formels{'level'}:$formels{'level'}\n";
}
#end IPT_ph5


our $currtime = time;

# first validate user
our $result = $formels{'customerId'};
if($result =~ /FAIL/) {
        &print_error("Cannot validate user: $result");
}
# Set some meaningful vars
our $uid = $result;
our $userid = $uid;
our $oenc = &encrypt($uid,$upass,$currtime);
our ($accno, $staffid) = &ex_staffid($uid);  

our $logo_accno = $accno;
$tmplroot = $tgwr."control/";

&log_entry($custlogfile, "SERVICE", "$uid");     

our $etproduct = $formels{et};
our $tdate = gmtime(time);
$tdate .= " GMT";
my $ac = ($mrwaccno) ? $mrwaccno : $accno;        
$ctrlname = "service";

SWITCH: {

	($formels{'level'} == 1) && do {
		&local_print1($userid, $oenc, $accno, $tmplroot);
		last SWITCH;
	};
	($formels{'level'} == 2) && do {
		&local_print2($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        }; 
        ($formels{'level'} == 3) && do {
                &local_print3($userid, $oenc, $accno, $tmplroot, $mrwaccno);
                last SWITCH;
        }; 
        ($formels{'level'} == 4) && do {
                &local_print4($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        }; 
        ($formels{'level'} == 5) && do {
                &local_print5($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        }; 
        ($formels{'level'} == 6) && do {
                &local_print6($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        };
        ($formels{'level'} == 7) && do {
                my $method = $formels{'method'};
                my $router = $formels{'router'};
                my $serviceid = $formels{'serviceid'};
                my $loginout = $formels{'loginout'};
                my $status = $formels{'status'};
                my $def_interface = $formels{'def_interface'};
                my $def_vrfname = $formels{'def_vrfname'};
                my $def_ipaddress = $formels{'def_ipaddress'};
                my $def_mapname = $formels{'def_mapname'};
                my $command = $formels{'command'};
                my $parameter = $formels{'parameter'};
                my $execute = $formels{'execute'};
                my $syntax = $formels{'syntax'};
                my $screen = $formels{'screen'};
                my $scrollpos = $formels{'scrollpos'};
                my $clear = $formels{'clear'};
                my $search = $formels{'search'};
                my $sch_serviceid = $formels{'sch_serviceid'};
                my $sch_interface = $formels{'sch_interface'};
                my $svcinfo = $formels{'svcinfo'};
		my $aendzend = $formels{'evpl_end'};

                &local_print7($userid, $oenc, $accno, $tmplroot, $method, $router, $serviceid, $loginout, $status, $def_interface, $def_vrfname, $def_ipaddress, $def_mapname, $command, $parameter, $execute, $syntax, $screen, $scrollpos, $clear, $search, $sch_serviceid, $sch_interface, $svcinfo, $aendzend);
                last SWITCH;
        };
        ($formels{'level'} == 8) && do {
                &local_print8($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        };

	# sub-level 2
        ($formels{'level'} =~ /^2\d{1,2}$/) && do {
        	if ($debug) {print IPT_RM "inside formel $formels{'level'}\n";}
		print DEBUG2 "inside formel $formels{'level'}\n";
		open (RAD1,">/tmp/olss1.txt");
		if ($formels{'level'} == 211) {

			$logger->debug("Entered  level value 211");
		   	 my $route = $input->{'route'};
			$logger->debug("route in level 211>>>>$route");
			my $prefix_type = $input->{'prefix'};
			$logger->debug("prefix_type in level 211>>>>$prefix_type");
			
			my $email_id = $input->{'email_id'};
			$logger->debug("email_id in level 211>>>>$email_id");
			my $maintained_by = $input->{'maintained_by'};
			$logger->debug("maintained_by in level 211>>>>$maintained_by");
			my $submit = $input->{'submit'};
			$logger->debug("submit in level 211>>>>$submit");
			my $ip_version = $input->{'ip_version'};
			$logger->debug("ip_version in level 211>>>>$ip_version");
		
			my $role = $input->{'role'};
			$logger->debug("role in level 211>>>>$role");

			
                        my $customer_origin = $input->{'customer_origin'};
	
                        if($customer_origin =~ /AS/i) {
                        $customer_origin = uc($customer_origin);
                         my @values = split('AS', $customer_origin);
                           $customer_origin = $values[1];
                                }
                        else {
                                $customer_origin = $customer_origin;
                        }

			print DEBUG2 "prefix_type:$prefix_type ,,,,,email_id:$email_id.....maintained_by:$maintained_by\n";
			$logger->debug("prefix_type:$prefix_type ,,,,,email_id:$email_id.....maintained_by:$maintained_by\n");
			print DEBUG2 "in bulk:userid:$userid,,,,,,,oenc:$oenc..accno:$accno........tmplroot:$tmplroot...level:$level....svc:$svc....upload_dir:$upload_dir \n";
			$logger->debug("in bulk:userid:$userid,,,,,,,oenc:$oenc..accno:$accno........tmplroot:$tmplroot...level:$level....svc:$svc....upload_dir:$upload_dir \n");		
		if ($prefix_type eq "Bulk") 
			{
			print DEBUG2 "upload_filehandle:$file_name\n";
			$logger->debug("upload_filehandle:$file_name\n");
				my $unix_timestamp = `date +%s`; 
				
			print DEBUG2 "unix_timestamp:$unix_timestamp\n";
			$logger->debug("unix_timestamp:$unix_timestamp\n");
				my $upload_file = "Input.txt"."_".$unix_timestamp;
				chomp($upload_file);
			print DEBUG2 "upload_file:$upload_file  \n";
			$logger->debug("upload_file:$upload_file  \n");
	
				my $upload_dir = "./tmp/route_management/".$upload_file; 
	
				my $upload_dir = "./tmp/route_management/".$upload_file;						
				my $input_file = $input->{'input_file'};
				chop($input_file);
				my $empty_file_flag = 0;
		  
				print DEBUG2 "contents_file:@contents_file\n";
				 my @contents_file = split(',', $input_file);
				if($#contents_file <= 0)
				{
 				print DEBUG2 "file is emptyoooooooo\n";
				$logger->debug("file is emptyoooooooo\n");
				$empty_file_flag = 1;
	
				}			
				else 
				{
				print DEBUG2 "not empty\n";
				$logger->debug("not empty\n");

					
			
					open ( UPLOADFILE, ">$upload_dir" ) or print "not uploaded\n";
					
					@contents_file	=join ("\n", @contents_file);
					foreach(@contents_file){
					
					
					print DEBUG2 "file contentets: $upload_filehandle\n";
					$logger->debug("file contentets: $upload_filehandle\n");
  
						 print UPLOADFILE;
						
					}
					close UPLOADFILE;
				}
				
				
###########################################  BULK UPLOAD ########################################################################
	print DEBUG2 "parameters passed to local_print_s2 in bulk:$userid, $oenc, $accno, $tmplroot, $level,$svc, $email_id, $maintained_by, $prefix_type, $upload_file\n";
	$logger->debug("parameters passed to local_print_s2 in bulk:$userid, $oenc, $accno, $tmplroot, $level,$svc, $email_id, $maintained_by, $prefix_type, $upload_file\n ");			
				my $routeListOutput = &local_print_s2($userid, $oenc, $accno, $tmplroot, $level,$svc, $email_id, $maintained_by, $prefix_type, $upload_file, $customer_origin, $empty_file_flag,$submit,$ip_version);
					
				print "Content-Type: application/json\n\n";
				print $routeListOutput;
				$logger->debug("result for bulk upload >>>>>>>>>>$routeListOutput\n");							
				last SWITCH;
			} 
			else 
			{
			$logger->debug("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Entered  level value 211 for Single!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
###########################################  SINGLE PREFIX UPLOAD ########################################################################
			print DEBUG2 "route:$route\n";
						$logger->debug("route:$route\n");
			my $routeListOutput =&local_print_s2($userid, $oenc, $accno, $tmplroot, $level,$svc, $email_id, $maintained_by, $prefix_type, $route, $customer_origin, $submit,$ip_version,$role); ###changed @ to $
			print DEBUG2 "parameters passed SINGLEto local_print_s2 in bulk:$userid, $oenc, $accno, $tmplroot, $level,$svc, $email_id, $maintained_by, $prefix_type, $upload_file,$route\n";	
  
			$logger->debug("parameters passed SINGLEto local_print_s2 in bulk:$userid, $oenc, $accno, $tmplroot, $level,$svc, $email_id, $maintained_by, $prefix_type, $upload_file,$route,$ip_version\n");			
			print "Content-Type: application/json\n\n";
			$logger->debug("result for  upload/view >>>>>>>>>> $routeListOutput\n");
			print $routeListOutput;
			last SWITCH;
			}
		
		} 
		elsif ($formels{'level'} == 212)
		{
		    
		 my $routeValues = $input->{'dbRowId'};
			$logger->debug("dbrowid in tgw--- $routeValues \n");
		my @route_array = split(',', $routeValues);
				my $route = "";
		
	
			foreach (@route_array) {
			$route .= $_."\0";
				

		}

			print RAD1 "prefixes --- $route\n";
			print DEBUG2 "userid:$userid\taccno:$accno\tformels{'level'}:$formels{'level'}\tformels{'svc'}:$formels{'svc'}\troute:$route\tformel routes:$formels{'route'}\n";
			$logger->debug("userid:$userid\taccno:$accno\tformels{'level'}:$formels{'level'}\tformels{'svc'}:$formels{'svc'}");		
			if($debug) {print IPT_RM "delete passing elements\n";}
			if($debug) {print IPT_RM "userid:$userid\taccno:$accno\tformels{'level'}:$formels{'level'}\tformels{'svc'}:$formels{'svc'}\tformels{'route'}:$formels{'route'}\n";}
			my $delete=&local_print_s2($userid, $oenc, $accno, $tmplroot, $formels{'level'},$formels{'svc'}, "", "", "", $route,$submit);
			    
			print "Content-Type: application/json\n\n";
			$logger->debug("result for delete >>>>>>>>>> $delete \n");
			print $$delete;

			
			last SWITCH;
		}	
		
		print RAD1 "route --------- $formels{'route'}\n";
		$logger->debug("route --------- $formels{'route'}\n");
		&local_print_s2($userid, $oenc, $accno, $tmplroot, $formels{'level'},
			$formels{'svc'}, $formels{'route'});
        	last SWITCH;
        };

        &print_error("Level unrecognised");
	exit;
};

sub local_print_s2 {

	$logger->debug("!!!!!!!!!!!!!!!!!Entered local print s2 in twg file!!!!!!!!!!!!!!!!!!!!!!!"); 
 
	#IPT_ph5
	#25 Jan
        #my($uid, $code, $accno, $tmplroot, $level, $svc, $route) = @_;
	my( $uid, $code, $accno, $tmplroot, $level, $svc, $email_id, $maintained_by, $prefix_type, $route, $customer_origin, $empty_file_flag, $submit,$ip_version,$role) = @_;
	my $opshandle = $svc; 
	$logger->debug("svc  passed to cgi from front end >>>>>>>>>>>>>>>>>>>>>>>>>>>>$svc"); 
	$logger->debug("opshandle pased to lib.pl file is >>>>>>>>>>>>>>>>>>>>>>>>>>>>$opshandle");
	my $submit = $input->{'submit'};
	my $ip_version = $input->{'ip_version'}; 
 

			#IPT_ph5
	my $submit = $input->{'submit'};
	my $ip_version = $input->{'ip_version'};
	my $role=$input->{'role'};
		
	if($submit eq "SUBMIT" or $submit eq "DELETE")
	{


	my @add_sub = &process_route($uid, $code, $accno, $level, $opshandle, $route, $email_id, $maintained_by, $prefix_type, $customer_origin, $empty_file_flag,$ip_version,$role);
	$logger->debug("add success message::@add_sub");

	$result = encode_json({result => \@add_sub});
				return $result;  
	}else
	{ 
  	$logger->debug("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Inside view Sub in tgw !!!!!!!!!!!!!!!!!!!");
	my  @view_sub= &screen_route($uid, $code, $accno, $tmplroot, $fnn, $opshandle);
  					 
	$result = encode_json({result => \@view_sub});
				return $result;  
 	}
				                               
              
     
}

