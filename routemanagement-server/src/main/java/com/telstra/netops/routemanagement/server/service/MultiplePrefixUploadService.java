package com.telstra.netops.routemanagement.server.service;

import com.telstra.netops.routemanagement.server.model.PrefixUploadRequest;
import com.telstra.netops.routemanagement.server.model.Success;

public interface MultiplePrefixUploadService {
	Success upload(PrefixUploadRequest request);
}
