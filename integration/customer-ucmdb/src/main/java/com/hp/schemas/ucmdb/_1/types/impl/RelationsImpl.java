/*
 * XML Type:  Relations
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.Relations
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML Relations(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class RelationsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.Relations
{
    
    public RelationsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RELATION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "relation");
    
    
    /**
     * Gets array of all "relation" elements
     */
    public com.hp.schemas.ucmdb._1.types.Relation[] getRelationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(RELATION$0, targetList);
            com.hp.schemas.ucmdb._1.types.Relation[] result = new com.hp.schemas.ucmdb._1.types.Relation[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "relation" element
     */
    public com.hp.schemas.ucmdb._1.types.Relation getRelationArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relation target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relation)get_store().find_element_user(RELATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "relation" element
     */
    public int sizeOfRelationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RELATION$0);
        }
    }
    
    /**
     * Sets array of all "relation" element
     */
    public void setRelationArray(com.hp.schemas.ucmdb._1.types.Relation[] relationArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(relationArray, RELATION$0);
        }
    }
    
    /**
     * Sets ith "relation" element
     */
    public void setRelationArray(int i, com.hp.schemas.ucmdb._1.types.Relation relation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relation target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relation)get_store().find_element_user(RELATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(relation);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "relation" element
     */
    public com.hp.schemas.ucmdb._1.types.Relation insertNewRelation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relation target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relation)get_store().insert_element_user(RELATION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "relation" element
     */
    public com.hp.schemas.ucmdb._1.types.Relation addNewRelation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relation target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relation)get_store().add_element_user(RELATION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "relation" element
     */
    public void removeRelation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RELATION$0, i);
        }
    }
}
