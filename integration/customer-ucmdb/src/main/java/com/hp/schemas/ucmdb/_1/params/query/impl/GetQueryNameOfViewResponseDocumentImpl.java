/*
 * An XML document type.
 * Localname: getQueryNameOfViewResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getQueryNameOfViewResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetQueryNameOfViewResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponseDocument
{
    
    public GetQueryNameOfViewResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETQUERYNAMEOFVIEWRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getQueryNameOfViewResponse");
    
    
    /**
     * Gets the "getQueryNameOfViewResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponse getGetQueryNameOfViewResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponse)get_store().find_element_user(GETQUERYNAMEOFVIEWRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getQueryNameOfViewResponse" element
     */
    public void setGetQueryNameOfViewResponse(com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponse getQueryNameOfViewResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponse)get_store().find_element_user(GETQUERYNAMEOFVIEWRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponse)get_store().add_element_user(GETQUERYNAMEOFVIEWRESPONSE$0);
            }
            target.set(getQueryNameOfViewResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getQueryNameOfViewResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponse addNewGetQueryNameOfViewResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponse)get_store().add_element_user(GETQUERYNAMEOFVIEWRESPONSE$0);
            return target;
        }
    }
}
