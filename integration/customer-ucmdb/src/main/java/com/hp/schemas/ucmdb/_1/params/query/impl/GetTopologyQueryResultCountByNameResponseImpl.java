/*
 * XML Type:  getTopologyQueryResultCountByNameResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML getTopologyQueryResultCountByNameResponse(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class GetTopologyQueryResultCountByNameResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse
{
    
    public GetTopologyQueryResultCountByNameResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TOPOLOGYCOUNTMAP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "topologyCountMap");
    
    
    /**
     * Gets the "topologyCountMap" element
     */
    public com.hp.schemas.ucmdb._1.types.query.TopologyCountMap getTopologyCountMap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.TopologyCountMap target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.TopologyCountMap)get_store().find_element_user(TOPOLOGYCOUNTMAP$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "topologyCountMap" element
     */
    public void setTopologyCountMap(com.hp.schemas.ucmdb._1.types.query.TopologyCountMap topologyCountMap)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.TopologyCountMap target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.TopologyCountMap)get_store().find_element_user(TOPOLOGYCOUNTMAP$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.query.TopologyCountMap)get_store().add_element_user(TOPOLOGYCOUNTMAP$0);
            }
            target.set(topologyCountMap);
        }
    }
    
    /**
     * Appends and returns a new empty "topologyCountMap" element
     */
    public com.hp.schemas.ucmdb._1.types.query.TopologyCountMap addNewTopologyCountMap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.TopologyCountMap target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.TopologyCountMap)get_store().add_element_user(TOPOLOGYCOUNTMAP$0);
            return target;
        }
    }
}
