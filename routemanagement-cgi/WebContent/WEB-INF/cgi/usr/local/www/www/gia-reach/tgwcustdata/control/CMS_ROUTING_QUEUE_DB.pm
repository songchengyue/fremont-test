# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 24 September 2001
# File: CMS_ROUTING_QUEUE_DB.pm
#
# $Id: CMS_ROUTING_QUEUE_DB.pm,v 1.6 2003/10/09 08:24:38 tonytam Exp $

package CMS_ROUTING_QUEUE_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use Log::Log4perl;
use OS_PARA;
use CMS_DB;
use CMS_SERVICE_DB;

use lib "$OS_PARA::values{cms_modules}{value}";
use lib "$OS_PARA::values{olss_modules}{value}";
use lib "$OS_PARA::values{cms_common_modules}{value}";
our $error;

########added loggers
my $log_conf =$OS_PARA::values{logfile}{value}; 
Log::Log4perl::init($log_conf);
my $logger = Log::Log4perl->get_logger();
########
$logger->debug("Inside CMS ROUTING QUEUE_DB. \n");
##Added By Karuna on 11-Nov-2010 for IPT5
my $debug = 1;
#To disable debug logs, uncomment the following line and vice versa
#$debug = 0;
if ($debug){open (IPT_RM, ">>/data1/tmp_log/routeqdb.log");}


sub check_routing_exist_allstatus_fix {
  my $route = shift @_;
  my $action = shift @_;
  my $opshandle = shift @_;
#print"$route,$action,$opshandle";

  
  my $sth = $dbh->prepare ("select count(r.route) from routing_queue r, service s
       where s.service_id = r.opshandle and r.action = '$action' and r.route = '$route'
       and s.as_no = (select as_no from service where service_id = '$opshandle')");
  
  unless ($sth->execute) {
    $error = $dbh::err;
    return 1;
  }
  
  #return $sth->rows;
  my @row_num = $sth->fetchrow_array;
  #print "ggjj/n";
  #print "$row_num[0]";
#close STDOUT;
  return $row_num[0];


 
}
sub add_routing_request_CHECKING {
#Number of arguements increased to 5-CRQ9161
$logger->debug("Inside add routing request. \n");

  if (not (scalar @_ == 6) )  {
    $error = "Incorrect number of arguments passed to add_routing_change.";

$logger->debug("Incorrect number of arguments passed to add_routing_change. \n");
	return 0;

  }

  my $opshandle = shift @_;
  my $action = shift @_;
  my $route = shift @_;
  my $as = shift @_;
#New vairiable customer origin is added-CRQ9161
  my $customer_origin = shift @_;
  my $ip_version = shift @_;
  my $sql_command;
  my $trackno;
  my $sth;
	         

  # add a new prefix
#IPT_ph5
  if (check_routing_exist($route, 2, $opshandle) == 0) {
$logger->debug(" inside if statement \n");

     #$sth = $dbh->prepare("SELECT NEXTVAL ('routing_queue_sequence')");
     $sth = $dbh->prepare("SELECT routing_queue_sequence.nextval from dual");
     $sth->execute;
     $trackno = ($sth->fetchrow_array)[0];
$logger->debug(" track no >>>>>>>>> ($sth->fetchrow_array)[0]\n");

     $sth->finish;

#IPT_Addendum_2   #Chandini  #Added backup_bha_status and changed both the bha status to 3
#Added new column customer origin
   #$sql_command =  "INSERT INTO routing_queue
                      #      (trackno, opshandle, action, route,
                      #      status, time_entered, time_complete, as_no, bha_status, bha_time_complete, backup_bha_status, backup_bha_time_complete, customer_origin)
                      #   VALUES('$trackno', '$opshandle', '$action',
                      #         '$route', 0, CURRENT_TIMESTAMP, null, '$as', 3, CURRENT_TIMESTAMP, 3, CURRENT_TIMESTAMP, '$customer_origin')"; 
   $sql_command =  "INSERT INTO routing_queue
                            (trackno, opshandle, action, route,
                            status, time_entered, time_complete, as_no, bha_status, bha_time_complete, backup_bha_status, backup_bha_time_complete, customer_origin, ip_version)
                         VALUES('$trackno', '$opshandle', '$action',
                               '$route', 0, CURRENT_TIMESTAMP, null, '$as', 3, CURRENT_TIMESTAMP, 3, CURRENT_TIMESTAMP, '$customer_origin','$ip_version')"; 


  } else { #update status of a deleted prefix
$logger->debug(" in else \n");

     $sth = $dbh->prepare("SELECT trackno from routing_queue where route = '$route'");
     $sth->execute;
     $trackno = ($sth->fetchrow_array)[0];
$logger->debug("  track no >>>>>>>>> ($sth->fetchrow_array)[0] \n");

     $sth->finish;

	##Karuna for IPT5 on 25-Nov-2010
#IPT_Addendum_2  #Chandini ##Added backup_bha_status and backup_bha_time_completed and bug fix to avoid duplicate entries in routing_queue table
     $sql_command = "UPDATE routing_queue set action = 1, status = 0, bha_status = 3, backup_bha_status = 3, time_complete = CURRENT_TIMESTAMP, bha_time_complete = CURRENT_TIMESTAMP, backup_bha_time_complete = CURRENT_TIMESTAMP, ip_version = '$ip_version' where route = '$route' and as_no = '$as'"; 
  }

  take_log($trackno, $route, $opshandle, $action);
$logger->debug(" before error \n");

  unless ($sth = $dbh->prepare($sql_command)) {
    $error = $dbh::err;
$logger->debug("DB errrorrrrrrr. \n");

    return 0;

  }
  my $result = $sth->execute;
  $error = $dbh::err;
  return $result; 
}


#Addendum_2 #Chandini
sub delete_routing_request {
  ##++IPT5 Karuna
  if($debug){print IPT_RM "DB entered delete_routing_request\n";}
  my $track_no = shift @_;
  my $opshandle = shift @_;
  my $action = shift @_;
  my $sth;

  if ($debug){print IPT_RM "DB delete_routing_request track_no:$track_no\topshandle:$opshandle\taction:$action\n";}
  ##--IPT5

  $sth = $dbh->prepare("SELECT route from routing_queue where trackno IN ($track_no)");
  $sth->execute;
  my $route = $sth->fetchrow_array;
  $sth->finish;


#######################################
#############################################
  take_log($track_no, $route, $opshandle, $action);

  $sth = $dbh->prepare("DELETE FROM routing_queue 
			WHERE trackno IN ($track_no) and status = 0 and action = 1");
  $sth->execute;
  my $row = $sth->rows;
  if ($row == 0) {
     # set status to 'queued'
     ##Karuna on 24-Nov-2010 for IPT5
			# IPT - Addendum_2 - BHA : 20110615 : NKT #
			#	action = 2, status = 0, bha_status = 0, backup_bha_status = 3,
     $sth = $dbh->prepare("UPDATE routing_queue set
				action = 2, status = 0, bha_status = 3, backup_bha_status = 3, time_entered = CURRENT_TIMESTAMP, time_complete = null where trackno IN ($track_no)");
     $sth->execute;
     $row = $sth->rows;
  } else {
     # update transaction log
   ####  $sth = $dbh->prepare("select transaction_no from routing_transaction_log where route_trackno = '$track_no' order by time_entered desc limit 1"); 
      $sth = $dbh->prepare("select * from (select transaction_no from routing_transaction_log where route_trackno IN ($track_no) order by time_entered desc) where rownum = 1");
     unless ($sth->execute) {
       $error = $dbh::err;
       return;
     }

     my $ref = $sth->fetchrow_hashref();
     $sth = $dbh->prepare("update routing_transaction_log set time_complete = CURRENT_TIMESTAMP where transaction_no = '$ref->{'transaction_no'}'"); 
     $sth->execute;
  }
  $error = $dbh::err;
  return $row;
}

#Addendum_2 ##Chandini
sub delete_routing_request_by_opshandle {
  my $opshandle = $dbh->quote(shift @_);
  my $sth;

  # remove on-queue route
  $sth = $dbh->prepare("delete from routing_queue where opshandle = $opshandle and action = 1 and status = 0");
  $sth->execute;
  $sth->finish;

  # delete completed route
  $sth = $dbh->prepare("UPDATE routing_queue set
                        action = 2, status = 0, bha_status = 3, backup_bha_status = 3,
                        time_entered = CURRENT_TIMESTAMP, time_complete = null
                        where opshandle = $opshandle");
  $sth->execute;
  $sth->finish;

  # remove transaction log
  $sth = $dbh->prepare("delete from routing_transaction_log where opshandle = $opshandle");
  $sth->execute;
  $sth->finish;

  return;
}

sub delete_routing_request_by_route {
  my $route = shift @_;
  my $as = shift @_;

  # my $sth = $dbh->prepare("select * from routing_queue where route = '$route' and as_no = '$as'");
  # my $sth = $dbh->prepare("select trackno, opshandle from routing_queue where route = '$route' and as_no = '$as'");

  # if action is 'delete' and status is 'queued', don't need to do it again
  my $sth = $dbh->prepare("select count(opshandle) from routing_queue where route = '$route' and as_no = '$as' and not(action = 2 and status = 0) ");

  unless ($sth->execute) {
    $error = $dbh::err;
    #return "0";
	$error = $dbh->errstr;
                if ($debug){print IPT_DBG "DB error:$error\n";}
                return "0";
  }

  # return if no row selected
 # my $row = $sth->rows;
  #if ($row == 0) { 
  	#$sth->finish;
	my @row_num = $sth->fetchrow_array;
	if ($row_num[0] == 0){
	$sth->finish;
	return;
  }
  else
  {
	my $sth = $dbh->prepare("select trackno,opshandle from routing_queue where route = '$route' and as_no = '$as' and not(action = 2 and status = 0) ");

	unless ($sth->execute) {
    $error = $dbh::err;
    #return "0";
	$error = $dbh->errstr;
                if ($debug){print IPT_DBG "DB error:$error\n";}
                return "0";

	}
	
	my $ref = $sth->fetchrow_hashref();
  # mark record for deletion
  delete_routing_request($ref->{'trackno'}, $ref->{'opshandle'}, 2); 

  $sth->finish;

  return;
  }

  
}


sub complete_routing_request_service {
  my $as= shift @_;
  my $opshandle = shift @_;
  my $status = shift @_;
  my $router_flag = shift @_;
  my $sth;
  my $result;
 
  ##Karuna for IPT 5 on 25-Nov-2010
  if ($debug) { print IPT_RM "DB complete_routing_request_service as:$as\topshandle:$opshandle\tstatus:$status\trouter_flag:$router_flag\n";}

  if ($router_flag == 1){
    	$sth = $dbh->prepare("update routing_queue set time_complete = CURRENT_TIMESTAMP, errormessage = NULL, 
                                 status = $status where as_no = '$as' 
                                 and status = '3' and errormessage is null");
  if ($debug) { print IPT_RM "DB complete_routing_request_service update routing_queue set time_complete = CURRENT_TIMESTAMP, errormessage = NULL, status = $status where as_no = '$as' and status = '3' and errormessage is null\n";}
  } else {
	$sth = $dbh->prepare("update routing_queue set time_complete = CURRENT_TIMESTAMP, errormessage = NULL, 
				status = $status where as_no = '$as'
				and status = '3'");
  if ($debug) { print IPT_RM "DB complete_routing_request_service update routing_queue set time_complete = CURRENT_TIMESTAMP, errormessage = NULL, status = $status where as_no = '$as' and status = '3'\n";}
  }

  unless($result = $sth->execute) {
    	$error = $dbh::err;
  }
  if ($error) {
	if ($debug) { print IPT_RM "DB complete_routing_request_service error:$error\n";}
	return "0";
  } else {
	return "1";
  }

}

#Addendum_2  #Chandini
sub complete_routing_request_service_bha {
  my $as= shift @_;
  my $opshandle = shift @_;
  my $status = shift @_;
  my $router_flag = shift @_;
  my $router_num = shift @_;
  my $sth;
  my $result;
  if ($debug){print IPT_RM "complete_routing_request_service_bha as:$as\topshandle:$opshandle\tstatus:$status\n";}
  if ($debug){print IPT_RM "complete_routing_request_service_bha as:$router_num = router num and router_flag: $router_flag\n";}

  if ($router_flag == 1){
	if($router_num == 0){
		$sth = $dbh->prepare("update routing_queue 
				set bha_time_complete = CURRENT_TIMESTAMP, 
				bha_errormessage = NULL, 
				bha_status = $status 
				where as_no = '$as' 
				and bha_errormessage is null
				and bha_status = '3' ");
	}else {
		$sth = $dbh->prepare("update routing_queue 
				set backup_bha_time_complete = CURRENT_TIMESTAMP, 
				backup_bha_errormessage = NULL, 
				backup_bha_status = $status 
				where as_no = '$as' 
				and backup_bha_errormessage is null
				and backup_bha_status = '3' ");
	}
  } else {
	if($router_num == 0){
		$sth = $dbh->prepare("update routing_queue 
				set bha_time_complete = CURRENT_TIMESTAMP, 
				bha_errormessage = NULL,
                                bha_status = $status 
				where as_no = '$as'
                                and bha_status = '3' ");
	} else{
		$sth = $dbh->prepare("update routing_queue 
				set backup_bha_time_complete = CURRENT_TIMESTAMP, 
				backup_bha_errormessage = NULL,
                                backup_bha_status = $status 
				where as_no = '$as'
                                and backup_bha_status = '3' ");
	}
  }
  $sth->execute;
    $error = $dbh::err;
    if ($debug){print IPT_RM "DB complete_routing_request_service_bha error:$error\n";}
    return;
}




sub complete_routing_request {
  if ($debug){print IPT_RM "DB complete_routing_request Entered\n";}
  my $action = lc(shift @_);
  my $track_no = shift @_;
  my $status = shift @_;
  if ($debug){print IPT_RM "DB complete_routing_request action:$action\ttrack_no:$track_no\tstatus:$status\n";}
  my $sth;
  my $result;
 
  if ($action eq "") {
     $sth = $dbh->prepare("select rqa.actiontext from routing_queue_action rqa, routing_queue rq
                            where rq.action = rqa.action and rq.trackno = '$track_no'");
     $sth->execute;
     my $r_ref = $sth->fetchrow_hashref();
     $action = lc($r_ref->{'actiontext'});
  }

  my $sql_command = "select transaction_no from routing_transaction_log where route_trackno = '$track_no' and action = ";
  if ($action eq "delete") {
    # Setting the status to 3 instead of deleting the route from the routing queue.
    #$sth = $dbh->prepare("delete from routing_queue where trackno = '$track_no'");
    $sth = $dbh->prepare("update routing_queue set time_complete = CURRENT_TIMESTAMP, errormessage = NULL, 
                                 status = $status where trackno = '$track_no'");
    $sql_command .= "2 "; 
  } else {
	if ($debug){print IPT_RM "DB complete_routing_request query:update routing_queue set time_complete = CURRENT_TIMESTAMP, errormessage = NULL,status = $status where trackno = '$track_no'\n";}
    $sth = $dbh->prepare("update routing_queue set time_complete = CURRENT_TIMESTAMP, errormessage = NULL,
				 status = $status where trackno = '$track_no'");
    $sql_command .= "1 ";
  }
  $sql_command .= "order by time_entered desc limit 1";
	print "second sql command : $sql_command";
  unless($result = $sth->execute) {
    $error = $dbh::err;
    return;
  }

  # update transaction log
  if ($status == 4) {
     $sth = $dbh->prepare($sql_command);
     unless($sth->execute) {
       $error = $dbh::err;
       return;
     }
  
     my $ref = $sth->fetchrow_hashref();
     $sth = $dbh->prepare("update routing_transaction_log set time_complete = CURRENT_TIMESTAMP where transaction_no = '$ref->{'transaction_no'}'"); 
     $result = $sth->execute;
     $error = $dbh::err;
  }
  return $result;
}


#IPT_ph5
sub complete_routing_request_bha {
  my $action = lc(shift @_);
  my $track_no = shift @_;
  my $status = shift @_;
  my $sth;
  my $result;

  if ($action eq "") {
     $sth = $dbh->prepare("select rqa.actiontext from routing_queue_action rqa, routing_queue rq
                            where rq.action = rqa.action and rq.trackno = '$track_no'");
     $sth->execute;
     my $r_ref = $sth->fetchrow_hashref();
     $action = lc($r_ref->{'actiontext'});
  }

  my $sql_command = "select transaction_no from routing_transaction_log where route_trackno = '$track_no' and action = ";
  if ($action eq "delete") {
    # Setting the status to 3 instead of deleting the route from the routing queue.
    # $sth = $dbh->prepare("update routing_queue set bha_status = $status, bha_time_complete = CURRENT_TIMESTAMP, bha_errormessage = NULL where trackno = '$track_no'");
    
    # IPT - Addendum_2 - BHA : 20110615 : NKT #
    $sth = $dbh->prepare("update routing_queue set bha_status = $status, bha_time_complete = CURRENT_TIMESTAMP, bha_errormessage = NULL, backup_bha_status = $status, backup_bha_time_complete = CURRENT_TIMESTAMP, backup_bha_errormessage = NULL where trackno = '$track_no'");
    $sql_command .= "2 ";
  } else {
    $sth = $dbh->prepare("update routing_queue set bha_status = $status, bha_time_complete = CURRENT_TIMESTAMP, bha_errormessage = NULL, backup_bha_status = $status, backup_bha_time_complete = CURRENT_TIMESTAMP, backup_bha_errormessage = NULL where trackno = '$track_no'");
    $sql_command .= "1 ";
  }
  $sql_command .= "order by time_entered desc limit 1";

  unless($result = $sth->execute) {
    $error = $dbh::err;
    return;
  }

  # update transaction log
  #if ($status == 4) {
   #  $sth = $dbh->prepare($sql_command);
    # unless($sth->execute) {
     #  $error = $dbh::err;
     #  return;
    # }

#     my $ref = $sth->fetchrow_hashref();
 #    $sth = $dbh->prepare("update routing_transaction_log set time_complete = CURRENT_TIMESTAMP
  #                              where transaction_no = '$ref->{'transaction_no'}'");
   #  $result = $sth->execute;
    # $error = $dbh::err;
  #}

  return $result;
}


sub get_status {
  my $sth = $dbh->prepare("SELECT status, statustext 
                           FROM routing_queue_status");

  unless ($sth->execute) {
    $error = $dbh::err;
    return;
  }

  my $result = $sth->fetchall_arrayref();
  $error = $dbh::err;
  return $result;
} 

sub get_action {
  my $sth = $dbh->prepare("SELECT action, actiontext 
                           FROM routing_queue_action");

  unless ($sth->execute) {
    $error = $dbh::err;
    return;
  }

  my $result = $sth->fetchall_arrayref();
  $error = $dbh::err;
  return $result;
} 

sub get_routing_request_by_as {
  my $as = shift @_ || "";
  my $status = shift @_ || "";

 #my $sql_command = "SELECT opshandle, route FROM routing_queue, service 
 #                  where routing_queue.opshandle = service.service_id and 
 #                 service.as_no = '$as'";
  my $sql_command = "SELECT opshandle, route FROM routing_queue, service 
                   where routing_queue.opshandle = service.service_id and 
                   service.as_no = '$as'";
  
  if ($status ne "") {
     $sql_command .= " and routing_queue.status = '$status'";
  }
  
  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}
#Chandini_IPT_ph5_03Mar
sub get_routing_request_by_as_irrd {
  my $as = shift @_ || "";

 # my $sql_command = "SELECT opshandle, route FROM routing_queue, service
 #                 where routing_queue.opshandle = service.service_id and
 #                service.as_no = '$as' and not(action = 2 and status = 4)";
my $sql_command = "SELECT opshandle, route FROM routing_queue, service
                   where routing_queue.opshandle = service.service_id and
                   service.as_no = '$as' and not(action = 2 and status = 4)";

  my $sth = $dbh->prepare ($sql_command);
if ($debug){print "sql_command:$sql_command\n";}

  unless ($sth->execute) {
    $error = $dbh::err;
if ($debug){print IPT_RM "in errord\n";}
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
if ($debug){
	print IPT_RM "result:$result\n";
	foreach my $row ( @{$result} ) {
	    print IPT_RM "@$row\n";
		}

	print IPT_RM "Count: $#{$result} ";	
}
  $sth->finish;
  $error = $dbh::err;
  return $result;
}
#Chandini_IPT_ph5_03Mar
sub get_gia_ipt_serv_by_as {
  my $as = shift @_ || "";

 # my $sql_command = "SELECT service_id from service where productcodeid in ('IPTRANSIT','GIA') and as_no = $as";
  my $sql_command = "SELECT service_id from service where productcodeid in ('IPTRANSIT','GID') and as_no ='$as' ";
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}


#Chandini_IPT_ph5
sub insert_route_bulkupload {
$logger->debug("hello in there!!!!!!!!!!!!!!!");
#Inserted new column customer origin to route_bulk_upload table-CRQ9161
  my ($route, $prefix_type, $email_id, $maintained_by, $as_no, $opshandle, $accno, $accno_check, $customer_origin,$ip_version) = @_;
  #CRQ000000005326 -- Jan-2012
$logger->debug("Inside insert sub for adding to bulk upload table.....$as_no..........$accno_check");
$logger->debug("route : $route");
  my $sql_command1 = "SELECT file_name from route_bulkupload where file_name = '$route' and as_no = '$as_no' and status = 0";
  my $sth1 = $dbh->prepare ($sql_command1);
  $sth1->execute;
  my $file_name = ($sth1->fetchrow_array)[0];

  $sth1->finish;
$logger->debug("file_name already present value fetched: $file_name");
  if($file_name){
          return "Re-entered";
  }

  #CRQ000000005326 -- Jan-2012
  my $sql_command;
  if ($accno_check){
	  $sql_command = "INSERT INTO route_bulkupload VALUES('$route', '$prefix_type', '$as_no', '$opshandle', 1, CURRENT_TIMESTAMP, '$accno', 'Yes', '$customer_origin','$ip_version','$email_id', '$maintained_by')";
  } else {
	  $sql_command = "INSERT INTO route_bulkupload VALUES('$route', '$prefix_type',  '$as_no', '$opshandle', 0, CURRENT_TIMESTAMP, '$accno', 'Yes', '$customer_origin','$ip_version','$email_id', '$maintained_by')";
  }
  my $sth = $dbh->prepare($sql_command);
  unless ($sth->execute) {
	  $error = $dbh::err;
	  return 0;
  }
  $sth->finish;
  return 1;
}


#Chandini_IPT_ph5
sub get_data_route_bulkupload{
#Added new field customer origin -CRQ9161
  my $sql_command = "SELECT file_name, prefix_type, email_id, maintained_by, as_no, service_id, customer_id, customer_origin, ip_version FROM route_bulkupload
                   where status = 0"; 
  my $sth = $dbh->prepare ($sql_command);
  unless ($sth->execute) {
	  $error = $dbh::err;
	  return 0;
  }
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  return $result;
}

#Chandini_IPT_ph5
sub update_status_bulkupload {
  my $sth = $dbh->prepare("UPDATE route_bulkupload SET status = 1
			where status = 0 ");
  $sth->execute;
  $sth->finish;
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  return 1;
}

#Chandini_IPT_ph5
sub get_cmsdb_bgpdump {
  my $route = shift @_; 
  my $sql_command = "select as_path from bgp_prefix_list where prefix like '$route'";
  my $sth = $dbh->prepare ($sql_command);
  unless ($sth->execute) {
          $error = $dbh::err;
          return 0;
  }
  my $result = ($sth->fetchrow_array)[0];
  $sth->finish;
  return $result;
}
#Chandini_IPT_ph5
sub get_wholesaler_template {
  my $accno = shift @_;
  my $sql_command = "select template_path from wholesaler left outer join customer on wholesaler.customer_id = customer.wholesaler_customer_id where customer.customer_id like '$accno'";
  my $sth = $dbh->prepare ($sql_command);
  unless ($sth->execute) {
          $error = $dbh::err;
          return 0;
  }
  my $result = ($sth->fetchrow_array)[0];
  $sth->finish;
  return $result;
}

   
#Chandini_IPT_ph5
sub check_route_db {
  my $route = shift @_;
  my $sql_command = "select count(*) from routing_queue where route like '$route'";
  my $sth = $dbh->prepare ($sql_command);
  unless ($sth->execute) {
          $error = $dbh::err;
          return 0;
  }
  my $result = ($sth->fetchrow_array)[0];
  $sth->finish;
  return $result;
}

#Chandini_IPT_ph5
sub check_irrd_object {
  my $as_no = shift @_;
  my $sql_command = "select service_id from service where as_no like '$as_no' and irrd_object != ''";
  my $sth = $dbh->prepare ($sql_command);
  unless ($sth->execute) {
          $error = $dbh::err;
          return 0;
  }
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  return $result;
}

#Chandini_IPT_ph5
sub get_router_software {
  my $service_id = shift @_;
  ##IPT5
  ##Modified by Karuna on 22Dec2010 to take care of deleted services
  #my $sql_command = "select router_software from router_software join routername on router_software.routername = routername.routername left outer join link on routername.routerid = link.routerid where link.service_id like '$service_id'";
  my $sql_command = "select router_software from router_software join routername on router_software.routername = routername.routername left outer join link on routername.routerid = link.routerid where link.service_id like '$service_id'
		     	UNION
			SELECT router_software FROM router_software,routername, deleted_links_routing where routername.routername = router_software.routername AND deleted_links_routing.routername = router_software.routername AND deleted_links_routing.service_id = '$service_id'";	
  my $sth = $dbh->prepare ($sql_command);
  unless ($sth->execute) {
          $error = $dbh::err;
          return 0;
  }
  my $result = ($sth->fetchrow_array)[0];
  $sth->finish;
  return $result;
}

#Addendum_2  New Function #Chandini
sub get_router_software_bha {
  my $router = shift @_;
  my $sql_command = "select router_software from router_software where routername like '$router'";
open(DEBUG33,">/data1/tmp_log/Ph5_DEBUG33");
print DEBUG33 "inside get_router_software : sql_command:$sql_command\n";
  my $sth = $dbh->prepare ($sql_command);
  unless ($sth->execute) {
          $error = $dbh::err;
          return 0;
  }
  my $result = ($sth->fetchrow_array)[0];
  $sth->finish;
  return $result;
}
#End Addendum_2



#Addendum_2 #Chandini
sub get_bha_routername {
	 my $service_id = shift @_;
	 my $router_num = shift @_;
	 #my $sql_command = "select routername from routername,link,service where routername.routerid=link.bha_routerid and link.service_id = service.service_id and service.service_id = '$service_id'";
	 my $sql_command;
  	 open(DEBUG34,">/data1/tmp_log/Ph5_DEBUG34");
	 if ($router_num == 0) {
		 $sql_command = "select routername from routername,link,service where routername.routerid=link.bha_routerid and link.serservice_id viceid = service.service_id and service.service_id = '$service_id'
				UNION
				SELECT bha_routername from deleted_links_routing where service_id = '$service_id'";

				print DEBUG34 "inside get_bha_routername Primary: sql_command:$sql_command\n";
	}else {
		 $sql_command = "select routername from routername,link,service where routername.routerid=link.bha_routerid_bac and link.service_id = service.service_id and service.service_id = '$service_id'
				UNION
				SELECT bha_routername_bac from deleted_links_routing where serviceid = '$service_id'";

				print DEBUG34 "inside get_bha_routername Secondary: sql_command:$sql_command\n";
	}
  my $sth = $dbh->prepare ($sql_command);
  unless ($sth->execute) {
          $error = $dbh::err;
          return 0;
  }
  my $result = ($sth->fetchrow_array)[0];
  $sth->finish;
  return $result;
}

#Chandini_IPT_ph5
sub get_bha_gen_prefix_by_service {
        my $sid = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT bha_auto_gen_prefix FROM service where serviceid = $sid");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#Chandini_IPT_ph5_03March
sub get_irrd_route_action2_status4 {
  my $as = shift @_ || "";
  my $opshandle = shift @_ || "";

  #my $sql_command = "SELECT route FROM routing_queue
  #                 where routing_queue.as_no = '$as' and routing_queue.status = 4 and routing_queue.action = 2";
  my $sql_command = "SELECT route FROM routing_queue
                   where routing_queue.as_no = '$as' and routing_queue.status = 4 
				   and routing_queue.action = 2 and routing_queue.opshandle = '$opshandle'";

  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

#Chandini_IPT_ph5_03March
#Addendum_2 #Chandini
sub update_to_act1sts0_irrd {
	my $opshandle = shift @_;
	my $route = shift @_;
	my $as_no = shift @_;
	my $ip_version = shift @_;

	my $sth = $dbh->prepare ("UPDATE routing_queue
                               SET status = 0, action = 1, bha_status = 3, backup_bha_status = 3, time_entered = CURRENT_TIMESTAMP, time_complete = null , bha_time_complete = null, backup_bha_time_complete = null,ip_version= '$ip_version'
								where route = '$route' and as_no = '$as_no' and opshandle = '$opshandle'");

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  return 1;
}



sub get_routing_request_by_as_v2 {
  my $as = shift @_ || "";
  my $status = shift @_ || "";

##  my $sql_command = "SELECT opshandle, route FROM routing_queue, service 
##                    where routing_queue.opshandle = service.serviceid and 
##                    service.as_no = '$as'";
  my $sql_command = "SELECT opshandle, route FROM routing_queue
                   where routing_queue.as_no = '$as'";
  
  if ($status ne "") {
     $sql_command .= " and routing_queue.status = '$status'";
  }
  print "next sql command : $sql_command \n";
  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

# For getting the deleted routes with the status of 3 and the action of 2. -- Incremental Update.
sub get_routing_request_by_as_v3 {
  my $as = shift @_ || "";
  my $status = shift @_ || "";
  my $action = shift @_ || "";
  my $ip_version = shift @_ || "";
  my $serviceid = shift @_ || "";

##  my $sql_command = "SELECT opshandle, route FROM routing_queue, service 
##                    where routing_queue.opshandle = service.serviceid and 
##                    service.as_no = '$as'";
  my $sql_command = "SELECT opshandle, route FROM routing_queue
                   where routing_queue.as_no = '$as'";
  
  if ($status ne "") {
# code change to handle null value
     $sql_command .= " and routing_queue.status = '$status'";
  } else {
     $sql_command .= " and nvl(routing_queue.status,479) = '479'";
  }
  if ($action ne "") {
     $sql_command .= " and routing_queue.action = '$action'";
  }
  if ($ip_version eq "IPv4") {
     $sql_command .= " and (routing_queue.ip_version ='$ip_version' OR NVL(routing_queue.ip_version,'NULL') = 'NULL')";
  }
  if ($ip_version eq "IPv6") {
     $sql_command .= " and routing_queue.ip_version IN ('$ip_version')";
  }
  if ($serviceid ne "") {
     $sql_command .= " and routing_queue.opshandle IN ('$serviceid')";
  }
print "Sql query = $sql_command\n";
  
  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

#IPT_ph5
#Addendum_2 #Chandini
sub get_routing_request_by_as_v3_bha {
  my $as = shift @_ || "";
  my $status = shift @_ || "";
  my $action = shift @_ || "";
  my $router_num = shift @_ || "";
  my $ip_version = shift @_ || "";
  my $serviceid = shift @_ || "";

  my $sql_command = "SELECT opshandle, route, ip_version FROM routing_queue
                   where routing_queue.as_no = '$as'";

  if ($status ne "") {
   if($router_num == 0){
     $sql_command .= " and routing_queue.bha_status = '$status'";
   }else{
     $sql_command .= " and routing_queue.backup_bha_status = '$status'";
   }
  }
  if ($action ne "") {
     $sql_command .= " and routing_queue.action = '$action'";
  }
  if ($ip_version eq "IPv4") {
     $sql_command .= " and (routing_queue.ip_version ='$ip_version' OR NVL(routing_queue.ip_version,'NULL') = 'NULL')";
  }
  if ($ip_version eq "IPv6") {
     $sql_command .= " and routing_queue.ip_version IN ('$ip_version')";
  }
  if ($serviceid ne "") {
     $sql_command .= " and routing_queue.opshandle IN ('$serviceid')";
  }

 print "sql command = $sql_command\n";


  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}


sub get_routing_request_by_trackno {
  
  if (scalar @_ != 1) {
    $error = 
      "Incorrect number of arguments passed to retrieve_routing_change.";
    return 0;
  }

  my $trackno = shift @_;

  return get_routing_request($trackno);

}

sub get_pending_requests {
  
  if (scalar @_ != 0) {
    $error = 
      "Incorrect number of arguments passed to retrieve_routing_change.";
    return 0;
  }

  my $sql_command = ("SELECT trackno, opshandle, route
                      FROM routing_queue 
                      WHERE status = 1;
                            ");

  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;

}

sub get_routing_request_by_opshandle {
$logger->debug("Inside CMS ROUTING QUEUE_DB. sub get_routing_request_by_opshandle\n");
  if (scalar @_ != 1) {
    $error = 
      "Incorrect number of arguments passed to get_routing_request_by_opshandle.";
    return 0;
  }

  my $opshandle = shift @_;
$logger->debug(" opshandle value>>>>>>>$opshandle \n");
  my $as_res = get_as_by_opshandle($opshandle, "all");
$logger->debug("as_res value>>>>>>>$as_res \n");
  my $ops_as = $as_res->[0][0];
$logger->debug("ops_as value>>>>>>>$ops_as \n");
	#11-Nov-2010 Added by Karuna
	if ($debug) {print IPT_RM "DB opshandle:$opshandle\t ops_as:$ops_as\n";}

  #Black Hole routing option for the opshandle
   my $black_hole_routing = get_bha_option($opshandle);
$logger->debug("black_hole_routing value>>>>>>>$black_hole_routing \n");
   my $bha_option = $black_hole_routing->[0][0];
$logger->debug("bha_option value>>>>>>>$bha_option\n");

   #querying the prefixlist from DB
   my $sql_command = "SELECT r.trackno, '$opshandle', a.actiontext, r.route, r.time_entered,
                      r.time_complete, s.statustext
                      FROM routing_queue r, routing_queue_status s, routing_queue_action a";

   if ($bha_option ne 'Yes') {
   	$sql_command .= " WHERE r.as_no = '$ops_as' and r.status = s.status and r.action = a.action
                      order by r.time_entered desc";
   } else {
	#Sequence of 'OR' conditions explained: r.status = a, r.bha_status = b and r.backup_bha_status = c
	#1. a=b=c; status = a
	#2. a<b,a<c status = a #3. b<a,b<c status = b #4. c<b,c<a status = c
	#5. a=b,b<c status = b #6. a=c,c<b status = c #7. b=c,c<a status = c

   	$sql_command .= " WHERE r.as_no = '$ops_as' and r.action = a.action and ((r.status = r.bha_status and r.status = r.backup_bha_status and r.backup_bha_status = r.bha_status and r.status = s.status) or (r.status < r.bha_status and r.status < r.backup_bha_status and r.status = s.status) or (r.bha_status < r.status and r.bha_status < r.backup_bha_status and r.bha_status = s.status) or (r.backup_bha_status < r.status and r.backup_bha_status < r.bha_status and r.backup_bha_status = s.status) or (r.status = r.bha_status and r.bha_status < r.backup_bha_status and r.bha_status = s.status) or (r.status = r.backup_bha_status and r.backup_bha_status < r.bha_status and r.backup_bha_status = s.status) or (r.bha_status = r.backup_bha_status and r.backup_bha_status < r.status and r.backup_bha_status = s.status))
                      order by r.time_entered desc";
  }
  ## End of Addendum_2
$logger->debug("sql query is >>>>>>>$sql_command \n");
  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
   my $result = $sth->fetchall_arrayref;
  
$logger->debug("output for view route------->>>>>>>>>>>: $result->[0]->[0],$result->[1]->[0]");
        $error = $dbh::err;
       return $result;

}

#Addendum_2 ##Chandini
#Querying the Black Hole routing option for the opshandle
sub get_bha_option {
$logger->debug("in get bha option in routing queue db!!!!\n");
  if (scalar @_ != 1) {
    $error =
      "Incorrect number of arguments passed to get_bha_option.";
    return 0;
  }

  my $opshandle = shift @_;
$logger->debug("opshandle in routing q db>>>>>>>>$opshandle\n");
  my $sql_command = ("SELECT black_hole_routing FROM service WHERE service_id = '$opshandle'");
  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}
### Addendum_2

sub get_as_by_opshandle {
  
  if (scalar @_ != 2) {
    $error = 
      "Incorrect number of arguments passed to get_as_by_opshandle.";
    return 0;
  }

  my $opshandle = shift @_;
  my $prefix_option = shift @_;
  my $sql_command = ("SELECT as_no, bgp_neighbor_ip, service_id, black_hole_routing, bha_bgp_neighbor_ip 
                      FROM service");
  if ($opshandle ne "all") { 
     $sql_command .= " WHERE service_id = '$opshandle'";
  } else {
     $sql_command .= " WHERE as_no != ''";
  }

  if ($prefix_option ne "all") {
     $sql_command .= " and auto_gen_prefix = '$prefix_option'";
  }
  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

#***********************************************************
#Name		:get_all_as
#Author		:KARUNA BALLAL
#Description	:Get AS Number of last deleted service 
#Input		:
#Output		:AS number/s
#***********************************************************
sub get_all_as{
	my $opshandle = shift @_;
	my $sql_command = "";
	if ($opshandle eq ""){
		$sql_command = ("SELECT as_no, bgp_neighbor_ip, serviceid, black_hole_routing, bha_bgp_neighbor_ip FROM deleted_links_routing WHERE status= 2 AND as_no is not null 
			    UNION 
			    SELECT as_no, bgp_neighbor_ip, service_id, black_hole_routing, bha_bgp_neighbor_ip FROM service WHERE as_no is not null");
	} else {
		$sql_command = ("SELECT as_no, bgp_neighbor_ip, serviceid, black_hole_routing, bha_bgp_neighbor_ip FROM deleted_links_routing WHERE status= 2 AND serviceid = '$opshandle'
				UNION
				SELECT as_no, bgp_neighbor_ip, service_id, black_hole_routing, bha_bgp_neighbor_ip FROM service WHERE service_id = '$opshandle'");
	}

	my $sth = $dbh->prepare ($sql_command);
	unless ($sth->execute) {
		$error = $dbh::err;
		return 0;
	}
	my $result = $sth->fetchall_arrayref;
	$sth->finish;
	$error = $dbh::err;
	return $result;
}	


#Addendum_2  ##Chandini
sub get_all_as_bha{
	my $opshandle = shift @_;
	my $sql_command = "";
	if ($opshandle eq ""){
		$sql_command = ("SELECT as_no, bgp_neighbor_ip, serviceid, black_hole_routing, bha_bgp_neighbor_ip FROM deleted_links_routing WHERE (bha_status='2' OR bha_status_bac = '2') AND as_no != '' 
			    UNION 
			    SELECT as_no, bgp_neighbor_ip, service_id, black_hole_routing, bha_bgp_neighbor_ip FROM service WHERE as_no != ''");
	} else {
		$sql_command = ("SELECT as_no, bgp_neighbor_ip, serviceid, black_hole_routing, bha_bgp_neighbor_ip FROM deleted_links_routing WHERE (bha_status='2' OR bha_status_bac = '2') AND serviceid = '$opshandle'
				UNION
				SELECT as_no, bgp_neighbor_ip, service_id, black_hole_routing, bha_bgp_neighbor_ip FROM service WHERE service_id = '$opshandle'");
	}
	my $sth = $dbh->prepare ($sql_command);
	unless ($sth->execute) {
		$error = $dbh::err;
		return 0;
	}
	my $result = $sth->fetchall_arrayref;
	$sth->finish;
	$error = $dbh::err;
	return $result;
}	

#***********************************************************
#Name           :get_deleted_ops_as
#Author         :KARUNA BALLAL
#Description    :Get AS Number of deleted services
#Input          :
#Output         :AS number/s
#***********************************************************
#Addendum_2   #Chandini 
sub get_deleted_ops_as {
	my $sql_command = "";
## Code change to handle null values
	#$sql_command = ("SELECT as_no, bgp_neighbor_ip, serviceid, black_hole_routing, bha_bgp_neighbor_ip, routername, status, bha_status, bha_routername, bha_routername_bac, bha_status_bac FROM deleted_links_routing WHERE status='0' OR bha_status='0' OR bha_status_bac = '0' AND as_no != ''");
	$sql_command = ("SELECT as_no, bgp_neighbor_ip, serviceid, black_hole_routing, bha_bgp_neighbor_ip, routername, status, bha_status, bha_routername, bha_routername_bac, bha_status_bac FROM deleted_links_routing WHERE (status='0' OR bha_status='0' OR bha_status_bac = '0') AND NVL(as_no,'NULL') != 'NULL'");
	my $sth = $dbh->prepare ($sql_command);
        unless ($sth->execute) {
                $error = $dbh::err;
                return 0;
        }
        my $result = $sth->fetchall_arrayref;
        $sth->finish;
        $error = $dbh::err;
        return $result;
}

sub get_as_by_router_country {
  
  if (scalar @_ != 2) {
    $error = 
      "Incorrect number of arguments passed to get_as_by_router_country.";
    return 0;
  }

  my $country_id = shift @_;
  my $country_name = shift @_;

  if ( ($country_id eq '') && ($country_name eq '') ) {
    $error = "one of arguments should be defined.";
    return 0;
  }

###  my $sql_command = ("SELECT distinct a.as_no
###			FROM service a, link b, routername c, popname d, cityname e, countryname f
###			WHERE
###			a.serviceid = b.serviceid
###			and b.routerid = c.routerid
###			and c.popid = d.popid
###			and d.cityid = e.cityid
###			and e.cityid = f.cityid
###			and a.productcodeid = 'GIA'
###			and a.as_no != ''
###			");
my $sql_command = ("SELECT distinct a.as_no
			FROM service a, link b, routername c, popname d, cityname e, countryname f
			WHERE
			a.service_id = b.serviceid
			and b.router_name = c.routername
			and c.popid = d.popid
			and d.cityid = e.cityid
			and e.cityid = f.cityid
			and a.productcodeid = 'GID'
			and a.as_no != ' '
			");

  if ($country_id ne '') { 
     $sql_command .= " and f.countryid = $country_id ";
  } elsif ($country_name ne '') {
     $sql_command .= " and f.countryname = '$country_name' ";
  }
	print " first sql command \n  $sql_command\n";
  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub get_route_obj {
  my $action = lc(shift @_);
  my $status = shift @_;

  #my $sql_command = "select rq.trackno, rq.route, s.as_no from routing_queue rq, service s 
  #                   where rq.opshandle = s.serviceid ";
  #Included customer_origin-CRQ9161
  my $sql_command = "select trackno, route, as_no, customer_origin  from routing_queue";

  if ($action eq "add") {
     $sql_command .= " and action = 1 "; 
  } elsif ($action eq "delete") {
     $sql_command .= " and action = 2 ";
  }

  if ($status ne "") {
     $sql_command .= " and status = $status"; 
  }
  $sql_command =~ s/and/where/;
  my $sth = $dbh->prepare ($sql_command);
	print "first sql command is :$sql_command \n";
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
 
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
   
}

sub get_route_obj_ip {
  my $action = lc(shift @_);
  my $status = shift @_;
  my $ip_num = shift @_;

  #my $sql_command = "select rq.trackno, rq.route, s.as_no from routing_queue rq, service s 
  #                   where rq.opshandle = s.serviceid ";
  #Included customer_origin-CRQ9161
  my $sql_command = "select trackno, route, as_no, customer_origin  from routing_queue";

  if ($action eq "add") {
     $sql_command .= " and action = 1 "; 
  } elsif ($action eq "delete") {
     $sql_command .= " and action = 2 ";
  }

  if ($status ne "") {
     $sql_command .= " and status = $status"; 
  }     
  $sql_command =~ s/and/where/;
  
  if ($ip_num == 0) {
     $sql_command .= " and (ip_version = 'IPv4' OR NVL(ip_version,'NULL') = 'NULL') "; 
  } elsif ($ip_num == 1) {
     $sql_command .= " and ip_version = 'IPv6' ";
  }
  
  my $sth = $dbh->prepare ($sql_command);
	print "first sql command is :$sql_command \n";
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
 
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
   
}


#Chandini_IPT_ph5
#Addendum-2 #Chandini
sub get_route_obj_bha {
  my $action = lc(shift @_);
  my $status = shift @_;

  my $sql_command = "select trackno, route, as_no, bha_status, backup_bha_status from routing_queue";

  if ($action eq "add") {
     $sql_command .= " and action = 1 ";
  } elsif ($action eq "delete") {
     $sql_command .= " and action = 2 ";
  }

  if ($status ne "") {
     $sql_command .= " and (bha_status = $status or backup_bha_status = $status)";
# Code added for testing - Should be removed == start
     #$sql_command .= " and as_no = 'AS865052'";
# Code added for testing - Should be removed == ends
  }

  $sql_command =~ s/and/where/;

  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;

}


sub get_route_obj_v1 {
  my $action = lc(shift @_);
  my $status = shift @_;
  my $as = shift @_;

  #my $sql_command = "select rq.trackno, rq.route, s.as_no from routing_queue rq, service s 
  #                   where rq.opshandle = s.serviceid ";
  my $sql_command = "select trackno, route, as_no from routing_queue";

  if ($action eq "add") {
     $sql_command .= " and action = 1 "; 
  } elsif ($action eq "delete") {
     $sql_command .= " and action = 2 ";
  }

  if ($status ne "") {
     $sql_command .= " and status = $status"; 
  }
  if ($as ne "") {
     $sql_command .= " and as_no = $as"; 
  }

  $sql_command =~ s/and/where/;
  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
 
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
   
}

sub get_prefix_list_by_as {
  if (scalar @_ != 2) {
    $error = 
      "Incorrect number of arguments passed to get_prefix_list_by_as.";
    return 0;
  }

  my $as = shift @_;
  my $bgp_neighbor_ip = shift @_;
  my $sql_command = ("SELECT prefix, next_hop, as_path  
                      FROM bgp_prefix_list 
                      WHERE as_path like '$as%'
		      AND peer_ip = '$bgp_neighbor_ip';
                     ");

  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub get_routing_request {

  my $trackno = shift @_ || "";
  my $opshandle = shift @_ || "";
  my $action = shift @_ || "";
  my $route = shift @_ || "";
  my $time_entered_start = shift @_ || "";
  my $time_entered_end = shift @_;
  my $time_complete_start = shift @_;
  my $time_complete_end = shift @_;

  my $status = shift @_; if (not defined($status)){$status="";}

  my $sql_command = ("SELECT trackno, opshandle, actiontext, route, 
                                   time_entered, 
                                   time_complete, statustext, errormessage, as_no
                            FROM routing_queue 
                              NATURAL JOIN routing_queue_status
                              NATURAL JOIN routing_queue_action ");

  if ($time_entered_start ne "") {
    $sql_command .= " AND time_entered >= '$time_entered_start'
                      AND time_entered <= '$time_entered_end'
                      AND ((time_complete >= '$time_complete_start'
                           AND time_complete <= '$time_complete_end') 
                       OR (time_complete IS NULL))";
  }
  if ($trackno ne "") { $sql_command .= "AND trackno LIKE '%$trackno%' "; }
  if ($route ne "") { $sql_command .= "AND route LIKE '%$route%' "; }
  if ($action ne "") { $sql_command .= "AND action = '$action' "; }
  if ($opshandle ne "") { $sql_command .= 
			    "AND opshandle LIKE '%$opshandle%' "; }
  if ($status ne "") { $sql_command .= "AND status = $status "; }
  $sql_command .= "order by time_entered desc" ;

  $sql_command =~ s/AND/WHERE/;

  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub accept_request {
  my $trackno = shift @_;

  my $sth = $dbh->prepare ("UPDATE routing_queue
                               SET status = 1,
                                   time_complete = CURRENT_TIMESTAMP
                            WHERE trackno = '$trackno'");

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  return 1;
}

sub complete_request {
  my $trackno = shift @_;

  my $sth = $dbh->prepare ("UPDATE routing_queue
                               SET status = 2,
                                   time_complete = CURRENT_TIMESTAMP,
                                   errormessage = NULL
                               WHERE trackno = '$trackno'");

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  return 1;
}

sub fail_request {
if ($debug){print IPT_RM "DB file:Entered fail_request\n";}
  my $trackno = shift @_;
  my $error = shift @_;
  $error = substr($error,0,1024);
  my $err_flag = shift @_;
  my $sth = "";
  my ($day, $month, $year) = (localtime)[3..5];
  $year = $year + 1900;
  $month = $month + 1;
  my $curr_date = sprintf("%04d-%02d-%02d", $year, $month, $day);
if ($debug){print IPT_RM "DB fail_request trackno:$trackno\terror:$error\nerr_flag:$err_flag\n";}
  
  if ($err_flag == 1){
	$sth = $dbh->prepare ("UPDATE routing_queue SET errormessage = '$error', time_complete = CURRENT_TIMESTAMP, status = 3 WHERE as_no in (select as_no from routing_queue where trackno ='$trackno') and (status =3 or status=4) and time_complete like '%$curr_date%'");
	if ($debug){print IPT_RM "DB fail_request query: UPDATE routing_queue SET errormessage = '$error', time_complete = CURRENT_TIMESTAMP, status = 3 WHERE as_no in (select as_no from routing_queue where trackno ='$trackno') and (status =3 or status=4) and time_complete like '%$curr_date%'";}
  } else { 
  	$sth = $dbh->prepare ("UPDATE routing_queue
                               SET errormessage = '$error',
                                   time_complete = CURRENT_TIMESTAMP,
                                   status = 3
                            WHERE trackno = '$trackno'");
  }
if ($debug){print IPT_RM "DB fail_request query: UPDATE routing_queue SET errormessage = '$error', time_complete = CURRENT_TIMESTAMP, status = 3 WHERE trackno = '$trackno'\n";}
  
  unless ($sth->execute) {
    $error = $dbh::err;
    if ($debug){print IPT_RM "DB fail_request error:$error\n";}
    return 0;
  }

 return 1;
}


# Feb 13, 2011
sub fail_request_bha_all {
if ($debug){print IPT_RM "DB file:Entered fail_request\n";}
  my $trackno = shift @_;
  my $error = shift @_;
  $error = substr($error,0,1024);
  my $sth = "";
if ($debug){print IPT_RM "DB fail_request_bha_all trackno:$trackno\terror:$error\n";}

        $sth = $dbh->prepare ("UPDATE routing_queue SET bha_errormessage = '$error', time_complete = CURRENT_TIMESTAMP, bha_status = 3 WHERE as_no in (select as_no from routing_queue where trackno ='$trackno') and bha_status =3 or bha_status=4");

if ($debug){print IPT_RM "DB fail_request_bha_all query: UPDATE routing_queue SET errormessage = '$error', time_complete = CURRENT_TIMESTAMP, status = 3 WHERE trackno = '$trackno'\n";}

  unless ($sth->execute) {
    $error = $dbh::err;
    if ($debug){print IPT_RM "DB fail_request_all error:$error\n";}
    return 0;
  }

 return 1;
}

#IPT_ph5
#Addendum_2 #Chandini
sub fail_request_bha {
  my $trackno = shift @_;
  my $error = shift @_;
  $error = substr($error,0,1024);
  my $err_flag = shift @_;
  my $router_num = shift @_;
  my $sth = "";
  my ($day, $month, $year) = (localtime)[3..5];
  $year = $year + 1900;
  $month = $month + 1;
  my $curr_date = sprintf("%04d-%02d-%02d", $year, $month, $day);
if ($debug){print IPT_RM "DB fail_request_bha query: UPDATE routing_queue SET errormessage = '$error', time_complete = CURRENT_TIMESTAMP, status = 3 WHERE trackno = '$trackno'\n";}

  if ($err_flag == 1){
	#Chandini Added here
	#Chandini: Changed 'time_complete' to bha_time_complete and backup_bha_time_complete check if its correctd
        if($router_num == 0){
		$sth = $dbh->prepare ("UPDATE routing_queue
                               SET bha_errormessage = '$error',
                                   bha_time_complete = CURRENT_TIMESTAMP,
                                   bha_status = 3
                            	WHERE as_no in (select as_no from routing_queue where trackno ='$trackno') 
				and (bha_status = 3 or bha_status=4) and bha_time_complete like '%$curr_date%'");
	} else {
		$sth = $dbh->prepare ("UPDATE routing_queue
                               SET backup_bha_errormessage = '$error',
                                   backup_bha_time_complete = CURRENT_TIMESTAMP,
                                   backup_bha_status = 3
                            	WHERE as_no in (select as_no from routing_queue where trackno ='$trackno') 
				and (backup_bha_status = 3 or backup_bha_status=4) and backup_bha_time_complete like '%$curr_date%'");
	}
  } else {
	#Chandini Added here
	if($router_num == 0){
		$sth = $dbh->prepare ("UPDATE routing_queue
				SET bha_errormessage = '$error',
				bha_time_complete = CURRENT_TIMESTAMP,
				bha_status = 3
				WHERE trackno = '$trackno'");
	}else{
		$sth = $dbh->prepare ("UPDATE routing_queue
				SET backup_bha_errormessage = '$error',
				backup_bha_time_complete = CURRENT_TIMESTAMP,
				backup_bha_status = 3
				WHERE trackno = '$trackno'");

	}
  }
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

 return 1;
}


sub reject_request {
  my $trackno = shift @_;

  my $sth = $dbh->prepare ("UPDATE routing_queue
                               SET status = -1,
                                   time_complete = CURRENT_TIMESTAMP
                            WHERE trackno = '$trackno'");

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  return 1;
}


#Addendum_2   ##Chandini #Bug fix to avoid dubplicate entry in the routing_queue table.
sub check_routing_exist {
  my $route = shift @_;
  my $action = shift @_;
  my $opshandle = shift @_;

  my $sth = $dbh->prepare ("select count(r.route) from routing_queue r, service s
       where s.service_id = r.opshandle and r.action = '$action' and r.route = '$route'
       and s.as_no = (select as_no from service where service_id = '$opshandle')");
  
  unless ($sth->execute) {
    $error = $dbh::err;
    return 1;
  }
  
  #return $sth->rows;
  my @row_num = $sth->fetchrow_array;
  return $row_num[0];
}

#02 Feb added for IPT_ph5 by Chandini
sub check_routing_exist_allstatus {
  my $route = shift @_;
  my $action = shift @_;
  my $opshandle = shift @_;

  my $sth = $dbh->prepare ("select r.route from routing_queue r, service s
       where s.service_id = r.opshandle and r.action = '$action' and r.route = '$route'
       and s.as_no = (select as_no from service where service_id = '$opshandle')");

  unless ($sth->execute) {
    $error = $dbh::err;
    return 1;
  }

    my @row_num = $sth->fetchrow_array;
  return $row_num[0];
}



sub check_reach_ip {
  my $route = shift @_;
 # my $sth = $dbh->prepare("select inet '$route' <<= ip_block from reach_ip_block");
  my $sth = $dbh->prepare("SELECT * from reach_ip_block WHERE ip_block in '$route'");
  unless ($sth->execute) {
    $error = $dbh::err;
    return 1;
  }
 
  my $res;
  while ($res = $sth->fetchrow_arrayref) {
    if ($res->[0]) {
       return 1;
    }
  } 
  return 0; 
}

sub check_bad_host {
  my $route = shift @_;
  my $as = shift @_;
  #my $sth = $dbh->prepare("select inet '$route' <<= ip_block from bad_host_list where as_no = '$as'");
  my $sth = $dbh->prepare("select * from bad_host_list where ip_block in '$route' and as_no in '$as'");
  unless ($sth->execute) {
    $error = $dbh::err;
    return 1;
  }
 
  my $res;
  while ($res = $sth->fetchrow_arrayref) {
    if ($res->[0]) {
       return 1;
    }
  } 
  return 0; 
}

sub get_transaction_list {
  my $sql_command = "SELECT route_trackno, opshandle, actiontext, route, time_entered, time_complete 
			FROM routing_transaction_log  
			NATURAL JOIN routing_queue_action order by time_entered desc";

  my $sth = $dbh->prepare ($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub take_log {
  my $track_no = $dbh->quote(shift @_);

my @delete_route_id = split(",", $track_no);

  my $route = $dbh->quote(shift @_);
  my $opshandle = $dbh->quote(shift @_);
  my $action = $dbh->quote(shift @_);
$logger->debug("trackno in take log: $track_no\n $route \n $opshandle \n$action");

#have inserted sequnce change here
foreach my $route_id (@delete_route_id) { 

$route_id= substr( $route_id, 1, (length($route_id) - 2) );
$logger->debug("trackno in foreach : $route_id");
  my $sql_command = "insert into routing_transaction_log 
			(transaction_no, route_trackno, route, opshandle, action, time_entered) 
			values (routing_transaction_log_seq.nextval,$route_id, $route, $opshandle, 
			$action, CURRENT_TIMESTAMP)";
 
  my $sth = $dbh->prepare($sql_command);
  $sth->execute;
  $sth->finish;
}

}

sub get_as_path {
#Comment by reni  
#my $sql_command = "select distinct as_path from bgp_prefix_list where as_path != ''";
  my $sql_command = "select distinct to_char(as_path) from bgp_prefix_list where as_path is not null";
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub is_customer_as {
  if (scalar @_ != 1) {
    $error = 
      "Incorrect number of arguments passed to is_customer_as.";
    return 0;
  }

  my $as = shift @_;
  my $sql_command = "select as_no from service where as_no = '$as'";
  my $sth = $dbh->prepare($sql_command);
  
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $row = $sth->rows;
  $sth->finish;
  $error = $dbh::err;
  return $row;
}

sub as_path_exist {
  if (scalar @_ != 1) {
    $error = 
      "Incorrect number of arguments passed to as_path_as.";
    return 0;
  }

  my $as_path = shift @_;
  my $sql_command = "select * from as_path_list where as_path = '$as_path'";
  my $sth = $dbh->prepare($sql_command);
  
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  $sth->finish;
  $error = $dbh::err;
  return $sth->rows;
}

sub get_new_as_path {
  my $sql_command = "select distinct as_path from bgp_prefix_list where as_path != '' except 
                     select distinct as_path from as_path_list where as_path != ''";
  my $sth = $dbh->prepare($sql_command);
 
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub get_cust_new_as_path {
  my $as_list = CMS_SERVICE_DB::get_customer_as();
	print "\n";
	print $#{$as_list};
	print "\n";
print "\ninside get_cust_new_as_path as list\n";
  my $cond = "where dbms_lob.substr(as_path) in (";
  if ($#{$as_list} > 0) {
    for my $i (0 .. $#{$as_list}){
      if ($i != 0) {
       $cond .= ",";
     }
      #$cond .= " $as_list->[$i][0]\|\^$as_list->[$i][0]";
      #$cond .= "\^$as_list->[$i][0]";
	$cond .= "'$as_list->[$i][0]'";
    } 
    $cond .= ")";

     my $sql_command = "select distinct dbms_lob.substr(as_path) from bgp_prefix_list *COND* 
                       and communities like '%4637:60950%' minus
                       select distinct dbms_lob.substr(as_path) from as_path_list *COND*";
    $sql_command =~ s/\*COND\*/$cond/g;

	print $sql_command;
    my $sth = $dbh->prepare($sql_command);

    unless ($sth->execute) {
      $error = $dbh::err;
      return 0;
    }
  
    my $result = $sth->fetchall_arrayref;
	

    $sth->finish;
print $#{$result};
    $error = $dbh::err;
    return $result;
  } else {
    return 0;
  }
}

sub get_del_as_path {
  my $sql_command = "select distinct as_path from as_path_list where as_path != '' 
                     EXCEPT 
                     select distinct as_path from bgp_prefix_list where as_path != ''";
  my $sth = $dbh->prepare($sql_command);
 
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub get_cust_del_as_path {
  my $as_list = CMS_SERVICE_DB::get_customer_as();
  my $cond = "where as_path ~* '";

  for my $i (0 .. $#{$as_list}){
    if ($i != 0) {
      $cond .= "\|";
    }
    #$cond .= " $as_list->[$i][0]\|\^$as_list->[$i][0]";
    $cond .= "\^$as_list->[$i][0]";
  } 
  $cond .= "'";

  my $sql_command = "select distinct as_path from as_path_list *COND* 
                     and communities like '%4637:60950%' except 
                     select distinct as_path from bgp_prefix_list *COND*";
  $sql_command =~ s/\*COND\*/$cond/g;

  my $sth = $dbh->prepare($sql_command);
 
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub make_as_path_image_old {
#  #remove existing as_path image
#  my $sql_command = "delete from as_path_list";
#  my $sth = $dbh->prepare($sql_command);
#  unless ($sth->execute) {
#    $error = $dbh::err;
#    return 0;
#  }
  
  #create a new as path image
  my $sql_command = "insert into as_path_list (prefix, as_path) select distinct prefix, as_path from bgp_prefix_list except select distinct prefix, as_path from as_path_list"; 
  my $sth = $dbh->prepare($sql_command);
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  $sth->finish;
  return;
}


sub make_as_path_image {

  #remove existing as_path image weekly, i.e. on Wed.
  my @dt = gmtime();
  if ($dt[6] == 3) { 
    my $sql_command = "delete from as_path_list";
    my $sth = $dbh->prepare($sql_command);
    unless ($sth->execute) {
      $error = $dbh::err;
      return 0;
    }
  }
  
  #create a new as path image
###aruna
####my $sql_command = "insert into as_path_list (prefix, as_path) select distinct prefix, as_path from bgp_prefix_list except select distinct prefix, as_path from as_path_list"; 
my $sql_command = "insert into as_path_list (prefix, as_path) select distinct prefix, to_char(as_path) from bgp_prefix_list minus select distinct prefix, to_char(as_path) from as_path_list";
  my $sth = $dbh->prepare($sql_command);
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  $sth->finish;
  return;
}


sub as_exist {
  if (scalar @_ != 1) {
    $error =
      "Incorrect number of arguments passed to as_exist.";
    return 0;
  }

  my $as = shift @_;
  my $sql_command = "select * from as_list where as_no = '$as'";
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $row = $sth->rows;
  $sth->finish;
  $error = $dbh::err;
  return $row;
}

sub get_as_list {
  my $sql_command = "select distinct as_no from as_list where as_no != ''";
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub insert_as {
  my $as = shift @_;
  my $sql_command = "insert into as_list values ('$as')"; 
  my $sth = $dbh->prepare($sql_command);
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  $sth->finish;
  return;
}

sub delete_as {
  my $as = shift @_;
  my $sql_command = "select count(as_path) from bgp_prefix_list 
                     where as_path like '$as %' or as_path like '% $as %' or as_path like '% $as'";
  my $sth = $dbh->prepare($sql_command);
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  if ((($sth->fetchrow_array)[0]) <= 1) {
    $sql_command = "delete from as_list where as_no = '$as'";
    $sth = $dbh->prepare($sql_command);
    $sth->execute;
    $sth->finish;
    return 1;
  }
  $sth->finish;
  return;
}

sub get_rollback_date {
  my @date_range;
  my $no_of_date = shift @_;
  if (not defined $no_of_date) {  
     $no_of_date = 7;
  }
  for my $i (1 .. $no_of_date) {
    my $sth = $dbh->prepare("select current_date - interval '$i day'");
    $sth->execute;
    my $res = $sth->fetchrow_arrayref;
    $date_range[$i][0] = $res->[0];
  }
  return \@date_range;
}

sub make_rollback_db {
  #my $sql_command = "select backup_time from routing_rollback where backup_time = current_date - interval '1 day'";
  my $sql_command = "select backup_time from routing_rollback where backup_time = sysdate-1";
 
  my $sth = $dbh->prepare($sql_command);
  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  if ($sth->rows == 0) {
    $sql_command = "select trackno, route, opshandle, time_complete, as_no from routing_queue where status = 3 or status = 4";
    my $sth2 = $dbh->prepare($sql_command);
    my $result;
    unless ($sth2->execute) {
      $error = $dbh::err;
      return 0;
    }
	
    while ($result = $sth2->fetchrow_arrayref) {
      $sql_command = "insert into routing_rollback (trackno,route,opshandle,time_complete,backup_time,as_no)
                     values ('$result->[0]', '$result->[1]', '$result->[2]', '$result->[3]', sysdate-1, '$result->[4]')"; 
      my $sth3 = $dbh->prepare($sql_command);
	  print($sql_command);
      $sth3->execute;
    }

    #remove outdated backup
    $sql_command = "delete from routing_rollback where backup_time < sysdate-8";
    my $sth3 = $dbh->prepare($sql_command);
    $sth3->execute; 
  }
  $sth->finish;
  return;
}

sub rollback_prefix {
  if (scalar @_ != 2) {
    $error = "Incorrect number of arguments passed to rollback_prefix.";
    return 0;
  }

  my $opshandle = shift @_;
  my $rollback_date = shift @_;
  my ($result, $ser_res);
 
  #retrieve the entrie service id, which have the same AS number
  my $as_res = get_as_by_opshandle($opshandle, "all");
  my $ops_as = $as_res->[0][0];

  my $sql_command = "select serviceid from service where as_no = '$ops_as'"; 
  my $ser_sth = $dbh->prepare($sql_command);
  unless ($ser_sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  while ($ser_res = $ser_sth->fetchrow_arrayref) {
    my $pro_service = $ser_res->[0];

    #retrieve any new prefix
    $sql_command = "select trackno from routing_queue where opshandle = '$pro_service' EXCEPT
                    select trackno from routing_rollback where backup_time = '$rollback_date' and opshandle = '$pro_service'";
    my $sth = $dbh->prepare($sql_command);
    unless ($sth->execute) {
      $error = $dbh::err;
      return 0;
    }
  
    while ($result = $sth->fetchrow_arrayref) {
      delete_routing_request($result->[0], $pro_service, 2);
    }
  
    #retrieve any deleted prefix
    $sql_command = "select route, as_no from routing_rollback 
                       where backup_time = '$rollback_date' and opshandle = '$pro_service' EXCEPT
                       select route, as_no from routing_queue where opshandle = '$pro_service'";
    $sth = $dbh->prepare($sql_command);
  
    unless ($sth->execute) {
      $error = $dbh::err;
      return 0;
    }
  
    while ($result = $sth->fetchrow_arrayref) {
      add_routing_request($pro_service, 1, $result->[0], $result->[1]);
    }
    $sth->finish;
  }

  $ser_sth->finish;
  $error = $dbh::err;
  return 1;
}

sub update_opshandle {
  my $prev_ops = $dbh->quote(shift @_);
  my $ops = $dbh->quote(shift @_);
  my ($sth, $result);

  $sth = $dbh->prepare("update routing_queue set opshandle = $ops where opshandle = $prev_ops");
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  $sth = $dbh->prepare("update routing_transaction_log set opshandle = $ops where opshandle = $prev_ops");
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  return 1;
}

sub get_bad_route {
  my $route = $dbh->quote(shift @_);

  my $sql_command = "SELECT r.trackno, r.opshandle, a.actiontext, r.route, r.as_no, r.time_entered,
                      r.time_complete, s.statustext
                      FROM routing_queue r left outer join routing_queue_status s on r.status = s.status 
                      left outer join routing_queue_action a on r.action = a.action
                      where
                      inet($route) >>= inet(r.route)
                      order by r.time_entered desc
                     ";

  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}
  
sub get_route_notification {
  my $sql_command = "select distinct c.email, c.customer_id from contact c, routing_queue rq, service s where rq.opshandle = s.service_id and s.customer_id = c.customer_id and c.typeid = 2 and rq.status = 4 and rq.mail_flag = 0";

  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub update_route_notification {
  my $sql_command = "UPDATE routing_queue set mail_flag = 1 where status = '4'";
  
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  return 1;
}


#***************************************************************************************************
# AUTHOR	:KARUNA BALLAL
# Name		:get_track_no
# Description	:Get the track number of a routing request, given the opshandle and AS number
# Input		:route, service, asno
# Output	:track_no
#***************************************************************************************************
sub get_track_no {
	my $route = shift (@_);
	my $asno = $dbh->quote(shift (@_));
	if($debug){print IPT_RM "DB get_track_no route:$route\tasno:$asno\n";}
	my $sql_command = "SELECT trackno from routing_queue where route = '$route' and as_no = $asno";
	my $sth = $dbh->prepare($sql_command);
	unless ($sth->execute) {
    		$error = $dbh::err;
		if($debug){print IPT_RM "DB get_track_no error:$error\n";}
    		return "0";
	}
	my $track_no = $sth->fetchall_arrayref;
	#my @track_no = $sth->fetchall_array;
	$sth->finish;
	if($debug){print IPT_RM "DB get_track_no before return track_no:@$track_no\n";}
	return $track_no;
}

#######  CRQ000000004421: 15-11-11 Chandini
#Get the as_no number from the 'deleted_links_routing' where status = 2
sub get_as_status2_from_dlr {
        my $sql_command = "select as_no from deleted_links_routing where status = 2";
        my $sth = $dbh->prepare ($sql_command);
        unless ($sth->execute) {
                $error = $dbh::err;
                return 0;
        }
        my $result = $sth->fetchall_arrayref;
        $sth->finish;
        $error = $dbh::err;
        return $result;
}
#Deleting all the entries from routing_queue table for the AS number whose last service is deleted.
sub delete_prefix_for_as {
  my $as = $dbh->quote(shift @_);
  my $sth;

  $sth = $dbh->prepare("delete from routing_queue where as_no = $as and action = 2");
  $sth->execute;
  $sth->finish;

  return;
}

#Change the all the status(status, bha_status and back_bha_status) to 1 in 'deleted_links_routing'
sub update_deleted_link_status {
                my $update_db_query = $dbh->prepare("UPDATE deleted_links_routing SET status = 1, bha_status = 1, bha_status_bac = 1 WHERE status = 2");
                $update_db_query->execute();
                $error = $dbh->errstr;
                $update_db_query->finish();
                return;
}
#######  CRQ000000004421: 15-11-11 Chandini

=pod
sub get_as_by_ops {
	my $opshandle = $dbh->quote(shift (@_));
	if($debug){print IPT_RM "DB  get_as_by_ops opshandle:$opshandle\n";}
	my $sql_command = "SELECT as_no from service where serviceid = $opshandle";
	my $sth = $dbh->prepare($sql_command);
	unless ($sth->execute) {
                $error = $dbh::err;
                if($debug){print IPT_RM "DB get_as_by_ops error:$error\n";}
                return "0";
        }
	my $as_num_curr = $sth->fetchrow_arrayref;
        $sth->finish;
        if($debug){print IPT_RM "DB get_as_by_ops before return as_num_curr:$as_num_curr\n";}
 	return $as_num_curr;
=cut


	

1;
