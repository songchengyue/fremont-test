# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 11 December 2001
# File: CMS_CUSTOMER_DB.pm
#
# $Id: CMS_SERVICE_DB.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $
###################################################################
# File        : CMS_SERVICE_DB.pm
# Description : Executes functions called from CMS_SERVICE.pm and interacts with tables in database for data queries 
# Parameters to be passed : serviceID,customer,description,bandwidth,Product code,CoS type,CoS values, portmode, aggregate_ser
#viceid, billable options
# Author      : KETAKI THOMBARE (Infosys Tech. Ltd., Bangalore )
# Started On  : 09 July 2009
# Modification History :
#  Date               Name                 Change/Description
# ------------------------------------------------------------------
# 11-Aug-2009    Ketaki Popat Thombare    Baseline for EVPL
# 29-Aug-2009    Meena Prakash            Added changes for search EVPL
# 14-Oct-2009	 Nisha Sasindran	  Added changes for mVPN: Add, Edit and Search
# 05-Feb-2010    Karuna Ballal            Added changes for EPL: Add, Edit and Search
# 25-AUG-2010	 Stavan Shah		  Addded changes for VLA ME: Add, Edit, Search
# 09-SEP-2010	 Karuna Ballal		  Added changes for IP Transit: Add, Edit, Search
# 14-JUN-2011    Maneet Kaur		  Added changes for IP Transit Addendum 2 
#######################################################################
package CMS_SERVICE_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
use CMS_ROUTING_QUEUE_DB;

# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;

our $error;
our $error1;

#Added by Karuna for IPT5
my $debug = 1;
#To disable debug logs, uncomment the following line and vice versa
#$debug = 0;
if ($debug){open (IPT_DBG, ">>/data1/tmp_log/service_db.log");}

sub add_service {
  CMS_DB::begin_transaction();
  my $serviceid_raw = shift @_;
  my $serviceid = $dbh_tr->quote($serviceid_raw);
  my $accno_raw = shift @_;
  my $accno = $dbh_tr->quote($accno_raw);
  my $description = $dbh_tr->quote(shift @_);
  my $currentbw = $dbh_tr->quote(shift @_);
  if ($debug){print IPT_DBG "DB currentbw passed: $currentbw\n";}
  my $productcode_raw = shift @_;
  my $productcode = $dbh_tr->quote($productcode_raw);
  #Karuna - Declare all variables used for EPL
  my ($epl_msid,$epl_aend_jid,$epl_zend_jid,$epl_aend_jno,$epl_zend_jno,$epl_aend_jid2,$epl_zend_jid2,$epl_aend_jno2,$epl_zend_jno2,$billable,$epl_billable,$epl_billable2,$billstartdate_year,$billstartdate_month,$billstartdate_day,$billstarttime_hour,$billstarttime_minute,$billstarttime_second,$billenddate_year,$billenddate_month,$billenddate_day,$billendtime_hour,$billendtime_minute,$billendtime_second,$as_no,$auto_gen_prefix,$bgp_neighbor_ip,$irrd_object,$ipt_msid) = "";
  #Declared for IP Transit phase 5 - kamal
  my ($black_hole_routing,$bha_bgp_neighbor_ip,$bha_auto_gen_prefix) = "";
  if ($productcode_raw =~ /EPL/){
  #Karuna - When product code is EPL
  $epl_msid = $dbh_tr->quote(shift @_);
  $epl_aend_jid = $dbh_tr->quote(shift @_);
  $epl_zend_jid = $dbh_tr->quote(shift @_);
  $epl_aend_jno = $dbh_tr->quote(shift @_);
  $epl_zend_jno = $dbh_tr->quote(shift @_);
  $epl_aend_jid2 = $dbh_tr->quote(shift @_);
  $epl_zend_jid2 = $dbh_tr->quote(shift @_);
  $epl_aend_jno2 = $dbh_tr->quote(shift @_);
  $epl_zend_jno2 = $dbh_tr->quote(shift @_);
  # for billing dates
  $epl_billable = shift @_;
  $epl_billable2 = shift @_;
  $billstartdate_year = shift @_;
  $billstartdate_month = shift @_;
  $billstartdate_day = shift @_;
  $billstarttime_hour = shift @_;
  $billstarttime_minute = shift @_;
  $billstarttime_second = shift @_;
  $billenddate_year = shift @_;
  $billenddate_month = shift @_;
  $billenddate_day = shift @_;
  $billendtime_hour = shift @_;
  $billendtime_minute = shift @_;
  $billendtime_second = shift @_;
  } elsif ($productcode_raw =~ /IPTRANSIT/){
  	$as_no = $dbh_tr->quote(shift @_);
  	$auto_gen_prefix = $dbh_tr->quote(shift @_);
  	$bgp_neighbor_ip = $dbh_tr->quote(shift @_);
  	$irrd_object = $dbh_tr->quote(shift @_);
	$ipt_msid = $dbh_tr->quote(shift @_);
	$black_hole_routing = $dbh_tr->quote(shift @_);
	$bha_bgp_neighbor_ip = $dbh_tr->quote(shift @_);
	$bha_auto_gen_prefix = $dbh_tr->quote(shift @_);
	$billable = '2';
  }

  my $switchsourcode = $dbh_tr->quote(shift @_);
  my $switchdestcode = $dbh_tr->quote(shift @_);
  my $pollingtype = $dbh_tr->quote(shift @_);
  my $costype = $dbh_tr->quote(shift @_);
  my $costype_e = $dbh_tr->quote(shift @_);
  my $portmode_raw = shift @_;
  my $portmode = $dbh_tr->quote($portmode_raw); 
  my $portmode_raw_e = shift @_;
  my $portmode_e = $dbh_tr->quote($portmode_raw_e);
  my $gold_classmap_raw    = shift @_;
  my $gold_rateacl_raw     = shift @_;
  my $silver_classmap_raw  = shift @_;
  my $silver_rateacl_raw   = shift @_;
  my $default_classmap_raw = shift @_;
  my $default_rateacl_raw  = shift @_;
  my $class1_classmap_raw = shift @_;
  my $class1_rateacl_raw  = shift @_;
  my $class2_classmap_raw = shift @_;
  my $class2_rateacl_raw  = shift @_;
  my $class4_classmap_raw = shift @_;
  my $class4_rateacl_raw  = shift @_;
  my $class1_classmap = $dbh_tr->quote($class1_classmap_raw);
  my $class1_rateacl  = $dbh_tr->quote($class1_rateacl_raw);
  my $class2_classmap = $dbh_tr->quote($class2_classmap_raw);
  my $class2_rateacl  = $dbh_tr->quote($class2_rateacl_raw);
  my $class4_classmap = $dbh_tr->quote($class4_classmap_raw);
  my $class4_rateacl  = $dbh_tr->quote($class4_rateacl_raw);
  my $gold_temp = $gold_classmap_raw;
  my $gold_temp1 = $gold_rateacl_raw;
  my $gold_classmap    = $dbh_tr->quote($gold_temp);
  my $gold_rateacl     = $dbh_tr->quote($gold_temp1);
  my @arr_temp = qw($gold_temp $gold_temp1);
  my $silver_classmap  = $dbh_tr->quote($silver_classmap_raw);
  my $silver_rateacl   = $dbh_tr->quote($silver_rateacl_raw);
  my $default_classmap = $dbh_tr->quote($default_classmap_raw);
  my $default_rateacl  = $dbh_tr->quote($default_rateacl_raw);
  if ($productcode_raw !~ /IPTRANSIT/){
        $as_no = $dbh_tr->quote(shift @_);
        $auto_gen_prefix = $dbh_tr->quote(shift @_);
        $bgp_neighbor_ip = $dbh_tr->quote(shift @_);
        $irrd_object = $dbh_tr->quote(shift @_);
  }
  my $aggregate_serviceid = $dbh_tr->quote(shift @_);
  my $aggregate_serviceid_a = $dbh_tr->quote(shift @_);
  my $aggregate_serviceid_z = $dbh_tr->quote(shift @_);
  my $agg_servid_tpm_raw = shift @_; 
  my $aggregate_serviceid_tpm = $dbh_tr->quote($agg_servid_tpm_raw);
  my $master_serviceid = $dbh_tr->quote(shift @_);
  # for billing dates
  if ($productcode_raw !~ "EPL") {
  $billable = shift @_;
  $billstartdate_year = shift @_;
  $billstartdate_month = shift @_;
  $billstartdate_day = shift @_;
  $billstarttime_hour = shift @_;
  $billstarttime_minute = shift @_;
  $billstarttime_second = shift @_;
  $billenddate_year = shift @_;
  $billenddate_month = shift @_;
  $billenddate_day = shift @_;
  $billendtime_hour = shift @_;
  $billendtime_minute = shift @_;
  $billendtime_second = shift @_;
  }

  if ($productcode_raw =~ /GIA/){
	$black_hole_routing = $dbh_tr->quote(shift @_);
        $bha_bgp_neighbor_ip = $dbh_tr->quote(shift @_);
        $bha_auto_gen_prefix = $dbh_tr->quote(shift @_);
  }
  my $serviceid_alt_raw = shift @_;
  my $serviceid_alt = $dbh_tr->quote($serviceid_alt_raw);
  my $aend_jobid = $dbh_tr->quote(shift @_);
  my $zend_jobid = $dbh_tr->quote(shift @_);
  my $aend_jobno = $dbh_tr->quote(shift @_);
  my $zend_jobno = $dbh_tr->quote(shift @_);
  my $aend_country = $dbh_tr->quote(shift @_);
  my $zend_country = $dbh_tr->quote(shift @_);
  #Added by Nisha for mVPN on 25Sep2009
  my $voice_mvpn_in = $dbh_tr->quote(shift @_);
  my $video_mvpn_in = $dbh_tr->quote(shift @_);
  my $video_mvpn_out = $dbh_tr->quote(shift @_);
  my $criticaldata_mvpn_in = $dbh_tr->quote(shift @_);
  my $criticaldata_mvpn_out = $dbh_tr->quote(shift @_);
  my $interactivedata_mvpn_in = $dbh_tr->quote(shift @_);
  my $interactivedata_mvpn_out = $dbh_tr->quote(shift @_);
  my $standarddata_mvpn_in = $dbh_tr->quote(shift @_);
  my $standarddata_mvpn_out = $dbh_tr->quote(shift @_);
  my $lowpriority_mvpn_in = $dbh_tr->quote(shift @_);
  my $lowpriority_mvpn_out = $dbh_tr->quote(shift @_);
  my $mvpnCheck = shift @_;
  #End of code Added by Nisha for mVPN on 25Sep2009
#open (EPL1, ">/data1/tmp_log/db_add.log"); 
our $test = "";   
my $temp_sql =  "";
my $temp_sql_z =  "";
my $temp_sql_tpm_a = "";
my $temp_sql_tpm_z = "";
  if ($currentbw eq "''") { $currentbw = "null"; }
my $new_bw = $currentbw;
 # my $sth = $dbh_tr->prepare ("SELECT serviceid FROM service WHERE serviceid ilike $serviceid");
 # $sth->execute();
 # my $r = $sth->fetchrow_hashref;
 # my $temp_serviceid = $serviceid;
 # $temp_serviceid =~ s/\'//g;
 # if($$r{serviceid} =~ /^$temp_serviceid$/i) {
 #       return "Duplicated ServiceID.";
 # }
  if ($productcode =~ /EVPL/) {
        $test = $productcode; 
 	my $serviceid_e = $serviceid_raw."a";
 	my $sth = $dbh_tr->prepare ("SELECT serviceid FROM service WHERE serviceid ilike '$serviceid_e'");
 	$sth->execute();
  	my $r = $sth->fetchrow_hashref;
 	if($r){
		return "Duplicated ServiceID.";
	}
#Added by karuna for EPL
} elsif ($productcode =~ /EPL/) {
  #Karuna - EPL Add service to DB
	#Duplicated sevice ID
        my $sth = $dbh_tr->prepare ("SELECT serviceid FROM service WHERE serviceid ilike $serviceid");
        $sth->execute();
        my $r = $sth->fetchrow_hashref;
        my $temp_serviceid1 = $serviceid;
        $temp_serviceid1 =~ s/\'//g;
	$temp_serviceid1 = lc($temp_serviceid1);
	 $$r{serviceid} = lc($$r{serviceid});
        #print EPL1 "serviceid:$serviceid\ttemp_serviceid1:$temp_serviceid1\tr{serviceid}:$$r{serviceid}\n";
        #if($$r{serviceid} =~ /^$temp_serviceid1$/i) 
        if($$r{serviceid} eq "") { }
        elsif($$r{serviceid} == $temp_serviceid1) {
                #print EPL1 "ServiceID already exists. Please enter a different service id.\n";
                $error = "Service ID already exists. Please enter a different service id.";
                 return ;
        }
        #remove single quotes
        $new_bw =~ s/\'//g;
        #convert to bps before sending to DB
        $new_bw = $new_bw*1000000;
        #re attach single quotes
        $new_bw = $dbh_tr->quote($new_bw);
        #print EPL1 "new_bw:$new_bw\n";
        my $bandwidth = $currentbw;
        $currentbw = $new_bw;
        $bandwidth =~ s/\'//g;
        #Bandwidth validation-Check for bandwidth in digits
	#in case decimal is needed, uncomment following line
	if($bandwidth !~ /(^(\d+\.\d+|\d+\.|\.\d+)$)|(^\d+$)/) {
        $error = "Bandwidth must be a number. Please re enter the bandwidth.";
        return ;
        }
        #print EPL1 " bandwidth validation:$bandwidth\n";
        $sth = $dbh_tr->prepare ("SELECT serviceid FROM service WHERE serviceid ilike $serviceid");
        $sth->execute();
        $r = $sth->fetchrow_hashref;
        my $temp_serviceid = $serviceid;
        $temp_serviceid =~ s/\'//g;
        if($$r{serviceid} =~ /^$temp_serviceid$/i) {
                $error = "Service ID already exists. Please enter a different service id.";
                return ;
        }
#Added new telstra id condition-TGC0031549
	elsif ($temp_serviceid =~ m/epl/gi){
                if ($epl_msid !~ "'-- Select --'"){
                $billable = $epl_billable2;
                if ($epl_aend_jno2 eq "''"){
                #Job note cannot be blank
                $error = "A-end job note cannot be blank for multipoint EPL service.";
                return ;
                } elsif (($epl_aend_jno2 !~ "''") && ($epl_aend_jno2 !~ m/^\'MA/gi)) {
                #A-end job note validation
                $error = "A-end job note must begin with MA for Point to Multipoint services.";
                return ;
                }
                elsif (($epl_zend_jno2 !~ "''") && ($epl_zend_jno2 !~ m/^(\'EZ|\'MZ)/gi)) {
                #Z-end job note validation
                $error = "Z-end job note must begin with either EZ or MZ.";
                return ;
                }
                #Assigning EPL job ids,notes to the GBS job ids, notes
        $aend_jobid = $epl_aend_jid2;
        $zend_jobid = $epl_zend_jid2;
        #convert to lower case before entering into the DB
        $aend_jobno = lc($epl_aend_jno2);
        $zend_jobno = lc($epl_zend_jno2);
  }
        else {
                $billable = $epl_billable2;
                $epl_msid = "''";
                if (($epl_aend_jno2 eq "''") && ($epl_zend_jno2 eq "''")){
                #Job note cannot be blank
                $error = "Please enter either A-end or Z-end job note.";
                return ;
                } elsif (($epl_zend_jno2 !~ "''") && ($epl_zend_jno2 !~ m/^(\'EZ|\'MZ)/gi)) {
                #Z-end job note validation
                $error = "Z-end job note must begin with either EZ or MZ.";
                return ;
                } elsif (($epl_aend_jno2 !~ "''") && ($epl_aend_jno2 !~ m/^(\'EA|\'MA)/gi)) {
                #A-end job note validation
                $error = "A-end job note must begin with either EA or MA.";
                return ;
               }
                #Assigning EPL job ids,notes to the GBS job ids, notes
        $aend_jobid = $epl_aend_jid2;
        $zend_jobid = $epl_zend_jid2;
        #convert to lower case before entering into the DB
        $aend_jobno = lc($epl_aend_jno2);
        $zend_jobno = lc($epl_zend_jno2);
 }
        }
		 elsif ($temp_serviceid =~ m/em/gi) {
                #For point to multipoint services
                $billable = $epl_billable2;
                #Master Service ID validation
                if ($epl_msid =~ "'-- Select --'") {
                $error = "Please select a Master Service ID for multipoint EPL service.";
                return ;
                }
                if ($epl_aend_jno2 eq "''"){
                #A-end job note validation
                $error = "A-end job note cannot be blank for multipoint EPL service.";
                return ;
                } elsif ($epl_aend_jno2 !~ m/^\'MA/i) {
                #$error = "Fields mismatch. Service ID indicates Point to Multipoint Service type, A-end job note must begin with MA.";
                $error = "A-end job note must begin with MA and should be of the format MA_SNG_6NTP_EM_1234";
                return ;
                } elsif (($epl_zend_jno2 !~ "''") && ($epl_zend_jno2 !~ m/^(\'EZ|\'MZ)/gi)) {
                #Z-end job note validation
                $error = "Z-end job note must begin with either EZ or MZ and should be of the format EZ_SNG_6NTP_EM_12345 or MZ_SNG_6NTP_EM_12345.";
                return ;
                } #end of point to multipoint job note validations
        #Assigning EPL job ids,notes to the GBS job ids, notes
        $aend_jobid = $epl_aend_jid2;
        $zend_jobid = $epl_zend_jid2;
	#convert to lower case before entering into the DB
        $aend_jobno = lc($epl_aend_jno2);
        $zend_jobno = lc($epl_zend_jno2);
        #print EPL1 "Before EM update:$epl_aend_jid2\tepl_aend_jno2:$epl_aend_jno2\tepl_zend_jno2:$epl_zend_jno2\taend_jobno:$aend_jobno\tzend_jobno:$zend_jobno\n";

        } elsif ($temp_serviceid =~ m/ew/gi) {
                #For point to point services
                $billable = $epl_billable;
                $epl_msid = "''";
                if (($epl_aend_jno eq "''") && ($epl_zend_jno eq "''")){
                #Job note cannot be blank
                $error = "Please enter either A-end or Z-end job note.";
                return ;
                } elsif (($epl_zend_jno !~ "''") && ($epl_zend_jno !~ m/^(\'EZ|\'MZ)/gi)) {
                #Z-end job note validation
                $error = "Z-end job note must begin with either EZ or MZ and should be of the format EZ_SNG_6NTP_EW_12345 or EZ_SNG_6NTP_EM_12345.";
                return ;
                } elsif (($epl_aend_jno !~ "''") && ($epl_aend_jno !~ m/^(\'EA|\'MA)/gi)) {
                #A-end job note validation
                $error = "A-end job note must begin with either EA or MA and should be of the format EA_SNG_6NTP_EW_12345 or EA_SNG_6NTP_EM_12345.";
                return ;
                } #end of point to point job note validations
        #Assigning EPL job ids,notes to the GBS job ids, notes
        $aend_jobid = $epl_aend_jid;
        $zend_jobid = $epl_zend_jid;
	#convert to lower case before entering into the DB
        $aend_jobno = lc($epl_aend_jno);
        $zend_jobno = lc($epl_zend_jno);
        } #end of service id checks
#end of EPL product code check
  }else{
        my $sth = $dbh_tr->prepare ("SELECT serviceid FROM service WHERE serviceid ilike $serviceid");
        #$sth = $dbh_tr->prepare ("SELECT serviceid FROM service WHERE serviceid ilike $serviceid");
        $sth->execute();
        my $r = $sth->fetchrow_hashref;
        my $temp_serviceid = $serviceid;
        $temp_serviceid =~ s/\'//g;
        if($$r{serviceid} =~ /^$temp_serviceid$/i) {
       		 return "Service ID already exists. Please enter a different Service ID.";
        }
  }

  # check for whether the accno is exist or not
  #$sth = $dbh_tr->prepare ("SELECT accno FROM customer WHERE accno ilike $accno");
  my $sth = $dbh_tr->prepare ("SELECT accno FROM customer WHERE accno like $accno");
  $sth->execute();
  my $r = $sth->fetchrow_hashref;
  CMS_DB::connect_to_database();
  $sth = $dbh->prepare ("alter table outagecustomercomments disable triggers");
  $sth->execute();
	
  my $temp_accno = $accno;
  $temp_accno =~ s/\'//g;
  if($$r{accno} =~ /^$temp_accno$/) {
#       return 0;
  } else {
        return "Customer does not exist.";
  }


  # check auto_gen_prefix value - Modified by Stavan on Aug16_2010
  if ($productcode =~ /GIA/) {
	my $temp_serviceid = $serviceid;
	$auto_gen_prefix = $auto_gen_prefix;
	if($black_hole_routing eq "\'Yes\'"){
        my $sth_bha = $dbh_tr->prepare ("SELECT bha_auto_gen_prefix FROM service WHERE as_no = $as_no AND black_hole_routing ilike 'Yes'");
        $sth_bha->execute;
        my $r_bha = $sth_bha->fetchrow_hashref;
        if ($sth_bha->rows > 0) {
                $bha_auto_gen_prefix = $dbh_tr->quote($$r_bha{bha_auto_gen_prefix});
        }
  }
  else{
        $bha_auto_gen_prefix="\'Disable\'";
        #$bha_auto_gen_prefix = $dbh_tr->quote($bha_auto_gen_prefix);
  }
  }
  else {
  if ($productcode =~ /IPTRANSIT/) {
  $sth = $dbh_tr->prepare ("SELECT auto_gen_prefix FROM service WHERE as_no = $as_no");
  $sth->execute;
  $r = $sth->fetchrow_hashref;
  if ($sth->rows > 0) {
     $auto_gen_prefix = $dbh_tr->quote($$r{auto_gen_prefix});
  }

  # Modifying the Auto-Gen Prefix for BHA Routers - Stavan Feb14_2011 
  ##++Modified for Addendum 2
  if($black_hole_routing eq "\'Yes\'"){
	my $sth_bha = $dbh_tr->prepare ("SELECT bha_auto_gen_prefix FROM service WHERE as_no = $as_no AND black_hole_routing ilike 'Yes'");
	$sth_bha->execute;
	my $r_bha = $sth_bha->fetchrow_hashref;
	if ($sth_bha->rows > 0) {
		$bha_auto_gen_prefix = $dbh_tr->quote($$r_bha{bha_auto_gen_prefix});
	}
  }
  else{
	$bha_auto_gen_prefix="\'Disable\'";
	#$bha_auto_gen_prefix = $dbh_tr->quote($bha_auto_gen_prefix);
  }
  }
  }
  ##--Addendum 2

  my $billstartdt = 'null';
  if ($billstartdate_year !~ /-/) {
        $billstartdt =
                $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $billstartdate_year, $billstartdate_month, $billstartdate_day,
                        $billstarttime_hour, $billstarttime_minute, $billstarttime_second));
  }
  my $billenddt = 'null';
  if ($billenddate_year !~ /-/) {
        $billenddt =
                $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $billenddate_year, $billenddate_month, $billenddate_day,
                        $billendtime_hour, $billendtime_minute, $billendtime_second));
  }
  if ($productcode =~ /MPLS/) {
     if ($billable == 2) {
         $billenddt = 'null';
     }
#Added by Nisha for mVPN to add additional columns for multicast 
$temp_sql = "INSERT INTO service(serviceid, accno, description, currentbw, productcodeid, switchsourcodeid, switchdestcodeid, pollingtype, costype, gold_classmap, gold_rateacl, silver_classmap, silver_rateacl, default_classmap, default_rateacl, irrd_object,aggregate_serviceid, billing_optionid, billing_starttime, billing_endtime, serviceid_alt, class1_classmap, class1_rateacl, class2_classmap, class2_rateacl, class4_classmap, class4_rateacl, mcast_class5_in, mcast_class4_in, mcast_class4_out, mcast_class3_in, mcast_class3_out, mcast_class2_in, mcast_class2_out, mcast_class1_in, mcast_class1_out, mcast_class0_in, mcast_class0_out, mcast_option)
	VALUES($serviceid, $accno, $description, $currentbw, $productcode, $switchsourcode, $switchdestcode, $pollingtype, $costype, $gold_classmap, $gold_rateacl, $silver_classmap, $silver_rateacl, $default_classmap, $default_rateacl, $irrd_object, $aggregate_serviceid, $billable, $billstartdt, $billenddt, $serviceid_alt, $class1_classmap, $class1_rateacl, $class2_classmap,$class2_rateacl, $class4_classmap, $class4_rateacl, $voice_mvpn_in, $video_mvpn_in, $video_mvpn_out, $criticaldata_mvpn_in, $criticaldata_mvpn_out, $interactivedata_mvpn_in, $interactivedata_mvpn_out, $standarddata_mvpn_in, $standarddata_mvpn_out, $lowpriority_mvpn_in, $lowpriority_mvpn_out, $mvpnCheck)";

#End of code Added by Nisha for mVPN

  } elsif ($productcode =~ /VPLS/) {
	open (IPT2, ">/data1/tmp_log/edit.log");
     if ($billable == 2) {
         $billenddt = 'null';
     }
     $costype = "reachvpls";
     $costype = "\'$costype\'";
$temp_sql =  "INSERT INTO service (serviceid, accno, description, currentbw, productcodeid, pollingtype, costype, gold_classmap, gold_rateacl, silver_classmap, silver_rateacl, default_classmap, default_rateacl, irrd_object,aggregate_serviceid, master_serviceid, billing_optionid, billing_starttime, billing_endtime, serviceid_alt, class1_classmap, class1_rateacl, class2_classmap, class2_rateacl, class4_classmap, class4_rateacl)
 VALUES($serviceid, $accno, $description, $currentbw, $productcode, $portmode, $costype, $gold_classmap, $gold_rateacl, $silver_classmap, $silver_rateacl, $default_classmap, $default_rateacl, $irrd_object, $aggregate_serviceid, $master_serviceid, $billable, $billstartdt, $billenddt, $serviceid_alt, $class1_classmap, $class1_rateacl, $class2_classmap,$class2_rateacl, $class4_classmap, $class4_rateacl)";
	print IPT2 "DB billable:$billable\n DB VPLS: temp_sql:$temp_sql\n";

#Added by karuna for EPL
  } elsif ($productcode =~ /EPL/){
#Added new telstra id condition-TGC0031549
	if ($epl_msid =~ "'-- Select --'") {
                        $epl_msid = "''";
                }

        if ($billable == 2) {
        $billenddt = 'null';
        }
        #print EPL1 "billable:$billable\tbillstartd:$billstartdt\tbillenddt:$billenddt\tcurrentbw:$currentbw\n";
        #print EPL1 "INSERT INTO service(serviceid, accno, description, currentbw, productcodeid, master_serviceid, aend_jobid,zend_jobid, aend_jobno, zend_jobno, billing_optionid, billing_starttime, billing_endtime) VALUES ($serviceid, $accno,$description, $currentbw, $productcode, $epl_msid, $epl_aend_jid, $epl_zend_jid, $epl_aend_jno, $epl_zend_jno, $billable, $billstartdt,$billenddt)";
        $temp_sql = "INSERT INTO service(serviceid, accno, description, currentbw, productcodeid, master_serviceid, aend_jobid,zend_jobid, aend_jobno, zend_jobno, billing_optionid, billing_starttime, billing_endtime) VALUES ($serviceid, $accno, $description, $currentbw, $productcode, $epl_msid, $aend_jobid, $zend_jobid, $aend_jobno, $zend_jobno, $billable, $billstartdt, $billenddt)";

} 
elsif ($productcode =~ /IPTRANSIT/){
	open (IPT, ">>/data1/tmp_log/addservice.log");
	print IPT "IP TRANSIT prod code: $productcode\n";
	print IPT "as_no:$as_no\tauto_gen_prefix:$auto_gen_prefix\tbgp_neighbor_ip:$bgp_neighbor_ip\tirrd_object:$irrd_object\n";
	print IPT "New Values= $black_hole_routing, $bha_bgp_neighbor_ip,$bha_auto_gen_prefix\n";
	$billable = '2'; 
	$billstartdt = 'null';
	$billenddt = 'null';
	$temp_sql =  "INSERT INTO service(serviceid, accno, description, currentbw, productcodeid, as_no, auto_gen_prefix, bgp_neighbor_ip, irrd_object, billing_optionid, billing_starttime, billing_endtime,  master_serviceid, black_hole_routing, bha_bgp_neighbor_ip, bha_auto_gen_prefix) VALUES ($serviceid, $accno, $description, $currentbw, $productcode, $as_no, $auto_gen_prefix, $bgp_neighbor_ip, $irrd_object, $billable, $billstartdt, $billenddt, $ipt_msid, $black_hole_routing, $bha_bgp_neighbor_ip,$bha_auto_gen_prefix)";
	 print IPT "temp_sql: $temp_sql\n"; 	
  } elsif ($productcode =~ /EVPL/){
     my $temp_serviceid =$serviceid;  
     $temp_serviceid =~ s/\'//g;
     my $serviceid_a=$temp_serviceid."a";
     my $serviceid_z=$temp_serviceid."z";
     if($portmode_e =~ /EVPL VLAN Service/){
#Chandini(changed from class_5 (gold) to class_3 (silver) in two insert statements )

        $portmode_e = "'"."VPM"."'";
$temp_sql =  "INSERT INTO service (serviceid, accno,description, currentbw, productcodeid, pollingtype, costype, silver_classmap, silver_rateacl,default_classmap, default_rateacl,aggregate_serviceid,billing_optionid, billing_starttime, billing_endtime)
        VALUES('$serviceid_a',$accno,$description, $currentbw, $productcode, $portmode_e, $costype_e, $gold_classmap, $gold_rateacl,$default_classmap, $default_rateacl,$aggregate_serviceid_a, $billable, $billstartdt, $billenddt)";
 $temp_sql_z =  "INSERT INTO service (serviceid,accno,description, currentbw, productcodeid, pollingtype, costype, silver_classmap, silver_rateacl, default_classmap, default_rateacl,aggregate_serviceid,billing_optionid, billing_starttime, billing_endtime) 
        VALUES('$serviceid_z',$accno,$description, $currentbw, $productcode, $portmode_e, $costype_e, $gold_classmap, $gold_rateacl,$default_classmap, $default_rateacl, $aggregate_serviceid_z, $billable, $billstartdt, $billenddt)";
     } else {
        $portmode_e = "'"."TPM"."'";

        my $aggregate_serviceid_pre = $agg_servid_tpm_raw;
        $aggregate_serviceid_pre =~ s/sw$/asw/;
        my $aggregate_serviceid_a = $aggregate_serviceid_pre;

        $agg_servid_tpm_raw =~ s/sw$/zsw/;
        my $aggregate_serviceid_z =$agg_servid_tpm_raw;


 $temp_sql = "INSERT INTO service (serviceid,accno,description, currentbw, productcodeid, pollingtype, costype, gold_classmap, gold_rateacl, default_classmap, default_rateacl,aggregate_serviceid,billing_optionid, billing_starttime, billing_endtime)
	VALUES('$serviceid_a',$accno,$description, $currentbw, $productcode, $portmode_e, $costype_e, $gold_classmap, $gold_rateacl,$default_classmap, $default_rateacl, '$aggregate_serviceid_a', $billable, $billstartdt, $billenddt)";
 $temp_sql_z = "INSERT INTO service (serviceid,accno,description, currentbw, productcodeid, pollingtype, costype, gold_classmap, gold_rateacl, default_classmap, default_rateacl,aggregate_serviceid,billing_optionid, billing_starttime, billing_endtime)
	VALUES('$serviceid_z',$accno,$description, $currentbw, $productcode, $portmode_e, $costype_e, $gold_classmap, $gold_rateacl,$default_classmap, $default_rateacl, '$aggregate_serviceid_z', $billable, $billstartdt, $billenddt)";
     }
  } elsif ($productcode =~ /ETHERNET/) {
# Added by Stavan for VLA on 18Aug2010
     $billable = 2;
     $billenddt = 'null';
     $billstartdt = 'null';
     $portmode = "ETH-TRANS";
     $portmode = "\'$portmode\'";
     $temp_sql =  "INSERT INTO service (serviceid, accno, description, currentbw, productcodeid, pollingtype, billing_optionid, billing_starttime, billing_endtime) VALUES($serviceid, $accno, $description, $currentbw, $productcode, $portmode, $billable, $billstartdt, $billenddt)";

#Added for IP Transit Ph 4a - AS Number validation
  } elsif ($productcode =~ /GIA/){
	#my $sth_asno = $dbh_tr->prepare ("select as_nos from ipc_china where as_nos like $as_no and dir_traffic like 'direct'");
	my $sth_asno = $dbh_tr->prepare ("select DISTINCT ON(dir_effective_date) as_nos,dir_traffic from ipc_china where as_nos like $as_no order by dir_effective_date DESC");
        $sth_asno->execute();
        #my $row2 = $sth_asno->rows();
	my $result = $sth_asno->fetchall_arrayref();
 	print IPT_DBG "ASN validation:$result->[0][1]\n";	
	if (($result->[0][1]) eq "direct"){
		return "AS Number $as_no already in use by an IPC service(Direct China Traffic).";
	}

        #if ($row2 > 0) {
                #return "AS Number $as_no already in use by an IPC service(Direct China Traffic).";
        #}
	#open (IPT1, ">>/data1/tmp_log/addservice.log");
	#print IPT1 "DB GIA INSERT INTO service(serviceid, accno, description, currentbw, productcodeid,as_no, auto_gen_prefix, bgp_neighbor_ip, irrd_object, aggregate_serviceid, billing_optionid, billing_starttime, billing_endtime) VALUES ($serviceid, $accno, $description, $currentbw, $productcode,$as_no, $auto_gen_prefix, $bgp_neighbor_ip, $irrd_object, $aggregate_serviceid, $billable, $billstartdt, $billenddt)";
	print IPT1 "DB GIA INSERT INTO service(serviceid, accno, description, currentbw, productcodeid,as_no, auto_gen_prefix, bgp_neighbor_ip, irrd_object, aggregate_serviceid, billing_optionid, billing_starttime, billing_endtime, black_hole_routing, bha_bgp_neighbor_ip, bha_auto_gen_prefix) VALUES ($serviceid, $accno, $description, $currentbw, $productcode,$as_no, $auto_gen_prefix, $bgp_neighbor_ip, $irrd_object, $aggregate_serviceid, $billable, $billstartdt, $billenddt,$black_hole_routing, $bha_bgp_neighbor_ip,$bha_auto_gen_prefix)";
	$temp_sql =  "INSERT INTO service(serviceid, accno, description, currentbw, productcodeid,as_no, auto_gen_prefix, bgp_neighbor_ip, irrd_object, aggregate_serviceid, billing_optionid, billing_starttime, billing_endtime, black_hole_routing, bha_bgp_neighbor_ip, bha_auto_gen_prefix) VALUES ($serviceid, $accno, $description, $currentbw, $productcode,$as_no, $auto_gen_prefix, $bgp_neighbor_ip, $irrd_object, $aggregate_serviceid, $billable, $billstartdt, $billenddt,$black_hole_routing, $bha_bgp_neighbor_ip,$bha_auto_gen_prefix)";
} else {
	open (IPT, ">>/data1/tmp_log/addservice.log");
	print IPT "ipt_msid: $ipt_msid\n";
	##Added by Karuna on 09Dec2010 for regression bug fix
	##++Start
	if($ipt_msid eq ""){
		$ipt_msid = "NULL";
	}
	##--end

	$temp_sql =  "INSERT INTO service(serviceid, accno, description, currentbw, productcodeid, switchsourcodeid, switchdestcodeid, as_no, auto_gen_prefix, bgp_neighbor_ip, irrd_object, aggregate_serviceid, billing_optionid, billing_starttime, billing_endtime, serviceid_alt,  aend_jobid, zend_jobid, aend_jobno, zend_jobno, aend_country, zend_country, master_serviceid)
	VALUES($serviceid, $accno, $description, $currentbw, $productcode, $switchsourcode, $switchdestcode, $as_no, $auto_gen_prefix, $bgp_neighbor_ip, $irrd_object, $aggregate_serviceid, $billable, $billstartdt, $billenddt, $serviceid_alt, $aend_jobid, $zend_jobid, $aend_jobno, $zend_jobno, $aend_country, $zend_country, $ipt_msid)";
  }
#print IPT "temp_sql: $temp_sql\n";
 	open (IPTe, ">>/data1/tmp_log/ethernet.txt");
        my $success = 1;
        my $success1 = 1;
	if ($productcode =~ /EVPL/){
		my $sth = $dbh_tr->prepare($temp_sql);
        	$success &&= $sth->execute;
                my $sth1 = $dbh_tr->prepare($temp_sql_z);
                $success1 &&= $sth1->execute; 
	} else {
	 	#print IPTe "Inside 459\n $temp_sql\n\n";	
		my $sth = $dbh_tr->prepare($temp_sql);
                $success &&= $sth->execute;
        }
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
	print IPTe "LIne 466 :$temp_sql \nLine 466 : $error\nSTH :$sth\nSuccess : $success";
	close IPTe;
        my $result = (($success && $success1) ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
	
        CMS_DB::disconnect_from_transaction();
        return $error if ($error);
        return $result;
}

sub get_service {
  my $serviceid = $dbh->quote(shift @_);
  my $accno = $dbh->quote(shift @_);

#Added additional columns by Nisha for mVPN on 25Sep2009
  my $sth = $dbh->prepare
    ("SELECT description, currentbw, productcodeid,
	switchsourcodeid, switchdestcodeid, pollingtype, costype, pollingtype, gold_classmap, gold_rateacl, silver_classmap, silver_rateacl, default_classmap, default_rateacl, class1_classmap, class1_rateacl, class2_classmap, class2_rateacl, class4_classmap, class4_rateacl, as_no, auto_gen_prefix, bgp_neighbor_ip, irrd_object, aggregate_serviceid, master_serviceid, billing_optionid, billing_starttime, billing_endtime, serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno, aend_country, zend_country, mcast_class5_in, mcast_class4_in, mcast_class4_out, mcast_class3_in, mcast_class3_out, mcast_class2_in, mcast_class2_out, mcast_class1_in, mcast_class1_out, mcast_class0_in, mcast_class0_out, mcast_option,black_hole_routing,bha_bgp_neighbor_ip,bha_auto_gen_prefix 
	FROM service
	WHERE serviceid ilike $serviceid
	AND accno = $accno");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchrow_arrayref;

  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub get_switchcode {
	#my $asname = shift @_;
	#if ($asname eq '') {
	#	$asname = 'switchcodeid';
	#}
	my $sth = $dbh->prepare("SELECT switchcodeid FROM switchcode order by switchcodeid");

	unless ($sth->execute) {
    		$error = $dbh->errstr;
    		return 0;
  	}

  	my $result = $sth->fetchall_arrayref;

  	if ($result) {
    		$error = $dbh->errstr;
    		$sth->finish;
    		return $result;
  	} else {
    		$error = $dbh->errstr;
    		return 0;
  	}
}

sub get_productcode {
	my $sth = $dbh->prepare("SELECT productcodeid FROM productcode order by productcodeid");

	unless ($sth->execute) {
    		$error = $dbh->errstr;
    		return 0;
  	}

  	my $result = $sth->fetchall_arrayref;

  	if ($result) {
    		$error = $dbh->errstr;
    		$sth->finish;
    		return $result;
  	} else {
    		$error = $dbh->errstr;
    		return 0;
  	}
}

sub get_portmode {
     #   my $sth = $dbh->prepare("SELECT port_mode FROM portmode");
	  my $sth = $dbh->prepare("SELECT ref_val FROM reference where productcode = 'VPLS' AND ref_type ='portmode'"); 

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}



sub get_portmode_evpl{
	my $sth = $dbh->prepare("SELECT ref_val FROM reference where productcode = 'EVPL' AND ref_type ='portmode'");

	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_costype_evpl{
	my $sth = $dbh->prepare("SELECT ref_val FROM reference where productcode = 'EVPL' AND ref_type ='costype' AND description ilike 'EVPL CoS type'");

	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_master_service{
	my $sth = "";
	my $prod = shift(@_);
	if ($prod =~ /IPTRANSIT/){
		$sth = $dbh->prepare("SELECT master_serviceid FROM master_service where master_serviceid ilike '%ipm%' ORDER BY master_serviceid");
	} else {
		$sth = $dbh->prepare("SELECT master_serviceid FROM master_service ORDER BY master_serviceid");
	}

	 unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#Added by Nisha on 29Sep10 for IPTRANSIT
sub get_asno_ipt{
	my $msid = $dbh->quote(shift(@_));
	my $sth = $dbh->prepare("SELECT as_nos FROM ipc_china LEFT OUTER JOIN ipt_master on ipt_master.ipc_servid = ipc_china.ipc_servid WHERE ipt_master.master_sid=$msid");

unless ($sth->execute) {
	$error = $dbh->errstr;
	return 0;
}

my $result = $sth->fetchrow_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $$result[0];
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_accno_ipt{
	my $msid = $dbh->quote(shift(@_));
        my $sth = $dbh->prepare("SELECT as_nos,customer from ipc_china LEFT OUTER JOIN ipt_master on ipt_master.ipc_servid = ipc_china.ipc_servid WHERE ipt_master.master_sid=$msid");

	unless ($sth->execute) {
        $error = $dbh->errstr;
        return 0;
	}
	my @retStr = $sth->fetchrow_array;
	my $retStr = join ',',@retStr;
	$sth->finish;
        return $retStr;
}


##########################################################
#Author: Karuna Ballal
#Description: fetches all Master Services for EPL
#Input: 
#Output: Result of select query in array reference $result
#Added masterepl condition for -TGC0031549
##########################################################
sub get_epl_master_service{
         my $sth = $dbh->prepare("SELECT master_serviceid FROM master_service where master_serviceid ilike '%emm%'or master_serviceid ilike 'masterepl%'");

         unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

##########################################################
#Author: Karuna Ballal
#Description: fetches all Master Services for IPT 
#Input: 
#Output: Result of select query in array reference $result
##########################################################
sub get_ipt_master_service{
	#store the product code 
	my $prod = $_;
         my $sth = $dbh->prepare("SELECT master_serviceid FROM master_service where master_serviceid ilike '%ipm%'");

         unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_productcode_for_all_service {
        my $sth = $dbh->prepare("SELECT serviceid, productcodeid, slareportcodeid
				 FROM service, customer
				 where service.accno=customer.accno");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_productcode_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT productcodeid FROM service
				WHERE serviceid = $sid");
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

	my $result = $sth->fetchrow_hashref;
	$error = $dbh->errstr;
	$sth->finish;
	return $result;
}

#IPT_Add_2
sub get_irrd_obj_serviceid {
	my $sid = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT irrd_object FROM service
                                WHERE serviceid = $sid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        $error = $dbh->errstr;
        $sth->finish;
        return $result;
#	return $$result{irrd_object};
}

sub get_serviceclass {
	my $sth = $dbh->prepare("SELECT serviceclassid FROM serviceclass");

	unless ($sth->execute) {
    		$error = $dbh->errstr;
    		return 0;
  	}

  	my $result = $sth->fetchall_arrayref;

  	if ($result) {
    		$error = $dbh->errstr;
    		$sth->finish;
    		return $result;
  	} else {
    		$error = $dbh->errstr;
    		return 0;
  	}
}

sub get_label_for_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT svclabel FROM service
				WHERE serviceid = $sid");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_reseller_label_for_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT reseller_svclabel as svclabel FROM service
				WHERE serviceid = $sid");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_auto_gen_prefix {
	#Karuna on 15th Apr 2010 - added "distinct" keyword to prevent duplication
	my $sth = $dbh->prepare("SELECT distinct option FROM gen_prefix_option");
  
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#Added for IP Transit Phase 5 Kamal

sub get_bha_auto_gen_prefix {
        my $sth = $dbh->prepare("SELECT distinct option FROM gen_prefix_option");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_black_hole_routing_option {
        my $sth = $dbh->prepare("SELECT distinct option FROM black_hole_routing_option");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}



#---------Phase 5 end-----------

#
# Added - for QOS polling
# rchew@maettr.bpa.nu, 13 August 2006
#
sub get_pollingtype {
	## XX to be completed
	my $sth = $dbh->prepare("SELECT pollingtype FROM pollingtype");
  
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }

}

#
# Added for Telstra/PCCW labels
# rchew@maettr.bpa.nu, 25 October 2006
#
sub get_costype {
        my $sth = $dbh->prepare("SELECT costype FROM costype");
  
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
  
}

sub get_gen_prefix_by_service {
	my $sid = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT auto_gen_prefix FROM service where serviceid = $sid");
 
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
 
        my $result = $sth->fetchall_arrayref;
 
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#*************************************************************************************************
#Name			:get_all_gen_prefix_by_service 
#Description		:get prefix option for all services, including last deleted of an AS num
#Author			:KARUNA BALLAL for IPT5
#input			:service id
#output			:auto_gen_prefix
#*************************************************************************************************
sub get_all_gen_prefix_by_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT auto_gen_prefix FROM service where serviceid = $sid
				UNION
				SELECT auto_gen_prefix FROM deleted_links_routing where serviceid = $sid and status = '2'");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#*************************************************************************************************
#Name                   :get_all_gen_prefix_by_service_bha
#Description            :gen BHA gen prefix option for all services, including last deleted of an AS num
#Author                 :KARUNA BALLAL for IPT5
#input                  :service id
#output                 :bha_auto_gen_prefix
#*************************************************************************************************
#Addendum_2 #Chandini

sub get_all_gen_prefix_by_service_bha {
	my $sid = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT bha_auto_gen_prefix, black_hole_routing FROM service where serviceid = $sid
                                UNION
                                SELECT bha_auto_gen_prefix, black_hole_routing FROM deleted_links_routing where serviceid = $sid and (bha_status='2' or bha_status_bac = '2')");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#Chandini_IPT_ph5
sub get_gen_prefix_by_service_bha {
        my $sid = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT bha_auto_gen_prefix FROM service where serviceid = $sid");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}
#Chandini_IPT_ph5_03Mar
sub get_srv_exists_srv_tbl {
        my $sid = $dbh->quote(shift @_);
        my $as_no = $dbh->quote(shift @_);
if ($debug){print IPT_DBG "sid:$sid, as_no:$as_no\n";}

	my $sth = $dbh->prepare("SELECT serviceid from service where as_no = $as_no AND serviceid like $sid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
if ($debug){print IPT_DBG "sth : $sth\n";}
if ($debug){print IPT_DBG "Entered 1\n";}
                return 0;
        }
	my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
if ($debug){print IPT_DBG "Entered 2\n";}
                return $result;
        } else {
                $error = $dbh->errstr;
if ($debug){print IPT_DBG "Entered 3\n";}
                return 0;
        }
}


sub get_aggregate_service {
        my $sth = $dbh->prepare("SELECT aggregate_serviceid from aggregate_service where aggregate_serviceid NOT LIKE  '%ec%sw' and aggregate_serviceid NOT LIKE '%evp%sw' order by aggregate_serviceid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


sub get_aggregate_service_evpl_tpm {
        my $sth = $dbh->prepare("select trim(trailing 'asw' from aggregate_serviceid)||'sw' as SID FROM aggregate_service where aggregate_serviceid like '%asw'");
unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}
sub get_aggregate_service_evpl_vpm {
        my $sth = $dbh->prepare("select aggregate_serviceid as SID from aggregate_service where aggregate_serviceid ~* '.*evp[0-9]{5}sw' order by SID");

	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


sub get_service_by_as {
        my $as = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT serviceid from service where as_no = $as");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#***************************************************************
#Name		:get_all_service_by_as
#Author		:KARUNA BALLAL
#Description	:get deleted service by its AS num
#Input		:AS number
#output		:service
#***************************************************************
sub get_all_service_by_as {
	my $as = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT serviceid from deleted_links_routing where as_no = $as AND status = '2'");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $rows = $sth->rows();
	if ($rows > 0){
		$sth = $dbh->prepare("SELECT serviceid from deleted_links_routing where as_no = $as AND status = '2'
					UNION
					SELECT serviceid from service where as_no = $as");
	} else {
		$sth = $dbh->prepare("SELECT serviceid from service where as_no = $as");
	}
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#Addendum_2 ##Chandini
sub get_all_service_by_as_bha {
        my $as = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT serviceid from deleted_links_routing where as_no = $as AND (bha_status = '2' or bha_status_bac = '2')");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $rows = $sth->rows();
        if ($rows > 0){
                $sth = $dbh->prepare("SELECT serviceid from deleted_links_routing where as_no = $as AND (bha_status = '2' or bha_status_bac = '2')
                                        UNION
                                        SELECT serviceid from service where as_no = $as");
        } else {
                $sth = $dbh->prepare("SELECT serviceid from service where as_no = $as");
        }
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#***************************************************************
#Name           :get_deleted_service
#Author         :KARUNA BALLAL
#Description    :get deleted service 
#Input          :
#output         :service
#***************************************************************
#Addendum_2 #Chandini
sub get_deleted_service {
	#my $sth = $dbh->prepare("select serviceid,routername,bha_routername from deleted_links_routing where status = 0");
	#Changed on 07mar2011
	my $sth = $dbh->prepare("select serviceid,routername,bha_routername,as_no,bha_routername_bac from deleted_links_routing where status = 0");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
	my $result = $sth->fetchall_arrayref;
	if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#Addendum_2 ##Chandini
sub get_deleted_service_bha {
        my $sth = $dbh->prepare("select serviceid,routername,bha_routername,bha_routername_bac from deleted_links_routing where bha_status = 0 OR bha_status_bac = 0");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}





sub get_service_by_bgp_ip {
        my $bgp_ip = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT serviceid from service where bgp_neighbor_ip = $bgp_ip");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

##Karuna on 22-Nov-2010 for IPT5
##++IPT5
sub get_bha_service_by_bgp_ip {
        my $bha_bgp_ip = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT serviceid from service where bha_bgp_neighbor_ip = $bha_bgp_ip");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}
##--IPT5


sub set_label_for_serviceid {
##++ EVPL - SD
	my $serviceid = shift @_;
	open (DEBUG_SD, ">/tmp/set_label_for_serviceid.txt");
##-- EVPL - SD
	my $label = $dbh->quote(shift @_);
##++ EVPL - SD
	my $productcode = $dbh->quote(shift @_);
    my $serviceid_z;
    my $serviceid_e;
    if($productcode =~ /EVPL/){
            $serviceid_z =  $dbh->quote($serviceid."z");
    		$serviceid_e =  $dbh->quote($serviceid."a");  
    }else {
            $serviceid_e =  $dbh->quote($serviceid);
    }
	
	my $sth1 = $dbh->prepare("UPDATE service SET svclabel = $label WHERE serviceid ilike $serviceid_z");
    my $result1 = $sth1->execute; 
    $error = $dbh->errstr;
  	print DEBUG_SD "UPDATE service SET svclabel = $label WHERE serviceid ilike $serviceid_z.\n"; 
  	
	my $sth = $dbh->prepare("UPDATE service SET svclabel = $label WHERE serviceid ilike $serviceid_e");
	print DEBUG_SD "Product Code = $productcode.\n";
	print DEBUG_SD "UPDATE service SET svclabel = $label WHERE serviceid ilike $serviceid_e.\n";  	
  	
	close DEBUG_SD;
##-- EVPL - SD

	my $result = $sth->execute;
	$error = $dbh->errstr;
	return $result;

}

sub set_label_for_serviceid_aggrmast {
        my $serviceid = $dbh->quote(shift @_);
        my $label = $dbh->quote(shift @_);
	my $master;

        my $sth = $dbh->prepare("UPDATE aggregate_service SET svclabel = $label WHERE aggregate_serviceid ilike $serviceid");
        my $result = $sth->execute;

	if ($result != 1) {
	     my $sth = $dbh->prepare("UPDATE master_service SET svclabel = $label WHERE master_serviceid ilike $serviceid");
	     my $result = $sth->execute;
	     $master = "master";
	}
        $error = $dbh->errstr;
	return ($result,$master);

}

sub set_reseller_label_for_serviceid {
##++ EVPL - SD
	my $serviceid = shift @_;
	open (DEBUG_SD, ">/tmp/set_label_for_serviceid.txt");
##-- EVPL - SD
	my $label = $dbh->quote(shift @_);
##++ EVPL - SD
	my $productcode = $dbh->quote(shift @_);
    my $serviceid_z;
    my $serviceid_e;
    if($productcode =~ /EVPL/){
            $serviceid_z =  $dbh->quote($serviceid."z");
    		$serviceid_e =  $dbh->quote($serviceid."a");  
    }else {
            $serviceid_e =  $dbh->quote($serviceid);
    }
	
	my $sth1 = $dbh->prepare("UPDATE service SET reseller_svclabel = $label WHERE serviceid ilike $serviceid_z");
    my $result1 = $sth1->execute; 
    $error = $dbh->errstr;
  	print DEBUG_SD "UPDATE service SET reseller_svclabel = $label WHERE serviceid ilike $serviceid_z.\n"; 
  	
	my $sth = $dbh->prepare("UPDATE service SET reseller_svclabel = $label WHERE serviceid ilike $serviceid_e");
	print DEBUG_SD "Product Code = $productcode.\n";
	print DEBUG_SD "UPDATE service SET reseller_svclabel = $label WHERE serviceid ilike $serviceid_e.\n";  	
  	
	close DEBUG_SD;
##-- EVPL - SD
	
	my $result = $sth->execute;
	$error = $dbh->errstr;
	return $result;
}

sub set_reseller_label_for_serviceid_aggrmast {
        my $serviceid = $dbh->quote(shift @_);
        my $label = $dbh->quote(shift @_);
	my $master;

        my $sth = $dbh->prepare("UPDATE aggregate_service SET reseller_svclabel = $label WHERE aggregate_serviceid ilike $serviceid");
        my $result = $sth->execute;

	if ($result != 1) {
	    my $sth = $dbh->prepare("UPDATE master_service SET reseller_svclabel = $label WHERE master_serviceid ilike $serviceid");
	    my $result = $sth->execute;
	    $master = "master";
	}
        $error = $dbh->errstr;
        return ($result,$master);
}

sub get_serviceid_alt_for_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT serviceid_alt FROM service
				WHERE serviceid = $sid");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_parent_site_id {
	my $sid = $dbh->quote(shift @_);
	my $accno = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT parent_site_id FROM service
				WHERE service_id = $sid and customer_id = $accno");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = ($sth->fetchrow_array)[0];
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


sub update_service {
  open (EPLed, ">>/data1/tmp_log/edit.log");
  CMS_DB::connect_to_database();
  CMS_DB::begin_transaction();
  my $serviceid_raw = shift @_;
  my $serviceid = $dbh_tr->quote($serviceid_raw);
  my $accno_raw = shift @_;
  my $accno = $dbh_tr->quote($accno_raw);
  my $description = $dbh_tr->quote(shift @_);
  my $currentbw = $dbh_tr->quote(shift @_);
  my $productcode_raw = shift @_;
  my $productcode = $dbh_tr->quote($productcode_raw);
  #Variables used for EPL
  my ($epl_msid,$epl_aend_jid,$epl_zend_jid,$epl_aend_jno,$epl_zend_jno,$epl_aend_jid2,$epl_zend_jid2,$epl_aend_jno2,$epl_zend_jno2,$billable,$epl_billable,$epl_billable2,$billstartdate_year,$billstartdate_month,$billstartdate_day,$billstarttime_hour,$billstarttime_minute,$billstarttime_second,$billenddate_year,$billenddate_month,$billenddate_day,$billendtime_hour,$billendtime_minute,$billendtime_second,$billable_raw, $epl_billable_raw, $epl_billable2_raw,$as_no,$auto_gen_prefix,$bgp_neighbor_ip,$irrd_object,$ipt_msid, $gia_aggregate_sid) = "";
 my ($black_hole_routing, $bha_bgp_neighbor_ip, $bha_auto_gen_prefix) = "";
  if ($productcode_raw =~ /EPL/){
	  $epl_msid = $dbh_tr->quote(shift @_);
	  $epl_aend_jid = $dbh_tr->quote(shift @_);
	  $epl_zend_jid = $dbh_tr->quote(shift @_);
	  $epl_aend_jno = $dbh_tr->quote(shift @_);
	  $epl_zend_jno = $dbh_tr->quote(shift @_);
	  $epl_aend_jid2 = $dbh_tr->quote(shift @_);
	  $epl_zend_jid2 = $dbh_tr->quote(shift @_);
	  $epl_aend_jno2 = $dbh_tr->quote(shift @_);
	  $epl_zend_jno2 = $dbh_tr->quote(shift @_);
	  # for billing dates
	  $epl_billable_raw = shift @_;
	  $epl_billable2_raw = shift @_;
	  $epl_billable = $dbh_tr->quote($epl_billable_raw);
	  $epl_billable2 = $dbh_tr->quote($epl_billable2_raw);
	  $billstartdate_year = shift @_;
	  $billstartdate_month = shift @_;
	  $billstartdate_day = shift @_;
	  $billstarttime_hour = shift @_;
	  $billstarttime_minute = shift @_;
	  $billstarttime_second = shift @_;
	  $billenddate_year = shift @_;
	  $billenddate_month = shift @_;
	  $billenddate_day = shift @_;
	  $billendtime_hour = shift @_;
	  $billendtime_minute = shift @_;
	  $billendtime_second = shift @_;
  }  elsif (($productcode_raw =~ /IPTRANSIT/)||($productcode_raw =~ /GIA/)){
        $as_no = $dbh_tr->quote(shift @_);
        $auto_gen_prefix = $dbh_tr->quote(shift @_);
        $bgp_neighbor_ip = $dbh_tr->quote(shift @_);
        $irrd_object = $dbh_tr->quote(shift @_);
	if ($productcode_raw =~ /IPTRANSIT/){
        $ipt_msid = $dbh_tr->quote(shift @_);
	}
	if ($productcode_raw =~ /GIA/){
		$gia_aggregate_sid = $dbh_tr->quote(shift @_);
	}		
	$black_hole_routing = $dbh_tr->quote(shift @_);
	$bha_bgp_neighbor_ip = $dbh_tr->quote(shift @_);
	$bha_auto_gen_prefix = $dbh_tr->quote(shift @_);
        $billable = '2';
	open (IPT2, ">>/data1/tmp_log/edit.log");
	print IPT2 "as_no:$as_no\tauto_gen_prefix:$auto_gen_prefix\tbgp_neighbor_ip:$bgp_neighbor_ip\tirrd_object:$irrd_object\tipt_msid:$ipt_msid\t $black_hole_routing \t $bha_bgp_neighbor_ip \t $bha_auto_gen_prefix\n";
	if ($productcode_raw =~ /GIA/){
		$billable_raw = shift @_;
  		$billable = $dbh_tr->quote($billable_raw);
  		$billstartdate_year = shift @_;
  		$billstartdate_month = shift @_;
  		$billstartdate_day = shift @_;
  		$billstarttime_hour = shift @_;
  		$billstarttime_minute = shift @_;
  		$billstarttime_second = shift @_;
  		$billenddate_year = shift @_;
  		$billenddate_month = shift @_;
  		$billenddate_day = shift @_;
  		$billendtime_hour = shift @_;
  		$billendtime_minute = shift @_;
  		$billendtime_second = shift @_;
	}
  }


  my $switchsourcode = $dbh_tr->quote(shift @_);
  my $switchdestcode = $dbh_tr->quote(shift @_);
  my $pollingtype = $dbh_tr->quote(shift @_);
  my $costype = $dbh_tr->quote(shift @_);
  my $portmode_raw = shift @_;
  my $portmode = $dbh_tr->quote($portmode_raw);
  my $gold_classmap_raw    = shift @_;
  my $gold_rateacl_raw     = shift @_;
  my $silver_classmap_raw  = shift @_;
  my $silver_rateacl_raw   = shift @_;
  my $default_classmap_raw = shift @_;
  my $default_rateacl_raw  = shift @_;
  my $class1_classmap_raw = shift @_;
  my $class1_rateacl_raw  = shift @_;
  my $class2_classmap_raw = shift @_;
  my $class2_rateacl_raw  = shift @_;
  my $class4_classmap_raw = shift @_;
  my $class4_rateacl_raw  = shift @_;
  my $gold_classmap    = $dbh_tr->quote($gold_classmap_raw);
  my $gold_rateacl     = $dbh_tr->quote($gold_rateacl_raw);
  my $silver_classmap  = $dbh_tr->quote($silver_classmap_raw);
  my $silver_rateacl   = $dbh_tr->quote($silver_rateacl_raw);
  my $default_classmap = $dbh_tr->quote($default_classmap_raw);
  my $default_rateacl  = $dbh_tr->quote($default_rateacl_raw);
  my $class1_classmap = $dbh_tr->quote($class1_classmap_raw);
  my $class1_rateacl  = $dbh_tr->quote($class1_rateacl_raw);
  my $class2_classmap = $dbh_tr->quote($class2_classmap_raw);
  my $class2_rateacl  = $dbh_tr->quote($class2_rateacl_raw);
  my $class4_classmap = $dbh_tr->quote($class4_classmap_raw);
  my $class4_rateacl  = $dbh_tr->quote($class4_rateacl_raw);

# Stavan - CRQ000000004060 - 20110824
  if ($productcode_raw =~ /GBS/) {
        $as_no = $dbh_tr->quote(shift @_);
        $auto_gen_prefix = $dbh_tr->quote(shift @_);
        $bgp_neighbor_ip = $dbh_tr->quote(shift @_);
        $irrd_object = $dbh_tr->quote(shift @_);
  }  
#  my $serviceclass = $dbh_tr->quote(shift @_);
  my $aggregate_serviceid = $dbh_tr->quote(shift @_);
  my $master_serviceid =$dbh_tr->quote(shift @_);
  # for billing dates
  #my $billable = $dbh_tr->quote(shift @_);  # changed to following 2 lines to fix the billing option updating problem
  if (($productcode_raw !~ /EPL/) && ($productcode_raw !~ /GIA/)){
  print EPLed "billable_raw:$billable_raw\n";
  $billable_raw = shift @_;
  $billable = $dbh_tr->quote($billable_raw);
  $billstartdate_year = shift @_;
  $billstartdate_month = shift @_;
  $billstartdate_day = shift @_;
  $billstarttime_hour = shift @_;
  $billstarttime_minute = shift @_;
  $billstarttime_second = shift @_;
  $billenddate_year = shift @_;
  $billenddate_month = shift @_;
  $billenddate_day = shift @_;
  $billendtime_hour = shift @_;
  $billendtime_minute = shift @_;
  $billendtime_second = shift @_;
  }
  my $serviceid_alt_raw = shift @_;
  my $serviceid_alt = $dbh_tr->quote($serviceid_alt_raw);
  my $aend_jobid = $dbh_tr->quote(shift @_);
  my $zend_jobid = $dbh_tr->quote(shift @_);
  my $aend_jobno = $dbh_tr->quote(shift @_);
  my $zend_jobno = $dbh_tr->quote(shift @_);
  my $aend_country = $dbh_tr->quote(shift @_);
  my $zend_country = $dbh_tr->quote(shift @_);
#Added by Nisha for mVPN on 30Sep2009
  my $voice_mvpn_in = $dbh_tr->quote(shift @_);
  my $video_mvpn_in = $dbh_tr->quote(shift @_);
  my $video_mvpn_out = $dbh_tr->quote(shift @_);
  my $criticaldata_mvpn_in = $dbh_tr->quote(shift @_);
  my $criticaldata_mvpn_out = $dbh_tr->quote(shift @_);
  my $interactivedata_mvpn_in = $dbh_tr->quote(shift @_);
  my $interactivedata_mvpn_out = $dbh_tr->quote(shift @_);
  my $standarddata_mvpn_in = $dbh_tr->quote(shift @_);
  my $standarddata_mvpn_out = $dbh_tr->quote(shift @_);
  my $lowpriority_mvpn_in = $dbh_tr->quote(shift @_);
  my $lowpriority_mvpn_out = $dbh_tr->quote(shift @_);
  my $mvpnCheck = $dbh_tr->quote(shift @_); 
#End of code Added by Nisha for mVPN on 30Sep2009

  # get service id - legacy - may be upper or lower case

  #modified by Karuna to add a check for EPL as well
  if($productcode =~ /(EVPL|EPL)/){
  }else{
    $serviceid =~ s/\'//g;
    $serviceid =~ s/\"//g;
  }


  my $sth = $dbh_tr->prepare("SELECT serviceid FROM service WHERE serviceid ilike ?");
  $sth->execute($serviceid);
  my $r = $sth->fetchrow_hashref;
  if($$r{serviceid} =~ /^$serviceid$/i) {
	$serviceid = "'$$r{serviceid}'";
  }

  my $billstartdt = 'null';
 # Stavan - CRQ000000004060 
  if (($billstartdate_year !~ /-/) && ( $billstartdate_year ne ''))  {
        $billstartdt =
                $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $billstartdate_year, $billstartdate_month, $billstartdate_day,
                        $billstarttime_hour, $billstarttime_minute, $billstarttime_second));
  }
  my $billenddt = 'null';
  if (($billenddate_year !~ /-/) && ( $billenddate_year ne '')) {
        $billenddt =
                $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $billenddate_year, $billenddate_month, $billenddate_day,
                        $billendtime_hour, $billendtime_minute, $billendtime_second));
  }

  # extend the update sql statement
  my $morefield;
  $currentbw =~ s/\'//g;  # remove quote for bandwidth - int
  if(($currentbw =~ /^\d+$/)&&($productcode !~ /EPL/)) {
        $morefield .= " , currentbw = $currentbw";
  } else {
  if ($productcode =~ /EPL/){
  	if ($currentbw !~ /(^(\d+\.\d+|\d+\.|\.\d+)$)|(^\d+$)/){
  	$error = "Bandwidth field should be a number. Please re enter the bandwidth.";
  	return;
	} else {
        #Convert bandwidth to bps for EPL
        $currentbw = $currentbw*1000000;
	$morefield .= " , currentbw = $currentbw";
	}#bandwidth check
  }
  }


if($productcode =~ /VPLS/)
{
  $pollingtype = $portmode; 
  $switchsourcode='null';
  $switchdestcode='null';
 # $auto_gen_prefix='';
  $costype = "reachvpls";
  $costype = "\'$costype\'";
} 

#Chandini
  if($productcode =~ /EVPL/){
  $silver_classmap = $gold_classmap;
  $silver_rateacl = $gold_rateacl;
  $gold_classmap = 'null';
  $gold_rateacl = 'null';
  }

#Added by karuna for EPL
  if ($productcode =~ /EPL/){
  my $temp_serviceid = $serviceid;
#Added new telstra id condition-TGC0031549
  if ($temp_serviceid =~ m/epl/gi) {
if  ($epl_msid !~ "'-- Select --'"){
                $billable = $epl_billable2;
                if ($epl_aend_jno2 eq "''"){
                #Job note cannot be blank
                $error = "A-end job note cannot be blank for multipoint EPL service.";
                return ;
                } elsif (($epl_aend_jno2 !~ "''") && ($epl_aend_jno2 !~ m/^\'MA/gi)) {
                #A-end job note validation
                $error = "A-end job note must begin with MA for Point to Multipoint services.";
                return ;
                }
                elsif (($epl_zend_jno2 !~ "''") && ($epl_zend_jno2 !~ m/^(\'EZ|\'MZ)/gi)) {
                #Z-end job note validation
                $error = "Z-end job note must begin with either EZ or MZ.";
                return ;
                }
                #Assigning EPL job ids,notes to the GBS job ids, notes
        $aend_jobid = $epl_aend_jid2;
        $zend_jobid = $epl_zend_jid2;
        #convert to lower case before entering into the DB
        $aend_jobno = lc($epl_aend_jno2);
        $zend_jobno = lc($epl_zend_jno2);
        $billable_raw = $epl_billable2_raw;
  }
        else {
                $billable = $epl_billable2;
                if (($epl_aend_jno2 eq "''") && ($epl_zend_jno2 eq "''")){
                #Job note cannot be blank
                $error = "Please enter either A-end or Z-end job note.";
                return ;
                } elsif (($epl_zend_jno2 !~ "''") && ($epl_zend_jno2 !~ m/^(\'EZ|\'MZ)/gi)) {
                #Z-end job note validation
                $error = "Z-end job note must begin with either EZ or MZ and should be of the format EZ_SNG_6NTP_EW_12345 or EZ_SNG_6NTP_EM_12345.";
                return ;
                } elsif (($epl_aend_jno2 !~ "''") && ($epl_aend_jno2 !~ m/^(\'EA|\'MA)/gi)) {
                #A-end job note validation
                $error = "A-end job note must begin with either EA or MA and should be of the format EA_SNG_6NTP_EW_12345 or EA_SNG_6NTP_EM_12345.";
                return ;
		                }
                #Assigning EPL job ids,notes to the GBS job ids, notes
                $epl_msid = "''";
  $aend_jobid = $epl_aend_jid2;
  $zend_jobid = $epl_zend_jid2;
  #conversion to lower case before entering into the DB
  $aend_jobno = lc($epl_aend_jno2);
  $zend_jobno = lc($epl_zend_jno2);
  $billable_raw = $epl_billable2_raw;
                }
                }
  elsif ($temp_serviceid =~ m/em/gi) {
  #for point to multipoint services
                $billable = $epl_billable2;
                if ($epl_msid =~ "'-- Select --'") {
                #MSID validation
                $error = "Please select a Master Service ID for multipoint EPL service.";
                return ;
                }
                if ($epl_aend_jno2 eq "''"){
                #A end job note validation
                $error = "A-end job note cannot be blank for multipoint EPL service.";
                return ;
                } elsif ($epl_aend_jno2 !~ m/^\'MA/i) {
                $error = "Fields mismatch. Service ID indicates Point to Multipoint Service type, A-end job note must begin with MA.";
                return ;
                } elsif (($epl_zend_jno2 !~ "''") && ($epl_zend_jno2 !~ m/^(\'EZ|\'MZ)/gi)) {
                #Z end job note validation
                $error = "Z-end job note must begin with either EZ or MZ and should be of the format EZ_ SNG_6NTP_EW_12345 or MZ_SNG_6NTP_EW_12345.";
                return ;
                } #end of point to multipoint job note validations
  $aend_jobid = $epl_aend_jid2;
  $zend_jobid = $epl_zend_jid2;
  #conversion to lower case before entering into the DB
  $aend_jobno = lc($epl_aend_jno2);
  $zend_jobno = lc($epl_zend_jno2);
  $billable_raw = $epl_billable2_raw;
  #end of point to multipoint services check
  } elsif ($temp_serviceid =~ m/ew/gi) {
  #point to point services
                $billable = $epl_billable;
                $epl_msid = "''";
                if (($epl_aend_jno eq "''") && ($epl_zend_jno eq "''")){
                #Job notes cannot be blank
                $error = "Please enter either A-end or Z-end job note.";
                return ;
                } elsif (($epl_zend_jno !~ "''") && ($epl_zend_jno !~ m/^(\'EZ|\'MZ)/gi)) {
                #Z end job note validation
                $error = "Z-end job note must begin with either EZ or MZ and should be of the format EZ_ SNG_6NTP_EW_12345 or MZ_SNG_6NTP_EW_12345.";
                return ;
                } elsif (($epl_aend_jno !~ "''") && ($epl_aend_jno !~ m/^(\'EA|\'MA)/gi)) {
                #A end job note validation
                $error = "A-end job note must begin with either EA or MA and should be of the format EA_HKG_HKTH_EW_12345 or MA_HKG_HKTH_EW_12345.";
                return ;
                }
  $aend_jobid = $epl_aend_jid;
  $zend_jobid = $epl_zend_jid;
  #conversion to lower case before entering into the DB
  $aend_jobno = lc($epl_aend_jno);
  $zend_jobno = lc($epl_zend_jno);
  $billable_raw = $epl_billable_raw;
  }
  $master_serviceid = $epl_msid;
 }

#added for MPLS fix by Karuna
  if ($productcode =~ /MPLS/) {
     my $n_billable = $billable;
     $n_billable =~ s/\'//g;
     if ($n_billable == 2) {
         $billenddt = 'null';
     }

     #CR-84 Juniper IPVPN Report
     my $router_os = CMS_ROUTERS_DB::get_routeros($serviceid_raw);
     if ($router_os =~ /Juniper/i) {
            $costype = "\'reachvpls\'";
     }
  }

if ($productcode =~ /IPTRANSIT/) {
	open (IPT2,">>/data1/tmp_log/edit.log");
	$master_serviceid = $ipt_msid;
	print IPT2 "inside prod code IPT: ipt_msid:$master_serviceid\n";
	#bandwidth not mandatory for IP Transit services
	if ($currentbw eq "") { $currentbw = "null";} 
}
print EPLed "entered\n";
print EPLed "productcode:$productcode\tmvpnCheck:$mvpnCheck\tbillable:$billable\tbillstartdt:$billstartdt\tbillenddt:$billenddt\tbillstartdate_year:$billstartdate_year\tbillenddate_year:$billenddate_year\n";



  $morefield .= " , productcodeid = $productcode";
  $morefield .= " , switchsourcodeid = $switchsourcode";
  $morefield .= " , switchdestcodeid = $switchdestcode";
  $morefield .= " , pollingtype = $pollingtype";
  $morefield .= " , costype = $costype";
 # $morefield .= " , portmode = $portmode";
  $morefield .= " , gold_classmap = $gold_classmap";
  $morefield .= " , gold_rateacl = $gold_rateacl";
  $morefield .= " , silver_classmap = $silver_classmap";
  $morefield .= " , silver_rateacl = $silver_rateacl";
  $morefield .= " , default_classmap = $default_classmap";
  $morefield .= " , default_rateacl = $default_rateacl";
  $morefield .= " , class1_classmap = $class1_classmap";
  $morefield .= " , class1_rateacl = $class1_rateacl";
  $morefield .= " , class2_classmap = $class2_classmap";
  $morefield .= " , class2_rateacl = $class2_rateacl";
  $morefield .= " , class4_classmap = $class4_classmap";
  $morefield .= " , class4_rateacl = $class4_rateacl";
  ##Added by Karuna on 01-Dec-2010 for EPL fix
  if ($as_no eq ""){
	$as_no = "NULL";
  }
  if ($bgp_neighbor_ip eq ""){
	$bgp_neighbor_ip = "NULL";
  }
  if ($irrd_object eq ""){
  	$irrd_object = "NULL";
  }
# Stavan - CRQ000000004060
  my $unquoted_billable =~ s/'//g;
  if ($unquoted_billable eq "") {
        $billable = "NULL";
  }
  $morefield .= " , as_no = $as_no";
  $morefield .= " , bgp_neighbor_ip = $bgp_neighbor_ip";
  $morefield .= " , irrd_object = $irrd_object";
#  $morefield .= " , serviceclassid = $serviceclass";
  $morefield .= " , aggregate_serviceid = $aggregate_serviceid"; 
  $morefield .= " , master_serviceid = $master_serviceid";
  # for billing dates
  $morefield .= " , billing_optionid = $billable";
  $morefield .= " , billing_starttime = $billstartdt";
  $morefield .= " , billing_endtime = $billenddt";
  # for serviceid for atm/fr
  $morefield .= " , serviceid_alt = $serviceid_alt";
  # for gbs
  $morefield .= " , aend_jobid = $aend_jobid";
  $morefield .= " , zend_jobid = $zend_jobid";
  $morefield .= " , aend_jobno = $aend_jobno";
  $morefield .= " , zend_jobno = $zend_jobno";
  $morefield .= " , aend_country = $aend_country";
  $morefield .= " , zend_country = $zend_country";
#Added by Nisha for mVPN on 30Sep2009
  $morefield .= " , mcast_class5_in = $voice_mvpn_in";
  $morefield .= " , mcast_class4_in = $video_mvpn_in";
  $morefield .= " , mcast_class4_out = $video_mvpn_out";
  $morefield .= " , mcast_class3_in = $criticaldata_mvpn_in";
  $morefield .= " , mcast_class3_out = $criticaldata_mvpn_out";
  $morefield .= " , mcast_class2_in = $interactivedata_mvpn_in";
  $morefield .= " , mcast_class2_out = $interactivedata_mvpn_out";
  $morefield .= " , mcast_class1_in = $standarddata_mvpn_in";
  $morefield .= " , mcast_class1_out = $standarddata_mvpn_out";
  $morefield .= " , mcast_class0_in = $lowpriority_mvpn_in";
  $morefield .= " , mcast_class0_out = $lowpriority_mvpn_out";
  $morefield .= " , mcast_option = $mvpnCheck";
  print EPLed "update fields: $morefield\n";
#End of code Added by Nisha for mVPN on 30Sep2009

#Added by Karuna for IP Transit
if ($productcode =~ /IPTRANSIT/) {
	open (IPT2,">>/data1/tmp_log/edit.log");
	if ($black_hole_routing =~ /No/){
		$bha_auto_gen_prefix = "\'Disable\'";
		print IPT2 "serviceid:$serviceid\tbha_bgp_neighbor_ip:$bha_bgp_neighbor_ip\n";
		my $null = "NULL";
		##Modified for Addendum 2
		#$sth = $dbh_tr->prepare ("UPDATE link SET bha_routerid = NULL, bha_description = NULL WHERE serviceid = $serviceid");
		$sth = $dbh_tr->prepare ("UPDATE link SET bha_routerid = NULL, bha_description = NULL, bha_routerid_bac = NULL, bha_description_bac = NULL WHERE serviceid = $serviceid");
                print IPT2 "UPDATE link SET bha_routerid = $null, bha_routerid_bac = $null WHERE serviceid = $serviceid\n";
		##--Addendum 2
		my $success1 = 1;
		#$success1 &&=$sth->execute;
		$sth->execute;
		$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
		print IPT2 "link update error:$error\n";
		$sth->finish();
		my $result1 = ($success1 ? $dbh_tr->commit : $dbh_tr->rollback);
		$dbh_tr->commit;
	}
	$sth = $dbh_tr->prepare ("UPDATE service SET accno = $accno, description = $description, productcodeid = $productcode, currentbw = $currentbw, master_serviceid = $ipt_msid, bgp_neighbor_ip = $bgp_neighbor_ip, as_no = $as_no,black_hole_routing =$black_hole_routing, bha_bgp_neighbor_ip =$bha_bgp_neighbor_ip, bha_auto_gen_prefix = $bha_auto_gen_prefix where serviceid = $serviceid");
	print IPT2 "\n UPDATE service SET accno = $accno description = $description, productcodeid = $productcode, currentbw = $currentbw, master_serviceid = $ipt_msid, bgp_neighbor_ip = $bgp_neighbor_ip, as_no = $as_no where serviceid = $serviceid";
  	my $success = 1;
  	$success &&= $sth->execute;
  	my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
  	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
		print IPT2 "UPDATE error:$error\n";
  	if ($success) {
		print IPT2 "update service set auto_gen_prefix = $auto_gen_prefix, bha_auto_gen_prefix = $bha_auto_gen_prefix, irrd_object = $irrd_object where as_no = $as_no\n";
        	#$sth = $dbh_tr->prepare ("update service set auto_gen_prefix = $auto_gen_prefix, irrd_object = $irrd_object where as_no = $as_no and serviceid = $serviceid");
        	$sth = $dbh_tr->prepare ("update service set auto_gen_prefix = $auto_gen_prefix, irrd_object = $irrd_object where as_no = $as_no");
        	$success &&= $sth->execute;
        	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
		print IPT2 "UPDATE error:$error\n";
  		$result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
		$sth->finish();
		if ($black_hole_routing =~ /Yes/){
			$sth = $dbh_tr->prepare ("update service set bha_auto_gen_prefix=$bha_auto_gen_prefix WHERE as_no = $as_no and black_hole_routing = 'Yes'");
			if ($debug){print IPT_DBG "DB UPDATE BHA yes:update service set bha_auto_gen_prefix=$bha_auto_gen_prefix WHERE as_no = $as_no and black_hole_routing='Yes'\n";}
			$success &&= $sth->execute;
			$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
			if ($debug){print IPT_DBG "DB UPDATE BHA yes ERROR:$error\n";}
			$result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
			$sth->finish();
		}
  	}

  	#CMS_DB::disconnect_from_transaction();
	close (IPT2);
  	return $error if ($error);
  	return $success;	
} elsif($productcode =~ /ETHERNET/) {
	$sth = $dbh_tr->prepare ("UPDATE service SET description = $description, productcodeid = $productcode, currentbw = $currentbw where accno = $accno AND serviceid = $serviceid");
        my $success = 1;
        $success &&= $sth->execute;
        my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
        #CMS_DB::disconnect_from_transaction();
        return $error if ($error);
        return $success;
 } elsif ($productcode =~ /GIA/){
	open (IPT2,">>/data1/tmp_log/edit.log");
	print IPT2 "as_no:$as_no \n";
        my $sth_asno = $dbh_tr->prepare ("select as_nos from ipc_china where as_nos like $as_no and dir_traffic like 'direct'");
        $sth_asno->execute();
        my $row2 = $sth_asno->rows();
	print IPT2 "row2:$row2\n";

        if ($row2 > 0) {
                $error= "AS Number $as_no already in use by an IPC service(Direct China Traffic).";
		return ;
        }
	$currentbw = "null";
        if ($black_hole_routing =~ /No/){
                $bha_auto_gen_prefix = "\'Disable\'";
		 ##++Modified for Addendum 2
		#$sth = $dbh_tr->prepare ("UPDATE link SET bha_routerid = NULL, bha_description = NULL WHERE serviceid = $serviceid");
		$sth = $dbh_tr->prepare ("UPDATE link SET bha_routerid = NULL, bha_description = NULL, bha_routerid_bac = NULL, bha_description_bac = NULL WHERE serviceid = $serviceid");
		print IPT2 "UPDATE link SET bha_routerid =NULL, bha_routerid_bac =NULL WHERE serviceid = $serviceid\n";
		##--Addendum 2
                $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                print IPT2 "link update error:$error\n";
		$sth->finish();
                $dbh_tr->commit;
	}
	#$sth = $dbh_tr->prepare("UPDATE service SET description = $description $morefield WHERE accno = $accno AND serviceid = $serviceid");
	$sth = $dbh_tr->prepare("UPDATE service SET accno = $accno, description = $description, productcodeid = $productcode, currentbw = $currentbw, bgp_neighbor_ip = $bgp_neighbor_ip, as_no = $as_no, aggregate_serviceid=$gia_aggregate_sid, black_hole_routing =$black_hole_routing, bha_bgp_neighbor_ip =$bha_bgp_neighbor_ip, bha_auto_gen_prefix =$bha_auto_gen_prefix, billing_optionid = $billable, billing_starttime = $billstartdt, billing_endtime = $billenddt where serviceid = $serviceid"); 
	print IPT2 "UPDATE service SET accno = $accno, description = $description, productcodeid = $productcode, currentbw = $currentbw, bgp_neighbor_ip = $bgp_neighbor_ip , as_no = $as_no,black_hole_routing =$black_hole_routing, bha_bgp_neighbor_ip =$bha_bgp_neighbor_ip, bha_auto_gen_prefix =$bha_auto_gen_prefix, billing_optionid = $billable, billing_starttime = $billstartdt, billing_endtime = $billenddt where serviceid = $serviceid\n";
} else { 
	print EPLed "UPDATE service SET description = $description $morefield WHERE accno = $accno AND serviceid = $serviceid\n";
  $sth = $dbh_tr->prepare ("UPDATE service SET description = $description $morefield WHERE accno = $accno AND serviceid = $serviceid");
} 
  my $success = 1;
  $success &&= $sth->execute();
	$sth->finish();
	print EPLed "success :$success\n";
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
	print EPLed "error:$error\n";
  if ($success) {
	$dbh_tr->commit();
        $success &&= update_billing(
                        $serviceid_raw, $accno_raw, $billable_raw,
                        $billstartdate_year, $billstartdate_month, $billstartdate_day,
                        $billstarttime_hour, $billstarttime_minute, $billstarttime_second,
                        $billenddate_year, $billenddate_month, $billenddate_day,
                        $billendtime_hour, $billendtime_minute, $billendtime_second,
                        $productcode_raw,
                        $gold_classmap_raw, $gold_rateacl_raw,
                        $silver_classmap_raw, $silver_rateacl_raw,
                        $default_classmap_raw, $default_rateacl_raw
                        )
  }

  if ($success) {
	#$sth = $dbh_tr->prepare ("update service set auto_gen_prefix = $auto_gen_prefix, irrd_object = $irrd_object where as_no = $as_no and serviceid = $serviceid");
	$sth = $dbh_tr->prepare ("update service set auto_gen_prefix = $auto_gen_prefix, irrd_object = $irrd_object where as_no = $as_no");
  	$success &&= $sth->execute;
       	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
	$dbh_tr->commit();
  }
  my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
	print EPLed "result:$result\n";
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
	#$sth->finish();

  CMS_DB::disconnect_from_transaction();
  return $error if ($error);
  return $success;
}
sub delete_service {
  my $serviceid = shift @_;
  my $accno_raw = shift @_;
  my $last_asn = shift @_;
  if ($debug){print IPT_DBG "DB delete_service last_asn:$last_asn\n";}
  my $accno = $dbh->quote($accno_raw);
  my ($sth, $result);

  # retrieve service's as no 
  $sth = $dbh->prepare("select as_no from service where serviceid = '$serviceid'");
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  $result = $sth->fetchall_arrayref;
  if ($result->[0][0] ne "") {
     my $sth2 = $dbh->prepare("select serviceid from service where as_no = '$result->[0][0]' and serviceid != '$serviceid'");
     unless ($sth2->execute) {
       $error = $dbh->errstr;
       return 0;
     }

     my $result2 = $sth2->fetchall_arrayref;
     if ($#{$result2} >= 0) {
        # update routing queue's service id
        CMS_ROUTING_QUEUE_DB::update_opshandle($serviceid, $result2->[0][0]);
     }
  }     
  CMS_DB::begin_transaction();
  my ($billable, $billing_starttime, $billing_endtime);
  my ($productcode, $gold_classmap, $gold_rateacl, $silver_classmap, $silver_rateacl, $default_classmap, $default_rateacl);
  my ($billstartdate_year, $billstartdate_month, $billstartdate_day, $billstarttime_hour, $billstarttime_minute, $billstarttime_second);
  my ($billenddate_year, $billenddate_month, $billenddate_day, $billendtime_hour, $billendtime_minute, $billendtime_second);
  if (my $tmp = get_service($serviceid, $accno_raw) ) {
        $productcode = $$tmp[2];
        $gold_classmap = $$tmp[10];
        $gold_rateacl = $$tmp[11];
        $silver_classmap = $$tmp[12];
        $silver_rateacl = $$tmp[13];
        $default_classmap = $$tmp[14];
        $default_rateacl = $$tmp[15];
        #Modified by Karuna for EPL on 09-Mar-2010
        if ($productcode =~ /EPL/){
        $billable = $$tmp[26];
        $billing_starttime = $$tmp[27];
        $billing_endtime = $$tmp[28];
        } else {
        $billable = $$tmp[31];
        $billing_starttime = $$tmp[32];
        $billing_endtime = $$tmp[33];
        }

        # startdate
        my @tmpdt  = split(/-|\s|\:|\+/, $billing_starttime);
        my @billdt;
        if ($tmpdt[0] ne '') {
                @billdt = (@tmpdt[0,1,2,3,4,5]);
                #@billdt = (@tmpdt[0,1,2]);
        } else {
                @billdt = ('----','--','--','--','--','--');
                #@billdt = ('----','--','--');
        }
        ( $billstartdate_year, $billstartdate_month, $billstartdate_day,
                $billstarttime_hour, $billstarttime_minute, $billstarttime_second
                ) = @billdt;
        # enddate
        #my @tmpdt  = split(/-|\s|\:|\+/, $billing_endtime);
        @tmpdt  = split(/-|\s|\:|\+/, $billing_endtime);
        #my @billdt;
        @billdt = ();
        if ($tmpdt[0] ne '') {
                @billdt = (@tmpdt[0,1,2,3,4,5]);
                #@billdt = (@tmpdt[0,1,2]);
        } else {
                @billdt = ('----','--','--','--','--','--');
                #@billdt = ('----','--','--');
        }
        ( $billenddate_year, $billenddate_month, $billenddate_day,
                $billendtime_hour, $billendtime_minute, $billendtime_second
                ) = @billdt;
  } else {
	my $tmperr = "Account does not have such a service.";
        $error = (length($error)!=0) ? $error."<br>"."$tmperr" : $tmperr;
        return 0;
  }

  ## Added by Karuna for IP Transit Ph3
  my $date = `date "+%Y-%m-%d"`;
  chomp($date);
  my $path = "/usr/local/www/wanchai/cms/data/poller/";
  my $file = "ipt_deleted_services.list";
  open (IPT_FH, ">>/$path$file") or die "Failed to write file $file: $!\n";

  ##Added by Karuna for IPT ph 3
  open (IPT_DEL, ">>/tmp/delete_link");
  my $del_query1 = $dbh->prepare("SELECT serviceid, accno, master_serviceid from service where serviceid ='$serviceid' and accno = $accno AND productcodeid='IPTRANSIT'");
  print IPT_DEL "del_query1:SELECT serviceid, accno, master_serviceid from service where serviceid ='$serviceid' and accno = $accno AND productcodeid='IPTRANSIT'\n";
  $del_query1->execute();
  my @service_details = $del_query1->fetchrow_array;
  $del_query1->finish();
  my $ipt = $service_details[0];
  my $ipt_accno = $service_details[1];
  my $ipm = $service_details[2];
  
  my $del_query2 = $dbh->prepare("select DISTINCT ON (serviceid) serviceid,popname from deleted_links where serviceid='$serviceid' order by serviceid,date_of_deletion DESC");
  print IPT_DEL "del_query2:SELECT DISTINCT ON (serviceid) serviceid,popname from deleted_links where serviceid='$serviceid' order by serviceid,date_of_deletion DESC\n";
  $del_query2->execute();
  my @link_details = $del_query2->fetchrow_array;
  $del_query2->finish();
  my $popname = $link_details[1];
#  if ($popname eq ""){
#	$popname = "-";
#  }
  $date = `date "+%Y-%m-%d"`;
  chomp($date);

  ##Added by Karuna on 17-Dec-2010
  ##Call function to update routing record
  #03Feb2011
  if ($last_asn eq "Yes"){
	if($debug) {print IPT_DBG "before calling record_service_details\n";}
	&record_service_details($serviceid, $accno, $last_asn);
   } else {
  	&record_service_details($serviceid, $accno);
  }


  print IPT_DEL "ipt:$ipt\tipt_accno:$ipt_accno\tipm:$ipm\tpopname:$popname\n"; 

  $sth = $dbh_tr->prepare
    ("DELETE FROM service
      WHERE accno = $accno
        AND serviceid ilike ?");

  my $success = 1;
  $success &&= $sth->execute($serviceid);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

  if ($success) {
        $success &&= update_billing(
                        $serviceid, $accno_raw, $billable,
                        $billstartdate_year, $billstartdate_month, $billstartdate_day,
                        $billstarttime_hour, $billstarttime_minute, $billstarttime_second,
                        $billenddate_year, $billenddate_month, $billenddate_day,
                        $billendtime_hour, $billendtime_minute, $billendtime_second,
                        $productcode,
                        $gold_classmap, $gold_rateacl,
                        $silver_classmap, $silver_rateacl,
                        $default_classmap, $default_rateacl
                        )
  }

   $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
  CMS_DB::disconnect_from_transaction();

  # delete route from routing queue
  CMS_ROUTING_QUEUE_DB::delete_routing_request_by_opshandle($serviceid);

  #write to list file
  unless ($error){
  	#print IPT_FH "$ipt\t$ipt_accno\t$ipm\t$popname\t$date\n";
	##Added on 09-Nov-2010 by Karuna
	if (($ipt ne "") && ($ipt_accno ne "")){
  		print IPT_FH "$ipt,$ipt_accno,$ipm,$popname,$date\n";
	}
  	close (IPT_FH);
  }
  return $success;
##  return 1;
}

sub get_accno_for_serviceid {
  # For OLSS
  my $serviceid = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT accno
      FROM service
      WHERE serviceid = $serviceid");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchrow_arrayref;

  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $$result[0];
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

#Added by Nisha for mVPN:06oct2009
sub search_service_mpls {
  my $serviceid = shift @_;
  my $accno = shift @_;
  my $description = shift @_;
  my $bandwidth = shift @_;
  my $productcode = shift @_;
  my $mcastoption = shift @_;

  my $cond = "";
  my $command = "";

  $command =
   ("SELECT service.serviceid, accno, service.description, currentbw, productcodeid, routername, interface, aggregate_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, mcast_class5_in, mcast_class4_in, mcast_class4_out, mcast_class3_in, mcast_class3_out, mcast_class2_in, mcast_class2_out, mcast_class1_in, mcast_class1_out, mcast_class0_in, mcast_class0_out, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'MPLS') COND");

  if($mcastoption == "1"){
    $cond .= "AND service.mcast_option = 1 ";
  } elsif($mcastoption == "2") {
    $cond .= "AND service.mcast_option = 2 ";
  } 

  if ($serviceid ne "") {
    $cond .= "AND service.serviceid ilike '%$serviceid%' ";
  }

  if ($accno ne "") {
    $cond .= "AND service.accno ilike '%$accno%' ";
  }

  if (($serviceid ne "") && ($accno ne "")) {
    $cond .= "AND service.serviceid ilike '%$serviceid%' AND service.accno ilike '%$accno%'";
  }

  if ($description ne "") {
    $cond .= "AND service.description ilike '%$description%' ";
  }

  if ($bandwidth ne "") {
    $cond .= "AND service.currentbw = $bandwidth ";
  }

  $command =~ s/COND/$cond/g;

  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
	$error = $dbh->errstr;
	return 0;
  }
  my $result = $sth->fetchall_arrayref;
  if ($result) {
	$error = $dbh->errstr;
	$sth->finish;
	return $result;
  } else {
	$error = $dbh->errstr;
	return 0;
  }
}
#End of code Added by Nisha for mVPN:06oct2009

sub search_service_evpl {
  my $serviceid = shift @_;
  my $accno = shift @_;
  my $description = shift @_;
  my $bandwidth = shift @_;
  my $productcode = shift @_;
  my $pollingtype = shift @_;
  my $costype = shift @_;
  my $switch_evpl_a = shift @_;
  my $switch_evpl_z = shift @_;
  my $switch_service_tpm = shift @_;

#When no filter condition is applied and productcode EVPL is selected

  my $cond = "";
  my $cond_a = "";
  my $cond_z = "";
  my $command = "";

if( ($pollingtype =~ /EVPL VLAN Service/) || ($pollingtype eq "") ) {

#When no filter condition is applied OR productcode EVPL is selected
#Chandini (changed all from class_5 to class_3 (gold to silver) in select queries)

  if( (($switch_evpl_a ne "")&& ($switch_evpl_z eq "")) && ($switch_service_tpm eq "")) {
  $command =  
    ("SELECT trim(trailing 'a' from service.serviceid) as serviceid, 'A' as end, accno, service.description, currentbw, productcodeid, routername, interface,  replace(aggregate_serviceid,'asw','sw'), reference.description, default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid ilike '%ec%a' COND_A UNION SELECT trim(trailing 'z' from service.serviceid) as serviceid, 'Z' as end, accno, service.description, currentbw, productcodeid, routername, interface, aggregate_serviceid, reference.description,  default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid ilike '%ec%z' COND_Z");
} elsif( (($switch_evpl_z ne "") && ($switch_evpl_a eq "")) && ($switch_service_tpm eq "") ) {
  $command = 
    ("SELECT trim(trailing 'a' from service.serviceid) as serviceid, 'A' as end, accno, service.description, currentbw, productcodeid, routername, interface, aggregate_serviceid, reference.description, default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime,service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid ilike '%ec%a' COND_A UNION SELECT trim(trailing 'z' from service.serviceid) as serviceid, 'Z' as end, accno, service.description, currentbw, productcodeid, routername, interface, replace(aggregate_serviceid,'zsw','sw'), reference.description,default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid ilike '%ec%z' COND_Z"); 
} elsif(($switch_evpl_z ne "") && ($switch_evpl_a ne "") && ($switch_service_tpm eq "") ) {
  $command = 
    ("SELECT trim(trailing 'a' from service.serviceid) as serviceid, 'A' as end, accno, service.description, currentbw, productcodeid, routername, interface, replace(aggregate_serviceid,'asw','sw'), reference.description, default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime,service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid like '%ec%a' COND_A UNION SELECT trim(trailing 'z' from service.serviceid) as serviceid, 'Z' as end, accno, service.description, currentbw, productcodeid, routername, interface, replace(aggregate_serviceid,'zsw','sw'), reference.description,default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid like '%ec%z' COND_Z");
} elsif(($switch_evpl_z eq "") && ($switch_evpl_a eq "") && ($switch_service_tpm eq "") )  {
#When no filter condition is applied or productcode EVPL is selected
  $command = 
	("SELECT trim(trailing 'a' from service.serviceid) as serviceid, 'A' as end, accno, service.description, currentbw, productcodeid, routername, interface, replace(aggregate_serviceid,'asw','sw'), reference.description, default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime,service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid like '%ec%a' COND_A  UNION SELECT trim(trailing 'z' from service.serviceid) as serviceid, 'Z' as end, accno, service.description, currentbw, productcodeid, routername, interface, replace(aggregate_serviceid,'zsw','sw'), reference.description,default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid like '%ec%z' COND_Z");
 }
} elsif($pollingtype =~ /EVPL Transparent Service/){

	if ($switch_service_tpm ne "") {
	  $command =   
	  ("SELECT trim(trailing 'a' from service.serviceid) as serviceid, 'A' as end, accno, service.description, currentbw, productcodeid, routername, interface, replace(aggregate_serviceid,'asw','sw'), reference.description, default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime,service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid like '%ec%a' COND_A  UNION   SELECT trim(trailing 'z' from service.serviceid) as serviceid, 'Z' as end, accno, service.description, currentbw, productcodeid, routername, interface, replace(aggregate_serviceid,'zsw','sw'), reference.description,default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid like '%ec%z' COND_Z");
	} elsif ($switch_service_tpm eq "") {
	  $command = 
	    ("SELECT trim(trailing 'a' from service.serviceid) as serviceid, 'A' as end, accno, service.description, currentbw, productcodeid, routername, interface,   replace(aggregate_serviceid,'asw','sw'), reference.description, default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime,service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid like '%ec%a' COND_A  
UNION SELECT trim(trailing 'z' from service.serviceid) as serviceid, 'Z' as end, accno, service.description, currentbw, productcodeid, routername, interface,  replace(aggregate_serviceid,'zsw','sw'), reference.description,default_rateacl, default_classmap,silver_rateacl, silver_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid like '%ec%z' COND_Z"); 
  	}
}

if ($productcode eq "") {
#When no filter condition is applied
  $command = 
	("SELECT trim(trailing 'a' from service.serviceid) as serviceid, 'A' as end, accno, service.description, currentbw, productcodeid, routername, interface, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, replace(aggregate_serviceid,'asw','sw'), master_serviceid, reference.description, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype  where service.serviceid ilike '%ec%a' AND service.productcodeid = 'EVPL' COND_A UNION SELECT trim(trailing 'z' from service.serviceid) as serviceid, 'Z' as end, accno, service.description, currentbw, productcodeid, routername,interface, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, replace(aggregate_serviceid,'zsw','sw'), master_serviceid, reference.description, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt,aend_jobid, zend_jobid, aend_jobno, zend_jobno  FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join reference  on reference.ref_val = service.pollingtype where service.serviceid ilike '%ec%z' AND service.productcodeid = 'EVPL' COND_Z");
} 

  if ($serviceid ne "") {
    $cond_a .= "AND service.serviceid ilike '%$serviceid%' ";
    $cond_z .= "AND service.serviceid ilike '%$serviceid%' ";
  }

  if ($accno ne "") {
    $cond_a .= "AND service.accno ilike '%$accno%' ";
    $cond_z .= "AND service.accno ilike '%$accno%' ";
  }

  if ($description ne "") {
    $cond_a .= "AND service.description ilike '%$description%' ";
    $cond_z .= "AND service.description ilike '%$description%' ";
  }

  if ($bandwidth ne "") {
    $cond_a .= "AND service.currentbw = '$bandwidth' ";
    $cond_z .= "AND service.currentbw = '$bandwidth' ";
  }

  if ($productcode ne "") {
	$productcode = "EVPL";
    $cond_a .= "AND service.productcodeid = '$productcode' ";
    $cond_z .= "AND service.productcodeid = '$productcode' ";
  }
  
  if($pollingtype ne "") {
   if($pollingtype =~ /EVPL VLAN Service/){
    $pollingtype = "VPM"; 
   } elsif ($pollingtype =~ /EVPL Transparent Service/){
    $pollingtype = "TPM";
   }
    $cond_a .= "AND service.pollingtype ilike '%$pollingtype%' ";
    $cond_z .= "AND service.pollingtype ilike '%$pollingtype%' ";
  }

  if($costype ne "") {
    $cond_a .= "AND service.costype ilike '%$costype%' ";
    $cond_z .= "AND service.costype ilike '%$costype%' ";
  }
  if( ($switch_evpl_a ne "") && ($switch_evpl_z ne "") ) {

    $cond_a .= "AND service.aggregate_serviceid ilike '%$switch_evpl_a%' ";
    $cond_z .= "AND service.aggregate_serviceid ilike '%$switch_evpl_z%' ";

  } elsif ( ($switch_evpl_a ne "") && ($switch_evpl_z eq "") ) {

    $cond_a .= "AND service.aggregate_serviceid ilike '%$switch_evpl_a%' ";
    $cond_z .= "AND service.aggregate_serviceid  not ilike '%$switch_evpl_z%' ";

  }  elsif ( ($switch_evpl_a eq "") && ($switch_evpl_z ne "") ) {

    $cond_a .= "AND service.aggregate_serviceid not ilike '%$switch_evpl_a%' ";
    $cond_z .= "AND service.aggregate_serviceid ilike '%$switch_evpl_z%' ";

  }

 if( ($switch_service_tpm ne "") ) {
    $cond_a .= "AND service.aggregate_serviceid ilike '%$switch_service_tpm%' ";
    $cond_z .= "AND service.aggregate_serviceid ilike '%$switch_service_tpm%' ";
  }

  $command =~ s/COND_A/$cond_a/g;
  $command =~ s/COND_Z/$cond_z/g;
  open (RE, ">/tmp/search.txt");   

        my $sth = $dbh->prepare($command);
        unless ($sth->execute) {
	    $error = $dbh->errstr;
	    return 0;
  	}

  	my $result = $sth->fetchall_arrayref;
  	if ($result) {
    	    $error = $dbh->errstr;
    	    $sth->finish;
    	    return $result;
  	} else {
    	    $error = $dbh->errstr;
    	    return 0;
  	}
}

############################################################
#Author: Karuna Ballal 
#Description: Sub routine to search services when product is EPL
#Input:serviceid, accno, description, bandwidth, productcode,
#epl_master, epl_aend_jobid, epl_zend_jobid, epl_aend_jobno,
#epl_zend_jobno
#Output: Search results in array reference $result
#############################################################
sub search_service_epl {
  my $serviceid = shift @_;
  my $accno = shift @_;
  my $description = shift @_;
  my $bandwidth = shift @_;
  my $productcode = shift @_;
  my $epl_master = shift @_;
  my $epl_aend_jobid = shift @_;
  my $epl_zend_jobid = shift @_;
  my $epl_aend_jobno = shift @_;
  my $epl_zend_jobno = shift @_;
  my $command = "";
  my $cond = "";

open (EPL_SEARCH,">/data1/tmp_log/searchservice.log");

  $command = ("SELECT service.serviceid, accno, service.description, currentbw, productcodeid, master_serviceid, billing_option.billing_option, service.billing_starttime, service.billing_endtime, aend_jobid, zend_jobid, aend_jobno, zend_jobno FROM service left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'EPL') COND");

  #add conditions to the query based on specified parameters
  if ($serviceid ne "") {
    $cond .= "AND service.serviceid ilike '%$serviceid%' ";
  }

  if ($accno ne "") {
    $cond .= "AND service.accno ilike '%$accno%' ";
  }

  if (($serviceid ne "") && ($accno ne "")) {
    $cond .= "AND service.serviceid ilike '%$serviceid%' AND service.accno ilike '%$accno%'";
  }
  if ($description ne "") {
    $cond .= "AND service.description ilike '%$description%' ";
  }

  if ($bandwidth ne "") {
    $cond .= "AND service.currentbw = $bandwidth ";
  }

  if($epl_master ne "") {
    $cond .= "AND service.master_serviceid ilike '%$epl_master%' ";
  }

  if($epl_aend_jobid ne "") {
    $cond .= "AND service.aend_jobid ilike '%$epl_aend_jobid%' ";
  }

  if($epl_zend_jobid ne "") {
    $cond .= "AND service.zend_jobid ilike '%$epl_zend_jobid%' ";
  }

  if($epl_aend_jobno ne "") {
    $cond .= "AND service.aend_jobno ilike '%$epl_aend_jobno%' ";
  }

  if($epl_zend_jobno ne "") {
    $cond .= "AND service.zend_jobno ilike '%$epl_zend_jobno%' ";
  }

  $command =~ s/COND/$cond/g;
#print EPL_SEARCH "command:$command\n";

  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
        $error = $dbh->errstr;
        return 0;
  }
  my $result = $sth->fetchall_arrayref;
  if ($result) {
        $error = $dbh->errstr;
        $sth->finish;
        return $result;
  } else {
        $error = $dbh->errstr;
        return 0;
  }

}


sub search_service {
  open (IPT3,">>/data1/tmp_log/searchserv.log");
  print IPT3 "DB Sid: $_[0]\t productcode:$_[4] $_[5]\n";
  #Added by karuna for IPTRANSIT
  my ($ipt_as, $ipt_bgpip, $ipt_irrd, $ipt_msid, $ipt_bha_routing, $ipt_bha_prefix, $ipt_bha_bgp, $ipt_prefix);
  my ($gia_as,$gia_irrd, $gia_aggregate, $gia_prefix, $gia_bgp, $gia_bha_routing, $gia_bha_prefix, $gia_bha_bgp);

  my $serviceid = shift @_;
  my $accno = shift @_;
  my $description = shift @_;
  my $bandwidth = shift @_;
  my $productcode = shift @_;
  if ($productcode =~ /IPTRANSIT/){
	$ipt_as = shift @_;
  	$ipt_irrd = shift @_;
  	$ipt_msid = shift @_;
	$ipt_prefix = shift @_;
  	$ipt_bgpip = shift @_;
	$ipt_bha_routing = shift @_;
	$ipt_bha_prefix = shift @_;
	$ipt_bha_bgp = shift @_;
  ##09-Nov-2010 IP Transit Ph5 - Karuna
  } elsif ($productcode =~ /GIA/){
	$gia_as = shift @_;
        $gia_irrd = shift @_;
        $gia_aggregate = shift @_;
	$gia_prefix = shift @_;
	$gia_bgp = shift @_;
	$gia_bha_routing = shift @_;
	$gia_bha_prefix = shift @_;
	$gia_bha_bgp = shift @_;
  } elsif ($productcode eq ""){
	shift @_;
	$gia_as = shift @_;
	$gia_irrd = shift @_;
  }
  ## --IPT5
	
  print IPT3 "DB:- ipt_as:$ipt_as\tipt_irrd:$ipt_irrd\tipt_msid:$ipt_msid\tipt_bgpip:$ipt_bgpip\n";
  print IPT3 "DB:- gia_as:$gia_as\tgia_irrd:$gia_irrd\tgia_aggregate:$gia_aggregate\tgia_prefix:$gia_prefix\tgia_bgp:$gia_bgp\tgia_bha_routing:$gia_bha_routing\tgia_bha_prefix:$gia_bha_prefix\tgia_bha_bgp:$gia_bha_bgp\n"; 

  my $pollingtype = shift @_;
  my $as = shift @_;
  my $irrd_obj = shift @_;
  my $aggregate_serviceid = shift @_;
  my $master_serviceid = shift @_;
  my $serviceid_alt = shift @_;
  my $aend_jobid = shift @_;
  my $zend_jobid = shift @_;
  my $aend_jobno = shift @_;
  my $zend_jobno = shift @_;  
  #Added by Karuna for EPL
  my $epl_master = shift @_;
  my $epl_aend_jobid = shift @_;
  my $epl_zend_jobid = shift @_;
  my $epl_aend_jobno = shift @_;
  my $epl_zend_jobno = shift @_;

  my ($cond, $cond_ipt) = "";
  my $routername = "";
  my $command_product="";
  my $command_product_count ="";
  my $command  ="";
  my ($command_v, $cmd_ipt) ="";

if($productcode ne "") {

	##++IPT5 - Karuna
	##Modified for Addendum 2
	if($productcode eq "GIA") {
		$command =
		#("SELECT serviceid, accno, description, link.bw, productcodeid, null, null, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, service.black_hole_routing, null, service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix,null, aggregate_serviceid, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid != 'GIA' AND productcodeid != 'MPLS' AND productcodeid != 'EVPL') AND service.productcodeid = 'GIA' 
		("SELECT serviceid, accno, description, link.bw, productcodeid, null, null, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, service.black_hole_routing, null, null, null, null, service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix, aggregate_serviceid, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid != 'GIA' AND productcodeid != 'MPLS' AND productcodeid != 'EVPL') AND service.productcodeid = 'GIA'
     		UNION
		SELECT service.serviceid, accno, service.description, link.bw, productcodeid, routername, interface, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, service.black_hole_routing, (select routername from routername where link.bha_routerid=routername.routerid), null, (select routername from routername where link.bha_routerid_bac=routername.routerid), null, service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix, aggregate_serviceid, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'GIA' OR productcodeid = 'MPLS') AND service.productcodeid = 'GIA' COND");
     		#SELECT service.serviceid, accno, service.description, link.bw, productcodeid, routername, interface, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, service.black_hole_routing, (select routername from routername where link.bha_routerid=routername.routerid), service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix,null, aggregate_serviceid, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'GIA' OR productcodeid = 'MPLS') AND service.productcodeid = 'GIA' COND"); 
	##--Addendum 2
	##--IPT5 - Karuna
	} else {
		#When any product is selected
  		print IPT3 "DB: PRODUCT SELECTED loop 1\n";
  		$command =  
    		("SELECT serviceid, accno, description, currentbw, productcodeid, null, null, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, master_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno FROM service left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid != 'GIA' AND productcodeid != 'MPLS' AND productcodeid != 'EVPL') COND
     		UNION
     		SELECT service.serviceid, accno, service.description, currentbw, productcodeid, routername, interface, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, master_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'GIA' OR productcodeid = 'MPLS') COND"
    		);
	}

} elsif ( ( ($productcode eq "")) && ( ($serviceid eq "") && ($accno eq "") && ($description eq "") && ($bandwidth eq "") && ($productcode eq "") && ($pollingtype eq "") && ($as eq "") && ($irrd_obj eq "") && ($aggregate_serviceid eq "") && ($master_serviceid eq "") && ($serviceid_alt eq "") && ($aend_jobid eq "") && ($zend_jobid eq "") && ($aend_jobno eq "") && ($zend_jobno eq "") ) ){

#Added MCAST rows by Nisha for mVPN: 05oct2009
#When no filter condition is applied
##09-Nov-2010 IP Transit Ph5 - Karuna
print IPT3 "elsif no prod ONE\n";
##Modified for Addendum 2
  $command = 
	("SELECT serviceid, accno, description, currentbw, productcodeid, null, null, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, master_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno, mcast_class5_in, mcast_class4_in, mcast_class4_out, mcast_class3_in, mcast_class3_out, mcast_class2_in, mcast_class2_out, mcast_class1_in, mcast_class1_out, mcast_class0_in, mcast_class0_out, service.black_hole_routing, null, null, null, null, service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix FROM service left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid != 'GIA' AND productcodeid != 'MPLS' AND productcodeid != 'EVPL' AND productcodeid != 'IPTRANSIT' AND productcodeid != 'ETHERNET') COND
	UNION 
	SELECT service.serviceid, accno, service.description, currentbw, productcodeid, routername, interface, as_no,bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, master_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno, mcast_class5_in, mcast_class4_in, mcast_class4_out, mcast_class3_in, mcast_class3_out, mcast_class2_in, mcast_class2_out, mcast_class1_in, mcast_class1_out, mcast_class0_in, mcast_class0_out, service.black_hole_routing, (select routername from routername where link.bha_routerid=routername.routerid), null, (select routername from routername where link.bha_routerid_bac=routername.routerid), null, service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'GIA' OR productcodeid = 'MPLS' OR productcodeid= 'IPTRANSIT' OR productcodeid = 'ETHERNET') COND");
##--Addendum 2
##--IPT5
} else {
#Added MCAST rows by Nisha for mVPN: 05oct2009
print IPT3 "else no prod TWO\n";
##Modified for Addendum 2
  $command =  
	("SELECT serviceid, accno, description, currentbw, productcodeid, null, null, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, master_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno, mcast_class5_in, mcast_class4_in, mcast_class4_out, mcast_class3_in, mcast_class3_out, mcast_class2_in, mcast_class2_out, mcast_class1_in, mcast_class1_out, mcast_class0_in, mcast_class0_out, service.black_hole_routing, null, null, null, null, service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix FROM service left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid != 'GIA' AND productcodeid != 'MPLS' AND productcodeid != 'EVPL' AND productcodeid != 'IPTRANSIT' AND productcodeid != 'ETHERNET') COND
	UNION
	SELECT service.serviceid,  accno, service.description, currentbw, productcodeid, routername, interface, as_no,bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, master_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno, mcast_class5_in, mcast_class4_in, mcast_class4_out, mcast_class3_in, mcast_class3_out, mcast_class2_in, mcast_class2_out, mcast_class1_in, mcast_class1_out, mcast_class0_in, mcast_class0_out, service.black_hole_routing, (select routername from routername where link.bha_routerid=routername.routerid), null, (select routername from routername where link.bha_routerid_bac=routername.routerid), null, service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'GIA' OR productcodeid = 'MPLS' OR productcodeid = 'IPTRANSIT' OR productcodeid = 'ETHERNET') COND");
##--Addendum 2
}

#When no filter condition is applied OR any product selected
#Added mcast columns for mVPN by Nisha
##Modified for Addendum 2
print IPT3 "no prod THREE\n";
 my $command_pid =
	("SELECT serviceid, accno, description, currentbw, productcodeid, null, null, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, master_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno, mcast_class5_in, mcast_class4_in, mcast_class4_out, mcast_class3_in, mcast_class3_out, mcast_class2_in, mcast_class2_out, mcast_class1_in, mcast_class1_out, mcast_class0_in, mcast_class0_out, service.black_hole_routing, null, null, null, null, service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix FROM service left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid != 'GIA' AND productcodeid != 'MPLS' AND productcodeid != 'VPLS' AND productcodeid != 'EVPL' AND productcodeid != 'ETHERNET' AND productcodeid != 'IPTRANSIT' AND productcodeid != 'ETHERNET') COND
	UNION
	SELECT service.serviceid, accno, service.description, currentbw, productcodeid, routername, interface, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, master_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt,aend_jobid, zend_jobid, aend_jobno, zend_jobno, mcast_class5_in, mcast_class4_in, mcast_class4_out, mcast_class3_in, mcast_class3_out, mcast_class2_in, mcast_class2_out, mcast_class1_in, mcast_class1_out, mcast_class0_in, mcast_class0_out, service.black_hole_routing, (select routername from routername where link.bha_routerid=routername.routerid), null, (select routername from routername where link.bha_routerid_bac=routername.routerid), null, service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'GIA' OR productcodeid = 'MPLS' OR productcodeid = 'VPLS' OR productcodeid = 'ETHERNET' OR productcodeid = 'IPTRANSIT'  OR productcodeid = 'ETHERNET') COND"
  );
##--Addendum 2

#When no filter condition is applied OR any product selected
  $command_v =
   ("SELECT service.serviceid, accno, service.description, currentbw, productcodeid, routername, interface, aggregate_serviceid,  master_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'VPLS') COND");

#For IP Transit services
##Modified for Addendum 2
#$cmd_ipt = ("SELECT service.serviceid, accno, service.description, bw, productcodeid, routername, interface, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null,service.black_hole_routing, (select routername from routername where link.bha_routerid=routername.routerid),service.bha_bgp_neighbor_ip,service.bha_auto_gen_prefix, null, master_serviceid,ipt_master.ipc_servid, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join ipt_master on ipt_master.master_sid = service.master_serviceid  where (productcodeid = 'IPTRANSIT') ");
$cmd_ipt = ("SELECT service.serviceid, accno, service.description, bw, productcodeid, routername, interface, as_no,bgp_neighbor_ip, auto_gen_prefix, irrd_object, null,service.black_hole_routing, (select routername from routername where link.bha_routerid=routername.routerid), null, (select routername from routername where link.bha_routerid_bac=routername.routerid), null, service.bha_bgp_neighbor_ip, service.bha_auto_gen_prefix, master_serviceid, ipt_master.ipc_servid, billing_option.billing_option, service.billing_starttime, service.billing_endtime FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid left outer join ipt_master on ipt_master.master_sid = service.master_serviceid where (productcodeid = 'IPTRANSIT') ");
##--Addendum 2

#print IPT3 "DB before cmd_ipt: $cmd_ipt\n";

if ($productcode =~ /IPTRANSIT/) {
print IPT3 "DB SEARCH set to null\n";
$pollingtype = $as = $irrd_obj = $aggregate_serviceid = "";
} 

  if ($serviceid ne "") {
    $cond .= "AND service.serviceid ilike '%$serviceid%' "; 
    $command_product="SELECT distinct productcodeid from service where serviceid ilike '%$serviceid%'"; 
    $command_product_count="SELECT COUNT(distinct productcodeid) from service where serviceid ilike '%$serviceid%'"; 
  }

  if ($accno ne "") {
    $cond .= "AND service.accno ilike '%$accno%' "; 
    $command_product="SELECT distinct productcodeid from service where accno ilike '%$accno%'";
    $command_product_count="SELECT COUNT(distinct productcodeid) from service where accno ilike '%$accno%'";
  }

  if (($serviceid ne "") && ($accno ne "")) {
    $cond .= "AND service.serviceid ilike '%$serviceid%' AND service.accno ilike '%$accno%'"; 
    $command_product="SELECT distinct productcodeid from service where serviceid ilike '%$serviceid%' AND accno ilike '%$accno%'"; 
    $command_product_count="SELECT COUNT(distinct productcodeid) from service where serviceid ilike '%$serviceid%' AND accno ilike '%$accno%'"; 
  }
  
  if ($description ne "") {
    $cond .= "AND service.description ilike '%$description%' "; 
    $command_product="SELECT distinct productcodeid from service where description ilike '%$description%'";
    $command_product_count="SELECT COUNT(distinct productcodeid) from service where description ilike '%$description%'";
  }

  ##Added on 29-Nov-2010 by Karuna for IPT5
  ##++IPT5
  if ($bandwidth ne "") {
	if(($productcode =~ /IPTRANSIT/) || ($productcode =~ /GIA/)){
		$cond .= "AND link.bw = $bandwidth ";
	} else {
    		$cond .= "AND service.currentbw = $bandwidth "; 
    		$command_product="SELECT distinct productcodeid from service where currentbw = $bandwidth"; 
    		$command_product_count="SELECT COUNT(distinct productcodeid) from service where currentbw = $bandwidth";
	}
  ##--IPT5
  }

  if ($productcode ne "") {
    $cond .= "AND service.productcodeid = '$productcode' ";
  }

  ##++IPT5
  #if ($as ne "") {
    #$cond .= "AND service.as_no = '$as' ";
  if ($gia_as ne "") {
	$cond .= "AND service.as_no = '$gia_as' ";
  }
  #if ($irrd_obj ne "") {
   # $cond .= "AND service.irrd_object ilike '%$irrd_obj%' ";
  if ($gia_irrd ne "") {
    $cond .= "AND service.irrd_object ilike '%$gia_irrd%' ";
  }
  if ($gia_aggregate ne "") {
    $cond .= "AND service.aggregate_serviceid ilike '%$gia_aggregate%' ";
  }
  if ($gia_prefix ne "0") {
    	if ($gia_prefix ne ""){
    		$cond .= "AND service.auto_gen_prefix ilike '$gia_prefix' ";
	}
  }
  if ($gia_bgp ne "") {
    $cond .= "AND service.bgp_neighbor_ip like '$gia_bgp' ";
  }
  if ($gia_bha_routing ne "0") { 
	if($gia_bha_routing ne ""){
    		$cond .= "AND service.black_hole_routing ilike '$gia_bha_routing' ";
	}
  }
  if ($gia_bha_prefix ne "0") {
	if ($gia_bha_prefix ne "") {
		$cond .= "AND service.bha_auto_gen_prefix ilike '$gia_bha_prefix' ";
	}
  }
  if ($gia_bha_bgp ne "") {
    $cond .= "AND service.bha_bgp_neighbor_ip like '$gia_bha_bgp' ";
  }
  ##--IPT5

  if ($serviceid_alt ne "") {
    $cond .= "AND service.serviceid_alt ilike '%$serviceid_alt%' ";
  }

  if($aend_jobid ne "") {
    $cond .= "AND service.aend_jobid ilike '%$aend_jobid%' ";
  }

  if($zend_jobid ne "") {
    $cond .= "AND service.zend_jobid ilike '%$zend_jobid%' ";
  }

  if($aend_jobno ne "") {
    $cond .= "AND service.aend_jobno ilike '%$aend_jobno%' ";
  }

  if($zend_jobno ne "") {
    $cond .= "AND service.zend_jobno ilike '%$zend_jobno%' ";
  }
  if($pollingtype ne "") {
    $cond .= "AND service.pollingtype ilike '%$pollingtype%' ";
  }
 
  if($aggregate_serviceid ne "") {
    $cond .= "AND service.aggregate_serviceid ilike '%$aggregate_serviceid%' ";
  }

  if($master_serviceid ne "") {
    $cond .= "AND service.master_serviceid ilike '%$master_serviceid%' ";
  }

  #Added by Karuna for EPL
  if($epl_master ne "") {
    $cond .= "AND service.master_serviceid ilike '%$master_serviceid%' ";
  }

  if($epl_aend_jobid ne "") {
    $cond .= "AND service.aend_jobid ilike '%$aend_jobid%' ";
  }

  if($epl_zend_jobid ne "") {
    $cond .= "AND service.zend_jobid ilike '%$zend_jobid%' ";
  }

  if($epl_aend_jobno ne "") {
    $cond .= "AND service.aend_jobno ilike '%$aend_jobno%' ";
  }

  if($epl_zend_jobno ne "") {
    $cond .= "AND service.zend_jobno ilike '%$zend_jobno%' ";
  }

  #Added by Karuna for IP TRANSIT
  if($ipt_as ne "") {
	$cond_ipt .= "AND service.as_no = '$ipt_as' ";
  }
  print IPT3 "ipt_irrd:$ipt_irrd\n";
  if($ipt_irrd ne "") {
	$cond_ipt .= "AND service.irrd_object ilike '%$ipt_irrd%' ";
  }
  if($ipt_msid ne "") {
	$cond_ipt .= "AND service.master_serviceid ilike '%$ipt_msid%' ";
  }
  if($ipt_bgpip ne "") {
	$cond_ipt .= "AND service.bgp_neighbor_ip like '$ipt_bgpip' ";
  }
  ##added on 29-Nov-2010
  if ($ipt_bha_routing ne "0") {
        if($ipt_bha_routing ne ""){
                $cond .= "AND service.black_hole_routing ilike '$ipt_bha_routing' ";
        }
  }
  if ($ipt_bha_prefix ne "0") {
        if ($ipt_bha_prefix ne "") {
                $cond .= "AND service.bha_auto_gen_prefix ilike '$ipt_bha_prefix' ";
        }
  }
  if ($ipt_prefix ne "0"){
 	if ($ipt_prefix ne "") {
    		$cond .= "AND service.auto_gen_prefix ilike '$ipt_prefix' ";
  	}
  }
  ##--IPT5

  $command =~ s/COND/$cond/g; 
  $command_v =~ s/COND/$cond/g; 
  $command_pid =~ s/COND/$cond/g;
  $command_product_count =~ s/COND/$cond/g;
  #print IPT3 "cond: $cond_ipt\n";
  #$cmd_ipt = s/CONDI/$cond_ipt/g;
  $cmd_ipt = $cmd_ipt.$cond_ipt.$cond;
  print IPT3 "DB before query cmd_ipt:$cmd_ipt\n";
  print IPT3 "Product Code:$productcode\n";
  #print IPT3 "command_v:$command_v\n";
#open (ME,">/tmp/cms_search_db");
  
  #Added by Karuna for IP Transit
  if ($productcode =~ /IPTRANSIT/) {
	print IPT3 " entered IPT query loop\n";
	my $sth = $dbh->prepare($cmd_ipt);
	unless ($sth->execute) {
            $error = $dbh->errstr;
	    print IPT3 "DB after cmd_ipt error\n";
            return 0;
        }
        my $result = $sth->fetchall_arrayref;
        if ($result) {
            $error = $dbh->errstr;
            $sth->finish;
	    print IPT3 "DB before return success\n";
            return $result;
        } else {
            $error = $dbh->errstr;
	    print IPT3 "DB before return 0\n";
            return 0;
        }
  }

  my $sth = $dbh->prepare($command_product);
  print IPT3  "DB SEARCH command_product=$command_product\n";
  unless ($sth->execute) {
    $error = $dbh->errstr;
  }
  my $result = $sth->fetchall_arrayref;
  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
  }
  print IPT3 "End of $command_product";
	

  my $sth1 = $dbh->prepare($command_product_count);
 #print ME "\n command_product_count=$command_product_count\n";
  unless ($sth1->execute) {
    $error = $dbh->errstr;
  }
  my $result_count = $sth1->fetchall_arrayref;
  if ($result_count) {
    $error = $dbh->errstr;
    $sth1->finish;
  }

  my $res_count= $$result_count[0];
  my $res_count1= $$res_count[0];
  print IPT3 "res_count :$res_count,res_count1:$res_count1\n";
	

  my $res_tmp="";
  my $tmp ="";
  my $Flag_v=0;
  my $Flag_o=0;
  for (my $x =0; $x < $res_count1; $x++){
	$tmp = $$result[$x];
	$res_tmp=$$tmp[0];
	#print ME "product code compare $res_tmp\n";
	print IPT3 "res_tmp:$res_tmp\n";
	if($res_tmp =~ /VPLS/){
	    $Flag_v = 1;
	    #$Flag_o = 0;
	}elsif($res_tmp eq ""){
        }else{
                $Flag_o = 1;
	}
  }
  print IPT3 "Flag_v:$Flag_v,Flag_o:$Flag_o\n";
  if ($productcode =~ /VPLS/){
#When productcode VPLS is selected
  	my $sth = $dbh->prepare($command_v);

  	unless ($sth->execute) {
    	    $error = $dbh->errstr;
    	    return 0;
  	}

  	my $result = $sth->fetchall_arrayref;
  	if ($result) {
    	    $error = $dbh->errstr;
    	    $sth->finish;
	    my $result_tmp = $$result[0];
	    for (my $i=0;$i<20;$i++){
	    }
    	    return $result;
  	} else {
    	    $error = $dbh->errstr;
    	    return 0;
  	}

  } elsif($Flag_v == 1 && $Flag_o == 1){
	my $sth = $dbh->prepare($command_pid);
        print IPT3 "Executing command:\n$command_pid\n";
  	unless ($sth->execute) {
  	    $error = $dbh->errstr;
   	    return 0;
  	}
	my $result = $sth->fetchall_arrayref;
  	if ($result) {
    	    $error = $dbh->errstr;
    	    $sth->finish;
     	    return $result;
  	} else {
    	    $error = $dbh->errstr;
    	    return 0;
  	}
  }elsif($Flag_v == 1){
  	my $sth = $dbh->prepare($command_v);
  	unless ($sth->execute) {
    	    $error = $dbh->errstr;
    	    return 0;
  	}

  	my $result = $sth->fetchall_arrayref;
  	if ($result) {
    	    $error = $dbh->errstr;
    	    $sth->finish;
    	    return $result;
  	} else {
    	    $error = $dbh->errstr;
    	    return 0;
  	}

  }else{
#When no filter condition is applied OR any product selected
    	print IPT3 "DB SEARCH ELSE NO FILTER\ncommand:$command\n";
	my $sth = $dbh->prepare($command);
	unless ($sth->execute) {
    	    $error = $dbh->errstr;
   	    return 0;
  	}

  	my $result = $sth->fetchall_arrayref;
  	if ($result) {
    	    $error = $dbh->errstr;
    	    $sth->finish;
    	    return $result;
  	} else {
    	    $error = $dbh->errstr;
    	    return 0;
  	}
  }
}

######################################################################
#Author: Stavan Shah
#Description: Sub routine to search services when product is ETHERNET
#Input:serviceid, accno, description, bandwidth, productcode
#Output: Search results in array reference $result
######################################################################
sub search_service_ethernet {
  my $serviceid = shift @_;
  my $accno = shift @_;
  my $description = shift @_;
  my $bandwidth = shift @_;
  my $productcode = shift @_;
  my $command = "";
  my $cond = "";

  $command = ("SELECT service.serviceid, accno, service.description, currentbw, productcodeid, routername, interface FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'ETHERNET') COND");

  #add conditions to the query based on specified parameters
  if ($serviceid ne "") {
    $cond .= "AND service.serviceid ilike '%$serviceid%' ";
  }
  if ($accno ne "") {
    $cond .= "AND service.accno ilike '%$accno%' ";
  }
  if (($serviceid ne "") && ($accno ne "")) {
    $cond .= "AND service.serviceid ilike '%$serviceid%' AND service.accno ilike '%$accno%'";
  }
  if ($description ne "") {
    $cond .= "AND service.description ilike '%$description%' ";
  }
  if ($bandwidth ne "") {
    $cond .= "AND service.currentbw = $bandwidth ";
  }

  $command =~ s/COND/$cond/g;
  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
        $error = $dbh->errstr;
        return 0;
  }
  my $result = $sth->fetchall_arrayref;
  if ($result) {
        $error = $dbh->errstr;
        $sth->finish;
        return $result;
  } else {
        $error = $dbh->errstr;
        return 0;
  }
}




sub get_qosservice {
  my $sth = $dbh->prepare("select serviceid, gold_classmap, gold_rateacl, silver_classmap, silver_rateacl, default_classmap, default_rateacl from service
                           where gold_classmap is not null or gold_rateacl is not null or silver_classmap is not null or silver_rateacl is not null or default_classmap is not null or default_rateacl is not null
                           order by serviceid");
  $sth->execute;
  my $array_ref = $sth->fetchall_arrayref();
  return $array_ref;
}

sub get_aut_num_obj {
  my $sth = $dbh->prepare("SELECT as_no FROM service group by as_no");
  $sth->execute;

  my $result = $sth->fetchall_arrayref();
  $sth->finish;

  my %aut_num = ();

  for my $i (0 .. $#{$result}) {
     $sth = $dbh->prepare("SELECT description FROM service where as_no = '$result->[$i][0]'");
     $sth->execute;
     my $res = $sth->fetchall_arrayref();
     $sth->finish;
     if ($#{$res} >= 0) {
        $aut_num{$result->[$i][0]} = $res->[0][0] ;
     }
  }
  return %aut_num;
}

sub get_customer_as {
  my $sql_command = "select distinct as_no from service where as_no != ''";
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
 
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}    

sub get_irrd_object {
  my $sql_command = "select distinct irrd_object, as_no from service where irrd_object != '' and as_no != ''";
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}
 
sub get_gold {
        my ($serviceid) = @_;
 
        my $sth = $dbh->prepare("select distinct gold_classmap, gold_rateacl from service
                                where serviceid='$serviceid' and (gold_classmap<>'' or gold_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}  

sub get_silver {
        my ($serviceid) = @_;

        my $sth = $dbh->prepare("select distinct silver_classmap, silver_rateacl from service
                                where serviceid='$serviceid' and (silver_classmap<>'' or silver_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}

sub get_default {
        my ($serviceid) = @_;

        my $sth = $dbh->prepare("select distinct default_classmap, default_rateacl from service
                                where serviceid='$serviceid' and (default_classmap<>'' or default_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}

sub get_class1 {
        my ($serviceid) = @_;

        my $sth = $dbh->prepare("select distinct class1_classmap, class1_rateacl from service
                                where serviceid='$serviceid' and (class1_classmap<>'' or class1_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}

sub get_class2 {
        my ($serviceid) = @_;
  
        my $sth = $dbh->prepare("select distinct class2_classmap, class2_rateacl from service
                                where serviceid='$serviceid' and (class2_classmap<>'' or class2_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}
  
sub get_class3 {
        my ($serviceid) = @_;
  
        my $sth = $dbh->prepare("select distinct class3_classmap, class3_rateacl from service
                                where serviceid='$serviceid' and (class3_classmap<>'' or class3_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}

sub get_class4 {
        my ($serviceid) = @_;
  
        my $sth = $dbh->prepare("select distinct class4_classmap, class4_rateacl from service
                                where serviceid='$serviceid' and (class4_classmap<>'' or class4_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}

sub get_billing_option {
        my $opt = shift @_;
  
        my $tmpsql = "SELECT billing_optionid, billing_option FROM billing_option ";
        if ($opt !~ /^all$/i) {  # "all" option not specified
                # ignore id 0
                $tmpsql .= "where billing_optionid != 0 ";
        }
        $tmpsql .= "order by billing_optionid";
        #my $sth = $dbh->prepare("SELECT billing_optionid, billing_option FROM billing_option order by billing_optionid");
        my $sth = $dbh->prepare($tmpsql);
  
        unless ($sth->execute) {  
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub update_billing {
        if (scalar @_ != 22) {
                my $tmperr = "update_billing: Incorrect number of parameters.";
                $error = (length($error)!=0) ? $error."<br>"."$tmperr" : $tmperr;
                return 0;
        }
        my $serviceid_raw = shift @_;
        my $serviceid = $dbh_tr->quote($serviceid_raw);
        my $accno = $dbh_tr->quote(shift @_);
        my $billable = shift @_;
        my $billstartdate_year = shift @_;
        my $billstartdate_month = shift @_;
        my $billstartdate_day = shift @_;
        my $billstarttime_hour = shift @_;
        my $billstarttime_minute = shift @_;
        my $billstarttime_second = shift @_;
        my $billenddate_year = shift @_;
        my $billenddate_month = shift @_;
        my $billenddate_day = shift @_;
        my $billendtime_hour = shift @_;
        my $billendtime_minute = shift @_;
        my $billendtime_second = shift @_;
        my $productcode = $dbh_tr->quote(shift @_);
        my $gold_classmap = $dbh_tr->quote(shift @_);
        my $gold_rateacl = $dbh_tr->quote(shift @_);
        my $silver_classmap = $dbh_tr->quote(shift @_);
        my $silver_rateacl = $dbh_tr->quote(shift @_);
        my $default_classmap = $dbh_tr->quote(shift @_);
        my $default_rateacl = $dbh_tr->quote(shift @_);

        my $actiontype;
        my $sth = $dbh->prepare ("SELECT serviceid FROM billing WHERE serviceid ilike ?");
        $sth->execute($serviceid_raw);
        my $r = $sth->fetchrow_hashref;
  	#++for EVPL changed tghe condition
        #if($$r{serviceid} =~ /^$serviceid_raw$/i)
        if ($productcode =~ /\'EVPL\'/){
                if($$r{serviceid} eq $serviceid_raw) {
                        #--EVPL end
                        #return 0;
                        $actiontype = 'U';  #update
                } else {
                        $actiontype = 'A';  #add
		}
        } else {
                $actiontype = 'U';
        }

        # format dates in ISO-8601 standard
        # (YYYY-MM-DD HH:MM:SS[+/-]XX where XX is the difference from UTC)

        my $billstartdt = 'null';
        if ($billstartdate_year !~ /-/) {
        	$billstartdt =
                $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $billstartdate_year, $billstartdate_month, $billstartdate_day,
                        $billstarttime_hour, $billstarttime_minute, $billstarttime_second));
        }

        my $billenddt = 'null';
        if ($billenddate_year !~ /-/) {
        	$billenddt =
                $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                              $billenddate_year, $billenddate_month, $billenddate_day,
                              $billendtime_hour, $billendtime_minute, $billendtime_second));
        }

        my @now = gmtime;
        my $timestamp = $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                                1900+$now[5], $now[4]+1, $now[3],
                                $now[2], $now[1], $now[0]));

	if ($billable == '') {
		$billable = 2;  # default billing option = 2, non usage billing
	}

        my $success = 1;

        if ($actiontype =~ /^U$/) {
                $sth = $dbh_tr->prepare
                ("DELETE FROM billing where serviceid = $serviceid");
                $success &&= $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
        }
        if ($success) {
                my $xx ="
                INSERT INTO billing(timestamp, serviceid, accno, billing_optionid, billing_starttime, billing_endtime,
                        productcodeid,
                        gold_classmap, gold_rateacl,
                        silver_classmap, silver_rateacl,
                        default_classmap, default_rateacl
                        )
                VALUES($timestamp, $serviceid, $accno, $billable, $billstartdt, $billenddt,
                        $productcode,
                        $gold_classmap, $gold_rateacl,
                        $silver_classmap, $silver_rateacl,
                        $default_classmap, $default_rateacl
                        )
                ";
		open (XX, ">/tmp/leo_debug~CMS_SERVICE_DB.pm~update_billing");
		print XX $xx;
		close (XX);
		$sth = $dbh_tr->prepare
		("$xx");
                $success &&= $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
        }
        #my $result = $sth->execute;
        #$error = $dbh_tr->errstr;
        #return $result;
        return $success;
}

sub search_billing {
  my $serviceid = shift @_;
  my $accno = shift @_;
  my $description = shift @_;
  my $bandwidth = shift @_;

 
  my $command =
	("select  
	a.timestamp, date_part('epoch', a.timestamp) as timestamp_in_uxtime,
	a.serviceid, a.accno, a.billing_optionid, b.billing_option,
	a.billing_starttime, date_part('epoch', a.billing_starttime) as billing_starttime_in_uxtime,
	a.billing_endtime, date_part('epoch', a.billing_endtime) as billing_endtime_in_uxtime,
	a.productcodeid,
	a.gold_classmap, a.gold_rateacl, a.silver_classmap, a.silver_rateacl, a.default_classmap, a.default_rateacl,
	d.type 
	from billing a
	left outer join billing_option b on a.billing_optionid = b.billing_optionid
	left outer join link c on a.serviceid = c.serviceid
	left outer join link_type d on c.typeid = d.typeid ");

  if ($serviceid ne "") {
    $command .= "AND serviceid ilike '%$serviceid%' ";
  }

  if ($accno ne "") {
    $command .= "AND accno ilike '%$accno%' ";
  }
 
  if ($description ne "") {
    $command .= "AND description ilike '%$description%' ";
  }

  if ($bandwidth ne "") {
    $command .= "AND currentbw = $bandwidth ";
  }

  $command =~ s/AND/WHERE/; # change first AND


  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }

}

#
# Added for new qos polling - only used for getting polling type
# rchew@maettr.bpa.nu, 13 August 2006
# Pre: serviceid
# Post: 
sub get_pollingtype_sid {
	my $serviceid = shift @_;
  	my $command =  "SELECT pollingtype FROM service where serviceid = '$serviceid'";
	my $sth = $dbh->prepare($command);
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	my $result = $sth->fetchall_arrayref;
	if ($result) {
		$error = $dbh->errstr;
		$sth->finish;
		return $result;
	} else {
		$error = $dbh->errstr;
		return 0;
	}
}

sub get_costype_sid {
        my $serviceid = shift @_;
  
  my $command =
    "SELECT costype
     FROM service where serviceid = '$serviceid'";
  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  
  my $result = $sth->fetchall_arrayref;
  
  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
  
  
}
sub get_ser_capacity {
        my $serviceid = shift @_;
  my $command =
    "SELECT bw
     FROM link where serviceid = '$serviceid'";
  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  my $result = $sth->fetchall_arrayref;
  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}



# This is to get all classes
sub get_cos_all {
        my ($serviceid) = shift @_;
  
        #++mVPN
        my ($mVPN_flag) = shift @_;
        my $query;

        if($mVPN_flag == 1){
    #            $query ="select distinct mcast_class0_in,mcast_class0_out,mcast_class1_in,mcast_class1_out,mcast_class2_in,mcast_class2_out,mcast_class3_in,mcast_class3_out,mcast_class4_in,mcast_class4_out,mcast_class5_in,mcast_class5_in from service where serviceid = '$serviceid'";
	#modified query for mVPN new requirement - Karuna
                $query ="select distinct mcast_class0_in,mcast_class0_out,mcast_class1_in,mcast_class1_out,mcast_class2_in,mcast_class2_out,mcast_class3_in,mcast_class3_out,mcast_class4_in,mcast_class4_out,gold_classmap,mcast_class5_in from service where serviceid = '$serviceid'";
        }else{
                $query = "select distinct default_classmap, default_rateacl, class1_classmap, class1_rateacl, class2_classmap, class2_rateacl, silver_classmap, silver_rateacl, class4_classmap, class4_rateacl, gold_classmap, gold_rateacl from service where serviceid='$serviceid'";
        }
        #--mVPN

        my %ret;  # $ret{classmap}{classname} = value / $ret{rateacl}{classname} = value
  
        # need to get class 0 (default), class 1, class 2, class 3 (silver), class 4 and class 5 (gold)
  
        #my $sth = $dbh->prepare("select distinct default_classmap, default_rateacl, class1_classmap, class1_rateacl, class2_classmap, class2_rateacl, silver_classmap, silver_rateacl, class4_classmap, class4_rateacl, gold_classmap, gold_rateacl from service where serviceid='$serviceid'");
	my $sth = $dbh->prepare($query);  
        $sth->execute;
        my $res = $sth->fetchall_arrayref();
  
        foreach my $d2 (@$res) {
                $ret{classmap}{class0} = $$d2[0];
                $ret{rateacl}{class0} = $$d2[1];
                $ret{classmap}{class1} = $$d2[2];
                $ret{rateacl}{class1} = $$d2[3];
                $ret{classmap}{class2} = $$d2[4];
                $ret{rateacl}{class2} = $$d2[5];
                $ret{classmap}{class3} = $$d2[6];
                $ret{rateacl}{class3} = $$d2[7];
                $ret{classmap}{class4} = $$d2[8];
                $ret{rateacl}{class4} = $$d2[9];
                $ret{classmap}{class5} = $$d2[10];
                $ret{rateacl}{class5} = $$d2[11];
        }
  
        return \%ret;

}

#This function will get the product code for the aggregate service id passed.-VPLS aggregate CR
sub get_productcode_agg_service {
	my $sid = shift @_;
	my $command = "SELECT distinct(productcodeid) FROM service WHERE aggregate_serviceid = '$sid'";
	my $sth = $dbh->prepare($command);
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	my $result = $sth->fetchall_arrayref;
	$error = $dbh->errstr;
	$sth->finish;
	
	
	return $result;
}
#++mVPN
sub check_mVPN_service {
        my $sid = shift @_;
        my $command = "SELECT mcast_option from service where serviceid = '$sid'";
        my $sth = $dbh->prepare($command);
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
        my $result = $sth->fetchrow_hashref;
        my $retVal = $$result{mcast_option};
        $sth->finish;
        return $retVal;
}
#--mVPN


#EPL
sub get_job_notes {
        my $sid = shift @_;
        my $command = "SELECT aend_jobno, zend_jobno from service where serviceid = '$sid' or  master_serviceid = '$sid'";
        my $sth = $dbh->prepare($command);
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
        #my $result = $sth->fetchall_arrayref({});
        my @result = $sth->fetchrow_array;
        $sth->finish;
        return @result;
}

sub get_service_bw{
        my $serviceid = shift @_;
  my $command =
    "SELECT currentbw
     FROM service where serviceid = '$serviceid'";
  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  my $result = $sth->fetchall_arrayref;
  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

#Added for EPL Prashant
sub get_prodcode_master_service {
        my $sid = shift @_;
        my $command = "SELECT distinct(productcodeid) FROM service WHERE master_serviceid = '$sid'";
        my $sth = $dbh->prepare($command);
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
        my $result = $sth->fetchall_arrayref;
        $error = $dbh->errstr;
        $sth->finish;


        return $result;
}

sub get_template_for_account{

        my $accno= shift @_;
        my $command = "select template_path from wholesaler where accno = (select wholesaler_accno from customer where accno ='$accno')";
        my $sth = $dbh->prepare($command);
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
        #my $result = $sth->fetchall_arrayref;
        my @result = $sth->fetchrow_array;
        $error = $dbh->errstr;
        $sth->finish;

        return @result;

}

sub get_master_service_bw{

        my $sid = shift @_;
        my $command = "select sum(currentbw) from service where master_serviceid = '$sid'";
        my $sth = $dbh->prepare($command);
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
        my @result = $sth->fetchrow_array;
        $error = $dbh->errstr;
        $sth->finish;

        return @result;

}
#--EPL

sub get_last_asn{
	if ($debug){print IPT_DBG "DB entered get_last_asn\n";}
	my $sid = shift @_;
	if ($debug){print IPT_DBG "DB sid:$sid\n";}
        my $command = "select as_no from service where as_no in (select as_no from service where serviceid = '$sid')";
	my $sth = $dbh->prepare($command);
        unless ($sth->execute) {
                $error = $dbh->errstr;
		if ($debug){print IPT_DBG "DB error:$error\n";}
                return 0;
        }
        my $row = $sth->rows();
	if ($debug){print IPT_DBG "DB row:$row\n";}
	my $retstr = "";
	if ($row == 1){
		$retstr = "Yes";
		if ($debug){print IPT_DBG "DB retstr:$retstr\n";}
	} else {
		$retstr = "No";
		if ($debug){print IPT_DBG "DB retstr:$retstr\n";}
	}
	return $retstr;
}

##++IPT5
#**************************************************************************************************
#Name		:record_service_details
#Author		:KARUNA BALLAL
#Description	:update record of service details before deletion of service, for routing reference
#input		:service id, account no
#output		:
#modification history	:
#KARUNA BALLAL		02Feb2010	Modified to change status of routes to deleted
#**************************************************************************************************
sub record_service_details{
	my $sid = $dbh->quote(shift @_);
	my $accno = (shift @_);
	my $last_asn = shift (@_);
	if($debug) {print IPT_DBG "DB Entered record_service_details\n";}
	if($debug) {print IPT_DBG "DB record_service_details sid:$sid\taccno:$accno\n last asn: $last_asn\n";}
	my $serv_query1 = $dbh->prepare("SELECT as_no, auto_gen_prefix, bgp_neighbor_ip, black_hole_routing, bha_auto_gen_prefix, bha_bgp_neighbor_ip FROM service WHERE serviceid = $sid and accno = $accno AND ((productcodeid = 'IPTRANSIT') OR (productcodeid = 'GIA'))AND as_no not like ''");
	if($debug) {print IPT_DBG "DB serv_query1:SELECT as_no, auto_gen_prefix, bgp_neighbor_ip, black_hole_routing, bha_auto_gen_prefix, bha_bgp_neighbor_ip FROM service WHERE serviceid = $sid and accno = $accno AND ((productcodeid = 'IPTRANSIT') OR (productcodeid = 'GIA'))\n";}
	$serv_query1->execute();
	my @service_details = $serv_query1->fetchrow_array;
	$serv_query1->finish;
	my $as_no = $service_details[0];
	my $auto_gen_prefix = $service_details[1];
	my $bgp_neighbor_ip = $service_details[2];
	my $black_hole_routing = $service_details[3];
	my $bha_auto_gen_prefix = $service_details[4];
	my $bha_bgp_neighbor_ip = $service_details[5];
	if($debug) {print IPT_DBG "DB record_service_details as_no:$as_no\tauto_gen_prefix:$auto_gen_prefix\tbgp_neighbor_ip:$bgp_neighbor_ip\tblack_hole_routing:$black_hole_routing\tbha_auto_gen_prefix:$bha_auto_gen_prefix\tbha_bgp_neighbor_ip:$bha_bgp_neighbor_ip\n";}

#++ Addendum 2	
	my $serv_query2="";
	if ($last_asn eq "Yes"){
		#$serv_query2 = $dbh->prepare("UPDATE deleted_links_routing SET as_no= '$as_no', bgp_neighbor_ip = '$bgp_neighbor_ip', bha_bgp_neighbor_ip = '$bha_bgp_neighbor_ip',black_hole_routing = '$black_hole_routing', auto_gen_prefix = 'Enable', bha_auto_gen_prefix = '$bha_auto_gen_prefix', service_deleted_timestamp = CURRENT_TIMESTAMP, status = 2, bha_status = 2 WHERE serviceid in (SELECT DISTINCT ON (serviceid) serviceid FROM deleted_links_routing WHERE serviceid=$sid ORDER BY serviceid, link_deleted_timestamp DESC)");
		$serv_query2 = $dbh->prepare("UPDATE deleted_links_routing SET as_no= '$as_no', bgp_neighbor_ip = '$bgp_neighbor_ip', bha_bgp_neighbor_ip = '$bha_bgp_neighbor_ip',black_hole_routing = '$black_hole_routing', auto_gen_prefix = 'Enable', bha_auto_gen_prefix = '$bha_auto_gen_prefix', service_deleted_timestamp = CURRENT_TIMESTAMP, status = 2, bha_status = 2, bha_status_bac=2 WHERE serviceid in (SELECT DISTINCT ON (serviceid) serviceid FROM deleted_links_routing WHERE serviceid=$sid ORDER BY serviceid, link_deleted_timestamp DESC)");
	} else { 
		#$serv_query2 = $dbh->prepare("UPDATE deleted_links_routing SET as_no= '$as_no', bgp_neighbor_ip = '$bgp_neighbor_ip', bha_bgp_neighbor_ip = '$bha_bgp_neighbor_ip', black_hole_routing = '$black_hole_routing', auto_gen_prefix = 'Enable', bha_auto_gen_prefix = '$bha_auto_gen_prefix', service_deleted_timestamp = CURRENT_TIMESTAMP, status=0, bha_status = 0 WHERE serviceid in (SELECT DISTINCT ON (serviceid) serviceid FROM deleted_links_routing WHERE serviceid=$sid ORDER BY serviceid, link_deleted_timestamp DESC)");
		#CRQ000000004421:15-11-11 Chandini
		$serv_query2 = $dbh->prepare("UPDATE deleted_links_routing SET as_no= '$as_no', bgp_neighbor_ip = '$bgp_neighbor_ip', bha_bgp_neighbor_ip = '$bha_bgp_neighbor_ip', black_hole_routing = '$black_hole_routing', auto_gen_prefix = 'Enable', bha_auto_gen_prefix = '$bha_auto_gen_prefix', service_deleted_timestamp = CURRENT_TIMESTAMP, status=1, bha_status = 1, bha_status_bac=1 WHERE serviceid in (SELECT DISTINCT ON (serviceid) serviceid FROM deleted_links_routing WHERE serviceid=$sid ORDER BY serviceid, link_deleted_timestamp DESC)");
		#$serv_query2 = $dbh->prepare("UPDATE deleted_links_routing SET as_no= '$as_no', bgp_neighbor_ip = '$bgp_neighbor_ip', bha_bgp_neighbor_ip = '$bha_bgp_neighbor_ip', black_hole_routing = '$black_hole_routing', auto_gen_prefix = 'Enable', bha_auto_gen_prefix = '$bha_auto_gen_prefix', service_deleted_timestamp = CURRENT_TIMESTAMP, status=0, bha_status = 0, bha_status_bac=0 WHERE serviceid in (SELECT DISTINCT ON (serviceid) serviceid FROM deleted_links_routing WHERE serviceid=$sid ORDER BY serviceid, link_deleted_timestamp DESC)");
	}
	if($debug) {print IPT_DBG "DB record_service_details serv_query2:UPDATE deleted_links_routing SET as_no= '$as_no', bgp_neighbor_ip = '$bgp_neighbor_ip', bha_bgp_neighbor_ip = '$bha_bgp_neighbor_ip', black_hole_routing= '$black_hole_routing', auto_gen_prefix = '$auto_gen_prefix', bha_auto_gen_prefix = '$bha_auto_gen_prefix', service_deleted_timestamp = CURRENT_TIMESTAMP, status=0, bha_status = 0, bha_status_bac = 0 WHERE serviceid in (SELECT DISTINCT ON (serviceid) serviceid FROM deleted_links_routing WHERE serviceid=$sid ORDER BY serviceid, link_deleted_timestamp DESC)\n";}
	$serv_query2->execute();
	$error = $dbh->errstr;
	if($debug) {print IPT_DBG "DB record_service_details error:$error\n";}
	$serv_query2->finish;
	
	if ($last_asn eq "Yes"){
		#Added on 02feb2010
		#my $serv_query3 = $dbh->prepare("UPDATE routing_queue SET status = '0', action ='2', bha_status = '0', time_entered= current_timestamp WHERE as_no like '$as_no' and action = '1'");
		my $serv_query3 = $dbh->prepare("UPDATE routing_queue SET status = '0', action ='2', bha_status = '3', backup_bha_status = '3', time_entered= current_timestamp WHERE as_no like '$as_no' and action = '1'");
#--Addendum 2
		$serv_query3->execute();
		$error = $dbh->errstr;
		if($debug) {print IPT_DBG "DB record_service_details update routing_queue error:$error\n";}
		if($debug) {print IPT_DBG "As no was: $as_no \n";}
		$serv_query3->finish;
	}
}

#**************************************************************************************************
#Name           :update_deleted_service_status
#Description	:Update the status of the deleted service/link once uploaded on the router
#Author		:KARUNA BALLAL for IPT5
#Inout		:
#Output		:
#***************************************************************************************************

sub update_deleted_service_status {
	my $serviceid = shift (@_);
	my $sth = $dbh->prepare("SELECT serviceid from deleted_links_routing where serviceid ='$serviceid'");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                if ($debug){print IPT_DBG "DB error:$error\n";}
                return "0";
        }
	my $row_num = $sth->rows();
	if ($debug){print IPT_DBG "DB row:$row_num\n";}
	if ($row_num > 0){
		#my $update_db_query = $dbh->prepare("UPDATE deleted_links_routing SET status='1' WHERE serviceid='$serviceid'");
		my $update_db_query = $dbh->prepare("UPDATE deleted_links_routing SET status='1' WHERE serviceid='$serviceid' and status not like 1");
		$update_db_query->execute();
		$error = $dbh->errstr;
		if($debug) {print IPT_DBG "DB update_deleted_service_status update error:$error\n";}
		$update_db_query->finish();
		if ($error) {
			return "0";
		} else {
			return "1";
		}
	} else {
		return "2";
	} 
	
}

#Addendum_2 ##Chandini
sub update_deleted_service_status_bha {
        my $serviceid = shift (@_);
        my $router_num = shift (@_);
        my $sth = $dbh->prepare("SELECT serviceid from deleted_links_routing where serviceid ='$serviceid'");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                if ($debug){print IPT_DBG "DB error:$error\n";}
                return "0";
        }
        my $row_num = $sth->rows();
        if ($debug){print IPT_DBG "DB row:$row_num\n";}
        if ($row_num > 0){
		my $update_db_query;
		if ($router_num == 0) {
			$update_db_query = $dbh->prepare("UPDATE deleted_links_routing SET bha_status='1' WHERE serviceid='$serviceid'");
		} else {
			$update_db_query = $dbh->prepare("UPDATE deleted_links_routing SET bha_status_bac='1' WHERE serviceid='$serviceid'");
		}

                $update_db_query->execute();
                $error = $dbh->errstr;
                if($debug) {print IPT_DBG "DB update_deleted_service_status update error:$error\n";}
                $update_db_query->finish();
                if ($error) {
                        return "0";
                } else {
                        return "1";
                }
        } else {
                return "2";
        }

}
##--IPT5

## IP Transit Addendum 2 ##
sub search_asnumber {
	if ($debug){print IPT_DBG "DB inside search_asnumber\n";}
	my $as_num = shift (@_);
	if ($as_num eq ""){ 
	if ($debug){print IPT_DBG "DB as_num eq null\n";}
	my $gia_query = $dbh->prepare("SELECT service.serviceid, accno, link.bw, productcodeid, routername, interface, as_no, master_serviceid, ipt_master.ipc_servid, null, null  FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join ipt_master on ipt_master.master_sid = service.master_serviceid where (productcodeid = 'GIA') and as_no is not null and as_no not like ''");
	unless ($gia_query->execute) {
                my $error = $dbh->errstr;
		if ($debug){print IPT_DBG "DB error:$error\n";}
                return 0;
        }
        $gia_query->execute();
        my $gia_result = $gia_query->fetchall_arrayref;
        $gia_query->finish;

	my $ipc_query = $dbh->prepare("select distinct d.serviceid as IPT, a.customer, (select link.bw from link where link.serviceid=d.serviceid), 'IPTRANSIT', (select routername.routername FROM routername where link.routerid = routername.routerid and link.serviceid=d.serviceid),(select interface from link where link.serviceid=d.serviceid),as_nos, c.master_sid, a.ipc_servid, current_committed_rate, dir_traffic, committed_stdt, dir_effective_date from ipc_china a join (select ipc_servid , max(committed_stdt) as lastupdate from ipc_china group by ipc_servid) b on (a.ipc_servid = b.ipc_servid and a.committed_stdt = b.lastupdate) left outer join (select master_sid,ipc_servid from ipt_master)c on (a.ipc_servid=c.ipc_servid) left outer join (select serviceid, master_serviceid from service) d on (c.master_sid=d.master_serviceid)");
	unless ($ipc_query->execute) {
                my $error = $dbh->errstr;
		if ($debug){print IPT_DBG "DB error:$error\n";}
                return 0;
        }
        $ipc_query->execute();
        my $ipc_result = $ipc_query->fetchall_arrayref;
        $ipc_query->finish;	

	my @result = (@$gia_result, @$ipc_result);
	my $ref_result = (\@result);

	return ($ref_result);
	}
	my ($main_result, $ipm_result);
	
	my $gia_query = $dbh->prepare("SELECT service.serviceid, accno, link.bw, productcodeid, routername, interface, as_no, master_serviceid, ipt_master.ipc_servid, null, null  FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join ipt_master on ipt_master.master_sid = service.master_serviceid where (productcodeid = 'GIA') and as_no = $as_num");
        unless ($gia_query->execute) {
                my $error = $dbh->errstr;
                return 0;
        }
        $gia_query->execute();
        my $gia_result = $gia_query->fetchall_arrayref;
        $gia_query->finish;

	my $sth_ipc = $dbh->prepare("select DISTINCT ON (ipc_servid) ipc_servid,customer,current_committed_rate,dir_traffic,as_nos,dir_effective_date from ipc_china where as_nos=$as_num order by ipc_servid,dir_effective_date DESC");
	unless ($sth_ipc->execute) {
		my $error = $dbh->errstr;
		return 0;
	}
	$sth_ipc->execute();
	my @ipc_result = $sth_ipc->fetchrow_array;
	$sth_ipc->finish;
	my $ipc = $ipc_result[0];
	my $accno_ipc = $ipc_result[1];
	my $committed_rate = $ipc_result[2];
	my $dir_traff = $ipc_result[3];
	
	if ($debug){print IPT_DBG "DB ipc:$ipc\taccno_ipc:$accno_ipc\tcommitted_rate:$committed_rate\tdir_traff:$dir_traff\n";}
	my $sth = $dbh->prepare("select serviceid from service where as_no = $as_num and productcodeid='IPTRANSIT'");
	unless ($sth->execute) {
		my $error = $dbh->errstr;
		return 0;
	}
	$sth->execute();
	my $row = $sth->rows();
	if ($debug){print IPT_DBG "DB number of IPT services: $row\n";}
	my @result;
	my $ipt_flag=0;
	if ($row > 0){
		$ipt_flag = 1;
	} else {
		$ipt_flag = 2;
	}
	$sth->finish;

	my @ipt_gia;
	if ($ipt_flag == 1){
		if ($debug){print IPT_DBG "DB ipt_flag=1\n";}
		my $main_query = $dbh->prepare("SELECT service.serviceid, accno, link.bw, productcodeid, routername, interface, as_no, master_serviceid, ipt_master.ipc_servid  FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join ipt_master on ipt_master.master_sid = service.master_serviceid where (productcodeid = 'IPTRANSIT') and as_no = $as_num");
		if ($debug){print IPT_DBG "DB main_query:$main_query\n";}
		unless ($main_query->execute) {
			my $error = $dbh->errstr;
			if ($debug){print IPT_DBG "DB error:$error\n";}
			return 0;
		}
		$main_query->execute();
		$main_result = $main_query->fetchall_arrayref;
		if ($debug){print IPT_DBG "DB main_result:$main_result\n";}
		$main_query->finish;
		my $length = ($#{$main_result});
		if ($debug){print IPT_DBG "DB length:$length\n";}

		for my $x(0..$length){
			$main_result->[$x][9] = $committed_rate;
			$main_result->[$x][10] = $dir_traff;

		}
		my $ipm_query = $dbh->prepare("select null, ipc_china.customer, null, 'IPTRANSIT', null, null, null, ipt_master.master_sid, ipt_master.ipc_servid from ipt_master left join ipc_china on ipc_china.ipc_servid=ipt_master.ipc_servid WHERE ipc_china.as_nos = $as_num and ipt_master.master_sid NOT IN(select master_serviceid from service where as_no = $as_num and productcodeid = 'IPTRANSIT')");
                unless ($ipm_query->execute) {
                        my $error = $dbh->errstr;
			if ($debug){print IPT_DBG "DB error:$error\n";}
                        return 0;
                }
                $ipm_query->execute();
                my $rows_ipm = $ipm_query->rows();
		if ($debug){print IPT_DBG "DB number of IPM services: $rows_ipm\n";}
                if ($rows_ipm > 0){
			if ($debug){print IPT_DBG "DB number of IPM services: $rows_ipm\n";}	
			$ipm_result = $ipm_query->fetchall_arrayref;
			$ipm_query->finish;
			my $length_ipm = ($#{$ipm_result});
			print "length:$length_ipm\n";

			for my $x(0..$length_ipm){
				$ipm_result->[$x][6] = $as_num;
				$ipm_result->[$x][9] = $committed_rate;
				$ipm_result->[$x][10] = $dir_traff;
			}
		@ipt_gia = (@$gia_result, @$ipm_result, @$main_result);
		} else {
			@ipt_gia = (@$gia_result, @$main_result);
		}
		my $ref_ipt_gia = \@ipt_gia;
		if ($debug){print IPT_DBG "DB ref:$ref_ipt_gia\n";}
		return ($ref_ipt_gia);
	} elsif ($ipt_flag == 2){
		if ($debug){print IPT_DBG "DB ipt_flag=2\n";}
		my $ipm_query = $dbh->prepare("select null, ipc_china.customer, null, 'IPTRANSIT', null, null, null, ipt_master.master_sid, ipt_master.ipc_servid from ipt_master left join ipc_china on ipc_china.ipc_servid=ipt_master.ipc_servid WHERE ipc_china.as_nos = $as_num");
		unless ($ipm_query->execute) {
                        my $error = $dbh->errstr;
                        return 0;
                }
                $ipm_query->execute();
		my $rows = $ipm_query->rows();
		if ($rows > 0){
		my $ipm_result = $ipm_query->fetchall_arrayref;
		$ipm_query->finish;
		my $length = ($#{$ipm_result});
		print "length:$length\n";

		for my $x(0..$length){
                        $ipm_result->[$x][6] = $as_num;
                        $ipm_result->[$x][9] = $committed_rate;
                        $ipm_result->[$x][10] = $dir_traff;
		}
		my @ipm_gia = (@$gia_result, @$ipm_result);
		my $ref_ipm_gia = \@ipm_gia;
		return ($ref_ipm_gia);
		} else {
			my $only_ipc = $dbh->prepare("select DISTINCT ON (ipc_servid) null, customer, null, productcode, null, null, as_nos, null, ipc_servid,current_committed_rate,dir_traffic,dir_effective_date from ipc_china where as_nos=$as_num order by ipc_servid,dir_effective_date DESC");
			unless ($only_ipc->execute) {
				my $error = $dbh->errstr;
				return 0;
			}
			$only_ipc->execute();
			my $ipc_result = $only_ipc->fetchall_arrayref;
			$only_ipc->finish;
			my @ipc_gia = (@$gia_result, @$ipc_result);
			my $ref_ipc_gia = \@ipc_gia;
			return ($ref_ipc_gia);
		}
	}
	#return $main_result;

}
	

1;
