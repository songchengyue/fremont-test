
# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
#
# $Id: CMS_LINKS.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $
###################################################################
# File        : CMS_LINKS.pm
# Description : Passes parameters and calls the functions in CMS_LINKS.pm file.
# Parameters to be passed and usage : linkid,serviceid,typeid,routername,
#                                     interface,bandwidth,description.
# Output of the script
# Input File  :
# Output File :
# Author      : PRADEEP THANRAJ,KETAKI THOMBARE (Infosys Tech. Ltd., Bangalore )
# Started On  : 13 AUG 2009
# Modification History :
#  Date               Name     		            Change/Description
# ------------------------------------------------------------------
# 14Aug2009    PRADEEP THANRAJ,KETAKI THOMBARE     Baseline for EVPL.
# 14Aug2009    PRADEEP THANRAJ,KETAKI THOMBARE     Added for EVPL.
# 13Oct2009    Nisha Sasindran			   Modified for mVPN - service validation for add link
# 12-Nov-2009  Sayantan Dutta  			   Added user_log sub routine for tracking links
# 01-jun-2011  Varun Yadav                         Added for Addendum-2 BHA Requirment. 
####################################################################


package CMS_LINKS;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_LINKS_DB;
use CMS_DB;

our @ISA = qw (Exporter);
my $retVal;

our $error;
my $debug = 1;
#uncomment to disable debug logs and vice versa
#$debug = 0;
if ($debug){open (IPT,">>/data1/tmp_log/links.log");}

# ++ Added user_log sub routine for tracking links - Sayantan dutta - 12-Nov-2009
sub user_log {

	my $login = '';
	$login = $CMS::html_values{'username'}{value};
	open(USER_LOG,">>/usr/local/www/wanchai/cms/userlogs/CMS.$login.link.log");

	my $date_time = `date "+%a %h %d %H-%M-%S UTC %Y"`;
	my $ops = shift @_;
	chomp($date_time);
	print USER_LOG ">>[Date = $date_time] ";
	print USER_LOG "[OPS = $ops] ";
	print USER_LOG "[LID = $CMS::html_values{linkid}{value}] ";
	print USER_LOG "[SID = $CMS::html_values{serviceid}{value}] ";
	
	if(($ops =~ /Add Link/)||($ops =~ /Edit Link/)) {
		if ($CMS::html_values{typeid}{value} == "1" ) {
				print USER_LOG "[Type = CORE] ";
		}
		if ($CMS::html_values{typeid}{value} == "2" ) {
				print USER_LOG "[Type = TRUNK] ";
		}
		if ($CMS::html_values{typeid}{value} == "3" ) {
				print USER_LOG "[Type = PEER] ";
		}
		if ($CMS::html_values{typeid}{value} == "4" ) {
				print USER_LOG "[Type = TRANSIT] ";
		}
		if ($CMS::html_values{typeid}{value} == "5" ) {
				print USER_LOG "[Type = LAN] ";
		}
		if ($CMS::html_values{typeid}{value} == "6" ) {
				print USER_LOG "[Type = INTFCE] ";
		}
		if ($CMS::html_values{typeid}{value} == "7" ) {
				print USER_LOG "[Type = ATM] ";
		}
		if ($CMS::html_values{typeid}{value} == "101" ) {
				print USER_LOG "[Type = CUST] ";
		}
		if ($CMS::html_values{typeid}{value} == "201" ) {
				print USER_LOG "[Type = DEMO] ";
		}
		print USER_LOG "[BW = $CMS::html_values{bandwidth}{value}] ";
		if($CMS::html_values{serviceid}{value} =~ /.*\[.*\].*ec.*/){
			print USER_LOG "[Router(A end) = $CMS::html_values{routerid}{value}] ";
			print USER_LOG "[Interface(A end) = $CMS::html_values{interface}{value}] ";
			print USER_LOG "[Description(A end) = $CMS::html_values{description}{value}] ";
			print USER_LOG "[Router(Z end) = $CMS::html_values{routerid_z}{value}] ";
			print USER_LOG "[Interface(Z end) = $CMS::html_values{interface_z}{value}] ";
			print USER_LOG "[Description(Z end) = $CMS::html_values{description_z}{value}] ";
		}
		else {
			print USER_LOG "[Router = $CMS::html_values{routerid}{value}] ";
			print USER_LOG "[Interface = $CMS::html_values{interface}{value}] ";
			print USER_LOG "[Description = $CMS::html_values{description}{value}] ";
		}
	}
	print USER_LOG "\n";
	close (USER_LOG);
}
# ++ Added user_log sub routine for tracking links - Sayantan dutta - 12-Nov-2009

sub start {
  
	if ($debug){print IPT "Entered PM start fn\n";}
  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} =~ /cancel/i))) {
    # default to view_current_outages
     
    CMS::clear_html_values();
    $CMS::html_values{subsystem}{value} = "links";
    $CMS::html_values{command}{value} = "add_link";
    
  }

  if (defined $CMS::html_values{stage}{value}) {
    unless (CMS::check_screen($CMS::html_values{current_screen}{value})){
      $CMS::html_values{stage}{value}--;
    }
  }

  if ($CMS::html_values{command}{value} eq "add_link") {

      	$CMS::html_values{linkid}{value} =~ tr/A-Z/a-z/;
      	$CMS::html_values{serviceid}{value} =~ tr/A-Z/a-z/;
	#++EVPL
	# Removing brackets and spaces for service id other than EVPL
      	if( $CMS::html_values{serviceid}{value} !~ /.*\[.*\].*ec.*/) { 
      	 	$CMS::html_values{serviceid}{value} =~ s/( |\[|\])//g;
	 	$CMS::html_values{linkid}{value} =~ s/( |\[|\])//g;
      	}    
	#--EVPL
     	if (not defined ($CMS::html_values{stage}{value})) {
      	  # first stage
	  #++EVPL
	  my $linktypes = CMS_LINKS_DB::get_link_types ($CMS::html_values{linkid}{value}, $CMS::html_values{serviceid}{value});
	  my $routerids = CMS_ROUTERS_DB::get_router("ALL");
       	  foreach my $router (@$routerids){
            my $string = join (", ", @$router);
            $string =~ s/, $//g;

            $router = [$$router[0], $string];
	  }

       	  $CMS::html_values{typeid}{options} = $linktypes;
       	  $CMS::html_values{routerid}{options} = $routerids;
       	  $CMS::html_values{routerid_z}{options} = $routerids;
	  ##Karuna - new implementation for Ph5 
	  ##++IPT5
	  my $bha_routerids = CMS_LINKS_DB::fetch_routers_bha();
	  foreach my $bha_router (@$bha_routerids){
	  	my $bha_string = join (", ", @$bha_router);
		$bha_string =~ s/, $//g;
		$bha_router = [$$bha_router[0], $bha_string];
          }
	  $CMS::html_values{bha_router}{options} = $bha_routerids;
         ##Varun Yadav-Addendum-2 BHA Requirment.
         ##++Addendum2 
	 my $bha_routeridsbac = CMS_LINKS_DB::fetch_routers_bha();
              foreach my $bha_routerbac (@$bha_routeridsbac){
                    my $bha_string = join (", ", @$bha_routerbac);
                   $bha_string =~ s/, $//g;
                  $bha_routerbac = [$$bha_routerbac[0], $bha_string];
              }
              $CMS::html_values{bha_routerbac}{options} = $bha_routeridsbac;
         ##--Addendum2
         ##--IPT5
	  #--EVPL
       	  CMS::output_html("links_add");
     	  # Passing values to database.
    	} else {
	  #++EVPL
	  #check whether the router and port combination exists in DB for both the a end and z end
	  my $chklink = CMS_LINKS_DB::check_existing_link($CMS::html_values{routerid}{value}, $CMS::html_values{interface}{value}, $CMS::html_values{linkid}{value}, 1);
	  my $chklink_z=1;
	  if( $CMS::html_values{serviceid}{value} =~ /.*\[.*\].*ec.*/ && $CMS::html_values{interface_z}{value} ){
	    $chklink_z= CMS_LINKS_DB::check_existing_link($CMS::html_values{routerid_z}{value},$CMS::html_values{interface_z}{value}, $CMS::html_values{linkid}{value}, 1);
	  }
	  #++EVPL
	  #fetching all values to be populated in the screen
	  my $linktypes = CMS_LINKS_DB::get_link_types ($CMS::html_values{linkid}{value}, $CMS::html_values{serviceid}{value});
	  my $routerids = CMS_ROUTERS_DB::get_router("ALL");
	  foreach my $router (@$routerids){
		my $string = join (", ", @$router);
		$string =~ s/, $//g;

		$router = [$$router[0], $string];
	  }

	  $CMS::html_values{typeid}{options} = $linktypes;
	  #$CMS::html_values{typeid_z}{options} = $linktypes;
	  $CMS::html_values{routerid}{options} = $routerids;
	  $CMS::html_values{routerid_z}{options} = $routerids;

	  ##Karuna - new implementation for Ph5 
	  ##++IPT5
	  my $bha_routerids = CMS_LINKS_DB::fetch_routers_bha();
	  foreach my $bha_router (@$bha_routerids){
	  	my $bha_string = join (", ", @$bha_router);
		$bha_string =~ s/, $//g;
		$bha_router = [$$bha_router[0], $bha_string];
          }
	  $CMS::html_values{bha_router}{options} = $bha_routerids ; 
          
         ##Varun Yadav-Addendum-2 BHA Requirment.
         ##++Addendum2 
	  my $bha_routeridsbac = CMS_LINKS_DB::fetch_routers_bha();
              foreach my $bha_routerbac (@$bha_routeridsbac){
                    my $bha_string = join (", ", @$bha_routerbac);
                   $bha_string =~ s/, $//g;
                  $bha_routerbac = [$$bha_routerbac[0], $bha_string];
              }
              $CMS::html_values{bha_routerbac}{options} = $bha_routeridsbac;
         ##--Addendum2
	 ##--IPT5

	  my ($servid,$linkid, $linkid_z,$servid_z);
          # Checking for router port whether its assigned or not.	
          if ($chklink != 1 ) {
            	CMS::add_error("Failed to add link.");
                if($CMS::html_values{serviceid}{value} =~ /.*\[.*\].*ec.*/){
                       CMS::add_error("A End Router port already exists.");
            	#CMS::output_html("links_add");
                }else{
                        CMS::add_error("Router port already exists.");
            	#CMS::output_html("links_add");
                }
            	CMS::output_html("links_add");
             	return;		
	  }
	  if( $chklink_z !=1 ){
		CMS::add_error("Failed to add link.");
		CMS::add_error("Z End Router port already exists.");
		CMS::output_html("links_add");
		return;
	  }		
	
	  $servid = $CMS::html_values{serviceid}{value};
	  $linkid = $CMS::html_values{linkid}{value};
	  #Checking for EVPL related service and replacing sw with asw and zsw in serviceid and appending a and z to linkid.
	  if ($servid =~ /.*\[.*\].*ec.*sw$/){
	    	$servid_z=$servid;	
           	$servid_z =~ s/sw/zsw/;
           	$servid =~ s/sw/asw/;
           	$linkid_z = $linkid."z";
           	$linkid .="a";

	  }elsif($servid =~ /.*\[.*\].*ec.*/){
	   	$servid_z = $servid."z";	
	   	$servid .= "a";
	   	$linkid_z = $linkid."z";
	   	$linkid .="a";	
	  } 	
	  my $addlink;
	  my @router_ipt = split(/\,/,($CMS::html_values{routerid_ipt}{value}));
	  my $router_ipt = $router_ipt[0];
	  my @bha_router = split(/\,/,($CMS::html_values{bha_router}{value}));
	  my $bha_router = $bha_router[0];
	 ##++Addendum 2 BHA Requirment. 
	  my @bha_routerbac = split(/\,/,($CMS::html_values{bha_routerbac}{value}));
	  my $bha_routerbac = $bha_routerbac[0] ;
         ##--Addendum 2 BHA Requirment.
	  if ($debug){ print IPT "PM add routerid:$CMS::html_values{routerid}{value}\tbha_router:$bha_router\n";}
	  ###++IPT5
	  # Passing values for A End(EVPL) and for other links.
	  if($CMS::html_values{interface}{value}){
		##++IPT5
		if($CMS::html_values{bha_router}{value}){
			$addlink = CMS_LINKS_DB::add_link(
			$linkid, $servid,
			$CMS::html_values{typeid}{value},
			$CMS::html_values{routerid}{value},
			$CMS::html_values{interface}{value},
                        $CMS::html_values{bandwidth}{value},
			$CMS::html_values{description}{value},
			$bha_router,
			$CMS::html_values{bha_description}{value},
                ##++Addendum 2 BHA Requirment.
                        $bha_routerbac,
			$CMS::html_values{bha_descriptionbac}{value});
                        
                ##--Addendum 2 BHA Requirment.
		##--IPT5
		} else {
	   		$addlink = CMS_LINKS_DB::add_link
                   	($linkid, $servid,
	                $CMS::html_values{typeid}{value},
                    	$CMS::html_values{routerid}{value},
                    	$CMS::html_values{interface}{value},
                    	$CMS::html_values{bandwidth}{value},
                    	$CMS::html_values{description}{value});
	  	}
	  }
	  #Passing values for Z End(EVPL).
	  if($linkid_z && $CMS::html_values{interface_z}{value}){
	 	$addlink = CMS_LINKS_DB::add_link(
                    $linkid_z,$servid_z,
		    $CMS::html_values{typeid}{value},
                    $CMS::html_values{routerid_z}{value},
                    $CMS::html_values{interface_z}{value},
                    $CMS::html_values{bandwidth}{value},
                    $CMS::html_values{description_z}{value});
	  }
	  #--EVPL
	  if ($addlink){      
		#CR-84 Juniper IPVPN Report 
		my $rtr = $CMS::html_values{routerid}{value};
		my $junos_update = CMS_LINKS_DB::update_junos_service($servid,$rtr);
		
		# ++ Added for User Log after adding service - Sayantan dutta - 12-Nov-2009
		user_log "Add Link";
		# -- Added for User Log after adding service - Sayantan dutta - 12-Nov-2009
	  	##Karuna - new implementation for Ph5 
	  	##++IPT5
	  	my $bha_routerids = CMS_LINKS_DB::fetch_routers_bha();
	  	foreach my $bha_router (@$bha_routerids){
	  		my $bha_string = join (", ", @$bha_router);
			$bha_string =~ s/, $//g;
			$bha_router = [$$bha_router[0], $bha_string];
          	}
	  	$CMS::html_values{bha_router}{options} = $bha_routerids ;
                 
            ##Varun Yadav-Addendum-2 BHA Requirment.
            ##++Addendum2 
          	my $bha_routeridsbac = CMS_LINKS_DB::fetch_routers_bha();
                foreach my $bha_routerbac (@$bha_routeridsbac){
                    my $bha_string = join (", ", @$bha_routerbac);
                   $bha_string =~ s/, $//g;
                  $bha_routerbac = [$$bha_routerbac[0], $bha_string];
               }
                $CMS::html_values{bha_routerbac}{options} = $bha_routeridsbac;
            ##--Addendum2
	  	##--IPT5
			
		CMS::add_error("Successfully added link.");
                CMS::output_html("links_add");
	  } else {
#Added by Nisha for mVPN
		my $err = $CMS_LINKS_DB::error;
	    if($err =~ /^Invalid/){
		CMS::add_error("Failed to add link.");
		CMS::add_error("Invalid Service.");
	    } else {
#End of code Added by Nisha for mVPN
		if ($debug) {print IPT "PM ADD error:$err\n";}
		CMS::add_error("Failed to add link.");
		CMS::add_error("Link already exists in database.");
	    }
		CMS::output_html("links_add");
          }
     }

  } elsif ($CMS::html_values{command}{value} eq "edit_link") {
	        open (DEBUG1,">/tmp/prad_editlink");
        #++EVPL
        # Removing brackets and spaces for service id other than EVPL
        print DEBUG1 "service id ----> $CMS::html_values{serviceid}{value}\n";
      if( $CMS::html_values{serviceid}{value} !~ /.*\[.*\].*ec.*/) {
print DEBUG11 "Inside \n";
	$CMS::html_values{linkid}{value} =~ s/( |\[|\])//g;
         $CMS::html_values{serviceid}{value} =~ s/( |\[|\])//g;
      }
        print DEBUG1 "service id ----> $CMS::html_values{serviceid}{value}\n";
        #--EVPL

      $CMS::html_values{linkid}{value} =~ tr/A-Z/a-z/;
      $CMS::html_values{serviceid}{value} =~ tr/A-Z/a-z/;

     if (not defined ($CMS::html_values{stage}{value})) {
      # first stage
       CMS::output_html("links_edit");
     } elsif ($CMS::html_values{stage}{value} == 2) {
	#++EVPL
	my $servid =$CMS::html_values{serviceid}{value};
	my $linkid =$CMS::html_values{linkid}{value};
        my ($linkid_z,$servid_z);
	#Checking for EVPL serviceid and replacing serviceid with asw and zsw also appending a and z to linkid.
        if ($servid =~ /.*\[.*\].*ec.*sw$/){
            $servid_z=$servid;
           $servid_z =~ s/sw/zsw/;
           $servid =~ s/sw/asw/;
           $linkid_z = $linkid."z";
           $linkid .="a";

        }elsif($servid =~ /.*\[.*\].*ec.*/){
           $servid_z = $servid."z";

           $servid .= "a";
           $linkid_z = $linkid."z";
           $linkid .="a";
        }
	print DEBUG1 "servid--->$servid----servid_z--->$servid_z-----linkid------>$linkid------linkid_z----->$linkid_z---\n";
	my $result = CMS_LINKS_DB::get_link($linkid,$servid);
	print DEBUG1 "got values for a end link ---> $result\n";
	my $result_z;
	if($linkid_z){
	 $result_z = CMS_LINKS_DB::get_link($linkid_z,$servid_z);
	print DEBUG1 "got values for z end link ---> $result_z\n";
	}

	if(!$result && !$result_z){
             CMS::add_error("Unable to retrieve details for that link.");
	     CMS::add_error("Linkid/Serviceid not present in database");
              CMS::output_html("links_edit");
		return;
	}
	my $linktypes = CMS_LINKS_DB::get_link_types ($CMS::html_values{linkid}{value}, $CMS::html_values{serviceid}{value}); 
	my $rourternames = CMS_ROUTERS_DB::get_router("ALL");

  	##Karuna - new implementation for Ph5 
  	##++IPT5
  	my $bha_routerids = CMS_LINKS_DB::fetch_routers_bha();
  	foreach my $bha_router (@$bha_routerids){
  		my $bha_string = join (", ", @$bha_router);
		$bha_string =~ s/, $//g;
		$bha_router = [$$bha_router[0], $bha_string];
       	 }
  	$CMS::html_values{bha_router}{options} = $bha_routerids;
        ## Varun Yadav - Addendum 2 BHA Requirment.
        ##++ Addendum 2
	my $bha_routeridsbac = CMS_LINKS_DB::fetch_routers_bha();
              foreach my $bha_routerbac (@$bha_routeridsbac){
                    my $bha_string = join (", ", @$bha_routerbac);
                   $bha_string =~ s/, $//g;
                  $bha_routerbac = [$$bha_routerbac[0], $bha_string];
              }
              $CMS::html_values{bha_routerbac}{options} = $bha_routeridsbac;
         ##--Addendum 2
  	##--IPT5

	 foreach my $router (@$rourternames){
	   my $string = join (", ", @$router);
	   $string =~ s/, $//g;
	   
	   $router = [$$router[0], $string];
	 }
	#getting values for dropdown.
  	$CMS::html_values{typeid}{options} = $linktypes;	
	$CMS::html_values{routerid}{options} =$rourternames;
         $CMS::html_values{routerid_z}{options} = $rourternames;

	##++IPT5
	if($result){
	 ($CMS::html_values{typeid}{value},
	  $CMS::html_values{routerid}{value},
	  $CMS::html_values{interface}{value},
	  $CMS::html_values{bandwidth}{value},
          $CMS::html_values{description}{value},
	  $CMS::html_values{bha_router}{value},
	  $CMS::html_values{bha_description}{value},
          ##++Addendum-2 BHA Requirment.
          $CMS::html_values{bha_routerbac}{value},
          $CMS::html_values{bha_descriptionbac}{value}
          ##--Addendum-2 BHA Requirment.
	  ) = @$result;
	}
	##--IPT5
	 if($result_z){
	 print DEBUG1 "fetching values for zend---> @$result_z[0]\n";
	 ($CMS::html_values{typeid}{value},
          $CMS::html_values{routerid_z}{value},
          $CMS::html_values{interface_z}{value},
          $CMS::html_values{bandwidth}{value},
          $CMS::html_values{description_z}{value}) = @$result_z;
	  }
	   CMS::output_html("links_edit2");
	

     } elsif ($CMS::html_values{stage}{value} == 3) {
	#++EVPL
        my $servid = $CMS::html_values{serviceid}{value};
        my $linkid = $CMS::html_values{linkid}{value};
        my ($linkid_z,$servid_z);
	#Checking serviceid for square brackets,ec and ending with sw.
        if ($servid =~ /.*\[.*\].*ec.*sw$/){
            $servid_z=$servid;
           $servid_z =~ s/sw/zsw/;
           $servid =~ s/sw/asw/;
           $linkid_z = $linkid."z";
           $linkid .="a";

	#Checking serviceid for square brackets and ec.
        }elsif($servid =~ /.*\[.*\].*ec.*/){
           $servid_z = $servid."z";

           $servid .= "a";
           $linkid_z = $linkid."z";
           $linkid .="a";
        }
        print DEBUG1 "servid--->$servid----servid_z--->$servid_z-----linkid------>$linkid------linkid_z----->$linkid_z---\n";
	my $checklink = CMS_LINKS_DB::check_existing_link
          ($CMS::html_values{routerid}{value},
           $CMS::html_values{interface}{value},
           $linkid, 0);
	print DEBUG1 "checklin---->$checklink\n";
	my $checklink_z = 1;
	if($linkid_z && $CMS::html_values{interface_z}{value}){
	$checklink_z = CMS_LINKS_DB::check_existing_link
          ($CMS::html_values{routerid_z}{value},
           $CMS::html_values{interface_z}{value},
           $linkid_z, 0);
	  print DEBUG1 "checklin1 ----->$checklink_z\n";
	}
                 #fetching all values to be populated in the screen
                my $linktypes = CMS_LINKS_DB::get_link_types ($CMS::html_values{linkid}{value}, $CMS::html_values{serviceid}{value});
                my $routerids = CMS_ROUTERS_DB::get_router("ALL");
                foreach my $router (@$routerids){
                        my $string = join (", ", @$router);
                        $string =~ s/, $//g;

                        $router = [$$router[0], $string];
                }

                $CMS::html_values{typeid}{options} = $linktypes;
                #$CMS::html_values{typeid_z}{options} = $linktypes;
                $CMS::html_values{routerid}{options} = $routerids;
                $CMS::html_values{routerid_z}{options} = $routerids;

       if ($checklink != 1 ) {
		CMS::add_error("Unable to edit link.");
                if($CMS::html_values{serviceid}{value} =~ /.*\[.*\].*ec.*/){
                       CMS::add_error("A End Router port already exists.");
                }else{
                        CMS::add_error("Router port already exists.");
                }
		CMS::output_html("links_edit2");
             return;
        }
        if( $checklink_z !=1 ){
                print DEBUG1 "z end exists\n";
		CMS::add_error("Unable to edit link.");
                CMS::add_error("Z End Router port already exists.");
		CMS::output_html("links_edit2");
                return;
        }
	#passing parameters for A End(EVPL) and other links to update a link.  
	my $updlink;
	##++IPT5
	my $bha_routername = (split /\,/,$CMS::html_values{bha_router}{value})[0];
	if ($debug){print IPT "bha_routername=$bha_routername\n";}
	my $router_ipt = $CMS::html_values{routerid_ipt}{value};
	if ($debug) {print IPT "PM UPDATE router_ipt:$router_ipt\n";}
	my $routername_ipt = (split /\,/,$CMS::html_values{routerid_ipt}{value})[0] ;
        ##++Addendum-2 BHA Requirment.
        my $bha_routernamebac = (split /\,/,$CMS::html_values{bha_routerbac}{value})[0];
        ##--Addendum-2 BHA Requirment.
	if ($bha_routername){
		    $updlink = CMS_LINKS_DB::update_link
                   ($linkid,
                    $servid,
                    $CMS::html_values{typeid}{value},
		    $CMS::html_values{routerid}{value},
                    $CMS::html_values{interface}{value},
                    $CMS::html_values{bandwidth}{value},
                    $CMS::html_values{description}{value},
                    $bha_routername,
                    $CMS::html_values{bha_description}{value},
                    ##++Addendum-2 BHA Requirment.
                    $bha_routernamebac,
                    $CMS::html_values{bha_descriptionbac}{value});
                    ##--Addendum-2 BHA Requirment.
	} else {
	$updlink = CMS_LINKS_DB::update_link
                   ($linkid,
                    $servid,
                    $CMS::html_values{typeid}{value},
                    $CMS::html_values{routerid}{value},
                    $CMS::html_values{interface}{value},
                    $CMS::html_values{bandwidth}{value},
                    $CMS::html_values{description}{value},
		    );
	}
	##--IPT5
	#passing parameters for Z End(EVPL) only.
	my $updlink_z;
	if($linkid_z){
        $updlink_z = CMS_LINKS_DB::update_link
                   ($linkid_z,
                    $servid_z,
                    $CMS::html_values{typeid}{value},
                    $CMS::html_values{routerid_z}{value},
                    $CMS::html_values{interface_z}{value},
                    $CMS::html_values{bandwidth}{value},
                   $CMS::html_values{description_z}{value});
	}
	       if (($updlink == 1) || ($updlink_z == 1)) {
		#CR-84 Juniper IPVPN Report
                my $rtr_edit = $CMS::html_values{routerid}{value};
                my $junos_edit_update = CMS_LINKS_DB::update_junos_service($servid,$rtr_edit);
				 
		 # ++ Added for User Log after editing service - Sayantan dutta - 12-Nov-2009
		 user_log "Edit Link";
		 # -- Added for User Log after editing service - Sayantan dutta - 12-Nov-2009
					
		 CMS::add_error("Edited link succesfully");
		 CMS::output_html("links_edit");

	       } else {

		 CMS::add_error("Unable to edit link.");
		 CMS::add_error("$updlink");
		 CMS::output_html("links_edit2");
 	      }
     }
   } elsif ($CMS::html_values{command}{value} eq "delete_link") {
     
     if (not defined ($CMS::html_values{stage}{value})) {
      # first stage
       CMS::output_html("links_delete");
     } else {
	
	        my $servid = $CMS::html_values{serviceid}{value};
        my $linkid = $CMS::html_values{linkid}{value};
        my ($linkid_z,$servid_z);
	#Checking for EVPL serviceid and replacing serviceid with asw and zsw also appending a and z to linkid.
        if ($servid =~ /.*\[.*\].*ec.*sw$/){
            $servid_z=$servid;
           $servid_z =~ s/sw/zsw/;
           $servid =~ s/sw/asw/;
           $linkid_z = $linkid."z";
           $linkid .="a";

        }elsif($servid =~ /.*\[.*\].*ec.*/){
           $servid_z = $servid."z";
           $servid .= "a";
           $linkid_z = $linkid."z";
           $linkid .="a";
        }

        my $result = CMS_LINKS_DB::delete_link($linkid,$servid);
        my $result_z;
        if($linkid_z){
         $result_z = CMS_LINKS_DB::delete_link($linkid_z,$servid_z);
        }


       if ($result ==1 || $result_z==1){ 

	 # ++ Added for User Log after deleting service - Sayantan dutta - 12-Nov-2009
	 user_log "Delete Link";
	 # -- Added for User Log after deleting service - Sayantan dutta - 12-Nov-2009
					
	 CMS::add_error("Link deleted successfully.");
	 CMS::output_html("links_delete");
       } else {
	 CMS::add_error("Unable to delete.");
	 CMS::add_error("LinkId/Serviceid not present in database");
	 CMS::output_html("links_delete");
	 
       }

       
     }

  } elsif ($CMS::html_values{command}{value} eq "search_link") {
 open (DEBUG,">/tmp/search_link"); 
    if ((defined $CMS::html_values{submit}{value}) &&
	($CMS::html_values{submit}{value} eq "Search")) {
   	my $res;

print DEBUG "INSIDE SEARCH\n";
	my $serviceid = $CMS::html_values{search_service}{value};
	if ($serviceid =~ /.*\[.*\].*ec.*sw$/){
 		$serviceid=~s/sw/%sw/;
	}
	#Fetching values for A End links(EVPL) and othet than EVPL links. 
 	#01Nov2010 - Changed for IPT5 by Karuna Ballal
	##++IPT5
	  $res = CMS_LINKS_DB::search_links_screen
	  ($CMS::html_values{search_linkid}{value},
	   $serviceid,
	   $CMS::html_values{search_type}{value},
	   $CMS::html_values{search_router}{value},
	   $CMS::html_values{search_interface}{value},
	   $CMS::html_values{search_bandwidth}{value},
	   $CMS::html_values{search_description}{value},
	   'A',
	   $CMS::html_values{search_bha_router}{value},
	   $CMS::html_values{search_bha_description}{value},
           ##++Addendum 2 BHA Requirment.
           $CMS::html_values{search_bha_routerbac}{value},
           $CMS::html_values{search_bha_descriptionbac}{value}
           ##--Addendum 2 BHA Requirment.
	   );
		print DEBUG "fetched vals for a en condition\n";
		print DEBUG "search_router:$CMS::html_values{search_router}{value}\n";
	#++EVPL
	##--IPT5

print DEBUG "res:$res\n";
	$CMS::html_values{links}{table}=$res;	
      	unshift (@{$CMS::html_values{links}{table}}, 
	       ["Link", "Service", "End", "Type", "Router", 
		"Interface", "Bandwidth", "Description", "BHA Router Primary", "BHA Description Primary"," BHA Router Backup","BHA Description Backup"]);
	##--EVPL
      
        $CMS::html_values{links}{header_rows} = 1;

    } else {
      
      $CMS::html_values{search_linkid}{value} = "";
      $CMS::html_values{search_searchid}{value} = "";
      $CMS::html_values{search_type}{value} = "";
      $CMS::html_values{search_router}{value} = "";
      $CMS::html_values{search_interface}{value} = "";
      $CMS::html_values{search_bandwidth}{value} = "";
      $CMS::html_values{search_description}{value} = "";
      ##++IPT5	
      $CMS::html_values{search_bha_router}{value} = "";
      $CMS::html_values{search_bha_description}{value} = "";
      ##++Addendum 2 BHA Requirment.
      $CMS::html_values{search_bha_routerbac}{value} = "";
      $CMS::html_values{search_bha_descriptionbac}{value} = "";
      ##--Addendum 2 BHA Requirment.
      ##--IPT5
    }
    
    CMS::output_html("links_search");

 
  }
#++EVPL
 elsif ($CMS::html_values{command}{value} eq "fetchall") {
	$retVal=CMS_LINKS_DB::fetchServfromDB($CMS::html_values{cond}{value});
        print "Content-type: html/text\n\n"; 
        print $retVal;
  } elsif ($CMS::html_values{command}{value} eq "getproductcode") {
	my $servid = $CMS::html_values{srv}{value};
        $retVal=CMS_LINKS_DB::fetchProdCodefromDB($servid);
        print "Content-type: html/text\n\n";
        print $retVal;
  }
 elsif ($CMS::html_values{command}{value} eq "fetchalllinks") {
        $retVal=CMS_LINKS_DB::fetchLinkidsfromDB();
        print "Content-type: html/text\n\n";
        print $retVal;
  } elsif ($CMS::html_values{command}{value} eq "fetchservid") {
        my $linkid = $CMS::html_values{linkid}{value};
        $retVal=CMS_LINKS_DB::fetchSerIDfromLinkId($linkid);
        print "Content-type: html/text\n\n";
        print $retVal;
  }
#--EVPL
#++IPT5
  elsif ($CMS::html_values{command}{value} eq "check_bha") {
	if ($debug){print IPT "entered check BHA fn\n";}
	my $servid = $CMS::html_values{servid}{value}; 
	if ($debug){print IPT "PM servid:$servid\n";}
	$retVal=CMS_LINKS_DB::check_prod_bha($servid);
	if ($debug){print IPT "retVal:$retVal\n";}
        print "Content-type: html/text\n\n";
        print $retVal;
  } elsif ($CMS::html_values{command}{value} eq "get_router_string") {
	my $type = $CMS::html_values{type_router}{value};
        ##called from the front end script
   	if ($debug){print IPT "PM links entered get_router_string\n";}
       	##get result from the DB module
	if ($debug){print IPT "PM before calling DB file\n";}
        #my $retStr=CMS_LINKS_DB::fetch_routers();
        my $retStr=&fetch_routers();
        #if ($debug){print IPT "PM retStr:$retStr\n";}
        ##print on screen for auto complete feature
        print "Content-type: html/text\n\n";
        print $retStr;
   } elsif ($CMS::html_values{command}{value} eq "get_router_bha") {
	if ($debug){print IPT "PM links entered get_router_bha\n";}
        ##get result from the DB module
        if ($debug){print IPT "PM before calling DB file\n";}
	my $retStr=&fetch_routers_bha();
	##print on screen for auto complete feature
        print "Content-type: html/text\n\n";
        print $retStr;
        } 
#--IPT5 
} 
  
sub fetch_routers {
                if ($debug){print IPT "DB entered fetch_routers\n";}
                ##fetch details from the DB
                my $sth1 = $dbh->prepare("SELECT routername, popname, community, cityname, regionname, qos, detail, prefix_update, vpls, evpl FROM RouterName natural join PopName natural join CityName natural join RegionName WHERE routername NOT IN (SELECT routername FROM RouterName WHERE router_switch like 'BHA Router') ORDER BY routername");
                unless ($sth1->execute) {
                	$error = $dbh->errstr;
                }
                my @db_vals = @{$sth1->fetchall_arrayref()};
                if ($error){
                        if ($debug){print IPT "DB error:$error\n";}
                        return $error;
                }
                my (@new_array,$str_ret) = "";
                foreach my $array (@db_vals){
                        #if ($debug){print IPT "DB array:$$array[0]\n";}
                        ##Join details with comma
                        my $str_vals = join ",",@$array;
                        #if ($debug){print IPT "DB str_vals:$str_vals\n";}
                        push (@new_array,$str_vals);
                        ##join each string with semi colon
                        $str_ret = join ":",@new_array;
                }
                $sth1->finish();
		my $sth_bha = $dbh->prepare("select routername, popname, community, cityname, regionname, qos, detail, prefix_update, vpls, evpl from RouterName natural join PopName natural join CityName natural join RegionName WHERE router_switch like 'BHA Router' order by routername");
		unless ($sth_bha->execute) {
                        $error = $dbh->errstr;
                }
                my @db_vals_bha = @{$sth_bha->fetchall_arrayref()};
                if ($error){
                        if ($debug){print IPT "DB error:$error\n";}
                        return $error;
                }
		my (@bha_array,$str_bha) = "";
		foreach my $bha_vals (@db_vals_bha){
			##Join details with comma
                        my $str_vals = join ",",@$bha_vals;
			push (@bha_array,$str_vals);
                        ##join each string with semi colon
                        $str_bha = join ":",@bha_array;
		}
		if ($debug){print IPT "fetch_routers str_bha:$str_bha\n";}
		my $str_joint = join "DELIMITER",$str_ret,$str_bha;
                #return $str_ret;
                return $str_joint;
}

sub fetch_routers_bha {
	if ($debug) {print IPT "PM Entered fetch_routers_bha\n";}
	my $sth = $dbh->prepare("select routername, popname, community, cityname, regionname, qos, detail, prefix_update, vpls, evpl from RouterName natural join PopName natural join CityName natural join RegionName WHERE router_switch like 'BHA Router' order by routername");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                }
	my $result = $sth->fetchall_arrayref();
=pod
	my @db_vals = @{$sth->fetchall_arrayref()};
	if ($error){
                        if ($debug){print IPT "DB error:$error\n";}
                        return $error;
                }
                my (@new_array,$str_ret) = "";
                foreach my $array (@db_vals){
			##Join details with comma
                        my $str_vals = join ",",@$array;
			push (@new_array,$str_vals);
                        ##join each string with semi colon
                        #$str_ret = join ":",@new_array;
                }
	if ($debug) {print IPT "PM fetch_routers_bha str_ret:$str_ret\n";}
=cut
                $sth->finish();
                #return $str_ret;
		return $result;
}
  

1;
