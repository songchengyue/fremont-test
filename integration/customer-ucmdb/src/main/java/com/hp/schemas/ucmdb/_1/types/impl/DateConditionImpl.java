/*
 * XML Type:  DateCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.DateCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML DateCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class DateConditionImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.DateCondition
{
    
    public DateConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "condition");
    private static final javax.xml.namespace.QName DATEOPERATOR$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "dateOperator");
    
    
    /**
     * Gets the "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.DateProp getCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "condition" element
     */
    public void setCondition(com.hp.schemas.ucmdb._1.types.DateProp condition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().add_element_user(CONDITION$0);
            }
            target.set(condition);
        }
    }
    
    /**
     * Appends and returns a new empty "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.DateProp addNewCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().add_element_user(CONDITION$0);
            return target;
        }
    }
    
    /**
     * Gets the "dateOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator.Enum getDateOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATEOPERATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "dateOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator xgetDateOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator)get_store().find_element_user(DATEOPERATOR$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "dateOperator" element
     */
    public void setDateOperator(com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator.Enum dateOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATEOPERATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DATEOPERATOR$2);
            }
            target.setEnumValue(dateOperator);
        }
    }
    
    /**
     * Sets (as xml) the "dateOperator" element
     */
    public void xsetDateOperator(com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator dateOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator)get_store().find_element_user(DATEOPERATOR$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator)get_store().add_element_user(DATEOPERATOR$2);
            }
            target.set(dateOperator);
        }
    }
    /**
     * An XML dateOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.DateCondition$DateOperator.
     */
    public static class DateOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.DateCondition.DateOperator
    {
        
        public DateOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected DateOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
