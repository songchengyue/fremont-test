/*
 * XML Type:  UcmdbClassModelHierarchy
 * Namespace: http://schemas.hp.com/ucmdb/1/types/classmodel
 * Java type: com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.classmodel.impl;
/**
 * An XML UcmdbClassModelHierarchy(@http://schemas.hp.com/ucmdb/1/types/classmodel).
 *
 * This is a complex type.
 */
public class UcmdbClassModelHierarchyImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy
{
    
    public UcmdbClassModelHierarchyImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLASSHIERARCHYNODE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "classHierarchyNode");
    
    
    /**
     * Gets array of all "classHierarchyNode" elements
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode[] getClassHierarchyNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CLASSHIERARCHYNODE$0, targetList);
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode[] result = new com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "classHierarchyNode" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode getClassHierarchyNodeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode)get_store().find_element_user(CLASSHIERARCHYNODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "classHierarchyNode" element
     */
    public int sizeOfClassHierarchyNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CLASSHIERARCHYNODE$0);
        }
    }
    
    /**
     * Sets array of all "classHierarchyNode" element
     */
    public void setClassHierarchyNodeArray(com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode[] classHierarchyNodeArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(classHierarchyNodeArray, CLASSHIERARCHYNODE$0);
        }
    }
    
    /**
     * Sets ith "classHierarchyNode" element
     */
    public void setClassHierarchyNodeArray(int i, com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode classHierarchyNode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode)get_store().find_element_user(CLASSHIERARCHYNODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(classHierarchyNode);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "classHierarchyNode" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode insertNewClassHierarchyNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode)get_store().insert_element_user(CLASSHIERARCHYNODE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "classHierarchyNode" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode addNewClassHierarchyNode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode)get_store().add_element_user(CLASSHIERARCHYNODE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "classHierarchyNode" element
     */
    public void removeClassHierarchyNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CLASSHIERARCHYNODE$0, i);
        }
    }
}
