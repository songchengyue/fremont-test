#!/usr/bin/perl

use strict;
use File::Copy;

my $file = "reach.acl";
my $time = time;
my $old = "old";
my @grep_t = `ps auxw | grep named.conf`;
my $diff = 0;
my @grep_first = split(" ",@grep_t[0]);
my $usr = @grep_first[0];
my $pid =  @grep_first[1];
my $cmd = `ps auxw | grep named.conf | grep -i indonesia | grep -i resolver | grep -v grep | cut -c9-15`;
print "before killing $cmd\n";

#sync
my $res = `/usr/bin/rsync -azlv hhtred278::dns_resolver_indonesia /etc/namedb/indonesia/resolver/dns_d > /var/log/dnsres_indonesia.conf.rsync`;
#take a diff
$diff = `diff /etc/namedb/indonesia/resolver/dns_d/reach.acl /etc/namedb/indonesia/resolver/reach.acl|wc -l`;
print "DIFF: $diff\n";
if ($diff > 0)
	{
        #backup
	copy ("/etc/namedb/indonesia/resolver/$file", "/etc/namedb/indonesia/resolver/backup_resolver/$file.$time");
	print "taking backup..\n";
	`mv /etc/namedb/indonesia/resolver/dns_d/reach.acl /etc/namedb/indonesia/resolver/reach.acl`;
		#kill the process
		print "killing the process $cmd\n";
		`sudo kill -9 $cmd`;
		#restart
		print "restarting the server\n";
		`sudo /usr/local/dns/sbin/named -c /etc/namedb/indonesia/resolver/named.conf -u named`
	}
elsif ($diff == 0)
	{
	print "deleting..\n";
	#delete file
	`rm /etc/namedb/indonesia/resolver/dns_d/reach.acl`;
	}

# remove out-dated file
`/usr/bin/find /etc/namedb/indonesia/resolver/backup_resolver -type f -mtime +20 -print | xargs rm -f`;
