#!/usr/bin/perl
use JSON::PP;
use CGI;
use CGI::Carp qw ( fatalsToBrowser );

my $req = new CGI;
my $input = decode_json( $req->param('POSTDATA') );

my $customerId          =$input->{'customerId'};
# expecting "41" form primary
my $level                       =$input->{'level'};
my $submit                          =$input->{'PRIMARY_VIEW_SUBMIT'};

warn "level: $level, submit: $submit, customerId $customerId";

print "Content-Type: application/json\n\n";

my %out = (
         'result' => [
             {
                 'DBrowId' => 'XXXXX',
                 'Hostname' => 'www',
                 'Contact' => 'x@abc.com',
                 'Account' => 'abc123',
                 'recordType' => 'A',
                 'Domain' => 'abc.com',
                 'parameter' => '1.1.1.1',
                 'RequestTime' =>  '2016-01-01 00:01:00'
             }
         ]
        );

my $output = encode_json \%out;
warn "JSON: $output\n";
print $output;
