@echo off

IF [%1]==[/?] GOTO :help

IF [%1]==[] GOTO :help

echo %* |find "/?" > nul
IF errorlevel 1 GOTO :main

:help
ECHO get-product-lines.bat TOKEN
GOTO :end

:main
curl -H "Authorization: Bearer %1" http://localhost:8080/lookingglass-server/productline

:end