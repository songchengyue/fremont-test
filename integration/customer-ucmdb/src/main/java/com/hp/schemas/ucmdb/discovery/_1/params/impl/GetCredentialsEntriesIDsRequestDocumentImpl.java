/*
 * An XML document type.
 * Localname: getCredentialsEntriesIDsRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getCredentialsEntriesIDsRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetCredentialsEntriesIDsRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequestDocument
{
    
    public GetCredentialsEntriesIDsRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCREDENTIALSENTRIESIDSREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getCredentialsEntriesIDsRequest");
    
    
    /**
     * Gets the "getCredentialsEntriesIDsRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequest getGetCredentialsEntriesIDsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequest)get_store().find_element_user(GETCREDENTIALSENTRIESIDSREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCredentialsEntriesIDsRequest" element
     */
    public void setGetCredentialsEntriesIDsRequest(com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequest getCredentialsEntriesIDsRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequest)get_store().find_element_user(GETCREDENTIALSENTRIESIDSREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequest)get_store().add_element_user(GETCREDENTIALSENTRIESIDSREQUEST$0);
            }
            target.set(getCredentialsEntriesIDsRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "getCredentialsEntriesIDsRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequest addNewGetCredentialsEntriesIDsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsRequest)get_store().add_element_user(GETCREDENTIALSENTRIESIDSREQUEST$0);
            return target;
        }
    }
}
