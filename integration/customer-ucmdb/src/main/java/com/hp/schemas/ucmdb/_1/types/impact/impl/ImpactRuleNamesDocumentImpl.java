/*
 * An XML document type.
 * Localname: ImpactRuleNames
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * A document containing one ImpactRuleNames(@http://schemas.hp.com/ucmdb/1/types/impact) element.
 *
 * This is a complex type.
 */
public class ImpactRuleNamesDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument
{
    
    public ImpactRuleNamesDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTRULENAMES$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "ImpactRuleNames");
    
    
    /**
     * Gets the "ImpactRuleNames" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames getImpactRuleNames()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames)get_store().find_element_user(IMPACTRULENAMES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ImpactRuleNames" element
     */
    public void setImpactRuleNames(com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames impactRuleNames)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames)get_store().find_element_user(IMPACTRULENAMES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames)get_store().add_element_user(IMPACTRULENAMES$0);
            }
            target.set(impactRuleNames);
        }
    }
    
    /**
     * Appends and returns a new empty "ImpactRuleNames" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames addNewImpactRuleNames()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames)get_store().add_element_user(IMPACTRULENAMES$0);
            return target;
        }
    }
    /**
     * An XML ImpactRuleNames(@http://schemas.hp.com/ucmdb/1/types/impact).
     *
     * This is a complex type.
     */
    public static class ImpactRuleNamesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames
    {
        
        public ImpactRuleNamesImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IMPACTRULENAME$0 = 
            new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "impactRuleName");
        
        
        /**
         * Gets array of all "impactRuleName" elements
         */
        public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName[] getImpactRuleNameArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(IMPACTRULENAME$0, targetList);
                com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName[] result = new com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "impactRuleName" element
         */
        public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName getImpactRuleNameArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName target = null;
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName)get_store().find_element_user(IMPACTRULENAME$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "impactRuleName" element
         */
        public int sizeOfImpactRuleNameArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IMPACTRULENAME$0);
            }
        }
        
        /**
         * Sets array of all "impactRuleName" element
         */
        public void setImpactRuleNameArray(com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName[] impactRuleNameArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(impactRuleNameArray, IMPACTRULENAME$0);
            }
        }
        
        /**
         * Sets ith "impactRuleName" element
         */
        public void setImpactRuleNameArray(int i, com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName impactRuleName)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName target = null;
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName)get_store().find_element_user(IMPACTRULENAME$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(impactRuleName);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "impactRuleName" element
         */
        public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName insertNewImpactRuleName(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName target = null;
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName)get_store().insert_element_user(IMPACTRULENAME$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "impactRuleName" element
         */
        public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName addNewImpactRuleName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName target = null;
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName)get_store().add_element_user(IMPACTRULENAME$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "impactRuleName" element
         */
        public void removeImpactRuleName(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IMPACTRULENAME$0, i);
            }
        }
    }
}
