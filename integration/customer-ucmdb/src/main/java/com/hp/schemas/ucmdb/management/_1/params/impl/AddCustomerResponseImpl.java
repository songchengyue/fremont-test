/*
 * XML Type:  addCustomerResponse
 * Namespace: http://schemas.hp.com/ucmdb/management/1/params
 * Java type: com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.management._1.params.impl;
/**
 * An XML addCustomerResponse(@http://schemas.hp.com/ucmdb/management/1/params).
 *
 * This is a complex type.
 */
public class AddCustomerResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse
{
    
    public AddCustomerResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
