/*
 * XML Type:  BytesParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.BytesParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML BytesParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class BytesParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.BytesParams
{
    
    public BytesParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName BYTESPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "bytesParam");
    
    
    /**
     * Gets array of all "bytesParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.BytesProp[] getBytesParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(BYTESPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.BytesProp[] result = new com.hp.schemas.ucmdb._1.types.BytesProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "bytesParam" element
     */
    public com.hp.schemas.ucmdb._1.types.BytesProp getBytesParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesProp)get_store().find_element_user(BYTESPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "bytesParam" element
     */
    public int sizeOfBytesParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BYTESPARAM$0);
        }
    }
    
    /**
     * Sets array of all "bytesParam" element
     */
    public void setBytesParamArray(com.hp.schemas.ucmdb._1.types.BytesProp[] bytesParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(bytesParamArray, BYTESPARAM$0);
        }
    }
    
    /**
     * Sets ith "bytesParam" element
     */
    public void setBytesParamArray(int i, com.hp.schemas.ucmdb._1.types.BytesProp bytesParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesProp)get_store().find_element_user(BYTESPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(bytesParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "bytesParam" element
     */
    public com.hp.schemas.ucmdb._1.types.BytesProp insertNewBytesParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesProp)get_store().insert_element_user(BYTESPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "bytesParam" element
     */
    public com.hp.schemas.ucmdb._1.types.BytesProp addNewBytesParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesProp)get_store().add_element_user(BYTESPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "bytesParam" element
     */
    public void removeBytesParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BYTESPARAM$0, i);
        }
    }
}
