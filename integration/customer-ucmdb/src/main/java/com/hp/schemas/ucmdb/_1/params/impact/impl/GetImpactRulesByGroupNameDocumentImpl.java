/*
 * An XML document type.
 * Localname: getImpactRulesByGroupName
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * A document containing one getImpactRulesByGroupName(@http://schemas.hp.com/ucmdb/1/params/impact) element.
 *
 * This is a complex type.
 */
public class GetImpactRulesByGroupNameDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameDocument
{
    
    public GetImpactRulesByGroupNameDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETIMPACTRULESBYGROUPNAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactRulesByGroupName");
    
    
    /**
     * Gets the "getImpactRulesByGroupName" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName getGetImpactRulesByGroupName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName)get_store().find_element_user(GETIMPACTRULESBYGROUPNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getImpactRulesByGroupName" element
     */
    public void setGetImpactRulesByGroupName(com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName getImpactRulesByGroupName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName)get_store().find_element_user(GETIMPACTRULESBYGROUPNAME$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName)get_store().add_element_user(GETIMPACTRULESBYGROUPNAME$0);
            }
            target.set(getImpactRulesByGroupName);
        }
    }
    
    /**
     * Appends and returns a new empty "getImpactRulesByGroupName" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName addNewGetImpactRulesByGroupName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName)get_store().add_element_user(GETIMPACTRULESBYGROUPNAME$0);
            return target;
        }
    }
}
