/*
 * XML Type:  StrCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.StrCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML StrCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class StrConditionImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.StrCondition
{
    
    public StrConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "condition");
    private static final javax.xml.namespace.QName STROPERATOR$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strOperator");
    
    
    /**
     * Gets the "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.StrProp getCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "condition" element
     */
    public void setCondition(com.hp.schemas.ucmdb._1.types.StrProp condition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().add_element_user(CONDITION$0);
            }
            target.set(condition);
        }
    }
    
    /**
     * Appends and returns a new empty "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.StrProp addNewCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().add_element_user(CONDITION$0);
            return target;
        }
    }
    
    /**
     * Gets the "strOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator.Enum getStrOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STROPERATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "strOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator xgetStrOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator)get_store().find_element_user(STROPERATOR$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "strOperator" element
     */
    public void setStrOperator(com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator.Enum strOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STROPERATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STROPERATOR$2);
            }
            target.setEnumValue(strOperator);
        }
    }
    
    /**
     * Sets (as xml) the "strOperator" element
     */
    public void xsetStrOperator(com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator strOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator)get_store().find_element_user(STROPERATOR$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator)get_store().add_element_user(STROPERATOR$2);
            }
            target.set(strOperator);
        }
    }
    /**
     * An XML strOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.StrCondition$StrOperator.
     */
    public static class StrOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator
    {
        
        public StrOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected StrOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
