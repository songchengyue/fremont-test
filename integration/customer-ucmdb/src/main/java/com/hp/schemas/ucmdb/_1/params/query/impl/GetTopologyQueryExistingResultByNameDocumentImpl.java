/*
 * An XML document type.
 * Localname: getTopologyQueryExistingResultByName
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getTopologyQueryExistingResultByName(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetTopologyQueryExistingResultByNameDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameDocument
{
    
    public GetTopologyQueryExistingResultByNameDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETTOPOLOGYQUERYEXISTINGRESULTBYNAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getTopologyQueryExistingResultByName");
    
    
    /**
     * Gets the "getTopologyQueryExistingResultByName" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByName getGetTopologyQueryExistingResultByName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByName target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByName)get_store().find_element_user(GETTOPOLOGYQUERYEXISTINGRESULTBYNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getTopologyQueryExistingResultByName" element
     */
    public void setGetTopologyQueryExistingResultByName(com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByName getTopologyQueryExistingResultByName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByName target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByName)get_store().find_element_user(GETTOPOLOGYQUERYEXISTINGRESULTBYNAME$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByName)get_store().add_element_user(GETTOPOLOGYQUERYEXISTINGRESULTBYNAME$0);
            }
            target.set(getTopologyQueryExistingResultByName);
        }
    }
    
    /**
     * Appends and returns a new empty "getTopologyQueryExistingResultByName" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByName addNewGetTopologyQueryExistingResultByName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByName target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByName)get_store().add_element_user(GETTOPOLOGYQUERYEXISTINGRESULTBYNAME$0);
            return target;
        }
    }
}
