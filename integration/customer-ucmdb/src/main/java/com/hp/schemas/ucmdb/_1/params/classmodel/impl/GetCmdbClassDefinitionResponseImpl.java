/*
 * XML Type:  getCmdbClassDefinitionResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/classmodel
 * Java type: com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.classmodel.impl;
/**
 * An XML getCmdbClassDefinitionResponse(@http://schemas.hp.com/ucmdb/1/params/classmodel).
 *
 * This is a complex type.
 */
public class GetCmdbClassDefinitionResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse
{
    
    public GetCmdbClassDefinitionResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName UCMDBCLASS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "UcmdbClass");
    private static final javax.xml.namespace.QName COMMENTS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "comments");
    
    
    /**
     * Gets the "UcmdbClass" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass getUcmdbClass()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass)get_store().find_element_user(UCMDBCLASS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "UcmdbClass" element
     */
    public void setUcmdbClass(com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass ucmdbClass)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass)get_store().find_element_user(UCMDBCLASS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass)get_store().add_element_user(UCMDBCLASS$0);
            }
            target.set(ucmdbClass);
        }
    }
    
    /**
     * Appends and returns a new empty "UcmdbClass" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass addNewUcmdbClass()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass)get_store().add_element_user(UCMDBCLASS$0);
            return target;
        }
    }
    
    /**
     * Gets the "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments getComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "comments" element
     */
    public void setComments(com.hp.schemas.ucmdb._1.types.ResComments comments)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            }
            target.set(comments);
        }
    }
    
    /**
     * Appends and returns a new empty "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments addNewComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            return target;
        }
    }
}
