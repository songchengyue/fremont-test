/*
 * XML Type:  IntCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML IntCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IntConditionImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.IntCondition
{
    
    public IntConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "condition");
    private static final javax.xml.namespace.QName INTOPERATOR$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intOperator");
    
    
    /**
     * Gets the "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.IntProp getCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "condition" element
     */
    public void setCondition(com.hp.schemas.ucmdb._1.types.IntProp condition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().add_element_user(CONDITION$0);
            }
            target.set(condition);
        }
    }
    
    /**
     * Appends and returns a new empty "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.IntProp addNewCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().add_element_user(CONDITION$0);
            return target;
        }
    }
    
    /**
     * Gets the "intOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator.Enum getIntOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INTOPERATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "intOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator xgetIntOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator)get_store().find_element_user(INTOPERATOR$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "intOperator" element
     */
    public void setIntOperator(com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator.Enum intOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INTOPERATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(INTOPERATOR$2);
            }
            target.setEnumValue(intOperator);
        }
    }
    
    /**
     * Sets (as xml) the "intOperator" element
     */
    public void xsetIntOperator(com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator intOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator)get_store().find_element_user(INTOPERATOR$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator)get_store().add_element_user(INTOPERATOR$2);
            }
            target.set(intOperator);
        }
    }
    /**
     * An XML intOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.IntCondition$IntOperator.
     */
    public static class IntOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.IntCondition.IntOperator
    {
        
        public IntOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IntOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
