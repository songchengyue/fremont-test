package com.telstra.netops.client.controller;

import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ModelMap;

import com.telstra.netops.client.config.ClientEnvironment;

public class AuthControllerTest {

	@InjectMocks
	private AuthController controller;
	
	@Mock
	private ClientEnvironment environment;
	
	@Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void refresh() {
		ModelMap model = new ModelMap();
		controller.refresh(model);
		assertFalse(model.isEmpty());
	}
	
}
