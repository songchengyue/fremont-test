/*
 * An XML document type.
 * Localname: getChangedCIsResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/history
 * Java type: com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.history.impl;
/**
 * A document containing one getChangedCIsResponse(@http://schemas.hp.com/ucmdb/1/params/history) element.
 *
 * This is a complex type.
 */
public class GetChangedCIsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponseDocument
{
    
    public GetChangedCIsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCHANGEDCISRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/history", "getChangedCIsResponse");
    
    
    /**
     * Gets the "getChangedCIsResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse getGetChangedCIsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse)get_store().find_element_user(GETCHANGEDCISRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getChangedCIsResponse" element
     */
    public void setGetChangedCIsResponse(com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse getChangedCIsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse)get_store().find_element_user(GETCHANGEDCISRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse)get_store().add_element_user(GETCHANGEDCISRESPONSE$0);
            }
            target.set(getChangedCIsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getChangedCIsResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse addNewGetChangedCIsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse)get_store().add_element_user(GETCHANGEDCISRESPONSE$0);
            return target;
        }
    }
}
