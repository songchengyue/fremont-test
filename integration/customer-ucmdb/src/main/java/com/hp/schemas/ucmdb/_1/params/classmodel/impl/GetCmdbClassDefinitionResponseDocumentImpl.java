/*
 * An XML document type.
 * Localname: getCmdbClassDefinitionResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/classmodel
 * Java type: com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.classmodel.impl;
/**
 * A document containing one getCmdbClassDefinitionResponse(@http://schemas.hp.com/ucmdb/1/params/classmodel) element.
 *
 * This is a complex type.
 */
public class GetCmdbClassDefinitionResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponseDocument
{
    
    public GetCmdbClassDefinitionResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCMDBCLASSDEFINITIONRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getCmdbClassDefinitionResponse");
    
    
    /**
     * Gets the "getCmdbClassDefinitionResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse getGetCmdbClassDefinitionResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse)get_store().find_element_user(GETCMDBCLASSDEFINITIONRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCmdbClassDefinitionResponse" element
     */
    public void setGetCmdbClassDefinitionResponse(com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse getCmdbClassDefinitionResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse)get_store().find_element_user(GETCMDBCLASSDEFINITIONRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse)get_store().add_element_user(GETCMDBCLASSDEFINITIONRESPONSE$0);
            }
            target.set(getCmdbClassDefinitionResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getCmdbClassDefinitionResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse addNewGetCmdbClassDefinitionResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponse)get_store().add_element_user(GETCMDBCLASSDEFINITIONRESPONSE$0);
            return target;
        }
    }
}
