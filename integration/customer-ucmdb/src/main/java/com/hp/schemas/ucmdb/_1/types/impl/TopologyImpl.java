/*
 * XML Type:  Topology
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.Topology
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML Topology(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class TopologyImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.Topology
{
    
    public TopologyImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CIS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "CIs");
    private static final javax.xml.namespace.QName RELATIONS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "relations");
    
    
    /**
     * Gets the "CIs" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs getCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(CIS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CIs" element
     */
    public void setCIs(com.hp.schemas.ucmdb._1.types.CIs cIs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(CIS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(CIS$0);
            }
            target.set(cIs);
        }
    }
    
    /**
     * Appends and returns a new empty "CIs" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs addNewCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(CIS$0);
            return target;
        }
    }
    
    /**
     * Gets the "relations" element
     */
    public com.hp.schemas.ucmdb._1.types.Relations getRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().find_element_user(RELATIONS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "relations" element
     */
    public boolean isSetRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RELATIONS$2) != 0;
        }
    }
    
    /**
     * Sets the "relations" element
     */
    public void setRelations(com.hp.schemas.ucmdb._1.types.Relations relations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().find_element_user(RELATIONS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().add_element_user(RELATIONS$2);
            }
            target.set(relations);
        }
    }
    
    /**
     * Appends and returns a new empty "relations" element
     */
    public com.hp.schemas.ucmdb._1.types.Relations addNewRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().add_element_user(RELATIONS$2);
            return target;
        }
    }
    
    /**
     * Unsets the "relations" element
     */
    public void unsetRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RELATIONS$2, 0);
        }
    }
}
