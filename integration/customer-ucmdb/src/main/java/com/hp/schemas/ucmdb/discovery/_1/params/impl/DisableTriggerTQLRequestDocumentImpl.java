/*
 * An XML document type.
 * Localname: disableTriggerTQLRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one disableTriggerTQLRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class DisableTriggerTQLRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequestDocument
{
    
    public DisableTriggerTQLRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DISABLETRIGGERTQLREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "disableTriggerTQLRequest");
    
    
    /**
     * Gets the "disableTriggerTQLRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequest getDisableTriggerTQLRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequest)get_store().find_element_user(DISABLETRIGGERTQLREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "disableTriggerTQLRequest" element
     */
    public void setDisableTriggerTQLRequest(com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequest disableTriggerTQLRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequest)get_store().find_element_user(DISABLETRIGGERTQLREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequest)get_store().add_element_user(DISABLETRIGGERTQLREQUEST$0);
            }
            target.set(disableTriggerTQLRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "disableTriggerTQLRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequest addNewDisableTriggerTQLRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DisableTriggerTQLRequest)get_store().add_element_user(DISABLETRIGGERTQLREQUEST$0);
            return target;
        }
    }
}
