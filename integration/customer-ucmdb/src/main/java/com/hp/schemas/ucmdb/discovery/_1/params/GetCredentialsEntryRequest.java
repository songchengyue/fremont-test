/*
 * XML Type:  getCredentialsEntryRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params;


/**
 * An XML getCredentialsEntryRequest(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public interface GetCredentialsEntryRequest extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetCredentialsEntryRequest.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("getcredentialsentryrequest7f2ftype");
    
    /**
     * Gets the "domainName" element
     */
    java.lang.String getDomainName();
    
    /**
     * Gets (as xml) the "domainName" element
     */
    org.apache.xmlbeans.XmlString xgetDomainName();
    
    /**
     * Sets the "domainName" element
     */
    void setDomainName(java.lang.String domainName);
    
    /**
     * Sets (as xml) the "domainName" element
     */
    void xsetDomainName(org.apache.xmlbeans.XmlString domainName);
    
    /**
     * Gets the "protocolName" element
     */
    java.lang.String getProtocolName();
    
    /**
     * Gets (as xml) the "protocolName" element
     */
    org.apache.xmlbeans.XmlString xgetProtocolName();
    
    /**
     * Sets the "protocolName" element
     */
    void setProtocolName(java.lang.String protocolName);
    
    /**
     * Sets (as xml) the "protocolName" element
     */
    void xsetProtocolName(org.apache.xmlbeans.XmlString protocolName);
    
    /**
     * Gets the "credentialsEntryID" element
     */
    java.lang.String getCredentialsEntryID();
    
    /**
     * Gets (as xml) the "credentialsEntryID" element
     */
    org.apache.xmlbeans.XmlString xgetCredentialsEntryID();
    
    /**
     * Sets the "credentialsEntryID" element
     */
    void setCredentialsEntryID(java.lang.String credentialsEntryID);
    
    /**
     * Sets (as xml) the "credentialsEntryID" element
     */
    void xsetCredentialsEntryID(org.apache.xmlbeans.XmlString credentialsEntryID);
    
    /**
     * Gets the "CmdbContext" element
     */
    com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext();
    
    /**
     * Sets the "CmdbContext" element
     */
    void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext);
    
    /**
     * Appends and returns a new empty "CmdbContext" element
     */
    com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest newInstance() {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
