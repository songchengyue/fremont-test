/*
 * XML Type:  updateCIsAndRelations
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * An XML updateCIsAndRelations(@http://schemas.hp.com/ucmdb/1/params/update).
 *
 * This is a complex type.
 */
public class UpdateCIsAndRelationsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations
{
    
    public UpdateCIsAndRelationsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "cmdbContext");
    private static final javax.xml.namespace.QName DATASTORE$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "dataStore");
    private static final javax.xml.namespace.QName CISANDRELATIONSUPDATES$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "CIsAndRelationsUpdates");
    private static final javax.xml.namespace.QName IGNOREVALIDATION$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "ignoreValidation");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets the "dataStore" element
     */
    public java.lang.String getDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATASTORE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "dataStore" element
     */
    public org.apache.xmlbeans.XmlString xgetDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DATASTORE$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "dataStore" element
     */
    public boolean isSetDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DATASTORE$2) != 0;
        }
    }
    
    /**
     * Sets the "dataStore" element
     */
    public void setDataStore(java.lang.String dataStore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATASTORE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DATASTORE$2);
            }
            target.setStringValue(dataStore);
        }
    }
    
    /**
     * Sets (as xml) the "dataStore" element
     */
    public void xsetDataStore(org.apache.xmlbeans.XmlString dataStore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DATASTORE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DATASTORE$2);
            }
            target.set(dataStore);
        }
    }
    
    /**
     * Unsets the "dataStore" element
     */
    public void unsetDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DATASTORE$2, 0);
        }
    }
    
    /**
     * Gets the "CIsAndRelationsUpdates" element
     */
    public com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates getCIsAndRelationsUpdates()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().find_element_user(CISANDRELATIONSUPDATES$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CIsAndRelationsUpdates" element
     */
    public void setCIsAndRelationsUpdates(com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates cIsAndRelationsUpdates)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().find_element_user(CISANDRELATIONSUPDATES$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().add_element_user(CISANDRELATIONSUPDATES$4);
            }
            target.set(cIsAndRelationsUpdates);
        }
    }
    
    /**
     * Appends and returns a new empty "CIsAndRelationsUpdates" element
     */
    public com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates addNewCIsAndRelationsUpdates()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().add_element_user(CISANDRELATIONSUPDATES$4);
            return target;
        }
    }
    
    /**
     * Gets the "ignoreValidation" element
     */
    public boolean getIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IGNOREVALIDATION$6, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "ignoreValidation" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(IGNOREVALIDATION$6, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "ignoreValidation" element
     */
    public boolean isNilIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(IGNOREVALIDATION$6, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "ignoreValidation" element
     */
    public boolean isSetIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IGNOREVALIDATION$6) != 0;
        }
    }
    
    /**
     * Sets the "ignoreValidation" element
     */
    public void setIgnoreValidation(boolean ignoreValidation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IGNOREVALIDATION$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IGNOREVALIDATION$6);
            }
            target.setBooleanValue(ignoreValidation);
        }
    }
    
    /**
     * Sets (as xml) the "ignoreValidation" element
     */
    public void xsetIgnoreValidation(org.apache.xmlbeans.XmlBoolean ignoreValidation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(IGNOREVALIDATION$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(IGNOREVALIDATION$6);
            }
            target.set(ignoreValidation);
        }
    }
    
    /**
     * Nils the "ignoreValidation" element
     */
    public void setNilIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(IGNOREVALIDATION$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(IGNOREVALIDATION$6);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "ignoreValidation" element
     */
    public void unsetIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IGNOREVALIDATION$6, 0);
        }
    }
}
