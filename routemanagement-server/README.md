# Telstra NetOps DNS Server

## Synopsis

This project is an NetOps LookingGlass Server.

## Prerequisites

* Git
* Java 8 JDK
* Maven
* curl

## Run

* Get the code

```
git clone https://<your_username>@bitbucket.org/telstradevelopment/telstra-netops.git
```

* Build and run

```
cd telstra-netops\routemanagement-server
mvn package
# activate the correct spring profiles for your environment by adding "-Dspring.profiles.active=<your_profiles>"
# to the tomcat java startup - usually you can do this by setting JAVA_OPTS=Dspring.profiles.active=<your_profiles>
# before starting tomcat
cp target\routemanagement-server-0.0.1-SNAPSHOT.war <tomcat_webapps_folder>/routemanagement-server.war
# now restart tomcat
```

* Test

To test you need to first get a token from the OAuth2 server. Then issue a request:

```
cd telstra-netops\oauth2-server
get-token.bat # get a token
curl -H "Authorization: Bearer <TOKEN>" http://localhost:2020/routemanagement-server/hello
```

