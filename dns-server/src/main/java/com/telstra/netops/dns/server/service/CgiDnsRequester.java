package com.telstra.netops.dns.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.telstra.netops.dns.server.model.cgi.CgiDnsRequest;
import com.telstra.netops.dns.server.util.JsonSerializerDeserializer;
import com.telstra.oauth2.client.OAuth2TokenResolver;

public class CgiDnsRequester<T> {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CgiDnsRequester.class);
	
	private final Class<T> typeParameterClass;
	private final JsonSerializerDeserializer jsonSerializerDeserializer;
	private final String cgiURL;
	
	
	public CgiDnsRequester(Class<T> typeParameterClass, JsonSerializerDeserializer jsonSerializerDeserializer, String cgiURL) {
		this.typeParameterClass = typeParameterClass;
		this.jsonSerializerDeserializer = jsonSerializerDeserializer;
		this.cgiURL = cgiURL;
	}
	
	protected T doRequest(CgiDnsRequest cgiDnsRequest, OAuth2TokenResolver tokenResolver) {
		RestTemplate restTemplate = new RestTemplate();
		LOGGER.debug("doRequest requester1111--------------->>>>");
		HttpHeaders headers = new HttpHeaders();
		headers.add(getAuthorizationHeaderName(tokenResolver), getAuthorizationHeaderValue(tokenResolver));
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();

		map.add("POSTDATA",
				jsonSerializerDeserializer.serialize(cgiDnsRequest));

		HttpEntity<MultiValueMap<String, Object>> httpRequest = new HttpEntity<MultiValueMap<String, Object>>(
				map, headers);
		
		ResponseEntity<T> responseEntity = restTemplate
				.postForEntity(cgiURL, httpRequest, typeParameterClass);
		LOGGER.debug("doRequest requester2222--------------->>>>"+responseEntity.getHeaders());
		LOGGER.debug("doRequest requester2222--------------->>>>"+responseEntity.getBody());
		LOGGER.debug("doRequest requester2222--------------->>>>"+responseEntity.getStatusCode());
		T wrapper = responseEntity.getBody();
		return wrapper;
	}
	
	private String getAuthorizationHeaderName(OAuth2TokenResolver tokenResolver) {
		return tokenResolver.getAuthorizationHeaderName();
	}
	
	private String getAuthorizationHeaderValue(OAuth2TokenResolver tokenResolver) {
		return tokenResolver.getAuthorizationHeaderValue();
	}

}
