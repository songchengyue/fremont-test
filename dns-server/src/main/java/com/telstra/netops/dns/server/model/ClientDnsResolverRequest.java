package com.telstra.netops.dns.server.model;

public class ClientDnsResolverRequest {
	
	private String contactEmailAddress;
	
	private String hostname;
	
	private String ipAddress;
	
	private IPVersion ipVersion ; // default
	
	private String customerId;
	
	private String serviceId ;

	
	public ClientDnsResolverRequest() {
	}
	
	public ClientDnsResolverRequest(String contactEmailAddress,
			String hostname, String ipAddress, String customerId ,String serviceId) {
		super();
		this.contactEmailAddress = contactEmailAddress;
		this.hostname = hostname;
		this.ipAddress = ipAddress;
		this.customerId = customerId;
		this.serviceId = serviceId;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}	
	
	public IPVersion getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(IPVersion ipVersion) {
		this.ipVersion = ipVersion;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}



}
