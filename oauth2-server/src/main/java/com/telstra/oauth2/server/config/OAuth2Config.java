package com.telstra.oauth2.server.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;

import com.telstra.oauth2.server.saml1.SAMLTokenGranter;
import com.telstra.oauth2.server.saml1.XMLAssertionUnmarshaller;

@Configuration
@EnableAuthorizationServer
public class OAuth2Config extends AuthorizationServerConfigurerAdapter {

	@Value("${oauth.server.clientId}")
	private String clientId;

	@Value("${oauth.server.clientSecret}")
	private String clientSecret;

	@Value("${oauth.server.clientAuthorities}")
	private String authorities;

	@Value("${oauth.server.clientScopes}")
	private String scopes;
	
	//
	// SAML tokens are valid for one-hour, so OAuth2 token should be valid for the same amount of time.
	// If you want to override this then add a different value in the properties config file.
	//
	@Value("${oauth.server.accessTokenValiditySeconds:3600}")
	private Integer accessTokenValiditySeconds;
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private XMLAssertionUnmarshaller unmarshaller;
	
	@Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.checkTokenAccess("hasAuthority('" + authorities + "')");
    }
	 
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints)
			throws Exception {
		endpoints.authenticationManager(authenticationManager);
		addSAMLTokenGranter(endpoints);
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients)
			throws Exception {
		clients.inMemory().withClient(clientId)
				.authorizedGrantTypes(SAMLTokenGranter.GRANT_TYPE)
				.authorities(authorities).scopes(scopes).secret(clientSecret)
				.accessTokenValiditySeconds(accessTokenValiditySeconds);
	}

	private void addSAMLTokenGranter(
			AuthorizationServerEndpointsConfigurer endpoints) {
		// Best way to do this until https://github.com/spring-projects/spring-security-oauth/issues/337 is resolved
		TokenGranter defaultTokenGranter = endpoints.getTokenGranter();
		List<TokenGranter> tokenGranters = new ArrayList<TokenGranter>();
		tokenGranters.add(defaultTokenGranter);
		tokenGranters.add(samlTokenGranter(endpoints));
		TokenGranter tokenGranter = new CompositeTokenGranter(tokenGranters);
		endpoints.tokenGranter(tokenGranter);
	}

	@Bean
	protected TokenGranter samlTokenGranter(
			AuthorizationServerEndpointsConfigurer endpoints) {
		return new SAMLTokenGranter(endpoints.getTokenServices(),
				endpoints.getClientDetailsService(),
				endpoints.getOAuth2RequestFactory(), unmarshaller);
	}

}
