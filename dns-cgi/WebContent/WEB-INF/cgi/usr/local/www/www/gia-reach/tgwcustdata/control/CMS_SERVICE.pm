# (C) Telstra 2001
#
# AUthor: Peter Marrinon (peterm@telstra.net)
#
# $Id: CMS_SERVICE.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $
###################################################################
# File        : CMS_SERVICE.pm
# Description : Passes parameters to CMS_SERVICE_DB.pm file from the GUI and call the appropiate functions.
# Parameters to be passed : serviceID,customer,description,bandwidth,Product code,CoS type,CoS values, portmode, aggregate_serviceid, billable options, 
# Author      : KETAKI THOMBARE (Infosys Tech. Ltd., Bangalore )
# Started On  : 09 July 2009
# Modification History :
#  Date               Name                 Change/Description
# ------------------------------------------------------------------
# 11-Aug-2009    Ketaki Popat Thombare    Baseline for EVPL
# 29-Aug-2009    Meena Prakash            Added changes for search EVPL 
# 14-Oct-2009	 Nisha Sasindran	  Added changes for mVPN: Add, Edit and Search
# 11-Nov-2009	 Sayantan Dutta		  Added user_log sub routine for tracking services
# 03-Feb-2010    Karuna Ballal            Added changes for EPL: Add, Edit, Search
# 17-Aug-2010    Stavan Shah              Added changes for VLA: Add,Edit, Search
# 09-Sep-2010    Karuna Ballal            Added changes for IP Transit: Add, Edit, Search
# 26-Oct-2010	 Kamalanand		  Added Changed for IPT Phase 5
# 14-Jun-2011    Varun Yadav              Added Changes for IP Transit Addendum-2 BHA Requirment 
####################################################################### 
package CMS_SERVICE;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_SERVICE_DB;
open(DEBUG,">/tmp/evpl.txt");
# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;

our @ISA = qw (Exporter);

our $error;

#Added by Karuna for IPT 5
my $debug = 1;
#Uncomment following line to disable debug logs, and vice versa
#$debug = 0;
if ($debug){open (IPT_DBG, ">>/data1/tmp_log/service_pm.log");}

sub generate_prefix_list {
  my $bgp_ip = shift @_;
  my $found = 0;

  my @wday = qw {sun mon tue wed thu fri sat};
  my @month = qw {Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec};

  my $output_dir = $OS_PARA::values{data_path}{value}."routing/prefix_list/";
  my $prefix_list = "";

  my $ops = CMS_SERVICE_DB::get_service_by_bgp_ip($bgp_ip);

  for my $j (0 .. $#{$ops}) {
    my @curr_gtime = gmtime(time);
    my $opshandle = $ops->[$j][0];

    my $today = "$month[$curr_gtime[4]]$curr_gtime[3]";
    my $count = $curr_gtime[6];

    my $file_status = `ls -l $output_dir$wday[$curr_gtime[6]]/$opshandle`;
    my @seg_status = split(/\s+/, $file_status);

#$prefix_list .= "@curr_gtime<br>\n";
#$prefix_list .= "$today,$seg_status[5]$seg_status[6]<br>";

    if ($today ne $seg_status[5].$seg_status[6]) {
	$count = $curr_gtime[6]-1;
    }

    for my $i (0 .. 2) {
#      my $prefix_file = "$output_dir$wday[$curr_gtime[6]--]/$opshandle";

      if ($wday[$count-$i] eq "sat") {
  	   $count--;
      }

      my $prefix_file = "$output_dir$wday[$count-$i]/$opshandle";

      if (-e $prefix_file) {
        $prefix_list .= "<b>Service: $opshandle</b><br>\n";
	open(PRE, "<$prefix_file");
        while(my $line = <PRE>) {
          chomp($line);
          $prefix_list .= "$line<br>\n";
        }
        $prefix_list .= "<br><hr><br>";
        $found++;
      }
    }
  }

  $CMS::html_values{bgp_ip}{value} = "$bgp_ip";
  if ($found > 0) {
    $CMS::html_values{prefix_list}{value} = "$prefix_list";
    $CMS::html_values{noresultsmessage}{value} = "";
  } else {
    $CMS::html_values{noresultsmessage}{value} = "No prefix list found.";
  }
  CMS::output_html("service_prefix_list");
}

sub generate_prefix_list_bha {
  my $bha_bgp_ip = shift @_;
  my $found = 0;

  my @wday = qw {sun mon tue wed thu fri sat};
  my @month = qw {Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec};

  my $output_dir = $OS_PARA::values{data_path}{value}."routing/prefix_list/";
  my $prefix_list = "";

  my $ops = CMS_SERVICE_DB::get_bha_service_by_bgp_ip($bha_bgp_ip);

  for my $j (0 .. $#{$ops}) {
  	my @curr_gtime = gmtime(time);
  	my $opshandle = $ops->[$j][0];

  	my $today = "$month[$curr_gtime[4]]$curr_gtime[3]";
  	my $count = $curr_gtime[6];
	my $str = "_primary_bha";
  	my $file_status = `ls -l $output_dir$wday[$curr_gtime[6]]/$opshandle$str`;
  	my @seg_status = split(/\s+/, $file_status);

  	if ($today ne $seg_status[5].$seg_status[6]) {
  		$count = $curr_gtime[6]-1;
  	}

  	for my $i (0 .. 2) {
  		if ($wday[$count-$i] eq "sat") {
  		$count--;
  		}

  		my $prefix_file = "$output_dir$wday[$count-$i]/$opshandle$str";
  		if (-e $prefix_file) {
  			$prefix_list .= "<b>Service: $opshandle</b><br>\n";
  			open(PRE, "<$prefix_file");
 			while(my $line = <PRE>) {
  				chomp($line);
  				$prefix_list .= "$line<br>\n";
  			}
  		$prefix_list .= "<br><hr><br>";
  		$found++;
  		}
  	}
  }
  $CMS::html_values{bha_bgp_ip}{value} = "$bha_bgp_ip";
  if ($found > 0) {
  	$CMS::html_values{prefix_list}{value} = "$prefix_list";
  	$CMS::html_values{noresultsmessage}{value} = "";
  } else {
  	$CMS::html_values{noresultsmessage}{value} = "No prefix list found.";
  }
  	CMS::output_html("bha_service_prefix_list");
 }

       ##++Addendum-2 BHA Requirment. 
       sub generate_prefix_list_bha_backup {
       my $bha_bgp_ip_backup = shift @_;
       my $found = 0;
    
       my @wday = qw {sun mon tue wed thu fri sat};
       my @month = qw {Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec};
    
       my $output_dir = $OS_PARA::values{data_path}{value}."routing/prefix_list/";
       my $prefix_list = "";
    
       my $ops = CMS_SERVICE_DB::get_bha_service_by_bgp_ip($bha_bgp_ip_backup);
    
       for my $j (0 .. $#{$ops}) {
             my @curr_gtime = gmtime(time);
             my $opshandle = $ops->[$j][0];
    
             my $today = "$month[$curr_gtime[4]]$curr_gtime[3]";
             my $count = $curr_gtime[6];
             my $str = "_backup_bha";
             my $file_status = `ls -l $output_dir$wday[$curr_gtime[6]]/$opshandle$str`;
             my @seg_status = split(/\s+/, $file_status);
    
             if ($today ne $seg_status[5].$seg_status[6]) {
                     $count = $curr_gtime[6]-1;
             }
    
             for my $i (0 .. 2) {
                     if ($wday[$count-$i] eq "sat") {
                     $count--;
                     }
    
                     my $prefix_file = "$output_dir$wday[$count-$i]/$opshandle$str";
                     if (-e $prefix_file) {
                             $prefix_list .= "<b>Service: $opshandle</b><br>\n";
                             open(PRE, "<$prefix_file");
                             while(my $line = <PRE>) {
                                     chomp($line);
                                     $prefix_list .= "$line<br>\n";
                             }
                     $prefix_list .= "<br><hr><br>";
                     $found++;
                     }
             }
       }
       $CMS::html_values{bha_bgp_ip_backup}{value} = "$bha_bgp_ip_backup";
       if ($found > 0) {
             $CMS::html_values{prefix_list}{value} = "$prefix_list";
             $CMS::html_values{noresultsmessage}{value} = "";
       } else {
             $CMS::html_values{noresultsmessage}{value} = "No prefix list found.";
       }
             CMS::output_html("bha_service_prefix_list_backup");
    }
    ##--Addendum 2 BHA Requirment.
# ++ Added user_log sub routine for tracking services - Sayantan dutta - 11-Nov-2009
sub user_log {

	my $login = '';
	$login = $CMS::html_values{'username'}{value};
	open(USER_LOG,">>/usr/local/www/wanchai/cms/userlogs/CMS.$login.service.log");

	my $date_time = `date "+%a %h %d %H-%M-%S UTC %Y"`;
	my $ops = shift @_;
	chomp($date_time);
	print USER_LOG ">>[Date = $date_time] ";
	print USER_LOG "[OPS = $ops] ";
	print USER_LOG "[SID = $CMS::html_values{service}{value}] ";
	print USER_LOG "[CUST = $CMS::html_values{accno}{value}] ";
	
	if(($ops =~ /Add Service/)||($ops =~ /Edit Service/)) {
		print USER_LOG "[DESC = $CMS::html_values{description}{value}] ";
		print USER_LOG "[BW = $CMS::html_values{bandwidth}{value}] ";
		print USER_LOG "[PROD = $CMS::html_values{productcode}{value}] ";
		if($CMS::html_values{productcode}{value} =~ /MPLS/) {
			print USER_LOG "[Polling Type = $CMS::html_values{pollingtype}{value}] ";
			print USER_LOG "[CoS Type = $CMS::html_values{costype}{value}] ";
			print USER_LOG "[Voice IN = $CMS::html_values{gold_rateacl}{value}] ";
			print USER_LOG "[Voice Out = $CMS::html_values{gold_classmap}{value}] ";
			print USER_LOG "[Video In = $CMS::html_values{class4_rateacl}{value}] ";
			print USER_LOG "[Video Out = $CMS::html_values{class4_classmap}{value}] ";
			print USER_LOG "[Critical In = $CMS::html_values{silver_rateacl}{value}] ";
			print USER_LOG "[Critical Out = $CMS::html_values{silver_classmap}{value}] ";
			print USER_LOG "[Interactive In = $CMS::html_values{class2_rateacl}{value}] ";
			print USER_LOG "[Interactive Out = $CMS::html_values{class2_classmap}{value}] ";
			print USER_LOG "[Standard In = $CMS::html_values{class1_rateacl}{value}] ";
			print USER_LOG "[Standard Out = $CMS::html_values{class1_classmap}{value}] ";
			print USER_LOG "[LPD In = $CMS::html_values{default_rateacl}{value}] ";
			print USER_LOG "[LPD Out = $CMS::html_values{default_classmap}{value}] ";
			if($CMS::html_values{mvpnCheck}{value} =~ /2/) {
				print USER_LOG "[Enable Multicast = No] ";
			}
			else {
				print USER_LOG "[Enable Multicast = Yes] ";
				print USER_LOG "[Multicast Voice IN = $CMS::html_values{voice_mvpn_in}{value}] ";
				print USER_LOG "[Multicast Video In = $CMS::html_values{video_mvpn_in}{value}] ";
				print USER_LOG "[Multicast Video Out = $CMS::html_values{video_mvpn_out}{value}] ";
				print USER_LOG "[Multicast Critical In = $CMS::html_values{criticaldata_mvpn_in}{value}] ";
				print USER_LOG "[Multicast Critical Out = $CMS::html_values{criticaldata_mvpn_out}{value}] ";
				print USER_LOG "[Multicast Interactive In = $CMS::html_values{interactivedata_mvpn_in}{value}] ";
				print USER_LOG "[Multicast Interactive Out = $CMS::html_values{interactivedata_mvpn_out}{value}] ";
				print USER_LOG "[Multicast Standard In = $CMS::html_values{standarddata_mvpn_in}{value}] ";
				print USER_LOG "[Multicast Standard Out = $CMS::html_values{standarddata_mvpn_out}{value}] ";
				print USER_LOG "[Multicast LPD In = $CMS::html_values{lowpriority_mvpn_in}{value}] ";
				print USER_LOG "[Multicast LPD Out = $CMS::html_values{lowpriority_mvpn_out}{value}] ";
			}
		}
		if($CMS::html_values{productcode}{value} =~ /VPLS/) {
			print USER_LOG "[Port Mode = $CMS::html_values{portmode}{value}] ";
			if($CMS::html_values{portmode}{value} =~ /VLV/) {
				print USER_LOG "[Voice IN = $CMS::html_values{gold_rateacl_v}{value}] ";
				print USER_LOG "[Voice Out = $CMS::html_values{gold_classmap_v}{value}] ";
				print USER_LOG "[Video In = $CMS::html_values{class4_rateacl_v}{value}] ";
				print USER_LOG "[Video Out = $CMS::html_values{class4_classmap_v}{value}] ";
				print USER_LOG "[Critical In = $CMS::html_values{silver_rateacl_v}{value}] ";
				print USER_LOG "[Critical Out = $CMS::html_values{silver_classmap_v}{value}] ";
				print USER_LOG "[Interactive In = $CMS::html_values{class2_rateacl_v}{value}] ";
				print USER_LOG "[Interactive Out = $CMS::html_values{class2_classmap_v}{value}] ";
				print USER_LOG "[Standard In = $CMS::html_values{class1_rateacl_v}{value}] ";
				print USER_LOG "[Standard Out = $CMS::html_values{class1_classmap_v}{value}] ";
				print USER_LOG "[LPD In = $CMS::html_values{default_rateacl_v}{value}] ";
				print USER_LOG "[LPD Out = $CMS::html_values{default_classmap_v}{value}] ";
			}
			print USER_LOG "[Master Service = $CMS::html_values{master_service}{value}] ";
		}
		if($CMS::html_values{productcode}{value} =~ /EVPL/) {
			print USER_LOG "[Type of Service = $CMS::html_values{portmode_e}{value}] ";
			if($CMS::html_values{portmode_e}{value} =~ /EVPL VLAN Service/) {
				print USER_LOG "[CoS Type = $CMS::html_values{costype_e}{value}] ";
				if($CMS::html_values{costype_e}{value} =~ /Standard/) {
					print USER_LOG "[Standard (A->Z) = $CMS::html_values{default_rateacl_e}{value}] ";
					print USER_LOG "[Standard (Z->A) = $CMS::html_values{default_classmap_e}{value}] ";
				}
				else {
					print USER_LOG "[Premium (A->Z) = $CMS::html_values{gold_rateacl_e}{value}] ";
					print USER_LOG "[Premium (Z->A) = $CMS::html_values{gold_classmap_e}{value}] ";
				}
				print USER_LOG "[Aggregate Service(A->Z) = $CMS::html_values{aggregate_service_a}{value}] ";
				print USER_LOG "[Aggregate Service(Z->A) = $CMS::html_values{aggregate_service_z}{value}] ";
			}
			else {
				print USER_LOG "[Switch Service = $CMS::html_values{aggregate_service_tpm}{value}] ";
			}
		}
		if($CMS::html_values{productcode}{value} =~ /GIA/) {
			print USER_LOG "[AS No. = $CMS::html_values{as_no}{value}] ";
			print USER_LOG "[Auto Gen Prefix = $CMS::html_values{auto_gen_prefix}{value}] ";
		}
		if($CMS::html_values{productcode}{value} =~ /GBS/) {
			print USER_LOG "[A-End Job ID = $CMS::html_values{aend_jobid}{value}] ";
			print USER_LOG "[Z-End Job ID = $CMS::html_values{zend_jobid}{value}] ";
			print USER_LOG "[A-End Job Note = $CMS::html_values{aend_jobno}{value}] ";
			print USER_LOG "[Z-End Job Note = $CMS::html_values{aend_jobno}{value}] ";
			print USER_LOG "[A-End Country Code = $CMS::html_values{aend_country}{value}] ";
			print USER_LOG "[Z-End Country Code = $CMS::html_values{zend_country}{value}] ";
		}
		if(($CMS::html_values{productcode}{value} =~ /ATM/) || ($CMS::html_values{productcode}{value} =~ /FR/)) {
			print USER_LOG "[Service ID = $CMS::html_values{serviceid_alt}{value}] ";
		}
		if (($CMS::html_values{productcode}{value} =~ /VPLS/) || ($CMS::html_values{productcode}{value} =~ /MPLS/) || ($CMS::html_values{productcode}{value} =~ /GIA/)) {
			print USER_LOG "[Aggregate Service = $CMS::html_values{aggregate_service}{value}] ";
		}
	}
	print USER_LOG "\n";
	close (USER_LOG);
}
# ++ Added user_log sub routine for tracking services - Sayantan dutta - 11-Nov-2009

sub start {

        # check billing options
        my $billerrmsg = '';
	open (IPT1, ">>/data1/tmp_log/addservice.log");
	print IPT1 "billable value: $CMS::html_values{'billable'}{value}\n";
	#Added by Karuna for IP Transit
	if ($CMS::html_values{productcode}{value} =~ /IPTRANSIT/ or $CMS::html_values{productcode}{value} =~ /ETHERNET/) {
		$CMS::html_values{'billable'}{value} = 2;
	}
        if ($CMS::html_values{'billable'}{value} <= 0) {
                $billerrmsg .= "Invalid Billable<br>";
        }
	#Added by Karuna for 30/31 days validations
        my $billstmth = $CMS::html_values{'billstartdate.month'}{value};
        my $billstday = $CMS::html_values{'billstartdate.day'}{value};
        my $billendmth = $CMS::html_values{'billenddate.month'}{value};
        my $billendday = $CMS::html_values{'billenddate.day'}{value};
        my @ar_mth = ('04','06','09','11');

        #Karuna- Check the no. of days in start date and end date
        foreach my $mth_check (@ar_mth) {
                if (($billstday == 31) && ($mth_check == $billstmth)) {
                        $billerrmsg .= "Invalid Billing Start Date <br>";
                } elsif (($billendday == 31) && ($mth_check == $billendmth)){
                        $billerrmsg .= "Invalid Billing End Date <br>";
                }
        }

        #Karuna - Special case for Feb
        if (($billstmth == 02) && ($billstday >= 30)){
                $billerrmsg .= "Invalid Billing Start Date <br>";
        } elsif (($billendmth == 02) && ($billendday  >= 30)){
                $billerrmsg .= "Invalid Billing End Date <br>";
        }

        # xxxdateok
        # 0 = FAIL, 1 = OK, 2 = ALL NULL
        my $billstartdateok = 0;
        my $billenddateok = 0;
        my ($tmpsd, $tmped);
        # start date
        $tmpsd  = $CMS::html_values{'billstartdate.year'}{value};
        $tmpsd .= $CMS::html_values{'billstartdate.month'}{value};
        $tmpsd .= $CMS::html_values{'billstartdate.day'}{value};
        #$tmpsd .= $CMS::html_values{'billstarttime.hour'}{value};
        #$tmpsd .= $CMS::html_values{'billstarttime.minute'}{value};
        #$tmpsd .= $CMS::html_values{'billstarttime.second'}{value};
        #if ($tmpsd =~ /^--------------$/) {
        if ($tmpsd =~ /^--------$/) {
                $billstartdateok = 2;
        } elsif ($tmpsd =~ /-/) {
                $billstartdateok = 0;
        } else {
                $billstartdateok = 1;
        }
        # end date
        $tmped  = $CMS::html_values{'billenddate.year'}{value};
        $tmped .= $CMS::html_values{'billenddate.month'}{value};
        $tmped .= $CMS::html_values{'billenddate.day'}{value};
        #$tmped .= $CMS::html_values{'billendtime.hour'}{value};
        #$tmped .= $CMS::html_values{'billendtime.minute'}{value};
        #$tmped .= $CMS::html_values{'billendtime.second'}{value};
        #if ($tmped =~ /^--------------$/) {
        if ($tmped =~ /^--------$/) {
                $billenddateok = 2;
        } elsif ($tmped =~ /-/) {
                $billenddateok = 0;
        } else {
                $billenddateok = 1;
        }
        #
        #if ( ($billstartdateok==1) && ($billenddateok==1) && ($tmped <= $tmpsd) ) {
	print IPT1 "tmpsd:$tmpsd\ttmped:$tmped\nbillstartdateok:$billstartdateok\tbillenddateok:$billenddateok\n";
        if ( ($billstartdateok==1) && ($billenddateok==1) && ($tmped < $tmpsd) ) {
                $billerrmsg .= "Invalid Billing End Date &amp; Time - it must be later than Billing Start Date &amp; Time<br>";
        }
        if ( $billstartdateok==0 ) {
                $billerrmsg .= "Invalid Billing Start Date &amp; Time<br>";
        }
        if ( $billenddateok==0 ) {
                $billerrmsg .= "Invalid Billing End Date &amp; Time<br>";
        }
        #Karuna - Added check when the prod code is not EPL or IP Transit
	print IPT1 "tmpsd:$tmpsd\ttmped:$tmped\nbillstartdateok:$billstartdateok\tbillenddateok:$billenddateok\n";
        if (($CMS::html_values{'billable'}{value} == 1) && ($CMS::html_values{productcode}{value} !~ /EPL/)){
                if ( $billstartdateok == 2 ) {
                        $billerrmsg .= "Invalid Billing Start Date &amp; Time - it must be filled when service is usage based billing<br>";
                }
        }
        #Karuna - Added check when the prod code is not EPL and service is point to point
        if (($CMS::html_values{'epl_billable'}{value} == 1) && ($CMS::html_values{productcode}{value} =~ /EPL/) && ($CMS::html_values{service}{value} =~ m/ew/gi)){
                if ( $billstartdateok == 2 ) {
                        $billerrmsg .= "Invalid Billing Start Date &amp; Time - it must be filled when service is usage based billing<br>";
                }
        }
        #Karuna - Added check when the prod code is not EPL and service is point to multipoint
        if (($CMS::html_values{'epl_billable2'}{value} == 1) && ($CMS::html_values{productcode}{value} =~ /EPL/) && ($CMS::html_values{service}{value} =~ m/em/gi)){
                if ( $billstartdateok == 2 ) {
                        $billerrmsg .= "Invalid Billing Start Date &amp; Time - it must be filled when service is usage based billing<br>";
                }
        }


  print IPT1 "before end of Start routine:billstartdateok:$billstartdateok\tbillenddateok:$billenddateok\n";
  print IPT1 "before end of Start routine:billable value:$CMS::html_values{'billable'}{value}\n";

  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value} || defined $CMS::html_values{submit1}{value}) &&
       ($CMS::html_values{submit}{value} =~ /cancel/i || $CMS::html_values{submit1}{value} =~ /cancel/i))) {
    # default to view_current_outages

    CMS::clear_html_values();
    $CMS::html_values{subsystem}{value} = "service";
    $CMS::html_values{command}{value} = "add_service";
    
  }

  if (defined $CMS::html_values{stage}{value}) {
    unless (CMS::check_screen($CMS::html_values{current_screen}{value})){
      $CMS::html_values{stage}{value}--;
    }
  }

  if ($CMS::html_values{command}{value} eq "add_service") {
	open (IPT1,">>/data1/tmp_log/addservice.log");
	print IPT1 "entered add service\n"; 
	print IPT1 "command:$CMS::html_values{command}{value}\n"; 
	print IPT1 "stage initial:$CMS::html_values{stage}{value}\n"; 
    # add a new service
    if (not defined ($CMS::html_values{stage}{value})) {
	print IPT1 "entered add service first stage\n"; 
      # first stage
      $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();      
      $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
      $CMS::html_values{portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
      $CMS::html_values{costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
      $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
      $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();      
      $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();      
      $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();      
      $CMS::html_values{bha_auto_gen_prefix}{options} = CMS_SERVICE_DB::get_bha_auto_gen_prefix();
      $CMS::html_values{black_hole_routing}{options} = CMS_SERVICE_DB::get_black_hole_routing_option();
      $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();      
      $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();      
      #$CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();    
      $CMS::html_values{aggregate_service_a}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();    
      $CMS::html_values{aggregate_service_z}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();    
      $CMS::html_values{aggregate_service_tpm}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_tpm();    
      unshift(@{$CMS::html_values{aggregate_service_a}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_z}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_tpm}{options}}, [""]);
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
#      $CMS::html_values{serviceclass}{options} = CMS_SERVICE_DB::get_serviceclass();
	$CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
        #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);
        
        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();

        # setup billing option
        if (not defined ($CMS::html_values{'billable'}{value})) {
                $CMS::html_values{'billable'}{value} = '1';
        }
        if (not defined ($CMS::html_values{'epl_billable'}{value})) {
                $CMS::html_values{'billable'}{value} = '1';
        }
        if (not defined ($CMS::html_values{'epl_billable2'}{value})) {
                $CMS::html_values{'billable'}{value} = '1';
        }
        # setup date and time, all dash
        $CMS::html_values{'billstartdate.year'}{value}  = '----';
        $CMS::html_values{'billstartdate.month'}{value} = '--';
        $CMS::html_values{'billstartdate.day'}{value}   = '--';
        $CMS::html_values{'billenddate.year'}{value}  = '----';
        $CMS::html_values{'billenddate.month'}{value} = '--';
        $CMS::html_values{'billenddate.day'}{value}   = '--';

	print IPT1 "stage:$CMS::html_values{stage}{value}\n"; 
      CMS::output_html("service_add");
    } elsif ($CMS::html_values{stage}{value} == 2) {
	open (IPT1,">>/data1/tmp_log/addservice.log");
	print IPT1 "entered add service2\n"; 

      $CMS::html_values{service}{value} =~ tr/A-Z/a-z/;
      #modified by Karuna to add a check for EPL as well
      if($CMS::html_values{productcode}{value} !~ /(EVPL|EPL)/){
      $CMS::html_values{service}{value} =~ s/( |\[|\])//g;
      }

       #++EVPL
        $CMS::html_values{portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
        $CMS::html_values{costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
        $CMS::html_values{aggregate_service_a}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
        $CMS::html_values{aggregate_service_z}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
        $CMS::html_values{aggregate_service_tpm}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_tpm();
        unshift(@{$CMS::html_values{aggregate_service_a}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_z}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_tpm}{options}}, [""]);
        #--EVPL

    if (($CMS::html_values{gold_classmap}{value} ne "" && $CMS::html_values{gold_classmap}{value} =~ / /) || 
($CMS::html_values{gold_rateacl}{value} ne "" && $CMS::html_values{gold_rateacl}{value} =~ / /) ||
($CMS::html_values{silver_classmap}{value} ne "" && $CMS::html_values{silver_classmap}{value} =~ / /) ||
($CMS::html_values{silver_rateacl}{value} ne "" && $CMS::html_values{silver_rateacl}{value} =~ / /) ||
($CMS::html_values{default_classmap}{value} ne "" && $CMS::html_values{default_classmap}{value} =~ / /) ||
($CMS::html_values{default_rateacl}{value} ne "" && $CMS::html_values{default_rateacl}{value} =~ / /) ||
($CMS::html_values{class1_classmap}{value} ne "" && $CMS::html_values{class1_classmap}{value} =~ / /) ||
($CMS::html_values{class1_rateacl}{value} ne "" && $CMS::html_values{class1_rateacl}{value} =~ / /) ||
($CMS::html_values{class2_classmap}{value} ne "" && $CMS::html_values{class2_classmap}{value} =~ / /) ||
($CMS::html_values{class2_rateacl}{value} ne "" && $CMS::html_values{class2_rateacl}{value} =~ / /) ||
($CMS::html_values{class4_classmap}{value} ne "" && $CMS::html_values{class4_classmap}{value} =~ / /) ||
($CMS::html_values{class4_rateacl}{value} ne "" && $CMS::html_values{class4_rateacl}{value} =~ / /) ||
($CMS::html_values{gold_classmap_v}{value} ne "" && $CMS::html_values{gold_classmap_v}{value} =~ / /) ||
($CMS::html_values{gold_rateacl_v}{value} ne "" && $CMS::html_values{gold_rateacl_v}{value} =~ / /) ||
($CMS::html_values{silver_classmap_v}{value} ne "" && $CMS::html_values{silver_classmap_v}{value} =~ / /) ||
($CMS::html_values{silver_rateacl_v}{value} ne "" && $CMS::html_values{silver_rateacl_v}{value} =~ / /) ||
($CMS::html_values{default_classmap_v}{value} ne "" && $CMS::html_values{default_classmap_v}{value} =~ / /) ||
($CMS::html_values{default_rateacl_v}{value} ne "" && $CMS::html_values{default_rateacl_v}{value} =~ / /) ||
($CMS::html_values{class1_classmap_v}{value} ne "" && $CMS::html_values{class1_classmap_v}{value} =~ / /) ||
($CMS::html_values{class1_rateacl_v}{value} ne "" && $CMS::html_values{class1_rateacl_v}{value} =~ / /) ||
($CMS::html_values{class2_classmap_v}{value} ne "" && $CMS::html_values{class2_classmap_v}{value} =~ / /) ||
($CMS::html_values{class2_rateacl_v}{value} ne "" && $CMS::html_values{class2_rateacl_v}{value} =~ / /) ||
($CMS::html_values{class4_classmap_v}{value} ne "" && $CMS::html_values{class4_classmap_v}{value} =~ / /) ||
($CMS::html_values{class4_rateacl_v}{value} ne "" && $CMS::html_values{class4_rateacl_v}{value} =~ / /) ||
($CMS::html_values{gold_classmap_e}{value} ne "" && $CMS::html_values{gold_classmap_e}{value} =~ / /) ||
($CMS::html_values{gold_rateacl_e}{value} ne "" && $CMS::html_values{gold_rateacl_e}{value} =~ / /) ||
($CMS::html_values{default_classmap_e}{value} ne "" && $CMS::html_values{default_classmap_e}{value} =~ / /) ||
($CMS::html_values{default_rateacl_e}{value} ne "" && $CMS::html_values{default_rateacl_e}{value} =~ / /)

) {
	# Failed to add service
	CMS::add_error("Failed to add service.");
	CMS::add_error("Space is not allowed in CoS Fields");
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();      
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        # switchcode for fr sla reports
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();      

        #EPL options
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);
        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();

	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
#        $CMS::html_values{serviceclass}{options} = CMS_SERVICE_DB::get_serviceclass();
	if ($CMS::html_values{gold_classmap}{value} =~ / /) {
 	  $CMS::html_values{gold_classmap}{value} = "";
	} 
	if ($CMS::html_values{gold_rateacl}{value} =~ / /) {
	  $CMS::html_values{gold_rateacl}{value} = "";
	}
	if ($CMS::html_values{silver_classmap}{value} =~ / /) {
 	  $CMS::html_values{silver_classmap}{value} = "";
	}
	if ($CMS::html_values{silver_rateacl}{value} =~ / /) {
	  $CMS::html_values{silver_rateacl}{value} = "";
	}
	if ($CMS::html_values{default_classmap}{value} =~ / /) {
 	  $CMS::html_values{default_classmap}{value} = "";
	} 
	if ($CMS::html_values{default_rateacl}{value} =~ / /) {
	  $CMS::html_values{default_rateacl}{value} = "";
	}
	# 10/8/06
	if ($CMS::html_values{class1_rateacl}{value} =~ / /) {
          $CMS::html_values{class1_rateacl}{value} = "";
        }
	if ($CMS::html_values{class1_classmap}{value} =~ / /) {
          $CMS::html_values{class1_classmap}{value} = "";
        }
	if ($CMS::html_values{class2_rateacl}{value} =~ / /) {
          $CMS::html_values{class2_rateacl}{value} = "";
        }
        if ($CMS::html_values{class2_classmap}{value} =~ / /) {
          $CMS::html_values{class2_classmap}{value} = "";
        }
	if ($CMS::html_values{class4_rateacl}{value} =~ / /) {
          $CMS::html_values{class4_rateacl}{value} = "";
        }
        if ($CMS::html_values{class4_classmap}{value} =~ / /) {
          $CMS::html_values{class4_classmap}{value} = "";
        }
	
        if ($CMS::html_values{gold_classmap_v}{value} =~ / /) {
          $CMS::html_values{gold_classmap_v}{value} = "";
        }
        if ($CMS::html_values{gold_rateacl_v}{value} =~ / /) {
          $CMS::html_values{gold_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{silver_classmap_v}{value} =~ / /) {
          $CMS::html_values{silver_classmap_v}{value} = "";
        }
        if ($CMS::html_values{silver_rateacl_v}{value} =~ / /) {
          $CMS::html_values{silver_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{default_classmap_v}{value} =~ / /) {
          $CMS::html_values{default_classmap_v}{value} = "";
        }
        if ($CMS::html_values{default_rateacl_v}{value} =~ / /) {
          $CMS::html_values{default_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{class1_rateacl_v}{value} =~ / /) {
          $CMS::html_values{class1_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{class1_classmap_v}{value} =~ / /) {
          $CMS::html_values{class1_classmap_v}{value} = "";
        }
        if ($CMS::html_values{class2_rateacl_v}{value} =~ / /) {
          $CMS::html_values{class2_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{class2_classmap_v}{value} =~ / /) {
          $CMS::html_values{class2_classmap_v}{value} = "";
        }
        if ($CMS::html_values{class4_rateacl_v}{value} =~ / /) {
          $CMS::html_values{class4_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{class4_classmap_v}{value} =~ / /) {
          $CMS::html_values{class4_classmap_v}{value} = "";
        }
	if ($CMS::html_values{gold_classmap_e}{value} =~ / /) {
          $CMS::html_values{gold_classmap_e}{value} = "";
        }
        if ($CMS::html_values{gold_rateacl_e}{value} =~ / /) {
          $CMS::html_values{gold_rateacl_e}{value} = "";
        }
	if ($CMS::html_values{default_classmap_e}{value} =~ / /) {
          $CMS::html_values{default_classmap_e}{value} = "";
        }
        if ($CMS::html_values{default_rateacl_e}{value} =~ / /) {
          $CMS::html_values{default_rateacl_e}{value} = "";
        }

        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();      
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
	$CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);


	CMS::output_html("service_add");
    } elsif ($CMS::html_values{service}{value} =~ /[^\w\s\[\]\-\:\.\_]/) { 
        CMS::add_error("Failed to add service.");
        CMS::add_error("Invalid Service");

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

        CMS::output_html("service_add");
        }elsif (($CMS::html_values{productcode}{value} =~ /MPLS/) && ($CMS::html_values{costype}{value} eq "")) {
        CMS::add_error("Failed to add service.");
        CMS::add_error("IPVPN Router Configuration Type - COS type is required");

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        #++EVPL 	
        $CMS::html_values{portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
        $CMS::html_values{costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
        #--EVPL
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

        CMS::output_html("service_add");
    } elsif (($CMS::html_values{productcode}{value} =~ /MPLS/) && ($CMS::html_values{pollingtype}{value} eq "") && (($CMS::html_values{gold_classmap}{value} ne "") || ($CMS::html_values{gold_rateacl}{value} ne "") || ($CMS::html_values{silver_classmap}{value} ne "") || ($CMS::html_values{silver_rateacl}{value} ne "") || ($CMS::html_values{default_classmap}{value} ne "") || ($CMS::html_values{default_rateacl}{value} ne "") || ($CMS::html_values{class1_classmap}{value} ne "") || ($CMS::html_values{class1_rateacl}{value} ne "") || ($CMS::html_values{class2_classmap}{value} ne "") || ($CMS::html_values{class2_rateacl}{value} ne "") || ($CMS::html_values{class4_classmap}{value} ne "") || ($CMS::html_values{class4_rateacl}{value} ne ""))) {
        CMS::add_error("Failed to add service.");
        CMS::add_error("RateACL No. / ClassMap Name does not require.");
        CMS::add_error("Please leave blank for:<br> - [IN] RateACL No. / ClassMap Name<br> - [OUT] ClassMap Name");

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

        CMS::output_html("service_add");
    } elsif (($CMS::html_values{productcode}{value} =~ /MPLS/) && ($CMS::html_values{pollingtype}{value} =~ /RATEACL/) && ($CMS::html_values{gold_rateacl}{value} eq "") && ($CMS::html_values{silver_rateacl}{value} eq "") && ($CMS::html_values{default_rateacl}{value} eq "") && ($CMS::html_values{class1_rateacl}{value} eq "") && ($CMS::html_values{class2_rateacl}{value} eq "") && ($CMS::html_values{class4_rateacl}{value} eq "")) {
        CMS::add_error("Failed to add service.");
        CMS::add_error("RateACL No. is required");

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

        CMS::output_html("service_add");
    } elsif (($CMS::html_values{productcode}{value} =~ /MPLS/) && ($CMS::html_values{pollingtype}{value} =~ /CLASSMAP/) && ($CMS::html_values{gold_classmap}{value} eq "") && ($CMS::html_values{gold_rateacl}{value} eq "") && ($CMS::html_values{silver_classmap}{value} eq "") && ($CMS::html_values{silver_rateacl}{value} eq "") && ($CMS::html_values{default_classmap}{value} eq "") && ($CMS::html_values{default_rateacl}{value} eq "") && ($CMS::html_values{class1_classmap}{value} eq "") && ($CMS::html_values{class1_rateacl}{value} eq "") && ($CMS::html_values{class2_classmap}{value} eq "") && ($CMS::html_values{class2_rateacl}{value} eq "") && ($CMS::html_values{class4_classmap}{value} eq "") && ($CMS::html_values{class4_rateacl}{value} eq "")) {
        CMS::add_error("Failed to add service.");
        CMS::add_error("ClassMap Name is required");

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        CMS::output_html("service_add");
	#CRQ000000005308 
}elsif (($CMS::html_values{productcode}{value} =~ /VPLS/) && ($CMS::html_values{bandwidth}{value} ne "")){
        CMS::add_error("Bandwidth field is mandatory");
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
	$CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
        $CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

        CMS::output_html("service_add");
    }
elsif ( ($CMS::html_values{as_no}{value} < 0) || ($CMS::html_values{as_no}{value} =~ m/[a-z]|[A-Z]/) || (($CMS::html_values{as_no}{value} >= 64512) && ($CMS::html_values{as_no}{value} <= 65535))) {
	CMS::add_error("Failed to add service.");
	CMS::add_error("Invalid AS number");
 
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();      
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();      
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	 $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();      
	$CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

	CMS::output_html("service_add");
    } elsif ($CMS::html_values{bgp_neighbor_ip}{value} =~ /^(0\.|10\.|14\.|24\.|39\.|127\.|128\.0\.|169.254\.|191\.255\.|192\.0\.0\.|192\.0\.2\.|192\.168\.|255\.255\.255\.)|^(172\.(1[6-9]|2[0-9]|31)\.)|^(2(2[4-9]|3[0-9]|4[0-9]|5[0-5])\.)/) {
	CMS::add_error("Failed to add service.");
	CMS::add_error("BGP negihbor IP is a private / experimental IP");
 
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();      
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();      
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();      
	$CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();


        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

	CMS::output_html("service_add");
    } elsif ($billerrmsg) {
        # Invalid billing option
        CMS::add_error("Failed to add service.");
        CMS::add_error("$billerrmsg");
         
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        #++EVPL
	$CMS::html_values{portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
        $CMS::html_values{costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
        #--EVPL 
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();     
        #++EVPL 
        $CMS::html_values{aggregate_service_a}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
        $CMS::html_values{aggregate_service_z}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
        $CMS::html_values{aggregate_service_tpm}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_tpm();
	unshift(@{$CMS::html_values{aggregate_service_a}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_z}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_tpm}{options}}, [""]);
        #--EVPL  
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #EPL
        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

        CMS::output_html("service_add");
    } elsif (($CMS::html_values{productcode}{value} =~ /(ATM|FR)/) && ($CMS::html_values{serviceid_alt}{value} eq "")) {
        CMS::add_error("Failed to add service.");
        CMS::add_error("Service ID for ATM/FR is required");
        
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

        CMS::output_html("service_add");
	#Bandwidth validation for EPL
     } elsif (($CMS::html_values{productcode}{value} =~ /EPL/) && ($CMS::html_values{bandwidth}{value} eq "")) {
        CMS::add_error("Failed to add service.");
        CMS::add_error("Bandwidth field is mandatory for EPL services");
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);
        CMS::output_html("service_add");
	#Added new telstra id condition-TGC0031549
	} elsif (($CMS::html_values{productcode}{value} =~ /EPL/) && ($CMS::html_values{service}{value} !~ m/epl/gi) && ($CMS::html_values{epl_aend_jobno2}{value} =~ /^ea/i)) {	        
	CMS::add_error("Failed to add service.");
        CMS::add_error("A-end job note must begin with MA and should be of the format MA_SNG_6NTP_EM_1234");
	$CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);
        CMS::output_html("service_add"); 
    } else {
      #modified by Karuna to add a check for EPL as well
      if($CMS::html_values{productcode}{value} !~ /(EVPL|EPL)/){
      $CMS::html_values{service}{value} =~ s/( |\[|\])//g;
      }
      $CMS::html_values{'aend_jobno'}{value} =~ tr/a-z/A-Z/;
      $CMS::html_values{'zend_jobno'}{value} =~ tr/a-z/A-Z/;
      $CMS::html_values{'aend_country'}{value} =~ tr/a-z/A-Z/;
      $CMS::html_values{'zend_country'}{value} =~ tr/a-z/A-Z/;
      my $return_result = "";
      if ($CMS::html_values{productcode}{value} =~ /VPLS/){
          if($CMS::html_values{portmode}{value} =~ /TRANS/){
                $CMS::html_values{aggregate_service}{value}='';
          }
          $return_result = CMS_SERVICE_DB::add_service
	
	  ($CMS::html_values{service}{value},
	   $CMS::html_values{accno}{value},
	   $CMS::html_values{description}{value},
	   $CMS::html_values{bandwidth}{value},
	   $CMS::html_values{productcode}{value},
	   $CMS::html_values{switchsourcode}{value},
	   $CMS::html_values{switchdestcode}{value},
	   $CMS::html_values{pollingtype}{value},
	   $CMS::html_values{costype}{value},
	   $CMS::html_values{costype_e}{value},
	   $CMS::html_values{portmode}{value},
	   $CMS::html_values{portmode_e}{value},
	   $CMS::html_values{gold_classmap_v}{value},
	   $CMS::html_values{gold_rateacl_v}{value},
	   $CMS::html_values{silver_classmap_v}{value},
	   $CMS::html_values{silver_rateacl_v}{value},
	   $CMS::html_values{default_classmap_v}{value},
	   $CMS::html_values{default_rateacl_v}{value},
	   $CMS::html_values{class1_classmap_v}{value},
           $CMS::html_values{class1_rateacl_v}{value},
           $CMS::html_values{class2_classmap_v}{value},
           $CMS::html_values{class2_rateacl_v}{value},
           $CMS::html_values{class4_classmap_v}{value},
           $CMS::html_values{class4_rateacl_v}{value},
	   $CMS::html_values{as_no}{value},
	   $CMS::html_values{auto_gen_prefix}{value},
	   $CMS::html_values{bgp_neighbor_ip}{value},
           $CMS::html_values{irrd_object}{value},
	   $CMS::html_values{aggregate_service}{value},
	   $CMS::html_values{aggregate_service_a}{value},
           $CMS::html_values{aggregate_service_z}{value},
	   $CMS::html_values{aggregate_service_tpm}{value},
	   $CMS::html_values{master_service}{value},
	   $CMS::html_values{billable}{value},
           $CMS::html_values{'billstartdate.year'}{value},
           $CMS::html_values{'billstartdate.month'}{value},
           $CMS::html_values{'billstartdate.day'}{value},
           '00',
           '00',
           '00',
           $CMS::html_values{'billenddate.year'}{value},
           $CMS::html_values{'billenddate.month'}{value},
           $CMS::html_values{'billenddate.day'}{value},
           '23',
           '59',
           '59',
           $CMS::html_values{'serviceid_alt'}{value},
	   $CMS::html_values{'aend_jobid'}{value},
	   $CMS::html_values{'zend_jobid'}{value},
	   $CMS::html_values{'aend_jobno'}{value},
	   $CMS::html_values{'zend_jobno'}{value},
	   $CMS::html_values{'aend_country'}{value},
	   $CMS::html_values{'zend_country'}{value}
           );
       }
       #++EVPL -- Calling add_service function for EVPL and passing parameters from GUI to CMS_SERVICE_DB.pm 
       elsif($CMS::html_values{productcode}{value} =~ /EVPL/){
        #$CMS::html_values{service}{value} =~ tr/A-Z/a-z/; 
	if($CMS::html_values{portmode_e}{value} =~ /EVPL Transparent Service/){
	$CMS::html_values{costype_e}{value}='';
	$CMS::html_values{aggregate_service_a}{value}='';
	$CMS::html_values{aggregate_service_z}{value}='';
        }
	$return_result = CMS_SERVICE_DB::add_service	

	   ($CMS::html_values{service}{value},
           $CMS::html_values{accno}{value},
           $CMS::html_values{description}{value},
           $CMS::html_values{bandwidth}{value},
           $CMS::html_values{productcode}{value},
           $CMS::html_values{switchsourcode}{value},
           $CMS::html_values{switchdestcode}{value},
           $CMS::html_values{pollingtype}{value},
           $CMS::html_values{costype}{value},
	   $CMS::html_values{costype_e}{value},
           $CMS::html_values{portmode}{value},
           $CMS::html_values{portmode_e}{value},		
           $CMS::html_values{gold_classmap_e}{value},
           $CMS::html_values{gold_rateacl_e}{value},
           $CMS::html_values{silver_classmap}{value},
           $CMS::html_values{silver_rateacl_v}{value},
           $CMS::html_values{default_classmap_e}{value},
           $CMS::html_values{default_rateacl_e}{value},
           $CMS::html_values{class1_classmap}{value},
           $CMS::html_values{class1_rateacl}{value},
           $CMS::html_values{class2_classmap}{value},
           $CMS::html_values{class2_rateacl}{value},
           $CMS::html_values{class4_classmap}{value},
           $CMS::html_values{class4_rateacl}{value},
           $CMS::html_values{as_no}{value},
           $CMS::html_values{auto_gen_prefix}{value},
           $CMS::html_values{bgp_neighbor_ip}{value},
           $CMS::html_values{irrd_object}{value},
           $CMS::html_values{aggregate_service}{value},
	   $CMS::html_values{aggregate_service_a}{value},
           $CMS::html_values{aggregate_service_z}{value},
	   $CMS::html_values{aggregate_service_tpm}{value},
           $CMS::html_values{master_service}{value},
           $CMS::html_values{billable}{value},
           $CMS::html_values{'billstartdate.year'}{value},
           $CMS::html_values{'billstartdate.month'}{value},
           $CMS::html_values{'billstartdate.day'}{value},
           '00',
           '00',
           '00',
           $CMS::html_values{'billenddate.year'}{value},
           $CMS::html_values{'billenddate.month'}{value},
	   $CMS::html_values{'billenddate.day'}{value},
           '23',
           '59',
           '59',
           $CMS::html_values{'serviceid_alt'}{value},
           $CMS::html_values{'aend_jobid'}{value},
           $CMS::html_values{'zend_jobid'}{value},
           $CMS::html_values{'aend_jobno'}{value},
           $CMS::html_values{'zend_jobno'}{value},
           $CMS::html_values{'aend_country'}{value},
           $CMS::html_values{'zend_country'}{value}
          );
	}
#Added by Nisha for MVPN on 25Sep2009
        elsif($CMS::html_values{productcode}{value} =~ /MPLS/) {
	
        $return_result = CMS_SERVICE_DB::add_service        
          ($CMS::html_values{service}{value},
           $CMS::html_values{accno}{value},
           $CMS::html_values{description}{value},
           $CMS::html_values{bandwidth}{value},
           $CMS::html_values{productcode}{value},
           $CMS::html_values{switchsourcode}{value},
           $CMS::html_values{switchdestcode}{value},
           $CMS::html_values{pollingtype}{value},
           $CMS::html_values{costype}{value},
           $CMS::html_values{costype_e}{value},
           $CMS::html_values{portmode}{value},
           $CMS::html_values{portmode_e}{value},
           $CMS::html_values{gold_classmap}{value},
           $CMS::html_values{gold_rateacl}{value},
           $CMS::html_values{silver_classmap}{value},
           $CMS::html_values{silver_rateacl}{value},
           $CMS::html_values{default_classmap}{value},
           $CMS::html_values{default_rateacl}{value},
           $CMS::html_values{class1_classmap}{value},
           $CMS::html_values{class1_rateacl}{value},
           $CMS::html_values{class2_classmap}{value},
           $CMS::html_values{class2_rateacl}{value},
           $CMS::html_values{class4_classmap}{value},
           $CMS::html_values{class4_rateacl}{value},
           $CMS::html_values{as_no}{value},
           $CMS::html_values{auto_gen_prefix}{value},
           $CMS::html_values{bgp_neighbor_ip}{value},
           $CMS::html_values{irrd_object}{value},
           $CMS::html_values{aggregate_service}{value},
           $CMS::html_values{aggregate_service_a}{value},
           $CMS::html_values{aggregate_service_z}{value},
           $CMS::html_values{aggregate_service_tpm}{value},
           $CMS::html_values{master_service}{value},
           $CMS::html_values{billable}{value},
           $CMS::html_values{'billstartdate.year'}{value},
           $CMS::html_values{'billstartdate.month'}{value},
           $CMS::html_values{'billstartdate.day'}{value},
           '00',
           '00',
           '00',
           $CMS::html_values{'billenddate.year'}{value},
           $CMS::html_values{'billenddate.month'}{value},
           $CMS::html_values{'billenddate.day'}{value},
           '23',
           '59',
           '59',
           $CMS::html_values{'serviceid_alt'}{value},
           $CMS::html_values{'aend_jobid'}{value},
           $CMS::html_values{'zend_jobid'}{value},
           $CMS::html_values{'aend_jobno'}{value},
           $CMS::html_values{'zend_jobno'}{value},
           $CMS::html_values{'aend_country'}{value},
           $CMS::html_values{'zend_country'}{value},
	   $CMS::html_values{'voice_mvpn_in'}{value},
	   $CMS::html_values{'video_mvpn_in'}{value},
	   $CMS::html_values{'video_mvpn_out'}{value},
	   $CMS::html_values{'criticaldata_mvpn_in'}{value},
	   $CMS::html_values{'criticaldata_mvpn_out'}{value},
	   $CMS::html_values{'interactivedata_mvpn_in'}{value},
	   $CMS::html_values{'interactivedata_mvpn_out'}{value},
	   $CMS::html_values{'standarddata_mvpn_in'}{value},
	   $CMS::html_values{'standarddata_mvpn_out'}{value},
	   $CMS::html_values{'lowpriority_mvpn_in'}{value},
	   $CMS::html_values{'lowpriority_mvpn_out'}{value},
           $CMS::html_values{'mvpnCheck'}{value}
          );
#End of code Added by Nisha for MVPN on 25Sep2009
#Code added by Karuna for EPL Add Service on 09Feb2010
        } elsif($CMS::html_values{productcode}{value} =~ /EPL/) {
        #open (EPL1, ">>/home/h999680/EPL/tmp/addservice.log");
        open (EPL1, ">>/data1/tmp_log/addservice.log");
        #print EPL1 "service:$CMS::html_values{service}{value}\taccno:$CMS::html_values{accno}{value}\tmsid:$CMS::html_values{'epl_msid'}{value}\taendjid:$CMS::html_values{'epl_aend_jobid'}{value}\taendjno$CMS::html_values{'epl_aend_jobno'}{value}\tzendjno:$CMS::html_values{'epl_zend_jobno'}{value}\taendjno2:$CMS::html_values{'epl_aend_jobno2'}{value}\tzendjno2:$CMS::html_values{'epl_zend_jobno2'}{value}\n";
        $return_result = CMS_SERVICE_DB::add_service
          ($CMS::html_values{service}{value},
           $CMS::html_values{accno}{value},
           $CMS::html_values{description}{value},
           $CMS::html_values{bandwidth}{value},
           $CMS::html_values{productcode}{value},
           $CMS::html_values{epl_msid}{value},
           $CMS::html_values{'epl_aend_jobid'}{value},
           $CMS::html_values{'epl_zend_jobid'}{value},
           $CMS::html_values{'epl_aend_jobno'}{value},
           $CMS::html_values{'epl_zend_jobno'}{value},
           $CMS::html_values{'epl_aend_jobid2'}{value},
           $CMS::html_values{'epl_zend_jobid2'}{value},
           $CMS::html_values{'epl_aend_jobno2'}{value},
           $CMS::html_values{'epl_zend_jobno2'}{value},
           $CMS::html_values{'epl_billable'}{value},
           $CMS::html_values{'epl_billable2'}{value},
           $CMS::html_values{'billstartdate.year'}{value},
           $CMS::html_values{'billstartdate.month'}{value},
           $CMS::html_values{'billstartdate.day'}{value},
           '00',
           '00',
           '00',
           $CMS::html_values{'billenddate.year'}{value},
           $CMS::html_values{'billenddate.month'}{value},
           $CMS::html_values{'billenddate.day'}{value},
           '23',
           '59',
           '59'
          );
	#End of code Added by Karuna for EPL Add Service


	#Code added by karuna for IP Transit ADD service
	#Code modified for IP Transit phase 5 by Kamal
        } elsif($CMS::html_values{productcode}{value} =~ /IPTRANSIT/) {
	   $return_result = CMS_SERVICE_DB::add_service
          ($CMS::html_values{service}{value},
           $CMS::html_values{accno}{value},
           $CMS::html_values{description}{value},
           $CMS::html_values{bandwidth}{value},
           $CMS::html_values{productcode}{value},
           $CMS::html_values{as_no}{value},
           $CMS::html_values{auto_gen_prefix}{value},
           $CMS::html_values{bgp_neighbor_ip}{value},
           $CMS::html_values{irrd_object}{value},
           $CMS::html_values{ipt_msid}{value},
	   $CMS::html_values{black_hole_routing}{value},
	   $CMS::html_values{bha_bgp_neighbor_ip}{value},
	   $CMS::html_values{bha_auto_gen_prefix}{value});
	#End of code added by Kamal;
	#End of code added by karuna
        } elsif ($CMS::html_values{productcode}{value} =~ /GIA/){
		 open (IPT1, ">>/data1/tmp_log/addservice.log");
		print IPT1 "GIA service:values passed\n";
		print IPT1 "service:$CMS::html_values{service}{value}\n accno:$CMS::html_values{accno}{value}\n prodcode:$CMS::html_values{productcode}{value}\n description:$CMS::html_values{description}{value}\n bandwidth:$CMS::html_values{bandwidth}{value}\n switchsourcode:$CMS::html_values{switchsourcode}{value}\n switchdestcode:$CMS::html_values{switchdestcode}{value}\n pollingtype:$CMS::html_values{pollingtype}{value}\nas_no:$CMS::html_values{as_no}{value}\nauto_gen_prefix:$CMS::html_values{auto_gen_prefix}{value}\nbgp_neighbor_ip:$CMS::html_values{bgp_neighbor_ip}{value}\nblack_hole_routing:$CMS::html_values{black_hole_routing}{value}\nbha_bgp_neighbor_ip:$CMS::html_values{bha_bgp_neighbor_ip}{value}bha_auto_gen_prefix:$CMS::html_values{bha_auto_gen_prefix}{value}\n ";
		$return_result = CMS_SERVICE_DB::add_service
          ($CMS::html_values{service}{value},
           $CMS::html_values{accno}{value},
           $CMS::html_values{description}{value},
           $CMS::html_values{bandwidth}{value},
           $CMS::html_values{productcode}{value},
           $CMS::html_values{switchsourcode}{value},
           $CMS::html_values{switchdestcode}{value},
           $CMS::html_values{pollingtype}{value},
           $CMS::html_values{costype}{value},
           $CMS::html_values{costype_e}{value},
           $CMS::html_values{portmode}{value},
           $CMS::html_values{portmode_e}{value},
           $CMS::html_values{gold_classmap}{value},
           $CMS::html_values{gold_rateacl}{value},
           $CMS::html_values{silver_classmap}{value},
           $CMS::html_values{silver_rateacl}{value},
           $CMS::html_values{default_classmap}{value},
           $CMS::html_values{default_rateacl}{value},
           $CMS::html_values{class1_classmap}{value},
           $CMS::html_values{class1_rateacl}{value},
           $CMS::html_values{class2_classmap}{value},
           $CMS::html_values{class2_rateacl}{value},
           $CMS::html_values{class4_classmap}{value},
           $CMS::html_values{class4_rateacl}{value},
           $CMS::html_values{as_no}{value},
           $CMS::html_values{auto_gen_prefix}{value},
           $CMS::html_values{bgp_neighbor_ip}{value},
           $CMS::html_values{irrd_object}{value},
           $CMS::html_values{aggregate_service}{value},
           $CMS::html_values{aggregate_service_a}{value},
           $CMS::html_values{aggregate_service_z}{value},
           $CMS::html_values{aggregate_service_tpm}{value},
           $CMS::html_values{master_service}{value},
           $CMS::html_values{billable}{value},
           $CMS::html_values{'billstartdate.year'}{value},
           $CMS::html_values{'billstartdate.month'}{value},
           $CMS::html_values{'billstartdate.day'}{value},
	   '00',
           '00',
           '00',
           $CMS::html_values{'billenddate.year'}{value},
           $CMS::html_values{'billenddate.month'}{value},
           $CMS::html_values{'billenddate.day'}{value},
           '23',
           '59',
           '59',
	   $CMS::html_values{black_hole_routing}{value},
           $CMS::html_values{bha_bgp_neighbor_ip}{value},
           $CMS::html_values{bha_auto_gen_prefix}{value});
	   
        } else {
	open (IPT1, ">/data1/tmp_log/addservice.log");
	print IPT1 "entered add service\n"; 
	print IPT1 "service:$CMS::html_values{service}{value}\taccno:$CMS::html_values{accno}{value}\tprodcode:$CMS::html_values{productcode}{value}\n";
	$return_result = CMS_SERVICE_DB::add_service
          ($CMS::html_values{service}{value},
           $CMS::html_values{accno}{value},
           $CMS::html_values{description}{value},
           $CMS::html_values{bandwidth}{value},
           $CMS::html_values{productcode}{value},
           $CMS::html_values{switchsourcode}{value},
           $CMS::html_values{switchdestcode}{value},
           $CMS::html_values{pollingtype}{value},
           $CMS::html_values{costype}{value},
           $CMS::html_values{costype_e}{value},
           $CMS::html_values{portmode}{value},
	   $CMS::html_values{portmode_e}{value},
           $CMS::html_values{gold_classmap}{value},
           $CMS::html_values{gold_rateacl}{value},
           $CMS::html_values{silver_classmap}{value},
           $CMS::html_values{silver_rateacl}{value},
           $CMS::html_values{default_classmap}{value},
           $CMS::html_values{default_rateacl}{value},
           $CMS::html_values{class1_classmap}{value},
           $CMS::html_values{class1_rateacl}{value},
           $CMS::html_values{class2_classmap}{value},
           $CMS::html_values{class2_rateacl}{value},
           $CMS::html_values{class4_classmap}{value},
           $CMS::html_values{class4_rateacl}{value},
           $CMS::html_values{as_no}{value},
           $CMS::html_values{auto_gen_prefix}{value},
           $CMS::html_values{bgp_neighbor_ip}{value},
           $CMS::html_values{irrd_object}{value},
           $CMS::html_values{aggregate_service}{value},
           $CMS::html_values{aggregate_service_a}{value},
           $CMS::html_values{aggregate_service_z}{value},	
	   $CMS::html_values{aggregate_service_tpm}{value},
	   $CMS::html_values{master_service}{value},
           $CMS::html_values{billable}{value},
           $CMS::html_values{'billstartdate.year'}{value},
           $CMS::html_values{'billstartdate.month'}{value},
           $CMS::html_values{'billstartdate.day'}{value},
           '00',
           '00',
           '00',
	   $CMS::html_values{'billenddate.year'}{value},
           $CMS::html_values{'billenddate.month'}{value},
           $CMS::html_values{'billenddate.day'}{value},
           '23',
           '59',
           '59',
           $CMS::html_values{'serviceid_alt'}{value},
           $CMS::html_values{'aend_jobid'}{value},
           $CMS::html_values{'zend_jobid'}{value},
           $CMS::html_values{'aend_jobno'}{value},
           $CMS::html_values{'zend_jobno'}{value},
           $CMS::html_values{'aend_country'}{value},
           $CMS::html_values{'zend_country'}{value}
          );
	}

if ($return_result == 1)
	{
	open (IPT1, ">/data1/tmp_log/addservice.log");
	print IPT1 "PM return result:$return_result\n"; 
	
	# have added service
 	$CMS::html_values{productcode}{value} = $CMS::html_values{productcode}{value};	
 	$CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();	
 	$CMS::html_values{switchsourcode}{value} = $CMS::html_values{switchsourcode}{value};	
 	$CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();	
 	$CMS::html_values{switchdestcode}{value} = $CMS::html_values{switchdestcode}{value};	
 	$CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();	
# 	$CMS::html_values{serviceclass}{value} = $CMS::html_values{serviceclass}{value};	
# 	$CMS::html_values{serviceclass}{options} = CMS_SERVICE_DB::get_serviceclass();	
 	$CMS::html_values{auto_gen_prefix}{value} = $CMS::html_values{auto_gen_prefix}{value};	
 	$CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();	
	$CMS::html_values{black_hole_routing}{value} = $CMS::html_values{black_hole_routing}{value};
	$CMS::html_values{black_hole_routing}{options} = CMS_SERVICE_DB::get_black_hole_routing_option();
	$CMS::html_values{bha_auto_gen_prefix}{value} = $CMS::html_values{bha_auto_gen_prefix}{value};
	$CMS::html_values{bha_auto_gen_prefix}{options} = CMS_SERVICE_DB::get_bha_auto_gen_prefix();
	$CMS::html_values{pollingtype}{value} = $CMS::html_values{pollingtype}{value};
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{costype}{value} = $CMS::html_values{costype}{value};
	$CMS::html_values{portmode}{value} = $CMS::html_values{portmode}{value};
        $CMS::html_values{portmode}{options} =  CMS_SERVICE_DB::get_portmode();
	$CMS::html_values{portmode_e}{value} = $CMS::html_values{portmode_e}{value};
	$CMS::html_values{portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
	$CMS::html_values{costype_e}{value} = $CMS::html_values{costype_e}{value};
        $CMS::html_values{costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
	$CMS::html_values{aggregate_service}{value} = $CMS::html_values{aggregate_service}{value};
	$CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
	$CMS::html_values{aggregate_service_a}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
        $CMS::html_values{aggregate_service_z}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
        $CMS::html_values{aggregate_service_tpm}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_tpm();
	unshift(@{$CMS::html_values{aggregate_service_a}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_z}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_tpm}{options}}, [""]);
        $CMS::html_values{master_service}{value} = $CMS::html_values{master_service}{value}; 	
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        $CMS::html_values{billable}{value} = $CMS::html_values{billable}{value};
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_master_service}{value} = $CMS::html_values{epl_master_service}{value};
        $CMS::html_values{epl_master_service}{options} = CMS_SERVICE_DB::get_epl_master_service();

        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

	# ++ Added for User Log after adding service - Sayantan dutta - 11-Nov-2009
	user_log "Add Service";
	# -- Added for User Log after adding service - Sayantan dutta - 11-Nov-2009

	CMS::add_error("Service added successfully.");
	CMS::output_html("service_add");
	
      } else {
	# Failed to add service
	CMS::add_error("Failed to add service.");
	CMS::add_error("$return_result");
	CMS::add_error($CMS_SERVICE_DB::error);
	open (IPT, ">>/data1/tmp_log/addservice.log");
	print IPT "return_result: $return_result\n";
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();      
	 $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
	$CMS::html_values{portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
        $CMS::html_values{costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
        # switchcode for fr sla reports
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();      
 	$CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();	
	$CMS::html_values{black_hole_routing}{value} = $CMS::html_values{black_hole_routing}{value};
        $CMS::html_values{black_hole_routing}{options} = CMS_SERVICE_DB::get_black_hole_routing_option();
        $CMS::html_values{bha_auto_gen_prefix}{value} = $CMS::html_values{bha_auto_gen_prefix}{value};
        $CMS::html_values{bha_auto_gen_prefix}{options} = CMS_SERVICE_DB::get_bha_auto_gen_prefix();
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();     
        #++EVPL 
        $CMS::html_values{aggregate_service_a}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
        $CMS::html_values{aggregate_service_z}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
        $CMS::html_values{aggregate_service_tpm}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_tpm();
	unshift(@{$CMS::html_values{aggregate_service_a}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_z}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_tpm}{options}}, [""]);
        #--EVPL
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
#        $CMS::html_values{serviceclass}{options} = CMS_SERVICE_DB::get_serviceclass();
	$CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();

        #Added by Karuna for EPL billing options
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

	CMS::output_html("service_add");
      }
     }
    }
  } elsif ($CMS::html_values{command}{value} eq "edit_service") {
  # edit a new service
  open (EPL2, ">/data1/tmp_log/editservice.log");

   if (not defined ($CMS::html_values{stage}{value})) {
	   # first stage - Commented as per review comments
	   #CMS::add_error("Entered  First Stage of EDIT."); 
	   CMS::output_html("service_edit");
   }   
   elsif ($CMS::html_values{stage}{value} == 2) {
	#print EPL2 "second stage fo EDIT\n";
	my $serv = $CMS::html_values{service}{value};
	#++EVPL--For EVPL services append a to it.
	#modified by Karuna to add a check for EPL as well
	my $evpl_serv = lc($serv);
	$evpl_serv =~ s/\s+//g;
	$evpl_serv = substr ($evpl_serv, 13, 2);
	#print EPL2 "serv:$serv\tevpl_serv:$evpl_serv\n";
	if(($serv =~ /\[/) && ($evpl_serv !~ /^(em|ew)$/)){
		#print EPL2 "entered if check\n";
		$serv.="a";
	} 
	#print EPL2 "before get_service service id: $serv\n";
	if (my $result = CMS_SERVICE_DB::get_service
	($serv,
	$CMS::html_values{accno}{value})) {

	if($$result[2] =~ /VPLS/){
		($CMS::html_values{description}{value},
		$CMS::html_values{bandwidth}{value},
		$CMS::html_values{productcode}{value},
		$CMS::html_values{switchsourcode}{value},
		$CMS::html_values{switchdestcode}{value},
		$CMS::html_values{pollingtype}{value},
		$CMS::html_values{costype}{value},
		$CMS::html_values{portmode}{value},

		$CMS::html_values{gold_classmap_v}{value},
		$CMS::html_values{gold_rateacl_v}{value},
		$CMS::html_values{silver_classmap_v}{value},
		$CMS::html_values{silver_rateacl_v}{value},
		$CMS::html_values{default_classmap_v}{value},
		$CMS::html_values{default_rateacl_v}{value},
		$CMS::html_values{class1_classmap_v}{value},
		$CMS::html_values{class1_rateacl_v}{value},
		$CMS::html_values{class2_classmap_v}{value},
		$CMS::html_values{class2_rateacl_v}{value},
		$CMS::html_values{class4_classmap_v}{value},
		$CMS::html_values{class4_rateacl_v}{value},

		$CMS::html_values{as_no}{value},
		$CMS::html_values{auto_gen_prefix}{value},
		$CMS::html_values{bgp_neighbor_ip}{value},
		$CMS::html_values{irrd_object}{value},
		$CMS::html_values{aggregate_service}{value},
		$CMS::html_values{master_service}{value},
		$CMS::html_values{billable}{value},
	        $CMS::html_values{billing_starttime}{value},
		$CMS::html_values{billing_endtime}{value},
		$CMS::html_values{serviceid_alt}{value},
		$CMS::html_values{aend_jobid}{value},
		$CMS::html_values{zend_jobid}{value},
		$CMS::html_values{aend_jobno}{value},
		$CMS::html_values{zend_jobno}{value},
		$CMS::html_values{aend_country}{value},
		$CMS::html_values{zend_country}{value}
		) = @$result;
		}
	#++ EVPL
	elsif($$result[2] =~ /EVPL/){
		($CMS::html_values{description}{value},
		$CMS::html_values{bandwidth}{value},
		$CMS::html_values{productcode}{value},
		$CMS::html_values{switchsourcode}{value},
		$CMS::html_values{switchdestcode}{value},
		$CMS::html_values{pollingtype}{value},
		$CMS::html_values{costype_e}{value},
		$CMS::html_values{portmode_e}{value},		
		$CMS::html_values{gold_classmap_e}{value},
		$CMS::html_values{gold_rateacl_e}{value},
		$CMS::html_values{silver_classmap_v}{value},
		$CMS::html_values{silver_rateacl_v}{value},
		$CMS::html_values{default_classmap_e}{value},
		$CMS::html_values{default_rateacl_e}{value},
		$CMS::html_values{class1_classmap_v}{value},
		$CMS::html_values{class1_rateacl_v}{value},
		$CMS::html_values{class2_classmap_v}{value},
		$CMS::html_values{class2_rateacl_v}{value},
		$CMS::html_values{class4_classmap_v}{value},
		$CMS::html_values{class4_rateacl_v}{value},
 		$CMS::html_values{as_no}{value},
		$CMS::html_values{auto_gen_prefix}{value},
		$CMS::html_values{bgp_neighbor_ip}{value},
		$CMS::html_values{irrd_object}{value},
		$CMS::html_values{aggregate_service_a}{value},
		$CMS::html_values{master_service}{value},
		$CMS::html_values{billable}{value},
		$CMS::html_values{billing_starttime}{value},
		$CMS::html_values{billing_endtime}{value},
		$CMS::html_values{serviceid_alt}{value},
		$CMS::html_values{aend_jobid}{value},
		$CMS::html_values{zend_jobid}{value},
		$CMS::html_values{aend_jobno}{value},
		$CMS::html_values{zend_jobno}{value},
		$CMS::html_values{aend_country}{value},
		$CMS::html_values{zend_country}{value})=@$result;
		my $servtype = $$result[7];
		if($servtype =~ /VPM/){
			$CMS::html_values{portmode_e}{value}= "EVPL VLAN Service";
			               $serv=$CMS::html_values{service}{value};
                my $result_z = CMS_SERVICE_DB::get_service($serv."z",$CMS::html_values{accno}{value});
                $CMS::html_values{aggregate_service_z}{value} = $$result_z[24];

		} else {
			$CMS::html_values{portmode_e}{value}= "EVPL Transparent Service";
			#remove asw from the agg service if preesnt
			if($$result[24]){
				$$result[24]=~s/asw/sw/;
			}	
			$CMS::html_values{aggregate_service_tpm}{value}=$$result[24];
		}
	} elsif($$result[2] =~ /MPLS/) {
	#Added by Nisha for mVPN on 29Sep2009 for edit service
         ($CMS::html_values{description}{value},
         $CMS::html_values{bandwidth}{value},
         $CMS::html_values{productcode}{value},
         $CMS::html_values{switchsourcode}{value},
         $CMS::html_values{switchdestcode}{value},
         $CMS::html_values{pollingtype}{value},
         $CMS::html_values{costype}{value},
         $CMS::html_values{portmode}{value},
         $CMS::html_values{gold_classmap}{value},
         $CMS::html_values{gold_rateacl}{value},
         $CMS::html_values{silver_classmap}{value},
         $CMS::html_values{silver_rateacl}{value},
         $CMS::html_values{default_classmap}{value},
         $CMS::html_values{default_rateacl}{value},
         $CMS::html_values{class1_classmap}{value},
         $CMS::html_values{class1_rateacl}{value},
         $CMS::html_values{class2_classmap}{value},
         $CMS::html_values{class2_rateacl}{value},
         $CMS::html_values{class4_classmap}{value},
         $CMS::html_values{class4_rateacl}{value},
         $CMS::html_values{as_no}{value},
         $CMS::html_values{auto_gen_prefix}{value},
         $CMS::html_values{bgp_neighbor_ip}{value},
         $CMS::html_values{irrd_object}{value},
         $CMS::html_values{aggregate_service}{value},
         $CMS::html_values{master_service}{value},
         $CMS::html_values{billable}{value},
         $CMS::html_values{billing_starttime}{value},
         $CMS::html_values{billing_endtime}{value},
         $CMS::html_values{serviceid_alt}{value},
         $CMS::html_values{aend_jobid}{value},
         $CMS::html_values{zend_jobid}{value},
         $CMS::html_values{aend_jobno}{value},
         $CMS::html_values{zend_jobno}{value},
         $CMS::html_values{aend_country}{value},
         $CMS::html_values{zend_country}{value},
	 $CMS::html_values{voice_mvpn_in}{value},
	 $CMS::html_values{video_mvpn_in}{value},
	 $CMS::html_values{video_mvpn_out}{value},
	 $CMS::html_values{criticaldata_mvpn_in}{value},
	 $CMS::html_values{criticaldata_mvpn_out}{value},
	 $CMS::html_values{interactivedata_mvpn_in}{value},
	 $CMS::html_values{interactivedata_mvpn_out}{value},
	 $CMS::html_values{standarddata_mvpn_in}{value},
	 $CMS::html_values{standarddata_mvpn_out}{value},
	 $CMS::html_values{lowpriority_mvpn_in}{value},
	 $CMS::html_values{lowpriority_mvpn_out}{value},
 	 $CMS::html_values{mvpnCheck}{value}
	) = @$result;

	#End of code Added by Nisha for mVPN on 29Sep2009
    	} elsif($$result[2] =~ /EPL/) {
        #Added by Karuna for EPL Edit Service
	open (EPL1, ">>/data1/tmp_log/addservice.log");
        print EPL1 "inside result 2 loop\n $$result[2] prodcode: $CMS::html_values{productcode}{value}\n";
          $CMS::html_values{description}{value} = $$result[0];
          my $epl_bw = $$result[1];
          #Bandwidth conversion to Mbps for EPL
          $epl_bw = $epl_bw/1000000;
          $CMS::html_values{bandwidth}{value} = $epl_bw;
          $CMS::html_values{productcode}{value}  = $$result[2];
          $CMS::html_values{epl_msid}{value} = $$result[25];
          $CMS::html_values{epl_aend_jobid}{value}  = $$result[30];
          $CMS::html_values{epl_zend_jobid}{value}  = $$result[31];
          $CMS::html_values{epl_aend_jobno}{value}   = $$result[32];
          $CMS::html_values{epl_zend_jobno}{value}  = $$result[33];
          $CMS::html_values{epl_aend_jobid2}{value}  = $$result[30];
          $CMS::html_values{epl_zend_jobid2}{value}  = $$result[31];
          $CMS::html_values{epl_aend_jobno2}{value}  = $$result[32];
          $CMS::html_values{epl_zend_jobno2}{value} = $$result[33];
          $CMS::html_values{epl_billable}{value} = $$result[26];
          $CMS::html_values{epl_billable2}{value} = $$result[26];
          $CMS::html_values{billing_starttime}{value} = $$result[27];
          $CMS::html_values{billing_endtime}{value} = $$result[28];

        print EPL1 "PM EDIT 2 service:$serv\nresults:msid-$$result[25]\tepl_billable$$result[28]\tbillingstart-$$result[30]\tbillend$$result[31]\taendjid-$$result[30]\tzendjid-$$result[31]\taendjno-$$result[32]\tzendjno-$$result[33]\n";
        } elsif($$result[2] =~ /IPTRANSIT/) {
		open (IPT2,">>/data1/tmp_log/editservice.log");
		print IPT2 "prodcode:$$result[2]\n"; 
		print IPT2 "Result = @$result\n";
		print IPT2 "black_hole_routing = $$result[47]\n";
		print IPT2 "49 + = $$result[49]\n";
		$CMS::html_values{description}{value} = $$result[0];
		$CMS::html_values{bandwidth}{value} = $$result[1];
		$CMS::html_values{productcode}{value}  = $$result[2];
		$CMS::html_values{as_no}{value} = $$result[20];
		$CMS::html_values{auto_gen_prefix}{value} = $$result[21];
         	$CMS::html_values{bgp_neighbor_ip}{value} = $$result[22];
         	$CMS::html_values{irrd_object}{value} = $$result[23];		
		$CMS::html_values{ipt_msid}{value} = $$result[25];
		$CMS::html_values{black_hole_routing}{value} = $$result[48];
		$CMS::html_values{bha_bgp_neighbor_ip}{value} = $$result[49];
		$CMS::html_values{bha_auto_gen_prefix}{value} = $$result[50];
	}

        #Added by Stavan for VLA on 18Aug2010
        elsif($$result[2] =~ /ETHERNET/) {
           $CMS::html_values{description}{value} = $$result[0];
           $CMS::html_values{bandwidth}{value} = $$result[1];
           $CMS::html_values{productcode}{value} = $$result[2];
           $CMS::html_values{pollingtype}{value} = $$result[5];
           $CMS::html_values{billable}{value} = $$result[26];
           $CMS::html_values{billing_starttime}{value} =  $$result[27];
           $CMS::html_values{billing_endtime}{value} =  $$result[28];
        }
        #-- End of VLA code
	##IP Transit Ph5
	elsif($$result[2] =~ /GIA/){
		$CMS::html_values{description}{value} = $$result[0];
                $CMS::html_values{bandwidth}{value} = $$result[1];
                $CMS::html_values{productcode}{value}  = $$result[2];
                $CMS::html_values{as_no}{value} = $$result[20];
                $CMS::html_values{auto_gen_prefix}{value} = $$result[21];
                $CMS::html_values{bgp_neighbor_ip}{value} = $$result[22];
                $CMS::html_values{irrd_object}{value} = $$result[23];
		$CMS::html_values{aggregate_service}{value} = $$result[24];
		$CMS::html_values{billable}{value} = $$result[26];
          	$CMS::html_values{billing_starttime}{value} = $$result[27];
          	$CMS::html_values{billing_endtime}{value} = $$result[28];
		$CMS::html_values{black_hole_routing}{value} = $$result[48];
                $CMS::html_values{bha_bgp_neighbor_ip}{value} = $$result[49];
                $CMS::html_values{bha_auto_gen_prefix}{value} = $$result[50];
	}
	##--IPT5
	else{
         ($CMS::html_values{description}{value},
         $CMS::html_values{bandwidth}{value},
         $CMS::html_values{productcode}{value},
         $CMS::html_values{switchsourcode}{value},
         $CMS::html_values{switchdestcode}{value},
         $CMS::html_values{pollingtype}{value},
         $CMS::html_values{costype}{value},
         $CMS::html_values{portmode}{value},
	 $CMS::html_values{gold_classmap}{value},
         $CMS::html_values{gold_rateacl}{value},
         $CMS::html_values{silver_classmap}{value},
         $CMS::html_values{silver_rateacl}{value},
         $CMS::html_values{default_classmap}{value},
         $CMS::html_values{default_rateacl}{value},
         $CMS::html_values{class1_classmap}{value},
         $CMS::html_values{class1_rateacl}{value},
         $CMS::html_values{class2_classmap}{value},
         $CMS::html_values{class2_rateacl}{value},
         $CMS::html_values{class4_classmap}{value},
         $CMS::html_values{class4_rateacl}{value},
         $CMS::html_values{as_no}{value},
         $CMS::html_values{auto_gen_prefix}{value},
         $CMS::html_values{bgp_neighbor_ip}{value},
         $CMS::html_values{irrd_object}{value},
         $CMS::html_values{aggregate_service}{value},
	 $CMS::html_values{master_service}{value},
         $CMS::html_values{billable}{value},
         $CMS::html_values{billing_starttime}{value},
         $CMS::html_values{billing_endtime}{value},
         $CMS::html_values{serviceid_alt}{value},
         $CMS::html_values{aend_jobid}{value},
         $CMS::html_values{zend_jobid}{value},
         $CMS::html_values{aend_jobno}{value},
         $CMS::html_values{zend_jobno}{value},
         $CMS::html_values{aend_country}{value},
         $CMS::html_values{zend_country}{value}
        ) = @$result;	
	}
	$CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
	$CMS::html_values{portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
        $CMS::html_values{costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
        # switchcode for fr sla reports
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();      
 	$CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();	
#IP Transit code Phase 5 - Kamal
	$CMS::html_values{black_hole_routing}{options} = CMS_SERVICE_DB::get_black_hole_routing_option();
	$CMS::html_values{bha_auto_gen_prefix}{options} = CMS_SERVICE_DB::get_bha_auto_gen_prefix();
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();      

 		
	#++EVPL
	$CMS::html_values{aggregate_service_tpm}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_tpm();
	my $vpmvals = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
	unshift(@$vpmvals , [""]);
	$CMS::html_values{aggregate_service_a}{options} = $vpmvals;
	$CMS::html_values{aggregate_service_z}{options} = $vpmvals;
	unshift(@{$CMS::html_values{aggregate_service_tpm}{options}}, [""]);
	#--EVPL
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
#       $CMS::html_values{serviceclass}{options} = CMS_SERVICE_DB::get_serviceclass();

        # for billing
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #for EPL Master Service
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        #unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);


        # startdate
        my @tmpdt  = split(/-|\s|\:|\+/, $CMS::html_values{billing_starttime}{value});
        $CMS::html_values{billing_startdate_debug}{value} = join("z", @tmpdt);
        my @billdt;
        if ($tmpdt[0] ne '') {
                #@billdt = (@tmpdt[0,1,2,3,4,5]);
                @billdt = (@tmpdt[0,1,2]);
        } else {
                #@billdt = ('----','--','--','--','--','--');
                @billdt = ('----','--','--');
        }

        (
        $CMS::html_values{'billstartdate.year'}{value},
        $CMS::html_values{'billstartdate.month'}{value},
        $CMS::html_values{'billstartdate.day'}{value}
        ) = @billdt;

        # enddate
        @tmpdt  = split(/-|\s|\:|\+/, $CMS::html_values{billing_endtime}{value});
        $CMS::html_values{billing_enddate_debug}{value} = join("z", @tmpdt);
        #@billdt;
        if ($tmpdt[0] ne '') {
                #@billdt = (@tmpdt[0,1,2,3,4,5]);
                @billdt = (@tmpdt[0,1,2]);
        } else {
                #@billdt = ('----','--','--','--','--','--');
                @billdt = ('----','--','--');
        }

        (
        $CMS::html_values{'billenddate.year'}{value},
        $CMS::html_values{'billenddate.month'}{value},
        $CMS::html_values{'billenddate.day'}{value}
        ) = @billdt;

	CMS::output_html("service_edit2");

      } else {
	# service does not exist

	CMS::add_error("That account does not have such a service.");
	CMS::output_html("service_edit");
      }

    } elsif ($CMS::html_values{stage}{value} == 3) {
       
      $CMS::html_values{service}{value} =~ tr/A-Z/a-z/; 
      #modified by Karuna to add a check for EPL as well
      if($CMS::html_values{productcode}{value} !~ /(EVPL|EPL)/){
      $CMS::html_values{service}{value} =~ s/( |\[|\])//g;
      }

    if (($CMS::html_values{gold_classmap}{value} ne "" && $CMS::html_values{gold_classmap}{value} =~ / /) ||
($CMS::html_values{gold_rateacl}{value} ne "" && $CMS::html_values{gold_rateacl}{value} =~ / /) ||
($CMS::html_values{silver_classmap}{value} ne "" && $CMS::html_values{silver_classmap}{value} =~ / /) ||
($CMS::html_values{silver_rateacl}{value} ne "" && $CMS::html_values{silver_rateacl}{value} =~ / /) ||
($CMS::html_values{default_classmap}{value} ne "" && $CMS::html_values{default_classmap}{value} =~ / /) ||
($CMS::html_values{default_rateacl}{value} ne "" && $CMS::html_values{default_rateacl}{value} =~ / /) ||
($CMS::html_values{class1_classmap}{value} ne "" && $CMS::html_values{class1_classmap}{value} =~ / /) ||
($CMS::html_values{class1_rateacl}{value} ne "" && $CMS::html_values{class1_rateacl}{value} =~ / /) ||
($CMS::html_values{class2_classmap}{value} ne "" && $CMS::html_values{class2_classmap}{value} =~ / /) ||
($CMS::html_values{class2_rateacl}{value} ne "" && $CMS::html_values{class2_rateacl}{value} =~ / /) ||
($CMS::html_values{class4_classmap}{value} ne "" && $CMS::html_values{class4_classmap}{value} =~ / /) ||
($CMS::html_values{class4_rateacl}{value} ne "" && $CMS::html_values{class4_rateacl}{value} =~ / /) ||
($CMS::html_values{gold_classmap_v}{value} ne "" && $CMS::html_values{gold_classmap_v}{value} =~ / /) ||
($CMS::html_values{gold_rateacl_v}{value} ne "" && $CMS::html_values{gold_rateacl_v}{value} =~ / /) ||
($CMS::html_values{silver_classmap_v}{value} ne "" && $CMS::html_values{silver_classmap_v}{value} =~ / /) ||
($CMS::html_values{silver_rateacl_v}{value} ne "" && $CMS::html_values{silver_rateacl_v}{value} =~ / /) ||
($CMS::html_values{default_classmap_v}{value} ne "" && $CMS::html_values{default_classmap_v}{value} =~ / /) ||
($CMS::html_values{default_rateacl_v}{value} ne "" && $CMS::html_values{default_rateacl_v}{value} =~ / /) ||
($CMS::html_values{class1_classmap_v}{value} ne "" && $CMS::html_values{class1_classmap_v}{value} =~ / /) ||
($CMS::html_values{class1_rateacl_v}{value} ne "" && $CMS::html_values{class1_rateacl_v}{value} =~ / /) ||
($CMS::html_values{class2_classmap_v}{value} ne "" && $CMS::html_values{class2_classmap_v}{value} =~ / /) ||
($CMS::html_values{class2_rateacl_v}{value} ne "" && $CMS::html_values{class2_rateacl_v}{value} =~ / /) ||
($CMS::html_values{class4_classmap_v}{value} ne "" && $CMS::html_values{class4_classmap_v}{value} =~ / /) ||
($CMS::html_values{class4_rateacl_v}{value} ne "" && $CMS::html_values{class4_rateacl_v}{value} =~ / /)
) {

        # Failed to add service
        CMS::add_error("Failed to edit service.");
        CMS::add_error("Space is not allowed in CoS Fields."); 
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
	$CMS::html_values{portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
        $CMS::html_values{costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
        # switchcode for fr sla reports
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
#        $CMS::html_values{serviceclass}{options} = CMS_SERVICE_DB::get_serviceclass();
        if ($CMS::html_values{gold_classmap}{value} =~ / /) {
          $CMS::html_values{gold_classmap}{value} = "";
        }
        if ($CMS::html_values{gold_rateacl}{value} =~ / /) {
          $CMS::html_values{gold_rateacl}{value} = "";
        }
        if ($CMS::html_values{silver_classmap}{value} =~ / /) {
          $CMS::html_values{silver_classmap}{value} = "";
        }
        if ($CMS::html_values{silver_rateacl}{value} =~ / /) {
          $CMS::html_values{silver_rateacl}{value} = "";
        }
        if ($CMS::html_values{default_classmap}{value} =~ / /) {
          $CMS::html_values{default_classmap}{value} = "";
        }
        if ($CMS::html_values{default_rateacl}{value} =~ / /) {
          $CMS::html_values{default_rateacl}{value} = "";
        }
        # 10/8/06
        if ($CMS::html_values{class1_rateacl}{value} =~ / /) {
          $CMS::html_values{class1_rateacl}{value} = "";
        }
        if ($CMS::html_values{class1_classmap}{value} =~ / /) {
          $CMS::html_values{class1_classmap}{value} = "";
        }
        if ($CMS::html_values{class2_rateacl}{value} =~ / /) {
          $CMS::html_values{class2_rateacl}{value} = "";
        }
        if ($CMS::html_values{class2_classmap}{value} =~ / /) {
          $CMS::html_values{class2_classmap}{value} = "";
        }
        if ($CMS::html_values{class4_rateacl}{value} =~ / /) {
          $CMS::html_values{class4_rateacl}{value} = "";
        }
        if ($CMS::html_values{class4_classmap}{value} =~ / /) {
          $CMS::html_values{class4_classmap}{value} = "";
        }

 	if ($CMS::html_values{gold_classmap_v}{value} =~ / /) {
          $CMS::html_values{gold_classmap_v}{value} = "";
        }
        if ($CMS::html_values{gold_rateacl_v}{value} =~ / /) {
          $CMS::html_values{gold_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{silver_classmap_v}{value} =~ / /) {
          $CMS::html_values{silver_classmap_v}{value} = "";
        }
        if ($CMS::html_values{silver_rateacl_v}{value} =~ / /) {
          $CMS::html_values{silver_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{default_classmap_v}{value} =~ / /) {
          $CMS::html_values{default_classmap_v}{value} = "";
        }
        if ($CMS::html_values{default_rateacl_v}{value} =~ / /) {
          $CMS::html_values{default_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{class1_rateacl_v}{value} =~ / /) {
          $CMS::html_values{class1_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{class1_classmap_v}{value} =~ / /) {
          $CMS::html_values{class1_classmap_v}{value} = "";
        }
        if ($CMS::html_values{class2_rateacl_v}{value} =~ / /) {
          $CMS::html_values{class2_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{class2_classmap_v}{value} =~ / /) {
          $CMS::html_values{class2_classmap_v}{value} = "";
        }
        if ($CMS::html_values{class4_rateacl_v}{value} =~ / /) {
          $CMS::html_values{class4_rateacl_v}{value} = "";
        }
        if ($CMS::html_values{class4_classmap_v}{value} =~ / /) {
          $CMS::html_values{class4_classmap_v}{value} = "";
        }

        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        CMS::output_html("service_edit2");
    } elsif (($CMS::html_values{productcode}{value} =~ /MPLS/) && ($CMS::html_values{costype}{value} eq "")) {
        CMS::add_error("Failed to edit service.");
        CMS::add_error("IPVPN Router Configuration Type - COS type is required");

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        CMS::output_html("service_edit2");
    } elsif (($CMS::html_values{productcode}{value} =~ /MPLS/) && ($CMS::html_values{pollingtype}{value} eq "") && (($CMS::html_values{gold_classmap}{value} ne "") || ($CMS::html_values{gold_rateacl}{value} ne "") || ($CMS::html_values{silver_classmap}{value} ne "") || ($CMS::html_values{silver_rateacl}{value} ne "") || ($CMS::html_values{default_classmap}{value} ne "") || ($CMS::html_values{default_rateacl}{value} ne "") || ($CMS::html_values{class1_classmap}{value} ne "") || ($CMS::html_values{class1_rateacl}{value} ne "") || ($CMS::html_values{class2_classmap}{value} ne "") || ($CMS::html_values{class2_rateacl}{value} ne "") || ($CMS::html_values{class4_classmap}{value} ne "") || ($CMS::html_values{class4_rateacl}{value} ne ""))) {
        CMS::add_error("Failed to edit service.");
        CMS::add_error("RateACL No. / ClassMap Name does not require.");
        CMS::add_error("Please leave blank for:<br> - [IN] RateACL No. / ClassMap Name<br> - [OUT] ClassMap Name");

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        CMS::output_html("service_edit2");
    } elsif (($CMS::html_values{productcode}{value} =~ /MPLS/) && ($CMS::html_values{pollingtype}{value} =~ /RATEACL/) && ($CMS::html_values{gold_rateacl}{value} eq "") && ($CMS::html_values{silver_rateacl}{value} eq "") && ($CMS::html_values{default_rateacl}{value} eq "") && ($CMS::html_values{class1_rateacl}{value} eq "") && ($CMS::html_values{class2_rateacl}{value} eq "") && ($CMS::html_values{class4_rateacl}{value} eq "")) {
        CMS::add_error("Failed to edit service.");
        CMS::add_error("RateACL No. is required");

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        CMS::output_html("service_edit2");
    } elsif (($CMS::html_values{productcode}{value} =~ /MPLS/) && ($CMS::html_values{pollingtype}{value} =~ /CLASSMAP/) && ($CMS::html_values{gold_classmap}{value} eq "") && ($CMS::html_values{gold_rateacl}{value} eq "") && ($CMS::html_values{silver_classmap}{value} eq "") && ($CMS::html_values{silver_rateacl}{value} eq "") && ($CMS::html_values{default_classmap}{value} eq "") && ($CMS::html_values{default_rateacl}{value} eq "") && ($CMS::html_values{class1_classmap}{value} eq "") && ($CMS::html_values{class1_rateacl}{value} eq "") && ($CMS::html_values{class2_classmap}{value} eq "") && ($CMS::html_values{class2_rateacl}{value} eq "") && ($CMS::html_values{class4_classmap}{value} eq "") && ($CMS::html_values{class4_rateacl}{value} eq "")) {
        CMS::add_error("Failed to edit service.");
        CMS::add_error("ClassMap Name is required");

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
	$CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
        $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        CMS::output_html("service_edit2");

    }elsif ( ($CMS::html_values{as_no}{value} < 0) || ($CMS::html_values{as_no}{value} =~ m/[a-z]|[A-Z]/) || (($CMS::html_values{as_no}{value} >= 64512) && ($CMS::html_values{as_no}{value} <= 65535))) {
	CMS::add_error("Failed to edit service.");
	CMS::add_error("Invalid AS number");
         $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
 
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();      
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();      
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();      
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
	if ($CMS::html_values{productcode}{value} =~ /IPTRANSIT/){
		$CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
        	$CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        	#unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);
	}else {
		$CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option('all');
	}
	CMS::output_html("service_edit2");

     } elsif($CMS::html_values{bgp_neighbor_ip}{value} =~ /^(0\.|10\.|14\.|24\.|39\.|127\.|128\.0\.|169.254\.|191\.255\.|192\.0\.0\.|192\.0\.2\.|192\.168\.|255\.255\.255\.)|^(172\.(1[6-9]|2[0-9]|31)\.)|^(2(2[4-9]|3[0-9]|4[0-9]|5[0-5])\.)/) {
	CMS::add_error("Failed to edit service.");
	CMS::add_error("Invalid BGP Neighbor IP - private / experimental addresses are not publicly routeable. See RFC 1918.");
         $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
 
        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();      
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();      
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();      
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();      
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
	$CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option('all');
	CMS::output_html("service_edit2");

     } elsif ($billerrmsg) {
        # Invalid billing option
        CMS::add_error("Failed to edit service.");
        CMS::add_error("$billerrmsg");
	#CMS::add_error("billerrmsg");
         $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();      
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option('all');
        $CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        $CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
        #for EPL Master Service
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        #unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);
	#Karuna on 16th Nov 2010 for IPT5
        $CMS::html_values{bha_auto_gen_prefix}{options} = CMS_SERVICE_DB::get_bha_auto_gen_prefix();
        $CMS::html_values{black_hole_routing}{options} = CMS_SERVICE_DB::get_black_hole_routing_option();
	##--IPT5


	#Added by Karuna for EVPL bug fix on 29th Mar 2010
	#++EVPL
        $CMS::html_values{portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
        $CMS::html_values{costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
        #--EVPL
        $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
        #++EVPL
        $CMS::html_values{aggregate_service_a}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
        $CMS::html_values{aggregate_service_z}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
        $CMS::html_values{aggregate_service_tpm}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_tpm();
        unshift(@{$CMS::html_values{aggregate_service_a}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_z}{options}}, [""]);
        unshift(@{$CMS::html_values{aggregate_service_tpm}{options}}, [""]);
        #--EVPL
	#End of bug fix added by Karuna

        CMS::output_html("service_edit2");
    } elsif (($CMS::html_values{productcode}{value} =~ /(ATM|FR)/) && ($CMS::html_values{serviceid_alt}{value} eq "")) {
        CMS::add_error("Failed to edit service.");
        CMS::add_error("Service ID for ATM/FR is required");

        $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();
        $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();
        $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();
	$CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	$CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
        $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        $CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option('all');

        CMS::output_html("service_edit2");
     } else {
	my $return_result = "";
        $CMS::html_values{'aend_jobno'}{value} =~ tr/a-z/A-Z/;
        $CMS::html_values{'zend_jobno'}{value} =~ tr/a-z/A-Z/;
        $CMS::html_values{'aend_country'}{value} =~ tr/a-z/A-Z/;
        $CMS::html_values{'zend_country'}{value} =~ tr/a-z/A-Z/;

       if($CMS::html_values{productcode}{value} =~ /VPLS/){
	if($CMS::html_values{portmode}{value} =~ /TRANS/){
		$CMS::html_values{aggregate_service}{value}='';
	}

	   $return_result = CMS_SERVICE_DB::update_service

          ($CMS::html_values{service}{value},
           $CMS::html_values{accno}{value},
           $CMS::html_values{description}{value},
           $CMS::html_values{bandwidth}{value},
           $CMS::html_values{productcode}{value},
           $CMS::html_values{switchsourcode}{value},
           $CMS::html_values{switchdestcode}{value},
           $CMS::html_values{pollingtype}{value},
           $CMS::html_values{costype}{value},
           $CMS::html_values{portmode}{value},
           $CMS::html_values{gold_classmap_v}{value},
           $CMS::html_values{gold_rateacl_v}{value},
           $CMS::html_values{silver_classmap_v}{value},
           $CMS::html_values{silver_rateacl_v}{value},
           $CMS::html_values{default_classmap_v}{value},
           $CMS::html_values{default_rateacl_v}{value},
           $CMS::html_values{class1_classmap_v}{value},
           $CMS::html_values{class1_rateacl_v}{value},
           $CMS::html_values{class2_classmap_v}{value},
           $CMS::html_values{class2_rateacl_v}{value},
           $CMS::html_values{class4_classmap_v}{value},
           $CMS::html_values{class4_rateacl_v}{value},
          # $CMS::html_values{as_no}{value},
          # $CMS::html_values{auto_gen_prefix}{value},
	  # '',
          # $CMS::html_values{bgp_neighbor_ip}{value},
          # $CMS::html_values{irrd_object}{value},
           $CMS::html_values{aggregate_service}{value},
	   $CMS::html_values{master_service}{value},
           $CMS::html_values{billable}{value},
           $CMS::html_values{'billstartdate.year'}{value},
           $CMS::html_values{'billstartdate.month'}{value},
           $CMS::html_values{'billstartdate.day'}{value},
           '00',
           '00',
           '00',
	   $CMS::html_values{'billenddate.year'}{value},
           $CMS::html_values{'billenddate.month'}{value},
           $CMS::html_values{'billenddate.day'}{value},
           '23',
           '59',
           '59',
           $CMS::html_values{serviceid_alt}{value},
           $CMS::html_values{'aend_jobid'}{value},
           $CMS::html_values{'zend_jobid'}{value},
           $CMS::html_values{'aend_jobno'}{value},
           $CMS::html_values{'zend_jobno'}{value},
           $CMS::html_values{'aend_country'}{value},
           $CMS::html_values{'zend_country'}{value}
          );
           }
	#++EVPL -- Added for A End and Z End
      elsif ($CMS::html_values{productcode}{value} =~ /EVPL/){
			my $serv_a=($CMS::html_values{service}{value})."a";
			my $serv_z=($CMS::html_values{service}{value})."z";
			my ($portmode_evpl,$swt_serv_a,$swt_serv_z);
			
			if($CMS::html_values{portmode_e}{value} =~ /EVPL VLAN Service/){
				$portmode_evpl='VPM';
				$swt_serv_a= $CMS::html_values{aggregate_service_a}{value};
				$swt_serv_z= $CMS::html_values{aggregate_service_z}{value}
			}else{
				$portmode_evpl='TPM';
				$swt_serv_a=$CMS::html_values{aggregate_service_tpm}{value};
				$swt_serv_a=~s/sw/asw/;
				$swt_serv_z=$CMS::html_values{aggregate_service_tpm}{value};
				$swt_serv_z=~s/sw/zsw/;				
				$CMS::html_values{costype_e}{value}='';
			}
		   $return_result = CMS_SERVICE_DB::update_service
	          ($serv_a,
	           $CMS::html_values{accno}{value},
	           $CMS::html_values{description}{value},
	           $CMS::html_values{bandwidth}{value},
	           $CMS::html_values{productcode}{value},
	           'null',
	           'null',
	           $portmode_evpl,
	           $CMS::html_values{costype_e}{value},
	           '',
	           $CMS::html_values{gold_classmap_e}{value},
	           $CMS::html_values{gold_rateacl_e}{value},
	           $CMS::html_values{silver_classmap_v}{value},
	           $CMS::html_values{silver_rateacl_v}{value},
	           $CMS::html_values{default_classmap_e}{value},
	           $CMS::html_values{default_rateacl_e}{value},
	           $CMS::html_values{class1_classmap_v}{value},
	           $CMS::html_values{class1_rateacl_v}{value},
	           $CMS::html_values{class2_classmap_v}{value},
	           $CMS::html_values{class2_rateacl_v}{value},
	           $CMS::html_values{class4_classmap_v}{value},
	           $CMS::html_values{class4_rateacl_v}{value},
	           #$CMS::html_values{as_no}{value},
		   	#	'',
	           #$CMS::html_values{bgp_neighbor_ip}{value},
	           #$CMS::html_values{irrd_object}{value},
	           $swt_serv_a,
		       $CMS::html_values{master_service}{value},
	           $CMS::html_values{billable}{value},
	           $CMS::html_values{'billstartdate.year'}{value},
	           $CMS::html_values{'billstartdate.month'}{value},
	           $CMS::html_values{'billstartdate.day'}{value},
	           '00',
	           '00',
	           '00',
		   		$CMS::html_values{'billenddate.year'}{value},
	           $CMS::html_values{'billenddate.month'}{value},
	           $CMS::html_values{'billenddate.day'}{value},
	           '23',
	           '59',
	           '59',
	           $CMS::html_values{serviceid_alt}{value},
	           $CMS::html_values{'aend_jobid'}{value},
	           $CMS::html_values{'zend_jobid'}{value},
	           $CMS::html_values{'aend_jobno'}{value},
	           $CMS::html_values{'zend_jobno'}{value},
	           $CMS::html_values{'aend_country'}{value},
	           $CMS::html_values{'zend_country'}{value}
	          );
		   $return_result = CMS_SERVICE_DB::update_service
	          ($serv_z,
	           $CMS::html_values{accno}{value},
	           $CMS::html_values{description}{value},
	           $CMS::html_values{bandwidth}{value},
	           $CMS::html_values{productcode}{value},
	           'null',
	           'null', 
	           $portmode_evpl, 
	           $CMS::html_values{costype_e}{value},
	           '', 
	           $CMS::html_values{gold_classmap_e}{value},
	           $CMS::html_values{gold_rateacl_e}{value},
	           $CMS::html_values{silver_classmap_v}{value},
	           $CMS::html_values{silver_rateacl_v}{value},
	           $CMS::html_values{default_classmap_e}{value},
	           $CMS::html_values{default_rateacl_e}{value},
	           $CMS::html_values{class1_classmap_v}{value},
	           $CMS::html_values{class1_rateacl_v}{value},
	           $CMS::html_values{class2_classmap_v}{value},
	           $CMS::html_values{class2_rateacl_v}{value},
	           $CMS::html_values{class4_classmap_v}{value},
	           $CMS::html_values{class4_rateacl_v}{value},
	           #$CMS::html_values{as_no}{value},
		   	#	'',
	           #$CMS::html_values{bgp_neighbor_ip}{value},
	           #$CMS::html_values{irrd_object}{value},
	           $swt_serv_z,
		       $CMS::html_values{master_service}{value},
	           $CMS::html_values{billable}{value},
	           $CMS::html_values{'billstartdate.year'}{value},
	           $CMS::html_values{'billstartdate.month'}{value},
	           $CMS::html_values{'billstartdate.day'}{value},
	           '00',
	           '00',
	           '00',
		   		$CMS::html_values{'billenddate.year'}{value},
	           $CMS::html_values{'billenddate.month'}{value},
	           $CMS::html_values{'billenddate.day'}{value},
	           '23',
	           '59',
	           '59',
	           $CMS::html_values{serviceid_alt}{value},
	           $CMS::html_values{'aend_jobid'}{value},
	           $CMS::html_values{'zend_jobid'}{value},
	           $CMS::html_values{'aend_jobno'}{value},
	           $CMS::html_values{'zend_jobno'}{value},
	           $CMS::html_values{'aend_country'}{value},
	           $CMS::html_values{'zend_country'}{value}
	          );		
		}	       
	#--EVPL
	#Added by Nisha for mVPN on 30Sep2009
	elsif ($CMS::html_values{productcode}{value} =~ /MPLS/){
		#if the muticast enabled value is NO then update all the mcast fields as blank
        	if($CMS::html_values{mvpnCheck}{value} == 2){
         		$CMS::html_values{voice_mvpn_in}{value} = "";
         		$CMS::html_values{video_mvpn_in}{value} = "";
         		$CMS::html_values{video_mvpn_out}{value} = "";
         		$CMS::html_values{criticaldata_mvpn_in}{value} = "";
         		$CMS::html_values{criticaldata_mvpn_out}{value} = "";
         		$CMS::html_values{interactivedata_mvpn_in}{value} = "";
         		$CMS::html_values{interactivedata_mvpn_out}{value} = "";
         		$CMS::html_values{standarddata_mvpn_in}{value} = "";
         		$CMS::html_values{standarddata_mvpn_out}{value} = "";
         		$CMS::html_values{lowpriority_mvpn_in}{value} = "";
         		$CMS::html_values{lowpriority_mvpn_out}{value} = "";
        	}
                #Added by Stavan on 02Sept2010 to prevent wrong updation of switchsourcecode and switchdestcode
                $CMS::html_values{switchsourcode}{value} = "";
                $CMS::html_values{switchdestcode}{value} = "";

		$return_result = CMS_SERVICE_DB::update_service
          ($CMS::html_values{service}{value},
           $CMS::html_values{accno}{value},
           $CMS::html_values{description}{value},
           $CMS::html_values{bandwidth}{value},
           $CMS::html_values{productcode}{value},
           $CMS::html_values{switchsourcode}{value},
           $CMS::html_values{switchdestcode}{value},
           $CMS::html_values{pollingtype}{value},
           $CMS::html_values{costype}{value},
           $CMS::html_values{portmode}{value},
           $CMS::html_values{gold_classmap}{value},
           $CMS::html_values{gold_rateacl}{value},
           $CMS::html_values{silver_classmap}{value},
           $CMS::html_values{silver_rateacl}{value},
           $CMS::html_values{default_classmap}{value},
           $CMS::html_values{default_rateacl}{value},
           $CMS::html_values{class1_classmap}{value},
           $CMS::html_values{class1_rateacl}{value},
           $CMS::html_values{class2_classmap}{value},
           $CMS::html_values{class2_rateacl}{value},
           $CMS::html_values{class4_classmap}{value},
           $CMS::html_values{class4_rateacl}{value},
           #$CMS::html_values{as_no}{value},
           #$CMS::html_values{auto_gen_prefix}{value},
           #$CMS::html_values{bgp_neighbor_ip}{value},
           #$CMS::html_values{irrd_object}{value},
           $CMS::html_values{aggregate_service}{value},
           $CMS::html_values{master_service}{value},
           $CMS::html_values{billable}{value},
           $CMS::html_values{'billstartdate.year'}{value},
           $CMS::html_values{'billstartdate.month'}{value},
           $CMS::html_values{'billstartdate.day'}{value},
           '00',
           '00',
           '00',
           $CMS::html_values{'billenddate.year'}{value},
           $CMS::html_values{'billenddate.month'}{value},
           $CMS::html_values{'billenddate.day'}{value},
           '23',
           '59',
           '59',
           $CMS::html_values{serviceid_alt}{value},
           $CMS::html_values{'aend_jobid'}{value},
           $CMS::html_values{'zend_jobid'}{value},
           $CMS::html_values{'aend_jobno'}{value},
           $CMS::html_values{'zend_jobno'}{value},
           $CMS::html_values{'aend_country'}{value},
           $CMS::html_values{'zend_country'}{value},
           $CMS::html_values{voice_mvpn_in}{value},
           $CMS::html_values{video_mvpn_in}{value},
           $CMS::html_values{video_mvpn_out}{value},
           $CMS::html_values{criticaldata_mvpn_in}{value},
           $CMS::html_values{criticaldata_mvpn_out}{value},
           $CMS::html_values{interactivedata_mvpn_in}{value},
           $CMS::html_values{interactivedata_mvpn_out}{value},
           $CMS::html_values{standarddata_mvpn_in}{value},
           $CMS::html_values{standarddata_mvpn_out}{value},
           $CMS::html_values{lowpriority_mvpn_in}{value},
           $CMS::html_values{lowpriority_mvpn_out}{value},
           $CMS::html_values{mvpnCheck}{value}
          );

	}
	#End of code Added by Nisha for mVPN on 30Sep2009
                elsif ($CMS::html_values{productcode}{value} =~ /EPL/) {
                #Added by Karuna for EPL for billing
                if (($CMS::html_values{service}{value} =~ /ew/i)&&($CMS::html_values{epl_billable}{value} == 2)){
		#open (EPL1, ">>/data1/tmp_log/addservice.log");
                #print EPL1 "inside bill check2\n";
                #print EPL1 "$CMS::html_values{epl_billable2}{value}\n";
                #$CMS::html_values{'billstartdate.year'}{value} = "----";
                $CMS::html_values{'billenddate.year'}{value} = "----";
		} elsif (($CMS::html_values{service}{value} =~ /em/i)&&($CMS::html_values{epl_billable2}{value} == 2)){
                $CMS::html_values{'billenddate.year'}{value} = "----";
                }
                #Added by Karuna for EPL Edit Service
                $return_result = CMS_SERVICE_DB::update_service
                ($CMS::html_values{service}{value},
                $CMS::html_values{accno}{value},
                $CMS::html_values{description}{value},
                $CMS::html_values{bandwidth}{value},
                $CMS::html_values{productcode}{value},
                $CMS::html_values{'epl_msid'}{value},
                $CMS::html_values{'epl_aend_jobid'}{value},
                $CMS::html_values{'epl_zend_jobid'}{value},
                $CMS::html_values{'epl_aend_jobno'}{value},
                $CMS::html_values{'epl_zend_jobno'}{value},
                $CMS::html_values{'epl_aend_jobid2'}{value},
                $CMS::html_values{'epl_zend_jobid2'}{value},
                $CMS::html_values{'epl_aend_jobno2'}{value},
                $CMS::html_values{'epl_zend_jobno2'}{value},
                $CMS::html_values{'epl_billable'}{value},
                $CMS::html_values{'epl_billable2'}{value},
                $CMS::html_values{'billstartdate.year'}{value},
                $CMS::html_values{'billstartdate.month'}{value},
                $CMS::html_values{'billstartdate.day'}{value},
                '00',
                '00',
                '00',
                $CMS::html_values{'billenddate.year'}{value},
                $CMS::html_values{'billenddate.month'}{value},
                $CMS::html_values{'billenddate.day'}{value},
                '23',
                '59',
                '59'
                );

	} elsif ($CMS::html_values{productcode}{value} =~ /IPTRANSIT/) {
  		open (IPT2, ">>/data1/tmp_log/editservice.log");
		print IPT2 "ipt_msid: $CMS::html_values{ipt_msid}{value}\n";
		$return_result = CMS_SERVICE_DB::update_service
	        ($CMS::html_values{service}{value},
           	$CMS::html_values{ipt_accno}{value},
           	$CMS::html_values{description}{value},
           	$CMS::html_values{bandwidth}{value},
           	$CMS::html_values{productcode}{value},
           	$CMS::html_values{as_no}{value},
           	$CMS::html_values{auto_gen_prefix}{value},
           	$CMS::html_values{bgp_neighbor_ip}{value},
           	$CMS::html_values{irrd_object}{value},
           	$CMS::html_values{ipt_msid}{value},
		$CMS::html_values{black_hole_routing}{value},
		$CMS::html_values{bha_bgp_neighbor_ip}{value},
		$CMS::html_values{bha_auto_gen_prefix}{value});
	} elsif ($CMS::html_values{productcode}{value} =~ /GIA/) {
		open (IPT2, ">>/data1/tmp_log/editservice.log");
                print IPT2 "PM FILE:black_hole_routing:$CMS::html_values{black_hole_routing}{value}\tbha_bgp_neighbor_ip:$CMS::html_values{bha_bgp_neighbor_ip}{value}\tbha_auto_gen_prefix:$CMS::html_values{bha_auto_gen_prefix}{value}\n";
		$return_result = CMS_SERVICE_DB::update_service
		($CMS::html_values{service}{value},
                $CMS::html_values{accno}{value},
                $CMS::html_values{description}{value},
                $CMS::html_values{bandwidth}{value},
                $CMS::html_values{productcode}{value},
                $CMS::html_values{as_no}{value},
                $CMS::html_values{auto_gen_prefix}{value},
                $CMS::html_values{bgp_neighbor_ip}{value},
                $CMS::html_values{irrd_object}{value},
		$CMS::html_values{aggregate_service}{value},
		$CMS::html_values{black_hole_routing}{value},
                $CMS::html_values{bha_bgp_neighbor_ip}{value},
                $CMS::html_values{bha_auto_gen_prefix}{value},
		$CMS::html_values{billable}{value},
           	$CMS::html_values{'billstartdate.year'}{value},
           	$CMS::html_values{'billstartdate.month'}{value},
           	$CMS::html_values{'billstartdate.day'}{value},
           	'00',
           	'00',
           	'00',
           	$CMS::html_values{'billenddate.year'}{value},
           	$CMS::html_values{'billenddate.month'}{value},
           	$CMS::html_values{'billenddate.day'}{value},
           	'23',
           	'59',
           	'59');
	} else{
                #Added by Stavan for keeping defaults for VLA on 18Aug2010
                if($CMS::html_values{productcode}{value} =~ /ETHERNET/){
                        $CMS::html_values{pollingtype}{value} = "TRANS";
                        $CMS::html_values{costype}{value} = "";
                        $CMS::html_values{portmode}{value} = "";
                }
                #Added by Stavan from preventing wrong update of CosType,switchsourcode and switchdestcode on 25Aug2010
                 if($CMS::html_values{productcode}{value} =~ /ATM/ or $CMS::html_values{productcode}{value} =~ /FR/ or $CMS::html_values{productcode}{value} =~ /GIA/ or $CMS::html_values{productcode}{value} =~ /GBS/){
                        $CMS::html_values{costype}{value} = "";
                        $CMS::html_values{switchsourcode}{value} = "";
                        $CMS::html_values{switchdestcode}{value} = "";
                }

          $return_result = CMS_SERVICE_DB::update_service
	  ($CMS::html_values{service}{value},
	   $CMS::html_values{accno}{value},
	   $CMS::html_values{description}{value},
	   $CMS::html_values{bandwidth}{value},
	   $CMS::html_values{productcode}{value},
	   $CMS::html_values{switchsourcode}{value},
	   $CMS::html_values{switchdestcode}{value},
	   $CMS::html_values{pollingtype}{value},
	   $CMS::html_values{costype}{value},
           $CMS::html_values{portmode}{value},
	   $CMS::html_values{gold_classmap}{value},
	   $CMS::html_values{gold_rateacl}{value},
	   $CMS::html_values{silver_classmap}{value},
	   $CMS::html_values{silver_rateacl}{value},
	   $CMS::html_values{default_classmap}{value},
	   $CMS::html_values{default_rateacl}{value},
	   $CMS::html_values{class1_classmap}{value},
           $CMS::html_values{class1_rateacl}{value},
           $CMS::html_values{class2_classmap}{value},
           $CMS::html_values{class2_rateacl}{value},
           $CMS::html_values{class4_classmap}{value},
           $CMS::html_values{class4_rateacl}{value},
	   $CMS::html_values{as_no}{value},
	   $CMS::html_values{auto_gen_prefix}{value},
	   $CMS::html_values{bgp_neighbor_ip}{value},
           $CMS::html_values{irrd_object}{value},
	   $CMS::html_values{aggregate_service}{value},
	   $CMS::html_values{master_service}{value},
	   $CMS::html_values{billable}{value},
           $CMS::html_values{'billstartdate.year'}{value},
           $CMS::html_values{'billstartdate.month'}{value},
           $CMS::html_values{'billstartdate.day'}{value},
           '00',
           '00',
           '00',
           $CMS::html_values{'billenddate.year'}{value},
           $CMS::html_values{'billenddate.month'}{value},
           $CMS::html_values{'billenddate.day'}{value},
           '23',
           '59',
           '59',
           $CMS::html_values{serviceid_alt}{value},
           $CMS::html_values{'aend_jobid'}{value},
           $CMS::html_values{'zend_jobid'}{value},
           $CMS::html_values{'aend_jobno'}{value},
           $CMS::html_values{'zend_jobno'}{value},
           $CMS::html_values{'aend_country'}{value},      
           $CMS::html_values{'zend_country'}{value}
          );
	}
          
   	 # have edited service
	if ($return_result)
         {
		open (IPT2, ">>/data1/tmp_log/editservice.log");
		print IPT2 "PM return result:$return_result\n";
		# ++ Added for User Log after editing service - Sayantan dutta - 11-Nov-2009
		user_log "Edit Service";
		# -- Added for User Log after editing service - Sayantan dutta - 11-Nov-2009

	 CMS::add_error("Service edited successfully.");
	 CMS::output_html("service_edit");
	
       } else {
	 # Failed to edit service
	 CMS::add_error("Failed to edit service.");
	 #commented out by Karuna on 18-11-2010 for consistency 
	 #CMS::add_error("DB_error");
	 CMS::add_error($CMS_SERVICE_DB::error);
         $CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();      
         $CMS::html_values{switchsourcode}{options} = CMS_SERVICE_DB::get_switchcode();      
         $CMS::html_values{switchdestcode}{options} = CMS_SERVICE_DB::get_switchcode();      
         $CMS::html_values{auto_gen_prefix}{options} = CMS_SERVICE_DB::get_auto_gen_prefix();      
	 $CMS::html_values{pollingtype}{options} = CMS_SERVICE_DB::get_pollingtype();
	 $CMS::html_values{costype}{options} = CMS_SERVICE_DB::get_costype();
        $CMS::html_values{portmode}{options} = CMS_SERVICE_DB::get_portmode();
         $CMS::html_values{aggregate_service}{options} = CMS_SERVICE_DB::get_aggregate_service();      
	$CMS::html_values{master_service}{options} = CMS_SERVICE_DB::get_master_service();
        #Karuna on 18th Nov 2010 for IPT5
	##++IPT5
        $CMS::html_values{bha_auto_gen_prefix}{options} = CMS_SERVICE_DB::get_bha_auto_gen_prefix();
        $CMS::html_values{black_hole_routing}{options} = CMS_SERVICE_DB::get_black_hole_routing_option();
        ##--IPT5

	#Added by Karuna for IP Transit
	if ($CMS::html_values{productcode}{value} =~ /IPTRANSIT/) {
        	$CMS::html_values{productcode}{options} = CMS_SERVICE_DB::get_productcode();      
		open (IPT2, ">>/data1/tmp_log/editservice.log");
		$CMS::html_values{billable}{value} = 2;
		print IPT2 "edit - SERVICE.pm billable: $CMS::html_values{billable}{value}\n";
		#$CMS::html_values{epl_billable}{value} = 2;
		#$CMS::html_values{epl_billable2}{value} = 2;
		$CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option();
	} else {
		$CMS::html_values{billable}{options} = CMS_SERVICE_DB::get_billing_option('all');
        	#Added by Karuna for EPL billing options
        	$CMS::html_values{epl_billable}{options} = CMS_SERVICE_DB::get_billing_option();
        	$CMS::html_values{epl_billable2}{options} = CMS_SERVICE_DB::get_billing_option();
	}

        #Added by Karuna for EPL master service ID
        $CMS::html_values{epl_msid}{options} = CMS_SERVICE_DB::get_epl_master_service();
        unshift(@{$CMS::html_values{epl_msid}{options}}, ["-- Select --"]);
       #Added by Karuna for IPT master service ID
        $CMS::html_values{ipt_msid}{options} = CMS_SERVICE_DB::get_master_service("IPTRANSIT");
        #unshift(@{$CMS::html_values{ipt_msid}{options}}, ["-- Select --"]);

	 CMS::output_html("service_edit2");
       }
     }
    }
       
  } elsif ($CMS::html_values{command}{value} eq "delete_service") {
    # delete a new service
	#open (IPT5, ">>/data1/tmp_log/deleteservice.log");
    
    if (not defined ($CMS::html_values{stage}{value})) {
      # first stage
      
      CMS::output_html("service_delete");
    } elsif ($CMS::html_values{stage}{value} == 2) {
	if ($debug){print IPT_DBG "delete stage = 2\n";}
	if ($debug){print IPT_DBG "submit: $CMS::html_values{submit}{value}\n";}
	if ($debug){print IPT_DBG "hidden_param: $CMS::html_values{hidden_param}{value}\n";}
	if ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} =~ /cancel/i)) 
	{
		if ($debug){print IPT_DBG "submit = cancel\n";}
    		CMS::clear_html_values();
    		$CMS::html_values{subsystem}{value} = "service";
    		#$CMS::html_values{command}{value} = "delete_service";
		CMS::output_html("service_delete");
  	}
	my $servid = $CMS::html_values{service}{value};
        my $accno = $CMS::html_values{accno}{value};
	my $ref = CMS_SERVICE_DB::get_productcode_service($servid);
	$CMS::html_values{productcode}{value} = $ref->{productcodeid};
	if ($debug){print IPT_DBG "productcode : $ref->{productcodeid}\n";}
	my $last_asn;
	if ($debug){print IPT_DBG "PM hidden_param:$CMS::html_values{hidden_param}{value}\n";}
        #++EVPL-- Check for '[]' and 'ec' in the serviceid. If it exists then delete both a and z entries from the database
	my $servid_z;
	my $res_z;
	if ($debug){print IPT_DBG "PM service_delete productcode:$CMS::html_values{productcode}{value}\n";}
	if(($CMS::html_values{productcode}{value} eq "EVPL") && ($servid =~ /.*\[.*\].*ec.*/)){
	$servid_z=$servid;
	$servid_z = $servid_z."z";
	$servid .="a"; 
        #--EVPL 
	} ##Added by Karuna on 17-Dec-2010
	##++IPT5
	elsif (($CMS::html_values{productcode}{value} eq "GIA") || ($CMS::html_values{productcode}{value} eq "IPTRANSIT")){
		$last_asn = CMS_SERVICE_DB::get_last_asn($servid);
		#$last_asn = $CMS::html_values{hidden_param}{value};
		if ($debug){print IPT_DBG "PM service_delete $CMS::html_values{productcode}{value} last_asn:$last_asn\n";}
	}
	my $res;
	if ($last_asn eq "Yes"){
		$res = CMS_SERVICE_DB::delete_service($servid,$accno,$last_asn);
	} else {
		$res = CMS_SERVICE_DB::delete_service($servid,$accno);
	}
	if($servid_z){
	$res_z = CMS_SERVICE_DB::delete_service($servid_z,$accno);
	}
      
      if ($res || $res_z){
	# have deleted service
	# ++ Added for User Log after deleting service - Sayantan dutta - 11-Nov-2009
	user_log "Delete Service";
	# -- Added for User Log after deleting service - Sayantan dutta - 11-Nov-2009
	CMS::add_error("Service deleted successfully.");
	CMS::output_html("service_delete");
	
      } else {
	# Failed to delete service
	my $err = $CMS_SERVICE_DB::error;
	CMS::add_error("Failed to delete service.");
	if($err =~ /link/){
		CMS::add_error("Link record exists for the service.");
	}if($err =~ /ip_resource/) {
		CMS::add_error("IP Address record exists for the service.");
	}
	if ($err =~ /Account does not have such a service/){
		CMS::add_error("Service ID does not exist.");
	}
	  CMS::output_html("service_delete");
      }
    }
  } elsif ($CMS::html_values{command}{value} eq "view_prefix_list") {
    generate_prefix_list($CMS::html_values{view_bgp_ip}{value});
  ##++IPT5
  } elsif ($CMS::html_values{command}{value} eq "view_prefix_list_bha") {
    generate_prefix_list_bha($CMS::html_values{view_bgp_ip}{value});
  ##--IPT 5
  ##++Addendum 2 BHA Requirment.
   } elsif ($CMS::html_values{command}{value} eq "view_prefix_list_bha_backup") {
    generate_prefix_list_bha_backup($CMS::html_values{view_bgp_ip}{value});
  ##--Addendum 2 BHA Requirment.
  } elsif ($CMS::html_values{command}{value} eq "search_service") {
    if ((defined $CMS::html_values{submit}{value}) &&
	($CMS::html_values{submit}{value} eq "Search")) {
	
	my $res = "";
	my $res_a = "";
	my $res_mpls = "";		#Added by Nisha for mVPN:06oct2009
	my $result_gen = "";
	my ($res_epl, $res_ipt, $res_gia) = "";   #Added by Karuna for EPL:04Mar2010
	my $res_ethernet = "";  	#Added by Stavan for VLA:18Aug2010
#++EVPL -- Calling search_service_evpl function 
     if($CMS::html_values{search_productcode}{value} =~ /EVPL/){

     $res = CMS_SERVICE_DB::search_service_evpl
        ($CMS::html_values{search_serviceid}{value},
         $CMS::html_values{search_accno}{value},
         $CMS::html_values{search_description}{value},
         $CMS::html_values{search_bandwidth}{value},
         $CMS::html_values{search_productcode}{value},
         $CMS::html_values{search_portmode_e}{value},
         $CMS::html_values{search_costype_e}{value},
         $CMS::html_values{search_switch_evpl_a}{value},
         $CMS::html_values{search_switch_evpl_z}{value},
         $CMS::html_values{search_switch_service_tpm}{value}
        );

	$CMS::html_values{services}{table} = $res;
        unshift (@{$CMS::html_values{services}{table}},["Service", "End","Customer", "Description", "Bandwidth", "Product Code", "Router", "Interface", "Switch Service", "Port Mode", "Standard A->Z", "Standard Z->A", "Premium A->Z", "Premium Z->A", "Usage Based Billing", "Billing Start", "Billing End"]);
     }
#--EVPL
#Added by Nisha for mVPN:06oct2009
     elsif($CMS::html_values{search_productcode}{value} =~ /MPLS/){

	#When product code MPLS is selected
	$res_mpls = CMS_SERVICE_DB::search_service_mpls
        ($CMS::html_values{search_serviceid}{value},
         $CMS::html_values{search_accno}{value},
         $CMS::html_values{search_description}{value},
         $CMS::html_values{search_bandwidth}{value},
         $CMS::html_values{search_productcode}{value},
         $CMS::html_values{mvpnCheck}{value}
        );

        $CMS::html_values{services}{table} = $res_mpls;

        unshift (@{$CMS::html_values{services}{table}},["Service", "Customer", "Description", "Bandwidth", "Product Code", "Router", "Interface", "Aggregate Service", "IPVPN Polling Type / Port Mode", "Voice/Gold [IN] RateACL No. / ClassMap Name", "Voice/Gold [OUT] ClassMap Name", "Video/Silver Plus [IN] RateACL No. / ClassMap Name", "Video/Silver Plus [OUT] ClassMap Name", "Critical/Silver [IN] RateACL No. / ClassMap Name", "Critical/Silver [OUT] ClassMap Name", "Interactive Data (Telstra only) [IN] RateACL No. / ClassMap Name", "Interactive Data (Telstra only) [OUT] ClassMap Name", "Standard (Telstra only) [IN] RateACL No. / ClassMap Name", "Standard (Telstra only) [OUT] ClassMap Name", "Low Priority Data/Bronze [IN] RateACL No. / ClassMap Name", "Low Priority Data/Bronze [OUT] ClassMap Name", "MCAST Voice [IN] ClassMap Name", "MCAST Video [IN] ClassMap Name", "MCAST Video [OUT] ClassMap Name", "MCAST Critical [IN] ClassMap Name", "MCAST Critical [OUT] ClassMap Name", "MCAST Interactive [IN] ClassMap Name", "MCAST Interactive [OUT] ClassMap Name", "MCAST Standard [IN] ClassMap Name", "MCAST Standard [OUT] ClassMap Name", "MCAST Low Priority [IN] ClassMap Name", "MCAST Low Priority [OUT] ClassMap Name", "Usage Based Billing", "Billing Start", "Billing End"]);

     }
#End of code Added by Nisha for mVPN:06oct2009
#Code added by Karuna for EPL Search Service on 04Mar2010
     elsif($CMS::html_values{search_productcode}{value} =~ /EPL/){
 	open (EPL1, ">>/data1/tmp_log/addservice.log");	
	print EPL1 "PM entered EPL search service\n";
	print EPL1 "PM service:$CMS::html_values{search_serviceid}{value}\tsearch_accno:$CMS::html_values{search_accno}{value}\tsearch_description:$CMS::html_values{search_description}{value}\tsearch_bandwidth:$CMS::html_values{search_bandwidth}{value}\tsearch_productcode:$CMS::html_values{search_productcode}{value}\tepl_master:$CMS::html_values{epl_master}{value}\tepl_aend_jobid:$CMS::html_values{epl_aend_jobid}{value}\tepl_zend_jobid:$CMS::html_values{epl_zend_jobid}{value}\tepl_aend_jobno:$CMS::html_values{epl_aend_jobno}{value}\tepl_zend_jobno:$CMS::html_values{epl_zend_jobno}{value}\n";

        #When product code EPL is selected
        $res_epl = CMS_SERVICE_DB::search_service_epl
        ($CMS::html_values{search_serviceid}{value},
         $CMS::html_values{search_accno}{value},
         $CMS::html_values{search_description}{value},
         $CMS::html_values{search_bandwidth}{value},
         $CMS::html_values{search_productcode}{value},
         $CMS::html_values{epl_master}{value},
         $CMS::html_values{epl_aend_jobid}{value},
         $CMS::html_values{epl_zend_jobid}{value},
         $CMS::html_values{epl_aend_jobno}{value},
         $CMS::html_values{epl_zend_jobno}{value}

        );
        #bandwidth conversion to Mbps
        for my $i (0 .. $#{$res_epl}) {
        my $epl_bw = $res_epl->[$i]->[3];
        $epl_bw = $epl_bw/1000000;
        $res_epl->[$i]->[3] = $epl_bw;
        }

        $CMS::html_values{services}{table} = $res_epl;
        #EPL Search Service table
        unshift (@{$CMS::html_values{services}{table}},["Service", "Customer", "Description", "Bandwidth (Mbps)", "Product Code", "Master Service", "Usage Based Billing", "Billing Start", "Billing End", "EPL A-end Job ID", "EPL Z-end Job ID", "EPL A-end Job Note", "EPL Z-end Job Note"]);

     }
        #Added by Stavan for VLA on 18Aug2010 when product code ETHERNET is selected
        elsif($CMS::html_values{search_productcode}{value} =~ /ETHERNET/){
        $res_ethernet = CMS_SERVICE_DB::search_service_ethernet
        ($CMS::html_values{search_serviceid}{value},
         $CMS::html_values{search_accno}{value},
         $CMS::html_values{search_description}{value},
         $CMS::html_values{search_bandwidth}{value},
         $CMS::html_values{search_productcode}{value}
        );
        $CMS::html_values{services}{table} = $res_ethernet;
        #ETHERNET Search Service table
        unshift (@{$CMS::html_values{services}{table}},["Service", "Customer", "Description", "Bandwidth (bps)", "Product Code", "Router", "Interface"]);

     } elsif ($CMS::html_values{search_productcode}{value} =~ /IPTRANSIT/) {
	open (IPT3, ">/data1/tmp_log/searchserv.log");
	print IPT3 "entered search IPT\n";
	print IPT3 "SID:$CMS::html_values{search_serviceid}{value}\taccno:$CMS::html_values{search_accno}{value}\tdescription:$CMS::html_values{search_description}{value}\tbandwidth:$CMS::html_values{search_bandwidth}{value}\tproductcode:$CMS::html_values{search_productcode}{value}\tas:$CMS::html_values{search_as}{value}\tirrd:$CMS::html_values{search_irrd}{value}\tmaster:$CMS::html_values{search_master}{value}\tip:$CMS::html_values{bgp_ip}{value}\n";

	$res_ipt = CMS_SERVICE_DB::search_service
	 ($CMS::html_values{search_serviceid}{value},
         $CMS::html_values{search_accno}{value},
         $CMS::html_values{search_description}{value},
         $CMS::html_values{search_bandwidth}{value},
         $CMS::html_values{search_productcode}{value},
	 $CMS::html_values{search_as}{value},
	 $CMS::html_values{search_irrd}{value},
	 $CMS::html_values{search_master}{value},
	 $CMS::html_values{prefix_list}{value},
         $CMS::html_values{bgp_ip}{value},
	 $CMS::html_values{bha_routing}{value},
         $CMS::html_values{bha_prefix_list}{value},
         $CMS::html_values{bha_bgp_ip}{value}
	);
	#for my $i (0 .. $#{$res_ipt}) {
	#my $ipt_bw = $res_ipt->[$i]->[3];
	#print IPT3 "PM bandwidth:$res_ipt->[$i]->[3]\n";
	#}
	foreach my $request (@{$res_ipt}) {
	print IPT3 "request:$$request[6]\n";
		if ($$request[8]){
		$$request[11] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list&view_bgp_ip=$$request[8]\" target=\"new_win\">View</a>";
		}
               ## ++Addendum 2 BHA Requirment. 
		if ($$request[17]){
		$$request[14] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list_bha&view_bgp_ip=$$request[17]\" target=\"new_win\">View</a>";
                  $$request[16] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication
_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list_bha_backup&view_bgp_ip=$$request[17]\"
 target=\"new_win\">View</a>";

		}
	}
       ##-- Addendum 2 BHA Requirment.	
	#print IPT3 "PM res_ipt: $res_ipt $$res_ipt[3]\n";
	 $CMS::html_values{services}{table} = $res_ipt;
	#print IPT3 "PM res_ipt: @{$CMS::html_values{services}{table}}\n";
       ##++Modified for Addendum 2 BHA Requirment. 
	unshift (@{$CMS::html_values{services}{table}},
["Service", "Customer", "Description", "Link Bandwidth", "Product Code", "Router", "Interface", "AS No.", "BGP Neighbor IP", "AutoPrefix List", "IRRD Object", "Prefix List","Black Hole Routing", "BHA Router Primary","BHA Prefix List Primary",  "BHA Router Backup","BHA Prefix List Backup","BHA BGP Neighbor IP", "BHA AutoPrefix List","Master Service", "IPC Service", "Usage Based Billing", "Billing Start", "Billing End"]);
       ##--Modified for Addendum 2BHA Requirment.
	##09-Nov-2010 IP Transit Ph5 - Karuna
	} elsif ($CMS::html_values{search_productcode}{value} =~ /GIA/) {
	open (IPT3, ">>/data1/tmp_log/searchserv.log");
	$res_gia = CMS_SERVICE_DB::search_service
	($CMS::html_values{search_serviceid}{value},
         $CMS::html_values{search_accno}{value},
         $CMS::html_values{search_description}{value},
         $CMS::html_values{search_bandwidth}{value},
         $CMS::html_values{search_productcode}{value},
         $CMS::html_values{search_as}{value},
         $CMS::html_values{search_irrd}{value},
         $CMS::html_values{gia_aggregate}{value},
	 $CMS::html_values{prefix_list}{value},
	 $CMS::html_values{bgp_ip}{value},
	 $CMS::html_values{bha_routing}{value},
	 $CMS::html_values{bha_prefix_list}{value},
	 $CMS::html_values{bha_bgp_ip}{value}
	);

	foreach my $request (@{$res_gia}) {
		print IPT3 "request8:$$request[8]\n";
		print IPT3 "request14:$$request[17]\n";
		my $bgp_val = $$request[8];
		my $bha_bgp_val = $$request[17];
		if ($$request[8]){
			print IPT3 "bgp_val:$bgp_val\tbha_bgp_val:$bha_bgp_val\n";
			$$request[11] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list&view_bgp_ip=$$request[8]\" target=\"new_win\">View</a>";
		}
                ##++Addendum 2 BHa Requirment.
		if ($$request[17]){
			$$request[14] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list_bha&view_bgp_ip=$$request[17]\" target=\"new_win\">View</a>";

                        $$request[16] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication
_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list_bha_backup&view_bgp_ip=$$request[17]\"
 target=\"new_win\">View</a>";

		}
	}
       ##-- Addendum 2 BHA Requirment.
       ##++ Modified for Addendum 2 BHA Requirment.
	$CMS::html_values{services}{table} = $res_gia;
	unshift (@{$CMS::html_values{services}{table}},
	["Service", "Customer", "Description", "Link Bandwidth", "Product Code", "Router", "Interface", "AS No.", "BGP Neighbor IP", "AutoPrefix List", "IRRD Object", "Prefix List","Black Hole Routing", "BHA Router Primary","BHA Prefix List Primary","BHA Router Backup","BHA Prefix List Backup", "BHA BGP Neighbor IP", "BHA AutoPrefix List","Aggregate Service", "Usage Based Billing", "Billing Start", "Billing End"]);
       ##-- Modified for Addendum 2 BHA Requirment.
   }  else {
#++EVPL - Passing parameters if product code is empty
	#When no filter condition is applied OR if any product is selected
     if( ($CMS::html_values{search_productcode}{value} eq "") && ($CMS::html_values{search_as}{value} eq "") && ($CMS::html_values{search_irrd}{value} eq "") && ($CMS::html_values{search_aggregate}{value} eq "") && ($CMS::html_values{search_master}{value} eq "") && ($CMS::html_values{search_serviceid_alt}{value} eq "") && ($CMS::html_values{search_aend_jobid}{value} eq "") && ($CMS::html_values{search_zend_jobid}{value} eq "") && ($CMS::html_values{search_aend_jobno}{value} eq "") && ($CMS::html_values{search_zend_jobno}{value} eq "") ) {

	#When no filter condition is applied
     $res = CMS_SERVICE_DB::search_service_evpl
          ($CMS::html_values{search_serviceid}{value},
         $CMS::html_values{search_accno}{value},
         $CMS::html_values{search_description}{value},
         $CMS::html_values{search_bandwidth}{value},
         $CMS::html_values{search_productcode}{value},
         $CMS::html_values{search_portmode_e}{value},
         $CMS::html_values{search_costype_e}{value},
         $CMS::html_values{search_switch_evpl_a}{value},
         $CMS::html_values{search_switch_evpl_z}{value},
         $CMS::html_values{search_switch_service_tpm}{value}
        );
     }
#--EVPL

	#When no filter condition is applied OR if any product is selected
	#modified by Karuna for EPL
	$res_a = CMS_SERVICE_DB::search_service
	  ($CMS::html_values{search_serviceid}{value},
	 $CMS::html_values{search_accno}{value},
	 $CMS::html_values{search_description}{value},
	 $CMS::html_values{search_bandwidth}{value},
	 $CMS::html_values{search_productcode}{value},
	 $CMS::html_values{search_portmode}{value},
	 $CMS::html_values{search_as}{value},
         $CMS::html_values{search_irrd}{value},
	 $CMS::html_values{search_aggregate}{value},
         $CMS::html_values{search_master}{value},
         $CMS::html_values{search_serviceid_alt}{value},
	 $CMS::html_values{search_aend_jobid}{value},
	 $CMS::html_values{search_zend_jobid}{value},
	 $CMS::html_values{search_aend_jobno}{value},
	 $CMS::html_values{search_zend_jobno}{value},
         $CMS::html_values{epl_master}{value},
         $CMS::html_values{epl_aend_jobid}{value},
         $CMS::html_values{epl_zend_jobid}{value},
         $CMS::html_values{epl_aend_jobno}{value},
         $CMS::html_values{epl_zend_jobno}{value}
        );

	$result_gen  = $res_a;
	open (IPT4, ">/data1/tmp_log/prefix.log");

	my $productcode_v;
        my $Flag_v=0;
        my $Flag_o=0;
	foreach my $request (@{$res_a}) {
		if($$request[4]){ 
			$productcode_v = $$request[4]; 
             		if($productcode_v =~ /VPLS/){
			   	$Flag_v = 1;
			} elsif ($productcode_v =~ /ETHERNET/) {
                          	# Added by Stavan for VLA on 19Aug_2010.For ETHERNET hide the Polling type field from frontend
                           	$$request[14]="";
	     		}else{
	        	   	$Flag_o = 1;
	     		}
		}

		if($productcode_v !~ /VPLS/){
		print IPT4 "request 7:$$request[7]\n";
		print IPT4 "request 8:$$request[8]\n";
       	  		if ($$request[8]) {
            		   $$request[11] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list&view_bgp_ip=$$request[8]\" target=\"new_win\">View</a>";
          		}
			if ($$request[48]){
				$$request[50] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list_bha&view_bgp_ip=$$request[48]\" target=\"new_win\">View</a>";
                }

        	}
       	}

	if($CMS::html_values{search_productcode}{value} eq "") {

        if($res) {
		push(@$res_a,@$res);
	}

	$res = $res_a;

#++EVPL - Getting 'a' or 'z' from an EVPL serviceID and replacing it with 'A' or 'Z' respectively for the column End in Search service screen
        my $count= $#$res;
for my $i (0 .. $#{$res}) {
        my $serveid = $$res[$i][0];
        if($serveid =~ /.*\[.*\].*ec./ || $serveid =~ /.*\[.*\].*ec.*sw$/){
                if($serveid=~  /.*\[.*\].*ec.*asw$/ || $serveid=~ /.*\[.*\].*ec.*a$/ ){
                        my $temp = $$res[$i];
                        splice(@$temp,1,0,'A');
                        $$res[$i]=$temp;
                }elsif($serveid=~  /.*\[.*\].*ec.*zsw$/ || $serveid=~ /.*\[.*\].*ec.*z$/){
                        my $temp = $$res[$i];
                        splice(@$temp,1,0,'Z');
                        $$res[$i]=$temp;
                }

                $$res[$i][0] =~  s/asw/sw/;
                $$res[$i][0] =~  s/zsw/sw/;
                if($$res[$i][0]=~/a$/ || $$res[$i][0]=~/z$/){
                $$res[$i][0]=~ s/a$//;
                $$res[$i][0] =~ s/z$//;
                }

                if($$res[$i][0]=~/swa$/ || $$res[$i][0]=~/swz$/){
                $$res[$i][0]=~ s/swa/sw/;
                $$res[$i][0]=~ s/swz/sw/;
                }
                }else{
                my $temp = $$res[$i];
                splice(@$temp,1,0,'');
                $$res[$i]=$temp;
                }
        }
}
#--EVPL
	if($CMS::html_values{search_productcode}{value} =~ /VPLS/){
	$CMS::html_values{services}{table} = $res_a;
      	unshift (@{$CMS::html_values{services}{table}},

["Service", "Customer", "Description", "Bandwidth", "Product Code", "Router", "Interface", "Aggregate Service", "Master Service","Port Mode","Voice[IN]", "Voice[OUT]", "Video[IN]", "Video[OUT]", "Critical[IN]", "Critical[OUT]", "Interactive Data[IN]", "Interactive Data[OUT]", "Standard[IN]", "Standard[OUT]", "Low Priority Data[IN]", "Low Priority Data[OUT]", "Usage Based Billing", "Billing Start", "Billing End"]);

	} elsif($Flag_v == 1 && $Flag_o == 1) {
	print IPT4 "PM SEARCH Flag_v 1 Flag_o 1 \n";
	print IPT4 "PM SEARCH WHEN NO FILTER CONDITION IS APPLIED\n";

	##09-Nov-2010 IP Transit Ph5 - Karuna
        ##++ Added for Addendum 2 BHA Requirment.
	foreach my $request (@{$res}) {
	my $bha_bgp_val = $$request[52];
                if ($$request[52]){
		print IPT4 "PM SEARCH bha_bgp_val:$bha_bgp_val\n";
			$$request[49] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list_bha&view_bgp_ip=$$request[52]\" target=\"new_win\">View</a>";

                        $$request[51] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list_bha_backup&view_bgp_ip= $$request[52]\" target=\"new_win\">View</a>";




		}
	}
        ##--Addendum 2 BHA  Requirment.
	#Added MCAST headers by Nisha for mVPN: 06oct2009
	#When no filter condition is applied
	$CMS::html_values{services}{table} = $res;
        ## ++Modified for Addendum 2 BHA Requirment.
      	unshift (@{$CMS::html_values{services}{table}},
              ["Service", "END", "Customer", "Description", "Bandwidth", "Product Code", "Router", "Interface", "AS No.", "BGP Neighbor IP", "AutoPrefix List", "IRRD Object", "Prefix List", "Aggregate Service", "Master Service","IPVPN Polling Type / Port Mode", "Voice/Gold [IN] RateACL No. / ClassMap Name", "Voice/Gold [OUT] ClassMap Name", "Video/Silver Plus [IN] RateACL No. / ClassMap Name", "Video/Silver Plus [OUT] ClassMap Name", "Critical/Silver [IN] RateACL No. / ClassMap Name", "Critical/Silver [OUT] ClassMap Name", "Interactive Data (Telstra only) [IN] RateACL No. / ClassMap Name", "Interactive Data (Telstra only) [OUT] ClassMap Name", "Standard (Telstra only) [IN] RateACL No. / ClassMap Name", "Standard (Telstra only) [OUT] ClassMap Name", "Low Priority Data/Bronze [IN] RateACL No. / ClassMap Name", "Low Priority Data/Bronze [OUT] ClassMap Name","Usage Based Billing", "Billing Start", "Billing End", "ATM/FR Service ID", "GBS A-End Job ID", "GBS Z-End Job ID", "GBS A-End Job Note", "GBS Z-EdJob Note", "MCAST Voice [IN] ClassMap Name", "MCAST Video [IN] ClassMap Name", "MCAST Video [OUT] ClassMap Name", "MCAST Critical [IN] ClassMap Name", "MCAST Critical [OUT] ClassMap Name", "MCAST Interactive [IN] ClassMap Name", "MCAST Interactive [OUT] ClassMap Name", "MCAST Standard [IN] ClassMap Name", "MCAST Standard [OUT] ClassMap Name", "MCAST Low Priority [IN] ClassMap Name", "MCAST Low Priority [OUT] ClassMap Name", "Black Hole Routing", "BHA Router Primary","BHA Prefix List Primary","BHA Router Backup","BHA Prefix List Backup", "BHA BGP Neighbor IP", "BHA AutoPrefix List" ]);
	##--IPT5
        ##--Addendum 2 BHA Requirment.
	} elsif($Flag_v == 1) {

        $CMS::html_values{services}{table} = $res_a;
   	unshift (@{$CMS::html_values{services}{table}},
             ["Service", "END", "Customer", "Description", "Bandwidth", "Product Code", "Router", "Interface", "Aggregate Service", "Master Service","Port Mode","Voice[IN]", "Voice[OUT]", "Video[IN]", "Video[OUT]", "Critical[IN]", "Critical[OUT]", "Interactive Data[IN]", "Interactive Data[OUT]", "Standard[IN]", "Standard[OUT]", "Low Priority Data[IN]", "Low Priority Data[OUT]", "Usage Based Billing", "Billing Start", "Billing End"]);
	
	} else {
		print IPT4 "PM SEARCH ELSE of Flag_v 1 \n";

## ++Addendum 2 BHA Requirment.
foreach my $request (@{$res_a}) {
            my $bha_bgp_val = $$request[52];
                   if ($$request[52]){
                    print IPT4 "PM SEARCH bha_bgp_val:$bha_bgp_val\n";
                            $$request[49] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication
_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list_bha&view_bgp_ip=$$request[52]\"
 target=\"new_win\">View</a>";
   
                            $$request[51] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication
_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list_bha_backup&view_bgp_ip= $$request[52]\" target=\"new_win\">View</a>";

   
   
   
                    }
          }
  
##--Addendum 2 BHA Requirment.







		#Added MCAST headers by Nisha for mVPN: 06oct2009
		#Any product selected other than VPLS and EVPL
        	$CMS::html_values{services}{table} = $res_a;
		#Added by Nisha for mVPN:07oct2009
		if($CMS::html_values{search_productcode}{value} eq ""){
			print IPT4 "PM SEARCH prod code null\n";
			#Include the MCAST headers only if the product is not selected
                        ##++Modified for Addendum 2 BHA Requirment.
        		unshift (@{$CMS::html_values{services}{table}},
              		["Service", "END", "Customer", "Description", "Bandwidth", "Product Code", "Router", "Interface", "AS No.", "BGP Neighbor IP", "AutoPrefix List", "IRRD Object", "Prefix List", "Aggregate Service", "Master Service","IPVPN Polling Type / Port Mode", "Voice/Gold [IN] RateACL No. / ClassMap Name", "Voice/Gold [OUT] ClassMap Name", "Video/Silver Plus [IN] RateACL No. / ClassMap Name", "Video/Silver Plus [OUT] ClassMap Name", "Critical/Silver [IN] RateACL No. / ClassMap Name", "Critical/Silver [OUT] ClassMap Name", "Interactive Data (Telstra only) [IN] RateACL No. / ClassMap Name", "Interactive Data (Telstra only)[OUT] ClassMap Name", "Standard (Telstra only) [IN] RateACL No. / ClassMap Name", "Standard (Telstra only) [OUT] ClassMap Name", "Low Priority Data/Bronze [IN] RateACL No. / ClassMap Name", "Low Priority Data/Bronze [OUT] ClassMap Name","Usage Based Billing", "Billing Start", "Billing End", "ATM/FR Service ID", "GBS A-End Job ID", "GBS Z-End Job ID", "GBS A-End Job Note", "GBS Z-EdJob Note", "MCAST Voice [IN] ClassMap Name", "MCAST Video [IN] ClassMap Name", "MCAST Video [OUT] ClassMap Name", "MCAST Critical [IN] ClassMap Name", "MCAST Critical [OUT] ClassMap Name", "MCAST Interactive [IN] ClassMap Name", "MCAST Interactive [OUT] ClassMap Name", "MCAST Standard [IN] ClassMap Name", "MCAST Standard [OUT] ClassMap Name", "MCAST Low Priority [IN] ClassMap Name", "MCAST Low Priority [OUT] ClassMap Name", "Black Hole Routing", "BHA Router Primary","BHA Prefix List Primary","BHA Router Backup","BHA Prefix List Backup", "BHA BGP Neighbor IP", "BHA Auto Prefix List"]);
			} else {

			print IPT4 "PM SEARCH ELSE of null prod code \n";
			#Remove the MCAST headers in case any product other than MPLS, EVPL and VPLS is selected
			#End of code Added by Nisha for mVPN:07oct2009
      			unshift (@{$CMS::html_values{services}{table}},
              		["Service","Customer", "Description", "Bandwidth", "Product Code", "Router", "Interface", "AS No.", "BGP Neighbor IP", "AutoPrefix List", "IRRD Object", "Prefix List", "Aggregate Service", "Master Service","IPVPN Polling Type / Port Mode", "Voice/Gold [IN] RateACL No. / ClassMap Name", "Voice/Gold [OUT] ClassMap Name", "Video/Silver Plus [IN] RateACL No. / ClassMap Name", "Video/Silver Plus [OUT] ClassMap Name", "Critical/Silver [IN] RateACL No. / ClassMap Name", "Critical/Silver [OUT] ClassMap Name", "Interactive Data (Telstra only) [IN] RateACL No. / ClassMap Name", "Interactive Data (Telstra only)[OUT] ClassMap Name", "Standard (Telstra only) [IN] RateACL No. / ClassMap Name", "Standard (Telstra only) [OUT] ClassMap Name", "Low Priority Data/Bronze [IN] RateACL No. / ClassMap Name", "Low Priority Data/Bronze [OUT] ClassMap Name","Usage Based Billing", "Billing Start", "Billing End", "ATM/FR Service ID", "GBS A-End Job ID", "GBS Z-End Job ID", "GBS A-End Job Note", "GBS Z-EdJob Note"]);
              		#["Service","Customer", "Description", "Bandwidth", "Product Code", "Router", "Interface", "AS No.", "BGP Neighbor IP", "AutoPrefix List", "IRRD Object", "Prefix List", "Aggregate Service", "Black Hole Routing","BHA Router","BHA BGP Neighbor IP","BHA Auto Prefix List", "BHA Prefix List", "Usage Based Billing", "Billing Start", "Billing End"]);
		}
        }
}
      $CMS::html_values{services}{header_rows} = 1;


      $CMS::html_values{search_productcode}{options} = CMS_SERVICE_DB::get_productcode();
      $CMS::html_values{search_portmode}{options} = CMS_SERVICE_DB::get_portmode();
      $CMS::html_values{search_master}{options} = CMS_SERVICE_DB::get_master_service();
      $CMS::html_values{search_aggregate}{options} = CMS_SERVICE_DB::get_aggregate_service();
      #++EVPL
      $CMS::html_values{search_portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
      $CMS::html_values{search_costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
      $CMS::html_values{aggregate_service_a}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
      $CMS::html_values{aggregate_service_z}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
      $CMS::html_values{aggregate_service_tpm}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_tpm();


      unshift(@{$CMS::html_values{search_productcode}{options}}, [""]);
      unshift(@{$CMS::html_values{search_portmode}{options}}, [""]);
      unshift(@{$CMS::html_values{search_portmode_e}{options}}, [""]);
      unshift(@{$CMS::html_values{search_costype_e}{options}}, [""]);
      #$CMS::html_values{search_productcode}{value} = "";
      #$CMS::html_values{search_portmode_e}{value} = "";
#--EVPL
} else {
      $CMS::html_values{search_serviceid}{value} = "";
      $CMS::html_values{search_accno}{value} = "";
      $CMS::html_values{search_description}{value} = "";
      $CMS::html_values{search_bandwidth}{value} = "";
      $CMS::html_values{search_productcode}{options} = CMS_SERVICE_DB::get_productcode();
      $CMS::html_values{search_portmode}{options} = CMS_SERVICE_DB::get_portmode();
      $CMS::html_values{search_master}{options} = CMS_SERVICE_DB::get_master_service();
      $CMS::html_values{search_aggregate}{options} = CMS_SERVICE_DB::get_aggregate_service();
      $CMS::html_values{search_portmode_e}{options} = CMS_SERVICE_DB::get_portmode_evpl();
      $CMS::html_values{search_costype_e}{options} = CMS_SERVICE_DB::get_costype_evpl();
      $CMS::html_values{search_switch_evpl_a}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
      $CMS::html_values{search_switch_evpl_z}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_vpm();
      $CMS::html_values{search_switch_service_tpm}{options} = CMS_SERVICE_DB::get_aggregate_service_evpl_tpm();

     
      unshift(@{$CMS::html_values{search_master}{options}}, [""]);
      unshift(@{$CMS::html_values{search_aggregate}{options}}, [""]);
      unshift(@{$CMS::html_values{search_productcode}{options}}, [""]);
      unshift(@{$cMS::html_values{search_portmode}{options}}, [""]);
      unshift(@{$CMS::html_values{search_portmode_e}{options}}, [""]);
      unshift(@{$CMS::html_values{search_costype_e}{options}}, [""]);
      $CMS::html_values{search_productcode}{value} = "";
      $CMS::html_values{search_portmode}{value} = "";
      $CMS::html_values{search_portmode_e}{value} = "";
      $CMS::html_values{search_costype_e}{value} = "";
      $CMS::html_values{search_as}{value} = "";
      $CMS::html_values{search_master}{value} = "";
      $CMS::html_values{search_aggregate}{value} = "";
    }
    CMS::output_html("service_search");
  }
#++EVPL
elsif ($CMS::html_values{command}{value} eq "fetchcust"){
open (EPL2,">/data1/tmp_log/editservice.log");
#print EPL2 "entered fetchcust\n";
$CMS::html_values{servid}{value} =~ tr/A-Z/a-z/;
my $serv = $CMS::html_values{servid}{value};

#modified by Karuna to make it specifically for EVPL
my $evpl_serv = lc($serv);
$evpl_serv =~ s/\s+//g;
$evpl_serv = substr ($evpl_serv, 13, 2);

#for EVPL services append a to it.
#print EPL2 "service in fetchcust: $serv\n";
#print EPL2 "prod code: $CMS::html_values{productcode}{value}\n";
#if(( $serv =~ /\[/)&& ($CMS::html_values{productcode}{value} =~ "EVPL"))
if(($serv =~ /\[/) && ($evpl_serv !~ /^(em|ew)$/)){
#print EPL2 "entered if loop in fetcust\n";
$serv.="a";
}
my $accno = CMS_SERVICE_DB::get_accno_for_serviceid($serv);

print "Content-type: html/text\n\n";
print $accno;
}
#--EVPL
elsif ($CMS::html_values{command}{value} eq "fetchas") {
	#Added by Nisha on 29Sep10 for IP Transit to fetch the AS Number for the selected Master service
	my $master_service = $CMS::html_values{msid}{value};
	my $asno = CMS_SERVICE_DB::get_asno_ipt($master_service);
	print "Content-type: html/text\n\n";
	print $asno;
} elsif ($CMS::html_values{command}{value} eq "fetchacc") {
        #Added by Karuna on 12Oct2010 for for IP Transit ph3 to fetch the account no, for the selected Master service
        my $master_service = $CMS::html_values{msid}{value};
        my $retStr = CMS_SERVICE_DB::get_accno_ipt($master_service);
	open (IPTACC, ">>/data1/tmp_log/getacc.log");
	print IPTACC "PM retStr:$retStr\n";
        print "Content-type: html/text\n\n";
        print $retStr;
} elsif ($CMS::html_values{command}{value} eq "Check_ASN") {
	#Added by Karuna on 02-Dec-2010 for IPT 5
	##++IPT5
	my $serv_id = $CMS::html_values{servid}{value};
	if ($debug){print IPT_DBG "PM serv_id:$serv_id\n";}
	my $retStr = CMS_SERVICE_DB::get_last_asn($serv_id);
	if ($debug){print IPT_DBG "PM retStr:$retStr\n";}
	print "Content-type: html/text\n\n";
	print $retStr;
	##--IPT5
}
## IP Transit Addendum 2 ##
elsif ($CMS::html_values{command}{value} eq "search_asnum") {
	if (not defined $CMS::html_values{submit}{value}){
		#CMS:add_error("Please enter the AS Number in numeric format");
		CMS::add_error("Please enter the AS Number in numeric format");
		CMS::output_html("service_search_asnum");
	}
	if ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} =~ /clear/i)) {
		# Clear button clicked
		CMS::clear_html_values();
		CMS::add_error("Please enter the AS Number in numeric format");
		$CMS::html_values{subsystem}{value} = "service";
		$CMS::html_values{command}{value} = "search_asnum";		
		CMS::output_html("service_search_asnum");
	} 

	if ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} =~ /search/i)) {
	#Search button clicked
	if ($debug){print IPT_DBG "PM search_asnum:search\n"};
	if ((($CMS::html_values{search_asnum}{value} < 0) || ($CMS::html_values{search_asnum}{value} =~ m/[a-z]|[A-Z]/) || ($CMS::html_values{search_asnum}{value} !~ m/[0-9]/)) && ($CMS::html_values{search_asnum}{value} ne "")) {
		#ASN validation - return nothing for alphanumeric input
		$CMS::html_values{search_asn}{header_rows} = 1;
		unshift (@{$CMS::html_values{search_asn}{table}},["Service", "Customer", "Link Bandwidth", "Product Code", "Router", "Interface", "AS No.", "Master Service ID", "IPC Service ID", "Committed In/Out", "Direct/Indirect"]);
		CMS::output_html("service_search_asnum");

	} else {
		#Valid ASN format, query DB
		my $asnum_res = CMS_SERVICE_DB::search_asnumber($CMS::html_values{search_asnum}{value});
		if ($debug){print IPT_DBG "PM search_asnum:$CMS::html_values{search_asnum}{value}\n"};
		$CMS::html_values{search_asn}{table} = $asnum_res;
		if ($debug){print IPT_DBG "PM inside search_asnum\n"};
		#Header row
		unshift (@{$CMS::html_values{search_asn}{table}},["Service", "Customer", "Link Bandwidth", "Product Code", "Router", "Interface", "AS No.", "Master Service ID", "IPC Service ID", "Committed In/Out", "Direct/Indirect"]);
		$CMS::html_values{search_asn}{header_rows} = 1;
		CMS::output_html("service_search_asnum");
	}
	}
}	

}
  
1;
