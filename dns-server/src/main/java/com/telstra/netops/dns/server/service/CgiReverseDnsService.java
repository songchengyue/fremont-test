package com.telstra.netops.dns.server.service;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.dns.server.exception.DnsApplicationBadResolverIpBusinessRuleException;
import com.telstra.netops.dns.server.exception.DnsApplicationGenericBusinessRuleException;
import com.telstra.netops.dns.server.model.ClientReverseDnsRequest;
import com.telstra.netops.dns.server.model.DnsReverseRecord;
import com.telstra.netops.dns.server.model.Success;
import com.telstra.netops.dns.server.model.cgi.CgiDnsRequest;
import com.telstra.netops.dns.server.model.cgi.DnsResultWrapper;
import com.telstra.netops.dns.server.model.cgi.DnsReverseRecordsResultWrapper;
import com.telstra.netops.dns.server.util.JsonSerializerDeserializer;
import com.telstra.oauth2.client.OAuth2TokenResolver;

@Service
@Profile("!local")
public class CgiReverseDnsService implements ReverseDnsService {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CgiResolverService.class);

	@Value("${dns.cgi.url}")
	private String cgiURL;
	
	@Autowired
	private JsonSerializerDeserializer jsonSerializerDeserializer;
	
	@Autowired
    private OAuth2TokenResolver tokenResolver;
	
	@Override
	public Success addDelegation(
			ClientReverseDnsRequest request) {
		
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createReverseDelegationDnsAdd(request);
		
		CgiDnsRequester<DnsResultWrapper> requester = new CgiDnsRequester<DnsResultWrapper>(DnsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isError()) {
			throw new DnsApplicationGenericBusinessRuleException(wrapper.getErrorMessage());
		}
		
		LOGGER.debug("in viewReverseDNS "+wrapper.toString());
		return new Success(wrapper.getSuccessMessage());
	}

	@Override
	public Success removeDelegation(
			ClientReverseDnsRequest request) {
		
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createReverseDelegationDnsRemove(request);

		CgiDnsRequester<DnsResultWrapper> requester = new CgiDnsRequester<DnsResultWrapper>(DnsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isError()) {
			throw new DnsApplicationGenericBusinessRuleException(wrapper.getErrorMessage());
		}
		
		LOGGER.debug("in viewReverseDNS "+wrapper.toString());
		return new Success(wrapper.getSuccessMessage());
	}

	@Override
	public List<DnsReverseRecord> list(String customerId) {
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.reverseDnsList(customerId);
		
		CgiDnsRequester<DnsReverseRecordsResultWrapper> requester = new CgiDnsRequester<DnsReverseRecordsResultWrapper>(DnsReverseRecordsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsReverseRecordsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isEmpty()) {
			return Collections.<DnsReverseRecord>emptyList();
		}
		
		LOGGER.debug("in viewReverseDNS "+wrapper.toString());
		return wrapper.getResult();
	}

	@Override
	public DnsResultWrapper addMapping(ClientReverseDnsRequest request) {
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createReverseMappingDnsAdd(request);

		CgiDnsRequester<DnsResultWrapper> requester = new CgiDnsRequester<DnsResultWrapper>(DnsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isError()) {
			throw new DnsApplicationBadResolverIpBusinessRuleException(wrapper.getErrorMessage());
		}
		
		LOGGER.debug("in removeMapping "+wrapper.toString());
		return wrapper;
	}

	@Override
	public DnsResultWrapper removeMapping(ClientReverseDnsRequest request) {
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createReverseMappingDnsRemove(request);

		CgiDnsRequester<DnsResultWrapper> requester = new CgiDnsRequester<DnsResultWrapper>(DnsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isError()) {
			throw new DnsApplicationBadResolverIpBusinessRuleException(wrapper.getErrorMessage());
		}
		
		LOGGER.debug("in removeMapping "+wrapper.toString());
		return wrapper;
	}
	
}