/*
 * An XML document type.
 * Localname: getImpactPathResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * A document containing one getImpactPathResponse(@http://schemas.hp.com/ucmdb/1/params/impact) element.
 *
 * This is a complex type.
 */
public class GetImpactPathResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponseDocument
{
    
    public GetImpactPathResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETIMPACTPATHRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactPathResponse");
    
    
    /**
     * Gets the "getImpactPathResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse getGetImpactPathResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse)get_store().find_element_user(GETIMPACTPATHRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getImpactPathResponse" element
     */
    public void setGetImpactPathResponse(com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse getImpactPathResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse)get_store().find_element_user(GETIMPACTPATHRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse)get_store().add_element_user(GETIMPACTPATHRESPONSE$0);
            }
            target.set(getImpactPathResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getImpactPathResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse addNewGetImpactPathResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse)get_store().add_element_user(GETIMPACTPATHRESPONSE$0);
            return target;
        }
    }
}
