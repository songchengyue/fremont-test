/*
 * XML Type:  getCIsById
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetCIsById
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML getCIsById(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class GetCIsByIdImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetCIsById
{
    
    public GetCIsByIdImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "cmdbContext");
    private static final javax.xml.namespace.QName CISTYPEDPROPERTIES$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "CIsTypedProperties");
    private static final javax.xml.namespace.QName IDS$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "IDs");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets the "CIsTypedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection getCIsTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().find_element_user(CISTYPEDPROPERTIES$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "CIsTypedProperties" element
     */
    public boolean isSetCIsTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CISTYPEDPROPERTIES$2) != 0;
        }
    }
    
    /**
     * Sets the "CIsTypedProperties" element
     */
    public void setCIsTypedProperties(com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection cIsTypedProperties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().find_element_user(CISTYPEDPROPERTIES$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().add_element_user(CISTYPEDPROPERTIES$2);
            }
            target.set(cIsTypedProperties);
        }
    }
    
    /**
     * Appends and returns a new empty "CIsTypedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection addNewCIsTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().add_element_user(CISTYPEDPROPERTIES$2);
            return target;
        }
    }
    
    /**
     * Unsets the "CIsTypedProperties" element
     */
    public void unsetCIsTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CISTYPEDPROPERTIES$2, 0);
        }
    }
    
    /**
     * Gets the "IDs" element
     */
    public com.hp.schemas.ucmdb._1.types.IDs getIDs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().find_element_user(IDS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "IDs" element
     */
    public void setIDs(com.hp.schemas.ucmdb._1.types.IDs iDs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().find_element_user(IDS$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().add_element_user(IDS$4);
            }
            target.set(iDs);
        }
    }
    
    /**
     * Appends and returns a new empty "IDs" element
     */
    public com.hp.schemas.ucmdb._1.types.IDs addNewIDs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().add_element_user(IDS$4);
            return target;
        }
    }
}
