/*
 * An XML document type.
 * Localname: ucmdbFault
 * Namespace: http://schemas.hp.com/ucmdb/1/params/ucmdb
 * Java type: com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.ucmdb.impl;
/**
 * A document containing one ucmdbFault(@http://schemas.hp.com/ucmdb/1/params/ucmdb) element.
 *
 * This is a complex type.
 */
public class UcmdbFaultDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument
{
    
    public UcmdbFaultDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName UCMDBFAULT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/ucmdb", "ucmdbFault");
    
    
    /**
     * Gets the "ucmdbFault" element
     */
    public com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault getUcmdbFault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault target = null;
            target = (com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault)get_store().find_element_user(UCMDBFAULT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ucmdbFault" element
     */
    public void setUcmdbFault(com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault ucmdbFault)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault target = null;
            target = (com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault)get_store().find_element_user(UCMDBFAULT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault)get_store().add_element_user(UCMDBFAULT$0);
            }
            target.set(ucmdbFault);
        }
    }
    
    /**
     * Appends and returns a new empty "ucmdbFault" element
     */
    public com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault addNewUcmdbFault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault target = null;
            target = (com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault)get_store().add_element_user(UCMDBFAULT$0);
            return target;
        }
    }
    /**
     * An XML ucmdbFault(@http://schemas.hp.com/ucmdb/1/params/ucmdb).
     *
     * This is a complex type.
     */
    public static class UcmdbFaultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument.UcmdbFault
    {
        
        public UcmdbFaultImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName MSG$0 = 
            new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/ucmdb", "msg");
        private static final javax.xml.namespace.QName STACKTRACE$2 = 
            new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/ucmdb", "stacktrace");
        
        
        /**
         * Gets the "msg" element
         */
        public java.lang.String getMsg()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MSG$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "msg" element
         */
        public org.apache.xmlbeans.XmlString xgetMsg()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MSG$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "msg" element
         */
        public void setMsg(java.lang.String msg)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MSG$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MSG$0);
                }
                target.setStringValue(msg);
            }
        }
        
        /**
         * Sets (as xml) the "msg" element
         */
        public void xsetMsg(org.apache.xmlbeans.XmlString msg)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MSG$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MSG$0);
                }
                target.set(msg);
            }
        }
        
        /**
         * Gets the "stacktrace" element
         */
        public java.lang.String getStacktrace()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STACKTRACE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "stacktrace" element
         */
        public org.apache.xmlbeans.XmlString xgetStacktrace()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(STACKTRACE$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "stacktrace" element
         */
        public void setStacktrace(java.lang.String stacktrace)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STACKTRACE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STACKTRACE$2);
                }
                target.setStringValue(stacktrace);
            }
        }
        
        /**
         * Sets (as xml) the "stacktrace" element
         */
        public void xsetStacktrace(org.apache.xmlbeans.XmlString stacktrace)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(STACKTRACE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(STACKTRACE$2);
                }
                target.set(stacktrace);
            }
        }
    }
}
