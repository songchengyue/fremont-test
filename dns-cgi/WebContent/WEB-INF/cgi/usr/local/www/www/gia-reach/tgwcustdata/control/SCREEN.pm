# (C) Telstra 2001
# 
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 30 March 2001
# File: SCREEN.pm
#
# This file contains generic procedures to print HTML interfaces
# using templates.
#
# $Id: SCREEN.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $

package SCREEN;
use Exporter;
use strict;

our @ISA     = qw(Exporter);
our @EXPORT  = qw(print_screen get_key_value_pairs 
		  $error_filename $error_code $default_value 
		  SUCCESS ERROR CAN_NOT_FIND_FILE
		  ERROR_FILE_NOT_SET
		  NO_ENV_VAR FAILED_RESTRICTION NO_CONTENT);

# Error Codes
use constant SUCCESS => 1;
use constant ERROR => 0;

$::error_filename; # path to error control file - user defined
$::error_code;     # code of last error that occured in module
$::default_value;     # default_value used when key is missing 

# print out a screen based on the template $filename
# [[[VALUE]]] in the template are replaced by the corresponding
# value in the %names array referred to by $names_ref
use constant CAN_NOT_FIND_FILE => -11;
use constant UNSPECIFIED_KEY => -12;

sub print_screen {
  my $filename = shift @_; 
  my $names_ref = shift @_;
  $$names_ref{STATUS} = shift @_;

  my $status = SUCCESS;

  unless (open (CTRL, $filename)) { 
    $::error_code = CAN_NOT_FIND_FILE;
    return ERROR;
  }
  
  my $select_name = undef;
  
  my $multiline = undef;
  my $multiline_name = undef;
  while (my $line = <CTRL>){
    # choose whether to ignore line or not
    # if line begins with ###KEY###VALUE###, then it will only be
    # printed if that key/value pair has been passed.
    
    if ($line =~ /^\#\#\#(.+?)\#\#\#(.+?)\#\#\#(.*)/) {
      if ((defined($$names_ref{$1})) && ($2 eq $$names_ref{$1})) {
	$line = $3;
      } else {
	next; # skip the line
      }
    }

    

    # deal with repeated lines - marked by ***XX***
    if ($line =~ /\*\*\*START (.+?)\*\*\*/) {
      $multiline_name = $1;
      next;
    } elsif ($line =~/\*\*\*END.*?\*\*\*/) {
      unless (defined($multiline_name)) {
	next ;# ignore if not in multiline
      }
      # have got to end of multiline;

      my $row;
      my $cell;
      $line = "";
      
      foreach $row (@{$$names_ref{$multiline_name}}) {
	$line .= $multiline;
	my $i = 0;
	foreach $cell (@$row) {
	  $i++;
	  $line =~ s/\*$i\*/$cell/g;
	}
      }      
      $multiline = undef;
      $multiline_name = undef;

    } elsif ($multiline_name) {
      $multiline .= $line;
      next;
    }


    # swap [[[X]]] with corresponding value from %names 
    # referred to $names_ref 
    while ($line =~ /\[\[\[.+\]\]\]/){
      my $key = (split (/\[\[\[|\]\]\]/, $line))[1];
      my $value;
      
      if (defined($value = $$names_ref{$key})) {
	$value =~ s/\n/<BR>/g;
      } else {
	$status = ERROR;
	$::error_code = UNSPECIFIED_KEY;
	($value = $::default_value) || ($value = "");
      }
      $line =~ s/\[\[\[.+?\]\]\]/$value/; # ? ensures minimal match
    }


    # check correct radio button - assumes name is radio button
    if (($line =~ /<input.+name="(.+?)".+?>(.+)/i) &&      
	defined($$names_ref{$1}) && 
	($2 =~ $$names_ref{$1})) {
      $line =~ s/<input(.+?)>/<input$1 CHECKED>/;
    }

    # select correct option
    if ($line =~ /<SELECT NAME="(.+?)">/i){
      $select_name = $1;
    }
    
    if ($line =~ /<\/SELECT>/i) {
      $select_name = undef;
    }
    
    if (defined($select_name)) {
      # between <SELECT> AND </SELECT>
      if ((defined($$names_ref{$select_name})) && ($line =~ /<OPTION VALUE=\"$$names_ref{$select_name}\">/i)) {
	# line matches passed option
	$line =~ s/(<OPTION VALUE=\"$$names_ref{$select_name}\")/$1 SELECTED/i;
      } elsif (not defined($$names_ref{$select_name})) {
	# no chosen value so
	# select first as default
	if ($line !~ /SELECTED/) {
	  if ($line =~ s/<OPTION VALUE=(.+?)>/<OPTION VALUE=$1 SELECTED>/i) {
	    $select_name = undef;
	  }
	}
      }
      
    }       
    print $line;
    
  }
  
  close CTRL;
  return $status;
}

# prints out an error message, as long as error_filename is declared.
use constant ERROR_FILE_NOT_SET => -22;
sub print_error {
  unless (defined($::error_filename)){
    $::error_code = ERROR_FILE_NOT_SET;
    return ERROR;
  }

  return (print_screen $::error_filename, @_);
}


# parse returned names into hash array
use constant NO_ENV_VAR => -31;
use constant FAILED_RESTRICTION => -32;
use constant NO_CONTENT => -33;

sub get_key_value_pairs {

  my $names_ref = shift @_;
  my $restrict_method = shift @_;

  if (defined($restrict_method)) {

    # check if it has been called
    if(!$ENV{'REQUEST_METHOD'}) {
      $::error_code = NO_ENV_VAR;
      return ERROR;
    }
    
    # First check for type of method
    if(($ENV{'REQUEST_METHOD'} ne $restrict_method)){
      $::error_code = FAILED_RESTRICTION;	
      return ERROR;
    }
  }

  # get POST data
  my $buffer;
  if (defined($ENV{'CONTENT_LENGTH'})) { 
    read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
    $buffer .= "&$ENV{'QUERY_STRING'}";
  } else {
    $buffer.= $ENV{'QUERY_STRING'};
  }

  #split pairs by the & which divides the vars
  my @nuid = split(/&/, $buffer);
  foreach my $nuid (@nuid)
    {
      my ($name, $value) = split(/=/, $nuid);
      $value =~ tr/+/ /;
      $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
      $$names_ref{$name} = $value;
    }
  $::error_code = SUCCESS;
  return SUCCESS;
}

1;
