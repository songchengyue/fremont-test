#!/bin/sh
# Where to send the files
HOST='192.168.39.57'
user='netops'   
pass='NeTOp$321'


# Used to check the time for sending an email
MAILTIME=`date "+%H"`
MINMAILTIME=`date "+%M"`


# Used to wildcard the files for FTPing


#file transfer for india
SOURCE_DIR="/usr/local/www/wanchai/netops/dns/revdns/rev_zones/india"
DEST_DIR="/var/webzone/india"


# See how long it takes to execute
#echo "Subject: FTPing Zone files from `hostname`">>/usr/local/www/wanchai/cms/data/revdns/reverse_transer.log
echo "Subject: FTPing Zone files from `hostname`">>/usr/local/www/wanchai/netops/dns/revdns/reverse_transer.log

#echo "start session "`date`"" >> /usr/local/www/wanchai/cms/data/revdns/reverse_transer.log
echo "start session "`date`"">>/usr/local/www/wanchai/netops/dns/revdns/reverse_transer.log

cd $SOURCE_DIR
FILE=*.arpa*

#ftp -n $HOST<<END_SCRIPT
#quote USER $user
#quote PASS $pass
#cd $DEST_DIR
#epsv off
#prompt
#mput $FILE>>/usr/local/www/wanchai/netops/dns/revdns/reverse_transer.log
#pwd
#quit
#END_SCRIPT



sshpass -p $pass sftp $user@$HOST <<END
cd $DEST_DIR
mput $FILE
pwd
quit
END


`mv $FILE /usr/local/www/wanchai/netops/dns/revdns/reverse_transfer/`


#file transfer for indonesia
SOURCE_DIR="/usr/local/www/wanchai/netops/dns/revdns/rev_zones/indonesia"
DEST_DIR="/var/webzone/indonesia"


# See how long it takes to execute
#echo "Subject: FTPing Zone files from `hostname`">>/usr/local/www/wanchai/cms/data/revdns/reverse_transer.log
echo "Subject: FTPing Zone files from `hostname`">>/usr/local/www/wanchai/netops/dns/revdns/reverse_transer.log

#echo "start session "`date`"" >> /usr/local/www/wanchai/cms/data/revdns/reverse_transer.log
echo "start session "`date`"">>/usr/local/www/wanchai/netops/dns/revdns/reverse_transer.log

cd $SOURCE_DIR
FILE=*.arpa*

#ftp -n $HOST<<END_SCRIPT
#quote USER $user
#quote PASS $pass
#cd $DEST_DIR
#epsv off
#prompt
#mput $FILE>>/usr/local/www/wanchai/netops/dns/revdns/reverse_transer.log
#pwd
#quit
#END_SCRIPT



sshpass -p $pass sftp $user@$HOST <<END
cd $DEST_DIR
mput $FILE
pwd
quit
END


`mv $FILE /usr/local/www/wanchai/netops/dns/revdns/reverse_transfer/`

#file transfer for international
SOURCE_DIR="/usr/local/www/wanchai/netops/dns/revdns/rev_zones/international"
DEST_DIR="/var/webzone/international"


# See how long it takes to execute
#echo "Subject: FTPing Zone files from `hostname`">>/usr/local/www/wanchai/cms/data/revdns/reverse_transer.log
echo "Subject: FTPing Zone files from `hostname`">>/usr/local/www/wanchai/netops/dns/revdns/reverse_transer.log

#echo "start session "`date`"" >> /usr/local/www/wanchai/cms/data/revdns/reverse_transer.log
echo "start session "`date`"">>/usr/local/www/wanchai/netops/dns/revdns/reverse_transer.log

cd $SOURCE_DIR
FILE=*.arpa*

#ftp -n $HOST<<END_SCRIPT
#quote USER $user
#quote PASS $pass
#cd $DEST_DIR
#epsv off
#prompt
#mput $FILE>>/usr/local/www/wanchai/netops/dns/revdns/reverse_transer.log
#pwd
#quit
#END_SCRIPT



sshpass -p $pass sftp $user@$HOST <<END
cd $DEST_DIR
mput $FILE
pwd
quit
END


`mv $FILE /usr/local/www/wanchai/netops/dns/revdns/reverse_transfer/`

