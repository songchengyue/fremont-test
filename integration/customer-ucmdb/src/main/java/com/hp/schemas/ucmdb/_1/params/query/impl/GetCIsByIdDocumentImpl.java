/*
 * An XML document type.
 * Localname: getCIsById
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetCIsByIdDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getCIsById(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetCIsByIdDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetCIsByIdDocument
{
    
    public GetCIsByIdDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCISBYID$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getCIsById");
    
    
    /**
     * Gets the "getCIsById" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCIsById getGetCIsById()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsById target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsById)get_store().find_element_user(GETCISBYID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCIsById" element
     */
    public void setGetCIsById(com.hp.schemas.ucmdb._1.params.query.GetCIsById getCIsById)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsById target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsById)get_store().find_element_user(GETCISBYID$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetCIsById)get_store().add_element_user(GETCISBYID$0);
            }
            target.set(getCIsById);
        }
    }
    
    /**
     * Appends and returns a new empty "getCIsById" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCIsById addNewGetCIsById()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsById target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsById)get_store().add_element_user(GETCISBYID$0);
            return target;
        }
    }
}
