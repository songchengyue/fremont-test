package com.telstra.netops.routemanagement.server.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telstra.netops.routemanagement.server.model.Deleted_Links_Routing;
import com.telstra.netops.routemanagement.server.model.Router_Software;

/**
 * Repository class for Deleted_Links_Routing listing
 *
 */
public interface DeletedLinksRoutingRepository extends CrudRepository<Deleted_Links_Routing, Long> {

	@Query("SELECT r FROM Deleted_Links_Routing r WHERE r.serviceId = :serviceId")
	List<Deleted_Links_Routing> findAllByServiceid(@Param("serviceId") String serviceId);

	@Query("SELECT r FROM Deleted_Links_Routing r WHERE r.serviceId = :serviceId")
	Deleted_Links_Routing findOneByServiceid(@Param("serviceId") String serviceId);

}
