/*
 * XML Type:  ClassModelQueryGetClassModelIconPathsRequest
 * Namespace: http://schemas.hp.com/ucmdb/ui/1/params
 * Java type: com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.ui._1.params;


/**
 * An XML ClassModelQueryGetClassModelIconPathsRequest(@http://schemas.hp.com/ucmdb/ui/1/params).
 *
 * This is a complex type.
 */
public interface ClassModelQueryGetClassModelIconPathsRequest extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ClassModelQueryGetClassModelIconPathsRequest.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("classmodelquerygetclassmodeliconpathsrequestb97atype");
    
    /**
     * Gets the "cmdbContext" element
     */
    com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext();
    
    /**
     * Sets the "cmdbContext" element
     */
    void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext);
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext();
    
    /**
     * Gets the "className" element
     */
    java.lang.String getClassName();
    
    /**
     * Gets (as xml) the "className" element
     */
    org.apache.xmlbeans.XmlString xgetClassName();
    
    /**
     * True if has "className" element
     */
    boolean isSetClassName();
    
    /**
     * Sets the "className" element
     */
    void setClassName(java.lang.String className);
    
    /**
     * Sets (as xml) the "className" element
     */
    void xsetClassName(org.apache.xmlbeans.XmlString className);
    
    /**
     * Unsets the "className" element
     */
    void unsetClassName();
    
    /**
     * Gets the "large" element
     */
    boolean getLarge();
    
    /**
     * Gets (as xml) the "large" element
     */
    org.apache.xmlbeans.XmlBoolean xgetLarge();
    
    /**
     * Sets the "large" element
     */
    void setLarge(boolean large);
    
    /**
     * Sets (as xml) the "large" element
     */
    void xsetLarge(org.apache.xmlbeans.XmlBoolean large);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest newInstance() {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
