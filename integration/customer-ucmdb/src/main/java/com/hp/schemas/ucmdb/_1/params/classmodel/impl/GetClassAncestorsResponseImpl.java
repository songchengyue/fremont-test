/*
 * XML Type:  getClassAncestorsResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/classmodel
 * Java type: com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.classmodel.impl;
/**
 * An XML getClassAncestorsResponse(@http://schemas.hp.com/ucmdb/1/params/classmodel).
 *
 * This is a complex type.
 */
public class GetClassAncestorsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse
{
    
    public GetClassAncestorsResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLASSHIERARCHY$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "classHierarchy");
    private static final javax.xml.namespace.QName COMMENTS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "comments");
    
    
    /**
     * Gets the "classHierarchy" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy getClassHierarchy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy)get_store().find_element_user(CLASSHIERARCHY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "classHierarchy" element
     */
    public void setClassHierarchy(com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy classHierarchy)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy)get_store().find_element_user(CLASSHIERARCHY$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy)get_store().add_element_user(CLASSHIERARCHY$0);
            }
            target.set(classHierarchy);
        }
    }
    
    /**
     * Appends and returns a new empty "classHierarchy" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy addNewClassHierarchy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy)get_store().add_element_user(CLASSHIERARCHY$0);
            return target;
        }
    }
    
    /**
     * Gets the "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments getComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "comments" element
     */
    public void setComments(com.hp.schemas.ucmdb._1.types.ResComments comments)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            }
            target.set(comments);
        }
    }
    
    /**
     * Appends and returns a new empty "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments addNewComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            return target;
        }
    }
}
