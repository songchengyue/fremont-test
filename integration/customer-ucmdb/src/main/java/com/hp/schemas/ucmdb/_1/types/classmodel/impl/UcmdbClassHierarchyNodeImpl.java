/*
 * XML Type:  UcmdbClassHierarchyNode
 * Namespace: http://schemas.hp.com/ucmdb/1/types/classmodel
 * Java type: com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.classmodel.impl;
/**
 * An XML UcmdbClassHierarchyNode(@http://schemas.hp.com/ucmdb/1/types/classmodel).
 *
 * This is a complex type.
 */
public class UcmdbClassHierarchyNodeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassHierarchyNode
{
    
    public UcmdbClassHierarchyNodeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLASSNAMES$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "classNames");
    private static final javax.xml.namespace.QName CLASSPARENTNAME$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "classParentName");
    
    
    /**
     * Gets the "classNames" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.ClassNames getClassNames()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.ClassNames target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.ClassNames)get_store().find_element_user(CLASSNAMES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "classNames" element
     */
    public void setClassNames(com.hp.schemas.ucmdb._1.types.classmodel.ClassNames classNames)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.ClassNames target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.ClassNames)get_store().find_element_user(CLASSNAMES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.ClassNames)get_store().add_element_user(CLASSNAMES$0);
            }
            target.set(classNames);
        }
    }
    
    /**
     * Appends and returns a new empty "classNames" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.ClassNames addNewClassNames()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.ClassNames target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.ClassNames)get_store().add_element_user(CLASSNAMES$0);
            return target;
        }
    }
    
    /**
     * Gets the "classParentName" element
     */
    public java.lang.String getClassParentName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLASSPARENTNAME$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "classParentName" element
     */
    public org.apache.xmlbeans.XmlString xgetClassParentName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CLASSPARENTNAME$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "classParentName" element
     */
    public void setClassParentName(java.lang.String classParentName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLASSPARENTNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLASSPARENTNAME$2);
            }
            target.setStringValue(classParentName);
        }
    }
    
    /**
     * Sets (as xml) the "classParentName" element
     */
    public void xsetClassParentName(org.apache.xmlbeans.XmlString classParentName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CLASSPARENTNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CLASSPARENTNAME$2);
            }
            target.set(classParentName);
        }
    }
}
