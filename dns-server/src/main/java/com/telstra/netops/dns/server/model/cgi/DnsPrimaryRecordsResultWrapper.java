package com.telstra.netops.dns.server.model.cgi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telstra.netops.dns.server.model.DnsPrimaryRecord;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DnsPrimaryRecordsResultWrapper {
	
	@JsonProperty
	private List<DnsPrimaryRecord> result;
	
	@JsonIgnore
	public boolean isEmpty() {
		return result == null || result.get(0) == null || result.get(0).notARecord();
	}

	public List<DnsPrimaryRecord> getResult() {
		return result;
	}

}
