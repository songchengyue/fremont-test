package com.telstra.netops.lookingglass.server.service;

import com.telstra.netops.lookingglass.server.model.ClientNodeTestRequest;
import com.telstra.netops.lookingglass.server.model.NodeTestResult;

public interface NodeTestService {

	NodeTestResult performNodeTest(ClientNodeTestRequest request);
	
	NodeTestResult performNodeHostTest(ClientNodeTestRequest request);

}
