package com.telstra.netops.routemanagement.server.model;

/**
 * Modal class for multiple prefix upload
 *
 */
public class MultiplePrefixUploadRequest {
	private int level;
	private String service;
	private String product;
	private String networkPrefix;
	private String maintainedBy;
	private String orgin;
	private String emailId;
	private String route;
	private String ipVersion;
	private String fileContent;
	private String disclaimer;
	private String submit;

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getService() {
		return service;
	}

	public void setSvc(String service) {
		this.service = service;
	}

	public String getProduct() {
		return product;
	}

	public void setEt(String product) {
		this.product = product;
	}

	public String getNetworkPrefix() {
		return networkPrefix;
	}

	public void setNetworkPrefix(String networkPrefix) {
		this.networkPrefix = networkPrefix;
	}

	public String getMaintainedBy() {
		return maintainedBy;
	}

	public void setMaintainedBy(String maintainedBy) {
		this.maintainedBy = maintainedBy;
	}

	public String getOrgin() {
		return orgin;
	}

	public void setOrgin(String orgin) {
		this.orgin = orgin;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(String ipVersion) {
		this.ipVersion = ipVersion;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public String getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}
}
