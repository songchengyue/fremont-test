/*
 * XML Type:  getChangedCIs
 * Namespace: http://schemas.hp.com/ucmdb/1/params/history
 * Java type: com.hp.schemas.ucmdb._1.params.history.GetChangedCIs
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.history.impl;
/**
 * An XML getChangedCIs(@http://schemas.hp.com/ucmdb/1/params/history).
 *
 * This is a complex type.
 */
public class GetChangedCIsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.history.GetChangedCIs
{
    
    public GetChangedCIsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/history", "cmdbContext");
    private static final javax.xml.namespace.QName IDS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/history", "ids");
    private static final javax.xml.namespace.QName FROMDATE$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/history", "fromDate");
    private static final javax.xml.namespace.QName TODATE$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/history", "toDate");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets the "ids" element
     */
    public com.hp.schemas.ucmdb._1.types.IDs getIds()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().find_element_user(IDS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ids" element
     */
    public void setIds(com.hp.schemas.ucmdb._1.types.IDs ids)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().find_element_user(IDS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().add_element_user(IDS$2);
            }
            target.set(ids);
        }
    }
    
    /**
     * Appends and returns a new empty "ids" element
     */
    public com.hp.schemas.ucmdb._1.types.IDs addNewIds()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().add_element_user(IDS$2);
            return target;
        }
    }
    
    /**
     * Gets the "fromDate" element
     */
    public java.util.Calendar getFromDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FROMDATE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getCalendarValue();
        }
    }
    
    /**
     * Gets (as xml) the "fromDate" element
     */
    public org.apache.xmlbeans.XmlDateTime xgetFromDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlDateTime target = null;
            target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(FROMDATE$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "fromDate" element
     */
    public void setFromDate(java.util.Calendar fromDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FROMDATE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FROMDATE$4);
            }
            target.setCalendarValue(fromDate);
        }
    }
    
    /**
     * Sets (as xml) the "fromDate" element
     */
    public void xsetFromDate(org.apache.xmlbeans.XmlDateTime fromDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlDateTime target = null;
            target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(FROMDATE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(FROMDATE$4);
            }
            target.set(fromDate);
        }
    }
    
    /**
     * Gets the "toDate" element
     */
    public java.util.Calendar getToDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TODATE$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getCalendarValue();
        }
    }
    
    /**
     * Gets (as xml) the "toDate" element
     */
    public org.apache.xmlbeans.XmlDateTime xgetToDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlDateTime target = null;
            target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(TODATE$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "toDate" element
     */
    public void setToDate(java.util.Calendar toDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TODATE$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TODATE$6);
            }
            target.setCalendarValue(toDate);
        }
    }
    
    /**
     * Sets (as xml) the "toDate" element
     */
    public void xsetToDate(org.apache.xmlbeans.XmlDateTime toDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlDateTime target = null;
            target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(TODATE$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(TODATE$6);
            }
            target.set(toDate);
        }
    }
}
