/*
 * XML Type:  CIProperties
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.CIProperties
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types;


/**
 * An XML CIProperties(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public interface CIProperties extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CIProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("cipropertiesc865type");
    
    /**
     * Gets the "dateProps" element
     */
    com.hp.schemas.ucmdb._1.types.DateProps getDateProps();
    
    /**
     * True if has "dateProps" element
     */
    boolean isSetDateProps();
    
    /**
     * Sets the "dateProps" element
     */
    void setDateProps(com.hp.schemas.ucmdb._1.types.DateProps dateProps);
    
    /**
     * Appends and returns a new empty "dateProps" element
     */
    com.hp.schemas.ucmdb._1.types.DateProps addNewDateProps();
    
    /**
     * Unsets the "dateProps" element
     */
    void unsetDateProps();
    
    /**
     * Gets the "doubleProps" element
     */
    com.hp.schemas.ucmdb._1.types.DoubleProps getDoubleProps();
    
    /**
     * True if has "doubleProps" element
     */
    boolean isSetDoubleProps();
    
    /**
     * Sets the "doubleProps" element
     */
    void setDoubleProps(com.hp.schemas.ucmdb._1.types.DoubleProps doubleProps);
    
    /**
     * Appends and returns a new empty "doubleProps" element
     */
    com.hp.schemas.ucmdb._1.types.DoubleProps addNewDoubleProps();
    
    /**
     * Unsets the "doubleProps" element
     */
    void unsetDoubleProps();
    
    /**
     * Gets the "floatProps" element
     */
    com.hp.schemas.ucmdb._1.types.FloatProps getFloatProps();
    
    /**
     * True if has "floatProps" element
     */
    boolean isSetFloatProps();
    
    /**
     * Sets the "floatProps" element
     */
    void setFloatProps(com.hp.schemas.ucmdb._1.types.FloatProps floatProps);
    
    /**
     * Appends and returns a new empty "floatProps" element
     */
    com.hp.schemas.ucmdb._1.types.FloatProps addNewFloatProps();
    
    /**
     * Unsets the "floatProps" element
     */
    void unsetFloatProps();
    
    /**
     * Gets the "intListProps" element
     */
    com.hp.schemas.ucmdb._1.types.IntListProps getIntListProps();
    
    /**
     * True if has "intListProps" element
     */
    boolean isSetIntListProps();
    
    /**
     * Sets the "intListProps" element
     */
    void setIntListProps(com.hp.schemas.ucmdb._1.types.IntListProps intListProps);
    
    /**
     * Appends and returns a new empty "intListProps" element
     */
    com.hp.schemas.ucmdb._1.types.IntListProps addNewIntListProps();
    
    /**
     * Unsets the "intListProps" element
     */
    void unsetIntListProps();
    
    /**
     * Gets the "intProps" element
     */
    com.hp.schemas.ucmdb._1.types.IntProps getIntProps();
    
    /**
     * True if has "intProps" element
     */
    boolean isSetIntProps();
    
    /**
     * Sets the "intProps" element
     */
    void setIntProps(com.hp.schemas.ucmdb._1.types.IntProps intProps);
    
    /**
     * Appends and returns a new empty "intProps" element
     */
    com.hp.schemas.ucmdb._1.types.IntProps addNewIntProps();
    
    /**
     * Unsets the "intProps" element
     */
    void unsetIntProps();
    
    /**
     * Gets the "strProps" element
     */
    com.hp.schemas.ucmdb._1.types.StrProps getStrProps();
    
    /**
     * True if has "strProps" element
     */
    boolean isSetStrProps();
    
    /**
     * Sets the "strProps" element
     */
    void setStrProps(com.hp.schemas.ucmdb._1.types.StrProps strProps);
    
    /**
     * Appends and returns a new empty "strProps" element
     */
    com.hp.schemas.ucmdb._1.types.StrProps addNewStrProps();
    
    /**
     * Unsets the "strProps" element
     */
    void unsetStrProps();
    
    /**
     * Gets the "strListProps" element
     */
    com.hp.schemas.ucmdb._1.types.StrListProps getStrListProps();
    
    /**
     * True if has "strListProps" element
     */
    boolean isSetStrListProps();
    
    /**
     * Sets the "strListProps" element
     */
    void setStrListProps(com.hp.schemas.ucmdb._1.types.StrListProps strListProps);
    
    /**
     * Appends and returns a new empty "strListProps" element
     */
    com.hp.schemas.ucmdb._1.types.StrListProps addNewStrListProps();
    
    /**
     * Unsets the "strListProps" element
     */
    void unsetStrListProps();
    
    /**
     * Gets the "longProps" element
     */
    com.hp.schemas.ucmdb._1.types.LongProps getLongProps();
    
    /**
     * True if has "longProps" element
     */
    boolean isSetLongProps();
    
    /**
     * Sets the "longProps" element
     */
    void setLongProps(com.hp.schemas.ucmdb._1.types.LongProps longProps);
    
    /**
     * Appends and returns a new empty "longProps" element
     */
    com.hp.schemas.ucmdb._1.types.LongProps addNewLongProps();
    
    /**
     * Unsets the "longProps" element
     */
    void unsetLongProps();
    
    /**
     * Gets the "bytesProps" element
     */
    com.hp.schemas.ucmdb._1.types.BytesProps getBytesProps();
    
    /**
     * True if has "bytesProps" element
     */
    boolean isSetBytesProps();
    
    /**
     * Sets the "bytesProps" element
     */
    void setBytesProps(com.hp.schemas.ucmdb._1.types.BytesProps bytesProps);
    
    /**
     * Appends and returns a new empty "bytesProps" element
     */
    com.hp.schemas.ucmdb._1.types.BytesProps addNewBytesProps();
    
    /**
     * Unsets the "bytesProps" element
     */
    void unsetBytesProps();
    
    /**
     * Gets the "xmlProps" element
     */
    com.hp.schemas.ucmdb._1.types.XmlProps getXmlProps();
    
    /**
     * True if has "xmlProps" element
     */
    boolean isSetXmlProps();
    
    /**
     * Sets the "xmlProps" element
     */
    void setXmlProps(com.hp.schemas.ucmdb._1.types.XmlProps xmlProps);
    
    /**
     * Appends and returns a new empty "xmlProps" element
     */
    com.hp.schemas.ucmdb._1.types.XmlProps addNewXmlProps();
    
    /**
     * Unsets the "xmlProps" element
     */
    void unsetXmlProps();
    
    /**
     * Gets the "booleanProps" element
     */
    com.hp.schemas.ucmdb._1.types.BooleanProps getBooleanProps();
    
    /**
     * True if has "booleanProps" element
     */
    boolean isSetBooleanProps();
    
    /**
     * Sets the "booleanProps" element
     */
    void setBooleanProps(com.hp.schemas.ucmdb._1.types.BooleanProps booleanProps);
    
    /**
     * Appends and returns a new empty "booleanProps" element
     */
    com.hp.schemas.ucmdb._1.types.BooleanProps addNewBooleanProps();
    
    /**
     * Unsets the "booleanProps" element
     */
    void unsetBooleanProps();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.types.CIProperties newInstance() {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.CIProperties parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.CIProperties) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
