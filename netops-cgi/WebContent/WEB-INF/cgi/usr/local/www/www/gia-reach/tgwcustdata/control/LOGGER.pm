# (C) Telstra 2001
#
#
# Author: Ash Garg
# Date: 05/07/01
# File: LOGGER.pm
#
# This file is used to log events to a database
#
# $Id: LOGGER.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $

#
#
#
#To create the database use the DBinit module with the logdatabase script 
#
#
#
#

package LOGGER;
use Exporter;
use strict;
use Time::localtime;

our @ISA = qw(Exporter);

#Error codes
use constant SUCCESS => 1;
use constant ERROR => 0;

## Adds a message to the database.
##Note: sizeof($type)==25 && sizeof($message)==100
sub log
{
        my $dbh = shift @_;
        my $username = shift @_;
        my $type = substr(shift @_,0,25);
        my $message = substr(shift @_,0,100);
        
	
        # Write to database
        my $command = 
	  "insert into log (username, type, message, timestamp) 
           values ('$username', '$type', '$message', CURRENT_TIMESTAMP)";
       
	my $sth = $dbh->prepare($command) || 
	  die "Failed to prepare command\n";
        $sth->execute || warn "Failed to execute command\n";
        return SUCCESS;
	
}

#returns all logged messages depending on the parameters provided.
#it does not do wild card searches at the moment.
# expected parameters are: database handle, system, type, message, unix time start, unix time stop
sub get_logs
{
        my $dbh = shift @_;
        my $username = shift @_;
        my $type = shift @_;
        my $message = shift @_;
        my $utstart = shift @_;
        my $utstop = shift @_;

	my $command = "select timestamp, type, username, message from log";
	my $flag=0;


	if($username)
	{
	  warn "username $username";
		if($flag==0)
		{
			$command .= " where";
		}
		else
		{
			$command .= " and";
		}
		$flag++;	

		$command .= " username like '%$username%'";
	}

	if($type)
	{
	  warn "type $type";
		if($flag==0)
		{
			$command .= " where";
		}
		else
		{
			$command .= " and";
		}
		$flag++;	

		$command .= " type like '%$type%'";
	}

	if($message)
	{
		if($flag==0)
		{
			$command .= " where";
		}
		else
		{
			$command .= " and";
		}
		$flag++;	

		$command .= " message like '%$message%'";
	}

	if($utstart)
	{
		if($flag==0)
		{
			$command .= " where";
		}
		else
		{
			$command .= " and";
		}
		$flag++;	
	
		my $tm = gmtime($utstart);

		my $day = $tm->mday;
		my $mon=($tm->mon)+1;
		my $mmon;
		my $year=$tm->year+1900;

		my $hour=$tm->hour;
		my $min=$tm->min;
		my $sec=$tm->sec;

	        if($mon=~/1/) {$mmon="jan";}
	        if($mon=~/2/) {$mmon="feb";}
	        if($mon=~/3/) {$mmon="mar";}
		if($mon=~/4/) {$mmon="apr";}
        	if($mon=~/5/) {$mmon="may";}
        	if($mon=~/6/) {$mmon="jun";}
        	if($mon=~/7/) {$mmon="jul";}
        	if($mon=~/8/) {$mmon="aug";}
        	if($mon=~/9/) {$mmon="sep";}
        	if($mon=~/10/) {$mmon="oct";}
        	if($mon=~/11/) {$mmon="nov";}
        	if($mon=~/12/) {$mmon="dec";}

		my $startdate="$day-$mmon-$year";
		my $starttime=sprintf("%02d:%02d:%02d",$hour,$min,$sec);

		$command .= " datestamp >= '$startdate'";

		if($flag==0)
		{
			$command .= " where";
		}
		else
		{
			$command .= " and";
		}
		$flag++;	

		$command .= " timestamp >= '$starttime'";
	}

	if($utstop)
	{
		if($flag==0)
		{
			$command .= " where";
		}
		else
		{
			$command .= " and";
		}
		$flag++;	
	
		my $tm = gmtime($utstart);

                my $day = $tm->mday;
                my $mon=($tm->mon)+1;
                my $mmon;
                my $year=$tm->year+1900;

                my $hour=$tm->hour;
                my $min=$tm->min;
                my $sec=$tm->sec;


                if($mon=~/1/) {$mmon="jan";}
                if($mon=~/2/) {$mmon="feb";}
                if($mon=~/3/) {$mmon="mar";}
                if($mon=~/4/) {$mmon="apr";}
                if($mon=~/5/) {$mmon="may";}
                if($mon=~/6/) {$mmon="jun";}
                if($mon=~/7/) {$mmon="jul";}
                if($mon=~/8/) {$mmon="aug";}
                if($mon=~/9/) {$mmon="sep";}
                if($mon=~/10/) {$mmon="oct";}
                if($mon=~/11/) {$mmon="nov";}
                if($mon=~/12/) {$mmon="dec";}

                my $enddate="$day-$mmon-$year";
		my $endtime=sprintf("%02d:%02d:%02d",$hour,$min,$sec);

		$command .= " datestamp <= '$enddate'";

		if($flag==0)
		{
			$command .= " where";
		}
		else
		{
			$command .= " and";
		}
		$flag++;	

		$command .= " timestamp <= '$endtime'";
	}



       	my $sth = $dbh->prepare($command) || die "Failed to prepare command\n";
        $sth->execute || warn "Failed to execute command\n";
 	my $array_ref = $sth->fetchall_arrayref();
	return $array_ref;
}

# By convention, end a package file with 1,
# so the use or require command succeeds.
1;
