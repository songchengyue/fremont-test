# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 24 September 2001
# File: CMS_NEWS_DB.pm
#
# $Id: CMS_NEWS_DB.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $

package CMS_NEWS_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;

our $error;
 
sub add_news_entry {
  my $accno = $dbh->quote(shift @_);
  my $cust_incoming = $dbh->quote(shift @_);
  my $cust_outgoing = $dbh->quote(shift @_);
  my $path = $dbh->quote(shift @_);
  my $delay = shift @_;
  
  my $sth = $dbh->prepare
    ("INSERT INTO news
         (accno, cust_incoming, cust_outgoing, path, delay, request_time, status)
         VALUES ($accno, $cust_incoming, $cust_outgoing, $path, $delay, CURRENT_TIMESTAMP,2)");

  # 2 is added

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;
}

sub delete_news_entry {
  my $accno =  $dbh->quote(shift @_);
  my $cust_incoming =  $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("DELETE FROM news
      WHERE accno = $accno
        AND cust_incoming = $cust_incoming");

 
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;
}

sub get_news_entries_for_account {
  my $accno =  $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT cust_incoming, cust_outgoing, path, delay, request_time, status
      FROM news
      WHERE accno = $accno");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
}

sub get_accno_for_news {
  my $cust_incoming = $dbh->quote(shift @_);

  my $sth = $dbh->prepare 
    ("SELECT accno
      FROM news
      WHERE cust_incoming = $cust_incoming");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  if (my @result = $sth->fetchrow_array) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result[0];
  } else {
    $error = $dbh->errstr;
    $sth->finish;
    return undef;
  }
}

sub get_news_feeds {

  my $sth = $dbh->prepare
    ("SELECT accno, cust_incoming, cust_outgoing, path, delay
      FROM news");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
}

sub search_news_feeds {
  
  my $accno = shift @_ || "";
  my $cust_incoming = shift @_ || "";
  my $cust_outgoing = shift @_ || "";
  my $path = shift @_ || "";
  my $delay = shift @_ || "";
  my $timestamp_start = shift @_ || "";
  my $timestamp_end = shift @_ || "";

  my $command = "SELECT accno, cust_incoming, cust_outgoing, path, 
                 delay, request_time
                 FROM news ";
  
  my $extension = "";

  if ($timestamp_start ne "") {
    $extension .= "and request_time >= '$timestamp_start'
                   and request_time <= '$timestamp_end' ";
  }
  
  if ($accno ne "") {
    $extension .= "and accno ilike '%$accno%' ";
  }
  
  if ($cust_incoming ne "") {
    $extension .= "and cust_incoming ilike '%$cust_incoming%' ";
  }
  
  if ($cust_outgoing ne "") {
    $extension .= "and cust_outgoing ilike '%$cust_outgoing%' ";
  }
  
  if ($path ne "") {
    $extension .= "and path ilike '%$path%' ";
  }
  
  if ($delay ne "") {
    $extension .= "and delay = $delay ";
  }
  


  $extension =~ s/and/where/;
  
  $command .= "$extension order by request_time DESC ";
  
  if ($extension eq "") {
    # limit by default
    $command .= "limit 20";
  }

  my $sth = $dbh->prepare($command);

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
  
}

1;
