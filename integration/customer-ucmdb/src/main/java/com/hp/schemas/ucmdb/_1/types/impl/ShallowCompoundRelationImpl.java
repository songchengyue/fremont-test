/*
 * XML Type:  ShallowCompoundRelation
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ShallowCompoundRelation(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ShallowCompoundRelationImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation
{
    
    public ShallowCompoundRelationImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ID$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "ID");
    private static final javax.xml.namespace.QName END1ID$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "end1ID");
    private static final javax.xml.namespace.QName END2ID$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "end2ID");
    
    
    /**
     * Gets the "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID getID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ID" element
     */
    public void setID(com.hp.schemas.ucmdb._1.types.ID id)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ID$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ID$0);
            }
            target.set(id);
        }
    }
    
    /**
     * Appends and returns a new empty "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID addNewID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ID$0);
            return target;
        }
    }
    
    /**
     * Gets the "end1ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID getEnd1ID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(END1ID$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "end1ID" element
     */
    public void setEnd1ID(com.hp.schemas.ucmdb._1.types.ID end1ID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(END1ID$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(END1ID$2);
            }
            target.set(end1ID);
        }
    }
    
    /**
     * Appends and returns a new empty "end1ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID addNewEnd1ID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(END1ID$2);
            return target;
        }
    }
    
    /**
     * Gets the "end2ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID getEnd2ID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(END2ID$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "end2ID" element
     */
    public void setEnd2ID(com.hp.schemas.ucmdb._1.types.ID end2ID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(END2ID$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(END2ID$4);
            }
            target.set(end2ID);
        }
    }
    
    /**
     * Appends and returns a new empty "end2ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID addNewEnd2ID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(END2ID$4);
            return target;
        }
    }
}
