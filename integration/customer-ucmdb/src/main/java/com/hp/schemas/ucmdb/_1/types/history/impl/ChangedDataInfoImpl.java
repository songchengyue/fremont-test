/*
 * XML Type:  ChangedDataInfo
 * Namespace: http://schemas.hp.com/ucmdb/1/types/history
 * Java type: com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.history.impl;
/**
 * An XML ChangedDataInfo(@http://schemas.hp.com/ucmdb/1/types/history).
 *
 * This is a complex type.
 */
public class ChangedDataInfoImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo
{
    
    public ChangedDataInfoImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHANGEDIDS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/history", "changedIDs");
    private static final javax.xml.namespace.QName ROOTID$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/history", "rootID");
    private static final javax.xml.namespace.QName CHANGETYPE$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/history", "changeType");
    
    
    /**
     * Gets the "changedIDs" element
     */
    public com.hp.schemas.ucmdb._1.types.IDs getChangedIDs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().find_element_user(CHANGEDIDS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "changedIDs" element
     */
    public void setChangedIDs(com.hp.schemas.ucmdb._1.types.IDs changedIDs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().find_element_user(CHANGEDIDS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().add_element_user(CHANGEDIDS$0);
            }
            target.set(changedIDs);
        }
    }
    
    /**
     * Appends and returns a new empty "changedIDs" element
     */
    public com.hp.schemas.ucmdb._1.types.IDs addNewChangedIDs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().add_element_user(CHANGEDIDS$0);
            return target;
        }
    }
    
    /**
     * Gets the "rootID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID getRootID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ROOTID$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "rootID" element
     */
    public void setRootID(com.hp.schemas.ucmdb._1.types.ID rootID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ROOTID$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ROOTID$2);
            }
            target.set(rootID);
        }
    }
    
    /**
     * Appends and returns a new empty "rootID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID addNewRootID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ROOTID$2);
            return target;
        }
    }
    
    /**
     * Gets the "changeType" element
     */
    public com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType.Enum getChangeType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHANGETYPE$4, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "changeType" element
     */
    public com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType xgetChangeType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType target = null;
            target = (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType)get_store().find_element_user(CHANGETYPE$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "changeType" element
     */
    public void setChangeType(com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType.Enum changeType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHANGETYPE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CHANGETYPE$4);
            }
            target.setEnumValue(changeType);
        }
    }
    
    /**
     * Sets (as xml) the "changeType" element
     */
    public void xsetChangeType(com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType changeType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType target = null;
            target = (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType)get_store().find_element_user(CHANGETYPE$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType)get_store().add_element_user(CHANGETYPE$4);
            }
            target.set(changeType);
        }
    }
    /**
     * An XML changeType(@http://schemas.hp.com/ucmdb/1/types/history).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo$ChangeType.
     */
    public static class ChangeTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType
    {
        
        public ChangeTypeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ChangeTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
