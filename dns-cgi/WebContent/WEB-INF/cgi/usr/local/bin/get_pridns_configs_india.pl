#!/usr/bin/perl

# INSTALL ON PRIMARY NAMESERVERS.

use strict;
use File::Copy;

my $file = "primary";
my $time = time;
my $cmd =`ps auxw | grep named.conf | grep -i india | grep -i primary | grep -v grep | cut -c9-15`;
print "before killing $cmd\n";

# get named config file

#rename ("/etc/namedb/customers/$file","/etc/namedb/backup/$file.$time");
#my $res = `/usr/local/bin/rsync -azlv hk-webserver::pridns/$file /etc/namedb/customers/ > /var/log/pridns.conf.rsync`;  # hk-spare change to wanchai when put into production
#my $res = `/usr/bin/rsync -azlv hk-webserver::pridns /etc/namedb/customers > /var/log/pridns.rsync`;  # hk-spare change to wanchai when put into production
my $res = `/usr/bin/rsync -azlv hhtred278::pridns_india/$file /etc/namedb/india/primary/customers/ > /var/log/pridns_india.conf.rsync`;

rename ("/etc/namedb/india/primary/customers/$file","/etc/namedb/india/primary/backup/$file.$time");

copy ("/etc/namedb/india/named.conf.template", "/etc/namedb/india/primary/named.conf.new");
copy ("/etc/namedb/india/primary/named.conf", "/etc/namedb/india/primary/backup/named.conf.$time");
`cat /etc/namedb/india/primary/backup/$file.$time >> /etc/namedb/india/primary/named.conf.new`;
rename ("/etc/namedb/india/primary/named.conf.new","/etc/namedb/india/primary/named.conf");

# get zone files

my $zone_file_path = '/etc/namedb/india/primary/zones/';
my $zone_file_backup_path = '/etc/namedb/india/primary/backup/';
opendir(ZF, $zone_file_path) || die "Cannot open zone files directory ($zone_file_path): $!\n";
my @current_files = readdir(ZF);
closedir(ZF);
foreach my $f (@current_files) {
        unless ( ($f eq ".") || ($f eq "..") || ($f eq 'db.example.com') || ($f !~ /^db\./) ) {
                my $fullf = "$zone_file_path".$f;
                 print "backup file $f...\n";
                my $targetf = $zone_file_backup_path."$f.$time";
                rename ("$fullf", "$targetf") || die "Cannot move file from ($fullf) to ($targetf): $!\n";
        }
}

#my $res = `/usr/local/bin/rsync -azlv --include 'db.*' --exclude '*' hk-webserver::pridns /etc/namedb/customers/ > /var/log/pridns.zone.rsync`;  # hk-spare change to wanchai when put into production
my $res = `/usr/bin/rsync -azlv --include 'db.*' --exclude '*' hhtred278::pridns_india /etc/namedb/india/primary/customers/ > /var/log/pridns_india.zone.rsync`;  # hk-spare change to wanchai when put into production
$res = `/bin/cp -p /etc/namedb/india/primary/customers/db.* $zone_file_path`;

#kill the process
print "killing the process $cmd\n";
`sudo kill -9 $cmd`;
#restart
print "restarting the server\n";
`sudo /usr/local/dns/sbin/named -c /etc/namedb/india/primary/named.conf -u named`;
my $cmd1 =`ps auxw | grep named.conf |grep -i india | grep -v grep | cut -c9-15`;
print "after restart $cmd1 \n";


# remove out-dated file
`/usr/bin/find /etc/namedb/india/primary/customers/ -type f -mtime +1 -print | xargs rm -f`;
`/usr/bin/find /etc/namedb/india/primary/backup/ -type f -mtime +1 -print | xargs rm -f`;
