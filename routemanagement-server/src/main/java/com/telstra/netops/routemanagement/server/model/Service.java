package com.telstra.netops.routemanagement.server.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

/**
 * Entity class for Service table
 *
 */
@Entity
@Table(name = "Service")
public class Service implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "Service_ID")
	private String serviceId;

	@Column(name = "Telstra_Service_ID")
	private String telstraServiceId;

	@Column(name = "Customer_ID")
	private String customerId;

	@Column(name = "Product_Line")
	private String productLine;

	/*@Column(name = "Router")
	private String router;*/

	@Column(name = "Interface")
	private String routerInterface;

	@Column(name = "Parent_Site_ID")
	private String parentSiteId;

	@Column(name = "As_No")
	private String asNo;

	@Column(name = "Bgp_Neighbor_Ip")
	private String bgpNeighborIp;

	@Column(name = "BGP_NEIGHBOR_IP_V6")
	private String bgpNeighborIpV6;

	@Column(name = "Irrd_Object")
	private String irrdObject;

	@Column(name = "Bha_Bgp_Neighbor_Ip")
	private String bhaBgpNeighborIp;

	@Column(name = "BHA_BGP_NEIGHBOR_IP_V6")
	private String bhaBgpNeighborIpV6;

	/*@Column(name = "Black_Hole_Routing")
	private String blackHoleRouter;*/

	public String getId() {
		return serviceId;
	}

	public void setId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getTelstraServiceId() {
		return telstraServiceId;
	}

	public void setTelstraServiceId(String telstraServiceId) {
		this.telstraServiceId = telstraServiceId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProductLine() {
		return productLine;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}

	/*public String getRouter() {
		return router;
	}

	public void setRouter(String router) {
		this.router = router;
	}*/

	public String getRouterInterface() {
		return routerInterface;
	}

	public void setRouterInterface(String routerInterface) {
		this.routerInterface = routerInterface;
	}

	public String getParentSiteId() {
		return parentSiteId;
	}

	public void setParentSiteId(String parentSiteId) {
		this.parentSiteId = parentSiteId;
	}

	public String getAsNo() {
		return asNo;
	}

	public void setAsNumber(String asNo) {
		this.asNo = asNo;
	}

	public String getBgpNeighborIP() {
		return bgpNeighborIp;
	}

	public void setBGPNeighborIP(String bgpNeighborIp) {
		this.bgpNeighborIp = bgpNeighborIp;
	}

	public String getIrrdObject() {
		return irrdObject;
	}

	public void setIrrdObject(String irrdObject) {
		this.irrdObject = irrdObject;
	}

	public String getBhaBgpNeighborIp() {
		return bhaBgpNeighborIp;
	}

	public void setBhaBgpNeighborIP(String bhaBgpNeighborIp) {
		this.bhaBgpNeighborIp = bhaBgpNeighborIp;
	}

	/*public String getBlackHoleRouter() {
		return blackHoleRouter;
	}

	public void setBlackHoleRouter(String blackHoleRouter) {
		this.blackHoleRouter = blackHoleRouter;
	}*/

	public String getBgpNeighborIpV6() {
		return bgpNeighborIpV6;
	}

	public void setBgpNeighborIpV6(String bgpNeighborIpV6) {
		this.bgpNeighborIpV6 = bgpNeighborIpV6;
	}

	public String getBhaBgpNeighborIpV6() {
		return bhaBgpNeighborIpV6;
	}

	public void setBhaBgpNeighborIpV6(String bhaBgpNeighborIpV6) {
		this.bhaBgpNeighborIpV6 = bhaBgpNeighborIpV6;
	}

	

}
