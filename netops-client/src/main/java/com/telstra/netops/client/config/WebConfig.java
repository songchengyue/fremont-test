package com.telstra.netops.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.ContentVersionStrategy;
import org.springframework.web.servlet.resource.VersionResourceResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/classes/templates/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
		
		//
		// CACHE BUSTING FOR JAVASCRIPT!
		//
		// We make JavaScript resources available via versioned names.
		// The version strategy is to include an MD5 hash of the content
		// of the resource in the name e.g. "app.module-c3cf4690c268c19f00f432ec3c7d19d9.js"
		//
		
		VersionResourceResolver versionResourceResolver = new VersionResourceResolver();
		versionResourceResolver.addVersionStrategy(new ContentVersionStrategy(), "/**");
		
        registry.addResourceHandler("/**/*.js")
            	.addResourceLocations("classpath:/static/")
            	.setCachePeriod(60 * 60 * 24 * 365) // one year
            	.resourceChain(true)
            	.addResolver(versionResourceResolver);
    }	

}
