package com.hp.ucmdb.generated;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator.Enum;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeDocument;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponseDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponseDocument;
import com.hp.schemas.ucmdb._1.types.props.CustomProperties;
import com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties;
import com.hp.schemas.ucmdb._1.types.props.PropertiesList;
import com.hp.schemas.ucmdb._1.types.props.TypedProperties;
import com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection;
import com.hp.schemas.ucmdb._1.types.*;
import com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator;

public class UcmdbRfs {
	static int numberOfChunks;
	static int chunkNumber;

	static String key1;
	static String key2;

	static String className2 ="resource_facing_service";

	static String propertyName1 = "cfs_id";
	static String propertyName2 = "service_id";
	static String conditionName2 = "cfs_id";
	static String conditionName3 = "service_id";

	
	static String callerApplication = "netops";
	static String cndtnLgclOprtrOr = "OR";
	static String strOprtr = "EqualIgnoreCase";


	static List<String> cmdbIds = new ArrayList<String>();
	static List<String> cfsIds = new ArrayList<String>();
	static List<String> cfsIdsTemp ;
	static List<String> cfsIdsNew = new ArrayList<String>();
	static List<String> serviceIds = new ArrayList<String>();
	static List<Timestamp> fullLastUpdatedDate = new ArrayList<Timestamp>();
	
	static int cfsIdSize ;
	static int cfsIdTempSize ;
	
	static String cmdbId = "";
	static String cfsId = "";
	static String serviceId = "";
	static String getName;
	static String getValue;
	static String configPath;

	private static final Logger LOGGER = LoggerFactory.getLogger(UcmdbNetopsSynch.class);
	static Properties props = new Properties();
	static Connection dbConnection;
	static Statement statement;
	static UcmdbServiceStub stub = null;
	
	public static void getServicesFromRfs(UcmdbServiceStub ucmdbStub,String mainConfigPath) {
		stub = ucmdbStub;
		configPath = mainConfigPath;
		LOGGER.info("configPath : " +configPath);
		try {
			loadParams(configPath);
        	//Deleting previous Entries from CDM_RFS_1_STG
        	dbConnection = getDBConnection();
			Statement delstmt = dbConnection.createStatement();
			delstmt.executeQuery("delete from CDM_RFS_1_STG");
			LOGGER.info("deleted");
			statement = dbConnection.createStatement();

			String selectSQL = "SELECT A_CFS_ID FROM CFS_STG " ;

			try {
				dbConnection = getDBConnection();
				statement = dbConnection.createStatement();

				//LOGGER.info(selectSQL);
				ResultSet rSet = statement.executeQuery(selectSQL);
				while(rSet.next())
				{
					//LOGGER.info("within select loop");
					cfsId = rSet.getString("A_CFS_ID");
					cfsIds.add(cfsId);
					//LOGGER.info("cfsId : " +cfsId);
				}
				cfsIdSize = cfsIds.size();
				LOGGER.info("cfsIdsSize : " + cfsIdSize);
				
		//Dividing into batches of 50
		//for(int i=0; i< cfsIds.size();i++)
		int i=0;
		while(i<cfsIdSize)
		{
			int count = 0;
			cfsIdsTemp = new ArrayList<String>();
					while((count<50) && (i<cfsIdSize))
					{
						//LOGGER.info("i : " +i);
						//LOGGER.info(cfsIds.get(i));
					cfsIdsTemp.add(cfsIds.get(i));
					i++;
					count++;
					//LOGGER.info("i : " +i);
					//LOGGER.info("count : " +count);
					}
					cfsIdTempSize = cfsIdsTemp.size();
					//LOGGER.info("helloo");
					//LOGGER.info("cfsIdTempSize : " +cfsIdTempSize);
					getFilteredCIsByTypeDemo();
		}
		insertIntoTable();
				
			} catch (Exception e) {
				 LOGGER.error("exception inside main Division", e);
			}
	
			
		} catch (Exception e) {
			 LOGGER.error("exception inside main", e);
		}
	}

	/* getFilteredCIsByType Operation*/
	public static void getFilteredCIsByTypeDemo() {
		try {	
			LOGGER.info("inside getFilteredCIsByTypeDemo method");
			//LOGGER.info("cfsIdTempSize : " + cfsIdTempSize);
			
			Enum conditionsLogicalOperator = Enum.forString(cndtnLgclOprtrOr);

			StrCondition[] strConditionArray = new StrCondition[cfsIdTempSize];
			for(int j=0; j<cfsIdTempSize;j++)
			{
				StrCondition strCondition =  StrCondition.Factory.newInstance();
				StrProp strProp = StrProp.Factory.newInstance();
				strProp.setName(conditionName2);
				strProp.setValue(cfsIdsTemp.get(j));
				strCondition.setCondition(strProp);
				strCondition.setStrOperator(StrOperator.Enum.forString(strOprtr));
				strConditionArray[j]= strCondition;
				
			}
	
			Conditions conditions = Conditions.Factory.newInstance();
			StrConditions strConditions= StrConditions.Factory.newInstance();
			
			
			strConditions.setStrConditionArray(strConditionArray);
			conditions.setStrConditions(strConditions);
			
			CmdbContext cntxt = CmdbContext.Factory.newInstance();
			cntxt.setCallerApplication(callerApplication);
			
			GetFilteredCIsByType getFilteredCIsByType = GetFilteredCIsByType.Factory.newInstance();
			getFilteredCIsByType.setCmdbContext(cntxt);
			getFilteredCIsByType.setType(className2);
			getFilteredCIsByType.setConditions(conditions);
			getFilteredCIsByType.setConditionsLogicalOperator(conditionsLogicalOperator);
			

			CustomProperties properties = CustomProperties.Factory.newInstance();
			PropertiesList propertiesList= PropertiesList.Factory.newInstance();
			propertiesList.addPropertyName(propertyName1);
			propertiesList.addPropertyName(propertyName2);
			properties.setPropertiesList(propertiesList);
			getFilteredCIsByType.setProperties(properties);

			GetFilteredCIsByTypeDocument request = GetFilteredCIsByTypeDocument.Factory.newInstance();
			request.setGetFilteredCIsByType(getFilteredCIsByType);
			
			//LOGGER.info("getFilteredCIsByType request : " + request);					
			//LOGGER.info("set inputs");
			
			HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
			basicAuthentication.setUsername("admin");
			basicAuthentication.setPassword("admin");
			int so_timeout = 480000;
			int con_timeout = 60000;
			stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
			//LOGGER.info("authentication passed");

			GetFilteredCIsByTypeResponseDocument response = stub.getFilteredCIsByType(request);
			//LOGGER.info("getFilteredCIsByType response : " + response);
						
			ChunkInfo chunkInfo = response.getGetFilteredCIsByTypeResponse().getChunkInfo();
			numberOfChunks = chunkInfo.getNumberOfChunks();
			ChunkKey chunkKey = chunkInfo.getChunksKey();
			key1 = chunkKey.getKey1();
			key2 = chunkKey.getKey2();
			
			LOGGER.info("numberOfChunks : " + numberOfChunks);
			//LOGGER.info("key1 : " + key1);
			//LOGGER.info("key2 : " + key2);
			if(numberOfChunks==0)
			{
			CI[] CI = response.getGetFilteredCIsByTypeResponse().getCIs().getCIArray();
			//LOGGER.info(CI.length);
			java.util.Date date= new java.util.Date();
			for(int j=0 ; j < CI.length ; j++)
			{
				cmdbId = CI[j].getID().getStringValue();
				cmdbIds.add(cmdbId);
				fullLastUpdatedDate.add(new Timestamp(date.getTime()));
				StrProp[] strprop = CI[j].getProps().getStrProps().getStrPropArray();
				//LOGGER.info("strprop.length : " + strprop.length);
				if(strprop.length!=2)
				{
					LOGGER.info("cmdbIdNoSrvc : "+cmdbId);
					serviceIds.add("");
					cfsIdsNew.add("");
				}
				else
				{
				for (int p = 0; p < strprop.length; p++)
				{
					getName = strprop[p].getName();
					getValue = strprop[p].getValue();
					if(getName.equalsIgnoreCase("cfs_id")){
						cfsId = getValue;
						//LOGGER.info("cfsId : " + cfsId);
						cfsIdsNew.add(getValue);
					}
					if(getName.equalsIgnoreCase("service_id")){
						serviceId = getValue;
						serviceIds.add(serviceId);
						//LOGGER.info("serviceId : " + serviceId);
					}
				}
				}
			}
			LOGGER.info("cmdbIdSize : "+cmdbIds.size());
			LOGGER.info("serviceIdsSize : "+serviceIds.size());
			LOGGER.info("cfsIdsNewSize : " +cfsIdsNew.size());
			//LOGGER.info("SUCCESS");
		
			}
			else
			{
				pullTopologyMapChunksFull();
			}
			
		
		}catch (RemoteException rme) {
			LOGGER.error("RemoteException inside getFilteredCIsByTypeDemo", rme);
		} catch (UcmdbFault ufe) {
			LOGGER.error("UcmdbFaultException inside getFilteredCIsByTypeDemo", ufe);
		} catch (Exception e) {
			LOGGER.error("exception inside getFilteredCIsByTypeDemo", e);
		}

	}

	 // pullTopologyMapChunksFull Operation
		public static void pullTopologyMapChunksFull() {
			LOGGER.info("inside pullTopologyMapChunksFull method");

			try {
				
				//LOGGER.info("numberOfChunks : " + numberOfChunks);
				//LOGGER.info("key1 : " + key1);
				//LOGGER.info("key2 : " + key2);
				for(int i=1 ; i <= numberOfChunks ; i++)
				{
					chunkNumber = i;
					//LOGGER.info("chunkNumber:" +chunkNumber);
					
					CmdbContext cntxt = CmdbContext.Factory.newInstance();
					cntxt.setCallerApplication(callerApplication);
					ChunkRequest chunkRequest = ChunkRequest.Factory.newInstance();
					ChunkInfo chunkInfo = ChunkInfo.Factory.newInstance();
					
					chunkRequest.setChunkNumber(chunkNumber);
					chunkInfo.setNumberOfChunks(numberOfChunks);

					ChunkKey chunksKey = ChunkKey.Factory.newInstance();
					chunksKey.setKey1(key1);
					chunksKey.setKey2(key2);
					
					chunkInfo.setChunksKey(chunksKey);
					chunkRequest.setChunkInfo(chunkInfo );
					
					
					TypedPropertiesCollection queryTypedProperties = TypedPropertiesCollection.Factory.newInstance();
					TypedProperties typedProperties = TypedProperties.Factory.newInstance();
					typedProperties.setType(className2);
					CustomTypedProperties properties = CustomTypedProperties.Factory.newInstance();
					PropertiesList propertiesList = PropertiesList.Factory.newInstance();
					String[] propertyNameArray = {propertyName1,propertyName2};
					propertiesList.setPropertyNameArray(propertyNameArray );

					properties.setPropertiesList(propertiesList);
					typedProperties.setProperties(properties);
					
					TypedProperties[] typedPropertiesArray ={typedProperties};
					queryTypedProperties.setTypedPropertiesArray(typedPropertiesArray );
					
					PullTopologyMapChunks pullTopologyMapChunks = PullTopologyMapChunks.Factory.newInstance();
					
					pullTopologyMapChunks.setCmdbContext(cntxt);
					pullTopologyMapChunks.setChunkRequest(chunkRequest);
					pullTopologyMapChunks.setQueryTypedProperties(queryTypedProperties);
					PullTopologyMapChunksDocument request = PullTopologyMapChunksDocument.Factory.newInstance();
					request.setPullTopologyMapChunks(pullTopologyMapChunks);
					//LOGGER.info("pullTopologyMapChunks request" +request);					
					
					HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
					basicAuthentication.setUsername("admin");
					basicAuthentication.setPassword("admin");
					int so_timeout = 480000;
					int con_timeout = 60000;
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
					//LOGGER.info("authentication passed");

					PullTopologyMapChunksResponseDocument response = stub.pullTopologyMapChunks(request);
					//LOGGER.info("pullTopologyMapChunks response : " + response);
					
					
					CINode[] ciNode = response.getPullTopologyMapChunksResponse().getTopologyMap().getCINodes().getCINodeArray();
					
					CI[] ci = ciNode[0].getCIs().getCIArray();
					//LOGGER.info("ci : " + ci);
					//LOGGER.info(ci.length);
					java.util.Date date= new java.util.Date();
					for(int j=0 ; j < ci.length ; j++)
					{						
						cmdbId = ci[j].getID().getStringValue();	
						
						cmdbIds.add(cmdbId);
						fullLastUpdatedDate.add(new Timestamp(date.getTime()));
						//LOGGER.info("IDs : " + ci[j].getID().getStringValue());
						//LOGGER.info("type : " +ci[j].getType());

							//LOGGER.info("within fetch for full load");
							StrProp[] strprop = ci[j].getProps().getStrProps().getStrPropArray();
							//LOGGER.info("strprop.length : " + strprop.length);
							if(strprop.length!=2)
							{
								LOGGER.info("cmdbIdNoSrvc : "+cmdbId);
								serviceIds.add("");
								cfsIdsNew.add("");
							}
							else
							{
							for (int p = 0; p < strprop.length; p++)
							{
								getName = strprop[p].getName();
								getValue = strprop[p].getValue();
								if(getName.equalsIgnoreCase("cfs_id")){
									cfsId = getValue;
									//LOGGER.info("cfsId : " + cfsId);
									cfsIdsNew.add(getValue);
								}
								if(getName.equalsIgnoreCase("service_id")){
									serviceId = getValue;
									serviceIds.add(serviceId);
									//LOGGER.info("serviceId : " + serviceId);
								}
								
							}	
							}
						
					}


				}
			LOGGER.info("OUTSIDE CHUNK LOOP");
			LOGGER.info("cmdbIdSize : "+cmdbIds.size());
			LOGGER.info("serviceIdsSize : "+serviceIds.size());
			LOGGER.info("cfsIdsNewSize : " +cfsIdsNew.size());
			
			}catch (RemoteException rme) {
				LOGGER.error("RemoteException inside pullTopologyMapChunksDemo", rme);
			} catch (UcmdbFault ufe) {
				LOGGER.error("UcmdbFaultException inside pullTopologyMapChunksDemo", ufe);
			}catch (Exception e) {
				LOGGER.error("exception inside pullTopologyMapChunksFull", e);
			}

		}
		
	private static void insertIntoTable() {
		//Inserting Values into CDM_RFS_1_STG TABLE
		LOGGER.info("Inserting");
		try {
			dbConnection = getDBConnection();
			String stringSQL = "INSERT INTO CDM_RFS_1_STG (CMDB_ID,A_CFS_ID,A_SERVICE_ID,TIMESTAMP) VALUES (?,?,?,?)" ;
			PreparedStatement ps = dbConnection.prepareStatement(stringSQL);
			
			for (int k = 0; k < serviceIds.size(); k++){
				ps.setString(1, cmdbIds.get(k));
				ps.setString(2, cfsIdsNew.get(k));
				ps.setString(3, serviceIds.get(k));
				ps.setTimestamp(4, fullLastUpdatedDate.get(k));									
				ps.addBatch();
			}		
			//LOGGER.info("completed batch execution");
			ps.executeBatch();
			ps.close();
			dbConnection.close();
			//LOGGER.info("connection closed");
		} 
		catch (SQLException sqle) {
			LOGGER.info(sqle.getMessage());
			LOGGER.error("SQL exception inside insertIntoTable", sqle);
		}
		catch (Exception e) {
			LOGGER.error("exception inside insertIntoTable", e);
		}	
		
	}

	public static void loadParams(String configPath) {
	    InputStream is = null;
	 
	    try {
	    	LOGGER.info("configPath : " +configPath);
	        File f = new File (configPath);
	    	 //File f = new File("E:\\Ashwini\\workspaceAhana\\ucmdbSrv\\src\\config.txt");
	        is = new FileInputStream( f );
	    }
	    catch ( Exception e ) { is = null; }
	 
	    try {
	        props.load( is );
	    }
	    catch ( Exception e ) {
	    	LOGGER.error("exception inside loadParams", e);
	    }

	   
	}
	
	private static Connection getDBConnection() {
		//Using config file to remove hardcoding
		String DB_DRIVER = props.getProperty("DB_DRIVER");
		String DB_CONNECTION = props.getProperty("DB_CONNECTION");
		String DB_USER = props.getProperty("DB_USER");
		String DB_PASSWORD = props.getProperty("DB_PASSWORD");
		
		try {
			
			Class.forName(DB_DRIVER);
			
		} catch (ClassNotFoundException e) {
			LOGGER.info(e.getMessage());
		}
		
		
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
			return dbConnection;
		
		
		} catch (SQLException e) {
			LOGGER.info(e.getMessage());
		}
		return dbConnection;
	}
	
}
