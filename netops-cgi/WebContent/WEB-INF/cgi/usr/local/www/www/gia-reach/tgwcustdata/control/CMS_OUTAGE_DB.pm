# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 12 July 2001
# File: CMS_DB.pm
#
# This module contains subroutines used to interact with the database
#
# $Id: CMS_OUTAGE_DB.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $

package CMS_OUTAGE_DB;
use Exporter;
use DBI;
use warnings;
use strict;
use LOGGER;
use CMS_DB;

our @ISA = qw (Exporter);

our $error;

use constant SUCCESS => 1;
use constant FAIL => 0;


sub get_outage_status {
  my $sth = $dbh->prepare("select * from outagestatus");
  $sth->execute;
  my $values = $sth->fetchall_arrayref;
  return $values;
}

sub get_outage_status_text {

  my $outage_status_id = shift @_;

  my $sth = $dbh->prepare("select outagestatustext from outage_status
                           where outagestatusid = $outage_status_id");
  $sth->execute;
  my $values = $sth->fetchrow_array;
  $sth->finish;
  return $values;

}

sub get_outage_types {
  my $sth = $dbh->prepare("select * from outagetype");
  $sth->execute;
  my $values = $sth->fetchall_arrayref;
  return $values;
}

sub get_outage_type_text {

  my $outage_type_id = shift @_;

  my $sth = $dbh->prepare("select * from outagetype
                           where outagetypeid = $outage_type_id");
  $sth->execute;
  my $values = $sth->fetchrow_array;
  $sth->finish;
  return $values;

}

sub get_outage_levels {
  my $sth = $dbh->prepare("select * from outagelevel");
  $sth->execute;
  my $values = $sth->fetchall_arrayref;
  return $values;
}

sub get_outage_level_text {

  my $outage_level_id = shift @_;

  my $sth = $dbh->prepare("select outageleveltext from outagelevel
                           where outagelevelid = $outage_level_id");
  $sth->execute;
  my $values = $sth->fetchrow_array;
  $sth->finish;
  return $values;

}

sub get_outage_item_types {
  my $sth = $dbh->prepare("select * from outageitemtype");
  $sth->execute;
  my $values = $sth->fetchall_arrayref;
  return $values;
}

sub get_outage_item_type_text {

  my $outage_item_type_id = shift @_;

  my $sth = $dbh->prepare("select outageitemtypetext from outageitemtype 
                           where outageitemtypeid = $outage_item_type_id");
  $sth->execute;
  my $values = $sth->fetchrow_array;
  $sth->finish;
  return $values;

}

sub get_outage_notify_areas {
  my $outage_item_type_id = shift @_;

 
  my $sth = $dbh->prepare
    ("select outagenotifyareaid, outagenotifyareatext
      from outagenotifyarea
      where outagenotifyareaid >= 
         (SELECT outagenotifyarealimitid
          FROM outageitemtype
          WHERE outageitemtypeid = $outage_item_type_id)");
  $sth->execute;
  my $values = $sth->fetchall_arrayref;

 
  return $values;
}

sub get_outage_notify_area_text {
  my $outage_notify_area_id = shift @_;

  my $sth = $dbh->prepare("select * from outagenotifyarea
                           where outagenotifyareaid = $outage_notify_area_id");

  $sth->execute;
  my $values = $sth->fetchrow_array;
  $sth->finish;
  return $values;
}


sub get_outage_items {
  # BAD PROGRAMMING ALERT!!!
  # Due to the hacking nature of this program
  # this section has to be hardcoded with values.
  # I don't like it but until routers, links and systems are 
  # put in a central database I don't really have a choice.
  my $outage_type_id = shift @_;
  my @items;

  if ($outage_type_id == 1) {
    # router
    
    # extract from routers.list

    my $routers = CMS_ROUTERS_DB::get_router("ALL");

    foreach my $router (@$routers) {
      push @items, [$$router[0], $$router[0]];
    }
    

 
  } elsif ($outage_type_id ==2) {
    # link
    
    my $links = CMS_LINKS_DB::search_links();

    foreach my $link (@$links) {
      push @items, ["$$link[6] (from $$link[3])","$$link[6] (from $$link[3])"];
    }
  


  } elsif ($outage_type_id ==3) {
    # system
    # The list of systems is stored in the file:
    # cms/config/systems.list

    open (SYSTEMS, "<../config/systems.list");
    while (my $line = <SYSTEMS>) {
      chomp $line;
				  
      push @items, [$line, $line];
    }
    close SYSTEMS;
    
  }

  return \@items;

}


sub add_outage_notice {

  
  my $username = $dbh->quote(shift @_);
  my $startdate_day = shift @_;
  my $startdate_month = shift @_;
  my $startdate_year = shift @_;
  my $startdate_hour = shift @_;
  my $startdate_minute = shift @_;

  my $enddate_day = shift @_;
  my $enddate_month = shift @_;
  my $enddate_year = shift @_;
  my $enddate_hour = shift @_;
  my $enddate_minute = shift @_;

  my $outagetypeid = shift @_;
  my $outagelevelid = shift @_;
  my $outageitemtypeid = shift @_;
  my $outageitem = $dbh->quote(shift @_);
  my $outagenotifyareaid = shift @_;
  my $outagestatusid = 1;   # HARD CODING WARNING!!!!

  my $cust_comment = $dbh->quote(shift @_);
  my $staff_comment = $dbh->quote(shift @_);

  # format dates in ISO-8601 standard
  # (YYYY-MM-DD HH:MM:SS[+/-]XX where XX is the difference from UTC)
  
  my $starttime = 
    $dbh->quote(sprintf("%04d-%02d-%02d %02d:%02d:00", 
			$startdate_year, $startdate_month, $startdate_day, 
			$startdate_hour, $startdate_minute));

  my $endtime = 
    $dbh->quote(sprintf("%04d-%02d-%02d %02d:%02d:00", 
			$enddate_year, $enddate_month, $enddate_day, 
			$enddate_hour, $enddate_minute));
  

  my @now = gmtime;
  # determine ticket number without final two digits (they are zero for now)
  my $ticketno = sprintf("%04d%02d%02d", 1900+$now[5],$now[4]+1, $now[3])*100;


  # all of todays 
  my $sth = $dbh->prepare("select max(ticketno) from outage
                           where ticketno > $ticketno");
  $sth->execute;
  my $ticketnos = $sth->fetchall_arrayref;
  $sth->finish;
  if (defined $$ticketnos[0][0]) {
    $ticketno = $$ticketnos[0][0];
  }
  $ticketno++;
  
  $sth = $dbh->prepare("insert into outage (ticketno, username, 
                                            starttime, endtime,
                                            reported, 
                                            lastupdated, 
                                            outagetypeid, outagelevelid, 
                                            outageitemtypeid, outageitem, 
                                            outagenotifyareaid, 
                                            outagestatusid) 
                            values ($ticketno, $username,  
                                    $starttime, $endtime, 
                                    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,
                                    $outagetypeid, $outagelevelid, 
                                    $outageitemtypeid, $outageitem, 
                                    $outagenotifyareaid, 
                                    $outagestatusid)");

  if ($sth->execute) {
    add_customer_comment($ticketno, $username, $cust_comment,);
    add_staff_comment($ticketno, $username, $staff_comment);
  }
  return $ticketno;

}
 

sub update_outage {
  # BAD PROGRAMMING ALERT!!!!!!!!!!!!!!! FIX
  # this updates the dates regardless of whether they have changed or not. In the later releases
  # some checks should be employed. Actually the DB overhead is probably negligible anyway. :) PM

   
  my $ticketno = shift @_;
  my $username = $dbh->quote(shift @_);
  my $startdate_day = shift @_;
  my $startdate_month = shift @_;
  my $startdate_year = shift @_;
  my $startdate_hour = shift @_;
  my $startdate_minute = shift @_;
  my $enddate_day = shift @_;
  my $enddate_month = shift @_;
  my $enddate_year = shift @_;
  my $enddate_hour = shift @_;
  my $enddate_minute = shift @_;
 
  my $outagestatusid = shift @_;
  my $cust_comment = $dbh->quote(shift @_);
  my $staff_comment = $dbh->quote(shift @_);


  my $starttime = 
    $dbh->quote(sprintf("%04d-%02d-%02d %02d:%02d:00", 
			$startdate_year, $startdate_month, $startdate_day, 
			$startdate_hour, $startdate_minute));

  my $endtime = 
    $dbh->quote(sprintf("%04d-%02d-%02d %02d:%02d:00", 
			$enddate_year, $enddate_month, $enddate_day, 
			$enddate_hour, $enddate_minute));



  my $sth = $dbh->prepare("update outage
                    set starttime  = $starttime,
                        endtime   = $endtime,
                        lastupdated = CURRENT_TIMESTAMP,
                        outagestatusid = $outagestatusid
                    where ticketno = $ticketno");

  

  if ($sth->execute) {
   
    if ($cust_comment ne "''") {add_customer_comment($ticketno, $username, $cust_comment);}
    if ($staff_comment ne "''") {add_staff_comment($ticketno, $username, $staff_comment);}
  }

} 



sub get_outage_list {

  my $ticketno = shift @_ || "";
  my $starttime_start = shift @_ || "";
  my $starttime_end = shift @_ || "";
  my $username = shift @_ || "";
  my $outageitemtype = shift @_ || "";
  my $outageitem = shift @_ || "";
  my $outagestatus = shift @_ || "";

  
  my $command = "SELECT ticketno, starttime, username, 
             outageitemtypetext, outageitem, outagestatustext
      FROM outage
      JOIN outageitemtype USING (outageitemtypeid)
      JOIN outagestatus   USING (outagestatusid) ";

  if ($starttime_start ne "") {
    $command .= "AND starttime >= '$starttime_start' 
                       AND starttime <= '$starttime_end'";
  } 
  
  if ($ticketno ne "") {
    $command .= "AND ticketno ilike '%$ticketno%' ";
  }
  if ($username ne "") {
    $command .= "AND username ilike '%$username%' ";
  }
  if ($outageitemtype ne "") {
    $command .= "AND outageitemtypeid = $outageitemtype ";
  } 
  if ($outageitem ne "") {
    $command .= "AND outageitem ilike '%$outageitem%' ";
  } 
  if ($outagestatus ne "") {
    $command .= "AND outagestatusid = $outagestatus ";
  }

  $command =~ s/AND/WHERE/; # change first AND to WHERE

  $command .= "ORDER BY starttime";

 
  my $sth = $dbh->prepare($command);
  $sth->execute;

  my $outages = $sth->fetchall_arrayref;

  $sth->finish;
 
  return $outages;  
}


sub get_current_outages {
  # THIS IS FOR OLSS

  my $sth = $dbh->prepare
    ("SELECT ticketno, starttime, endtime, outageitem
       FROM OUTAGE
       WHERE outagestatusid = 1
       ORDER BY starttime");
  $sth->execute;

  if (my $outage = $sth->fetchall_arrayref) {
    $sth->finish;

    foreach my $outage (@$outage) {
      
      
      $sth = $dbh->prepare 
	("SELECT comments FROM outagecustomercomments
          WHERE ticketno = $$outage[0]");

      $sth->execute;

      my $comments = $sth->fetchall_arrayref;
      $sth->finish;

      my $concat_comments = "";
      foreach my $comment (@$comments) {
	$concat_comments .= "$$comment[0]^^";
      }
      
      $$outage[4] = $concat_comments;
    }
    $error = $dbh->errstr;
    return $outage;
  } else {
    $sth->finish;
    $error = $dbh->errstr;
    return undef;
  }
}

sub get_outage_notice {
  ;
  my $ticketno = shift @_;

  my $sth = $dbh->prepare
    ("SELECT username, starttime, endtime, reported, lastupdated, 
             outagetypetext, outageleveltext, outageitemtypetext, 
             outageitem, outagenotifyareatext, outagestatusid, 
             outagestatustext 
      FROM outage 
      
      NATURAL JOIN outagetype 
      NATURAL JOIN outagelevel 
      NATURAL JOIN outageitemtype 
      NATURAL JOIN outagenotifyarea 
      NATURAL JOIN outagestatus 
      WHERE ticketno = $ticketno
" );

 
  $sth->execute;
 
  if (my $outage = $sth->fetchrow_arrayref) {
    # reformat dates
    $sth->finish;

    return $outage;
  } else {
    $sth->finish;

    return undef;
  }
 
  
}

sub add_customer_comment {
  
  my $ticketno = shift @_;
  my $username = shift @_; # this must be quoted
  my $comments = shift @_; # this must be quoted

 
  my $sth = 
    $dbh->prepare
      ("insert into outagecustomercomments
           (ticketno, comments, username, timestamp)
           values ($ticketno, $comments, $username, CURRENT_TIMESTAMP)");
  $sth->execute;

}


sub get_customer_comments {
  my $ticketno = shift @_;
  my $sth = 
    $dbh->prepare("select timestamp,username,comments 
                           from outagecustomercomments
                           where ticketno = $ticketno");
  $sth->execute;
  my $values = $sth->fetchall_arrayref;
 
  return $values;
}

sub add_staff_comment {
 
  my $ticketno = shift @_;
  my $username = shift @_; # this must be quoted
  my $comments = shift @_; # this must be quoted
 
  my @now = gmtime(time);

  my $sth = 
    $dbh->prepare("insert into outagestaffcomments
                       (ticketno, comments, username, timestamp)
                       values ($ticketno, $comments, $username, CURRENT_TIMESTAMP)");
  
  $sth->execute;


}


sub get_staff_comments {
  ;
  my $ticketno = shift @_;
  my $sth = $dbh->prepare("select timestamp,username,comments 
                           from outagestaffcomments
                           where ticketno = $ticketno");
  $sth->execute;
  my $values = $sth->fetchall_arrayref;
 
  return $values;
}



######################
# Basic table subroutines

sub add_outage_type {
  my $outagetypeid = shift @_;
  my $outagetypetext = $dbh->quote(shift @_);

  # add outage_type
  my $sth = $dbh->prepare("insert into outagetype(outagetypeid, outagetypetext) 
                           values($outagetypeid, $outagetypetext)");
  $sth->execute;

 
}

sub add_outage_level {
  my $outagelevelid = shift @_;
  my $outageleveltext = $dbh->quote(shift @_);

  # add outage_level
  my $sth = $dbh->prepare("insert into outagelevel(outagelevelid, outageleveltext) 
                           values($outagelevelid, $outageleveltext)");
  $sth->execute;

 
}

sub add_outage_status {
  my $outagestatusid = shift @_;
  my $outagestatustext = $dbh->quote(shift @_);

  # add outage_status
  my $sth = $dbh->prepare("insert into outagestatus(outagestatusid, outagestatustext) 
                           values($outagestatusid, $outagestatustext)");
  $sth->execute;

 
}

sub add_outage_notify_area {
  my $outagenotifyareaid = shift @_;
  my $outagenotifyareatext = $dbh->quote(shift @_);

  # add outage_notify_area
  my $sth = $dbh->prepare("insert into outagenotifyarea(outagenotifyareaid, outagenotifyareatext) 
                           values($outagenotifyareaid, $outagenotifyareatext)");
  $sth->execute;

 
}

sub add_outage_item_type {
  my $outageitemtypeid = shift @_;
  my $outageitemtypetext = $dbh->quote(shift @_);
  my $outagenotifyareaid = shift @_;

  # add outage_item_type
  my $sth = $dbh->prepare("insert into outageitemtype
                             (outageitemtypeid, outageitemtypetext,outagenotifyarealimitid) 
                           values($outageitemtypeid, $outageitemtypetext, $outagenotifyareaid)");
  $sth->execute;

 
}

sub get_outage_contacts_for_all {

  my $sth = $dbh->prepare
    ("SELECT firstname || surname, email
      FROM contact
      NATURAL JOIN contact_type 
      WHERE type = 'outage'");

  

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  
  $error = $dbh->errstr;
  $sth->finish;
  return $result;

}

sub get_outage_contacts_for_router {
  my $router = $dbh->quote(shift @_);
  my $outagenotifyareaid = shift @_;


  # get the pop, city and region of the router
  my $sth = $dbh->prepare
    ("SELECT popname, cityname, regionname
      FROM routername
      NATURAL JOIN popname
      NATURAL JOIN cityname
      NATURAL JOIN regionname
      WHERE routername = $router");

  $sth->execute;
  
  my ($pop, $city, $region) = $sth->fetchrow_array;
  $sth->finish;

  
  my $command = 
    "SELECT firstname || surname, email
     FROM contact
     NATURAL JOIN contact_type 
     NATURAL JOIN customer
     NATURAL JOIN service
     INNER JOIN link USING (serviceid) ";

  if ($outagenotifyareaid == 1) {
    # router

    $command .= "NATURAL JOIN routername
                 WHERE routername = $router
                 AND type = 'outage' ";

  } elsif ($outagenotifyareaid == 2) {
    # pop

    $command .= "NATURAL JOIN routername
                 NATURAL JOIN popname
                 WHERE popname = '$pop' 
                 AND type = 'outage' ";

  } elsif ($outagenotifyareaid == 3) {
    # city

    $command .= "NATURAL JOIN routername
                 NATURAL JOIN popname
                 NATURAL JOIN cityname
                 WHERE cityname = '$city' 
                 AND type = 'outage' ";

  } elsif ($outagenotifyareaid == 4) {
    # region

    $command .= "NATURAL JOIN routername
                 NATURAL JOIN popname
                 NATURAL JOIN cityname
                 NATURAL JOIN regionname
                 WHERE regionname = '$region' 
                 AND type = 'outage'";

  } elsif ($outagenotifyareaid == 5) {
    # all

    $command .= "WHERE type = 'outage'";

  }


  $sth = $dbh->prepare ($command);


  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  
  $error = $dbh->errstr;
  $sth->finish;
  return $result;
  
}

1;
