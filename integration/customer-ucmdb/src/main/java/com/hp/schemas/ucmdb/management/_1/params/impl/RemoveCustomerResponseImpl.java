/*
 * XML Type:  removeCustomerResponse
 * Namespace: http://schemas.hp.com/ucmdb/management/1/params
 * Java type: com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.management._1.params.impl;
/**
 * An XML removeCustomerResponse(@http://schemas.hp.com/ucmdb/management/1/params).
 *
 * This is a complex type.
 */
public class RemoveCustomerResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse
{
    
    public RemoveCustomerResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
