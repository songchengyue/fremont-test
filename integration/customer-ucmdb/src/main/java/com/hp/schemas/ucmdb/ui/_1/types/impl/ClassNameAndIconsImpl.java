/*
 * XML Type:  ClassNameAndIcons
 * Namespace: http://schemas.hp.com/ucmdb/ui/1/types
 * Java type: com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.ui._1.types.impl;
/**
 * An XML ClassNameAndIcons(@http://schemas.hp.com/ucmdb/ui/1/types).
 *
 * This is a complex type.
 */
public class ClassNameAndIconsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons
{
    
    public ClassNameAndIconsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLASSNAMEANDICON$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/ui/1/types", "classNameAndIcon");
    
    
    /**
     * Gets array of all "classNameAndIcon" elements
     */
    public com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon[] getClassNameAndIconArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CLASSNAMEANDICON$0, targetList);
            com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon[] result = new com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "classNameAndIcon" element
     */
    public com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon getClassNameAndIconArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon target = null;
            target = (com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon)get_store().find_element_user(CLASSNAMEANDICON$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "classNameAndIcon" element
     */
    public int sizeOfClassNameAndIconArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CLASSNAMEANDICON$0);
        }
    }
    
    /**
     * Sets array of all "classNameAndIcon" element
     */
    public void setClassNameAndIconArray(com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon[] classNameAndIconArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(classNameAndIconArray, CLASSNAMEANDICON$0);
        }
    }
    
    /**
     * Sets ith "classNameAndIcon" element
     */
    public void setClassNameAndIconArray(int i, com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon classNameAndIcon)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon target = null;
            target = (com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon)get_store().find_element_user(CLASSNAMEANDICON$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(classNameAndIcon);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "classNameAndIcon" element
     */
    public com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon insertNewClassNameAndIcon(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon target = null;
            target = (com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon)get_store().insert_element_user(CLASSNAMEANDICON$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "classNameAndIcon" element
     */
    public com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon addNewClassNameAndIcon()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon target = null;
            target = (com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon)get_store().add_element_user(CLASSNAMEANDICON$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "classNameAndIcon" element
     */
    public void removeClassNameAndIcon(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CLASSNAMEANDICON$0, i);
        }
    }
}
