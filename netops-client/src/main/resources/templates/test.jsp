<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!doctype html>
<html lang="en">
<head>
   <title>Test Netops Hosting Portal</title>
   
    <script type="text/javascript" src="<c:url value="/js/jquery-2.1.4.js"/>"></script>
   	<script type="text/javascript">
		
    $(document).ready(function() {

        //
        // This is roughly how C3, the real Netops host portal, integrates with Netops
        //

        $("a.post").click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            var params = new Object();
            params['CustomerId'] = $("#customerId").val();
            params['redirect'] = $("#redirect").val();
            params['productLine'] = $("#productLine").val();
            var inputs = '';
            for(var key in params) {
            	inputs += '<input type="hidden" name="' + key + '" value="' + params[key] + '" />';
            }
            $("body").append('<form action="/netops-client-1.0/" target="_self" method="POST" id="poster">'+inputs+'</form>');    //this is where the form is created.
            $("#poster").submit();
        });
    });
		
	</script>
</head>
<body>
<p>Welcome to the Test Netops Hosting Portal. This is a site for testing the integration between Netops and its host portal!</p>
<p>Enter Customer Id for Testing: <input id="customerId" name="customerId" type="text" value="14264"></input></p>
<p>Enter Redirect:
<select id="redirect">
  <option value="NONE">--None--</option>
  <option value="LG_NodeTest">LookingGlass - Node Test</option>
  <option value="LG_NodeHostTest">LookingGlass - Node Host Test</option>
  <option value="LG_NodeHostTrace">LookingGlass - Node Host Trace</option>
  <option value="DNS_PrimaryDnsAdd">DNS - Primary Add Domain</option> 
  <option value="DNS_PrimaryDnsView">DNS - Primary View Domain</option>
  <option value="DNS_SecondaryDnsAdd">DNS - Secondary Add Domain</option>
  <option value="DNS_SecondaryDnsView">DNS - Secondary view Domain</option>
  <option value="DNS_SecondaryDnsRemove">DNS - Secondary Remove Domain</option>
  <option value="DNS_DnsResolverAdd">DNS - Resolver Add Domain</option>
  <option value="DNS_DnsResolverView">DNS - Resolver View Domain</option>
  <option value="DNS_ReverseDelegationAdd">DNS - Reverse Delegation Add Domain</option>
  <option value="DNS_ReverseMappingAdd">DNS - Reverse Mapping Add Domain</option>
  <option value="DNS_ReverseDelegationAndMappingView">DNS - Reverse Delegation And Mapping View Domain</option>
</select>
</p>
<p>Enter Product Line:
<select id="productLine">
  <option value="NONE">--None--</option>
  <option value="IPT">IPT</option>
  <option value="GID">GID</option> 
</select>
</p>
<p><a class="post" href="/netops-client-1.0/">Test!</a></p>
</body>
</html>
