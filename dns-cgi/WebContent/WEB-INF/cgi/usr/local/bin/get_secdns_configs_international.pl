#!/usr/bin/perl

# INSTALL ON SECONDARY NAMESERVERS.

use strict;
use File::Copy;


my $file = "secondary";

my $time = time;

#rename ("/etc/namedb/customers/$file","/etc/namedb/customers/$file.$time");
#rename ("/usr/local/www/wanchai/netops/data/sec_dns/$file","/usr/local/www/wanchai/netops/data/sec_dns/$file.$time");
#my $res = `/usr/bin/rsync -azlv wanchai::secdns /etc/namedb/customers > /var/log/secdns.rsync`;
#my $res = `/usr/bin/rsync -azlv -e "ssh -p 22" wanchai::secdns /etc/namedb/customers > /var/log/secdns.rsync`;
#my $res= `usr/bin/rsync -azlv --include 'secondary' --exclude '*' hhtred274::secdns /etc/namedb/customers > /var/log/secdns.rsync`;
my $res = `/usr/bin/rsync -azlv hhtred278::secdns_international /etc/namedb/international/secondary/customers > /var/log/secdns_international.rsync`;
rename ("/etc/namedb/international/secondary/customers/$file","/etc/namedb/international/secondary/customers/$file.$time");

copy ("/etc/namedb/named.conf.template", "/etc/namedb/international/secondary/named.conf.new");
#copy ("/etc/namedb/named.conf", "/etc/namedb/backup/named.conf.$time");
#copy ("/etc/namedb/named.conf", "/etc/namedb/backup/named.conf");
`cat /etc/namedb/international/secondary/customers/$file.$time >> /etc/namedb/international/secondary/named.conf.new`;
rename ("/etc/namedb/international/secondary/named.conf.new","/etc/namedb/international/secondary/named.conf");
copy ("/etc/namedb/international/secondary/named.conf", "/etc/namedb/international/secondary/backup/named.conf.$time");

copy("/etc/namedb/customers/service.list", "/data1/ftp/adams/service.list");

# remove out-dated file
#`/usr/bin/find /etc/namedb/customers -type f -mtime +10 -print | xargs rm -f`;
#`/usr/bin/find /etc/namedb/backup -type f -mtime +10 -print | xargs rm -f`;
`/usr/bin/find /etc/namedb/international/secondary/customers -type f -mtime +20 -print | xargs rm -f`;
`/usr/bin/find /etc/namedb/international/secondary/backup -type f -mtime +20 -print | xargs rm -f`;
