/*
 * XML Type:  releaseChunksResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML releaseChunksResponse(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class ReleaseChunksResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse
{
    
    public ReleaseChunksResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
