/*
 * XML Type:  DateConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.DateConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML DateConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class DateConditionsImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.DateConditions
{
    
    public DateConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DATECONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "dateCondition");
    
    
    /**
     * Gets array of all "dateCondition" elements
     */
    public com.hp.schemas.ucmdb._1.types.DateCondition[] getDateConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DATECONDITION$0, targetList);
            com.hp.schemas.ucmdb._1.types.DateCondition[] result = new com.hp.schemas.ucmdb._1.types.DateCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "dateCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.DateCondition getDateConditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateCondition)get_store().find_element_user(DATECONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "dateCondition" element
     */
    public int sizeOfDateConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DATECONDITION$0);
        }
    }
    
    /**
     * Sets array of all "dateCondition" element
     */
    public void setDateConditionArray(com.hp.schemas.ucmdb._1.types.DateCondition[] dateConditionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(dateConditionArray, DATECONDITION$0);
        }
    }
    
    /**
     * Sets ith "dateCondition" element
     */
    public void setDateConditionArray(int i, com.hp.schemas.ucmdb._1.types.DateCondition dateCondition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateCondition)get_store().find_element_user(DATECONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(dateCondition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "dateCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.DateCondition insertNewDateCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateCondition)get_store().insert_element_user(DATECONDITION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "dateCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.DateCondition addNewDateCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateCondition)get_store().add_element_user(DATECONDITION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "dateCondition" element
     */
    public void removeDateCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DATECONDITION$0, i);
        }
    }
}
