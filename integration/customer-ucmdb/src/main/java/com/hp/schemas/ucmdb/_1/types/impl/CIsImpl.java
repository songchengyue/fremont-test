/*
 * XML Type:  CIs
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.CIs
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML CIs(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class CIsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.CIs
{
    
    public CIsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CI$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "CI");
    
    
    /**
     * Gets array of all "CI" elements
     */
    public com.hp.schemas.ucmdb._1.types.CI[] getCIArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CI$0, targetList);
            com.hp.schemas.ucmdb._1.types.CI[] result = new com.hp.schemas.ucmdb._1.types.CI[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "CI" element
     */
    public com.hp.schemas.ucmdb._1.types.CI getCIArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CI target = null;
            target = (com.hp.schemas.ucmdb._1.types.CI)get_store().find_element_user(CI$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "CI" element
     */
    public int sizeOfCIArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CI$0);
        }
    }
    
    /**
     * Sets array of all "CI" element
     */
    public void setCIArray(com.hp.schemas.ucmdb._1.types.CI[] ciArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(ciArray, CI$0);
        }
    }
    
    /**
     * Sets ith "CI" element
     */
    public void setCIArray(int i, com.hp.schemas.ucmdb._1.types.CI ci)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CI target = null;
            target = (com.hp.schemas.ucmdb._1.types.CI)get_store().find_element_user(CI$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(ci);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "CI" element
     */
    public com.hp.schemas.ucmdb._1.types.CI insertNewCI(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CI target = null;
            target = (com.hp.schemas.ucmdb._1.types.CI)get_store().insert_element_user(CI$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "CI" element
     */
    public com.hp.schemas.ucmdb._1.types.CI addNewCI()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CI target = null;
            target = (com.hp.schemas.ucmdb._1.types.CI)get_store().add_element_user(CI$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "CI" element
     */
    public void removeCI(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CI$0, i);
        }
    }
}
