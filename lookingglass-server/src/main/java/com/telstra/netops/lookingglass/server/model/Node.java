package com.telstra.netops.lookingglass.server.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.telstra.netops.lookingglass.server.model.jpa.IPVersionConverter;

@Entity
@Table(name = "Looking_Glass_Node")
public class Node implements Serializable {

	@Id
	@GeneratedValue
	@Column(name = "Node_ID")
	private Long id;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="Pop_City_ID")
	private City city;
	 
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="Pop_Country_ID")
	private Country country;
	
	@Convert(converter = IPVersionConverter.class)
	@Column(name = "IP_Version")
	private IPVersion ipVersion;
	
	@Column(name = "IP_Address")
	private String ipAddress;
	
	protected Node() {		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public IPVersion getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(IPVersion ipVersion) {
		this.ipVersion = ipVersion;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@JsonProperty
	public String getCityAndCountryFormatted() {
		return city.getName()  + ", " + country.getName();
	}
	
	
}
