/*
 * XML Type:  DoubleParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.DoubleParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML DoubleParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class DoubleParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.DoubleParams
{
    
    public DoubleParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DOUBLEPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "doubleParam");
    
    
    /**
     * Gets array of all "doubleParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.DoubleProp[] getDoubleParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DOUBLEPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.DoubleProp[] result = new com.hp.schemas.ucmdb._1.types.DoubleProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "doubleParam" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleProp getDoubleParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().find_element_user(DOUBLEPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "doubleParam" element
     */
    public int sizeOfDoubleParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DOUBLEPARAM$0);
        }
    }
    
    /**
     * Sets array of all "doubleParam" element
     */
    public void setDoubleParamArray(com.hp.schemas.ucmdb._1.types.DoubleProp[] doubleParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(doubleParamArray, DOUBLEPARAM$0);
        }
    }
    
    /**
     * Sets ith "doubleParam" element
     */
    public void setDoubleParamArray(int i, com.hp.schemas.ucmdb._1.types.DoubleProp doubleParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().find_element_user(DOUBLEPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(doubleParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "doubleParam" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleProp insertNewDoubleParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().insert_element_user(DOUBLEPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "doubleParam" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleProp addNewDoubleParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().add_element_user(DOUBLEPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "doubleParam" element
     */
    public void removeDoubleParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DOUBLEPARAM$0, i);
        }
    }
}
