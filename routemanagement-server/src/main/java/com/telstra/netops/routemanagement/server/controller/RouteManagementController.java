package com.telstra.netops.routemanagement.server.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.telstra.netops.routemanagement.server.model.CustomerRouterDataServiceDTO;
import com.telstra.netops.routemanagement.server.model.DeletePrefixRecord;
import com.telstra.netops.routemanagement.server.model.PrefixRecord;
import com.telstra.netops.routemanagement.server.model.PrefixUploadRequest;
import com.telstra.netops.routemanagement.server.model.RouterInformationDTO;
import com.telstra.netops.routemanagement.server.model.Service;
import com.telstra.netops.routemanagement.server.model.Success;
import com.telstra.netops.routemanagement.server.service.ServiceListService;
import com.telstra.netops.routemanagement.server.service.MultiplePrefixUploadService;
import com.telstra.netops.routemanagement.server.service.PrefixService;
import com.telstra.netops.routemanagement.server.service.SinglePrefixAddService;
import com.telstra.oauth2.client.CustomerIdResolver;
import com.telstra.oauth2.client.UserProfileResolver;

/**
 * Main controller class for Route Management module
 *
 */
@Controller
public class RouteManagementController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RouteManagementController.class.getName());

	@Autowired
	HttpServletRequest request;

	@Autowired
	private ServiceListService displayServiceListService;
	@Autowired
	private HttpSession httpSession;

	@Autowired
	private CustomerIdResolver customerIdResolver;

	@Autowired
	private SinglePrefixAddService singlePrefixUploadService;

	@Autowired
	private MultiplePrefixUploadService multiplePrefixUploadService;

	@Autowired
	private PrefixService viewPrefixListService;
	
    @Autowired
    private UserProfileResolver userProfileResolver;



	/**
	 * 
	 * This fetches and Displays the AS Number- Service list of the customer.
	 * 
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/service", method = RequestMethod.GET)
	public @ResponseBody List<CustomerRouterDataServiceDTO> getCustomerServiceList(
			@RequestParam(value = "productLine") String productLine, Principal principal) {
		LOGGER.info("in Routemanagement service list details controller");
		List<CustomerRouterDataServiceDTO> serviceListData = displayServiceListService
				.fetchServiceDetails(customerIdResolver.resolve(principal), productLine);
		return serviceListData;
	}

	/**
	 * 
	 * Adds the single prefix IPv4 record which is entered by the customer.
	 * 
	 * @param request
	 * @param principal
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/singleprefix", method = RequestMethod.POST)
	public @ResponseBody Success singlePrefixUpload(@RequestBody PrefixUploadRequest request,
			@RequestParam(value = "service") String service,
			Principal principal) throws ServletException, IOException {
		request.setService(service);
		request.setUserProfile(userProfileResolver.resolve(principal));
		request.setCustomerId(customerIdResolver.resolve(principal));
		Success success = singlePrefixUploadService.upload(request);
		return success;
	}

	/**
	 * 
	 * Uploads the bulk prefix file for IPv4.
	 * 
	 * @param file
	 * @param data
	 * @param principal
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/multipleprefix", method = RequestMethod.POST)
	public @ResponseBody Success multiplePrefixUpload(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "props") Object data, @RequestParam(value = "service") String service,
			Principal principal) throws Exception {

		Gson gson = new Gson();
		PrefixUploadRequest request = gson.fromJson(data.toString(), PrefixUploadRequest.class);
		ByteArrayInputStream stream = new ByteArrayInputStream(file.getBytes());
		String fileContentString = IOUtils.toString(stream, "UTF-8");
		fileContentString = fileContentString.replaceAll(System.getProperty("line.separator"), ",");
		request.setFileContent(fileContentString);
		request.setCustomerId(customerIdResolver.resolve(principal));
		request.setService(service);
		request.setUserProfile(userProfileResolver.resolve(principal));
		System.out.println("PINKYY user profile is "+userProfileResolver.resolve(principal));
		Success success = multiplePrefixUploadService.upload(request);
		return success;
	}

	/**
	 * Displays the list of Prefixes added for that customer
	 * 
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/prefix", method = RequestMethod.GET)
	public @ResponseBody List<PrefixRecord> viewPrefixList(@RequestParam(value = "service") String service,
			Principal principal) {
		String customerId = customerIdResolver.resolve(principal);
		PrefixUploadRequest request = new PrefixUploadRequest();
		LOGGER.debug("resolverCustomerId: " + customerId);
		request.setCustomerId(customerIdResolver.resolve(principal));
		request.setService(service);
		return viewPrefixListService.list(request);
	}

	/**
	 * Deletes the list of prefix records selected by the customer
	 * 
	 * @param ids
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/prefix", method = RequestMethod.DELETE)
	public @ResponseBody Success deletePrefixList(@RequestParam("ids") String ids,
			@RequestParam(value = "service") String service, Principal principal) {
		LOGGER.debug("in Primary delete");
		DeletePrefixRecord request = new DeletePrefixRecord();
		request.setCustomerId(customerIdResolver.resolve(principal));
		request.setService(service);
		request.setIds(ids.split(","));
		Success success = viewPrefixListService.remove(request);
		return success;
	}

	/**
	 * 
	 * This fetches and Displays the Serviceid, as number, product type, black
	 * hole router, black hole neighbor ip, router version, router name, bgp
	 * neighbor ip
	 * 
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/routerinformation", method = RequestMethod.GET)
	public @ResponseBody List<RouterInformationDTO> fetchRouterInformation(
	@RequestParam(value = "productLine") String productLine, Principal principal) {

		List<RouterInformationDTO> routerInformationList = displayServiceListService
				.viewRouterDetails(customerIdResolver.resolve(principal), productLine);
		return routerInformationList;
	}

	/**
	 * 
	 * This updates the Product type, black hole router,irrd object black hole
	 * neighbor ip, router software version, router name, bgp neighbor ip fields
	 * 
	 * @param principal
	 * @return
	 */

	@RequestMapping(value = "/routerinformation", method = RequestMethod.PUT)
	public @ResponseBody Success editRouterInformation(@RequestBody RouterInformationDTO request, Principal principal)
			throws ServletException, IOException {
		request.setCustomerId(customerIdResolver.resolve(principal));
		Success success = displayServiceListService.editRouterInformation(request);
		return success;
	}

}