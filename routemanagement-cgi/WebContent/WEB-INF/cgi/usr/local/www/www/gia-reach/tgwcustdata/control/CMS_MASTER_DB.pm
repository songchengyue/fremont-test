#! /usr/local/bin/perl
########################################################################
## File        : CMS_MASTER_DB.pm
# Description : Perl Module to handle the DB queries for Master Services
# Parameters to be passed : Master Service ID
# # Modification History :
#  Date               Name                 Change/Description
# ------------------------------------------------------------------
# 09-SEP-2010	Karuna Ballal		Modified for IP Transit
# 28-SEP-2010	Chandini 		Modified for IP Transit
#######################################################################

package CMS_MASTER_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
use CMS_ROUTING_QUEUE_DB;

our $error;
sub add_master_service {
  CMS_DB::begin_transaction(); 
  my $master_serviceid_raw = shift @_;
  my $master_serviceid = $dbh->quote($master_serviceid_raw);
  my $description = $dbh->quote(shift @_);
  my $success = 1;

  #Added by Karuna for IP Transit
  #Chandini_IPT
  my $ipc_service = $dbh->quote($_[0]);
  my $costype = $dbh->quote($_[1]);
  my $ratetype = $dbh->quote($_[2]);
  my $curr_acdr = $_[3];

  my $sth = $dbh->prepare ("SELECT master_serviceid FROM master_service WHERE master_serviceid ilike $master_serviceid");
  $sth->execute();
  my $r = $sth->fetchrow_hashref;
  my $temp_master_serviceid = $master_serviceid;
  $temp_master_serviceid =~ s/\'//g;
  if($$r{master_serviceid} =~ /^$temp_master_serviceid$/i) {
        return "Duplicated Master ServiceID.";
  }

  
 my $sth1 = $dbh->prepare ("INSERT INTO master_service (master_serviceid, description) VALUES($master_serviceid, $description)");


 $success &&= $sth1->execute;
 $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

 if ($error) {
	return $error;
 } else {
  	#Added by Karuna for IP Transit
#removed ipm keyword location fix for insert delete and update-TGC0032462
  	if ($master_serviceid =~ /ipm/i) {
			if ($curr_acdr eq ""){
				$curr_acdr = "null";
			}
			#Chandini_IPT
  			$sth = $dbh->prepare ("INSERT INTO ipt_master (master_sid, cos_type, rate_type, current_acdr, current_stdt,ipc_servid) VALUES ($master_serviceid, $costype, $ratetype, $curr_acdr, current_date, $ipc_service)");
			$success &&= $sth->execute;
			$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
			return $error if ($error);
	}  	
 }
 return $success;
 # return "1";
}

sub get_master_serviceid {
  
  my $sth = $dbh->prepare("SELECT master_serviceid FROM master_service where master_serviceid<>'' ORDER BY master_serviceid");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  
  if($result->[0]) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } 
  else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub get_allmaster_serviceid {
  
  my $sth = $dbh->prepare("SELECT master_serviceid FROM master_service ORDER BY master_serviceid");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  
  if($result->[0]) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } 
  else {
    $error = $dbh->errstr;
    return 0;
  }
}


sub get_description {
  my $master_serviceid = $dbh->quote(shift @_);

  #Added for IP Transit by Karuna
  my $sth;

  #Added for IP Transit by Karuna
  #Chandini_IPT
  if ($master_serviceid =~ /ipm/i) {
	$sth = $dbh->prepare
	("SELECT master_service.description, ipt_master.ipc_servid, ipt_master.cos_type, ipt_master.rate_type, ipt_master.current_acdr, ipt_master.current_stdt, ipt_master.proposed_acdr, ipt_master.proposed_stdt, ipt_master.proposed_rate  FROM ipt_master left join master_service on ipt_master.master_sid=master_service.master_serviceid WHERE ipt_master.master_sid = $master_serviceid");

  } else {
  	$sth = $dbh->prepare
    	("SELECT description FROM master_service WHERE master_serviceid = $master_serviceid");
  }
  unless ($sth->execute) {
  	$error = $dbh->errstr;
    	return "0";
  }
  my ($result,@result);  

  @result = $sth->fetchrow_array;
  $sth->finish;
  if ($master_serviceid =~ /ipm/i) {
	#return the array reference
	return \@result;
  } else {
	#return the description
	return $result[0];
  }
}

sub update_description {
  my $master_serviceid = $dbh->quote(shift @_);
  my $description = $dbh->quote(shift @_);
  #Chandini_IPT
  my $ipc_service = (shift @_);
  my $costype = $dbh->quote(shift @_);
  my $pro_ratetype = $dbh->quote(shift @_);
  my $acdr_eff = $dbh->quote(shift @_);
  my $pro_date_yr = (shift @_);
  my $pro_date_mth =(shift @_);
  my $pro_date_day =(shift @_);
  my $pro_date = $dbh->quote ($pro_date_yr."-".$pro_date_mth."-".$pro_date_day);
  my $pro_dt_val = $pro_date_yr."-".$pro_date_mth."-".$pro_date_day;
  #System date for IP Transit
  my ($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = gmtime;
  my $year = 1900 + $yearOffset;
  $month = $month + 1;
  if ($month <= 9){
        $month = "0".$month;
  }
  if ($dayOfMonth <= 9){
        $dayOfMonth = "0".$dayOfMonth;
  }
  my $sys_date = $year."-".$month."-".$dayOfMonth;




  my $sth = $dbh->prepare("UPDATE master_service SET description = $description WHERE master_serviceid = $master_serviceid");
  

  unless ($sth->execute) {
    	$error = $dbh->errstr;
    	return 0;
  }

  if ($master_serviceid =~ /ipm/i) {
	my $get_all = $dbh->prepare("SELECT current_stdt, proposed_stdt, rate_type, current_acdr FROM ipt_master WHERE master_sid = $master_serviceid");
	$get_all->execute();
	my $db_arrayref = $get_all->fetchall_arrayref; 
	my $db_curr_stdt = $db_arrayref->[0][0];
	my $db_pro_stdt = $db_arrayref->[0][1];
	my $db_ratetype = $db_arrayref->[0][2];
	my $db_acdr = $db_arrayref->[0][3];
	my $pro_rate_val = $pro_ratetype;
	$pro_rate_val =~ s/\'//g;
	$pro_rate_val =~ s/\s+//g;
	$db_ratetype =~ s/\s+//g;
	my $acdr_eff_val = $acdr_eff; 
	$acdr_eff_val =~ s/\'//g; 
	if ($pro_ratetype =~ /Flat Rate/){
		if ($pro_rate_val eq $db_ratetype){
			$error = "Please select a different Proposed Rate Type.";
			return 0;
		} 
		if ($acdr_eff_val eq ""){
			$acdr_eff = "null";
		}
		#Chandini_IPT
		$sth = $dbh->prepare("UPDATE ipt_master SET cos_type = $costype, proposed_rate = $pro_ratetype, proposed_acdr = $acdr_eff, proposed_stdt = $pro_date, ipc_servid = '$ipc_service' WHERE master_sid = $master_serviceid");

	} elsif ($pro_ratetype =~ /95percentile/){
		#check acdr value in the DB
		my $get_acdr = $dbh->prepare("SELECT current_acdr from ipt_master where current_acdr=$acdr_eff and master_sid = $master_serviceid and rate_type=$pro_ratetype");
		$get_acdr->execute();
		my $acdr_db = $get_acdr->fetchrow_hashref;

		# Entered Effective ACDR cannot be same as the existing current ACDR
		if ($get_acdr->rows > 0){
			$error = "Proposed ACDR is same as the Current ACDR.";
			return 0;
		}

		#Chandini_IPT
		$sth = $dbh->prepare("UPDATE ipt_master SET cos_type = $costype, proposed_rate = $pro_ratetype, proposed_acdr = $acdr_eff, proposed_stdt = $pro_date,ipc_servid = '$ipc_service' WHERE master_sid = $master_serviceid");
	} elsif ($pro_ratetype =~ /Select/){
		#Chandini_IPT
		$sth = $dbh->prepare("UPDATE ipt_master SET cos_type = $costype, proposed_rate = null, proposed_acdr = null, proposed_stdt = null, ipc_servid = '$ipc_service' WHERE master_sid = $master_serviceid");
		#$sth = $dbh->prepare("UPDATE ipt_master SET cos_type = $costype, proposed_rate = null, proposed_acdr = null, proposed_stdt = null WHERE master_sid = $master_serviceid");
	}
	unless ($sth->execute) {
    		$error = $dbh->errstr;
    		return 0;
  	}
  $sth = $dbh->prepare("SELECT customer, as_nos from ipc_china where ipc_servid = '$ipc_service'");
  $sth->execute();
  my @ipc_vals = $sth->fetchrow_array;	
  my $ipc_accno = $ipc_vals[0];
  my $ipc_asno = $ipc_vals[1];
  
  $sth = $dbh->prepare("UPDATE service SET accno='$ipc_accno', as_no = '$ipc_asno' where master_serviceid=$master_serviceid");
  unless ($sth->execute) {
        $error = $dbh->errstr;
 	return 0;
  }
  return 1;
  }

  return 1;
}



sub delete_master {
  my $master_serviceid = $dbh->quote(shift @_);
 
  my $sth_master = $dbh->prepare("DELETE FROM master_service WHERE master_serviceid = $master_serviceid");

  unless ($sth_master->execute) {
  	$error = $dbh->errstr;
    	#return $error;
  }
  if ($error =~ /master_service_fk1/){
	return $error;
  } elsif ($error =~ /ipt_master/){
  	if ($master_serviceid =~ /ipm/) {
		my $sth_ipm = $dbh->prepare("DELETE FROM ipt_master WHERE master_sid = $master_serviceid");
		$error = "";
		unless ($sth_ipm->execute) {
			$error = $dbh->errstr;
			return $error;
		}
  	}
	$sth_master->execute();
  }
  return "1";
}

sub list_masterservice {
  my $sth;
  my $master_serviceid = shift @_;
  #Added by Karuna for EPL MSID
  my $epl_msid = $master_serviceid;
  #Convert to lower case
  $epl_msid = lc($epl_msid);
  #Remove space
  $epl_msid =~ s/ //g;
  #extract the code - emm for EPL, vlm for VPLS 
  $epl_msid = substr ($epl_msid,5,3);

  if ($master_serviceid eq '') {
  	$sth = $dbh->prepare
	("SELECT DISTINCT master_service.master_serviceid, aggregate_serviceid, serviceid, service.accno
        FROM master_service left join service on master_service.master_serviceid=service.master_serviceid");
  }elsif ($epl_msid =~ /emm/) {
    	#Added by Karuna for listing EPL Master Service IDs
    	$sth = $dbh->prepare ("SELECT DISTINCT master_service.master_serviceid, serviceid, service.accno FROM master_service left join service on master_service.master_serviceid=service.master_serviceid WHERE master_service.master_serviceid='$master_serviceid'");
  } elsif ($master_serviceid =~ /ipm/i) {
	#Added by Karuna for listing IP Transit Master Service IDs
	#Chandini_IPT
	$sth = $dbh->prepare 
	#("SELECT DISTINCT ipt_master.master_sid, ipt_master.ipc_servid, service.serviceid, service.accno, ipt_master.cos_type, ipt_master.rate_type, ipt_master.current_acdr, ipt_master.current_stdt FROM ipt_master left join service on ipt_master.master_sid=service.master_serviceid WHERE ipt_master.master_sid = '$master_serviceid'");
	("SELECT DISTINCT ipt_master.master_sid, ipt_master.ipc_servid, service.serviceid, ipc_china.customer, ipt_master.cos_type, ipt_master.rate_type, ipt_master.current_acdr, ipt_master.current_stdt,ipt_master.proposed_rate, ipt_master.proposed_acdr, ipt_master.proposed_stdt FROM ipt_master left join service on ipt_master.master_sid=service.master_serviceid  left join ipc_china on ipc_china.ipc_servid=ipt_master.ipc_servid WHERE ipt_master.master_sid ='$master_serviceid'");
  } else {
  	$sth = $dbh->prepare
       ("SELECT DISTINCT master_service.master_serviceid, aggregate_serviceid, serviceid, service.accno
        FROM master_service left join service on master_service.master_serviceid=service.master_serviceid
        WHERE master_service.master_serviceid='$master_serviceid'");
  }

  unless ($sth->execute) {
  	$error = $dbh->errstr;
  	return 0;
  }

     my $result = $sth->fetchall_arrayref;
    if ($result) {
      $error = $dbh->errstr;
      $sth->finish;
      return $result;
    } else {
      $error = $dbh->errstr;
      return 0;
    }
}

############################################################################
# Sub Routine: get_ipt_costype
# Description: fetches the cos type options for an IP Transit Master service
# Input parameters: none
# Output paramaters: array reference of cos type values
# Author: Karuna Ballal
###########################################################################

sub get_ipt_costype {

my $sth = $dbh->prepare("SELECT DISTINCT cos_type from ipt_cos ORDER BY cos_type");
unless ($sth->execute) {
	$error = $dbh->errstr;
	return "0";
}
my $result = $sth->fetchall_arrayref;

if($result->[0]) {
	$error = $dbh->errstr;
	$sth->finish;
	return $result;
}
else {
	$error = $dbh->errstr;
	return 0;
}

}

############################################################################
# Sub Routine: get_ipt_ratetype
# Description: fetches the rate type options for an IP Transit Master service
# Input parameters: none
# Output paramaters: array reference of rate type values
# Author: Karuna Ballal
###########################################################################

sub get_ipt_ratetype {

my $sth;
$sth = $dbh->prepare("SELECT DISTINCT rate_type from ipt_rate_type ORDER BY rate_type DESC");
unless ($sth->execute) {
        $error = $dbh->errstr;
        return "0";
}
my $result = $sth->fetchall_arrayref;

if($result->[0]) {
        $error = $dbh->errstr;
        $sth->finish;
        return $result;
}
else {
        $error = $dbh->errstr;
        return 0;
}

}

############################################################################
# Sub Routine: get_ipt_ipcservice
# Description: fetches the IPC Services of IP Transit Product
# Input parameters: none
# Output paramaters: array reference of IPC services
# Author:Chandini 
###########################################################################

sub get_ipt_ipcservice {

my $sth;
$sth = $dbh->prepare("SELECT DISTINCT ipc_servid from ipc_china where ipc_servid ilike '%ipc%' ORDER BY ipc_servid");
unless ($sth->execute) {
        $error = $dbh->errstr;
        return "0";
}
my $result = $sth->fetchall_arrayref;

if($result->[0]) {
        $error = $dbh->errstr;
        $sth->finish;
        return $result;
}
else {
        $error = $dbh->errstr;
        return 0;
}

}



1;
