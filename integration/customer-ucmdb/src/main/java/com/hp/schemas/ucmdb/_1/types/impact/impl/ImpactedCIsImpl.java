/*
 * XML Type:  ImpactedCIs
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML ImpactedCIs(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ImpactedCIsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs
{
    
    public ImpactedCIsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTEDCI$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "impactedCI");
    
    
    /**
     * Gets array of all "impactedCI" elements
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactedCI[] getImpactedCIArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IMPACTEDCI$0, targetList);
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCI[] result = new com.hp.schemas.ucmdb._1.types.impact.ImpactedCI[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "impactedCI" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactedCI getImpactedCIArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCI target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCI)get_store().find_element_user(IMPACTEDCI$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "impactedCI" element
     */
    public int sizeOfImpactedCIArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IMPACTEDCI$0);
        }
    }
    
    /**
     * Sets array of all "impactedCI" element
     */
    public void setImpactedCIArray(com.hp.schemas.ucmdb._1.types.impact.ImpactedCI[] impactedCIArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(impactedCIArray, IMPACTEDCI$0);
        }
    }
    
    /**
     * Sets ith "impactedCI" element
     */
    public void setImpactedCIArray(int i, com.hp.schemas.ucmdb._1.types.impact.ImpactedCI impactedCI)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCI target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCI)get_store().find_element_user(IMPACTEDCI$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(impactedCI);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "impactedCI" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactedCI insertNewImpactedCI(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCI target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCI)get_store().insert_element_user(IMPACTEDCI$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "impactedCI" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactedCI addNewImpactedCI()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCI target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCI)get_store().add_element_user(IMPACTEDCI$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "impactedCI" element
     */
    public void removeImpactedCI(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IMPACTEDCI$0, i);
        }
    }
}
