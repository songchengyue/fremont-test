/*
 * An XML document type.
 * Localname: rediscoverCIsResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one rediscoverCIsResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class RediscoverCIsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponseDocument
{
    
    public RediscoverCIsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REDISCOVERCISRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "rediscoverCIsResponse");
    
    
    /**
     * Gets the "rediscoverCIsResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse getRediscoverCIsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse)get_store().find_element_user(REDISCOVERCISRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "rediscoverCIsResponse" element
     */
    public void setRediscoverCIsResponse(com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse rediscoverCIsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse)get_store().find_element_user(REDISCOVERCISRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse)get_store().add_element_user(REDISCOVERCISRESPONSE$0);
            }
            target.set(rediscoverCIsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "rediscoverCIsResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse addNewRediscoverCIsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse)get_store().add_element_user(REDISCOVERCISRESPONSE$0);
            return target;
        }
    }
}
