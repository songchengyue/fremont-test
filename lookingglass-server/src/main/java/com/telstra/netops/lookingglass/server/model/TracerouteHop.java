package com.telstra.netops.lookingglass.server.model;

public class TracerouteHop {
	
	private String text;

	public TracerouteHop() {		
	}
	
	public TracerouteHop(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}	

}
