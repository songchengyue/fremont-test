--
-- Schema for persisting tokens.
-- Based on https://github.com/spring-projects/spring-security-oauth/blob/master/spring-security-oauth2/src/test/resources/schema.sql
-- but converted for Oracle
--
-- TODO - Run SQL on application start up? Doesn't seem to work even though I have defined "platform" in my datasource properties.
--        Maybe need to change this script to not be as "destructive" if we do decide to run it on startup.
--
drop table oauth_client_details;

create table oauth_client_details (
  client_id VARCHAR2(256) PRIMARY KEY,
  resource_ids VARCHAR2(256),
  client_secret VARCHAR2(256),
  scope VARCHAR2(256),
  authorized_grant_types VARCHAR2(256),
  web_server_redirect_uri VARCHAR2(256),
  authorities VARCHAR2(256),
  access_token_validity INTEGER,
  refresh_token_validity INTEGER,
  additional_information CLOB,
  autoapprove VARCHAR2(256)
);

delete from oauth_client_details;

insert into oauth_client_details(client_id, client_secret, authorities, scope, authorized_grant_types)
values ('samlbearerdemo', 'samlsecret', 'ROLE_CLIENT', 'read', 'urn:ietf:params:oauth:grant-type:saml1-bearer');

drop table oauth_client_token;

create table oauth_client_token (
  token_id VARCHAR2(256),
  token BLOB,
  authentication_id VARCHAR2(256) PRIMARY KEY,
  user_name VARCHAR2(256),
  client_id VARCHAR2(256)
);

drop table oauth_access_token;

create table oauth_access_token (
  token_id VARCHAR2(256),
  token BLOB,
  authentication_id VARCHAR2(256) PRIMARY KEY,
  user_name VARCHAR2(256),
  client_id VARCHAR2(256),
  authentication BLOB,
  refresh_token VARCHAR2(256)
);

drop table oauth_refresh_token;

create table oauth_refresh_token (
  token_id VARCHAR(256),
  token BLOB,
  authentication BLOB
);

drop table oauth_code;

create table oauth_code (
  code VARCHAR(256), authentication BLOB
);

drop table oauth_approvals;

create table oauth_approvals (
	userId VARCHAR2(256),
	clientId VARCHAR2(256),
	scope VARCHAR2(256),
	status VARCHAR2(10),
	expiresAt TIMESTAMP,
	lastModifiedAt TIMESTAMP
);

drop table ClientDetails;

-- customized oauth_client_details table
create table ClientDetails (
  appId VARCHAR2(256) PRIMARY KEY,
  resourceIds VARCHAR2(256),
  appSecret VARCHAR2(256),
  scope VARCHAR2(256),
  grantTypes VARCHAR2(256),
  redirectUrl VARCHAR2(256),
  authorities VARCHAR2(256),
  access_token_validity INTEGER,
  refresh_token_validity INTEGER,
  additionalInformation CLOB,
  autoApproveScopes VARCHAR2(256)
);