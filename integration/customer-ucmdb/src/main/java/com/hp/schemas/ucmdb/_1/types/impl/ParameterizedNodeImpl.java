/*
 * XML Type:  ParameterizedNode
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ParameterizedNode
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ParameterizedNode(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ParameterizedNodeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ParameterizedNode
{
    
    public ParameterizedNodeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PARAMETERS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "parameters");
    private static final javax.xml.namespace.QName NODELABEL$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "nodeLabel");
    
    
    /**
     * Gets the "parameters" element
     */
    public com.hp.schemas.ucmdb._1.types.CIProperties getParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().find_element_user(PARAMETERS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "parameters" element
     */
    public void setParameters(com.hp.schemas.ucmdb._1.types.CIProperties parameters)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().find_element_user(PARAMETERS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().add_element_user(PARAMETERS$0);
            }
            target.set(parameters);
        }
    }
    
    /**
     * Appends and returns a new empty "parameters" element
     */
    public com.hp.schemas.ucmdb._1.types.CIProperties addNewParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().add_element_user(PARAMETERS$0);
            return target;
        }
    }
    
    /**
     * Gets the "nodeLabel" element
     */
    public java.lang.String getNodeLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NODELABEL$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "nodeLabel" element
     */
    public org.apache.xmlbeans.XmlString xgetNodeLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NODELABEL$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "nodeLabel" element
     */
    public void setNodeLabel(java.lang.String nodeLabel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NODELABEL$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NODELABEL$2);
            }
            target.setStringValue(nodeLabel);
        }
    }
    
    /**
     * Sets (as xml) the "nodeLabel" element
     */
    public void xsetNodeLabel(org.apache.xmlbeans.XmlString nodeLabel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NODELABEL$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NODELABEL$2);
            }
            target.set(nodeLabel);
        }
    }
}
