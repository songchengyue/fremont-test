/*
 * An XML document type.
 * Localname: getQueryNameOfView
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getQueryNameOfView(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetQueryNameOfViewDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewDocument
{
    
    public GetQueryNameOfViewDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETQUERYNAMEOFVIEW$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getQueryNameOfView");
    
    
    /**
     * Gets the "getQueryNameOfView" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfView getGetQueryNameOfView()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfView target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfView)get_store().find_element_user(GETQUERYNAMEOFVIEW$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getQueryNameOfView" element
     */
    public void setGetQueryNameOfView(com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfView getQueryNameOfView)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfView target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfView)get_store().find_element_user(GETQUERYNAMEOFVIEW$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfView)get_store().add_element_user(GETQUERYNAMEOFVIEW$0);
            }
            target.set(getQueryNameOfView);
        }
    }
    
    /**
     * Appends and returns a new empty "getQueryNameOfView" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfView addNewGetQueryNameOfView()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfView target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfView)get_store().add_element_user(GETQUERYNAMEOFVIEW$0);
            return target;
        }
    }
}
