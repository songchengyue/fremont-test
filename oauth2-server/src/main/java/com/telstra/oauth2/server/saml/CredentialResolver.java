package com.telstra.oauth2.server.saml;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Collections;

import org.opensaml.xml.security.Criteria;
import org.opensaml.xml.security.CriteriaSet;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.credential.KeyStoreCredentialResolver;
import org.opensaml.xml.security.criteria.EntityIDCriteria;
import org.opensaml.xml.security.x509.X509Credential;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class CredentialResolver {
	
	@Value("${security.saml.bearer.auth.oauth2.saml.assertion.validation.keystore.file}")
	private Resource keystoreFile;
	
	@Value("${security.saml.bearer.auth.oauth2.saml.assertion.validation.keystore.password}")
	private String password;
	
	@Value("${security.saml.bearer.auth.oauth2.saml.assertion.validation.keystore.alias}")
	private String alias;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CredentialResolver.class);
	
	public Credential resolve() {
		KeyStore keystore;
		try {
			keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(keystoreFile.getInputStream(), password.toCharArray());
			 
			KeyStoreCredentialResolver resolver = new KeyStoreCredentialResolver(keystore, Collections.emptyMap());
			 
			Criteria criteria = new EntityIDCriteria(alias);
			CriteriaSet criteriaSet = new CriteriaSet(criteria);
			 
			X509Credential credential = (X509Credential)resolver.resolveSingle(criteriaSet);
			return credential;
		} catch (KeyStoreException e) {
			LOGGER.error("can't resolve credentials", e);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("can't resolve credentials", e);
		} catch (CertificateException e) {
			LOGGER.error("can't resolve credentials", e);
		} catch (IOException e) {
			LOGGER.error("can't resolve credentials", e);
		} catch (SecurityException e) {
			LOGGER.error("can't resolve credentials", e);
		}
		
		return null;
	}

}
