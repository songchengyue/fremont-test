package com.telstra.netops.lookingglass.server.config;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class NetopsWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private Environment env;
	
	@Value("${h2.console.endpoint:#{null}}")
	private String h2Endpoint;
	
	@Value("${cors.allowedOrigins:#{null}}")
	private String corsAllowedOrigins;
	
	@Override
	public void configure(WebSecurity web) throws Exception {
				 
		// required for CORS when Spring Security is used!
		if (corsAllowedOrigins != null) {
			web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
		}
		
		// H2 console
		if (env.acceptsProfiles("local") && h2Endpoint != null) {
			web.ignoring().antMatchers("/" + h2Endpoint + "/**");
		}
	}
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
            	if (corsAllowedOrigins != null) {
            		registry.addMapping("/**").allowedOrigins(corsAllowedOrigins);
            	}
            }
        };
    }
	
	/*@Bean
	@Profile("local")
	public ServletRegistrationBean h2servletRegistration() {
	    ServletRegistrationBean registration = new ServletRegistrationBean(new WebServlet());
	    registration.addUrlMappings("/" + h2Endpoint  + "/*");
	    return registration;
	}*/


}
