/*
 * XML Type:  Scope
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/types
 * Java type: com.hp.schemas.ucmdb.discovery._1.types.Scope
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.types.impl;
/**
 * An XML Scope(@http://schemas.hp.com/ucmdb/discovery/1/types).
 *
 * This is a complex type.
 */
public class ScopeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.types.Scope
{
    
    public ScopeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EXCLUDE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/types", "Exclude");
    private static final javax.xml.namespace.QName INCLUDE$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/types", "Include");
    
    
    /**
     * Gets the "Exclude" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude getExclude()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude)get_store().find_element_user(EXCLUDE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Exclude" element
     */
    public void setExclude(com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude exclude)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude)get_store().find_element_user(EXCLUDE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude)get_store().add_element_user(EXCLUDE$0);
            }
            target.set(exclude);
        }
    }
    
    /**
     * Appends and returns a new empty "Exclude" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude addNewExclude()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude)get_store().add_element_user(EXCLUDE$0);
            return target;
        }
    }
    
    /**
     * Gets the "Include" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.Scope.Include getInclude()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.Scope.Include target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.Scope.Include)get_store().find_element_user(INCLUDE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Include" element
     */
    public void setInclude(com.hp.schemas.ucmdb.discovery._1.types.Scope.Include include)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.Scope.Include target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.Scope.Include)get_store().find_element_user(INCLUDE$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.types.Scope.Include)get_store().add_element_user(INCLUDE$2);
            }
            target.set(include);
        }
    }
    
    /**
     * Appends and returns a new empty "Include" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.Scope.Include addNewInclude()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.Scope.Include target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.Scope.Include)get_store().add_element_user(INCLUDE$2);
            return target;
        }
    }
    /**
     * An XML Exclude(@http://schemas.hp.com/ucmdb/discovery/1/types).
     *
     * This is a complex type.
     */
    public static class ExcludeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude
    {
        
        public ExcludeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName RANGES$0 = 
            new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/types", "Ranges");
        
        
        /**
         * Gets array of all "Ranges" elements
         */
        public com.hp.schemas.ucmdb.discovery._1.types.IPRange[] getRangesArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(RANGES$0, targetList);
                com.hp.schemas.ucmdb.discovery._1.types.IPRange[] result = new com.hp.schemas.ucmdb.discovery._1.types.IPRange[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Ranges" element
         */
        public com.hp.schemas.ucmdb.discovery._1.types.IPRange getRangesArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb.discovery._1.types.IPRange target = null;
                target = (com.hp.schemas.ucmdb.discovery._1.types.IPRange)get_store().find_element_user(RANGES$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Ranges" element
         */
        public int sizeOfRangesArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RANGES$0);
            }
        }
        
        /**
         * Sets array of all "Ranges" element
         */
        public void setRangesArray(com.hp.schemas.ucmdb.discovery._1.types.IPRange[] rangesArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(rangesArray, RANGES$0);
            }
        }
        
        /**
         * Sets ith "Ranges" element
         */
        public void setRangesArray(int i, com.hp.schemas.ucmdb.discovery._1.types.IPRange ranges)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb.discovery._1.types.IPRange target = null;
                target = (com.hp.schemas.ucmdb.discovery._1.types.IPRange)get_store().find_element_user(RANGES$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ranges);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Ranges" element
         */
        public com.hp.schemas.ucmdb.discovery._1.types.IPRange insertNewRanges(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb.discovery._1.types.IPRange target = null;
                target = (com.hp.schemas.ucmdb.discovery._1.types.IPRange)get_store().insert_element_user(RANGES$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Ranges" element
         */
        public com.hp.schemas.ucmdb.discovery._1.types.IPRange addNewRanges()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb.discovery._1.types.IPRange target = null;
                target = (com.hp.schemas.ucmdb.discovery._1.types.IPRange)get_store().add_element_user(RANGES$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "Ranges" element
         */
        public void removeRanges(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RANGES$0, i);
            }
        }
    }
    /**
     * An XML Include(@http://schemas.hp.com/ucmdb/discovery/1/types).
     *
     * This is a complex type.
     */
    public static class IncludeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.types.Scope.Include
    {
        
        public IncludeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName RANGES$0 = 
            new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/types", "Ranges");
        
        
        /**
         * Gets array of all "Ranges" elements
         */
        public com.hp.schemas.ucmdb.discovery._1.types.IPRange[] getRangesArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(RANGES$0, targetList);
                com.hp.schemas.ucmdb.discovery._1.types.IPRange[] result = new com.hp.schemas.ucmdb.discovery._1.types.IPRange[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Ranges" element
         */
        public com.hp.schemas.ucmdb.discovery._1.types.IPRange getRangesArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb.discovery._1.types.IPRange target = null;
                target = (com.hp.schemas.ucmdb.discovery._1.types.IPRange)get_store().find_element_user(RANGES$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Ranges" element
         */
        public int sizeOfRangesArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RANGES$0);
            }
        }
        
        /**
         * Sets array of all "Ranges" element
         */
        public void setRangesArray(com.hp.schemas.ucmdb.discovery._1.types.IPRange[] rangesArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(rangesArray, RANGES$0);
            }
        }
        
        /**
         * Sets ith "Ranges" element
         */
        public void setRangesArray(int i, com.hp.schemas.ucmdb.discovery._1.types.IPRange ranges)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb.discovery._1.types.IPRange target = null;
                target = (com.hp.schemas.ucmdb.discovery._1.types.IPRange)get_store().find_element_user(RANGES$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ranges);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Ranges" element
         */
        public com.hp.schemas.ucmdb.discovery._1.types.IPRange insertNewRanges(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb.discovery._1.types.IPRange target = null;
                target = (com.hp.schemas.ucmdb.discovery._1.types.IPRange)get_store().insert_element_user(RANGES$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Ranges" element
         */
        public com.hp.schemas.ucmdb.discovery._1.types.IPRange addNewRanges()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.hp.schemas.ucmdb.discovery._1.types.IPRange target = null;
                target = (com.hp.schemas.ucmdb.discovery._1.types.IPRange)get_store().add_element_user(RANGES$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "Ranges" element
         */
        public void removeRanges(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RANGES$0, i);
            }
        }
    }
}
