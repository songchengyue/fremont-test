/*
 * An XML document type.
 * Localname: dispatchAdHocJobResopnse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one dispatchAdHocJobResopnse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class DispatchAdHocJobResopnseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument
{
    
    public DispatchAdHocJobResopnseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DISPATCHADHOCJOBRESOPNSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "dispatchAdHocJobResopnse");
    
    
    /**
     * Gets the "dispatchAdHocJobResopnse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse getDispatchAdHocJobResopnse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse)get_store().find_element_user(DISPATCHADHOCJOBRESOPNSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "dispatchAdHocJobResopnse" element
     */
    public void setDispatchAdHocJobResopnse(com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse dispatchAdHocJobResopnse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse)get_store().find_element_user(DISPATCHADHOCJOBRESOPNSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse)get_store().add_element_user(DISPATCHADHOCJOBRESOPNSE$0);
            }
            target.set(dispatchAdHocJobResopnse);
        }
    }
    
    /**
     * Appends and returns a new empty "dispatchAdHocJobResopnse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse addNewDispatchAdHocJobResopnse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse)get_store().add_element_user(DISPATCHADHOCJOBRESOPNSE$0);
            return target;
        }
    }
}
