#!/usr/bin/perl
use JSON::PP;
use CGI;
use CGI::Carp qw ( fatalsToBrowser );

my $req = new CGI;
my $input = decode_json( $req->param('POSTDATA') );

my $code                =$input->{'code'};
my $customerId          =$input->{'customerId'};
my $level                       =$input->{'level'};
my $et                          =$input->{'et'};
my $pop                         =$input->{'pop'};
my $toHost                      =$input->{'toHost'};
my $submit                      =$input->{'submit'};
my $numPings                      =$input->{'numPings'};

warn "level: $level, pop: $pop, toHost: $toHost, submit: $submit";

sub average($) {
    my($data) = @_;
    if (not @$data) {
        die("Empty arrayn");
    }
    my $total = 0;
    foreach (@$data) {
        $total += $_;
    }
    my $average = $total / @$data;
    return $average;
}

sub stdev($) {
    my($data) = @_;
    if(@$data == 1){
        return 0;
    }
    my $average = average($data);
    my $sqtotal = 0;
    foreach(@$data) {
        $sqtotal += ($average-$_) ** 2;
    }
    my $std = ($sqtotal / (@$data-1)) ** 0.5;
    return $std;
}

print "Content-Type: application/json\n\n";

my $mock_error = 0;
$numPings = 10 unless ($numPings);
my @pings = ();
for (my $i = 0; $i < $numPings; $i++) {
    my $ping = sprintf "%.2f", 1.3 + rand(0.2);
    push @pings, $ping;
}

# calculate stats
my $min = 100;
my $max = 0;
for (my $i = 0; $i < $numPings; $i++) {
    $min = $pings[$i] if ($pings[$i] < $min);
    $max = $pings[$i] if ($pings[$i] > $max);
}

my $avg = sprintf "%.2f", average(\@pings);
my $stddev = sprintf "%.2f", stdev(\@pings);

if (!$mock_error) {
    my $date = gmtime() . " GMT";
    my $loss = 0;
    my %out = (
        'date' => $date,
        'MinLatency' => $min,
        'MaxLatency' => $max, 
        'AvgLatency' => $avg,
        'StdDeviation' => $stddev,
        'AvgLoss' => $loss,
        );

    for ( my $i = 0; $i < $numPings; $i++) {
        my $key = "result" . ( $i + 1 );       
        $out{$key} = $pings[$i];
    }

    my $output = encode_json \%out;
    print $output;
} else {
    my %err = ( 'error' => 'This is a mock error');

    my $error = encode_json \%err;
    print $error;
}
