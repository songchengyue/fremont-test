###################################################################
# File        : CMS_CUST_SERVICE_DB.pm
# Author: 	Karuna Ballal	
# Date:		28th Sep 2010
###################################################################
# Modification History :
#  Date               Name                 Change/Description
# ------------------------------------------------------------------
# 28SEP2010	KARUNA BALLAL		Created for IP Transit ph4a
#######################################################################
##! /usr/local/bin/perl
package CMS_CUST_SERVICE_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;
use CMS_DB;
use DBI;
use CMS;
our @ISA = qw (Exporter);
our @EXPORT = qw ($dbh $dbh_tr);


our $error;
my $debug = 1;
#uncomment the line below to turn off the debug statements 
#$debug = 0; 
if($debug){open (IPC1,">/data1/tmp_log/custserv_db1.log");}
my $error_message;
our $dbh;
our $dbh_tr;
my $dbname="cms";
my $dbuser="postgres";

use constant SUCCESS => 1;
use constant FAIL => 0;

if($debug){print IPC1 "ENTERED DB file1 DBG: $debug\n";}

# connects to database
sub connect_to_database {
	if (defined ($dbh)) {
		if($debug){print IPC1 "FAIL during connection\n";}
		return FAIL;
	} else {
		$dbh = DBI->connect("dbi:Pg:dbname=$dbname",$dbuser);
		my $sth = $dbh->prepare("SET TIME ZONE 'UTC'");
		$sth->execute;
		if($debug){print IPC1 "PASS during connection\n";}
		return SUCCESS;
  	}

}

sub begin_transaction {
	# because of the stupid way DBI handles this, I have to start
	# a second connection

	if (defined ($dbh_tr)) {
		if($debug){print IPC1 "FAIL during transaction\n";}
		return FAIL;
	} else {
		my %options;
		$options{AutoCommit} = 0;
		$dbh_tr = DBI->connect("dbi:Pg:dbname=$dbname", $dbuser, "",\%options);
		my $sth = $dbh_tr->prepare("SET TIME ZONE 'UTC'");
		$sth->execute;
		if($debug){print IPC1 "PASS during connection\n";}
		return SUCCESS;
  	}
}

# disconnect from database
sub disconnect_from_database {
	$dbh->disconnect;
	undef $dbh;
}

# disconnect from transaction
sub disconnect_from_transaction {
	$dbh_tr->disconnect;
	undef $dbh_tr;
}
################################################################
# Name          : get_productcode 
# Description   : get all Customer Based Products
# Author        : Karuna Ballal
# Input         : 
# Outupt        : all Customer Based Products 
################################################################
sub get_productcode{
	my $sth = $dbh->prepare("SELECT productcodeid FROM productcode where customer_based ilike 'y' order by productcodeid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

################################################################
# Name          : update_service
# Description   : Deletes the service details from the DB and creates a list file
# Author        : Karuna Ballal
# Input         : all service parameters
# Outupt        : list file ipc_deleted_services.list
################################################################
sub delete_cust_service{
	if ($debug){print IPC1 "DB ENTERED FUNCTION delete_cust_service\n";}
	CMS_DB::connect_to_database();
        CMS_DB::begin_transaction();
	print IPC1 "DB ENTERED FUNCTION delete_cust_service\n";
	my $service = $_[0];
	my $accno = $_[1];
	my $hidden_param = $_[2];
	if ($debug){print IPC1 "DB delete_cust_service service:$service\taccno:$accno\thidden_param:$hidden_param\n";}
	my @hidden_vals = split(/\,/,$hidden_param);
	my $as_no = $hidden_vals[0];
	my @hid_val = split(/\-/,$hidden_vals[0]);
	if ($debug){print IPC1 "DB delete_cust_service hid_val:@hid_val\n";}
	my $committed_rate = $hidden_vals[1];
	my $eff_dir_dt = $hidden_vals[2];
	my $description = $hidden_vals[3];
	my $previous_committed_rate = $hidden_vals[4];
	my $committed_stdt = $hidden_vals[5];
	##Karuna on 16-11-2010
	my @comm_start = split(/ /,$committed_stdt);
	my $comm_start = $comm_start[0]; 
	##--IPT4a
	my $path = "/usr/local/www/wanchai/cms/data/poller/";
	my $file = "ipc_deleted_services.list";
	my $tomo_date = `date -v+1d "+%Y-%m-%d"`;
	chomp($tomo_date);

	## list file to enter deleted services
	open (IPC_FH, ">>$path$file") or die "Failed to write file $file: $!\n";
	if ($debug){print IPC1 "DB delete_cust_service as_no:$as_no\tcommitted_rate:$committed_rate\teff_dir_dt:$eff_dir_dt\tcomm_start:$comm_start\n";}

	## check if the Service is linked to an IPM
	my $ipc_exist_query = ("SELECT ipc_servid from ipt_master WHERE ipc_servid in (SELECT ipc_servid from ipc_china where ipc_servid = '$service')");
	my $sth_ipc = $dbh->prepare($ipc_exist_query);
	$sth_ipc->execute();
	my $row = $sth_ipc->rows();
	if ($row > 0){
		## throw error
                return "There is an active IPM Service linked to this IPC Service.";
	}

	## validation for manually entered service ID and customer
	$ipc_exist_query = ("SELECT ipc_servid from ipc_china where ipc_servid = '$service'");
	$sth_ipc = $dbh->prepare($ipc_exist_query);
        $sth_ipc->execute();
        $row = $sth_ipc->rows();
	if ($debug) {print IPC1 "DB DELETE row:$row\n";}
        if ($row <= 0){
		## throw error
		return "IPC Service does not exist.";
	}

	my $del_query = ("DELETE FROM ipc_china WHERE ipc_servid like '$service' AND customer like '$accno' AND as_nos like '$as_no'"); 
	if ($debug){print IPC1 "DB DELETE del_query:$del_query\n";}	
	my $sth = $dbh->prepare($del_query);
	unless ($sth->execute) {
                $error = $dbh->errstr;
                if ($debug){print IPC1 "DB DELETE error:$error\n";}
                return $error;
        }
	$sth->finish();
	$dbh->commit;
	if ($previous_committed_rate eq ""){
		$previous_committed_rate = "-";
	}
	if ($description eq ""){
		$description = "-";
	}
	
	## append to the list file
	#print IPC_FH "$service\t$accno\t$description\t$committed_rate\t$previous_committed_rate\t$committed_stdt\tNA\t$as_no\t$tomo_date\tIPTRANSIT\n";
	print IPC_FH "$service\t$accno\t$description\t$committed_rate\t$previous_committed_rate\t$comm_start\tNA\t$as_no\t$tomo_date\tIPTRANSIT\n";
	close (IPC_FH);
	#CMS_DB::disconnect_from_transaction();
	return 1;
		
}
################################################################
# Name          : add_cust_service 
# Description   : sub routine to add customer based service to the DB 
# Author        : Karuna Ballal
# Input         : serviceid, customer, description, productcode, committed_rate, dir_traffic, as_no
# Outupt        : 
################################################################
sub add_cust_service {
	CMS_DB::begin_transaction();
	my $serviceid = $dbh->quote($_[0]);
	my $customer = $dbh->quote($_[1]);
	my $description = $dbh->quote($_[2]);
	my $productcode = $dbh->quote($_[3]);
	my $committed_rate = $dbh->quote($_[4]);
	my $dir_traffic = $dbh->quote($_[5]);
	my $as_no = $dbh->quote($_[6]);
	if ($debug){print IPC1 "serviceid:$serviceid\tcustomer:$customer\tdescription:$description\tproductcode:$productcode\tcommitted_rate:$committed_rate\tdir_traffic:$dir_traffic\tas_no:$as_no\n";}
	
	#check if the Service ID already exists
	my $sth = $dbh_tr->prepare ("SELECT DISTINCT ipc_servid FROM ipc_china WHERE ipc_servid like $serviceid ");
	if ($debug){print IPC1 "DB query:SELECT ipc_servid FROM ipc_china WHERE ipc_servid like $serviceid\n";}
	$sth->execute();
	my $row = $sth->rows();
	if ($debug){print IPC1 "DB row sid:$row\n";}
	if ($row > 0){
		return "Service ID already exists.";
	}

	#check if the customer exists or not
	$sth = $dbh_tr->prepare ("SELECT accno FROM customer WHERE accno like $customer");
	if ($debug){print IPC1 "DB query:SELECT accno FROM customer WHERE accno like $customer\n";}
	$sth->execute();
  	$row = $sth->fetchrow_hashref;
  	$sth = $dbh->prepare ("alter table outagecustomercomments disable triggers");
  	$sth->execute();

  	my $temp_accno = $customer;
  	$temp_accno =~ s/\'//g;
  	if($$row{accno} =~ /^$temp_accno$/) {
	} else {
        	return "Customer does not exist.";
  	}

	#check for AS Number in existing products -for Direct China traffic only 
	if ($dir_traffic eq "'direct'"){
		my $sth2 = $dbh_tr->prepare ("SELECT as_no from service where as_no like $as_no AND productcodeid like 'GIA'");
		if ($debug){print IPC1 "DB query:SELECT as_no from service where as_no like $as_no AND productcodeid like 'GIA\n";}
		$sth2->execute();
        	my $row2 = $sth2->rows();

        	if($row2 > 0) {
			return "AS Number already in use by a GIA service.";
		}
	}
	
	#check AS Number in IPC table
	my $sth2 = $dbh_tr->prepare ("select as_nos from ipc_china where as_nos like $as_no");
        if ($debug){print IPC1 "DB query:SELECT as_nos from ipc_china where as_nos like $as_no\n";}
	$sth2->execute();
        my $row2 = $sth2->rows();

	if ($row2 > 0) {
		return "AS Number already in use by another IPC service.";
        }
	if ($debug){print IPC1 "row2:$row2\n";}
	$productcode =~ s/\s+$//;
	if ($debug){print IPC1 "row2:productcode:$productcode 1\n";}
	
	#add IPC record to the DB
	CMS_DB::connect_to_database();
	my $sth_ipc = $dbh_tr->prepare ("INSERT INTO ipc_china (ipc_servid, customer, description, productcode, current_committed_rate, committed_stdt, dir_traffic, as_nos, dir_effective_date) VALUES ($serviceid, $customer, $description, $productcode, $committed_rate, current_timestamp, $dir_traffic, $as_no,current_timestamp)");
	if ($debug){print IPC1 "insert query:INSERT INTO ipc_china (ipc_servid, customer, description, productcode, current_committed_rate, committed_stdt, dir_traffic, as_nos, dir_effective_date) VALUES ($serviceid, $customer, $description, $productcode, $committed_rate, current_timestamp, $dir_traffic, $as_no, current_timestamp)\n";}
	my $success = 1;
	$success &&= $sth_ipc->execute;
	my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
	if ($debug){print IPC1 "success:$success\terror:$error\n";}
	return $error if ($error);
	return $result;
	
#	CMS_DB::disconnect_from_transaction();
#	CMS_DB::disconnect_from_database();
}

################################################################
# Name          : get_service 
# Description   : fetches all service IDs for auto populate feature
# Author        : Karuna Ballal
# Input         : 
# Outupt        :
################################################################
sub get_service {
	my $sth = $dbh->prepare("SELECT DISTINCT ipc_servid from ipc_china WHERE productcode like 'IPTRANSIT%'");
	$sth->execute();
	my $result = $sth->fetchall_arrayref;
        my $retString;
        foreach my $servid (@$result){
                if ( $retString ne "" ){
                        $retString=$retString.",".$$servid[0];
                }
                else {
                        $retString=$$servid[0];
               }
        }

        $sth->finish;
        return $retString;
}

################################################################
# Name          : get_accno_for_service 
# Description   : fetches the corresponding acc no. for the particular service 
# Author        : Karuna Ballal
# Input         : service id
# Outupt        : account number
################################################################
sub get_accno_for_service {
	my $serviceid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT customer from ipc_china WHERE ipc_servid = $serviceid");
	unless ($sth->execute) {
    		$error = $dbh->errstr;
    		return 0;
  	}

  	my $result = $sth->fetchrow_arrayref;
	if ($debug) {print IPC1 "Edit:result $$result[0]\n";}

  	if ($result) {
    		$error = $dbh->errstr;
    		$sth->finish;
    		return $$result[0];
  	} else {
    		$error = $dbh->errstr;
    		return 0;
  	}
}

################################################################
# Name          : get_service_details
# Description   : fetches the corresponding details, given accno. and service
# Author        : Karuna Ballal
# Input         : service id
# Outupt        : ipc_servid,customer,description,current_committed_rate,dir_traffic,as_nos,dir_effective_date 
################################################################
sub get_service_details {
	my $serviceid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("select DISTINCT ON (ipc_servid) ipc_servid,customer,description,current_committed_rate,dir_traffic,as_nos,dir_effective_date from ipc_china where ipc_servid like $serviceid order by ipc_servid,dir_effective_date DESC");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
	$sth->execute();
	my $row = $sth->rows();
	if ($row <= 0){
		if ($debug) {print IPC1 "DB EDIT1 IPC does no exist.\n";}
                return "IPC Service does not exist.";
        } else {
        	my @result = $sth->fetchrow_array;
        	$sth->finish;
		return \@result;
	}
}

################################################################
# Name          : search_service 
# Description   : queries the DB based on entered paramater/s 
# Author        : Karuna Ballal
# Input         : one or multiple search parameters
# Outupt        : search result
################################################################
sub search_service {
	my $productcode = $_[0];
	my $serviceid = $_[1];
	my $accno = $_[2];
	my $description = $_[3];
	my $committed_rate = $_[4];
	my $as_no = $_[5];
	my $dir_traffic = $_[6];
	my $command = "";
	my $cond = "";
	if($debug){print IPC1 "ENTERED DB file1 SEARCh DBG: $debug\n";}

	#$command = ("(SELECT DISTINCT ON (ipc_china.ipc_servid) ipc_china.ipc_servid, master_sid,serviceid,ipc_china.customer,ipc_china.description,productcode,current_committed_rate,as_nos,dir_traffic
		     #FROM ipc_china 
		     #left outer join ipt_master on ipc_china.ipc_servid = ipt_master.ipc_servid
		     #left outer join service on service.master_serviceid=ipt_master.master_sid
		     #where master_sid is not null COND)
		     #UNION
		     #(SELECT DISTINCT ON (ipc_china.ipc_servid) ipc_china.ipc_servid, master_sid,serviceid,ipc_china.customer,ipc_china.description,productcode,
		     #current_committed_rate,as_nos,dir_traffic
		     #FROM ipc_china
		     #left outer join ipt_master on ipc_china.ipc_servid = ipt_master.ipc_servid
		     #left outer join service on service.master_serviceid=ipt_master.master_sid
		     #where master_sid is null COND order by ipc_china.ipc_servid)");	

	##Karuna on 16-11-2010 for bug fix IPT4a
	$command =("select distinct a.ipc_servid, c.master_sid, d.serviceid, a.customer, a.description, a.productcode, current_committed_rate, as_nos,dir_traffic, committed_stdt, dir_effective_date from ipc_china a join (select ipc_servid , max(committed_stdt) as lastupdate from ipc_china group by ipc_servid) b on (a.ipc_servid = b.ipc_servid and a.committed_stdt = b.lastupdate) left outer join (select master_sid,ipc_servid from ipt_master)c on (a.ipc_servid=c.ipc_servid) left outer join (select serviceid, master_serviceid from service) d on (c.master_sid=d.master_serviceid) COND");

	## Search parameters
	if ($debug){print IPC1 "productcode:$productcode\n";}
	if ($productcode ne "") {
		if ($cond !~ /WHERE/){
			$cond .= "WHERE productcode like '$productcode' ";
		} else {
			$cond .= "AND productcode like '$productcode' ";
		}
	}
	if ($serviceid ne "") {
		if ($cond !~ /WHERE/){
                        $cond .= "WHERE a.ipc_servid ilike '%$serviceid%' ";
		} else {
			$cond.= "AND a.ipc_servid ilike '%$serviceid%' ";
		}
	}
	if ($debug){print IPC1 "accno:$accno\n";}
	if ($accno ne "") {
		if ($cond !~ /WHERE/){
                        $cond .= "WHERE a.customer ilike '$accno' ";
		} else {	
                	$cond.= "AND a.customer ilike '$accno' ";
		}
	}
	if ($description ne "") {
		if ($cond !~ /WHERE/){
                        $cond .= "WHERE a.description ilike '%$description%' ";
		} else {
			$cond.= "AND a.description ilike '%$description%' ";
		}
	}
	if ($committed_rate  ne "") {
		if ($cond !~ /WHERE/){
                        $cond .= "WHERE a.current_committed_rate ilike '$committed_rate' ";
		} else {
                	$cond.= "AND a.current_committed_rate ilike '$committed_rate' ";
		}
	}
	if ($as_no ne "") {
		if ($cond !~ /WHERE/){
                        $cond .= "WHERE a.as_nos ilike '$as_no' ";
		} else {
                	$cond.= "AND a.as_nos ilike '$as_no' ";
		}
	}
	if ($dir_traffic ne "null") {
		if ($cond !~ /WHERE/){
                        $cond .= "WHERE a.dir_traffic like '$dir_traffic' ";
		} else {
                	$cond.= "AND a.dir_traffic like '$dir_traffic' ";
		}
	}
	## substitute in the query
	$command =~ s/COND/$cond/g;
	if ($debug) {print IPC1 "DB Search command:$command\n";}
	#CMS::add_error($command);

  	my $sth = $dbh->prepare($command);
  	unless ($sth->execute) {
        	$error = $dbh->errstr;
        	return 0;
  	}

  	my $result = $sth->fetchall_arrayref;
  	if ($result) {
        	$error = $dbh->errstr;
        	$sth->finish;
        	return $result;
  	} else {
        	$error = $dbh->errstr;
        	return 0;
  	}
	
}

################################################################
# Name          : update_service 
# Description   : Updates the service details in the DB
# Author        : Karuna Ballal
# Input         : all service parameters
# Outupt        : 
################################################################
sub update_service {
	CMS_DB::connect_to_database();
	CMS_DB::begin_transaction();
	## front end parameters
	my $serviceid = $_[0];
	my $ip_serv = $dbh->quote($serviceid);
        my $accno = $_[1];
        my $description = $_[2];
        my $committed_rate = $_[3];
        my $dir_traffic = $_[4];
        my $as_no = $_[5];
	my $eff_dir_date = $_[6];
	if ($debug) {print IPC1 "DB input update:serviceid:$serviceid\taccno:$accno\tdescription:$description\tcommitted_rate:$committed_rate\tdir_traffic:$dir_traffic\tas_no:$as_no\teff_dir_date:$eff_dir_date\n";}
	
	my $edit_query = ("SELECT DISTINCT ON (ipc_servid) ipc_servid,customer,description,current_committed_rate,dir_traffic,as_nos,dir_effective_date from ipc_china where ipc_servid like $ip_serv order by ipc_servid,dir_effective_date DESC");
	my $sth = $dbh->prepare($edit_query);
	if ($debug){print IPC1 "DB Update edit_query:$edit_query\n";}
	$sth->execute();
	if ($debug){print IPC1 "DB Update sth:$sth\n";}
	$error = $dbh->errstr;
	if ($debug){print IPC1 "DB update error:$error\n";}

	## DB values
	my ($db_servid,$db_acc,$db_desc,$db_comm_rt,$db_dir_traffic,$db_asnum,$db_dir_effdt);
	my @edit_db = $sth->fetchrow_array;
	$sth->finish;
	$db_servid = $edit_db[0];
	$db_acc = $edit_db[1];
	$db_desc  = $edit_db[2];
	$db_comm_rt = $edit_db[3];
	$db_dir_traffic = $edit_db[4];
	$db_asnum = $edit_db[5];
	$db_dir_effdt = $edit_db[6];
	if ($debug){print IPC1 "DB update db_servid:$db_servid\tdb_acc:$db_acc\tdb_desc:$db_desc\tdb_comm_rt:$db_comm_rt\tdb_dir_traffic:$db_dir_traffic\tdb_asnum:$db_asnum\tdb_dir_effdt:$db_dir_effdt\n";}

	if ($dir_traffic eq $db_dir_traffic){
		#No change in the direct/indirect status
		if ($debug){print IPC1 "DB update NO CHANGE IN DIR TRAFFIC\n";}
		if ($db_comm_rt eq $committed_rate) {

			#no change in committed rate
			my $update_query = ("UPDATE ipc_china SET description='$description' where ipc_servid='$db_servid' AND customer='$db_acc' AND current_committed_rate='$db_comm_rt' AND dir_traffic='$db_dir_traffic' AND as_nos='$as_no' AND dir_effective_date='$db_dir_effdt'");
			if ($db_dir_effdt eq ""){ 
			$update_query = ("UPDATE ipc_china SET description='IP Transit ph4a test service1' where ipc_servid='phkgripc90001' AND customer='REACH' AND current_committed_rate='555555' AND dir_traffic='indirect' AND as_nos='3434' AND dir_effective_date is null");
			if ($debug){print IPC1 "DB update same committed rate update_query:$update_query\n";}
			} 
			if ($debug){print IPC1 "DB update same committed rate update_query:$update_query\n";}
			my $sth = $dbh->prepare($update_query);
        		unless ($sth->execute) {
                		$error = $dbh->errstr;
                		return $error;
        		}
		} else {

			#change in committed rate
			my $update_query = ("UPDATE ipc_china SET description='$description', previous_committed_rate='$db_comm_rt', current_committed_rate='$committed_rate', committed_stdt = to_timestamp((current_date+integer '1'),'YYYY-MM-DD HH:MI:SS.US') WHERE ipc_servid='$db_servid' AND customer='$db_acc' AND dir_traffic='$db_dir_traffic' AND as_nos='$as_no' AND dir_effective_date='$db_dir_effdt'");
			if ($debug){print IPC1 "DB update new committed rate update_query:$update_query\n";}
			my $sth = $dbh->prepare($update_query);
        		unless ($sth->execute) {
                		$error = $dbh->errstr;
                		return $error;
        		}
		}
	} else {

		#change in the direct/indirect status
		if ($debug){print IPC1 "DB update CHANGE IN DIR TRAFFIC\n";}
		if ($dir_traffic eq "direct"){
			my $sth2 = $dbh_tr->prepare ("SELECT as_no from service where as_no like $as_no AND productcodeid like 'GIA'");
                	if ($debug){print IPC1 "DB query:SELECT as_no from service where as_no like $as_no AND productcodeid like 'GIA'\n";}
                	$sth2->execute();
                	my $row2 = $sth2->rows();

                	if($row2 > 0) {
                        	return "AS Number already in use by a GIA service.";
                	}
        	}

		## insert a new record in case of change in dir traffic status
		my $insert_query = ("INSERT INTO ipc_china (ipc_servid,customer,description,current_committed_rate,previous_committed_rate,committed_stdt,dir_traffic,as_nos,dir_effective_date,productcode) VALUES ($ip_serv,'$accno','$description','$committed_rate','$db_comm_rt',to_timestamp((current_date+integer '1'),'YYYY-MM-DD HH:MI:SS.US'),'$dir_traffic',$db_asnum,to_timestamp((current_date+integer '1'),'YYYY-MM-DD HH:MI:SS.US'),'IPTRANSIT')");
		if ($debug){print IPC1 "DB update insert_query:$insert_query\n";}
		my $sth = $dbh->prepare($insert_query);
        	unless ($sth->execute) {
                	$error = $dbh->errstr;
                	return $error;
        	}
        }
	return 1;
	
}


################################################################
# Name          : get_del_details 
# Description   : get all service details to populate the hidden parameter on click of "Delete" button 
# Author        : Karuna Ballal
# Input         : service id 
# Outupt        : all service parameters from the DB 
################################################################
sub get_del_details {

	my $service = shift(@_);

	my $del_query = ("SELECT DISTINCT ON (ipc_servid) as_nos, current_committed_rate, dir_effective_date, description, previous_committed_rate, committed_stdt FROM ipc_china WHERE ipc_servid like '$service' order by ipc_servid,dir_effective_date DESC");
	if ($debug){print IPC1 "DB DELETE get details: del_query:$del_query\n";}
	my $sth = $dbh->prepare($del_query);
	if ($debug){print IPC1 "DB DELETE before unless\n";}
	unless ($sth->execute) {
		$error = $dbh->errstr;
		if ($debug){print IPC1 "DB DELETE error:$error\n";}
		return $error;
	}

	my @retStr = $sth->fetchrow_array;
	if ($debug){print IPC1 "DB DELETE retStr array:@retStr\n";}
	
	## return string of parameters as hidden values
	my $retStr = join ',',@retStr;
	$sth->finish;
	return $retStr;
}

#close (IPC1);	
1;
