#! /usr/local/bin/perl
########################################################################
# File        : CMS_MASTER.pm
# Description : Perl Module to handle the paramters from the template files and the queries in CMS_MASTER_DB.pm 
# Parameters to be passed : Master Service ID, description, cos type (IP Transit), rate type(IP Transit), ACDR (IP Transit)
# # Modification History :
#  Date               Name                 Change/Description
# ------------------------------------------------------------------
# 09-SEP-2010   Karuna Ballal           Modified for IP Transit
# 28-SEP-2010   Chandini                Modified for IP Transit
#######################################################################


package CMS_MASTER;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_MASTER_DB;
# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;


sub start {
#	print "Content-type: text/html\n\n";

	my $ipt_msid = $CMS::html_values{master_serviceid}{value};
	if ((not defined($CMS::html_values{command}{value})) || ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} =~ /cancel/i))) {
		# default to add_master_code
		CMS::clear_html_values();
		$CMS::html_values{subsystem}{value} = "master";
		$CMS::html_values{command}{value} = "add_master";
	}

	if (defined $CMS::html_values{stage}{value}) {
    		unless (CMS::check_screen($CMS::html_values{current_screen}{value})){
      			$CMS::html_values{stage}{value}--;
    		}
  	}

	if($CMS::html_values{command}{value} eq "add_master") {

		#Added by Karuna for IP Transit
		#CoS Type dropdown values from the DB
		$CMS::html_values{costype}{options} = CMS_MASTER_DB::get_ipt_costype();
		#Current Rate Type dropdown values from the DB
		$CMS::html_values{ratetype}{options} = CMS_MASTER_DB::get_ipt_ratetype();
		#Chandini_IPT
		#Current IPC  services dropdown values from the DB
		$CMS::html_values{ipcservice}{options} = CMS_MASTER_DB::get_ipt_ipcservice();
		unshift(@{$CMS::html_values{ipcservice}{options}}, ["-- Select --"]);

		# Add a new aggregate service
    		if (not defined ($CMS::html_values{submit}{value})) {  
      			CMS::output_html("master_add");
		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Add New Master Service" )) {
      			$CMS::html_values{master_serviceid}{value} =~ s/\s+//g;
			$CMS::html_values{master_service}{value} =~ s/( |\[|\])//g;
			$CMS::html_values{master_serviceid}{value} =~ tr/A-Z/a-z/;
      			$CMS::html_values{description}{value} =~ s/^\s+|\s+$//g;
      			if($CMS::html_values{master_serviceid}{value} =~ /^$/){
        			CMS::add_error("Failed to add New Master Service.");
       				CMS::add_error("Master Service cannot be null or consist of whitespace characters only.");
        			CMS::output_html("master_add");
      			} else {
				my $res = "1";
#Pattern match added for ipm-TGC0032462
				if ($ipt_msid =~ /ipm/i) {
					$res = CMS_MASTER_DB::add_master_service(
					$CMS::html_values{master_serviceid}{value},
					$CMS::html_values{description}{value},
					#Chandini_IPT
					$CMS::html_values{ipcservice}{value},
					$CMS::html_values{costype}{value},
					$CMS::html_values{ratetype}{value},
					$CMS::html_values{acdr_t}{value},
					); 
				} else {
        				$res = CMS_MASTER_DB::add_master_service($CMS::html_values{master_serviceid}{value}, $CMS::html_values{description}{value});
				}
				
				if ($res == "1") {
			        	CMS::add_error("Master Service added successfully.");
          				CMS::output_html("master_add");
				} else {
					CMS::add_error("Failed to add Master Service.");
					CMS::add_error("$res");
          				CMS::output_html("master_add");
	       			}
          			#CMS::output_html("master_add");
			}
		}
	} elsif ($CMS::html_values{command}{value} eq "edit_master") {

		#Added by Karuna for IP Transit
		#CoS Type dropdown values from the DB
		$CMS::html_values{costype}{options} = CMS_MASTER_DB::get_ipt_costype();
		#Chandini_IPT
		#IPC Service dropdown values from the DB
		$CMS::html_values{ipcservice}{options} = CMS_MASTER_DB::get_ipt_ipcservice();
		#Rate Type dropdown values from the DB
		$CMS::html_values{ratetype}{options} = CMS_MASTER_DB::get_ipt_ratetype();
		#Proposed Rate Type dropdown values from the DB
		$CMS::html_values{pro_ratetype}{options} = CMS_MASTER_DB::get_ipt_ratetype();
		unshift(@{$CMS::html_values{pro_ratetype}{options}}, ["-- Select --"]);

    		# Edit an existing master service  
    		if (not defined ($CMS::html_values{submit}{value})) {
      			# first stage
      			if(my $codes = CMS_MASTER_DB::get_master_serviceid) {
        			$CMS::html_values{master_serviceid}{options} = $codes;  
			        CMS::output_html("master_edit"); 
      			} else {  
        			CMS::add_error("No master service exist.");
        			CMS::output_html("master_edit");
			}
		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Edit Master Service" )) { 
     	 		my $result = CMS_MASTER_DB::get_description($CMS::html_values{master_serviceid}{value});
			if ($result ne "0") {
				## begin IP transit specific code
				if ($ipt_msid =~ /ipm/i) {
        				$CMS::html_values{master_serviceid}{value} = $CMS::html_values{master_serviceid}{value};
        				$CMS::html_values{description}{value} = @$result[0];
					#Chandini_IPT
        				$CMS::html_values{ipcservice}{value} = @$result[1];
					$CMS::html_values{costype}{value} = @$result[2];
					$CMS::html_values{ratetype}{value} = @$result[3];
					$CMS::html_values{acdr_t}{value} = @$result[4];
					my $curr_stdt = @$result[5];
					$CMS::html_values{'curr_date.year'}{value} = substr($curr_stdt,0,4);
					$CMS::html_values{'curr_date.month'}{value} = substr($curr_stdt,5,2);
					$CMS::html_values{'curr_date.day'}{value} = substr($curr_stdt,8,2);
					$CMS::html_values{acdreff}{value} = @$result[6];
					my $pro_stdt = @$result[7];
					$CMS::html_values{'pro_date.year'}{value} = substr($pro_stdt,0,4);
                                        $CMS::html_values{'pro_date.month'}{value} = substr($pro_stdt,5,2);
                                        $CMS::html_values{'pro_date.day'}{value} = substr($pro_stdt,8,2);
					$CMS::html_values{pro_ratetype}{value} = @$result[8];
					#$CMS::html_values{curr_date.year}\t$CMS::html_values{curr_date.month}\t$CMS::html_values{curr_date.day}\n";
				## End of Ip transit specific code
				} else {
					$CMS::html_values{description}{value} = $result;
				}
       	 			CMS::output_html("master_edit2");
      			} else {
       	 			# aggregate service does not exist
        			CMS::add_error("The Master Service entered does not exists.");
        			CMS::output_html("master_edit");
      			}
		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Update Master Service" )) {
			## Begin IP Transit specific Edit Master Code
			if ($ipt_msid =~ /ipm/i) {
				my $acdr_db;

				#Current time in GMT
				my ($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = gmtime;
				my $year = 1900 + $yearOffset;
				$month = $month + 1;
				if ($month <= 9){
        				$month = "0".$month;
				}
				if ($dayOfMonth <= 9){
        				$dayOfMonth = "0".$dayOfMonth;
				}
				#Take a day later's date for validation purpose
				my $day = $dayOfMonth + 1;
				if ($day <= 9){
                                        $day  = "0".$day;
				}
				#Current Date
				#my $sys_date = $year."-".$month."-".$day;
				my $sys_date = $year."-".$month."-".$day;
				my $proposed_date = $CMS::html_values{'pro_date.year'}{value}."-".$CMS::html_values{'pro_date.month'}{value}."-".$CMS::html_values{'pro_date.day'}{value}; 
				my $ipm_result;

				if (($CMS::html_values{pro_ratetype}{value} == "95percentile") && ($CMS::html_values{acdreff}{value} eq "")){
					CMS::add_error("Failed to edit Master Service.");
                                        CMS::add_error("Proposed ACDR value mandatory for Proposed Rate Type 95 percentile.");
					my $result = CMS_MASTER_DB::get_description($CMS::html_values{master_serviceid}{value});
					$CMS::html_values{master_serviceid}{value} = $CMS::html_values{master_serviceid}{value};
					$CMS::html_values{description}{value} = @$result[0];
					#Chandini_IPT
					$CMS::html_values{ipcservice}{value} = @$result[1];
					$CMS::html_values{costype}{value} = @$result[2];
					$CMS::html_values{ratetype}{value} = @$result[3];
					$CMS::html_values{acdr_t}{value} = @$result[4];
					my $curr_stdt = @$result[5];
					$CMS::html_values{'curr_date.year'}{value} = substr($curr_stdt,0,4);
					$CMS::html_values{'curr_date.month'}{value} = substr($curr_stdt,5,2);
					$CMS::html_values{'curr_date.day'}{value} = substr($curr_stdt,8,2);
					CMS::output_html("master_edit2");
				} 
				elsif (($CMS::html_values{pro_ratetype}{value} !~ /-- Select --/) && ($proposed_date lt $sys_date)) {
					CMS::add_error("Failed to edit Master Service.");
                                        CMS::add_error("Invalid Proposed Effective Date.");
					my $result = CMS_MASTER_DB::get_description($CMS::html_values{master_serviceid}{value});
					$CMS::html_values{master_serviceid}{value} = $CMS::html_values{master_serviceid}{value};
					$CMS::html_values{ipcservice} {value} = $CMS::html_values{'ipcservice'}{value};
					$CMS::html_values{costype}{value} = $CMS::html_values{'existing.costype'}{value};
					$CMS::html_values{ratetype}{value} = @$result[2];
					$CMS::html_values{acdr_t}{value} = @$result[3];
					my $curr_stdt = @$result[4];
					$CMS::html_values{'curr_date.year'}{value} = substr($curr_stdt,0,4);
					$CMS::html_values{'curr_date.month'}{value} = substr($curr_stdt,5,2);
					$CMS::html_values{'curr_date.day'}{value} = substr($curr_stdt,8,2);
					
					CMS::output_html("master_edit2");
					
				} elsif (($CMS::html_values{acdreff}{value} ne "") && ($CMS::html_values{acdreff}{value} !~ /(^(\d+\.\d+|\d+\.|\.\d+)$)|(^\d+$)/)){
					CMS::add_error("Failed to edit Master Service.");
                                        CMS::add_error("Invalid Proposed ACDR format.");
					my $result = CMS_MASTER_DB::get_description($CMS::html_values{master_serviceid}{value});
                                        $CMS::html_values{master_serviceid}{value} = $CMS::html_values{master_serviceid}{value};
                                        $CMS::html_values{description}{value} = @$result[0];
					#Chandini_IPT
                                        $CMS::html_values{ipcservice}{value} = @$result[1];
                                        $CMS::html_values{costype}{value} = @$result[2];
                                        $CMS::html_values{ratetype}{value} = @$result[3];
                                        $CMS::html_values{acdr_t}{value} = @$result[4];
                                        my $curr_stdt = @$result[5];
                                        $CMS::html_values{'curr_date.year'}{value} = substr($curr_stdt,0,4);
                                        $CMS::html_values{'curr_date.month'}{value} = substr($curr_stdt,5,2);
                                        $CMS::html_values{'curr_date.day'}{value} = substr($curr_stdt,8,2);

                                        CMS::output_html("master_edit2");

				} else {
					$ipm_result = CMS_MASTER_DB::update_description(
					$CMS::html_values{master_serviceid}{value},
					$CMS::html_values{description}{value},
					#Chandini_IPT
					$CMS::html_values{ipcservice}{value},
					$CMS::html_values{costype}{value},
					$CMS::html_values{pro_ratetype}{value},
					$CMS::html_values{acdreff}{value},
					$CMS::html_values{'pro_date.year'}{value},
					$CMS::html_values{'pro_date.month'}{value},
					$CMS::html_values{'pro_date.day'}{value}
					);
					if ($ipm_result == 1) {
						my $codes = CMS_MASTER_DB::get_master_serviceid;
                                		$CMS::html_values{master_serviceid}{options} = $codes;
                                		CMS::add_error("The Master Service updated successfully.");
                        	        	CMS::output_html("master_edit");
					} elsif ($ipm_result == 0) {
						CMS::add_error("Failed to edit Master Service.");
						CMS::add_error($CMS_MASTER_DB::error);
						my $result = CMS_MASTER_DB::get_description($CMS::html_values{master_serviceid}{value});
                                        	$CMS::html_values{master_serviceid}{value} = $CMS::html_values{master_serviceid}{value};
                                        	$CMS::html_values{description}{value} = @$result[0];
						#Chandini_IPT
                                        	$CMS::html_values{ipcservice}{value} = @$result[1];
                                        	$CMS::html_values{costype}{value} = @$result[2];
                                        	$CMS::html_values{ratetype}{value} = @$result[3];
                                        	$CMS::html_values{acdr_t}{value} = @$result[4];
                                        	my $curr_stdt = @$result[5];
                                        	$CMS::html_values{'curr_date.year'}{value} = substr($curr_stdt,0,4);
                                        	$CMS::html_values{'curr_date.month'}{value} = substr($curr_stdt,5,2);
                                        	$CMS::html_values{'curr_date.day'}{value} = substr($curr_stdt,8,2);

						CMS::output_html("master_edit2");
					}
				} 
			}
			## End of IP Transit specific Edit Master Code
			else {
      			if(CMS_MASTER_DB::update_description($CMS::html_values{master_serviceid}{value}, $CMS::html_values{description}{value}))    {
        			my $codes = CMS_MASTER_DB::get_master_serviceid;
        			$CMS::html_values{master_serviceid}{options} = $codes;
        			CMS::add_error("The Master Service updated successfully.");
        			CMS::output_html("master_edit");
      			} else {
        			CMS::add_error("Failed to edit Master Service.");
        			CMS::add_error($CMS_MASTER_DB::error);
        			CMS::output_html("master_edit2");   
      				}
			}

		}
		
		  }elsif ($CMS::html_values{command}{value} eq "delete_master") {

    		if (not defined ($CMS::html_values{stage}{value})) {
      			# first stage
      			if(my $codes = CMS_MASTER_DB::get_master_serviceid){
        			$CMS::html_values{master_serviceid}{options} = $codes;
        			CMS::output_html("master_delete");
      			} else {
        			CMS::add_error("No Master Service exists.");
        			CMS::output_html("master_delete");
      			}
		 } elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Delete Master Service" )) {
      			# second stage
			my $res = CMS_MASTER_DB::delete_master($CMS::html_values{master_serviceid}{value});
			if ($res == 1) {

			my $error = $CMS_MASTER_DB::error;
			CMS::add_error($error);
        			CMS::add_error("Master Service deleted successfully.");
				if(CMS_MASTER_DB::get_master_serviceid){
        			   $CMS::html_values{master_serviceid}{options} = CMS_MASTER_DB::get_master_serviceid;
				}
        			CMS::output_html("master_delete");
      			} else {
        			my $error = $CMS_MASTER_DB::error;
                                CMS::add_error("Deletion failed.");
                                CMS::add_error("There is active service linked with the master service ID");
        			$CMS::html_values{master_serviceid}{options} = CMS_MASTER_DB::get_master_serviceid;
        			CMS::output_html("master_delete");
      			}

    		}
	        	
}
  	  elsif ($CMS::html_values{command}{value} eq "list_master") {
                # list out existing master services
                if (not defined ($CMS::html_values{stage}{value})) {
                # first stage

      			if(my $codes = CMS_MASTER_DB::get_allmaster_serviceid) {
        			$CMS::html_values{master_serviceid}{options} = $codes;
                        	CMS::add_error("Click below to list Master Services.");
        			CMS::output_html("master_list");
      			} else {
        			CMS::add_error("No Master Service exists.");
        			CMS::output_html("master_list");
      			}


		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "List Master Service" )) {
			my $epl_msid = $CMS::html_values{master_serviceid}{value};
			
			$CMS::html_values{master}{table} = CMS_MASTER_DB::list_masterservice($CMS::html_values{master_serviceid}{value});
			if ($epl_msid =~ /emm/i){
				unshift(@{$CMS::html_values{master}{table}}, ["Master Service ID","Service ID","Acc No."]);
				}elsif ($ipt_msid =~ "ipm") {
				#Chandini_IPT
				unshift(@{$CMS::html_values{master}{table}}, ["Master Service ID","IPC Service ID","Service ID","Customer","CoS Type","Current Rate Type","Current ACDR","Current Effective Date","Proposed Rate Type","Proposed ACDR","Proposed Effective Date"]);			
				} else {
				unshift(@{$CMS::html_values{master}{table}}, ["Master Service ID","Aggregate Service ID","Service ID","Acc No."]);
				}
				$CMS::html_values{master}{header_rows} = 1;
				my $codes = CMS_MASTER_DB::get_allmaster_serviceid;
				$CMS::html_values{master_serviceid}{options} = $codes;
        	                CMS::output_html("master_list2");
		}						

	}

}


1
