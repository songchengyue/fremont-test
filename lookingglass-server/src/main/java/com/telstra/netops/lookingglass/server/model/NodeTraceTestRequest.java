package com.telstra.netops.lookingglass.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NodeTraceTestRequest {
	
	@JsonProperty
	private String level;
	
	@JsonProperty
	private String pop;
	
	@JsonProperty
	private String toHost;
	
	@JsonProperty
	private String submit;
	
	@JsonProperty
	public String customerId;
	
	@JsonProperty
	public String product;
	
	@JsonProperty
	private Integer numPings;

	public NodeTraceTestRequest() {
	}	
	
	public NodeTraceTestRequest(String level, String customerId, String product, String pop, String toHost,
			String submit, Integer numPings) {
		super();
		this.level = level;
		this.customerId =customerId;
		this.product = product;
		this.pop = pop;
		this.toHost = toHost;
		this.submit = submit;
		this.numPings = numPings;
	}
	


	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getPop() {
		return pop;
	}

	public void setPop(String pop) {
		this.pop = pop;
	}

	public String getToHost() {
		return toHost;
	}

	public void setToHost(String toHost) {
		this.toHost = toHost;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}
	
	
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}
		
	public Integer getNumPings() {
		return numPings;
	}

	public void setNumPings(Integer numPings) {
		this.numPings = numPings;
	}
	

}
