package com.telstra.netops.lookingglass.server.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telstra.netops.lookingglass.server.model.ProductLine;
import com.telstra.netops.lookingglass.server.repository.ServiceRepository;
import com.telstra.netops.lookingglass.server.repository.ToolAccessRepository;

@Service
public class ProductToolMappingService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductToolMappingService.class);
	
	@Autowired
	private ServiceRepository serviceRepository;
	
	@Autowired
	private ToolAccessRepository toolAccessRepository;
	
	public List<ProductLine> getProductLines(String customerId) {
		return serviceRepository.findDistinctProductLineByCustomerId(customerId);
	}
	
	public List<String> getTools(String productLine) {
		return toolAccessRepository.findByProductLine(productLine);
	}

}
