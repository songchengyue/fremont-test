# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 15 October 2001
# File: CMS_TROUBLE_TICKET_DB.pm
#
# This module contains subroutines for the trouble ticket subroutine
#
# $Id: CMS_TROUBLE_TICKET_DB.pm,v 1.1.1.1 2003/10/19 13:49:52 rxc Exp $

package CMS_TROUBLE_TICKET_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;

our @ISA        = qw(Exporter);
our $error;

sub add_trouble_ticket {
  my $contact_name = $dbh->quote(shift @_);
  my $contact_phone = $dbh->quote(shift @_);
  my $contact_email = $dbh->quote(shift @_);
  my $company_name = $dbh->quote(shift @_);
  my $accno = $dbh->quote(shift @_);
  my $opshandle = $dbh->quote(shift @_);
  my $fault = $dbh->quote(shift @_);

  my $sth = $dbh->prepare("SELECT NEXTVAL ('trouble_ticket_sequence')");
  $sth->execute;
  my $trackno = ($sth->fetchrow_array)[0];
  $sth->finish;

  $sth = $dbh->prepare
    ("insert into trouble_ticket(trackno, 
                                 contact_name, contact_phone, contact_email,
                                 company_name, accno, opshandle, fault,
                                 reported, status)
      values($trackno,   $contact_name, $contact_phone, $contact_email,
                         $company_name, $accno, $opshandle, $fault,
                         CURRENT_TIMESTAMP, 1)");

  my $result = $sth->execute;
  $error = $dbh::err;
  if ($result) { $result = $trackno; }
  return $result;

}

sub get_current_trouble_tickets {
  my $sth = $dbh->prepare
    ("SELECT trackno, reported, accno, opshandle, statustext, 
             substring(fault for 40)
      FROM trouble_ticket 
           NATURAL JOIN trouble_ticket_status
      WHERE status < 3");

  if ($sth->execute) {
    my $result = $sth->fetchall_arrayref;
    $error = $dbh::err;
    return $result;
  } else {
    $error = $dbh::err;
    return 0;
  } 
}

sub get_current_trouble_tickets_for_account {
  my $accno = shift @_;
  my $sth = $dbh->prepare
    ("SELECT trackno, reported, accno, opshandle, statustext, 
             substring(fault for 40)
      FROM trouble_ticket 
           NATURAL JOIN trouble_ticket_status
      WHERE status < 3
        AND accno = '$accno'");

  if ($sth->execute) {
    my $result = $sth->fetchall_arrayref;
    $error = $dbh::err;
    return $result;
  } else {
    $error = $dbh::err;
    return 0;
  } 
}

sub search_trouble_tickets {
  my $trackno = shift @_;
  my $reported_start = shift @_;
  my $reported_end = shift @_;
  my $accno = shift @_;
  my $opshandle = shift @_;
  my $status = shift @_;
  my $fault = shift @_;

  my $sql_command = 
    ("SELECT trackno, reported, accno, opshandle, statustext, 
             substring(fault for 40)
      FROM trouble_ticket
           NATURAL JOIN trouble_ticket_status
      WHERE reported >= '$reported_start' 
        AND reported <= '$reported_end' ");

  if ($trackno ne "") {$sql_command .= "AND trackno LIKE '%$trackno%' " };
  if ($accno ne "") {$sql_command .= "AND accno LIKE '%$accno%' " };
  if ($opshandle ne ""){$sql_command.= "AND opshandle LIKE '%$opshandle%' " };
  if ((defined $status) && ($status ne "")) {$sql_command .= "AND status LIKE '%$status%' " };
  if ($fault ne "") {$sql_command .= "AND fault LIKE '%$fault%' " };  
  
  my $sth = $dbh->prepare($sql_command);

  if ($sth->execute) {
    my $result = $sth->fetchall_arrayref;
    $error = $dbh::err;
    return $result;
  } else {
    $error = $dbh::err;
    return 0;
  } 
}

sub get_trouble_ticket {
  my $trackno = $dbh->quote(shift @_);
  
  my $sth = $dbh->prepare
    ("SELECT trackno, contact_name, contact_phone, contact_email,
             company_name, accno, opshandle, fault, reported, status, fault
      FROM trouble_ticket 
      WHERE trackno = $trackno");

  if ($sth->execute) {
    my @result = $sth->fetchrow_array;
    $error = $dbh::err;
    $sth->finish;
    return \@result;
  } else {
    $error = $dbh::err;
    return 0;
  } 
}

sub update_status {
  my $trackno = $dbh->quote(shift @_);
  my $status = shift @_;

  my $sth = $dbh->prepare
    ("update trouble_ticket
      set status = $status
      where trackno = $trackno");
  
  my $result = $sth->execute;
  $error = $dbh::err;
  return $result;
}

sub add_comment {
  my $trackno = $dbh->quote(shift @_);
  my $username = $dbh->quote(shift @_);
  my $comment = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("insert into trouble_ticket_comment
             (trackno, username, comment_time, comment)
       values($trackno, $username, CURRENT_TIMESTAMP, $comment)");

  my $result = $sth->execute;
  $error = $dbh::err;
  return $result;
}

sub get_comments {
  my $trackno = $dbh->quote(shift @_);
  
  my $sth = $dbh->prepare
    ("SELECT username, comment_time, comment
      FROM trouble_ticket_comment 
      WHERE trackno = $trackno");

  if ($sth->execute) {
    my $result = $sth->fetchall_arrayref;
    $error = $dbh::err;
    return $result;
  } else {
    $error = $dbh::err;
    return 0;
  } 
}

sub get_status {
  my $sth = $dbh->prepare
    ("SELECT status, statustext FROM trouble_ticket_status");

  if ($sth->execute) {
    my $result = $sth->fetchall_arrayref;
    $error = $dbh::err;
    return $result;
  } else {
    $error = $dbh::err;
    return 0;
  } 
}

1;
