/*
 * XML Type:  UcmdbClass
 * Namespace: http://schemas.hp.com/ucmdb/1/types/classmodel
 * Java type: com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.classmodel.impl;
/**
 * An XML UcmdbClass(@http://schemas.hp.com/ucmdb/1/types/classmodel).
 *
 * This is a complex type.
 */
public class UcmdbClassImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass
{
    
    public UcmdbClassImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "name");
    private static final javax.xml.namespace.QName CLASSTYPE$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "classType");
    private static final javax.xml.namespace.QName DISPLAYLABEL$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "displayLabel");
    private static final javax.xml.namespace.QName DESCRIPTION$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "description");
    private static final javax.xml.namespace.QName PARENTNAME$8 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "parentName");
    private static final javax.xml.namespace.QName QUALIFIERS$10 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "qualifiers");
    private static final javax.xml.namespace.QName ATTRIBUTES$12 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "attributes");
    
    
    /**
     * Gets the "name" element
     */
    public java.lang.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "name" element
     */
    public org.apache.xmlbeans.XmlString xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "name" element
     */
    public void setName(java.lang.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NAME$0);
            }
            target.setStringValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "name" element
     */
    public void xsetName(org.apache.xmlbeans.XmlString name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NAME$0);
            }
            target.set(name);
        }
    }
    
    /**
     * Gets the "classType" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType.Enum getClassType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLASSTYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "classType" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType xgetClassType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType)get_store().find_element_user(CLASSTYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "classType" element
     */
    public void setClassType(com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType.Enum classType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLASSTYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLASSTYPE$2);
            }
            target.setEnumValue(classType);
        }
    }
    
    /**
     * Sets (as xml) the "classType" element
     */
    public void xsetClassType(com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType classType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType)get_store().find_element_user(CLASSTYPE$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType)get_store().add_element_user(CLASSTYPE$2);
            }
            target.set(classType);
        }
    }
    
    /**
     * Gets the "displayLabel" element
     */
    public java.lang.String getDisplayLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DISPLAYLABEL$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "displayLabel" element
     */
    public org.apache.xmlbeans.XmlString xgetDisplayLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DISPLAYLABEL$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "displayLabel" element
     */
    public void setDisplayLabel(java.lang.String displayLabel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DISPLAYLABEL$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DISPLAYLABEL$4);
            }
            target.setStringValue(displayLabel);
        }
    }
    
    /**
     * Sets (as xml) the "displayLabel" element
     */
    public void xsetDisplayLabel(org.apache.xmlbeans.XmlString displayLabel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DISPLAYLABEL$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DISPLAYLABEL$4);
            }
            target.set(displayLabel);
        }
    }
    
    /**
     * Gets the "description" element
     */
    public java.lang.String getDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "description" element
     */
    public org.apache.xmlbeans.XmlString xgetDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPTION$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "description" element
     */
    public void setDescription(java.lang.String description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPTION$6);
            }
            target.setStringValue(description);
        }
    }
    
    /**
     * Sets (as xml) the "description" element
     */
    public void xsetDescription(org.apache.xmlbeans.XmlString description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPTION$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DESCRIPTION$6);
            }
            target.set(description);
        }
    }
    
    /**
     * Gets the "parentName" element
     */
    public java.lang.String getParentName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PARENTNAME$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "parentName" element
     */
    public org.apache.xmlbeans.XmlString xgetParentName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PARENTNAME$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "parentName" element
     */
    public void setParentName(java.lang.String parentName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PARENTNAME$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PARENTNAME$8);
            }
            target.setStringValue(parentName);
        }
    }
    
    /**
     * Sets (as xml) the "parentName" element
     */
    public void xsetParentName(org.apache.xmlbeans.XmlString parentName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PARENTNAME$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PARENTNAME$8);
            }
            target.set(parentName);
        }
    }
    
    /**
     * Gets the "qualifiers" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers getQualifiers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers)get_store().find_element_user(QUALIFIERS$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "qualifiers" element
     */
    public boolean isSetQualifiers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(QUALIFIERS$10) != 0;
        }
    }
    
    /**
     * Sets the "qualifiers" element
     */
    public void setQualifiers(com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers qualifiers)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers)get_store().find_element_user(QUALIFIERS$10, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers)get_store().add_element_user(QUALIFIERS$10);
            }
            target.set(qualifiers);
        }
    }
    
    /**
     * Appends and returns a new empty "qualifiers" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers addNewQualifiers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers)get_store().add_element_user(QUALIFIERS$10);
            return target;
        }
    }
    
    /**
     * Unsets the "qualifiers" element
     */
    public void unsetQualifiers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(QUALIFIERS$10, 0);
        }
    }
    
    /**
     * Gets the "attributes" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Attributes getAttributes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Attributes target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Attributes)get_store().find_element_user(ATTRIBUTES$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "attributes" element
     */
    public boolean isSetAttributes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ATTRIBUTES$12) != 0;
        }
    }
    
    /**
     * Sets the "attributes" element
     */
    public void setAttributes(com.hp.schemas.ucmdb._1.types.classmodel.Attributes attributes)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Attributes target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Attributes)get_store().find_element_user(ATTRIBUTES$12, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.Attributes)get_store().add_element_user(ATTRIBUTES$12);
            }
            target.set(attributes);
        }
    }
    
    /**
     * Appends and returns a new empty "attributes" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Attributes addNewAttributes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Attributes target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Attributes)get_store().add_element_user(ATTRIBUTES$12);
            return target;
        }
    }
    
    /**
     * Unsets the "attributes" element
     */
    public void unsetAttributes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ATTRIBUTES$12, 0);
        }
    }
    /**
     * An XML classType(@http://schemas.hp.com/ucmdb/1/types/classmodel).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass$ClassType.
     */
    public static class ClassTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass.ClassType
    {
        
        public ClassTypeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ClassTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
