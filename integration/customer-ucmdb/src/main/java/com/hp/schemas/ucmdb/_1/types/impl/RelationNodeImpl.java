/*
 * XML Type:  RelationNode
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.RelationNode
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML RelationNode(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class RelationNodeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.RelationNode
{
    
    public RelationNodeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LABEL$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "label");
    private static final javax.xml.namespace.QName RELATIONS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "relations");
    
    
    /**
     * Gets the "label" element
     */
    public java.lang.String getLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LABEL$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "label" element
     */
    public org.apache.xmlbeans.XmlString xgetLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LABEL$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "label" element
     */
    public void setLabel(java.lang.String label)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LABEL$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LABEL$0);
            }
            target.setStringValue(label);
        }
    }
    
    /**
     * Sets (as xml) the "label" element
     */
    public void xsetLabel(org.apache.xmlbeans.XmlString label)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LABEL$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(LABEL$0);
            }
            target.set(label);
        }
    }
    
    /**
     * Gets the "relations" element
     */
    public com.hp.schemas.ucmdb._1.types.Relations getRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().find_element_user(RELATIONS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "relations" element
     */
    public void setRelations(com.hp.schemas.ucmdb._1.types.Relations relations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().find_element_user(RELATIONS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().add_element_user(RELATIONS$2);
            }
            target.set(relations);
        }
    }
    
    /**
     * Appends and returns a new empty "relations" element
     */
    public com.hp.schemas.ucmdb._1.types.Relations addNewRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().add_element_user(RELATIONS$2);
            return target;
        }
    }
}
