package com.telstra.netops.dns.server.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class DnsApplicationExceptionHandler extends ResponseEntityExceptionHandler {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DnsApplicationExceptionHandler.class);
	
	@ExceptionHandler({DnsApplicationGenericBusinessRuleException.class})
	protected ResponseEntity<Object> handleGenericBusinessRuleException(RuntimeException e, WebRequest request) {
		LOGGER.debug("handling business rule exception response");
		DnsApplicationGenericBusinessRuleException bre = (DnsApplicationGenericBusinessRuleException)e;		
		return handleExceptionInternal(e, bre.getError(), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, request);
	}

}
