<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!doctype html>
<html lang="en" class="no-js" ng-app="netops">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=10" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Pragma" content="no-cache">
	<img href="<c:url value="/img/favicon.ico"/>" rel="icon" type="image/x-icon" />
<%--   <img src="<c:url value="/img/ajax-loader.gif" />" alt="TestDisplay"/> --%>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<c:url value="/css/reset.css"/>"> <!-- CSS reset -->

	<link rel="stylesheet" href="<c:url value="/css/style.css"/>"> <!-- Resource style -->
	
	<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>"> <!-- Bootstrap -->
	
		
	
	<script type="text/javascript" src="<c:url value="/js/jquery-2.1.4.js"/>"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

	<script type="text/javascript"  src="<c:url value="/js/jquery.menu-aim.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/main.js"/>"></script> <!-- Resource jQuery -->
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/angular.js"/>"></script> <!-- Angular -->
	<script type="text/javascript" src="<c:url value="/js/angular-route.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/angular-local-storage.min.js"/>"></script>

    <script type="text/javascript">
	if (!String.prototype.startsWith) {
	    String.prototype.startsWith = function(searchString, position){
	      position = position || 0;
	      return this.substr(position, searchString.length) === searchString;
	  };
	}
	</script>
	
	<!-- TODO Require modules without having to name the explicitly -->
	<script type="text/javascript" src="<c:url value="${urls.getForLookupPath('/modules/util/util.module.js')}"/>"></script> <!-- Util -->
	<script type="text/javascript" src="<c:url value="${urls.getForLookupPath('/modules/lookingglass/lookingglass.module.js')}"/>"></script> <!-- Looking Glass -->
	<script type="text/javascript" src="<c:url value="${urls.getForLookupPath('/modules/dns/dns.module.js')}"/>"></script> <!-- DNS -->
	<script type="text/javascript" src="<c:url value="${urls.getForLookupPath('/modules/routemanagement/routemanagement.module.js')}"/>"></script> <!-- Route Management -->
	<script type="text/javascript" src="<c:url value="${urls.getForLookupPath('/app.module.js')}"/>"></script> <!-- Netops -->	
	
	<script type="text/javascript">
		
		// defer angular bootstrapping to inject our own modules
	
		window.name = 'NG_DEFER_BOOTSTRAP!';

		angular.element(document).ready(function() {
			angular.module('BootstrapModule', ['LocalStorageModule'])
				.run(function($http, localStorageService, $location) {

				    // if token is 'NOT_ISSUED' it means we have already have our OAuth token and Customer Id!
				    // 'NOT_ISSUED' MUST match AuthController.ONE_TIME_TOKEN_NOT_ISSUED constant.
				    if (localStorageService.isSupported) {
	    				localStorageService.set('role', '${userProfile}');
	    				
	    				<c:forEach items='${environment}' var="envVar">
	    				    localStorageService.set('env.' + '${envVar.key}', '${envVar.value}');
                        </c:forEach>

					    if ('${onetimetoken}' != 'NOT_ISSUED') {				        
							$http.get('oauthtoken?onetimetoken=' + '${onetimetoken}')
				    			.then(function (response) {
				    			    
				    			    // need to set productLine BEFORE token as events listen for token
				    			    // set use productLine!
				    			    if ('${productLine}' != 'NONE'){
				    			    	localStorageService.set('productLine', '${productLine}');
				    			    }
				    			    
				    				console.log('setting token in local storage');
				    				localStorageService.set('token', response.data.token);
				    				
				    				if ('${redirect}' != 'NONE'){
			  			    			console.log('redirecting to /${redirect} with product line ${productLine}');
				    	    			$location.path('/${redirect}');
				        			}
				  				}, function (response) {
									// TODO - what to do here?
									console.log('failed to get one time token!');
				  			});
			  			} else {
			  			    // FIXME - hack to re-trigger product load, no notification event is generated
			  				localStorageService.set('token', localStorageService.get('token'));
			  			}
			  			
		  			} else {
		  			    $location.path('/NoLocalStorage');
		  			}
				    
		  			
	  			});
			angular.resumeBootstrap([ 'BootstrapModule' ]);	
		});
		
	</script>

	<script type="text/javascript" src="<c:url value="/js/modernizr.js"/>"></script><!-- Modernizr -->
	
	<title>Netops</title>
</head>
<body>
	
	<div class="right"></div>
	<div ng-controller="ProductController">
	<header class="cd-main-header">
		<a href="#/Netops"><img class="cd-logo" src="img/Telstralogo.png" alt="Logo"></a>
		<p class="cd-wel" ng-show="welcome">Welcome ${c3UserName} ${environment["description"]}</p><!--${c3UserName} Role: ${t2rrole}  Customer ID: {{custid}} -->
		
			<nav class="cd-nav">
				<ul class="cd-top-nav">
					<li><a ng-controller="MainController" ng-click="logout()">Logout</a></li>
				 	<li	>											
						<select class="dropdown select" id="productSelect" ng-model="sProductLine" ng-change="getTools()"
						    ng-options="productLine.code for productLine in productLines track by productLine.code">>
							<option value="" style="display:none">Product Selection</option>
						</select>
					</li>						
				</ul>
			</nav>
		<!--</div>	-->
	</header> <!-- .cd-main-header -->

	<main class="cd-main-content">
	
	    <!-- TODO Have modules contribute their menu items without having to the explicitly include them here! -->
		<nav class="cd-side-nav" >
			<ul>
				<li class="cd-label">Tools</li>
				 <li class="has-children" ng-show="data.dns">
					<a >DNS</a>				
					<ul >
						<li class="has-children " >
						<a   >Primary DNS</a>
						 	<ul>
						 	  <li><a href="#/DNS_PrimaryDnsAdd">Add DNS Record</a></li>
						 	 <li><a href="#/DNS_PrimaryDnsView">View/Delete DNS Record</a></li>
						 	 
					</ul>
					   </li>
						<li class="has-children ">  
						<a>Secondary DNS</a>
						    <ul>
						 	 <li><a href="#/DNS_SecondaryDnsAdd">Add Domain</a></li>
						 	 <li><a href="#/DNS_SecondaryDnsView">View Domain</a></li>
						 	 <li><a href="#/DNS_SecondaryDnsRemove">Remove Domain</a></li>
						 	 
						 	</ul>
					   </li>
					   	<li class="has-children " > 
						<a >DNS Resolver</a>
						<ul>
						 	 <li><a href="#/DNS_DnsResolverAdd">Add/Cancel DNS Resolver</a></li>
						 	  <li><a href="#/DNS_DnsResolverView">View DNS Resolver</a></li>
						 	</ul>
					   </li>
						<li class="has-children ">  
						<a >Reverse DNS</a>
						<ul>
						 	 <a href="#/DNS_ReverseDelegationAdd">Reverse Delegation</a>
						 	 <a href="#/DNS_ReverseMappingAdd">Reverse Mapping</a>
						 	 <a href="#/DNS_ReverseDelegationAndMappingView">View Reverse Mapping and Reverse Delegation</a>
						 	</ul>
					   </li>
					</ul>
					
				</li> 
				
				<li class="has-children" ng-show="data.lookingGlass">
					<a href="#/LG_LookingGlass">Looking Glass</a>					
					<ul>
						<li><a href="#/LG_NodeTest">Node to Node</a></li>
						<li><a href="#/LG_NodeHostTest">Node to Host</a></li>
						<li><a href="#/LG_NodeHostTrace">Node to Host Trace</a></li>
					</ul>
				</li>
				<!-- <li ng-show="data.ncli">
					<a href="#0">Network CLI</a>
				</li>
			
				<li class="has-children " ng-show=data.routeManagement>
					<a href="#/RM_RouteManagement">Route Management</a>					
						<ul>
						<li><a href="#/RM_ServiceListSingle">Single Prefix Upload</a></li>
						<li><a href="#/RM_ServiceListMultiple">Multiple Prefix Upload</a></li>
						<li><a href="#/RM_ServiceListView">View/Delete Prefix</a></li>
						<li><a href="#/RM_RouterInformationView"  ng-hide="role!='INTERNAL'">Add Router Information</a></li>
					</ul>
				</li> -->
			</ul>
		</nav>
		<div class="content-wrapper">
			<div ng-view>
			 <div>
     		  <br><br>
     		  <h4>Welcome to Netops Andrew and David!</h4>
     		  <h5>You can use Netops tools that are available for your services. Please select a product to view the related tools.</h5>
    		 </div>
			</div>
		</div> <!-- .content-wrapper -->
	</main> <!-- .cd-main-content -->
</div>

	<div class="bottom">
		<p style="text-align:center;color:#ffffff;"><font size="2">Supported on IE10+ &amp; latest versions of Chrome &amp; Firefox. </p>
		
	</div>
</body>
</html>