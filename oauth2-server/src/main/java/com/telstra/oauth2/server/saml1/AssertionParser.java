package com.telstra.oauth2.server.saml1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.joda.time.DateTime;
import org.opensaml.saml1.core.Assertion;
import org.opensaml.saml1.core.Attribute;
import org.opensaml.saml1.core.AttributeStatement;
import org.opensaml.saml1.core.AuthenticationStatement;
import org.opensaml.saml1.core.Condition;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.validation.ValidationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

@Component
public class AssertionParser {
	
	private static final Object ROLE_PREFIX = "ROLE_";
	
	private static final Object ROLE_ATTRIBUTE_NAME = "role";

	public Collection<GrantedAuthority> parseRoles(Assertion assertion) {
		List<AttributeStatement> statements = assertion.getAttributeStatements();
		if (statements == null || statements.isEmpty()) {
			return new ArrayList<GrantedAuthority>();
		}
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (AttributeStatement statement : assertion.getAttributeStatements()) {
			for (Attribute attribute : statement.getAttributes()) {
				if (ROLE_ATTRIBUTE_NAME.equals(attribute.getAttributeName())) {
					for (XMLObject value : attribute.getAttributeValues()) {
						Element attributeValueElement = value.getDOM();
                        String role = attributeValueElement.getTextContent();
                        authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + role));
					}
				}
			}
		}
		
		return authorities;
	}
	
	public User parseUser(Assertion assertion) throws ValidationException {
		return new User(getUserName(assertion), "unknown", parseRoles(assertion));		
	}
	
	String getUserName(Assertion assertion) throws ValidationException {
		List<AuthenticationStatement> statements = assertion.getAuthenticationStatements();
		
		if (!statements.isEmpty()) {
			AuthenticationStatement statement = statements.get(0);
			
			return statement.getSubject().getNameIdentifier().getNameIdentifier();
		}
		
		throw new ValidationException("missing subject");
	}
	
	public String parseIssuer(Assertion assertion) {
		return assertion.getIssuer();		
	}

	public List<Condition> parseConditions(Assertion assertion) {
		return assertion.getConditions().getConditions();
	}
	
	public DateTime parseConditionsNotOnOrAfterDate(Assertion assertion) {
		return assertion.getConditions().getNotOnOrAfter();
	}

}
