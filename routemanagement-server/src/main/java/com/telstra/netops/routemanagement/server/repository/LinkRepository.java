package com.telstra.netops.routemanagement.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telstra.netops.routemanagement.server.model.Link;

/**
 * Repository class for Link listing
 *
 */
public interface LinkRepository extends CrudRepository<Link, Long> {

	@Query("SELECT r FROM Link r WHERE r.serviceId = :serviceId")
	List<Link> findAllByServiceid(@Param("serviceId") String serviceId);
}
