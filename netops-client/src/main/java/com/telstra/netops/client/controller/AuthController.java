package com.telstra.netops.client.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPConnection;
import com.telstra.netops.client.config.ClientEnvironment;
import com.telstra.netops.client.exception.LdapPasswordCheckException;
import com.telstra.netops.client.exception.MissingRequestHeaderException;
import com.telstra.netops.client.model.AuthRequest;
import com.telstra.netops.client.model.Token;
import com.telstra.netops.client.model.OAuthToken;


@Controller
public class AuthController {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AuthController.class);
	
	private static final String GRANT_TYPE = "urn:ietf:params:oauth:grant-type:saml1-bearer";
	
	private static final String ONE_TIME_TOKEN_NOT_ISSUED = "NOT_ISSUED";
	
	private static final String REDIRECT_NONE = "NONE";
	
	private static final String PRODUCT_LINE_NONE = "NONE";
		
	@Value("${oauth.server.tokenUrl}")
	private String oauthUrl;
	
	@Value("${oauth.server.clientId}")
	private String oauthClientId;
	
	@Value("${oauth.server.clientSecret}")
	private String oauthClientSecret;

	@Value("${identityProvider.ldap.ldapHost:#{null}}")
	private String ldapHost;
	
	@Value("${identityProvider.ldap.ldapPort:#{null}}")
	private Integer ldapPort;
	
	@Value("${identityProvider.ldap.ldapUsername:#{null}}")
	private String ldapUsername;
	
	@Value("${identityProvider.ldap.ldapPassword:#{null}}")
	private String ldapPassword;
	
	@Value("${environment.description:}")
	private String environmentDescription;
	
	@Autowired
	private ClientEnvironment environment;

	private Map<String, OAuthToken> map = new ConcurrentHashMap<String, OAuthToken>();
	
	@RequestMapping(value = "/", method=RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String auth(
			@RequestHeader(value="userdn", defaultValue="${client.default.userdn:#{null}}") String userdn,
			@RequestHeader(value="Authorization", defaultValue="${client.default.authorization:#{null}}") String authorization,
			@ModelAttribute AuthRequest authRequest,
			ModelMap model,
			HttpServletResponse response
			) throws MissingRequestHeaderException {
				
		if (userdn == null) {
			throw new MissingRequestHeaderException("userdn");
		}
		
		if (authorization == null) {
			throw new MissingRequestHeaderException("Authorization");
		}
		
		authRequest.setUserdn(userdn);
		authRequest.setAuthorization(authorization);
		LOGGER.info("REDIRECT is " + authRequest.getRedirect());
		LOGGER.info("REDIRECT product line is " + authRequest.getProductLine());
		model.addAttribute("redirect", (authRequest.getRedirect() != null) ? authRequest.getRedirect() : REDIRECT_NONE);
		model.addAttribute("productLine", (authRequest.getProductLine() != null) ? authRequest.getProductLine() : PRODUCT_LINE_NONE);
		model.addAttribute("environmentDescription", environmentDescription);
		
		for (String key : environment.getEnvironment().keySet()) {
			LOGGER.info("client env " + key + " = " + environment.getEnvironment().get(key));
		}
		
		model.addAttribute("environment", environment.getEnvironment());
		
		return processAuthRequest(authRequest, model);
	}
	
	@RequestMapping(value = "/", method=RequestMethod.GET)
	public String refresh(ModelMap model) {
		// user hit refresh, one time token is only issued on POST
		model.addAttribute("onetimetoken", ONE_TIME_TOKEN_NOT_ISSUED);
		model.addAttribute("redirect", REDIRECT_NONE);
		model.addAttribute("productType", PRODUCT_LINE_NONE);
		model.addAttribute("environmentDescription", environmentDescription);
		model.addAttribute("environment", environment.getEnvironment());
		return "netops";		
	}		
	
	@RequestMapping(value = "/oauthtoken", method=RequestMethod.GET)
	public @ResponseBody OAuthToken oauthTokenAndCustomer(@RequestParam(value = "onetimetoken") String oneTimeToken) {
		OAuthToken tokenAndCustomer = map.remove(oneTimeToken);
		return tokenAndCustomer;		
	}
	
	private String processAuthRequest(AuthRequest authRequest, ModelMap model) {
		LOGGER.info("userdn  is" + authRequest.getUserdn());
		String userDN = authRequest.getUserdn();
		LOGGER.info("authorization is " + authRequest.getAuthorization());
		LOGGER.info("customerId is " + authRequest.getCustomerId());
		String base64EncodedSamlTokenAndPassword = authRequest.getAuthorization().substring("Basic ".length());
		LOGGER.info("base64EncodedSamlTokenAndPassword  is " + base64EncodedSamlTokenAndPassword);
		String[] decodedSamlTokenAndPassword = new String(Base64.decodeBase64(base64EncodedSamlTokenAndPassword)).split(":");
		LOGGER.info("decodedSamlTokenAndPassword is " + decodedSamlTokenAndPassword);
		String samlToken = decodedSamlTokenAndPassword[0];
		LOGGER.info("samlToken is " + samlToken);
		String c3username = getC3Username(authRequest);
		if (c3username != null) {
			LOGGER.info("c3username is " + c3username);
			model.addAttribute("c3UserName", c3username);
		}

		String userProfile = getUserProfile(authRequest);
		if (userProfile != null) {
			LOGGER.info("userProfile is " + userProfile);
			model.addAttribute("userProfile", userProfile);
		}
		
		checkUsernameAndPassword(userDN, decodedSamlTokenAndPassword[1]);
		
		String oauthToken = getOAuthAccessToken(samlToken, authRequest.getCustomerId(), userProfile);
		LOGGER.info("oauth access token is " + oauthToken);
		
		if (oauthToken == null) {
			return "error";
		}
		
		// save "one time token" for AngularJS to get via "/oauthtoken" endpoint
		String oneTimeToken = UUID.randomUUID().toString();
		map.put(oneTimeToken, new OAuthToken(oauthToken));
		model.addAttribute("onetimetoken", oneTimeToken);
				
		return "netops";		
	}
	
	private String getOAuthAccessToken(String samlToken, String customerId, String userProfile) {
		try {
			RestTemplate restTemplate = new RestTemplate();
	
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Basic " + Base64.encodeBase64String(new String(oauthClientId + ":" + oauthClientSecret).getBytes()));
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			
			MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
			map.add("grant_type", GRANT_TYPE);
			map.add("assertion", samlToken);
			map.add("customerid", customerId);
			map.add("userprofile", userProfile);
		    
			HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
			
			ResponseEntity<Token> responseEntity = restTemplate.postForEntity(oauthUrl, request, Token.class);
			
			if (!HttpStatus.OK.equals(responseEntity.getStatusCode())) {
				LOGGER.error("token endpoint " + oauthUrl + " returned HTTP status code " + responseEntity.getStatusCode());
				return null;
			}
			
			Token token = responseEntity.getBody();
			
			LOGGER.error("got oauth token: " + token);
			
			return token.getAccessToken();
		} catch (HttpClientErrorException e) {
			LOGGER.error("token endpoint " + oauthUrl + " returned HTTP status code " + e.getStatusCode(), e);
			return null;
		}
	}
	
	private String getC3Username(AuthRequest authRequest) {
		for (String userDNSubHeader : authRequest.getUserdn().split(",")) {
			if (userDNSubHeader.contains("cn")) {
				int firstEquals = userDNSubHeader.indexOf("=");
				String c3username = userDNSubHeader.substring((firstEquals + 1));
				LOGGER.info("User name  is" + c3username);
				return c3username;

			}		
		}
		
		return null;
	}

	private String getUserProfile(AuthRequest authRequest) {
		for (String userDNSubHeader : authRequest.getUserdn().split(",")) {
			if (userDNSubHeader.contains("ou")) {
				int firstEquals = userDNSubHeader.indexOf("=");

				String userProfile = userDNSubHeader.substring((firstEquals + 1));
				
				return userProfile;
			}		
		}
		
		return null;
	}
	
	private void checkUsernameAndPassword(String userDn, String password) {
		if (ldapHost == null) {
			LOGGER.warn("skipping LDAP password check");
			return;
		}
		
		int ldapVersion = LDAPConnection.LDAP_V3;
		String objectDN = userDn;
		String testPassword = password;
		LDAPConnection lc = new LDAPConnection();

		try {
			LOGGER.info("in try block about to establish LDAP connection with host: "+ldapHost+"< >port "+ldapPort);
			// connect to the server
			lc.connect(ldapHost, ldapPort);
			LOGGER.info("LDAP connection established :");


			// authenticate to the server
			lc.bind(ldapVersion, ldapUsername, ldapPassword.getBytes("UTF8"));

			LDAPAttribute attr = new LDAPAttribute("userPassword", testPassword);
			LOGGER.info("going to compare passwords with userDN "+objectDN);
			boolean correct = lc.compare(objectDN, attr);
			LOGGER.info("LDAP authentication is :"+correct);
			// disconnect with the server
			lc.disconnect();

			if (!correct){
				throw new LdapPasswordCheckException("password check failed");
			}
			
		} catch (Exception e) {
			throw new LdapPasswordCheckException("password check failed", e);
		}

	}
	
}
