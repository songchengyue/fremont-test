package com.telstra.netops.routemanagement.server.service;

import com.telstra.netops.routemanagement.server.model.PrefixUploadRequest;
import com.telstra.netops.routemanagement.server.model.Success;

public interface SinglePrefixAddService {

	Success upload(PrefixUploadRequest request);

}
