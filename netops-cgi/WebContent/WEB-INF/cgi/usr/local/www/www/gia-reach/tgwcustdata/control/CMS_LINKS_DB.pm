# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 14 November 2001
# File: CMS_LINKS_DB.pm
#
# $Id: CMS_LINKS_DB.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $
###################################################################
# File        : CMS_LINKS_DB.pm
# Description : Executes functions called from CMS_SERVICE.pm and 
#		interacts with tables in database for data queries.
# Parameters to be passed and usage : linkid,serviceid,typeid,routername,
#                                     interface,bandwidth,description.
# Output of the script
# Input File  :
# Output File :
# Author      : PRADEEP THANRAJ,KETAKI THOMBARE (Infosys Tech. Ltd., Bangalore )
# Started On  : 17 AUG 2009
# Modification History :
#  Date               Name                          Change/Description
# ------------------------------------------------------------------
# 14Aug2009    PRADEEP THANRAJ,KETAKI THOMBARE     Baseline for EVPL.
# 22Aug2009    PRADEEP THANRAJ,KETAKI THOMBARE     Added for EVPL.
# 13Oct2009    Nisha Sasindran			   Modified for mVPN - service validation for add link
# 21Oct2010    Karuna Ballal			   Modified for IPT3 - Delete_service
# 02Jun2011    Varun Yadav, Maneet Kaur            Modified for Addendum-2 BHA Requirment.
####################################################################

package CMS_LINKS_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;

our $error;
my $debug = 1;
#uncomment following line to turn off debug logs and vice versa
#$debug = 0;
if ($debug){open (LINK,">>/data1/tmp_log/links.log");}

sub add_link {
  my $lid = shift @_;
  my $linkid = $dbh->quote($lid);
  my $sid = shift @_;
  my $serviceid = $dbh->quote($sid);
  my $typeid = $dbh->quote(shift @_);
  my $routername = $dbh->quote(shift @_);
  my $interface = $dbh->quote(shift @_);
  my $bw = shift @_;
  my $description = $dbh->quote(shift @_);
  ##++IPT5
  my $bha_router = $dbh->quote(shift @_);
  my $bha_desc = $dbh->quote(shift @_);
  ##++Addendum 2 
  my $bha_router_bac = $dbh->quote(shift @_);
  my $bha_desc_bac = $dbh->quote(shift @_);
  ##--Addendum 2
  ##--IPT5
  if ($debug) {print LINK "DB bha_router:$bha_router\tbha_desc:$bha_desc\n";}
  if ($debug) {print LINK "DB bha_router_bac:$bha_router_bac\tbha_desc_bac:$bha_desc_bac\n";}

  $linkid =~ tr/A-Z/a-z/;
  $serviceid =~ tr/A-Z/a-z/;

  # For backward compatibility - need to check whether the serviceid is upper case or not
  my $sth = $dbh->prepare("SELECT serviceid FROM link WHERE serviceid ilike ?");
  $sth->execute($sid);
  my $r = $sth->fetchrow_hashref;
  if($$r{serviceid} =~ /^$sid$/i) {
        $serviceid = $dbh->quote($$r{serviceid});
  }

  # now check if an uppercase version of linkid exists
  $sth = $dbh->prepare("SELECT linkid FROM link WHERE linkid ilike ?");
  $sth->execute($lid);
  $r = $sth->fetchrow_hashref;

  # need to get router id (assume entry exists)
  $sth = $dbh->prepare("SELECT routerid FROM routername
             WHERE routername = $routername");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  my $routerid = ($sth->fetchrow_array)[0];
  $sth->finish;

  ##++IPT5
  #double check BHA option Yes or NO
  $sth = $dbh->prepare("select black_hole_routing from service where serviceid= $serviceid");
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  my $bha_option = ($sth->fetchrow_array)[0];
  $sth->finish;
  ##Fetch BHA router ID
  $sth = $dbh->prepare("SELECT routerid FROM routername WHERE routername = $bha_router");
  unless ($sth->execute) {
    	$error = $dbh->errstr;
    	return 0;
  }
  my $bha_routerid = ($sth->fetchrow_array)[0];
  if ($bha_option eq "No"){
	$bha_routerid = "NULL";
  }
  $sth->finish;

  ##++Addendum 2
  ##Fetch BHA Backup router ID 
  $sth = $dbh->prepare("SELECT routerid FROM routername WHERE routername = $bha_router_bac");
  unless ($sth->execute) {
    	$error = $dbh->errstr;
    	return 0;
  }
  my $bha_routerid_bac = ($sth->fetchrow_array)[0];
  if ($bha_option eq "No"){
	$bha_routerid_bac = "NULL";
  }
  $sth->finish;
  ##--Addendum 2
  ##--IPT5


  #Added by Nisha for mVPN:checking if the service is present in the service table
  my $serviceid_service = "SELECT serviceid from service where serviceid = $serviceid";
  $sth = $dbh->prepare($serviceid_service);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  my $result = $sth->fetchrow_arrayref;
  my $result_service=$$result[0];
  my $service_serviceid = $result_service;
  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
  }

  my $serviceid_agg = "SELECT aggregate_serviceid from aggregate_service where aggregate_serviceid = $serviceid";
  $sth = $dbh->prepare($serviceid_agg);
  unless ($sth->execute) {
 	$error = $dbh->errstr;
	return 0;
  }
  $result = $sth->fetchrow_arrayref;
  my $result_agg = $$result[0];
  my $agg_serviceid = $result_agg;
  if ($result) {
	$error = $dbh->errstr;
	$sth->finish;
  }

  if($service_serviceid ne  "" || $agg_serviceid ne ""){
    #insert only if the service is a valid one
	###++IPT5
        ##++Addendum 2
    	if($bha_router ne "NULL"){
		$sth = $dbh->prepare("INSERT INTO link(linkid, serviceid, typeid, routerid, interface, bw, description,bha_routerid,bha_description,bha_routerid_bac,bha_description_bac)
			VALUES($linkid, $serviceid, $typeid, $routerid, $interface, $bw, $description, $bha_routerid, $bha_desc,$bha_routerid_bac,$bha_desc_bac)") ;
        ##--Addendum 2
	##--IPT5
	} else {
    		$sth = $dbh->prepare
    		("INSERT INTO link(linkid, serviceid, typeid, routerid, interface, bw, description)
        	VALUES($linkid, $serviceid, $typeid, $routerid, $interface, $bw, $description)");
	}

    my $result = $sth->execute;
    $error = $dbh->errstr;
    return $result;
  } else {
    $error = "Invalid Serviceid";
    return 0;
  }
  #End of code Added by Nisha for mVPN
}

sub update_link {
  my $lid = shift @_;
  my $linkid = $dbh->quote($lid);
  my $sid = shift @_;
  my $serviceid = $dbh->quote($sid);
  my $typid = shift @_;
  my $typeid = $dbh->quote($typid);
  my $routname = shift @_;
  my $routername = $dbh->quote($routname);
  my $intid = shift @_;
  my $interface = $dbh->quote($intid);
  my $bw = shift @_;
  my $desc = shift @_;
  my $description = $dbh->quote($desc);
  ##++IPT5
  my $bha_routername = $dbh->quote(shift @_);
  my $bha_description = $dbh->quote(shift @_);
  ##++Addendum 2
  my $bha_routername_bac = $dbh->quote(shift @_);
  my $bha_description_bac = $dbh->quote(shift @_);
  ##--Addendum 2
  if (($bha_routername eq "") || ($bha_routername eq "\'existing.bha_router\'")){
	if ($debug) { print LINK "DB UPDATE null bha\n";}
	$bha_routername = "NULL";
  } 
  if ($bha_description eq ""){
	$bha_description = "NULL";
  }
  ##--IPT5
  ##++Addendum 2
       if (($bha_routername_bac eq "") || ($bha_routername_bac eq "\'existing.bha_router_bac\'")){
             if ($debug) { print LINK "DB UPDATE null bha backup \n";}
             $bha_routername_bac = "NULL";
       }
       if ($bha_description_bac eq ""){
             $bha_description_bac = "NULL";
       }
   
  ##--Addendum 2
  if ($debug) { print LINK "DB UPDATE bha_routername:$bha_routername\tbha_description:$bha_description\n";}
  if ($debug) { print LINK "DB UPDATE bha_routername_bac:$bha_routername_bac\tbha_description_bac:$bha_description_bac\n";}


open DEBUG ,">/tmp/tmp_prad_edit_db";
  $linkid =~ tr/A-Z/a-z/;
  $serviceid =~ tr/A-Z/a-z/;

  # backward compatibility - check case of service id
  # For backward compatibility - need to check whether the serviceid is upper case or not
  my $sth = $dbh->prepare("SELECT serviceid FROM link WHERE serviceid ilike ?");
  $sth->execute($sid);
  my $r = $sth->fetchrow_hashref;
  if($$r{serviceid} =~ /^$sid$/i) {
        $serviceid = $dbh->quote($$r{serviceid});
  }
  # now check if an uppercase version of linkid exists
  $sth = $dbh->prepare("SELECT linkid FROM link WHERE linkid ilike ?");
  $sth->execute($lid);
  $r = $sth->fetchrow_hashref;

  # need to get router id (assume entry exists)
  $sth = $dbh->prepare("SELECT routerid FROM routername
             WHERE routername = $routername");
  unless ($sth->execute) {
  	$error = $dbh->errstr;
    	return 0;
  }
  my $routerid = ($sth->fetchrow_array)[0];
  $sth->finish;
  my $bha_routerid="NULL";

  my $bha_routerid_bac="NULL";
  ##++IPT5
  if ($debug){print LINK "DB UPDATE bha_routername when not null:$bha_routername\n";}
  if ($bha_routername ne "NULL"){
  if ($debug){print LINK "DB UPDATE inside if bha_routername when not null:$bha_routername\n";}
  my $sth_bha = $dbh->prepare("SELECT routerid FROM routername WHERE routername = $bha_routername");
  unless ($sth_bha->execute) {
        $error = $dbh->errstr;
	if ($debug){print LINK "DB UPDATE error:$error";}
        return 0;
  }

  if (($sth_bha->rows())>0){
	if ($debug){print LINK "BHA router exists\n";}
 	$bha_routerid = ($sth_bha->fetchrow_array)[0];
  	$sth_bha->finish;
  } else {
	if ($debug){print LINK "BHA router does not exist\n";}
	$error = "BHA routername does not exist.";
	return $error;
  }
  ##++Addendum 2
  if ($debug){print LINK "DB UPDATE bha_routername_bac when not null:$bha_routername_bac\n";}

  my $sth_bha_bac = $dbh->prepare("SELECT routerid FROM routername WHERE routername = $bha_routername_bac");
  unless ($sth_bha_bac->execute) {
        $error = $dbh->errstr;
        if ($debug){print LINK "DB UPDATE error:$error";}
        return 0;
       }
    
  if (($sth_bha_bac->rows())>0){
        if ($debug){print LINK "BHA router exists\n";}
        $bha_routerid_bac = ($sth_bha_bac->fetchrow_array)[0];
        $sth_bha_bac->finish;
  } else {
        if ($debug){print LINK "BHA router backup does not exist\n";}
        $error = "BHA routername backup does not exist.";
        return $error;
  }
       
##--Addendum-2

  }
  ##--IPT5


 #++EVPL
  my $result;
  my $chk_link = check_link_exists($linkid);
	print DEBUG "chk_link----->$chk_link\n";
	print DEBUG "lid--->$lid :::: sid---->$sid :::typid---->$typid :::routname--->$routname :::intid---$intid-- :::bw--->$bw :::desc--->$desc \n";

#add scenario if link does not exist and the interface is given in the screen	
 if($chk_link == 0 && $intid ne ""){
	print DEBUG "going to add link \n";
	print DEBUG "lid--->$lid::::sid---->$sid:::typid---->$typid:::routname--->$routname:::intid---$intid:::bw--->$bw:::desc--->$desc\n";
         $result = add_link ($lid, $sid,$typid,$routname,$intid,$bw,$desc);
	} 
#delete screnario if link id exists and then interface is empty
elsif($chk_link == 1 && $intid eq ""){
	print DEBUG "GOING  to delete link $lid ::::: $sid \n";
	$result = delete_link($lid, $sid);
}
elsif($chk_link == 1 && $intid ne ""){
	print DEBUG "going to edit link\n";
	#--EVPL
	$sth = $dbh->prepare
		("UPDATE link
		SET typeid = $typeid,
		routerid = $routerid,
		interface = $interface,
		bw = $bw,
		description = $description,
		bha_routerid = $bha_routerid,
		bha_description = $bha_description,
                bha_routerid_bac = $bha_routerid_bac,
                bha_description_bac = $bha_description_bac
		WHERE linkid = $linkid
		AND serviceid = $serviceid");
	$result = $sth->execute;
	$error = $dbh->errstr;
	if ($debug){print LINK "DB UPDATE UPDATE link SET typeid = $typeid, routerid = $routerid, interface = $interface, bw = $bw, description = $description, bha_routerid = $bha_routerid, bha_description = $bha_description,  bha_routerid_bac = $bha_routerid_bac,  bha_description_bac = $bha_description_bac WHERE linkid = $linkid AND serviceid = $serviceid\n";}
	if ($debug){print LINK "DB UPDATE error:$error\n";}
}
  #return $result;
  return 1;
close (DEBUG);
}

sub delete_link {
  my $lid = shift @_;
  my $linkid = $dbh->quote($lid);
  my $sid = shift @_;
  my $serviceid = $dbh->quote($sid);

open (DEBUG,">/tmp/delete_link");
  # For backward compatibility - need to check whether the serviceid is upper case or not
  my $sth = $dbh->prepare("SELECT serviceid FROM link WHERE serviceid ilike ?");
  $sth->execute($sid);
  my $r = $sth->fetchrow_hashref;
  if($$r{serviceid} =~ /^$sid$/i) {
        $serviceid = $dbh->quote($$r{serviceid});
  }

  # now check if an uppercase version of linkid exists
  $sth = $dbh->prepare("SELECT linkid FROM link WHERE linkid ilike ?");
  $sth->execute($lid);
  $r = $sth->fetchrow_hashref;
  #if($$r{linkid} =~ /^$lid$/i) {
  	#$linkid = $dbh->quote($$r{linkid});
  #}

  &record_link_details($linkid, $serviceid);

  ##Added by Karuna on 21-10-2010 for IP Transit ph3
  my $links_del_query = $dbh->prepare("insert into deleted_links (linkid,serviceid,accno,popname,date_of_deletion) (select l.linkid,s.serviceid,s.accno,p.popname,current_timestamp from service s, link l, routername r, popname p where s.productcodeid='IPTRANSIT' and s.accno in (select accno from customer where wholesaler_accno='100418') and s.serviceid = l.serviceid and l.routerid = r.routerid and r.popid = p.popid and l.serviceid=$serviceid and l.linkid=$linkid)");  

  print DEBUG "links_del_query:insert into deleted_links (linkid,serviceid,accno,popname, date_of_deletion) (select l.linkid,s.serviceid,s.accno,p.popname, current_timestamp from service s, link l, routername r, popname p where s.productcodeid='IPTRANSIT' and s.accno in (select accno from customer where wholesaler_accno='100418') and s.serviceid = l.serviceid and l.routerid = r.routerid and r.popid = p.popid and l.serviceid=$serviceid and l.linkid=$linkid\n";

  my $del_res = $links_del_query->execute();
  my $del_error = $dbh->errstr;
  print DEBUG "del_res:$del_res\tdel_error:$del_error\n";

  $sth = $dbh->prepare
    ("DELETE FROM link
       WHERE linkid = $linkid
         AND serviceid = $serviceid");

  my $result = $sth->execute;
  print DEBUG "result:$result\n";
  $error = $dbh->errstr;
  print DEBUG "error:$error\n";
  unless ($error){
	print DEBUG "no error\n";
	$links_del_query->finish();
  }
  return $result;
  close (DEBUG);
}

sub search_links {
  my $linkid = shift @_;
  my $service = shift @_;
  my $type = shift @_;
  my $routername = shift @_;
  my $interface = shift @_;
  my $bandwidth = shift @_;
  my $description = shift @_;

if ($debug){print LINK "DB search_links serviceid passed:$service\n";}

  my $command =
    "SELECT linkid, serviceid, type, routername, interface, bw, description
     FROM link
     NATURAL JOIN link_type
     NATURAL JOIN routername ";

  if ($linkid ne "") {
    $command .= "AND linkid ilike '%$linkid%' ";
  }
  if ($service ne "") {
    $command .= "AND serviceid ilike '$service' ";
  }
  if ($type ne "") {
    $command .= "AND type ilike '%$type%' ";
  }
  if ($routername ne "") {
    $command .= "AND routername ilike '%$routername%' ";
  }
  if ($interface ne "") {
    $command .= "AND interface ilike '%$interface%' ";
  }
  if ($bandwidth ne "") {
    $command .= "AND bw ilike '%$bandwidth%' ";
  }
  if ($description ne "") {
    $command .= "AND description ilike '%$description%' ";
  }

  $command =~ s/AND/WHERE/; # change first AND
if ($debug){print LINK "DB search_links command:$command\n";}
  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
   return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }


}

#***************************************************************************************
#Name		:search_all_links
#Description	:search all links for routing, including last deleted link of an AS num
#Author		:KARUNA BALLAL for IPT5
#Input		:service id
#Output		:link details
#****************************************************************************************

sub search_all_links {
  my $service = shift @_;

  my $command =
    "SELECT linkid, serviceid, type, routername, interface, bw, description
     FROM link
     NATURAL JOIN link_type
     NATURAL JOIN routername WHERE serviceid ilike '$service' 
     UNION
     SELECT linkid, serviceid, null, routername, null, null, null FROM deleted_links_routing 
     WHERE serviceid ilike '$service' AND service_deleted_timestamp is not null AND status = '2'";


  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
   return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }


}
#Chandini_IPT_5
#Addendum_2 Chandini
sub search_links_bha {
  my $service = shift @_;
  my $router_num = shift @_;

  my $command;
if($router_num == 0) {
  $command =
    "SELECT routername
     FROM routername
     LEFT OUTER JOIN link on routername.routerid = link.bha_routerid
     where link.serviceid ilike '$service'
     UNION
     SELECT bha_routername from deleted_links_routing where serviceid like '$service' and bha_status = '2'";
} else {
  $command =
    "SELECT routername
     FROM routername
     LEFT OUTER JOIN link on routername.routerid = link.bha_routerid_bac
     where link.serviceid ilike '$service'
     UNION
     SELECT bha_routername_bac from deleted_links_routing where serviceid like '$service' and bha_status_bac = '2'";
}
 
  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
   return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

#++EVPL
#this function will be called when the search will be done through the screen 
# and since the existing function is used by to generate the sites.list file. 
sub search_links_screen {
 open (DEBUG,">/tmp/search_link_db");
print DEBUG "INSIDE DB LINK\n";
  my $linkid = shift @_;
  my $service = shift @_;
  my $type = shift @_;
  my $routername = shift @_;
  my $interface = shift @_;
  my $bandwidth = shift @_;
  my $description = shift @_;
#++EVPL
  my $end = shift @_;
#--EVPL

  ##++IPT5
  my $bha_router = shift @_;
  my $bha_desc = shift @_;
  ##--IPT5

  ##++Addendum 2
  my $bha_router_bac = shift @_;
  my $bha_desc_bac = shift @_;
  ##--Addendum 2

print DEBUG "routername:$routername\n";
print DEBUG "linkid :$linkid\n";

##Changed for IPT5 - 01 Nov 2010 by Karuna Ballal
##Changed for Addendum 2 by Maneet Kaur
  my $command = 
  "SELECT
CASE 
 WHEN linkid like '%[%ec%a' THEN trim(trailing 'a' from linkid) 
 WHEN linkid like '%[%ec%z' THEN trim(trailing 'z' from linkid) 
 ELSE linkid 
END,

CASE
 WHEN serviceid like '%[%ec%asw' THEN replace(serviceid,'asw','sw')
 WHEN serviceid like '%[%ec%a' THEN trim(trailing 'a' from serviceid) 
 WHEN serviceid like '%[%ec%zsw' THEN replace(serviceid,'zsw','sw')
 WHEN serviceid like '%[%ec%z' THEN trim(trailing 'z' from serviceid) 
 ELSE serviceid 
END,

CASE
 WHEN serviceid like '%[%ec%a%' THEN 'A'
 WHEN serviceid like '%[%ec%z%' THEN 'Z'
 ELSE ' '
END,type, routername, interface, bw, description, (select routername from routername where link.bha_routerid=routername.routerid), bha_description, (select routername from routername where link.bha_routerid_bac=routername.routerid), bha_description_bac

from link
     NATURAL JOIN link_type
     NATURAL JOIN routername "; 
 
  if ($linkid ne "") {
    $command .= "AND linkid ilike '%$linkid%' ";
  }
  if ($service ne "") {
    $command .= "AND serviceid ilike '%$service%' ";
  }
  if ($type ne "") {
    $command .= "AND type ilike '%$type%' ";
  }
  if ($routername ne "") {
    $command .= "AND routername ilike '%$routername%' ";
  }
  if ($interface ne "") {
    $command .= "AND interface ilike '%$interface%' ";
  }
  if ($bandwidth ne "") {
    $command .= "AND bw ilike '%$bandwidth%' ";
  }
  if ($description ne "") {
    $command .= "AND description ilike '%$description%' ";
  }
  ##++IPT5
  if ($bha_router ne "") {
	my $sth_bha = $dbh->prepare("SELECT routerid from routername where routername like '%$bha_router%'");
	$sth_bha->execute();
	my $bha_routerid = $sth_bha->fetchrow_array();
 	$command .= "AND bha_routerid like '$bha_routerid' ";
  }
  if ($bha_desc ne "") {
        $command .= "AND bha_description ilike '%$bha_desc%' ";
  }  
  ##--IPT5

  ##++Addendum 2
  if ($bha_router_bac ne "") {
        my $sth_bha_bac = $dbh->prepare("SELECT routerid from routername where routername like '%$bha_router_bac%'");
        $sth_bha_bac->execute();
        my $bha_routerid_bac = $sth_bha_bac->fetchrow_array();
        $command .= "AND bha_routerid_bac like '$bha_routerid_bac' ";
  }
  if ($bha_desc_bac ne "") {
        $command .= "AND bha_description_bac ilike '%$bha_desc_bac%' ";
  }
  ##--Addendum 2

  $command =~ s/AND/WHERE/; # change first AND
  $command.= 'ORDER BY linkid';
	
 print DEBUG "command ----> $command\n";
  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {
print DEBUG "INSIDE RESULT\n";
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
print DEBUG "result:$result\n";
  } else {
    $error = $dbh->errstr;
    return 0;
  }

close (DEBUG);
}

# Added for new qos polling - only used for generating sites.list
# rchew@maettr.bpa.nu, 13 August 2006
# Pre:
# Post:
sub m_search_links {

  my $command =
    "SELECT serviceid, pollingtype 
     FROM service where productcodeid = 'MPLS'";
  
  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }


}


#
# Added for new vpls polling - only used for generating sites.list
# sayantan.dutta@reach.com, 15 April 2009
# Pre:
# Post:
sub v_search_links {

  my $command =
    "SELECT serviceid, pollingtype FROM service where productcodeid = 'VPLS' OR productcodeid = 'ETHERNET' OR productcodeid = 'EVPL'";

  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub get_link {
open (DEBUG,">/tmp/EDIT");
  my $lid = shift @_;
  my $linkid = $dbh->quote($lid);
  my $sid = shift @_;
  my $serviceid = $dbh->quote($sid);

  # For backward compatibility - need to check whether the serviceid is upper case or not
  my $sth = $dbh->prepare("SELECT serviceid FROM link WHERE serviceid ilike ?");
  $sth->execute($sid);
  my $r = $sth->fetchrow_hashref;
  if($$r{serviceid} =~ /^$sid$/i) {
        $serviceid = $dbh->quote($$r{serviceid});
  }

  # now check if an uppercase version of linkid exists
  $sth = $dbh->prepare("SELECT linkid FROM link WHERE linkid ilike ?");
  $sth->execute($lid);
  $r = $sth->fetchrow_hashref;

 ##++IPT5
 $sth = $dbh->prepare
    ("SELECT typeid, routername, interface, bw, description, (select routername from routername where link.bha_routerid=routername.routerid) as bha_routername, bha_description,(select routername from routername where link.bha_routerid_bac=routername.routerid) as bha_routernamebac ,bha_description_bac
      FROM link
      NATURAL JOIN routername
      WHERE linkid = $linkid 
        AND serviceid = $serviceid");

  ##--IPT5
    unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchrow_arrayref;

  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh::err;
    return 0;
  }
close (DEBUG);
}

sub get_link_types {
open (DEBUG,">/tmp/get_link");
  my $lid = shift @_;
  my $linkid = $dbh->quote($lid);
  my $sid = shift @);
  my $serviceid = $dbh->quote($sid);

	my $sth;
  # For backward compatibility - need to check whether the serviceid is upper case or not
  #my $sth = $dbh->prepare("SELECT serviceid FROM link WHERE serviceid ilike ?");
  #$sth->execute($sid);
  #my $r = $sth->fetchrow_hashref;
  #if($$r{serviceid} =~ /^$sid$/i) {
  #      $serviceid = $dbh->quote($$r{serviceid});
  #}

  # now check if an uppercase version of linkid exists
  #$sth = $dbh->prepare("SELECT linkid FROM link WHERE linkid ilike ?");
  #$sth->execute($lid);
  #$r = $sth->fetchrow_hashref;
#print DEBUG "linkid:$$r{linkid}\n";
#print DEBUG "linkid_gui:$lid\n";
 # if($$r{linkid} =~ /^$lid$/i) {
#print DEBUG "INSIDE\n";
 # 	$linkid = $dbh->quote($$r{linkid});
 # }

  $sth = $dbh->prepare
    ("SELECT typeid, type FROM link_type");

 
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  my $result = $sth->fetchall_arrayref;

  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh::err;
    return 0;
  }
close (DEBUG);
}

sub check_existing_link {
	open (DEBUG1,">>/tmp/prad_addlink_DB");
  my $router = shift @_;
  my $port = shift @_;
  my $link_id = shift @_;
  my $action = shift @_;

  my $sql_command = "select * from link l, routername r where r.routerid = l.routerid and r.routername = '$router' and l.interface ilike '$port'"; 
  if ($action == 0) { #update request
     $sql_command .= " and l.linkid != '$link_id'";
  }
print $sql_command;
	print DEBUG1 "sql_command-----> $sql_command\n";
close DEBUG1;
  my $sth = $dbh->prepare($sql_command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
	print DEBUG1 "result---->$result\n";
  if ($result) {
     $error = $dbh->errstr;
	my $cnt = $sth->rows;
	print DEBUG1 "rows---->$cnt\n";
     if ($sth->rows != 0) {
        return 0;
     } else {
	return 1;
     }
  } else {
     $error = $dbh::err;
     return 0;
  }
close (DEBUG1);
}

sub get_core_link {
  my $sql_command = "SELECT regionname, cityname, popname, routername, linkid, serviceid, description from link natural join regionname natural join cityname natural join popname  natural join routername natural join link_type where type = 'CORE' order by regionname, cityname, popname, routername, serviceid;";

  my $sth = $dbh->prepare($sql_command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh::err;
    return 0;
  }
}
#++EVPL
sub fetchProdCodefromDB{
        my $servid = shift @_;
        #CMS_DB::connect_to_database;
        my $query = "select productcodeid from service where serviceid = '$servid'";
        my $sth = $dbh->prepare($query);
        $sth->execute();
        my $result = $sth->fetchrow_arrayref;
        my $productcode = $$result[0];
if (!$productcode)
{
$servid = $servid."a";
 $query = "select productcodeid from service where serviceid = '$servid'";
print DEBUG "EVPL query:$query\n";
 $sth = $dbh->prepare($query);
        $sth->execute();
        $result = $sth->fetchrow_arrayref;
        $productcode = $$result[0];
print DEBUG "EVPL product:$productcode\n";
}

return $productcode;
}

sub fetchServfromDB{

my $cond = shift @_; 
my $sth ;
if($cond eq "both"){
$sth= $dbh->prepare("select serviceid as SID from service where productcodeid <> 'EVPL' union select trim(trailing 'a' from serviceid) as SID FROM service where serviceid not like '%z' and serviceid is not null and productcodeid='EVPL' union select trim(trailing 'asw' from aggregate_serviceid)||'sw' as SID FROM aggregate_service where aggregate_serviceid not like '%zsw' and aggregate_serviceid is not null order by SID");
 }
else {
$sth= $dbh->prepare("select serviceid as SID from service where productcodeid <> 'EVPL' union select trim(trailing 'a' from serviceid) as SID FROM service where serviceid not like '%z' and serviceid is not null and productcodeid='EVPL'order by SID");
}

        $sth->execute();
        my $result = $sth->fetchall_arrayref;
        my $retString;
        foreach my $servid (@$result){
                if ( $retString ne "" ){
                        $retString=$retString.",".$$servid[0];
                }
                else {
                        $retString=$$servid[0];
               }
        }

        $sth->finish;
        #CMS_DB::disconnect_from_database;
        return $retString;
}
sub fetchLinkidsfromDB{
	my $sth ;
	$sth= $dbh->prepare("select linkid as linkid from link where serviceid not like '%[%ec%' union select trim(trailing 'a' from linkid) as linkid FROM link where serviceid like '%[%]%ec%' and serviceid not like '%[%]%ec%z' and serviceid not like '%[%]%ec%zsw' union select trim(trailing 'z' from linkid) as linkid FROM link where serviceid like '%[%]%ec%zsw' order by linkid");

        $sth->execute();
        my $result = $sth->fetchall_arrayref;
        my $retString;
        foreach my $linkid (@$result){
                if ( $retString ne "" ){
                        $retString=$retString.",".$$linkid[0];
                }
                else {
                        $retString=$$linkid[0];
               }
        }

        $sth->finish;
        return $retString;
}
sub fetchSerIDfromLinkId {
	my $link_raw = shift @_;
	my $linkid = $dbh->quote($link_raw);
	my $sth ;
	open (DEBUG1,">/tmp/tmp_prad_cms");
	$sth= $dbh->prepare("select serviceid  from link where linkid = $linkid");
	print DEBUG1 "linkid---->$linkid\n";
	$linkid=$dbh->quote($link_raw."a");
  	print DEBUG1 "linkid---->$linkid\n";	
	my $sth_evpl = $dbh->prepare("select trim(trailing 'a' from serviceid) as serviceid  from link where linkid = $linkid");
	$linkid=$dbh->quote($link_raw."z");
 	my $sth_evpl_z =  $dbh->prepare("select trim(trailing 'z' from serviceid) as serviceid  from link where linkid = $linkid");	

	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
    	my $result = $sth->fetchrow_arrayref;
        print DEBUG1 "result---->$result\n";	
	if(!$result){
		
	        $sth_evpl->execute;
                $result = $sth_evpl->fetchrow_arrayref;
		print DEBUG1 "result---->$result\n";
	
	}
	if(!$result){
		$sth_evpl_z->execute;
		$result = $sth_evpl_z->fetchrow_arrayref;
		print DEBUG1 "result---->$result\n";
	}
	my $ret_servid;
	if ($result) {
		$error = $dbh->errstr;
		$sth->finish;
		$ret_servid =$$result[0];
	} else {
		$error = $dbh->errstr;
		return 0;
	}
	$ret_servid =~ s/asw/sw/;
	$ret_servid =~ s/zsw/sw/;
	return $ret_servid;
close (DEBUG1);
}
#if linkid exists then it will return - 1 else it will return - 0 
sub check_link_exists {
  my $link_id = shift @_;

  my $sql_command = "select * from link where linkid = $link_id"; 

    my $sth = $dbh->prepare($sql_command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  if ($result) {
     $error = $dbh->errstr;
     if ($sth->rows != 0) {
        return 1;
     } else {	
	return 0;
     }
  } else {
     $error = $dbh::err;
     return 0;
  }
}
#--EVPL


######################################################################
# Name		:check_prod_bha
# Author 	:KARUNA BALLAL
# Description	:check the product type to load BHA elements for IPT, GIA
# input		:service id
# output	:product type
######################################################################

sub check_prod_bha {
	my $servid = shift @_;
	my $sql_command = "SELECT productcodeid,black_hole_routing from service WHERE serviceid = '$servid'";
	my $sth = $dbh->prepare($sql_command);
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return $error;
	}
	my @check = $sth->fetchrow_array();
	my $prod = join ",",@check;
	$error = $dbh->errstr;
	if ($error){
		return $error;
	} else {
		return $prod;
	}
}


######################################################################
# Name          :fetch_routers_bha
# Author        :KARUNA BALLAL
# Description   :Fetch all BHA routers from the DB
# input         :
# output        :List of Black Hole routers
#######################################################################
sub fetch_routers_bha {
        if ($debug) {print LINK "PM Entered fetch_routers_bha\n";}
        my $sth = $dbh->prepare("select routername, popname, community, cityname, regionname, qos, detail, prefix_update, vpls, evpl from RouterName natural join PopName natural join CityName natural join RegionName WHERE router_switch like 'BHA Router' order by routername");
        unless ($sth->execute) {
                $error = $dbh->errstr;
        }
        my $result = $sth->fetchall_arrayref();
        $sth->finish();
        return $result;
}

###################################################################################
# Name          :record_link_details
# Author        :KARUNA BALLAL
# Description   :insert deleted link details into another table for GIA, IP Transit
# input         :link id, service id
# output        :
####################################################################################
sub record_link_details {
#	CMS_DB::connect_to_database();
	#CMS_DB::begin_transaction();
	if ($debug) {print LINK "DB Entered record_link_details\n";}
	my $linkid = (shift @_);
	my $serviceid = (shift @_);
	if ($debug) {print LINK "PM linkid:$linkid\tserviceid:$serviceid\n";}
	my $check_asn_query = $dbh->prepare("SELECT as_no from service where serviceid = $serviceid and as_no not like ''");
	if ($debug) {print LINK "DB SELECT as_no from service where serviceid = $serviceid and as_no not like ''\n";}
	$check_asn_query->execute();
	my $rows_check_asn = $check_asn_query->rows();
	if ($rows_check_asn > 0) {
		my $sth;
                my $exist = $dbh->prepare("SELECT serviceid from deleted_links_routing where serviceid = $serviceid and linkid = $linkid");
                if ($debug) {print LINK "DB record_link_details SELECT serviceid from deleted_links_routing where serviceid = $serviceid and linkid = $linkid\n";}
                $exist->execute();
                my $rows = $exist->rows();
#++Modified for Addendum-2
                if ($debug) {print LINK "SELECT serviceid from deleted_links_routing where serviceid = $serviceid and linkid = $linkid\nDB rows:$rows\n";}
		if ($rows > 0) {
			#$sth = $dbh->prepare("UPDATE deleted_links_routing SET routername = routername.routername, bha_routername = (select routername from routername where link.bha_routerid=routername.routerid), bha_routername_bac = (select routername from routername where link.bha_routerid_bac=routername.routerid)  FROM link NATURAL JOIN routername WHERE link.serviceid=$serviceid and link.linkid=$linkid");
			$sth = $dbh->prepare("UPDATE deleted_links_routing SET routername = routername.routername, bha_routername = (select routername from routername where link.bha_routerid=routername.routerid), bha_routername_bac = (select routername  from routername where link.bha_routerid_bac=routername.routerid), link_deleted_timestamp = CURRENT_TIMESTAMP FROM link NATURAL JOIN routername WHERE link.serviceid=$serviceid and link.linkid=$linkid and deleted_links_routing.serviceid=$serviceid and deleted_links_routing.linkid=$linkid");
			if ($debug) {print LINK "DB record_link_details UPDATE deleted_links_routing SET routername = routername.routername, bha_routername = (select routername from routername where link.bha_routerid=routername.routerid), bha_routername_bac = (select routername  from routername where link.bha_routerid_bac=routername.routerid), link_deleted_timestamp = CURRENT_TIMESTAMP FROM link NATURAL JOIN routername WHERE link.serviceid=$serviceid and link.linkid=$linkid and deleted_links_routing.serviceid=$serviceid and deleted_links_routing.linkid=$linkid\n";}
		} else {
                        $sth = $dbh->prepare("INSERT INTO deleted_links_routing (linkid, serviceid, routername, bha_routername, link_deleted_timestamp, bha_routername_bac) (select linkid, serviceid,  routername, (select routername from routername where link.bha_routerid=routername.routerid) as bha_routername, CURRENT_TIMESTAMP, (select routername from routername where link.bha_routerid_bac=routername.routerid) as bha_routername_bac from link NATURAL JOIN routername where link.linkid=$linkid and link.serviceid in (SELECT serviceid from service WHERE serviceid=$serviceid AND (productcodeid='IPTRANSIT' OR productcodeid='GIA')))");
                        if ($debug) {print LINK "PM record_link_details query: INSERT INTO deleted_links_routing (linkid, serviceid, routername, bha_routername, link_deleted_timestamp, bha_routername_bac) (select linkid, serviceid, routername, (select routername from routername where link.bha_routerid=routername.routerid) as bha_routername, CURRENT_TIMESTAMP, (select routername from routername where link.bha_routerid_bac=routername.routerid) as bha_routername_bac from link NATURAL JOIN routername NATURAL JOIN service where (service.productcodeid like 'IPTRANSIT' or service.productcodeid like 'GIA') AND link.serviceid=$serviceid and link.linkid=$linkid)\n";}
#--Addendum-2
		}
		unless ($sth->execute) {
                	$error = $dbh->errstr;
			if ($debug) {print LINK "record_link_details error:$error\n";}
        	}
		$sth->finish;
	}	
}

#CR-84 Juniper IPVPN Report
# On addition of Link on Juniper Device having an IPVPN service present - costype is changed to reachvpls 
sub update_junos_service {
	my ($serviceid,$routerid) = @_;
	$routerid = $dbh->quote($routerid);

	my $query = "select router_software from router_software where routername = $routerid";
	my $sth = $dbh->prepare($query);
        $sth->execute;
        my $result = $sth->fetchrow_arrayref;
        my $router_os = $$result[0];
	$sth->finish;

	my $prodcode = CMS_SERVICE_DB::get_productcode_service($serviceid);
	my $prod = $$prodcode{productcodeid};
	
        if ($prod =~ /MPLS/i && $router_os =~ /Juniper/i) {
		$serviceid = $dbh->quote($serviceid);
                $sth = $dbh->prepare ("UPDATE service set costype = 'reachvpls' where serviceid = $serviceid");
                my $result = $sth->execute;
		unless ($sth->execute) {
			$error = $dbh->errstr;
			return $error;
		}
		$sth->finish;
        }
	return;
}


1;
