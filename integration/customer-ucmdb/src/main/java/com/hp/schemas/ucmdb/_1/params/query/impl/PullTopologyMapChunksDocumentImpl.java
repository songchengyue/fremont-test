/*
 * An XML document type.
 * Localname: pullTopologyMapChunks
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one pullTopologyMapChunks(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class PullTopologyMapChunksDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksDocument
{
    
    public PullTopologyMapChunksDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PULLTOPOLOGYMAPCHUNKS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "pullTopologyMapChunks");
    
    
    /**
     * Gets the "pullTopologyMapChunks" element
     */
    public com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks getPullTopologyMapChunks()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks)get_store().find_element_user(PULLTOPOLOGYMAPCHUNKS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "pullTopologyMapChunks" element
     */
    public void setPullTopologyMapChunks(com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks pullTopologyMapChunks)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks)get_store().find_element_user(PULLTOPOLOGYMAPCHUNKS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks)get_store().add_element_user(PULLTOPOLOGYMAPCHUNKS$0);
            }
            target.set(pullTopologyMapChunks);
        }
    }
    
    /**
     * Appends and returns a new empty "pullTopologyMapChunks" element
     */
    public com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks addNewPullTopologyMapChunks()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks)get_store().add_element_user(PULLTOPOLOGYMAPCHUNKS$0);
            return target;
        }
    }
}
