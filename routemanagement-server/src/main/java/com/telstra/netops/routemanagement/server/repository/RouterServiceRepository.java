package com.telstra.netops.routemanagement.server.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telstra.netops.routemanagement.server.model.Router_Software;

/**
 * Repository class for Router_Software listing
 *
 */
public interface RouterServiceRepository extends CrudRepository<Router_Software, Long> {

	@Query("SELECT r FROM Router_Software r WHERE r.routerName = :routerName")
	List<Router_Software> findAllByRoutername_RouterSoftware(@Param("routerName") String routerName);

	@Query("SELECT r FROM Router_Software r WHERE r.routerName = :routerName")
	Router_Software findOneByRoutername(@Param("routerName") String routerName);

}
