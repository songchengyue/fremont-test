/*
 * XML Type:  IDs
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IDs
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML IDs(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IDsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.IDs
{
    
    public IDsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ID$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "ID");
    
    
    /**
     * Gets array of all "ID" elements
     */
    public com.hp.schemas.ucmdb._1.types.ID[] getIDArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ID$0, targetList);
            com.hp.schemas.ucmdb._1.types.ID[] result = new com.hp.schemas.ucmdb._1.types.ID[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID getIDArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ID$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "ID" element
     */
    public int sizeOfIDArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ID$0);
        }
    }
    
    /**
     * Sets array of all "ID" element
     */
    public void setIDArray(com.hp.schemas.ucmdb._1.types.ID[] idArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(idArray, ID$0);
        }
    }
    
    /**
     * Sets ith "ID" element
     */
    public void setIDArray(int i, com.hp.schemas.ucmdb._1.types.ID id)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ID$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(id);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID insertNewID(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().insert_element_user(ID$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID addNewID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ID$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "ID" element
     */
    public void removeID(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ID$0, i);
        }
    }
}
