/*
 * XML Type:  BooleanCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.BooleanCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML BooleanCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class BooleanConditionImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.BooleanCondition
{
    
    public BooleanConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "condition");
    private static final javax.xml.namespace.QName BOOLEANOPERATOR$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "booleanOperator");
    
    
    /**
     * Gets the "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanProp getCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "condition" element
     */
    public void setCondition(com.hp.schemas.ucmdb._1.types.BooleanProp condition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().add_element_user(CONDITION$0);
            }
            target.set(condition);
        }
    }
    
    /**
     * Appends and returns a new empty "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanProp addNewCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().add_element_user(CONDITION$0);
            return target;
        }
    }
    
    /**
     * Gets the "booleanOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator.Enum getBooleanOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(BOOLEANOPERATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "booleanOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator xgetBooleanOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator)get_store().find_element_user(BOOLEANOPERATOR$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "booleanOperator" element
     */
    public void setBooleanOperator(com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator.Enum booleanOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(BOOLEANOPERATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(BOOLEANOPERATOR$2);
            }
            target.setEnumValue(booleanOperator);
        }
    }
    
    /**
     * Sets (as xml) the "booleanOperator" element
     */
    public void xsetBooleanOperator(com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator booleanOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator)get_store().find_element_user(BOOLEANOPERATOR$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator)get_store().add_element_user(BOOLEANOPERATOR$2);
            }
            target.set(booleanOperator);
        }
    }
    /**
     * An XML booleanOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.BooleanCondition$BooleanOperator.
     */
    public static class BooleanOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.BooleanCondition.BooleanOperator
    {
        
        public BooleanOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected BooleanOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
