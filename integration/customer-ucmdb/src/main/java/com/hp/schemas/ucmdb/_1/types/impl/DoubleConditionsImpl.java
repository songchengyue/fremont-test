/*
 * XML Type:  DoubleConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.DoubleConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML DoubleConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class DoubleConditionsImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.DoubleConditions
{
    
    public DoubleConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DOUBLECONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "doubleCondition");
    
    
    /**
     * Gets array of all "doubleCondition" elements
     */
    public com.hp.schemas.ucmdb._1.types.DoubleCondition[] getDoubleConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DOUBLECONDITION$0, targetList);
            com.hp.schemas.ucmdb._1.types.DoubleCondition[] result = new com.hp.schemas.ucmdb._1.types.DoubleCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "doubleCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleCondition getDoubleConditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleCondition)get_store().find_element_user(DOUBLECONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "doubleCondition" element
     */
    public int sizeOfDoubleConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DOUBLECONDITION$0);
        }
    }
    
    /**
     * Sets array of all "doubleCondition" element
     */
    public void setDoubleConditionArray(com.hp.schemas.ucmdb._1.types.DoubleCondition[] doubleConditionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(doubleConditionArray, DOUBLECONDITION$0);
        }
    }
    
    /**
     * Sets ith "doubleCondition" element
     */
    public void setDoubleConditionArray(int i, com.hp.schemas.ucmdb._1.types.DoubleCondition doubleCondition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleCondition)get_store().find_element_user(DOUBLECONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(doubleCondition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "doubleCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleCondition insertNewDoubleCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleCondition)get_store().insert_element_user(DOUBLECONDITION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "doubleCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleCondition addNewDoubleCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleCondition)get_store().add_element_user(DOUBLECONDITION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "doubleCondition" element
     */
    public void removeDoubleCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DOUBLECONDITION$0, i);
        }
    }
}
