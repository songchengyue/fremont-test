package com.telstra.netops.dns.server.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SERVICE")
public class Service implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "Service_Access_ID")
	private Long id;

	@Column(name = "SERVICE_ID")
	private String serviceId;
	
	@Column(name = "CUSTOMER_ID")
	private String customerId;
	
	@ManyToOne(fetch=FetchType.EAGER) 	
	// TODO either change the name of the field or use the same product_line in ALL tables!
	@JoinColumn(name="Product_Line", referencedColumnName = "Service_Product_Line")
	private ProductLine productLine;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductLine getProductLine() {
		return productLine;
	}

	public void setProductLine(ProductLine productLine) {
		this.productLine = productLine;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}
