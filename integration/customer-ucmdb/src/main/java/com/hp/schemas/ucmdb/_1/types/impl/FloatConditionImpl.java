/*
 * XML Type:  FloatCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.FloatCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML FloatCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class FloatConditionImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.FloatCondition
{
    
    public FloatConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "condition");
    private static final javax.xml.namespace.QName FLOATOPERATOR$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "floatOperator");
    
    
    /**
     * Gets the "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatProp getCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "condition" element
     */
    public void setCondition(com.hp.schemas.ucmdb._1.types.FloatProp condition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().add_element_user(CONDITION$0);
            }
            target.set(condition);
        }
    }
    
    /**
     * Appends and returns a new empty "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatProp addNewCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().add_element_user(CONDITION$0);
            return target;
        }
    }
    
    /**
     * Gets the "floatOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator.Enum getFloatOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FLOATOPERATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "floatOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator xgetFloatOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator)get_store().find_element_user(FLOATOPERATOR$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "floatOperator" element
     */
    public void setFloatOperator(com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator.Enum floatOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FLOATOPERATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FLOATOPERATOR$2);
            }
            target.setEnumValue(floatOperator);
        }
    }
    
    /**
     * Sets (as xml) the "floatOperator" element
     */
    public void xsetFloatOperator(com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator floatOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator)get_store().find_element_user(FLOATOPERATOR$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator)get_store().add_element_user(FLOATOPERATOR$2);
            }
            target.set(floatOperator);
        }
    }
    /**
     * An XML floatOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.FloatCondition$FloatOperator.
     */
    public static class FloatOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.FloatCondition.FloatOperator
    {
        
        public FloatOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected FloatOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
