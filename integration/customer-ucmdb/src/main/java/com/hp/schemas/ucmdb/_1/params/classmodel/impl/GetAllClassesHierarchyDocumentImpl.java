/*
 * An XML document type.
 * Localname: getAllClassesHierarchy
 * Namespace: http://schemas.hp.com/ucmdb/1/params/classmodel
 * Java type: com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.classmodel.impl;
/**
 * A document containing one getAllClassesHierarchy(@http://schemas.hp.com/ucmdb/1/params/classmodel) element.
 *
 * This is a complex type.
 */
public class GetAllClassesHierarchyDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyDocument
{
    
    public GetAllClassesHierarchyDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETALLCLASSESHIERARCHY$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getAllClassesHierarchy");
    
    
    /**
     * Gets the "getAllClassesHierarchy" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy getGetAllClassesHierarchy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy)get_store().find_element_user(GETALLCLASSESHIERARCHY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getAllClassesHierarchy" element
     */
    public void setGetAllClassesHierarchy(com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy getAllClassesHierarchy)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy)get_store().find_element_user(GETALLCLASSESHIERARCHY$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy)get_store().add_element_user(GETALLCLASSESHIERARCHY$0);
            }
            target.set(getAllClassesHierarchy);
        }
    }
    
    /**
     * Appends and returns a new empty "getAllClassesHierarchy" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy addNewGetAllClassesHierarchy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy)get_store().add_element_user(GETALLCLASSESHIERARCHY$0);
            return target;
        }
    }
}
