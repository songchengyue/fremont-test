/*
 * An XML document type.
 * Localname: getProbeScopeResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getProbeScopeResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetProbeScopeResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponseDocument
{
    
    public GetProbeScopeResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETPROBESCOPERESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbeScopeResponse");
    
    
    /**
     * Gets the "getProbeScopeResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse getGetProbeScopeResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse)get_store().find_element_user(GETPROBESCOPERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getProbeScopeResponse" element
     */
    public void setGetProbeScopeResponse(com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse getProbeScopeResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse)get_store().find_element_user(GETPROBESCOPERESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse)get_store().add_element_user(GETPROBESCOPERESPONSE$0);
            }
            target.set(getProbeScopeResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getProbeScopeResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse addNewGetProbeScopeResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse)get_store().add_element_user(GETPROBESCOPERESPONSE$0);
            return target;
        }
    }
}
