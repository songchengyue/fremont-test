/*
 * An XML document type.
 * Localname: checkViewDiscoveryProgressResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one checkViewDiscoveryProgressResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class CheckViewDiscoveryProgressResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponseDocument
{
    
    public CheckViewDiscoveryProgressResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHECKVIEWDISCOVERYPROGRESSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "checkViewDiscoveryProgressResponse");
    
    
    /**
     * Gets the "checkViewDiscoveryProgressResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponse getCheckViewDiscoveryProgressResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponse)get_store().find_element_user(CHECKVIEWDISCOVERYPROGRESSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "checkViewDiscoveryProgressResponse" element
     */
    public void setCheckViewDiscoveryProgressResponse(com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponse checkViewDiscoveryProgressResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponse)get_store().find_element_user(CHECKVIEWDISCOVERYPROGRESSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponse)get_store().add_element_user(CHECKVIEWDISCOVERYPROGRESSRESPONSE$0);
            }
            target.set(checkViewDiscoveryProgressResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "checkViewDiscoveryProgressResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponse addNewCheckViewDiscoveryProgressResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressResponse)get_store().add_element_user(CHECKVIEWDISCOVERYPROGRESSRESPONSE$0);
            return target;
        }
    }
}
