package com.telstra.netops.routemanagement.server.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.telstra.netops.routemanagement.server.model.Service;
import com.telstra.netops.routemanagement.server.model.Success;

/**
 * Repository class for Service listing
 *
 */
public interface CustomerServiceRepository extends PagingAndSortingRepository<Service, Long> {

	@Query("SELECT DISTINCT r.asNo FROM Service r WHERE r.customerId = :customerId and r.productLine = :productLine  ORDER BY r.asNo")
	List<String> findDistinctAsNumberByCustomerId(@Param("customerId") String customerId,
			@Param("productLine") String productLine);

	@Query("SELECT DISTINCT r.telstraServiceId FROM Service r WHERE r.asNo = :asNo and r.customerId = :customerId and r.productLine = :productLine  ORDER BY r.telstraServiceId")
	List<String> findDistinctTelstraServiceIdByAsNumber(@Param("asNo") String asNo,
			@Param("customerId") String customerId, @Param("productLine") String productLine);

	@Query("SELECT DISTINCT r.serviceId FROM Service r WHERE r.asNo = :asNo and r.customerId = :customerId and r.productLine = :productLine  ORDER BY r.serviceId")
	List<String> findDistinctServiceIdByAsNumber(@Param("asNo") String asNo, @Param("customerId") String customerId,
			@Param("productLine") String productLine);

	@Query("SELECT r FROM Service r WHERE r.customerId = :customerId")
	List<Service> findAllByCustomerId(@Param("customerId") String customerId);

	@Query("SELECT r FROM Service r WHERE r.serviceId = :serviceId and r.productLine = :productLine")
	Service findOneByServiceId(@Param("serviceId") String serviceId, @Param("productLine") String productLine);

	@Query("SELECT r FROM Service r WHERE r.customerId = :customerId and r.productLine = :productLine")
	List<Service> findAllByServiceId(@Param("customerId") String customerId, @Param("productLine") String productLine);

}
