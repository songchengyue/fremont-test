/*
 * An XML document type.
 * Localname: removeTriggerTQLRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one removeTriggerTQLRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class RemoveTriggerTQLRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequestDocument
{
    
    public RemoveTriggerTQLRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REMOVETRIGGERTQLREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "removeTriggerTQLRequest");
    
    
    /**
     * Gets the "removeTriggerTQLRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequest getRemoveTriggerTQLRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequest)get_store().find_element_user(REMOVETRIGGERTQLREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "removeTriggerTQLRequest" element
     */
    public void setRemoveTriggerTQLRequest(com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequest removeTriggerTQLRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequest)get_store().find_element_user(REMOVETRIGGERTQLREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequest)get_store().add_element_user(REMOVETRIGGERTQLREQUEST$0);
            }
            target.set(removeTriggerTQLRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "removeTriggerTQLRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequest addNewRemoveTriggerTQLRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerTQLRequest)get_store().add_element_user(REMOVETRIGGERTQLREQUEST$0);
            return target;
        }
    }
}
