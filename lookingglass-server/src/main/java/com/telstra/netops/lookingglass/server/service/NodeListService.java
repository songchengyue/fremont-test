package com.telstra.netops.lookingglass.server.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telstra.netops.lookingglass.server.repository.NodeRepository;
import com.telstra.netops.lookingglass.server.model.IPVersion;
import com.telstra.netops.lookingglass.server.model.Node;

@Service
public class NodeListService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NodeListService.class);

	@Autowired
	private NodeRepository repository;
		
	public List<Node> getNodes(IPVersion ipVersion)  {	
		return repository.findByIpVersionOrderByCityNameAscCountryNameAsc(ipVersion);
	}

}
