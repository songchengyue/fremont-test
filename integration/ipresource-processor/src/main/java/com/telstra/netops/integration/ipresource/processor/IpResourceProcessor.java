package com.telstra.netops.integration.ipresource.processor;

public interface IpResourceProcessor {
	
	void process(IpResource ipResource);

}
