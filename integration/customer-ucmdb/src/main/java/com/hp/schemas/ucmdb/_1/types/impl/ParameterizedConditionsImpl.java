/*
 * XML Type:  ParameterizedConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ParameterizedConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ParameterizedConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ParameterizedConditionsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ParameterizedConditions
{
    
    public ParameterizedConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NODE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "node");
    
    
    /**
     * Gets array of all "node" elements
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedCondition[] getNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(NODE$0, targetList);
            com.hp.schemas.ucmdb._1.types.ParameterizedCondition[] result = new com.hp.schemas.ucmdb._1.types.ParameterizedCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "node" element
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedCondition getNodeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedCondition)get_store().find_element_user(NODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "node" element
     */
    public int sizeOfNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(NODE$0);
        }
    }
    
    /**
     * Sets array of all "node" element
     */
    public void setNodeArray(com.hp.schemas.ucmdb._1.types.ParameterizedCondition[] nodeArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(nodeArray, NODE$0);
        }
    }
    
    /**
     * Sets ith "node" element
     */
    public void setNodeArray(int i, com.hp.schemas.ucmdb._1.types.ParameterizedCondition node)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedCondition)get_store().find_element_user(NODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(node);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "node" element
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedCondition insertNewNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedCondition)get_store().insert_element_user(NODE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "node" element
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedCondition addNewNode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedCondition)get_store().add_element_user(NODE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "node" element
     */
    public void removeNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(NODE$0, i);
        }
    }
}
