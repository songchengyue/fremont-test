package com.telstra.netops.lookingglass.server.controller;

import java.security.Principal;
import java.util.List;

import com.telstra.netops.lookingglass.server.model.ClientNodeTestRequest;
import com.telstra.netops.lookingglass.server.model.IPVersion;
import com.telstra.netops.lookingglass.server.model.Node;
import com.telstra.netops.lookingglass.server.model.NodeTestResult;
import com.telstra.netops.lookingglass.server.model.ProductLine;
import com.telstra.netops.lookingglass.server.model.TracerouteHop;
import com.telstra.netops.lookingglass.server.service.NodeTestService;
import com.telstra.netops.lookingglass.server.service.ProductToolMappingService;
import com.telstra.netops.lookingglass.server.service.NodeListService;
import com.telstra.netops.lookingglass.server.service.TracerouteTestService;
import com.telstra.oauth2.client.CustomerIdResolver;
import com.telstra.oauth2.client.UserProfileResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LookingGlassController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LookingGlassController.class);
	
	@Autowired
	private NodeListService nodeListService;
	
	@Autowired
	private ProductToolMappingService productToolMappingService;
	
	@Autowired
	private NodeTestService nodeTestService;
	
	@Autowired
	private TracerouteTestService tracerouteTestService;
	
	@Autowired
	private CustomerIdResolver customerIdResolver;
	
	@Autowired
	private UserProfileResolver userProfileResolver;

	@RequestMapping(value = "/ping", method=RequestMethod.GET)
	public @ResponseBody NodeTestResult getNodeTestResult(@RequestParam ("snode") Long snode, @RequestParam ("dnode") Long dnode, @RequestParam ("ipv") IPVersion ipVersion, @RequestParam ("numpings") Integer numPings, Principal principal) {
		ClientNodeTestRequest request = ClientNodeTestRequest.createNodeTestRequest(snode, dnode, numPings);	
		LOGGER.info("source node: "+snode+" target node: "+dnode);
		request.setCustomerId(customerIdResolver.resolve(principal));
		request.setUserProfile(userProfileResolver.resolve(principal));
		return nodeTestService.performNodeTest(request);
	}
		
	@RequestMapping(value = "/node", method=RequestMethod.GET)
	public @ResponseBody NodeTestResult  getHostTestResult(@RequestParam ("snode") Long snode, @RequestParam ("dnode") String dnode, @RequestParam ("ipv") IPVersion ipVersion, Principal principal) {		
		ClientNodeTestRequest request = ClientNodeTestRequest.createNodeHostTestRequest(snode, dnode);	
		LOGGER.info("source node: "+snode+" target node: "+dnode);
		request.setCustomerId(customerIdResolver.resolve(principal));
		return nodeTestService.performNodeHostTest(request);
	}
	
	@RequestMapping(value = "/trace", method=RequestMethod.GET)
	public @ResponseBody List<TracerouteHop> getTracerouteTestResult(@RequestParam ("snode") Long snode, @RequestParam ("dnode") String dnode, @RequestParam ("ipv") IPVersion ipVersion, Principal principal) {
		ClientNodeTestRequest request = ClientNodeTestRequest.createNodeTracerouteTestRequest(snode, dnode);	
		LOGGER.info("source node: "+snode+" target node: "+dnode);
		request.setCustomerId(customerIdResolver.resolve(principal));
		return tracerouteTestService.performTracerouteTest(request);		
	}
	
	@RequestMapping(value = "/nodes", method=RequestMethod.GET)
	public @ResponseBody List<Node> getNodes(@RequestParam ("ipv") IPVersion ipVersion) {
		return nodeListService.getNodes(ipVersion);
	}
	
	@RequestMapping(value = "/productline", method=RequestMethod.GET)
	public @ResponseBody List<ProductLine> getProduct(Principal principal) {
		return productToolMappingService.getProductLines(customerIdResolver.resolve(principal));		
	}
	
	@RequestMapping(value = "/tools", method=RequestMethod.GET)
	public @ResponseBody List<String> getTools(@RequestParam ("productLine") String productLine) {
		return productToolMappingService.getTools(productLine);
	}

}



