package com.telstra.netops.client.model;

public class AuthRequest {
	
	private String customerId;
	
	private String userdn;
	
	private String authorization;
	
	private String redirect;
	
	private String productLine;

	public String getUserdn() {
		return userdn;
	}

	public void setUserdn(String userdn) {
		this.userdn = userdn;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	public String getProductLine() {
		return productLine;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}
	
}
