# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 12 July 2001
# File: AUTH_DB.pm
#
# This module contains subroutines used to interact with the database
#
# $Id: AUTH_DB.pm,v 1.1.1.1 2003/02/20 04:37:32 rchew Exp $

package AUTH_DB;
use Exporter;
use DBI;

our @ISA = qw (Exporter);
our @EXPORT_OK = qw ($error_message
		     add_user
		     delete_user
		     add_task
		     delete_task
		     list_permissions
		     get_code);

my $error_message;
my $dbh;
my $database="users";
my $dbuser="postgres";
my $host="reachdev.telstra.net";

use constant SUCCESS => 1;
use constant FAIL => 0;

# connects to database
sub connect_to_database 
{
	if (defined ($dbh)) 
	{ 
    		return $dbh;
  	} 
	else 
	{ 
    		return $dbh = DBI->connect("dbi:Pg:dbname=$database","postgres");
  	}
}

# disconnect from database
sub disconnect_from_database 
{
	$dbh->disconnect;
	undef $dbh;
}


sub add_user 
{
	my ($username, $code, $firstname, $lastname, $empno, $emailaddress, $phone, $workgroup) = @_;

	connect_to_database();

	my $sth = $dbh->prepare("insert into Tusername(username,code) values('$username','$code')") || die "Failed to prepare adduser commadn\n";
	my $return=$sth->execute || warn "Failed to execute addused command\n";

	if($return)
	{
		my $sth = $dbh->prepare("insert into Tuserdetails(username, firstname, lastname, employeenumber, emailaddress, phone, workgroup) values('$username', '$firstname', '$lastname', '$empno', '$emailaddress', '$phone', '$workgroup')") || die "Failed to prepare adduserdetails commadn\n";
		$return=$sth->execute || warn "Failed to execute adduseddetails command\n";
	}

	disconnect_from_database();
 	return $return;
}

sub delete_user 
{
	my $username = shift @_;
	my $return;

	connect_to_database();
	
	my $sth = $dbh->prepare("delete from Tusername where username = '$username'") || die "Failed to prepare deleteuser command\n";
	$return=$sth->execute || warn "Failed to execute deleteuser command\n";

	if($return)
	{
		my $sth = $dbh->prepare("delete from Tuserdetails where username = '$username'") || die "Failed to prepare deleteuserdetails command\n";
		$return=$sth->execute || warn "Failed to execute deleteuserdetails command\n";
		if($return)
		{
			my $sth = $dbh->prepare("delete from Tusergroup where username = '$username'") || die "Failed to prepare deleteusergroup command\n";
			$return=$sth->execute || warn "Failed to execute deleteusergroup command\n";
		}
	}
	disconnect_from_database();
	return $return;
}

#sub update_password(username, password)
sub update_password 
{
	my $username = shift @_;
	my $code = shift @_;

	connect_to_database();

	my $sth = $dbh->prepare("update Tusername set code='$code' where username='$username'") || die "Failed to prepare moduser commadn\n";
	my $return=$sth->execute || warn "Failed to execute moduser command\n";

	disconnect_from_database();
 	return $return;
}

sub get_code 
{
	my $username = shift @_;
	connect_to_database();
	my $sth = $dbh->prepare("select code from Tusername where username = '$username'") || die "Failed to prepare command\n";
	$sth->execute() || warn "Failed to execute command\n";
	my $code=$sth->fetchrow_array();
	$sth->finish();
	disconnect_from_database();
	return $code;
}

sub list_user
{
	my $orderby = shift @_;

	connect_to_database();

	my $sth = $dbh->prepare("select Tuserdetails.username, Tuserdetails.firstname, Tuserdetails.lastname, Tuserdetails.workgroup, Tuserdetails.employeenumber from Tusername, Tuserdetails where Tusername.username = Tuserdetails.username order by Tuserdetails.$orderby") || die "Failed to prepare queryusername command\n";
	$sth->execute || warn "Failed to execute queryusername command\n";
	my $array_ref = $sth->fetchall_arrayref();

	disconnect_from_database();
	return $array_ref;
}

sub list_user_details
{
	my $username = shift @_;

	connect_to_database();

	my $sth = $dbh->prepare("select Tuserdetails.username, Tuserdetails.firstname, Tuserdetails.lastname, Tuserdetails.workgroup, Tuserdetails.employeenumber, Tuserdetails.emailaddress, Tuserdetails.phone from Tuserdetails where Tuserdetails.username = '$username'") || die "Failed to prepare queryuserdetail command\n";
	$sth->execute || warn "Failed to execute queryuserdetails command\n";
	my $array_ref = $sth->fetchall_arrayref();

	disconnect_from_database();
	return $array_ref;
}

sub update_user_details
{
        my($username, $firstname, $lastname, $workgroup, $employeeno, $emailaddress, $phone) = @_;

	connect_to_database();

	my $sth = $dbh->prepare("update Tuserdetails set firstname='$firstname', lastname='$lastname', workgroup='$workgroup', employeenumber='$employeeno', emailaddress='$emailaddress', phone='$phone' where username='$username'") || die "Failed to prepare moduserdetails commadn\n";
	my $return=$sth->execute || warn "Failed to execute moduserdetails command\n";

	disconnect_from_database();
 	return $return;
}

sub add_group
{
	my $group = shift @_;

	connect_to_database();

	#get a groupid
	my $sth = $dbh->prepare("select nextval ('Tgroups_seq')") || die "Failed to prepare getuniquregroupid command\n";
	$sth->execute || warn "Failed to execute getuniquregroupid command\n";
	my $groupid = $sth->fetchrow_array();

	$sth->finish();

	my $sth2 = $dbh->prepare("insert into Tgroups(groupid, groupname) values ($groupid,'$group')") || die "Failed to prepare insertgroup command\n";
	my $return=$sth2->execute || warn "Failed to execute insertgroup command\n";

	disconnect_from_database();
 	return $return;
}

sub remove_group
{
	my $group = shift @_;

	connect_to_database();


	my $sth2 = $dbh->prepare("delete from Tgroups where groupname='$group'") || die "Failed to prepare deletegroup command\n";
	my $return=$sth2->execute || warn "Failed to execute deletegroup command\n";

	disconnect_from_database();
 	return $return;
}

sub query_group
{
	my $group = shift @_;

	connect_to_database();

	my $sth = $dbh->prepare("select groupid from Tgroups where groupname like '%$group%'") || die "Failed to prepare querygroupid command\n";
	$sth->execute || warn "Failed to execute querygroupid command\n";
	my $groupid = $sth->fetchrow_array();
	$sth->finish();
	disconnect_from_database();
 	return $groupid;
}

sub get_group_list
{

	connect_to_database();

	my $sth = $dbh->prepare("select groupname from Tgroups order by groupname") || die "Failed to prepare querygroup command\n";
	$sth->execute || warn "Failed to execute querygroup command\n";
	my $array_ref = $sth->fetchall_arrayref();

	disconnect_from_database();
	return $array_ref;
}

sub get_group_task_list
{

	my $group = shift @_;
	connect_to_database();

	my $sth = $dbh->prepare("select Tsystem.system, Tsystemtask.task, Tsystemtask.systaskid from Tsystemtask, Tsystem where Tsystemtask.sysid = Tsystem.sysid order by Tsystem.system, Tsystemtask.task") || die "Failed to prepare querygroupsystemtasks command\n";
	$sth->execute || warn "Failed to execute querygroupsystemtasks command\n";
	my $systemtask_ref = $sth->fetchall_arrayref();

	my $sth = $dbh->prepare("select Tgroups.groupname, Tgroupsystemtask.systaskid from Tgroupsystemtask, Tgroups where Tgroupsystemtask.groupid = Tgroups.groupid and Tgroups.groupname='$group'") || die "Failed to prepare querygroupsystemtasks command\n";
	$sth->execute || warn "Failed to execute querygroupsystemtasks command\n";
	my $group_ref = $sth->fetchall_arrayref();

	foreach my $systask_row (@$systemtask_ref)
        {
		my $systaskid=@$systask_row[2];
		foreach my $group_row (@$group_ref)
		{
			my $group_taskid=@$group_row[1];

                        if($systaskid eq $group_taskid)
                        {
                                @$systask_row[2]=@$group_row[0];
                        }
                }

		if ($systaskid eq @$systask_row[2])
                {
                                @$systask_row[2]="";
                }
	}
	disconnect_from_database();
	return $systemtask_ref;
}

sub get_user_group_list
{

	my $username = shift @_;

	connect_to_database();

	my $sth = $dbh->prepare("select groupname, groupid from Tgroups") || die "Failed to prepare querygroups command\n";
	$sth->execute || warn "Failed to execute querygroupsystemtasks command\n";
	my $group_ref = $sth->fetchall_arrayref();

	my $sth = $dbh->prepare("select groupid, username from Tusergroup where username = '$username'") || die "Failed to prepare querygroupsystemtasks command\n";
	$sth->execute || warn "Failed to execute querygroupsystemtasks command\n";
	my $usergroup_ref = $sth->fetchall_arrayref();

	foreach my $group_row (@$group_ref)
        {
		my $groupid=@$group_row[1];

		foreach my $usergroup_row (@$usergroup_ref)
		{
                        if($groupid eq @$usergroup_row[0])
                        {
                                @$group_row[1]=$username;
                        }
                }

		if ($groupid eq @$group_row[1])
                {
                                @$group_row[1]="";
                }
	}
	disconnect_from_database();
	return $group_ref;
}

sub get_group_list_by_username
{

	my $username = shift @_;
	connect_to_database();

	my $sth = $dbh->prepare("select Tgroups.groupname from Tgroups, Tusergroup where Tusergroup.username='$username' and Tusergroup.groupid=Tgroups.groupid") || die "Failed to prepare querygroup command\n";
	$sth->execute || warn "Failed to execute querygroup command\n";
	my $array_ref = $sth->fetchall_arrayref();

	disconnect_from_database();
	return $array_ref;
}


##remove a system to the database
##Requires: sysid
##attach a group to the user.
sub add_user_to_group
{
	my $username = shift @_;
	my $groupid = shift @_;

	connect_to_database();

	my $sth = $dbh->prepare("insert into Tusergroup(groupid, username) values($groupid, '$username')") || die "Failed to prepare addusrgroup command\n";
	my $return=$sth->execute || warn "Failed to execute addusrgroup command\n";

	disconnect_from_database();
 	return $return;
}

##deattach a group to the user.
sub remove_user_from_group
{
	my $username = shift @_;
	my $groupid = shift @_;

	connect_to_database();

	my $sth = $dbh->prepare("delete from Tusergroup where groupid=$groupid and username='$username'") || die "Failed to prepare removeusrgroup command\n";
	my $return=$sth->execute || warn "Failed to execute removeusrgroup command\n";

	disconnect_from_database();
 	return $return;
}

##attach a group to a task.
sub add_task_to_group
{
	my $groupid = shift @_;
	my $systaskid = shift @_;

	connect_to_database();

	my $sth = $dbh->prepare("insert into Tgroupsystemtask(groupid, systaskid) values($groupid, $systaskid)") || die "Failed to prepare addtaskgroup command\n";
	my $return=$sth->execute || warn "Failed to execute addtaskgroup command\n";

	disconnect_from_database();
 	return $return;
}

##deattach a group to a task.
sub remove_task_from_group
{
	my $groupid = shift @_;
	my $systaskid = shift @_;

	connect_to_database();

	my $sth = $dbh->prepare("delete from Tgroupsystemtask where groupid=$groupid and systaskid=$systaskid") || die "Failed to prepare removetskgroup command\n";
	my $return=$sth->execute || warn "Failed to execute removetskgroup command\n";

	disconnect_from_database();
 	return $return;
}


##add a system to the database
sub add_system
{
	my $system = shift @_;

	connect_to_database();

	#get a systemid
	my $sth = $dbh->prepare("select nextval ('Tsystem_seq')") || die "Failed to prepare getuniquresystemid command\n";
	$sth->execute || warn "Failed to execute getuniquresystemid command\n";
	my $sysid = $sth->fetchrow_array();
	$sth->finish();

	my $sth2 = $dbh->prepare("insert into Tsystem(sysid, system) values ($sysid,'$system')") || die "Failed to prepare insertsystem command\n";
	$sth2->execute || warn "Failed to execute insertsystem command\n";
	my $return=$sth2->finish();

	disconnect_from_database();
 	return $return;
}

##remove a system to the database
##Requires: sysid
sub remove_system
{
	my $sysid = shift @_;

	connect_to_database();

	my $sth = $dbh->prepare("delete from Tsystem where sysid=$sysid") || die "Failed to prepare deletesystem command\n";
	my $return=$sth->execute || warn "Failed to execute deletesystem command\n";

	disconnect_from_database();
	return $return;
}

##query for a system in db
sub query_system
{
	my $system = shift @_;

	connect_to_database();

	#get a systemid
	my $sth = $dbh->prepare("select sysid from Tsystem where system like '%$system%'") || die "Failed to prepare querysystemid command\n";
	$sth->execute || warn "Failed to execute querysystemid command\n";
	my $sysid = $sth->fetchrow_array();
	$sth->finish();
	disconnect_from_database();
 	return $sysid;
}

##query for a system in db
sub get_system_list
{

	connect_to_database();

	#get a systemid
	my $sth = $dbh->prepare("select system from Tsystem order by system") || die "Failed to prepare querysystem command\n";
	$sth->execute || warn "Failed to execute querysystem command\n";
	my $array_ref = $sth->fetchall_arrayref();

	disconnect_from_database();
	return $array_ref;
}


# sub add_task()
sub add_task
{
	my $sysid= shift;
	my $task = shift;

	connect_to_database();

	#get a systemtaskid
	my $sth = $dbh->prepare("select nextval ('Tsystemtask_seq')") || die "Failed to prepare uniqsystemtaskid command\n";
	$sth->execute || warn "Failed to execute uniqsystemtaskid command\n";
	my $systaskid = $sth->fetchrow_array();

	my $sth = $dbh->prepare("insert into Tsystemtask(systaskid, sysid, task) values ($systaskid, $sysid, '$task')") || die "Failed to prepare addpermission command\n";
	my $return=$sth->execute || warn "Failed to execute addpermission command\n";

	return $return;
}

sub delete_task 
{
	my $sysid= shift;
	my $task = shift;

	connect_to_database();

	my $sth = $dbh->prepare("delete from Tsystemtask where sysid=$sysid and task='$task'") || die "Failed to prepare removepermission command\n";
	my $return=$sth->execute || warn "Failed to execute removepermission command\n";

	return $return;
}

##query for a system in db
sub query_systemtask
{
	my $sysid= shift @_;
	my $task= shift @_;

	connect_to_database();

	#get a systemtaskid
	my $sth = $dbh->prepare("select systaskid from Tsystemtask where sysid = $sysid and task like '%$task%'") || die "Failed to prepare querysystemtaskid command\n";
	$sth->execute || warn "Failed to execute querysystemtaskid command\n";
	my $systaskid = $sth->fetchrow_array();
	$sth->finish();
	disconnect_from_database();
	return $systaskid;
}

sub list_task
{
	connect_to_database();

	my $sth = $dbh->prepare("select Tsystem.system, Tsystemtask.task from Tsystem, Tsystemtask where Tsystemtask.sysid=Tsystem.sysid") || die "Failed to prepare querytasklist command\n";
	$sth->execute || warn "Failed to execute  querytasklist command\n";
	my $array_ref = $sth->fetchall_arrayref();
	disconnect_from_database();
 	return $array_ref;
}

#sub list_permissions (username)
sub list_task_by_username 
{
	my $username = shift;

	connect_to_database();

	my $sth = $dbh->prepare("select Tusergroup.username, Tgroup.groupname, Tsystem.system, Tsystemtask.task from Tsystem, Tgroup, Tsystemtask, Tusergroup where Tusergroup.username='$username' and Tusergroup.group=Tgroup.groupname and Tgroup.systaskid=Tsystemtask.systaskid and Tsystemtask.sysid = Tsystem.sysid") || die "Failed to prepare querysuerpriv command\n";
	$sth->execute || warn "Failed to execute querysuerpriv command\n";
	my $array_ref = $sth->fetchall_arrayref();

	disconnect_from_database();
	return $array_ref;
}

#sub check_permission ($username, $sysid, $task)
sub check_permission 
{
	my $username = shift;
	my $sysid = shift;
	my $task = shift;

	connect_to_database();

	my $sth = $dbh->prepare("select Tsystemtask.task from Tgroupsystemtask, Tgroups, Tusergroup, Tsystemtask where Tusergroup.username='$username' and Tusergroup.groupid = Tgroupsystemtask.groupid and Tgroupsystemtask.systaskid = Tsystemtask.systaskid and Tsystemtask.sysid = $sysid and Tsystemtask.task='$task'") || die "Failed to prepare checkpermission command\n";
	$sth->execute || warn "Failed to execute checkpremission command\n";
	my $permit = $sth->fetchrow_array();
	$sth->finish();
	disconnect_from_database();
	if($permit)
	{
		return SUCCESS;
	}	

	return ERROR;
}

sub get_auth
{
        my $username = shift;
        open(DEBUG,">/tmp/super1");
        print DEBUG "Value of username is $username\n"; 
        connect_to_database();
        my $sth = $dbh->prepare("select count(*) from tusergroup where username ilike '$username' and groupid = '1'")||
                  die "Failed to prepare superuser command\n";
        $sth->execute || warn "Failed to execute superuser command\n";
        my $permit = $sth->fetchrow_array();
        $sth->finish();
        disconnect_from_database();
        print DEBUG "Value of permit is $permit\n"; 
        if($permit)
        {
                return 1;
        }
          
                return 0;
}
1;
