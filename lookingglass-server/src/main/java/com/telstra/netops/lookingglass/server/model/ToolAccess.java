package com.telstra.netops.lookingglass.server.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tool_access")
public class ToolAccess implements Serializable {
	
	@Id
	@GeneratedValue
	@Column(name = "Tool_Access_ID")
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="Product_Line")
	private ProductLine productLine;
	
	@Column(name = "User_Role")
	private String userRole;
	
	@Column(name = "Tool")
	private String tool;
	
	@Column(name = "menu_access")
	private String menuAccess;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductLine getProductLine() {
		return productLine;
	}

	public void setProductLine(ProductLine productLine) {
		this.productLine = productLine;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getTool() {
		return tool;
	}

	public void setTool(String tool) {
		this.tool = tool;
	}

	public String getMenuAccess() {
		return menuAccess;
	}

	public void setMenuAccess(String menuAccess) {
		this.menuAccess = menuAccess;
	}

}
