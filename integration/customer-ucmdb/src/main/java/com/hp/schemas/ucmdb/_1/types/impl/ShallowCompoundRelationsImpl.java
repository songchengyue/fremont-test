/*
 * XML Type:  ShallowCompoundRelations
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ShallowCompoundRelations(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ShallowCompoundRelationsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations
{
    
    public ShallowCompoundRelationsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SHALLOWCOMPOUNDRELATIONS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "shallowCompoundRelations");
    
    
    /**
     * Gets array of all "shallowCompoundRelations" elements
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation[] getShallowCompoundRelationsArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SHALLOWCOMPOUNDRELATIONS$0, targetList);
            com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation[] result = new com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "shallowCompoundRelations" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation getShallowCompoundRelationsArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation)get_store().find_element_user(SHALLOWCOMPOUNDRELATIONS$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "shallowCompoundRelations" element
     */
    public int sizeOfShallowCompoundRelationsArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SHALLOWCOMPOUNDRELATIONS$0);
        }
    }
    
    /**
     * Sets array of all "shallowCompoundRelations" element
     */
    public void setShallowCompoundRelationsArray(com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation[] shallowCompoundRelationsArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(shallowCompoundRelationsArray, SHALLOWCOMPOUNDRELATIONS$0);
        }
    }
    
    /**
     * Sets ith "shallowCompoundRelations" element
     */
    public void setShallowCompoundRelationsArray(int i, com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation shallowCompoundRelations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation)get_store().find_element_user(SHALLOWCOMPOUNDRELATIONS$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(shallowCompoundRelations);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "shallowCompoundRelations" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation insertNewShallowCompoundRelations(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation)get_store().insert_element_user(SHALLOWCOMPOUNDRELATIONS$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "shallowCompoundRelations" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation addNewShallowCompoundRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCompoundRelation)get_store().add_element_user(SHALLOWCOMPOUNDRELATIONS$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "shallowCompoundRelations" element
     */
    public void removeShallowCompoundRelations(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SHALLOWCOMPOUNDRELATIONS$0, i);
        }
    }
}
