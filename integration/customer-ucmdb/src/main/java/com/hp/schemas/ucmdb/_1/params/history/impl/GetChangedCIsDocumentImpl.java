/*
 * An XML document type.
 * Localname: getChangedCIs
 * Namespace: http://schemas.hp.com/ucmdb/1/params/history
 * Java type: com.hp.schemas.ucmdb._1.params.history.GetChangedCIsDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.history.impl;
/**
 * A document containing one getChangedCIs(@http://schemas.hp.com/ucmdb/1/params/history) element.
 *
 * This is a complex type.
 */
public class GetChangedCIsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.history.GetChangedCIsDocument
{
    
    public GetChangedCIsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCHANGEDCIS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/history", "getChangedCIs");
    
    
    /**
     * Gets the "getChangedCIs" element
     */
    public com.hp.schemas.ucmdb._1.params.history.GetChangedCIs getGetChangedCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.history.GetChangedCIs target = null;
            target = (com.hp.schemas.ucmdb._1.params.history.GetChangedCIs)get_store().find_element_user(GETCHANGEDCIS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getChangedCIs" element
     */
    public void setGetChangedCIs(com.hp.schemas.ucmdb._1.params.history.GetChangedCIs getChangedCIs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.history.GetChangedCIs target = null;
            target = (com.hp.schemas.ucmdb._1.params.history.GetChangedCIs)get_store().find_element_user(GETCHANGEDCIS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.history.GetChangedCIs)get_store().add_element_user(GETCHANGEDCIS$0);
            }
            target.set(getChangedCIs);
        }
    }
    
    /**
     * Appends and returns a new empty "getChangedCIs" element
     */
    public com.hp.schemas.ucmdb._1.params.history.GetChangedCIs addNewGetChangedCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.history.GetChangedCIs target = null;
            target = (com.hp.schemas.ucmdb._1.params.history.GetChangedCIs)get_store().add_element_user(GETCHANGEDCIS$0);
            return target;
        }
    }
}
