/*
 * XML Type:  ID
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ID
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ID(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.ID.
 */
public class IDImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.hp.schemas.ucmdb._1.types.ID
{
    
    public IDImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, true);
    }
    
    protected IDImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    
    private static final javax.xml.namespace.QName TEMP$0 = 
        new javax.xml.namespace.QName("", "temp");
    
    
    /**
     * Gets the "temp" attribute
     */
    public boolean getTemp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(TEMP$0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_default_attribute_value(TEMP$0);
            }
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "temp" attribute
     */
    public org.apache.xmlbeans.XmlBoolean xgetTemp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_attribute_user(TEMP$0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_default_attribute_value(TEMP$0);
            }
            return target;
        }
    }
    
    /**
     * True if has "temp" attribute
     */
    public boolean isSetTemp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(TEMP$0) != null;
        }
    }
    
    /**
     * Sets the "temp" attribute
     */
    public void setTemp(boolean temp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(TEMP$0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(TEMP$0);
            }
            target.setBooleanValue(temp);
        }
    }
    
    /**
     * Sets (as xml) the "temp" attribute
     */
    public void xsetTemp(org.apache.xmlbeans.XmlBoolean temp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_attribute_user(TEMP$0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_attribute_user(TEMP$0);
            }
            target.set(temp);
        }
    }
    
    /**
     * Unsets the "temp" attribute
     */
    public void unsetTemp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(TEMP$0);
        }
    }
}
