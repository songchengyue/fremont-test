#!/usr/bin/perl
#
# Module : OS_REPORTS.pm
# Desc   : Centralised module for OLSS reporting system
# Last Update : 20030212 by Leo
# Changes:-
#

package OS_REPORTS;
use OLSS_PLOT;
use Exporter;

#use OS_PARA;
#require "$OS_PARA::values{libfile}{value}";

use strict;
use warnings;

#require "ctime.pl";
#require "/usr/local/lib/perl5/5.8.0/ctime.pl";
use Date::Parse;
use Date::Calc;
use OS_REPORTS_DB;
#use lib '/usr/local/www/wanchai/cms/modules';
#use CMS_OLSS;

# Global variables
our @ISA        = qw(Exporter);
#our @EXPORT     = qw(@cms_atm_sids $atm_sid_cnt @atm_sids);
#our (@cms_atm_sids, $atm_sid_cnt, @atm_sids);
our	$error;

our $encet;

# Local variables
my $debug = 0; # 0 - normal mode, 1 - debug mode
my $tdata = "/tmp/rptdata.$$";


########################### OUTPUT CODE ##############################


sub hello {
        print "Hello from OS_REPORTS.pm!\n";
}  # end of sub hello


sub screencsv {
	my @vals = @_;
	my @rvals;
	if ($vals[0] =~ /^FAIL$/) {
		shift @vals;
		push @rvals, "Content-type: text/html\n\n";
		push @rvals;
	} else {
		push @rvals, "Content-type: text/plain\n\n";
		push @rvals, @vals;
	}
	print @rvals;
}


# Pre: start date, end date, service id, service type, report type
# Post: data in arrary 
sub get_table_data {
	my @retvals = ();

	my ($sdate, $edate, $sid, $stype, $rtype) = @_;
	my ($ustart, $usdate, $uend, $uedate);

	SW: {
		# Date check and date conversion
		if (!(dateok($sdate) && dateok($edate))) {
			push @retvals, "FAIL";
			push @retvals, "Invalid date ($sdate) ($edate)";
			last SW;
		} else {
			($ustart, $usdate) = get_uxtime($sdate);
			($uend,   $uedate) = get_uxtime($edate);
		}

		# ATM
		if ($stype =~ /^ATM$/i) {
			if ($rtype =~ /^ATMSLA$/i) {
		                @retvals = OS_REPORTS_DB::get_atm_sla($ustart, $uend, $sid, 'hourly');
			} else {
				push @retvals, "FAIL";
				push @retvals, "Invalid report type ($stype) ($rtype)";
				last SW;
			}

		# FR
		} elsif ($stype =~ /^FR$/i) {
			if ($rtype =~ /^FRAVAIL$/i) {  # FR availability report
				#local_debug("ustart=$ustart|uend=$uend");
				my ($t_ustart, $t_usdate, $t_uend, $t_uedate) = get_prev_dates(2);
				@retvals = OS_REPORTS_DB::get_fr_ar($t_ustart, $t_uend, $sid);
			} elsif ($rtype =~ /^FRRTD$/i) {
				# FR round trip delay report
				@retvals = OS_REPORTS_DB::get_fr_delay($ustart, $uend, $sid);
			} elsif ($rtype =~ /^FRSLA$/i) {
				# FR DDR report
				@retvals = OS_REPORTS_DB::get_fr_sla($ustart, $uend, $sid);
			} else {
				push @retvals, "FAIL";
                                push @retvals, "Invalid report type ($stype) ($rtype)";
                                last SW;
			}
		# GBS
		} elsif ($stype =~ /^GBS$/i) {
			$rtype =~ s/Generate//g;
			$rtype =~ s/Data//g;
			$rtype =~ s/Report//g;
			$rtype =~ s/( |\+)//g;
			@retvals = OS_REPORTS_DB::get_gbs_sla($rtype, $ustart, $uend, $sid);
			#push @retvals, "FAIL";
			#push @retvals, "GBS ($rtype)($ustart, $uend, $sid)";
			#push @retvals, @retvals1;
		} else {
			push @retvals, "FAIL";
	                push @retvals, "Invalid service type ($stype)";
			last SW;
		}
	}  # end of SW:
	return @retvals;
}  # end of sub get_table_data


sub get_graph {
	my ($start, $end, $opshandle, $stype, $data, $type) = @_;
	#local_debug( $start, $end, $opshandle, $stype, $data, $type );
	my @lines = get_table_data($start, $end, $opshandle, $stype, $data);
	if ($lines[0] =~ /^FAIL$/) {
		return @lines;
	}

	my @retvals;

	# write retrieved data to temp file first and retrieve start and end date
	$start = (split(/\t/, $lines[0]))[0];
	open (T, ">$tdata");
		foreach my $tt (@lines) { 
			print T "$tt\n";
			$end = (split(/\t/, $tt))[0];
		}
	close (T);

	# plot graph
	#
	# ATM SLA
	# Type 0 - DDR
	#      1 - CTD
	#      2 - AVAILABILITY
	#
	# FR SLA
	# Type 0 - Outage
	#      1 - Round Trip Delay
	#      2 - Data Delivery Ratio (DDR)

	# Vertical min and max value
	my ($cnt, @chkflds, $vmax, $vmin);
	if ($data =~ /^ATMSLA$/i) {
		if ($type == 1) {
			@chkflds = (15, 16, 17);  # maxctd, minctd, avgctd
		}
	}
	if ($data =~ /^FR(RTD|SLA)$/i) {
		if ($type == 1) {
			@chkflds = (3, 4, 5); # maxrtd, minrtd, avgrtd
		}
	}
	$cnt = 0;
	foreach my $x (@lines) {
		my @xx = split(/\t/, $x);
		$cnt++;
		if ($cnt == 1) {
			$vmax = 0;
			$vmin = $xx[$chkflds[0]];
		}
		foreach my $xxx (@chkflds) {
			my $tv = $xx[$xxx];
			$vmax = ($tv > $vmax) ? $tv : $vmax;
			$vmin = ($tv < $vmin) ? $tv : $vmin;
		}
	}

	# yLabel
	my @ylabels;
	if ($stype =~ /^ATM$/i) {
		@ylabels = ("%:99.99000000:100.00000000", "Millisecond(s):$vmin:$vmax", "%:99:100");  # vertical_lable:min_limit:max_limit
	} elsif ($stype =~ /^FR$/i) {
		@ylabels = ("Second(s)", "Millisecond(s):$vmin:$vmax","%:99.99000000:100.00000000");
	}

	# xLabel
	my @xlabels;
	if ($stype =~ /^ATM$/i) {
		@xlabels = ("Data Delivery Ratio", "Cell Transfer Delay", "Availability");
	} elsif ($stype =~ /^FR$/i) {
		@xlabels = ("Availability", "Round Trip Delay","Data Delivery Ratio");
	}

	# Intervals
	my @intervals;
	if ($stype =~ /^ATM$/i) {
		#@intervals = (86400, 86400, 86400);
		@intervals = (3600, 3600, 3600);
	} elsif ($stype =~ /^FR$/i) {
		#@intervals = (86400, 86400);
		@intervals = (86400, 3600, 3600);
	}

	# data columns
	my @dcols = ();
	if ($stype =~ /^ATM$/i) {
		$dcols[0] = "5:-:DDR A-B Guaranteed||8:-:DDR A-B Non-guaranteed||11:-:DDR B-A Guaranteed||14:-:DDR B-A Non-guaranteed";
		$dcols[1] = "15:-:Maximum||16:-:Minimum||17:-:Average";
		$dcols[2] = "20:-:Availability";
	} elsif ($stype =~ /^FR$/i) {
		$dcols[0] = "3:-:Outage (sec)";
                $dcols[1] = "5:-:Average||3:-:Maximum||4:-:Minimum";
                $dcols[2] = "27:-:B to A CIR DDR||26:-:A to B CIR DDR||29:-:B to A EIR DDR||28:-:A to B EIR DDR";
	}

	# temp gif file name, depends on @ylabels
	my @giffiles = ();
	foreach my $i (0 .. $#ylabels) {
		$giffiles[$i] = "/tmp/gif.$$.$i";
	}

	# convert kbps to bps
	my @converts = ();
	if ($stype =~ /^ATM$/i) {
		$converts[0] = 0;
		$converts[1] = 0;
		$converts[2] = 0;
	} elsif ($stype =~ /^FR$/i) {
		$converts[0] = 0;
                $converts[1] = 0;
                $converts[2] = 0;
	}

	# graph type, e.g. LINE2
	my @graphtypes;
	if ($stype =~ /^ATM$/i) {
		@graphtypes = ("LINE2:LINE2", "LINE2", "LINE2");
	} elsif ($stype =~ /^FR$/i) {
		@graphtypes = ("LINE2", "LINE2", "LINE2");
	}

	# Parameters: datafile, giffilename, start_ux, end_ux, interval, samplesize, 
	#		ylabel, xlabel, convert [0|1], graphtype [LINE1|LINE2|...], "datacol:-:label", ...
	my $i = $type;
	my @datacols = split(/\|\|/, $dcols[$i]);
	OLSS_PLOT::rrd_plot($tdata, $giffiles[$i], $start, $end, $intervals[$i],
		$#lines, $ylabels[$i], $xlabels[$i], $converts[$i], $graphtypes[$i], "", @datacols);
	push @retvals, $giffiles[$i];

	return @retvals;
}  # end of sub get_graph 


# Check whether the date format is correct
# Pre: mm/dd/yyyy
# Post: 1 - ok, 0 - fail
sub dateok {
	my $in = $_[0];
	my @retvals = ();

	my @tmp = ();
        @tmp = split(/\//, $in);

	# input yyyy, mm, dd
	return Date::Calc::check_date($tmp[2],$tmp[0],$tmp[1]);
} # end of sub dateok


# Get unix time given a date
# Pre: mm/dd/yyyy
# Post: uxtime, date
sub get_uxtime {
        my $date = shift;

        #$date =~ s/\///g;
        #my (@v) = split(//, $date);

        my(@v) = split(/\//, $date);

        my(@mons)= ('fill', 'jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec');

        my $day   = $v[1] + 0; 
        my $month = $v[0] + 0;
        $month = $mons[$month];
        my $year = "$v[2]";

        $date = "$day $month $year";

        my $time = str2time($date, "GMT");

        return ($time, $date);
}  # end of get_uxtime


# Get a date given uxtime
# Pre: uxtime
# Post: yyyy, mm, dd, hh, min, ss
sub get_datetime {
        my $uxtime = shift;
	
	my ($yr, $mth, $day, $hr, $min, $sec) = 
		Date::Calc::Add_Delta_DHMS(1970, 1, 1, 0, 0, 0,
						0, 0, 0, $uxtime);

        return ($yr, $mth, $day, $hr, $min, $sec);
}  # end of get_datatime


# Generate sla reports
# Pre: $opshandle, $data, $start, $end, $disp, code, svccode, [type], accno
sub gen_sla_rpt {
        #my ($opshandle, $data, $start, $end, $disp, $code, $svc, $type, $accno) = @_;
        my $opshandle = shift @_;
        my $data = shift @_;
        my $start = shift @_;
        my $end = shift @_;
        my $disp = shift @_;
        my $code = shift @_;
        my $svc = shift @_;
        my $type = shift @_;
        my $accno = shift @_;
	my (@tmp, @vals);

	my $stype = get_productcode_by_serviceid($opshandle);

if (1) {
	open(XX, ">/tmp/leo-debug~OS_REPORT.pm~gen_sla_rpt");
	print XX "opshandle($opshandle) data($data) start($start) end($end) disp($disp) code($code) svc($svc) type($type) stype($stype) accno($accno)\n";
	close(XX);
}

	# advance end date if start day is same as end day
	if ($start eq $end) {
		my @xx = split(/\//, $end);
		my ($xy, $xm, $xd) = Date::Calc::Add_Delta_YMD($xx[2], $xx[0], $xx[1], 0, 0, 1);  # advance 1 day
		$xm = (length($xm) != 2) ? "0$xm" : $xm;
		$xd = (length($xd) != 2) ? "0$xd" : $xd;
		$end = "$xm".'/'."$xd".'/'."$xy";
	}

	# get data from db
	if ($disp =~ /^(TABLE|CSV)$/i) {
		# table format
		@tmp = get_table_data($start, $end, $opshandle, $stype, $data);
	} elsif ($disp =~ /GRAPH/i) {
		# graph format
		# e.g. $stype = ATM, $data = ATMSLA, $type = 0, return gif filename
		@tmp = get_graph($start, $end, $opshandle, $stype, $data, $type);
		#local_debug( @tmp );
	}
	push @vals, @tmp;

	# further makeup
	if ($vals[0] !~ /FAIL/i) {
		if ($stype =~ /ATM/i) {	 # service type is ATM
			if ($data =~ /ATMSLA/i) { # report type is atmsla
				gen_sla_rpt_atm($accno, $opshandle, $data, $start, $end, $disp, $code, $svc, $stype, $type, @vals);
			} else {
				push @vals, "FAIL";
				push @vals, "Unknown data type $data.";
				return @vals;
			}
		} elsif ($stype =~ /FR/i) {  # service type is FR
			if ($data =~ /^FR(AVAIL|RTD|SLA)$/i) { # report type is fravail
				gen_sla_rpt_fr($accno, $opshandle, $data, $start, $end, $disp, $code, $svc, $stype, $type, @vals);
				#gen_fr_rpt_avail($opshandle, $code, $svc, $disp, @vals);
			} else {
				push @vals, "FAIL";
				push @vals, "Unknown data type $data.";
				return @vals;
			}
		} elsif ($stype =~ /GBS/i) { # New for GBS 2 Jan 2006
			# check $data to see if mthlyavail, mthlyperf, biperf, weeklyperf, dailyperf, hourlyperf, 15minperf	
			# $data = mthlyavail, mthlyperf, biperf, weeklyperf, dailyperf, hourlyperf, 15minperf
			gen_sla_rpt_gbs($accno, $opshandle, $data, $start, $end, $disp, $code, $svc, $stype, $type, @vals);
		} else { # stype mismatch
			push @vals, "FAIL";
			push @vals, "Unknown service type $stype.";
			return @vals;
		}
	} else { # error found
		return @vals;
	}
}


# Pre : $accno, $opshandle, $data, $start, $end, $disp, $code, $svc, $stype, $type, @vals
sub gen_sla_rpt_atm {
        my $accno = shift @_;
	my $opshandle = shift @_;
	#require "$OS_PARA::values{libfile}{value}";
        my $svclabel = &get_label_by_serviceid($accno, $opshandle, 0);
        my $serviceid_alt = &get_serviceid_alt($accno, $opshandle, 0);
        my $displaysvc = ($svclabel) ? "$svclabel ($opshandle)" : $opshandle;

        my $displaysvc = '';
        if (($svclabel) && ($serviceid_alt)) {
                $svclabel =~ s/\^//g;
                $displaysvc = "$svclabel ($serviceid_alt) ($opshandle)";
        } elsif ($svclabel) {
                $svclabel =~ s/\^//g;
                $displaysvc = "$svclabel ($opshandle)";
        } elsif ($serviceid_alt) {
                $displaysvc = "$serviceid_alt ($opshandle)";
        }

	my $data  = shift @_;
	my $start = shift @_;
	my $end   = shift @_;
	my $disp  = shift @_;
	my $code  = shift @_;
	my $svc   = shift @_;
	my $stype = shift @_;
	my $type  = shift @_;
	my @lines = @_;
	my @retvals = ();
	my %mths = ("01", "jan", "02", "feb", "03", "mar", "04", "apr", "05", "may", "06", "jun", "07", "jul", "08", "aug", "09", "sep", "10", "oct", "11", "nov", "12", "dec");
	my $cnt = 0;

	# calc the average
	my ($ctdmin, $ctdmax);
	my ($fromd, $tod);
	my ($tot_a_tx_g, $tot_a_rx_g, $tot_b_tx_g, $tot_b_rx_g);
	my ($tot_a_tx_ng, $tot_a_rx_ng, $tot_b_tx_ng, $tot_b_rx_ng);
	#my ($tot_a_ddr_g, $tot_a_ddr_ng, $tot_b_ddr_g, $tot_b_ddr_ng);
	my ($tot_maxctd, $tot_minctd, $tot_avgctd);
	my ($tot_av);
	#my ($avg_a_ddr_g, $avg_a_ddr_ng, $avg_b_ddr_g, $avg_b_ddr_ng);
	my ($a_ddr_g, $a_ddr_ng, $b_ddr_g, $b_ddr_ng);
	#my ($avg_maxctd, $avg_minctd, $avg_avgctd);
	my ($max_maxctd, $min_minctd, $avg_avgctd);
	my ($avg_av);
	#if ($disp =~ /TABLE|GRAPH/i) {
	if ($disp =~ /TABLE/i) {
		foreach my $tmp (@lines) {
			$cnt++;
			my @v = split(/\t/, $tmp);

			if ($cnt == 1) { 
				$fromd = $v[2] ;
				$ctdmin = $v[16];
				$ctdmax = $v[15];
			};
			$tod = $v[2];

			$ctdmin = ($v[16] < $ctdmin) ? $v[16] : $ctdmin;
			$ctdmax = ($v[15] > $ctdmax) ? $v[15] : $ctdmax;

			$tot_a_tx_g   += $v[3];
			$tot_a_rx_g   += $v[4];
			#$tot_a_ddr_g  += $v[5];
			$tot_a_tx_ng  += $v[6];
			$tot_a_rx_ng  += $v[7];
			#$tot_a_ddr_ng += $v[8];
			$tot_b_tx_g   += $v[9];
			$tot_b_rx_g   += $v[10];
			#$tot_b_ddr_g  += $v[11];
			$tot_b_tx_ng  += $v[12];
			$tot_b_rx_ng  += $v[13];
			#$tot_b_ddr_ng += $v[14];
			$tot_maxctd   += $v[15];
			$tot_avgctd   += $v[17];
			$tot_minctd   += $v[16];
			$tot_av       += $v[20];
			
		}
		$fromd = substr($fromd,9,10);
		$fromd = $mths{(split(/\//,$fromd))[0]}.substr($fromd,2,9);
		$tod = substr($tod,9,10);
		$tod = $mths{(split(/\//,$tod))[0]}.substr($tod,2,9);

		#$avg_a_ddr_g  = $tot_a_ddr_g  / $cnt;
		#$avg_a_ddr_ng = $tot_a_ddr_ng / $cnt;
		#$avg_b_ddr_g  = $tot_b_ddr_g  / $cnt;
		#$avg_b_ddr_ng = $tot_b_ddr_ng / $cnt;

		# DDR calculation
		# A-B guaranteed
		if ($tot_a_tx_g == $tot_a_rx_g) {
			$a_ddr_g = 100;
		} elsif ( ($tot_a_tx_g != 0) && ($tot_a_rx_g == 0) ) {
			$a_ddr_g = 0;
		} else {
			$a_ddr_g = ($tot_a_tx_g != 0) ? ( 1 - (($tot_a_tx_g - $tot_a_rx_g) / $tot_a_tx_g)) * 100 : 0;
		}
		# A-B non-guaranteed
		if ($tot_a_tx_ng == $tot_a_rx_ng) {
			$a_ddr_ng = 100;
		} elsif ( ($tot_a_tx_ng != 0) && ($tot_a_rx_ng == 0) ) {
			$a_ddr_ng = 0;
		} else {
			$a_ddr_ng = ($tot_a_tx_ng != 0) ? ( 1 - (($tot_a_tx_ng - $tot_a_rx_ng) / $tot_a_tx_ng)) * 100 : 0;
		}
		# B-A guaranteed
		if ($tot_b_tx_g == $tot_b_rx_g) {
			$b_ddr_g = 100;
		} elsif ( ($tot_b_tx_g != 0) && ($tot_b_rx_g == 0) ) {
			$b_ddr_g = 0;
		} else {
			$b_ddr_g = ($tot_b_tx_g != 0) ? ( 1 - (($tot_b_tx_g - $tot_b_rx_g) / $tot_b_tx_g)) * 100 : 0;
		}
		# B-A non-guaranteed
		if ($tot_b_tx_ng == $tot_b_rx_ng) {
			$b_ddr_ng = 100;
		} elsif ( ($tot_b_tx_ng != 0) && ($tot_b_rx_ng == 0) ) {
			$b_ddr_ng = 0;
		} else {
			$b_ddr_ng = ($tot_b_tx_ng != 0) ? ( 1 - (($tot_b_tx_ng - $tot_b_rx_ng) / $tot_b_tx_ng)) * 100 : 0;
		}

		#$avg_maxctd   = $tot_maxctd   / $cnt;
		#$avg_minctd   = $tot_minctd   / $cnt;
		$max_maxctd   = $ctdmax;
		$avg_avgctd   = $tot_avgctd   / $cnt;
		$min_minctd   = $ctdmin;
		$avg_av       = $tot_av       / $cnt;
	}

	if ($disp =~ /TABLE/i) {  # display in table format
		#push @retvals, "<p class=header>SLA Report for $opshandle</p><p class=text>\n";
		push @retvals, "<p class=header>SLA Report for $displaysvc</p><p class=text>\n";
		push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=71&et=$encet\">";
		push @retvals, "Click here to generate another report.</a>\n<p class=text>";

		my $th = "<td class=th>";
		my $tp = "<td class=text>";
		my $thx = "<td class=thx>";


		# new table summary for ATM SLA
		push @retvals, "<p class=header>Summary<p>";
		push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>Period (mmm/dd/yyyy)</td><td class=text>$fromd - $tod</td></tr>\n";
                push @retvals, "<tr></tr>\n";
		#push @retvals, "<tr><td>a_ddr_g($a_ddr_g) a_ddr_ng($a_ddr_ng) b_ddr_g($b_ddr_g) b_ddr_ng($b_ddr_ng) max_maxctd($max_maxctd) avg_avgctd($avg_avgctd) min_minctd($min_minctd) avg_av($avg_av) ctdmax($ctdmax) ctdmin($ctdmin)</td></tr>";
		push @retvals, "<tr><td class=th colspan=2>DDR (%)</td></tr>\n";
		my $tfmt = '%11.8f';
		$a_ddr_g = sprintf("$tfmt", $a_ddr_g);
		$a_ddr_ng = sprintf("$tfmt", $a_ddr_ng);
		$b_ddr_g = sprintf("$tfmt", $b_ddr_g);
		$b_ddr_ng = sprintf("$tfmt", $b_ddr_ng);
		push @retvals, "<tr><td class=text>DDR A-B Guaranteed</td><td class=text>$a_ddr_g</td></tr>\n";
		push @retvals, "<tr><td class=text>DDR A-B Non-guaranteed</td><td class=text>$a_ddr_ng</td></tr>\n";
		push @retvals, "<tr><td class=text>DDR B-A Guaranteed</td><td class=text>$b_ddr_g</td></tr>\n";
		push @retvals, "<tr><td class=text>DDR B-A Non-guaranteed</td><td class=text>$b_ddr_ng</td></tr>\n";
		push @retvals, "<tr><td class=th colspan=2>Cell Transfer Delay (ms)</td></tr>\n";
		$tfmt = '%7.3f';
		$max_maxctd = sprintf("$tfmt", $max_maxctd);
		$avg_avgctd = sprintf("$tfmt", $avg_avgctd);
		$min_minctd = sprintf("$tfmt", $min_minctd);
		push @retvals, "<tr><td class=text>CTD Maximum</td><td class=text>$max_maxctd</td></tr>\n";
		push @retvals, "<tr><td class=text>CTD Average</td><td class=text>$avg_avgctd</td></tr>\n";
		push @retvals, "<tr><td class=text>CTD Minimum</td><td class=text>$min_minctd</td></tr>\n";
		push @retvals, "<tr><td class=th colspan=2>Availability (%)</td></tr>\n";
		$tfmt = '%7.4f';
		$avg_av = sprintf("$tfmt", $avg_av);
		push @retvals, "<tr><td class=text>Availability</td><td class=text>$avg_av</td></tr>\n";

		push @retvals, "</table>";


if (0) {  # old summary

		push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>Date</td>$th DDR<br>A-B Guaranteed (%)</td>$th DDR<br>A-B Non-guaranteed (%)</td>$th DDR<br>B-A Guaranteed (%)</td>$th DDR<br>B-A Non-guaranteed (%)</td>$th Cell Transfer Delay (Max.)</td>$th Cell Transfer Delay (Avg.)</td>$th Cell Transfer Delay (Min.)</td>$th Availability(%)</td><tr>\n";

		foreach my $tmp (@lines) {
			my @v = split(/\t/, $tmp);
			my $logdate = $v[2];
			my $A_dailyddr_g  = $v[5];
			my $A_dailyddr_ng = $v[8];
			my $B_dailyddr_g  = $v[11];
			my $B_dailyddr_ng = $v[14];
			my $dailymaxctd = $v[15];
			my $dailyavgctd = $v[17];
			my $dailyminctd = $v[16];
			my $dailyav     = $v[20];
			#local_debug( "$logdate|$dailyclr|$dailymaxctd|$dailyavgctd|$dailyminctd");
			push @retvals, "<tr>$thx $logdate</td>";
			push @retvals, "$tp $A_dailyddr_g</td>$tp $A_dailyddr_ng</td>";
			push @retvals, "$tp $B_dailyddr_g</td>$tp $B_dailyddr_ng</td>";
			push @retvals, "$tp $dailymaxctd</td>";
			push @retvals, "$tp $dailyavgctd</td>$tp $dailyminctd</td>$tp $dailyav</td>";
			push @retvals, "</tr>\n";
		}
		push @retvals, "</table>\n";

}

	} elsif ($disp =~ /GRAPH/i) {  # display in graph format
		if ($type eq '') { # level 711
			@retvals = ();
			#push @retvals, "<p class=header>Report for $opshandle</p><p class=text>";
			push @retvals, "<p class=header>Report for $displaysvc</p><p class=text>";
			#push @retvals, "Report period: $start to $end (mm/dd/yyyy)</p><p class=text>\n</p><p class=text>";
			$fromd = $mths{(split(/\//,$start))[0]}.substr($start,2,9);
			$tod = $mths{(split(/\//,$end))[0]}.substr($end,2,9);
			push @retvals, "Report period: $fromd to $tod (mmm/dd/yyyy)</p><p class=text>\n</p><p class=text>";
			#push @retvals, "Report type: $data</p>\n<p class=text>";
			push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=71&et=$encet\">";
			push @retvals, "Click here to generate another report.</a>\n<p><hr><p><p class=text>";
			$start =~ s/\//\%2f/g;
			$end =~ s/\//\%2f/g;
			my $flg = "code=$code&svc=$svc&level=7111&start=$start&end=$end&data=$data&disp=graph&et=$encet";
			#my $dflg = "code=$code&prod=$svcprodcode&data=$data&svc=$svc&level=csv&id=$$";
			my $dflg = "code=$code&svc=$svc&level=csv&start=$start&end=$end&data=$data&disp=csv&et=$encet"; # for csv download

			# Thoughput
			push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?$dflg&type=0\" target=\"_blank\">\n";
			push @retvals, "<center><img alt=\"Thoughput\" src=\"/cgi-bin/tgwciss-reports.cgi?$flg&type=0\"></p></center>\n";
			push @retvals, "</a>\n";
			# Latency
			push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?$dflg&type=1\" target=\"_blank\">\n";
			push @retvals, "<center><img alt=\"Latency\" src=\"/cgi-bin/tgwciss-reports.cgi?$flg&type=1\"></p></center>\n";
			push @retvals, "</a>\n";
			#Availability
			push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?$dflg&type=2\" target=\"_blank\">\n";
			push @retvals, "<center><img alt=\"Availability\" src=\"/cgi-bin/tgwciss-reports.cgi?$flg&type=2\"></p></center>\n";
			push @retvals, "</a>\n";

		} else { # level 7111, gen graph only
			#local_debug ("lines=$lines[0]\n");
			print "Content-type: image/gif\n\n";
			open (IN, "$lines[0]");
				while (my $gt = <IN>) {
					#push @retvals, $gt;
					print $gt;
				}
			close (IN); 
			#local_debug ("retvals=@retvals\n");
		}
	} elsif ($disp =~ /^csv$/i) {  # csv download
		#my @headers = qw(Timestamp Key Logdate A_txcell A_rxcell A_DDR A_max_calculated_cell_transfer_delay A_min_calculated_cell_transfer_delay A_avg_calculated_cell_transfer_delay A_time_available A_total_time A_availability);
		#my @headers = qw(Timestamp Key Logdate A_txcell_g A_rxcell_g A_DDR_g A_txcell_ng A_rxcell_ng A_DDR_ng B_txcell_g B_rxcell_g B_DDR_g B_txcell_ng B_rxcell_ng B_DDR_ng A_max_calculated_cell_transfer_delay A_min_calculated_cell_transfer_delay A_avg_calculated_cell_transfer_delay A_time_available A_total_time A_availability);
		my @headers = ("Timestamp","Key","Logdate","A to B transmitted guaranteed cells","A to B received guaranteed cells","A to B guaranteed DDR","A to B transmitted non guaranteed cells","A to B received non guaranteed cells","A to B non guaranteed DDR","B to A transmitted guaranteed cells","B to A received guaranteed cells","B to A guaranteed DDR","B to A transmitted non guaranteed cells","B to A received non guaranteed cells","B to A non guaranteed DDR","Maximum CTD","Minimum CTD","Average CTD","Time available","Total time",'% Availability');
		my $header = join("\t", @headers);
		my @fldi;
		if ($type == 0) {  # ddr
			@fldi = (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14);
		} elsif ($type == 1) {  # ctd
			@fldi = (0,1,2,15,16,17);
		} elsif ($type == 2) {  # availability
			@fldi = (0,1,2,18,19,20);
		}
		@lines = ($header, @lines);
		foreach my $tmp (@lines) {
			my @t = split(/\t/, $tmp);
			@t = (@fldi) ? @t[@fldi] : @t;
			$tmp = join(',', @t);
			push @retvals, "$tmp\n";
		}
		#push @retvals, "csv download";
	} else {
		push @retvals, "FAIL";
		push @retvals, "Invalid display type $disp";
		return @retvals;
	}
	return @retvals;
}


# Pre : $accno, $opshandle, $data, $start, $end, $disp, $code, $svc, $stype, $type, @vals
sub gen_sla_rpt_fr {
        my $accno = shift @_;
	my $opshandle = shift @_;
	#my $svclabel = get_label_by_serviceid($opshandle);
	#my $displaysvc = ($svclabel) ? "$svclabel ($opshandle)" : $opshandle;
        my $svclabel = &get_label_by_serviceid($accno, $opshandle, 0);
        my $serviceid_alt = &get_serviceid_alt($accno, $opshandle, 0);
        my $displaysvc = ($svclabel) ? "$svclabel ($opshandle)" : $opshandle;

        my $displaysvc = '';
        if (($svclabel) && ($serviceid_alt)) {
                $svclabel =~ s/\^//g;
                $displaysvc = "$svclabel ($serviceid_alt) ($opshandle)";
        } elsif ($svclabel) {
                $svclabel =~ s/\^//g;
                $displaysvc = "$svclabel ($opshandle)";
        } elsif ($serviceid_alt) {
                $displaysvc = "$serviceid_alt ($opshandle)";
        }

	my $data  = shift @_;
	my $start = shift @_;
	my $end   = shift @_;
	my $disp  = shift @_;
	my $code  = shift @_;
	my $svc   = shift @_;
	my $stype = shift @_;
	my $type  = shift @_;
	my @lines = @_;
	my @retvals = ();
	my %mths = ("01", "jan", "02", "feb", "03", "mar", "04", "apr", "05", "may", "06", "jun", "07", "jul", "08", "aug", "09", "sep", "10", "oct", "11", "nov", "12", "dec");
	my $cnt = 0;

	# RTD, calc the max, min and average
	my ($rtdmin, $rtdmax);
	my ($fromd, $tod);
	my ($tot_maxrtd, $tot_minrtd, $tot_avgrtd);
	my ($avg_maxrtd, $avg_minrtd, $avg_avgrtd);
	# DDR
	my ($tot_atob_cir_by, $tot_atob_eir_by, $tot_atob_tot_by);
	my ($tot_btoa_cir_by, $tot_btoa_eir_by, $tot_btoa_tot_by);
	my ($tot_begeirb);
	my ($tot_igeir, $tot_aigb, $tot_aigeirb, $tot_adiscard);
	my ($tot_egeir, $tot_bigb, $tot_bigeirb, $tot_bdiscard);
	my ($atob_cir_ddr, $btoa_cir_ddr, $atob_eir_ddr, $btoa_eir_ddr);

	if ($disp =~ /TABLE/i) {
		foreach my $tmp (@lines) {
			$cnt++;
			my @v = split(/\t/, $tmp);

			# RTD and dates
			if ($cnt == 1) {
				$fromd = $v[2] ;
				$rtdmin = $v[4];
				$rtdmax = $v[3];
			};
			$tod = $v[2];
			
			$rtdmin = ($v[4] < $rtdmin) ? $v[4] : $rtdmin;
			$rtdmax = ($v[3] > $rtdmax) ? $v[3] : $rtdmax;
			
			$tot_maxrtd   += $v[3];
			$tot_avgrtd   += $v[5];
			$tot_minrtd   += $v[4];

			# DDR
			my $offset = 6; # first 6 element is for RTD
			if ($v[$offset] eq '') {  # check whether the element is exist
			} else {
				$tot_atob_cir_by += $v[5 + $offset];
				$tot_atob_tot_by += $v[6 + $offset];
				$tot_btoa_cir_by += $v[3 + $offset];
				$tot_btoa_tot_by += $v[4 + $offset];
				$tot_igeir       += $v[12 + $offset];
				$tot_begeirb      = $tot_igeir;
				$tot_aigb        += $v[8 + $offset];
				$tot_aigeirb     += $v[9 + $offset];
				$tot_adiscard    += $v[10 + $offset];
				$tot_egeir       += $v[7 + $offset];
				$tot_bigb        += $v[13 + $offset];
				$tot_bigeirb     += $v[14 + $offset];
				$tot_bdiscard    += $v[15 + $offset];
			}
		}
		$fromd = substr($fromd,9,10);
		$fromd = $mths{(split(/\//,$fromd))[0]}.substr($fromd,2,9);
		$tod = substr($tod,9,10);
		$tod = $mths{(split(/\//,$tod))[0]}.substr($tod,2,9);
		
		$avg_maxrtd = $tot_maxrtd   / $cnt;
		$avg_avgrtd = $tot_avgrtd   / $cnt;
		$avg_minrtd = $tot_minrtd   / $cnt;

		# DDR
		$tot_atob_eir_by = $tot_atob_tot_by - $tot_atob_cir_by;
		$tot_btoa_eir_by = $tot_btoa_tot_by - $tot_btoa_cir_by;

		my $ddr_100 = ($tot_aigb==$tot_atob_tot_by) ? 1 : 0;
		my $ddr_0   = (($tot_aigb>0)&&($tot_atob_tot_by==0)) ? 1 : 0;
		my $t;

		if ($ddr_100) {
			$atob_cir_ddr = 100;
			$btoa_cir_ddr = 100;
			$atob_eir_ddr = 100;
			$btoa_eir_ddr = 100;
		} elsif ($ddr_0) {
			$atob_cir_ddr = 0;
			$btoa_cir_ddr = 0;
			$atob_eir_ddr = 0;
			$btoa_eir_ddr = 0;
		} else {
			$t = ($tot_aigb - $tot_aigeirb - $tot_adiscard);
			$atob_cir_ddr = ($t > 0 ) ? (($tot_atob_tot_by - $tot_igeir)/$t) * 100 : 0;

			$t = ($tot_bigb - $tot_bigeirb - $tot_bdiscard);
			$btoa_cir_ddr = ($t > 0 ) ? (($tot_btoa_tot_by - $tot_egeir)/$t) * 100 : 0;

			$t = ($tot_aigeirb);
			$atob_eir_ddr = ($t > 0 ) ? (($tot_igeir)/$t) * 100 : 0;
			$atob_eir_ddr = ($tot_igeir == $t) ? 100 : $atob_eir_ddr;

			$t = ($tot_bigeirb);
			$btoa_eir_ddr = ($t > 0 ) ? (($tot_egeir)/$t) * 100 : 0;
			$btoa_eir_ddr = ($tot_egeir == $t) ? 100 : $btoa_eir_ddr;
		}
		my $tfmt = '%.3f';
		$atob_cir_ddr = sprintf("$tfmt",$atob_cir_ddr);
		$btoa_cir_ddr = sprintf("$tfmt",$btoa_cir_ddr);
		$atob_eir_ddr = sprintf("$tfmt",$atob_eir_ddr);
		$btoa_eir_ddr = sprintf("$tfmt",$btoa_eir_ddr);

	}

	if ($data =~ /^FR(RTD|SLA)$/i) {
		if ($disp =~ /TABLE/i) {  # display in table format
			#push @retvals, "<p class=header>SLA Report for $opshandle</p><p class=text>\n";
			push @retvals, "<p class=header>SLA Report for $displaysvc</p><p class=text>\n";
			push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=71&et=$encet\">";
			push @retvals, "Click here to generate another report.</a>\n<p class=text>";
	
			my $th = "<td class=th nowrap>";
			my $tp = "<td class=text>";
			my $thx = "<td class=thx>";
			my ($switch_s, $switch_d) = split(/\-/,(split(/\t/,$lines[0]))[1]);
			my ($sw_s, $ccode_s, $cname_s) = get_city_by_switchcode( $switch_s );
			my ($sw_d, $ccode_d, $cname_d) = get_city_by_switchcode( $switch_d );

			# new summary table for fr rtd
			push @retvals, "<p class=header>Summary<p>";
			push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>Period (mmm/dd/yyyy)</td><td class=text>$fromd - $tod</td></tr>\n"; 
			push @retvals, "<tr></tr>\n";
			push @retvals, "<tr><td class=th colspan=1>Switch</td><td class=text>$cname_s to $cname_d <!--$switch_s $switch_d--></td></tr>\n";
			#push @retvals, "<tr><td class=text colspan=2>$cname_s to $cname_d <!--$switch_s $switch_d--></td></tr>\n";
			push @retvals, "<tr></tr>\n";
			#push @retvals, "<tr><td>avg_maxrtd($avg_maxrtd) avg_avgrtd($avg_avgrtd) avg_minrtd($avg_minrtd) rtdmax($rtdmax) rtdmin($rtdmin)</td></tr>";
			push @retvals, "<!--avg_maxrtd($avg_maxrtd) avg_avgrtd($avg_avgrtd) avg_minrtd($avg_minrtd) rtdmax($rtdmax) rtdmin($rtdmin)-->\n";
			push @retvals, "<tr><td class=th colspan=2>Round Trip Delay (ms)</td></tr>\n";
			my $tfmt = '%7.0f';
			$avg_minrtd = sprintf("$tfmt", $avg_minrtd);
			$avg_avgrtd = sprintf("$tfmt", $avg_avgrtd);
			$avg_maxrtd = sprintf("$tfmt", $avg_maxrtd);
			$rtdmin = sprintf("$tfmt", $rtdmin);
			$rtdmax = sprintf("$tfmt", $rtdmax);
			#push @retvals, "<tr><td class=text>Minimum</td><td class=text>$avg_minrtd</td></tr>\n";
			push @retvals, "<tr><td class=text>Minimum</td><td class=text>$rtdmin</td></tr>\n";
			push @retvals, "<tr><td class=text>Average</td><td class=text>$avg_avgrtd</td></tr>\n";
			#push @retvals, "<tr><td class=text>Maximum</td><td class=text>$avg_maxrtd</td></tr>\n";
			push @retvals, "<tr><td class=text>Maximum</td><td class=text>$rtdmax</td></tr>\n";
			# DDR
			push @retvals, "<tr></tr>\n";
			push @retvals, "<tr><td class=th colspan=2>DDR</td></tr>\n";
			push @retvals, "<tr><td class=text>A to B CIR DDR</td><td class=text>$atob_cir_ddr%</td></tr>\n";
			push @retvals, "<tr><td class=text>A to B EIR DDR</td><td class=text>$atob_eir_ddr%</td></tr>\n";
			push @retvals, "<tr><td class=text>B to A CIR DDR</td><td class=text>$btoa_cir_ddr%</td></tr>\n";
			push @retvals, "<tr><td class=text>B to A EIR DDR</td><td class=text>$btoa_eir_ddr%</td></tr>\n";

			push @retvals, "</table>\n";
			#

if (0) {  # old format
			push @retvals, "<p class=thx> Switch Source : $cname_s <!--$switch_s--> &nbsp;&nbsp;&nbsp; Switch Destination : $cname_d <!--$switch_d--></p>\n";
			push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>Date</td>";
			#push @retvals, "$th Switch Source</td>$th Switch Destination</td>";
			push @retvals, "$th Delay Min. (ms)</td>$th Delay Avg. (ms)</td>$th Delay Max. (ms)</td><tr>\n";
			foreach my $tmp (@lines) {
				my @v = split(/\t/, $tmp);
				my $logdate = $v[2];
				($switch_s, $switch_d) = split(/\-/,$v[1]);
				my $dailyminrtd = $v[4];
				my $dailyavgrtd = $v[5];
				my $dailymaxrtd = $v[3];
				push @retvals, "<tr>$thx $logdate</td>";
				#push @retvals, "$tp $switch_s</td>$tp $switch_d</td>";
				push @retvals, "$tp $dailyminrtd</td>$tp $dailyavgrtd</td>$tp $dailymaxrtd</td>";
				push @retvals, "</tr>\n";
			}
			push @retvals, "</table>\n";
}  # end of old format
	
		} elsif ($disp =~ /GRAPH/i) {  # display in graph format
			if ($type eq '') { # level 711
				@retvals = ();
				#push @retvals, "<p class=header>Report for $opshandle</p><p class=text>";
				push @retvals, "<p class=header>Report for $displaysvc</p><p class=text>";

				$fromd = $mths{(split(/\//,$start))[0]}.substr($start,2,9);
				$tod = $mths{(split(/\//,$end))[0]}.substr($end,2,9);
				push @retvals, "Report period: $fromd to $tod (mmm/dd/yyyy)";

open (OUT, ">/tmp/leo-debug~OS_REPORT.pm~gen_sla_rpt_fr");

				#my ($switch_s, $switch_d) = get_switchcode_by_serviceid($opshandle);

				my @t = split(/-/, $opshandle);
				my $ttype = ($t[1] =~ /\d+\.\d+/) ? 'ATM' : 'FR';
				my ($sw_s, $ccode_s, $cname_s, $fnnmatch_s, $fnntype_s) = get_switchcity_by_fnn($t[0],$ttype);
				$ttype = ($t[3] =~ /\d+\.\d+/) ? 'ATM' : 'FR';
				my ($sw_d, $ccode_d, $cname_d, $fnnmatch_d, $fnntype_d) = get_switchcity_by_fnn($t[2],$ttype);

print OUT "opshandle($opshandle) sw_s($sw_s) ccode_s($ccode_s) cname_s($cname_s) sw_d($sw_d) ccode_d($ccode_d) cname_d($cname_d)\n";

##				my ($sw_s, $ccode_s, $cname_s) = get_city_by_switchcode( $switch_s );
##				my ($sw_d, $ccode_d, $cname_d) = get_city_by_switchcode( $switch_d );
				push @retvals, "\n\n<!--sw_s($sw_s)($ccode_s)($cname_s) sw_d($sw_d)($ccode_d)($cname_d)-->\n\n";
##				push @retvals, "<br>Switch: $cname_s to $cname_d</p><p class=text>\n</p><p class=text>";
				push @retvals, "<br>Location: $cname_s to $cname_d</p><p class=text>\n</p><p class=text>";

close (OUT);

				#push @retvals, "Report period: $start to $end (mm/dd/yyyy)</p><p class=text>\n</p><p class=text>";
				#push @retvals, "Report type: $data</p>\n<p class=text>";
				push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=71&et=$encet\">";
				push @retvals, "Click here to generate another report.</a>\n<p><hr><p><p class=text>";
				$start =~ s/\//\%2f/g;
				$end =~ s/\//\%2f/g;
				my $flg = "code=$code&svc=$svc&level=7111&start=$start&end=$end&data=$data&disp=graph&et=$encet";
				my $dflg = "code=$code&svc=$svc&level=csv&start=$start&end=$end&data=$data&disp=csv&et=$encet";
				# Round Trip Delay 
				push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?$dflg&type=1\" target=\"_blank\">\n";
				push @retvals, "<center><img alt=\"Round Trip Delay\" src=\"/cgi-bin/tgwciss-reports.cgi?$flg&type=1\"></p></center>\n";
				# DDR
				push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?$dflg&type=2\" target=\"_blank\">\n";
				push @retvals, "<center><img alt=\"Data Delivery Ratio\" src=\"/cgi-bin/tgwciss-reports.cgi?$flg&type=2\"></p></center>\n";
				push @retvals, "</a>\n";
			} else { # level 7111, gen graph only
				#local_debug ("lines=$lines[0]\n");
				print "Content-type: image/gif\n\n";
				open (IN, "$lines[0]");
					while (my $gt = <IN>) {
						#push @retvals, $gt;
						print $gt;
					}
				close (IN); 
				#local_debug ("retvals=@retvals\n");
			}
		} elsif ($disp =~ /^csv$/i) {  # csv download
			my @headers = ("Timestamp","Key","Logdate","Maximum delay","Minimum delay","Average delay");
			push @headers, ("Timestamp","Key","Logdate");
			push @headers, ("B to A CIR Bytes","B to A Total Bytes","A to B CIR Bytes","A to B Total Bytes");
			push @headers, ("A End EIR Egress Bytes","A End Ingress Bytes","A End EIR Ingres Bytes","A End Discarded Bytes");
			push @headers, ("B End Egress Bytes","B End Egress EIR Bytes","B End Ingress Bytes","B End Ingress EIR Bytes","B End Discarded Bytes");
			push @headers, ("A End CIR","Service CIR","B to A Utilisation","A to B Utilisation");
			push @headers, ("A to B CIR DDR","B to A CIR DDR","A to B EIR DDR","B to A EIR DDR");
			my $header = join("\t", @headers);
			my @fldi;
			if ($type == 0) {  # availability
				@fldi = (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14);
			} elsif ($type == 1) {  # rtd 
				@fldi = (0,1,2,3,4,5);
			} elsif ($type == 2) {  # ddr
				@fldi = (0,7,8,26,27,28,29);
			}
			@lines = ($header, @lines);
			foreach my $tmp (@lines) {
				my @t = split(/\t/, $tmp);
				if ($t[$fldi[-1]] eq '') {  # check the existence of required last element
					my $tmpfirst = $t[0];
					@t = $tmpfirst;
				} else {
					@t = (@fldi) ? @t[@fldi] : @t;
				}
				my $tmpx = join(',', @t);
				push @retvals, "$tmpx\n";
			}
		} else {
			push @retvals, "FAIL";
			push @retvals, "Invalid display type $disp";
			return @retvals;
		}
	} elsif ($data =~ /^FRAVAIL$/i) {
		if ($disp =~ /TABLE/i) {  # display in table format
			#push @retvals, "<p class=header>SLA Report for $opshandle</p><p class=text>\n";
			push @retvals, "<p class=header>SLA Report for $displaysvc</p><p class=text>\n";
			push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=71&et=$encet\">";
			push @retvals, "Click here to generate another report.</a>\n<p class=text>";
	
			my $th = "<td class=th nowrap>";
			my $tp = "<td class=text>";
			my $thx = "<td class=thx>";
			#my ($switch_s, $switch_d) = split(/\-/,(split(/\t/,$lines[0]))[1]);
			#push @retvals, "<p class=thx> Switch Source : $switch_s &nbsp;&nbsp;&nbsp; Switch Destination : $switch_d</p>\n";
			push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>Month</td>";
			#push @retvals, "$th Switch Source</td>$th Switch Destination</td>";
			push @retvals, "$th Availability (".'%'.")</td><tr>\n";

			my %outages = ();
			my %totals = ();
			foreach my $tmp (@lines) {
				my @v = split(/\t/,$tmp);
				my $logdate = substr($v[2],-7,7);
				my @xx = split(/\//,$logdate);
				$logdate = $mths{$xx[0]}.'/'.$xx[1];
				my $outage = $v[3];
				$outages{$logdate} += $outage;
				$totals{$logdate} += 86400; 
			}
			foreach my $key (keys %outages) {
				push @retvals, "<tr>$thx $key</td>";
				my $otg = $outages{$key};
				my $tot = $totals{$key};
				my $av  = (($tot-$otg)/$tot)*100;
				$av = sprintf("%7.3f",$av);
				push @retvals, "$tp $av</td>";
				push @retvals, "</tr>\n";
			}
			push @retvals, "</table>\n";
		} elsif ($disp =~ /GRAPH/i) {  # display in table format
			if ($type eq '') { # level 711
				@retvals = ();
				#push @retvals, "<p class=header>Report for $opshandle</p><p class=text>";
				push @retvals, "<p class=header>Report for $displaysvc</p><p class=text>";
				my ($ustart, $usdate, $uend, $uedate) = get_prev_dates(2);
				$usdate = uc($usdate);
				$uedate = uc($uedate);
				push @retvals, "\n<!--ustart($ustart) usdate($usdate) uend($uend) uedate($uedate)-->\n";
				my @tmp = split(/ /,$usdate);
				$tmp[0] = (length($tmp[0])!=2) ? "0$tmp[0]" : $tmp[0];
				$tmp[1] = lc($tmp[1]);
				$usdate = $tmp[1].'/'.$tmp[0].'/'.$tmp[2];
				@tmp = split(/ /,$uedate);
				$tmp[0] = (length($tmp[0])!=2) ? "0$tmp[0]" : $tmp[0];
				$tmp[1] = lc($tmp[1]);
				$uedate = $tmp[1].'/'.$tmp[0].'/'.$tmp[2];

				push @retvals, "Report period: &nbsp; $usdate &nbsp; to &nbsp; $uedate</p><p class=text>\n</p><p class=text>";
				#push @retvals, "Report type: $data</p>\n<p class=text>";
				push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=71&et=$encet\">";
				push @retvals, "Click here to generate another report.</a>\n<p><hr><p><p class=text>";
				$start =~ s/\//\%2f/g;
				$end =~ s/\//\%2f/g;
				my $flg = "code=$code&svc=$svc&level=7111&start=$start&end=$end&data=$data&disp=graph";
				my $dflg = "code=$code&svc=$svc&level=csv&start=$start&end=$end&data=$data&disp=csv";
				# Availibility
				push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?$dflg&type=0\" target=\"_blank\">\n";
				push @retvals, "<center><img alt=\"Availibility\" src=\"/cgi-bin/tgwciss-reports.cgi?$flg&type=0\"></p></center>\n";
				push @retvals, "</a>\n";
			} else { # level 7111, gen graph only
				#local_debug ("lines=$lines[0]\n");
				print "Content-type: image/gif\n\n";
				open (IN, "$lines[0]");
					while (my $gt = <IN>) {
						#push @retvals, $gt;
						print $gt;
					}
				close (IN); 
				#local_debug ("retvals=@retvals\n");
			}
		} elsif ($disp =~ /^csv$/i) {  # csv download
			my @headers = ("Timestamp","Key","Logdate","Outage");
			my $header = join("\t", @headers);
			my @fldi;
			if ($type == 0) {  # availability
				@fldi = (0,1,2,3);
			} elsif ($type == 1) {  # rtd 
				@fldi = (0,1,2,3,4,5);
			}
			@lines = ($header, @lines);
			foreach my $tmp (@lines) {
				my @t = split(/\t/, $tmp);
				@t = (@fldi) ? @t[@fldi] : @t;
				my $tmpx = join(',', @t);
				push @retvals, "$tmpx\n";
			}
		} else {
			push @retvals, "FAIL";
			push @retvals, "Invalid display type $disp";
			return @retvals;
		}
	} else {
		push @retvals, "FAIL";
		push @retvals, "Invalid display data type $data";
		return @retvals;
	}
	return @retvals;
}


# Get product code by service id
# Pre : service id
# Post : product code
sub get_productcode_by_serviceid {
	my $sid = shift @_;
	OS_REPORTS_DB::connect_to_database();

	my $result = OS_REPORTS_DB::get_productcode_service($sid);
	$error = $OS_REPORTS_DB::error;

	OS_REPORTS_DB::disconnect_from_database();
	return $$result{productcodeid};
}

# Get switch code by service id
# Pre : service id
# Post : switch source, switch destination
sub get_switchcode_by_serviceid {
	my $sid = shift @_;
	OS_REPORTS_DB::connect_to_database();

	my $result = OS_REPORTS_DB::get_switchcode_service($sid);
	$error = $OS_REPORTS_DB::error;

	OS_REPORTS_DB::disconnect_from_database();
	#local_debug("($$result{switchsourcodeid}) ($$result{switchdestcodeid})");
	return $$result{switchsourcodeid}, $$result{switchdestcodeid};
}


# Get city name by switch code
# Pre : switch code
# Post : switch code, city id, city name
sub get_city_by_switchcode {
	my $sid = shift @_;
	OS_REPORTS_DB::connect_to_database();

	my $result = OS_REPORTS_DB::get_city_switchcode($sid);
	$error = $OS_REPORTS_DB::error;

	OS_REPORTS_DB::disconnect_from_database();
	#local_debug("($$result{switchsourcodeid}) ($$result{switchdestcodeid})");
	return $$result{switchcodeid}, $$result{cityid}, $$result{cityname};
}


# Get switch, city id, city name, fnnmatch, fnntype by fnn
# Pre : fnn
# Post : switch code, city id, city name, fnnpre
sub get_switchcity_by_fnn {

	my $fnn = shift @_;
	my $fnntype = shift @_;

	OS_REPORTS_DB::connect_to_database();

open (OUT, ">/tmp/leo-debug~OS_REPORT.pm~get_switchcity_by_fnn");
print OUT "fnn($fnn)\n";

	my $result = OS_REPORTS_DB::get_switchcity_fnn($fnn, $fnntype);
	$error = $OS_REPORTS_DB::error;

print OUT "result($result)\n";

	OS_REPORTS_DB::disconnect_from_database();
	#local_debug("($$result{switchsourcodeid}) ($$result{switchdestcodeid})");
	return $$result{switchcode}, $$result{cityid}, $$result{cityname}, $$result{fnnmatch}, $$result{fnntype};

close (OUT);

}


# Get label by service id
# Pre : service id
# Post : label code
sub get_label_by_serviceid {
##	my $sid = shift @_;
##	OS_REPORTS_DB::connect_to_database();
##
##	my $result = OS_REPORTS_DB::get_productcode_service($sid);
##	$error = $OS_REPORTS_DB::error;
##
##	OS_REPORTS_DB::disconnect_from_database();
##	return $$result{productcodeid};
##	#return 'LABEL TEST';
##	return OS_REPORTS_DB::get_label_service($sid);
        my $svc_label;
        my $accno = shift;
        my $ohandle = shift;
        my $label = shift;

        my $attribute = CMS_OLSS::is_wholesaler($accno);
        eval {
                if (($attribute =~ /^1$/i) && ($label =~ /^0$/i)) {
                        $svc_label = CMS_OLSS::get_reseller_label_for_serviceid($ohandle);
                } else {
                        $svc_label = CMS_OLSS::get_label_for_serviceid($ohandle);
                }
        };
        return $svc_label;
}


# Get service id alternative
# Pre: service_id
sub get_serviceid_alt {
        my $serviceid_alt;
        my $accno = shift;
        my $ohandle = shift;
        my $label = shift;

        eval {
                $serviceid_alt = CMS_OLSS::get_serviceid_alt_for_serviceid($ohandle);
        };
        return $serviceid_alt;
}


# Get previous dates
# Pre : no of last months
# Post : start date (uxtime), start date (dd mmm yyyy), end date (uxtime), end date (dd mmm yyyy)
sub get_prev_dates {
	# how many months record will be retrieved
	my $months = shift @_;

	# start day
	my ($syear, $smonth, $sday) = Date::Calc::Add_Delta_YMD(Date::Calc::Today(),0,-($months),0);
	$sday = 1;
	my $t_sdate = $smonth.'/'.$sday.'/'.$syear;

	# end day
	my ($eyear, $emonth, $eday) = Date::Calc::Add_Delta_YMD(Date::Calc::Today(),0,-1,0);
	$eday = Date::Calc::Days_in_Month($eyear, $emonth);
	my $t_edate = $emonth.'/'.$eday.'/'.$eyear;

	# convert to uxtime
	my ($t_ustart, $t_usdate) = get_uxtime($t_sdate);
	my ($t_uend,   $t_uedate) = get_uxtime($t_edate);
	$t_uend += 86399;

	return ($t_ustart, $t_usdate, $t_uend, $t_uedate);
}


sub local_debug {
	my @vals = @_;
	open (DEBUG, ">/tmp/debug-os_reports.pm");
		foreach my $t (@vals) {
			print DEBUG "$t\n";
		}
	close (DEBUG);
}

# GBS
# 25/12/2005 - Richard Chew, rchew@maettr.bpa.nu
# gen_sla_rpt_gbs($accno, $opshandle, $data, $start, $end, $disp, $code, $svc, $stype, $type, @vals);
sub gen_sla_rpt_gbs {
	my($accno, $opshandle, $data, $start, $end, $disp, $code, $svc, $stype, $type, @vals) = @_;

	my @retvals;
	my(@mons)= ('f','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
	my %tmph;
	my %perflabel = ('mthlyavail', 'Monthly Availability', 'mthlyperf','Monthly Performance','weeklyperf','Weekly Performance','dailyperf','Daily Performance',
			'biperf', 'Fortnightly Performance', 'hourlyperf', 'Hourly Performance', '15minperf','15 Minute Performance');


	#my $tlabel  = $perflabel{$vals[$#vals]} . "(XX)($vals[$#vals]) (@vals)";
	my $tlabel  = $vals[$#vals]; # . "(XX)($vals[$#vals]) (@vals)";
	pop(@vals);

	push @retvals, "<p class=header>SLA Report for $opshandle - " . $tlabel . " from $start to $end</p><p class=text>\n";
	push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=71&et=$encet\">";
        push @retvals, "Click here to generate another report.</a>\n<p class=text>";


open (my $out, ">>/tmp/rchew-genslarptgbs");
print $out "($data) -> $opshandle -> (@vals)";

	# mthlyavail
	if($data =~ /(mthlyavail)/i) {
		foreach my $i (0..$#vals) {
			my @tmpv = split(/\t/, $vals[$i]);
			#my($yr, $mth, $day, $hr, $min, $sec) = get_datetime($tmpv[0]);
			# process tmpv[0] - yyyymmddhhmmss
			my @td = split(//, $tmpv[0]);
			my $yr = $td[0] . $td[1] . $td[2] . $td[3];
			my $mth;
			if($td[4] > 0) {
				$mth = $td[4] . $td[5];
			} else {
				$mth = $td[5];
			}
			$tmph{$yr}{$mth} = $tmpv[1]; # this assumes only one value
		}

		push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>%</td>";
		foreach my $i (1..$#mons) {
			push @retvals, "<td class=th>$mons[$i]</td>";
		}
		push @retvals, "</tr>\n";

		# now sort by year first
		foreach my $key (sort numerically keys %tmph) {
			push @retvals, "<tr><td class=text>$key</td>";
			foreach my $i (1..$#mons) {
				if(exists $tmph{$key}{$i}) {
					push @retvals, "<td class=text>$tmph{$key}{$i}</td>";
				} else {
					push @retvals, "<td class=text>-</td>";
				}
			}
			push @retvals, "</tr>\n";
		}
		push @retvals, "</table>\n";
	} else {
		# headers
		push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>Date/Time</td>";
		push @retvals, "<td class=th>End</td><td class=th>BBE</td><td class=th>ES</td><td class=th>SES</td>";
		push @retvals, "<td class=th>UAS</td><td class=th>FEBBE</td><td class=th>FEES</td>";
		push @retvals, "<td class=th>FESES</td><td class=th>FEUAS</td>";
		push @retvals, "<td class=th>BBER</td><td class=th>ESR</td><td class=th>SESR</td>\n";

		# one of the perf reports
		foreach my $i (0..$#vals) {
			my @tmpv = split(/\t/, $vals[$i]);
			push @retvals, "<tr class=text>";
			foreach my $xx (0..$#tmpv) {
				push @retvals, "<td class=text>$tmpv[$xx]</td>";
			}
			push @retvals, "</tr>\n";
			
		}
		push @retvals, "</table>\n";

	}
		

print $out "\n--> (@retvals)\n";
close($out);

	return @retvals;
			

	

}

sub numerically {$a <=> $b}


1;

