/*
 * An XML document type.
 * Localname: deleteCIsAndRelationsResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * A document containing one deleteCIsAndRelationsResponse(@http://schemas.hp.com/ucmdb/1/params/update) element.
 *
 * This is a complex type.
 */
public class DeleteCIsAndRelationsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponseDocument
{
    
    public DeleteCIsAndRelationsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DELETECISANDRELATIONSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "deleteCIsAndRelationsResponse");
    
    
    /**
     * Gets the "deleteCIsAndRelationsResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse getDeleteCIsAndRelationsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse)get_store().find_element_user(DELETECISANDRELATIONSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "deleteCIsAndRelationsResponse" element
     */
    public void setDeleteCIsAndRelationsResponse(com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse deleteCIsAndRelationsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse)get_store().find_element_user(DELETECISANDRELATIONSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse)get_store().add_element_user(DELETECISANDRELATIONSRESPONSE$0);
            }
            target.set(deleteCIsAndRelationsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "deleteCIsAndRelationsResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse addNewDeleteCIsAndRelationsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse)get_store().add_element_user(DELETECISANDRELATIONSRESPONSE$0);
            return target;
        }
    }
}
