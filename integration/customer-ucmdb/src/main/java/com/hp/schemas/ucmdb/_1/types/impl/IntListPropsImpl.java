/*
 * XML Type:  IntListProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntListProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML IntListProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IntListPropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.IntListProps
{
    
    public IntListPropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INTLISTPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intListProp");
    
    
    /**
     * Gets array of all "intListProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.IntListProp[] getIntListPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INTLISTPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.IntListProp[] result = new com.hp.schemas.ucmdb._1.types.IntListProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "intListProp" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListProp getIntListPropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().find_element_user(INTLISTPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "intListProp" element
     */
    public int sizeOfIntListPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTLISTPROP$0);
        }
    }
    
    /**
     * Sets array of all "intListProp" element
     */
    public void setIntListPropArray(com.hp.schemas.ucmdb._1.types.IntListProp[] intListPropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(intListPropArray, INTLISTPROP$0);
        }
    }
    
    /**
     * Sets ith "intListProp" element
     */
    public void setIntListPropArray(int i, com.hp.schemas.ucmdb._1.types.IntListProp intListProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().find_element_user(INTLISTPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(intListProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "intListProp" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListProp insertNewIntListProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().insert_element_user(INTLISTPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "intListProp" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListProp addNewIntListProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().add_element_user(INTLISTPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "intListProp" element
     */
    public void removeIntListProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTLISTPROP$0, i);
        }
    }
}
