package com.telstra.netops.dns.server.service;

import java.util.List;

import com.telstra.netops.dns.server.model.ClientReverseDnsRequest;
import com.telstra.netops.dns.server.model.DnsReverseRecord;
import com.telstra.netops.dns.server.model.Success;
import com.telstra.netops.dns.server.model.cgi.DnsResultWrapper;

public interface ReverseDnsService {

	Success addDelegation(ClientReverseDnsRequest request);

	Success removeDelegation(ClientReverseDnsRequest request);

	DnsResultWrapper addMapping(ClientReverseDnsRequest request);
	
	DnsResultWrapper removeMapping(ClientReverseDnsRequest request);

	List<DnsReverseRecord> list(String customerId);

}
