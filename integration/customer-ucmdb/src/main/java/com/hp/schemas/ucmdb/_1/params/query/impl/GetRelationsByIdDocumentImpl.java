/*
 * An XML document type.
 * Localname: getRelationsById
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getRelationsById(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetRelationsByIdDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdDocument
{
    
    public GetRelationsByIdDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETRELATIONSBYID$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getRelationsById");
    
    
    /**
     * Gets the "getRelationsById" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetRelationsById getGetRelationsById()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetRelationsById target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetRelationsById)get_store().find_element_user(GETRELATIONSBYID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getRelationsById" element
     */
    public void setGetRelationsById(com.hp.schemas.ucmdb._1.params.query.GetRelationsById getRelationsById)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetRelationsById target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetRelationsById)get_store().find_element_user(GETRELATIONSBYID$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetRelationsById)get_store().add_element_user(GETRELATIONSBYID$0);
            }
            target.set(getRelationsById);
        }
    }
    
    /**
     * Appends and returns a new empty "getRelationsById" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetRelationsById addNewGetRelationsById()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetRelationsById target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetRelationsById)get_store().add_element_user(GETRELATIONSBYID$0);
            return target;
        }
    }
}
