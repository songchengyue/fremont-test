/*
 * XML Type:  FloatConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.FloatConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML FloatConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class FloatConditionsImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.FloatConditions
{
    
    public FloatConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FLOATCONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "floatCondition");
    
    
    /**
     * Gets array of all "floatCondition" elements
     */
    public com.hp.schemas.ucmdb._1.types.FloatCondition[] getFloatConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(FLOATCONDITION$0, targetList);
            com.hp.schemas.ucmdb._1.types.FloatCondition[] result = new com.hp.schemas.ucmdb._1.types.FloatCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "floatCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatCondition getFloatConditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatCondition)get_store().find_element_user(FLOATCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "floatCondition" element
     */
    public int sizeOfFloatConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FLOATCONDITION$0);
        }
    }
    
    /**
     * Sets array of all "floatCondition" element
     */
    public void setFloatConditionArray(com.hp.schemas.ucmdb._1.types.FloatCondition[] floatConditionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(floatConditionArray, FLOATCONDITION$0);
        }
    }
    
    /**
     * Sets ith "floatCondition" element
     */
    public void setFloatConditionArray(int i, com.hp.schemas.ucmdb._1.types.FloatCondition floatCondition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatCondition)get_store().find_element_user(FLOATCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(floatCondition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "floatCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatCondition insertNewFloatCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatCondition)get_store().insert_element_user(FLOATCONDITION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "floatCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatCondition addNewFloatCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatCondition)get_store().add_element_user(FLOATCONDITION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "floatCondition" element
     */
    public void removeFloatCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FLOATCONDITION$0, i);
        }
    }
}
