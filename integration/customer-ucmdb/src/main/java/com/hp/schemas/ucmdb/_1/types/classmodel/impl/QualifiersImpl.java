/*
 * XML Type:  Qualifiers
 * Namespace: http://schemas.hp.com/ucmdb/1/types/classmodel
 * Java type: com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.classmodel.impl;
/**
 * An XML Qualifiers(@http://schemas.hp.com/ucmdb/1/types/classmodel).
 *
 * This is a complex type.
 */
public class QualifiersImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers
{
    
    public QualifiersImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUALIFIER$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "qualifier");
    
    
    /**
     * Gets array of all "qualifier" elements
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Qualifier[] getQualifierArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(QUALIFIER$0, targetList);
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifier[] result = new com.hp.schemas.ucmdb._1.types.classmodel.Qualifier[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "qualifier" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Qualifier getQualifierArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifier target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifier)get_store().find_element_user(QUALIFIER$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "qualifier" element
     */
    public int sizeOfQualifierArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(QUALIFIER$0);
        }
    }
    
    /**
     * Sets array of all "qualifier" element
     */
    public void setQualifierArray(com.hp.schemas.ucmdb._1.types.classmodel.Qualifier[] qualifierArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(qualifierArray, QUALIFIER$0);
        }
    }
    
    /**
     * Sets ith "qualifier" element
     */
    public void setQualifierArray(int i, com.hp.schemas.ucmdb._1.types.classmodel.Qualifier qualifier)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifier target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifier)get_store().find_element_user(QUALIFIER$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(qualifier);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "qualifier" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Qualifier insertNewQualifier(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifier target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifier)get_store().insert_element_user(QUALIFIER$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "qualifier" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Qualifier addNewQualifier()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifier target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifier)get_store().add_element_user(QUALIFIER$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "qualifier" element
     */
    public void removeQualifier(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(QUALIFIER$0, i);
        }
    }
}
