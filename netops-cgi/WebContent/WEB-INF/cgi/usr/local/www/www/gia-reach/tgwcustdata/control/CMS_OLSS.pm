# Date: 12 November 2001
# File: CMS_OLSS.pm
#
# This module contains subroutines used by OLSS to connect
# to CMS.
# It simply wraps other calls to subroutines in other 
# modules.
#
# $Id: CMS_OLSS.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $
###################################################################
# File        : CMS_OLSS.pm
# Description : All library functions for accessing the DB file		
# Output of the script
# Input File  : 
# Output File :
# Author      : PRADEEP THANRAJ (Infosys Tech. Ltd., Bangalore )
# Started On  : 07 July 2009
# Modification History :
#  Date               Name             Change/Description
# ------------------------------------------------------------------
# 07Jul2009    	     Pradeep			Baselined for EVPL
# 07Jul2009    	     Pradeep			changes for EVPL
# 07Feb2010          Chandini                   Baselined for EVPL
# 13Sep2010          Chandini                   Baselined for IP-Transit
####################################################################

package CMS_OLSS;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
use CMS_OUTAGE_DB;
use CMS_NOTICES_DB;
use CMS_CUSTOMER_DB;
use CMS_TROUBLE_TICKET_DB;
use CMS_ROUTING_QUEUE_DB;
use CMS_SECDNS_DB;
use CMS_SECMX_DB;
use CMS_NEWS_DB;
use CMS_LINKS_DB;
use CMS_SERVICE_DB;
use CMS_PC_DB;
use CMS_AGGREGATES_DB;
use AUTH;
use CMS_OLSS_CUSTACC_DB;
use CMS_PRIDNS_DB;
use CMS_DNSRESOLVER_DB; 
use CMS_REVERSEDEL_DB;

our $error;

##Added By Karuna on 16-Nov-2010 for IPT5
my $debug = 1;
#To disable debug logs, uncomment the following line and vice versa
#$debug = 0;
if ($debug){open (IPT_RM, ">/data1/tmp_log/cms_olss.log");}

sub authenticate_staff {
  # Input: username, authentication code (as per AUTH module specs)
  # Output: authentication code on success, undef on failure
  my $username = shift @_;
  my $authcode = shift @_;
 
  return AUTH::authenticate_user($username, $authcode, "cms", "login");
}

# OUTAGES WRAPPERS;

sub add_customer {
  # Input: accno, fnn, company_name, password, billing option, wholesaler_accno, ep_access
  # Output: Database result
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::add_customer(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#sub verify_plaintext_password {
#  #Input: accno, password
#  #Output: encrypted password on sucess
#  
#  CMS_DB::connect_to_database;
#  my $result = CMS_CUSTOMER_DB::verify_plaintext_password(@_);
#  $error = $CMS_CUSTOMER_DB::error;
#  CMS_DB::disconnect_from_database;
#  return $result;
#}

sub verify_plaintext_password {
  #Input: accno (ep_access)|username, password
  #Output: encrypted password on sucess
  
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::verify_plaintext_password(@_); # Check ep_access PASSWORD
  $error = $CMS_CUSTOMER_DB::error;
  if (!$result) {
      my @input = @_;
      if ($input[0] =~ /^(.+)\^\^/) {
          $input[0] = $1;
      }
      $result = CMS_OLSS_CUSTACC_DB::verify_plaintext_password(@input); # Check OLSS CUST ACC PASSWORD
      $error = $CMS_OLSS_CUSTACC_DB::error;
  }
  CMS_DB::disconnect_from_database;
  return $result;
}

#sub verify_encrypted_password {
#  #Input: accno, password
#  #Output: encrypted password on sucess
#  
#  CMS_DB::connect_to_database;
#  my $result = CMS_CUSTOMER_DB::verify_encrypted_password(@_);
#  $error = $CMS_CUSTOMER_DB::error;
#  CMS_DB::disconnect_from_database;
#  return $result;
#}

sub verify_encrypted_password {
  #Input: accno, password
  #Output: encrypted password on sucess
  
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::verify_encrypted_password(@_); # Check ep_access PASSWORD
  $error = $CMS_CUSTOMER_DB::error;
  if (!$result) {
      my @input = @_;
      if ($input[0] =~ /^(.+)\^\^/) {
          $input[0] = $1;
      }
      $result = CMS_OLSS_CUSTACC_DB::verify_encrypted_password(@input); # Check OLSS CUST ACC PASSWORD
      $error = $CMS_OLSS_CUSTACC_DB::error;
  }
  CMS_DB::disconnect_from_database;
  return $result;
}

sub change_password {
  # Input: accno, old password, new password
  # Output: 1 on success
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::change_password(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_customer {
  # Input: accno
  # Output: accno, fnn, company name, billing_option, wholesaler_accno
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_customer(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_accno_for_serviceid {
  # Input: serviceid
  # Output: accno
  CMS_DB::connect_to_database;
  my $result = CMS_SERVICE_DB::get_accno_for_serviceid(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_productcode_for_serviceid {
	# Input: serviceid
	# Output: productcodeid
	my $sid = shift @_;
	CMS_DB::connect_to_database;
	my $result = CMS_SERVICE_DB::get_productcode_service($sid);
        $error = $CMS_CUSTOMER_DB::error;

	CMS_DB::disconnect_from_database;
	return $$result{productcodeid};
}

#IPT_Add_2
sub get_irrd_obj_serviceid {
	my $sid = shift @_;
        CMS_DB::connect_to_database;
        my $result = CMS_SERVICE_DB::get_irrd_obj_serviceid($sid);
        $error = $CMS_SERVICE_DB::error;

        CMS_DB::disconnect_from_database;
        return $$result{irrd_object};
}

sub set_label_for_serviceid {
	# input: serviceid, label
	# Output: 1 on success, 0 on failure

	CMS_DB::connect_to_database;
	my $result = CMS_SERVICE_DB::set_label_for_serviceid(@_);
	CMS_DB::disconnect_from_database;
	return $result;
}

sub set_label_for_serviceid_aggrmast {
        # input: serviceid, label
        # Output: 1 on success, 0 on failure

        CMS_DB::connect_to_database;
        my ($result,$master) = CMS_SERVICE_DB::set_label_for_serviceid_aggrmast(@_);
        CMS_DB::disconnect_from_database;
        return ($result,$master);
}

sub set_reseller_label_for_serviceid {
	# input: serviceid, label
	# Output: 1 on success, 0 on failure

	CMS_DB::connect_to_database;
	my $result = CMS_SERVICE_DB::set_reseller_label_for_serviceid(@_);
	CMS_DB::disconnect_from_database;
	return $result;
}

sub set_reseller_label_for_serviceid_aggrmast {
        # input: serviceid, label
        # Output: 1 on success, 0 on failure

        CMS_DB::connect_to_database;
        my ($result,$master) = CMS_SERVICE_DB::set_reseller_label_for_serviceid_aggrmast(@_);
        CMS_DB::disconnect_from_database;
        return ($result,$master);
}

sub get_label_for_serviceid {
	# input: serviceid
	# output: label
	my $sid = shift @_;
	CMS_DB::connect_to_database;
	my $result = CMS_SERVICE_DB::get_label_for_service($sid);
	CMS_DB::disconnect_from_database;
	return $$result{svclabel};
}

sub get_reseller_label_for_serviceid {
  # input: serviceid
  # output: label
  my $sid = shift @_;
  CMS_DB::connect_to_database;
  my $result = CMS_SERVICE_DB::get_reseller_label_for_service($sid);
  CMS_DB::disconnect_from_database;
  return $$result{svclabel};
}

sub get_serviceid_alt_for_serviceid {
        # input: serviceid
        # output: alternative service id
        my $sid = shift @_;
        CMS_DB::connect_to_database;
        my $result = CMS_SERVICE_DB::get_serviceid_alt_for_service($sid);
        CMS_DB::disconnect_from_database;
        return $$result{serviceid_alt};
}

sub is_wholesaler {
  # input: accno
  # output: 1 if is wholesaler, 0 otherwise
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::is_wholesaler(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_template_path {
  # input: accno
  # output: path to templates or 0 

  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_template_path(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_wholesaler_contactinfo {
  # input accno
  # output: contactinfo
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_wholesaler_contactinfo(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_wholesaler_email {
  # input: accno
  # output: path to templates or 0 

  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_wholesaler_email(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_wholesaler_logo {
	# input: accno
	# output: path_to_logo
	CMS_DB::connect_to_database;
	my $result = CMS_CUSTOMER_DB::get_wholesaler_logo(@_);
	CMS_DB::disconnect_from_database;
	return $result;

}

sub get_current_outages {
  # inputs: none
  # output: ref to 2D array
  # (ticketno, starttime, endtime, item, message)
  CMS_DB::connect_to_database;
  my $result = CMS_OUTAGE_DB::get_current_outages();
  $error = $CMS_OUTAGE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

# NOTICES

sub get_recent_notices {

  # inputs: (opt) number of notices to be returned (defaults to 10)
  # output: ref to 2D array
  CMS_DB::connect_to_database;
  my $number = shift @_ || 10; 
  # return $number of notices. Defaults to 10 if nothing passed.

  my $result = CMS_NOTICE_DB::get_recent_notices(10);
  $error = $CMS_NOTICE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}


# TROUBLE TICKET WRAPPERS;

sub add_trouble_ticket {
  CMS_DB::connect_to_database;
  my $result = CMS_TROUBLE_TICKET_DB::add_trouble_ticket(@_);
  $error = $CMS_TROUBLE_TICKET_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_current_trouble_tickets {
  CMS_DB::connect_to_database;
  my $result = CMS_TROUBLE_TICKET_DB::get_current_trouble_tickets(@_);
  $error = $CMS_TROUBLE_TICKET_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_trouble_ticket {
  CMS_DB::connect_to_database;
  my $result = CMS_TROUBLE_TICKET_DB::get_trouble_ticket(@_);
  $error = $CMS_TROUBLE_TICKET_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub cancel_ticket {
  CMS_DB::connect_to_database;
  # code 4 is code for cancel
  my $result = CMS_TROUBLE_TICKET_DB::update_status($_[0],4);
  $error = $CMS_TROUBLE_TICKET_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_current_trouble_tickets_for_account {
  CMS_DB::connect_to_database;
  my $result = CMS_TROUBLE_TICKET_DB::get_current_trouble_tickets_for_account(@_);
  $error = $CMS_TROUBLE_TICKET_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

# ROUTING QUEUE WRAPPERS

sub add_routing_request {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::add_routing_request(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub delete_routing_request {
  if($debug) {print IPT_RM "ENTERED CMS_OLSS delete_routing_request\n";} 
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::delete_routing_request(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub check_routing_exist {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::check_routing_exist(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#11 Feb added for IPT_ph5 by Chandini
sub check_routing_exist_allstatus {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::check_routing_exist_allstatus(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}


sub check_reach_ip {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::check_reach_ip(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub check_bad_host {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::check_bad_host(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_pending_routing_requests_for_account {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::get_pending_routing_requests_for_account(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_routing_request_by_opshandle{
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::get_routing_request_by_opshandle(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_as_by_opshandle{
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::get_as_by_opshandle(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_prefix_list_by_as{
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::get_prefix_list_by_as(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#Chandini_IPT_ph5
sub insert_route_bulkupload{
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::insert_route_bulkupload(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}
  
#Chandini_IPT_ph5
sub get_data_route_bulkupload{
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::get_data_route_bulkupload;
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#Chandini_IPT_ph5
sub update_status_temptable {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::update_status_temptable(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#Chandini_IPT_ph5
sub get_cmsdb_bgpdump {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::get_cmsdb_bgpdump(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#Chandini_IPT_ph5
sub get_wholesaler_template {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::get_wholesaler_template(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}


#Chandini_IPT_ph5
sub update_status_bulkupload {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::update_status_bulkupload;
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
} 

#Chandini_IPT_ph5
sub check_route_db {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::check_route_db(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#Chandini_IPT_ph5
sub check_irrd_object {
  CMS_DB::connect_to_database;
  my $result = CMS_ROUTING_QUEUE_DB::check_irrd_object(@_);
  $error = $CMS_ROUTING_QUEUE_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

  

# SECONDARY DNS WRAPPERS

sub add_secdns_entry { # (opshandle, domain_name, primary_ip)
  CMS_DB::connect_to_database;
  my $result = CMS_SECDNS_DB::add_secdns_entry(@_);
  $error = $CMS_SECDNS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub update_secdns_entry { # (opshandle, domain_name, primary_ip)
  CMS_DB::connect_to_database;
  my $result = CMS_SECDNS_DB::update_secdns_entry(@_);
  $error = $CMS_SECDNS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub delete_secdns_entry { # (opshandle, domain_name)
  CMS_DB::connect_to_database;
  my $result = CMS_SECDNS_DB::delete_secdns_entry(@_);
  $error = $CMS_SECDNS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_secdns_entries_for_account { # (opshandle)
  CMS_DB::connect_to_database;
  my $result = CMS_SECDNS_DB::get_secdns_entries_for_account(@_);
  $error = $CMS_SECDNS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_accno_for_dns_domain { # (domain_name)
  CMS_DB::connect_to_database;
  my $result = CMS_SECDNS_DB::get_accno_for_dns_domain(@_);
  $error = $CMS_SECDNS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result; # opshandle | under
}


# PRIMARY DNS WRAPPERS

sub get_pridns_type { # (search_by, value)
	CMS_DB::connect_to_database;
	my $result = CMS_PRIDNS_DB::get_pridns_type(@_);
	$error = $CMS_PRIDNS_DB::error;
	CMS_DB::disconnect_from_database;
	return $result;
}

sub get_accno_for_pridns_domain { # (domain_name)
  CMS_DB::connect_to_database;
  my $result = CMS_PRIDNS_DB::get_accno_for_dns_domain(@_);
  $error = $CMS_PRIDNS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result; # opshandle | under
}

sub get_pridns_status { # (search_by, value)
	CMS_DB::connect_to_database;
	my $result = CMS_PRIDNS_DB::get_pridns_status(@_);
	$error = $CMS_PRIDNS_DB::error;
	CMS_DB::disconnect_from_database;
	return $result;
}

sub add_pridns_entry { # (account_number, contact_email, domain_name, host,  record_type_id, record_parameter)
	CMS_DB::connect_to_database;
	my $result = CMS_PRIDNS_DB::add_pridns_entry(@_);
	$error = $CMS_PRIDNS_DB::error;
	CMS_DB::disconnect_from_database;
	return $result;
}
sub del_pridns_entry { # (account_number, db_row_id)
	CMS_DB::connect_to_database;
	my $result = CMS_PRIDNS_DB::del_pridns_entry(@_);
	$error = $CMS_PRIDNS_DB::error;
	CMS_DB::disconnect_from_database;
	return $result;
}
close (DEBUG);

sub update_pridns_entry { # (account_number,contact_email, domain_name, host,  record_type_id, record_parameter) 
  CMS_DB::connect_to_database;
  my $result = CMS_PRIDNS_DB::update_pridns_entry(@_);
  $error = $CMS_PRIDNS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_pridns_entries_for_account { # (account_number)
	CMS_DB::connect_to_database;
	my $result = CMS_PRIDNS_DB::get_pridns_entries_for_account(@_);
	$error = $CMS_PRIDNS_DB::error;
	CMS_DB::disconnect_from_database;
	return $result;
}

sub get_pridns_entries_for_dbrowid { # (db_row_id)
	CMS_DB::connect_to_database;
	my $result = CMS_PRIDNS_DB::get_pridns_entries_for_dbrowid(@_);
	$error = $CMS_PRIDNS_DB::error;
	CMS_DB::disconnect_from_database;
	return $result;
}

sub get_accno_for_pridns_dbrowid { # (db_row_id)
	CMS_DB::connect_to_database;
	my $result = CMS_PRIDNS_DB::get_accno_for_pridns_dbrowid(@_);
	$error = $CMS_PRIDNS_DB::error;
	CMS_DB::disconnect_from_database;
	return $result;
}

#DNS RESOLVER

sub get_dns_resolver_entry { # (account_number, contact_email, domain_name, record_parameter)
        CMS_DB::connect_to_database;
        my $result = CMS_DNSRESOLVER_DB::get_dns_resolver_entry(@_);
        $error = $CMS_DNSRESOLVER_DB::error;
        CMS_DB::disconnect_from_database;
        return $result;
}

sub view_dns_resolver { # (account_number)
        CMS_DB::connect_to_database;
        my $result = CMS_DNSRESOLVER_DB::view_dns_resolver(@_);
        $error = $CMS_DNSRESOLVER_DB::error;
        CMS_DB::disconnect_from_database;
        return $result;
}

sub get_acl_parm  {
        CMS_DB::connect_to_database;
        my $result = CMS_DNSRESOLVER_DB::get_acl_parm(@_);
        $error = $CMS_DNSRESOLVER_DB::error;
        CMS_DB::disconnect_from_database;
        return $result;
}

sub cancel_dns_resolver { # (account_number, contact_email, domain_name, record_parameter)
        CMS_DB::connect_to_database;
        my $result = CMS_DNSRESOLVER_DB::cancel_dns_resolver(@_);
        $error = $CMS_DNSRESOLVER_DB::error;
        CMS_DB::disconnect_from_database;
        return $result;
}

#REVERSE DELEGATION

sub req_reverse_mapping { # (account_number, contact_email, IP, PTR, IN, domain_name, record_parameter)
        CMS_DB::connect_to_database;
        my $result = CMS_REVERSEDEL_DB::req_reverse_mapping(@_);
        $error = $CMS_REVERSEDEL_DB::error;
        CMS_DB::disconnect_from_database;
        return $result;
}

sub view_dns_reverse { # (account_number)
        CMS_DB::connect_to_database;
        my $result = CMS_REVERSEDEL_DB::view_dns_reverse(@_);
        $error = $CMS_REVERSEDEL_DB::error;
        CMS_DB::disconnect_from_database;
        return $result;
}

sub get_acl_rev  {
        CMS_DB::connect_to_database;
        my $result = CMS_REVERSEDEL_DB::rev_acl_parm(@_);
        $error = $CMS_REVERSEDEL_DB::error;
        CMS_DB::disconnect_from_database;
        return $result;
}

sub reverse_cancel { # (account_number, contact_email, IP, PTR, IN, domain_name, record_parameter)
        CMS_DB::connect_to_database;
        my $result = CMS_REVERSEDEL_DB::reverse_cancel(@_);
        $error = $CMS_REVERSEDEL_DB::error;
        CMS_DB::disconnect_from_database;
        return $result;
}

# SECONDARY MX

sub add_secmx_entry { # (opshandle, domain_name)
  CMS_DB::connect_to_database;
  my $result = CMS_SECMX_DB::add_secmx_entry(@_);
  $error = $CMS_SECMX_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub delete_secmx_entry { # (opshandle, domain_name)
  CMS_DB::connect_to_database;
  my $result = CMS_SECMX_DB::delete_secmx_entry(@_);
  $error = $CMS_SECMX_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_secmx_entries_for_account { # (opshandle)
  CMS_DB::connect_to_database;
  my $result = CMS_SECMX_DB::get_secmx_entries_for_account(@_);
  $error = $CMS_SECMX_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_accno_for_mx_domain { # (domain)
  CMS_DB::connect_to_database;
  my $result = CMS_SECMX_DB::get_accno_for_mx_domain(@_);
  $error = $CMS_SECMX_DB::error;
  CMS_DB::disconnect_from_database;
  return $result; # opshandle | undef
}

# NEWS

sub add_news_entry { # (opshandle, incoming, outgoing, alias, delay)
  CMS_DB::connect_to_database;
  my $result = CMS_NEWS_DB::add_news_entry(@_);
  $error = $CMS_NEWS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub delete_news_entry { # (opshandle, incoming)
  CMS_DB::connect_to_database;
  my $result = CMS_NEWS_DB::delete_news_entry(@_);
  $error = $CMS_NEWS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_news_entries_for_account { # (opshandle)
  CMS_DB::connect_to_database;
  my $result = CMS_NEWS_DB::get_news_entries_for_account(@_);
  $error = $CMS_NEWS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result; # cust_incoming, cust_outgoing, alias, delay, request_time, status
}


sub get_accno_for_news { # (cust_incoming)
  CMS_DB::connect_to_database;
  my $result = CMS_NEWS_DB::get_accno_for_news(@_);
  $error = $CMS_NEWS_DB::error;
  CMS_DB::disconnect_from_database;
  return $result; # opshandle | undef
}


# LINKS DATABASE

sub get_maxbw_for_service {
  # inputs: serviceid
  # outputs: bandwidth 
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_maxbw_for_service(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_currbw_for_service {
  # inputs: serviceid
  # output: bandwidth | undef
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_currbw_for_service(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub set_currbw_for_service {
  # inputs: serviceid, bandwidth
  # output: 1 on success, 0 on failure
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::set_currbw_for_service(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_accountattribute_for_accno {
	# Inputs: accno
	# outputs: type of account
	CMS_DB::connect_to_database;
	my $result = CMS_CUSTOMER_DB::get_account_attribute(@_);
	$error = $CMS_CUSTOMER_DB::error;
	CMS_DB::disconnect_from_database;

	return $$result{accountattributeid};

}

sub get_mrwaccno_for_ro_acc {
	# Inputs: accno
	# outputs: mrw_accno

	CMS_DB::connect_to_database;
	my $result = CMS_CUSTOMER_DB::get_mrwaccno_for_ro(@_);
	CMS_DB::disconnect_from_database;

	return $$result{mrw_accno};
}

#Chandini_EPL(directly used this without any change for EPL; To call the function in DB file to get the list of EPL Services)
sub get_serviceid_for_accno {
  # inputs: accno and productcode (default to 'GIA' if no productcode supplied)
  # output: reference to 2D array containing service ids
  
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_serviceid_for_accno(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#Chandini_EPL(directly used this without any change for EPL; To call the function in DB file to get the list of Master Services)
########################### Chandini ####################3
sub get_master_services_for_accno {
  # inputs: accno
  # output: reference to 2D array containing service ids

  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_master_services_for_accno(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_master_serv_for_wholesaler_accno {
  # inputs: accno
  # output: reference to 2D array containing service ids

  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_master_serv_for_wholesaler_accno(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_services_of_master {
  # inputs: master service id , accno
  # output: reference to 2D array containing service ids

  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_services_of_master_for_accno(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}
####(used till here)

sub get_aggregate_services_for_accno {
  # inputs: master service id , accno
  # output: reference to 2D array containing service ids

  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_aggregate_services_for_accno(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}
################################# End -------- Chandini####################

#++EVPL

sub get_evpl_services {
  # inputs: accno 
  # output: reference to 2D array containing service ids
  
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_evpl_services(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_evplservices_wholesaler {
  # inputs: accno 
  # output: reference to 2D array containing service ids
  
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_evplservices_wholesaler(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#--EVPL
#Chandini_EPL
#Calls the function in DB file to get the Master Service ID associated with the EPL service
sub get_masterid {
  # inputs: serviceid
  # output: masterservice id 

  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_masterid(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#Chandini_EPL
#Calls the function in DB file to get the Master Service ID associated with the EPL service
sub get_masterid_wholesaler {
  # inputs: serviceid
  # output: masterservice id

  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_masterid_wholesaler(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}


#Chandini_EPL
#Calls the function in DB file to get the Master Service ID Label 
sub get_reseller_label_for_masterservice {
  # input: serviceid
  # output: label
  my $sid = shift @_;
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_reseller_label_for_masterservice($sid);
  CMS_DB::disconnect_from_database;
  return $$result{svclabel};
}

#Chandini_EPL
#Calls the function in DB file to get the Master Service ID Label
sub get_reseller_label_for_masterservice_wholesaler {
  # input: serviceid
  # output: label
  my $sid = shift @_;
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_reseller_label_for_masterservice_wholesaler($sid);
  CMS_DB::disconnect_from_database;
  return $$result{svclabel};
}


sub get_serviceid_for_accno_aggrmast {
  # inputs: accno and productcode (default to 'GIA' if no productcode supplied)
  # output: reference to 2D array containing service ids

  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_serviceid_for_accno_aggrmast(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#Chandini_EPL(directly used this without any change for EPL; To call the function in DB file to get the list of wholesale EPL Services)
sub get_serviceid_for_wholesaler_accno {
  # inputs: accno and productcode (default to 'GIA' if no productcode supplied)
  # output: reference to 2D array containing service ids
  
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_serviceid_for_wholesaler_accno(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_serviceid_for_wholesaler_accno_aggrmast {
  # inputs: accno and productcode (default to 'GIA' if no productcode supplied)
  # output: reference to 2D array containing service ids

  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_serviceid_for_wholesaler_accno_aggrmast(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

# CUSTOMER DETAILS

sub get_outage_contact_details {
  # input: accno
  # outputs: 2D array - firstname, surname, telephone, fax, email, mobile, 
  #                     subscriptionid 

  my $accno = shift @_;
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_contact_details 
      ($accno, 3);
  
  if ($error = $CMS_CUSTOMER_DB::error) {
    return $result;
  }

  my $result2 = CMS_CUSTOMER_DB::get_subsciption($accno);
  
  # NASTY HACK - RC wants subscriptionid passed as part of this sub
  # ASSUMPTION: only one outage contact per accno

  $$result[0][6] = $$result2[0][0];

  # END NASTY HACK

  $error = $CMS_CUSTOMER_DB::error;
  
  CMS_DB::disconnect_from_database;
  return $result;
  
}

sub update_outage_contact_details {
  # inputs: accno, firstname, surname, telephone, fax, email, mobile, 
  #         subscriptionid 
  # outputs: success/fail

  my $accno = shift @_;
  my $subscription = pop @_;

  CMS_DB::connect_to_database;
  unless (my $result = CMS_CUSTOMER_DB::update_contact_details 
	  ($accno, 3, @_)) {
    
    $error = $CMS_CUSTOMER_DB::error;
    return $result;
  }

  my $result = CMS_CUSTOMER_DB::update_contact_details($accno, $subscription);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;

}

sub get_tech_contact_details {
  # input: accno
  # outputs: 2D array - firstname, surname, telephone, fax, email, mobile 

  my $accno = shift @_;
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_contact_details 
    ($accno, 2);
  
  $error = $CMS_CUSTOMER_DB::error;
  return $result;
}

sub update_tech_contact_details {
  # inputs: accno, firstname, surname, telephone, fax, email, mobile 
  # outputs: success/fail

  my $accno = shift @_;
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::update_contact_details 
    ($accno, 2, @_);
  
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

#----------------------------------------------------------------------------

sub get_product_codes_for_accno {
  # inputs : account number
  # outputs: Reference to a hash of hash references with the following keys;
  #          'code' and 'name'
  # Eg:
  #     $return = CMS_OLSS::get_product_codes_for_accno($accno);
  #     if($return){
  #       for my $code (keys %$return){
  #	     print "CODE = ".$return->{$code}{code}.
  #                ",NAME = ".$return->{$code}{name}."\n";
  #	  }
  #     }
  #
  my $accno = shift @_;

  CMS_DB::connect_to_database;
  my $result = CMS_PC_DB::get_product_codes_for_accno($accno);
  $error = $CMS_PC_DB::error;
  CMS_DB::disconnect_from_database;

  return $result;
}

sub verify_accno_product {
  # inputs : account number, product code
  # outputs: 'TRUE' if the account has the specified product code, otherwise 'FALSE'

  my ($accno,$productcode) = @_;
  CMS_DB::connect_to_database;
  my $result = CMS_PC_DB::verify_accno_product($accno,$productcode);
  $error = $CMS_PC_DB::error;
  return $result;
}

sub access_through_EP {
  # inputs : account number
  # outputs: 'TRUE' if accno has Enterprise Portal Access, otherwise 'FALSE'
  
  my $accno = shift @_;
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::access_through_EP($accno);
  $error = $CMS_PC_DB::error;
  return $result;
}

sub get_productname_for_productcode {
  # inputs : product code
  # outputs: product name associated with the product code
  
  my $productcode = shift @_;
  CMS_DB::connect_to_database;
  my $result = CMS_PC_DB::get_productname($productcode);
  $error = $CMS_PC_DB::error;
  return $result;
}

# AGGREGATE SERVICE DATABASE
sub get_aggregate_by_acc {
  # inputs : accno
  # outputs: aggregate service list

  CMS_DB::connect_to_database;
  my $result = CMS_AGGREGATES_DB::get_aggregate_by_acc(@_);
  $error = $CMS_AGGREGATES_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

# AGGREGATE SERVICE DATABASE
sub get_aggregate_by_acc_aggrmast {
  # inputs : accno
  # outputs: aggregate service list

  CMS_DB::connect_to_database;
  my $result = CMS_AGGREGATES_DB::get_aggregate_by_acc_aggrmast(@_);
  $error = $CMS_AGGREGATES_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

# Get links DBL-RATEACL
sub get_pollingtype_sid {
  # inputs : serviceid
  # outputs : pollingtype

  my $sid = shift @_;
  CMS_DB::connect_to_database;
  my $result = CMS_SERVICE_DB::get_pollingtype($sid);
  $error = $CMS_SERVICE_DB::error;
  return $result;

}

# get classes
sub get_costype_sid {
	my $sid = shift @_;

	CMS_DB::connect_to_database; 
	my $result = CMS_SERVICE_DB::get_costype_sid($sid);
	$error = $CMS_SERVICE_DB::error;

	my $costype = $$result[0];

	return $costype;
}

#Chandini_EVPL
sub get_ser_capacity {
        my $sid = shift @_;

        CMS_DB::connect_to_database;
        my $result = CMS_SERVICE_DB::get_ser_capacity($sid);
        $error = $CMS_SERVICE_DB::error;

        my $ser_capacity = $$result[0];

        return $ser_capacity;
}


# OLSS Customer Login Account

sub is_olss_cust_acc {
  #Input: username
  #Output: 1 - Yes, 0 - No

  my $loginid = shift @_;
 
  CMS_DB::connect_to_database;
  my $result = CMS_OLSS_CUSTACC_DB::is_olss_cust_acc($loginid);
  $error = $CMS_OLSS_CUSTACC_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub set_login_success {
  #Input: username

  my $loginid = shift @_;
 
  CMS_DB::connect_to_database;
  my $result = CMS_OLSS_CUSTACC_DB::set_login_success($loginid);
  $error = $CMS_OLSS_CUSTACC_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub set_login_failure {
  #Input: username

  my $loginid = shift @_;
 
  CMS_DB::connect_to_database;
  my $result = CMS_OLSS_CUSTACC_DB::set_login_failure($loginid);
  $error = $CMS_OLSS_CUSTACC_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_login_failure {
  #Input: username

  my $loginid = shift @_;
 
  CMS_DB::connect_to_database;
  my $result = CMS_OLSS_CUSTACC_DB::get_login_failure($loginid);
  $error = $CMS_OLSS_CUSTACC_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub check_password_expired {
  #Input: username

  my $loginid = shift @_;
 
  CMS_DB::connect_to_database;
  my $result = CMS_OLSS_CUSTACC_DB::check_password_expired($loginid);
  $error = $CMS_OLSS_CUSTACC_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_olss_cust_access_accno {
  #Input: username

  my $loginid = shift @_;
 
  CMS_DB::connect_to_database;
  my $result = CMS_OLSS_CUSTACC_DB::get_olss_cust_access_accno($loginid);
  $error = $CMS_OLSS_CUSTACC_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;

}

sub change_olss_cust_password {
  # Input: username, old password, new password
  # Output: 1 on success
  CMS_DB::connect_to_database;
  my $result = CMS_OLSS_CUSTACC_DB::change_password(@_);
  $error = $CMS_OLSS_CUSTACC_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub verify_newpassword {
  #Input: username, new password
  #Output: 1 - Yes, 0 - No

  CMS_DB::connect_to_database;
  my $result = CMS_OLSS_CUSTACC_DB::verify_newpassword(@_);
  $error = $CMS_OLSS_CUSTACC_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}

sub get_productcode_agg_service {
  # inputs : Aggregate Service id
  # outputs: Product code for the service id 
  
  my $servid = shift @_;
  CMS_DB::connect_to_database;
  my $result = CMS_SERVICE_DB::get_productcode_agg_service($servid);
  $error = $CMS_PC_DB::error;
  return $result;
}

sub get_aggr_label_forservice {
        # input: serviceid
        # output: label
        my $sid = shift;
	my $accno = shift;
my $result;	
        CMS_DB::connect_to_database;
	my $iswholesale=CMS_CUSTOMER_DB::is_wholesaler($accno);
	if($iswholesale =~ /^1$/i) {
        	$result = CMS_AGGREGATES_DB::get_aggr_reseller_label_forservice($sid);
	}else{
		$result = CMS_AGGREGATES_DB::get_aggr_label_forservice($sid);
	}
        CMS_DB::disconnect_from_database;
        return $$result{svclabel};
}
#++mVPN
sub check_mVPN_service {
	my $servid = shift @_;
	my $mVPN_flag = shift @_;
	CMS_DB::connect_to_database;
	my $result = CMS_SERVICE_DB::check_mVPN_service($servid,$mVPN_flag);
	$error = $CMS_PC_DB::error;
	return $result;
}
#--mVPN

#Stavan-Aug10-IPTRANSIT_CHINA_METERING

# input accno
# output: list of customers for the wholesaler
sub get_customers_for_wholesaler {
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_customers_for_wholesaler(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}


# input accno
# output: list of ipc service for the retailer
sub get_ipc_services_for_retailer {
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_ipc_services_for_retailer(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}


# input accno
# output: list of asnos for the customer
sub get_asnos_for_chinametering_customers {
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_asnos_for_chinametering_customers(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;
}


# input accno
# output: list of asnos for the customer
sub get_asnos_for_ipc_service {
  CMS_DB::connect_to_database;
  my $result = CMS_CUSTOMER_DB::get_asnos_for_ipc_service(@_);
  $error = $CMS_CUSTOMER_DB::error;
  CMS_DB::disconnect_from_database;
  return $result;

}

#Chandini_IPT
#Function to call query in CMS_CUSTOMER_DB.pm file to get the costype of masterservice id
sub get_costype_masterserv {
	# input: serviceid
	# output: cos_type
	my $sid = shift @_;
	CMS_DB::connect_to_database;
	my $result = CMS_CUSTOMER_DB::get_costype_masterserv($sid);
	CMS_DB::disconnect_from_database;
	return $$result{cos_type};
}
1;
