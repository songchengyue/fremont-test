package com.telstra.netops.dns.server.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientReverseDnsRequest {
	
	private String customerId;

	private IPVersion ipVersion;

	private String contactEmailAddress;
	
	private String ipBlock;
	
	private String nameServer;
	
	private String serviceId ;

	public ClientReverseDnsRequest() {		
	}
	
	public ClientReverseDnsRequest(String customerId, IPVersion ipVersion,
			String contactEmailAddress, String ipBlock, String nameServer ,String serviceId) {
		super();
		this.customerId = customerId;
		this.ipVersion = ipVersion;
		this.contactEmailAddress = contactEmailAddress;
		this.ipBlock = ipBlock;
		this.nameServer = nameServer;
		this.serviceId = serviceId;
	}
	
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public IPVersion getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(IPVersion ipVersion) {
		this.ipVersion = ipVersion;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getIpBlock() {
		return ipBlock;
	}

	public void setIpBlock(String ipBlock) {
		this.ipBlock = ipBlock;
	}

	public String getNameServer() {
		return nameServer;
	}

	public void setNameServer(String nameServer) {
		this.nameServer = nameServer;
	}
	
	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}	
	
}
