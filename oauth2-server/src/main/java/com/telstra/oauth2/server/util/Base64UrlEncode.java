package com.telstra.oauth2.server.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.codec.binary.Base64;

public class Base64UrlEncode {

	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			System.out.println("ERROR: missing file name");
			System.exit(1);
		}
		
		byte[] bytes = Files.readAllBytes(Paths.get(args[0]));
       
		String encoded = Base64.encodeBase64String(bytes);
		System.out.print(encoded);
	}

}
