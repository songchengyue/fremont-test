/*
 * An XML document type.
 * Localname: getAllClassesHierarchyResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/classmodel
 * Java type: com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.classmodel.impl;
/**
 * A document containing one getAllClassesHierarchyResponse(@http://schemas.hp.com/ucmdb/1/params/classmodel) element.
 *
 * This is a complex type.
 */
public class GetAllClassesHierarchyResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponseDocument
{
    
    public GetAllClassesHierarchyResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETALLCLASSESHIERARCHYRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getAllClassesHierarchyResponse");
    
    
    /**
     * Gets the "getAllClassesHierarchyResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse getGetAllClassesHierarchyResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse)get_store().find_element_user(GETALLCLASSESHIERARCHYRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getAllClassesHierarchyResponse" element
     */
    public void setGetAllClassesHierarchyResponse(com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse getAllClassesHierarchyResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse)get_store().find_element_user(GETALLCLASSESHIERARCHYRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse)get_store().add_element_user(GETALLCLASSESHIERARCHYRESPONSE$0);
            }
            target.set(getAllClassesHierarchyResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getAllClassesHierarchyResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse addNewGetAllClassesHierarchyResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse)get_store().add_element_user(GETALLCLASSESHIERARCHYRESPONSE$0);
            return target;
        }
    }
}
