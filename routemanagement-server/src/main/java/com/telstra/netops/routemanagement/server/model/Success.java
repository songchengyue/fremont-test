package com.telstra.netops.routemanagement.server.model;

/**
 * Modal class for Success message
 *
 */
public class Success {

	private String message;

	public Success() {
	}

	public Success(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
