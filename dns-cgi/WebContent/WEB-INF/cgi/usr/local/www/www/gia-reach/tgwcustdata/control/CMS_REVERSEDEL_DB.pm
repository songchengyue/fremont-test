#(C) Telstra 2009
#
# Author:Karuna Ballal
# Date: 13 May 2009
# File: CMS_REVERSEDEL_DB.pm
#
# $Id: CMS_REVERSEDEL_DB.pm,v 1.6 2016/07/20 15:37:02 d804709 Exp $


package CMS_REVERSEDEL_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
use CMS_OLSS;


our $error;
our $rdns_value = 0;

#Logger Initialisation
my $log_conf =$OS_PARA::values{logfile}{value}; 
Log::Log4perl::init($log_conf);
our $logger = Log::Log4perl->get_logger();

# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;

# Select reverse dns record types
# Pre  : contact_email, host, record_parameter
# Prost: $result
sub req_reverse_mapping {
open (DEBUG, ">/tmp/rev_req_map");
print DEBUG "INSIDE req_reverse_mapping\n\n";
$logger->debug("INSIDE req_reverse_mapping");
CMS_DB::begin_transaction();
my $uid     = '';
my $accno   = '';
my $contact = '';
my $ptr     = '';
my $in      = '';
my $host    = '';
my $parm    = '';
my $parm_val = '';
my $ip_version = "" ;
my $uid     = $dbh_tr->quote(shift @_);
my $accno   = $dbh_tr->quote(shift @_);
my $contact = $dbh_tr->quote(shift @_);
#my $parm    = $dbh_tr->quote(shift @_);
my $ip_blk = $dbh_tr->quote(shift @_);
my $ip_version = $dbh_tr->quote(shift @_);
my $dom    = (shift @_);
my $service_id   = $dbh_tr->quote(shift @_);
$logger->debug(" Domain = $dom  OKK: $ip_blk ipversion: $ip_version");
$dom = $dom.".";
$dom = "\'$dom\'";
$logger->debug(" -------->>>>uid= $uid, accno =$accno,contact = $contact, param= $parm, Ip_blk= $ip_blk, Domain = $dom, serviceid= $service_id");
my $parm_val = $ip_blk;
	#my $new_ip_blk = join "/",$ip_blk,"32";
my $temp_sql = '';
my $success = 1;
my $ti = '';
my $match_ip = '';
my $r2 = '';
my $r3 = '';
my $r4 = '';
my $sth = '';
my $r1 = '';
my $r = '';
my $temp_x  = '';
my $div_val = '';
my $host = '';
#my $ptr = '';
my $ip_flag = 1;
 my @z1_octets = "";
my $z1_octet1 = "";
my $z1_octet2 = "";
my $z1_octet3 = "";
my $z1_octet4 = "";
	my $z1_octet5 = "";
	my $z1_octet6 = "";
	my $z1_octet7 = "";
	my $z1_octet8 = "";
my $z1_bin_1 = "";
my $z1_bin_2 = "";
my $z1_bin_3 = "";
my $z1_bin_4 = "";
	my $z1_bin_5 = "";
	my $z1_bin_6 = "";
	my $z1_bin_7 = "";
	my $z1_bin_8 = "";
my @z1_binary = "";
my $z1_binary_stat = "";
my @z1_binary_bit = "";
my @ip_octets = "";
my $ip_octets1 = "";
my $ip_octets2 = "";
my $ip_octets3 = "";
my $ip_octets4 = "";
	my $ip_octets5 = "";
	my $ip_octets6 = "";
	my $ip_octets7 = "";
	my $ip_octets8 = "";
my $ip_octet1 = "";
my $ip_octet2 = "";
my $ip_octet3 = "";
my $ip_octet4 = "";
	my $ip_octet5 = "";
	my $ip_octet6 = "";
	my $ip_octet7 = "";
	my $ip_octet8 = "";
my $ip_bin_1 = "";
my $ip_bin_2 = "";
my $ip_bin_3 = "";
my $ip_bin_4 = "";
	my $ip_bin_5 = "";
	my $ip_bin_6 = "";
	my $ip_bin_7 = "";
	my $ip_bin_8 = "";
my @ip_binary = "";
my $ip_binary_stat = "";
my @ip_binary_bit = "";
my $ip_length = "";
my $ip_binary_result = "";
my $z1_binary_result = "";
my $loop_count2 = 0;
my $ipprefix = "";
my $compare_flag=0;
my $last_oct_db = '';
my $ipnew_chk1='';

print DEBUG "accno:$accno\n";
$parm_val =~ s/\'//g;
	my $new_ip_blk ="";
	$logger->debug("FIRST IPVERSION first : $ip_version");
	if ($ip_version =~ '4' ){
		$new_ip_blk = join "/",$parm_val,"32";
		$logger->debug("FIRST IPVERSION 4 block : $new_ip_blk");
	}elsif ($ip_version =~ '6' ){
		$new_ip_blk = join "/",$parm_val,"64";
		$logger->debug("FIRST IPVERSION 6 block : $new_ip_blk");
	}
my @y = split(/\//,$new_ip_blk);
my $z1= shift(@y);
my $z2= shift(@y);
$logger->debug("FIRST IPVERSION CECHKL : $ip_version");
	if ($ip_version =~ '4'){
my @ip_octets = split(/\./,$z1);
	$logger->debug("i am herer : $ip_version, $ptr");
#code to split the stored IPs into octets
my $ip_octets1 = shift(@ip_octets);
my $ip_octets2 = shift(@ip_octets);
my $ip_octets3 = shift(@ip_octets);
$ip_octets4 = shift(@ip_octets);
$logger->debug("ipoctet: octet 4 : $ip_octets4 ");
my $last_oct = join "/",$ip_octets4,$z2;
print DEBUG "ip_octets4:$ip_octets4\n";
print DEBUG "last oct:$last_oct\n";
print DEBUG "parm_val:$parm_val\n";
my $ip_ptr = join ".",$ip_octets3,$ip_octets2,$ip_octets1;
$ptr = join ".",$ip_octets3,$ip_octets2,$ip_octets1,"in-addr.arpa";
#my $ptr = join ".", $ip_ptr,"in-addr.arpa";
$ptr = "\'$ptr\'";
#my $new_ip_octets4 = dbh quote( $ip_octets4);
$ti = rindex($z1,".");
$match_ip  = substr($z1,0,$ti);
	$logger->debug("PPPPTTTRRRR $ptr, T1: $ti");
print DEBUG "match ip:$match_ip\n";
	}
# start of ipv6 for Reverse Mapping 

	elsif ($ip_version =~ '6'){
		my $ipv6 = Net::IP::ip_expand_address($z1, 6);

my @ip_octets = split(/\:/,$ipv6);
my $ip_octets1 = shift(@ip_octets);
my $ip_octets2 = shift(@ip_octets);
my $ip_octets3 = shift(@ip_octets);
$ip_octets4 = shift(@ip_octets);
$logger->debug("ipv: $ipv6");
		#code to split the stored IPs into octets
		$ip_octets4 = substr($ipv6,15);
		
   		#foreach my $count (3 .. $#ip_octets){
			#$logger->debug("ipv octet loopppppp");
			#$ip_octets4 = join ":",$ip_octets[$count];					#---------check 
				
		#}
	$logger->debug("ipoctet: $ipv6 octet 4 : $ip_octets4 ");
		my $rev6=new Net::IP ($z1);
		my $ptrval=$rev6->reverse_ip(); 
        #my $ptrsub;		# ipv6.arpa 
	$logger->debug("reverse value: $ptrval  ");
	 #$ptr = join ":",$ip_octets3,$ip_octets2,$ip_octets1,".ip6.arpa";
	 
		$ptr= substr($ptrval,44,-1); 
		#$ptr=substr($ptrsub,1);
    		# remove trailing dots -first 10 nibbles for zone file creation 
		$ptr = "\'$ptr\'";
		#$ti = rindex($ipv6,":",[15])
		$logger->debug("Z1:$z1");
		$match_ip  = substr($z1,0,14);		# first 3 ipv6 segemnts for match ip ## not using ipv6 NET::IP expand function due to it is adding a zero 
		$logger->debug("PTR value: $ptr , Match $match_ip ");
	#$match_ip  = $z1;  ##hard coded 
        		
	}

$logger->debug("uid= $uid, accno =$accno,contact = $contact, param= $parm, Ip_blk= $ip_blk, Domain = $dom, serviceid=$service_id, ptr=$ptr, ipoct=$ip_octets4, ipversionnnn:$ip_version match ip : $match_ip");

# check for whether the accno exist or not
	#my $sth = $dbh_tr->prepare ("SELECT count(accno) FROM olss_cust_access_acc WHERE accno like $accno");
	#$sth->execute();
	#my $r = $sth->fetchall_arrayref;
	#removing this check for OLSS_Replacement
	my $r = 1;
	#if($r->[0]->[0] >= 1) {
	if($r >= 1) {
print DEBUG "inside DB mapping select loop\n";
	$logger->debug("inside DB mapping select loop");

	#check in ip_resource block
        my $sth3 = $dbh_tr->prepare ("SELECT count(ip_block) FROM ip_resource WHERE ip_block like '$match_ip%' AND customer_id like $accno");
        $sth3->execute();
        my $rt = $sth3->fetchall_arrayref;
        if ($rt->[0]->[0] >= 1){
	print DEBUG "inside DB mapping user count check\n";
	$logger->debug("inside DB mapping user count check");
	my $sth = $dbh_tr->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$parm_val'");
        $sth->execute();
        my $r2 = $sth->fetchrow_hashref;
	#  START_CHECK_3_OCTETS_SAME
        print DEBUG "DB CHECK_3_OCTETS_SAME\n";
        my $db_flag = 0;
        my $sth = $dbh_tr->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$match_ip%' and customer_id like $accno");
	$logger->debug("inside DB mapping SELECT IP BLOCK");
        $sth->execute();
        my $r5 = $sth->fetchall_arrayref;
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
        print DEBUG "DB 3 temp_x:$temp_x\n";
		$logger->debug("goigggg insode loop $#{$r5} ");
        for my $lp_count (0 .. $#{$r5}) {
        print DEBUG "lp_count:$lp_count\n";
        my $Ar_chk1 = $r5->[$lp_count]->[0];
        my $ip_block_chk1 = $Ar_chk1;
        print DEBUG "DB ip from db is $ip_block_chk1\n\n";
	$logger->debug("ip   version  :  $ip_version");	
			if ($ip_version =~ '4'){
        my @ip_chk1 = split(/\//,$ip_block_chk1);
        $ipnew_chk1  = shift(@ip_chk1);#ipnew_chk has ip
        my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
        my @ip_octets = split(/\./,$ipnew_chk1);

        #code to split the stored IPs into octets and changing them to binary
        $ip_octet1 = shift(@ip_octets);
        $ip_octet2 = shift(@ip_octets);
        $ip_octet3 = shift(@ip_octets);
        $ip_octet4 = shift(@ip_octets);
        $last_oct_db = join "/",$ip_octet4,$ipprefixip_chk1;
        print DEBUG "last oct from DB:$last_oct_db\n";

        $ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
        $ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
        $ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
        $ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

		#combining individual IPs into one variable/array
				@ip_binary = ();
				push(@ip_binary, $ip_bin_1);
				push(@ip_binary, $ip_bin_2);
				push(@ip_binary, $ip_bin_3);
				push(@ip_binary, $ip_bin_4);
				$ip_binary_stat = join("",@ip_binary);
				@ip_binary_bit = split(//,$ip_binary_stat);

		#code to split the entered IPs into octets and changing octets to binary.
		         $logger->debug("Z1v4:$z1");
				@z1_octets = split(/\./,$z1);
				$z1_octet1 = shift(@z1_octets);
				$z1_octet2 = shift(@z1_octets);
				$z1_octet3 = shift(@z1_octets);
				$z1_octet4 = shift(@z1_octets);

				$z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
				$z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
				$z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
				$z1_bin_4 = unpack("B8", pack('C',$z1_octet4));

        #combining the individual IPs into one variable/array
				@z1_binary = ();
				push(@z1_binary,$z1_bin_1);
				push(@z1_binary,$z1_bin_2);
				push(@z1_binary,$z1_bin_3);
				push(@z1_binary,$z1_bin_4);
				$z1_binary_stat = join("",@z1_binary);
				@z1_binary_bit = split(//,$z1_binary_stat);
				
				print DEBUG "input is @z1_binary_bit\n";
				$ipprefix = $ipprefixip_chk1;
			}
								
			if ($ip_version =~ '6'){
			$logger->debug("inside ipblock v6");
				my @ip_chk1 = split(/\//,$ip_block_chk1);
			    $ipnew_chk1= shift(@ip_chk1);#ipnew_chk has ip
				my $ipv6db = Net::IP::ip_expand_address($ipnew_chk1, 6);
				my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
				my @ip_octets = split(/\:/,$ipv6db);
				$logger->debug("inside ipblock ipnew_chk1  $ipnew_chk1  prefix check : $ipprefixip_chk1");
			#code to split the stored IPs into octets and changing them to binary
				$ip_octet1 = shift(@ip_octets);
				$ip_octet2= shift(@ip_octets);
				$ip_octet3 = shift(@ip_octets);   
				$ip_octet4 = shift(@ip_octets);      
				$ip_octet5 = shift(@ip_octets);      
				$ip_octet6 = shift(@ip_octets);    
				$ip_octet7 = shift(@ip_octets);     
				$ip_octet8 = shift(@ip_octets);
     
				$last_oct_db = join "/",$ip_octet8,$ipprefixip_chk1;
         #print $last_oct_db;
				print DEBUG "last oct from DB:$last_oct_db\n";
				$logger->debug("last oct from DB:$last_oct_db");
		#close STDOUT;

				$ip_bin_1 = unpack("B16", pack('H4',$ip_octet1));
				$ip_bin_2 = unpack("B16", pack('H4',$ip_octet2));
				$ip_bin_3 = unpack("B16", pack('H4',$ip_octet3));
				$ip_bin_4 = unpack("B16", pack('H4',$ip_octet4));
				$ip_bin_5 = unpack("B16", pack('H4',$ip_octet5));
				$ip_bin_6 = unpack("B16", pack('H4',$ip_octet6));
				$ip_bin_7 = unpack("B16", pack('H4',$ip_octet7));
				$ip_bin_8 = unpack("B16", pack('H4',$ip_octet8));
        #combining individual IPs into one variable/array
        @ip_binary = ();
        push(@ip_binary, $ip_bin_1);
        push(@ip_binary, $ip_bin_2);
        push(@ip_binary, $ip_bin_3);
        push(@ip_binary, $ip_bin_4);
				push(@ip_binary, $ip_bin_5);
				push(@ip_binary, $ip_bin_6);
				push(@ip_binary, $ip_bin_7);
				push(@ip_binary, $ip_bin_8);
        $ip_binary_stat = join(",",@ip_binary);
        @ip_binary_bit = split(//,$ip_binary_stat);
		$logger->debug("binary: $ip_binary_stat ");
my $ipv6 = Net::IP::ip_expand_address($z1, 6);
	#code to split the entered IPs into octets and changing octets to binary.
	$logger->debug("Z1v6:$ipv6");
        @z1_octets = split(/\:/,$ipv6);
        $z1_octet1 = shift(@z1_octets);
        $z1_octet2 = shift(@z1_octets);
        $z1_octet3 = shift(@z1_octets);
        $z1_octet4 = shift(@z1_octets);
				$z1_octet5 = shift(@z1_octets);
				$z1_octet6 = shift(@z1_octets);
				$z1_octet7 = shift(@z1_octets);
				$z1_octet8 = shift(@z1_octets);

        $z1_bin_1 = unpack("B16", pack('H4',$z1_octet1));
        $z1_bin_2 = unpack("B16", pack('H4',$z1_octet2));
        $z1_bin_3 = unpack("B16", pack('H4',$z1_octet3));
        $z1_bin_4 = unpack("B16", pack('H4',$z1_octet4));
				$z1_bin_5 = unpack("B16", pack('H4',$z1_octet5));
				$z1_bin_6 = unpack("B16", pack('H4',$z1_octet6));
				$z1_bin_7 = unpack("B16", pack('H4',$z1_octet7));
				$z1_bin_8 = unpack("B16", pack('H4',$z1_octet8));
        $logger->debug("Z1 bin 3: $z1_bin_3");
        #combining the individual IPs into one variable/array
        @z1_binary = ();
        push(@z1_binary,$z1_bin_1);
        push(@z1_binary,$z1_bin_2);
        push(@z1_binary,$z1_bin_3);
        push(@z1_binary,$z1_bin_4);
				push(@z1_binary,$z1_bin_5);
				push(@z1_binary,$z1_bin_6);
				push(@z1_binary,$z1_bin_7);
				push(@z1_binary,$z1_bin_8);
        $z1_binary_stat = join(",",@z1_binary);
		$logger->debug("STAT: $z1_binary_stat ");
        @z1_binary_bit = split(//,$z1_binary_stat);
        print DEBUG "input is @z1_binary_bit\n";
		
        $ipprefix = $ipprefixip_chk1;
		$logger->debug("ipprefix is :$ipprefix");
			}
		$logger->debug("ip checkkkkkkkkk: $ipnew_chk1");
        # check for whether the IP already exist in reverse
		$logger->debug("ip octects44: $ip_octets4");
        my $sth1 = $dbh_tr->prepare ("SELECT count(last_oct) FROM reverse WHERE last_oct like '$ip_octets4' and ptrrec like $ptr and customer_id like $accno and status = 1");
        $sth1->execute();
        $r3 = $sth1->fetchall_arrayref;
        $logger->debug("ip CHECk:$ipnew_chk1, match ip: $match_ip");
         # CHECK WHEN 3 Octets are SAME and IP PREFIX is diff
        if  ($ipnew_chk1 =~ /$match_ip/)  {        
        print DEBUG "DB entered match ip case\n";
        if($r3->[0]->[0] < 1) {
                print DEBUG "DB does not already exist in DB\n";
                print DEBUG "z2:$z2\tipprefix:$ipprefix\n";
					$logger->debug("z2 & ipprefix: $z2 ,  $ipprefix ");
						if($z2 > $ipprefix){
							$ip_length = $ipprefix;
						}
						else {
						$ip_length = $z2;
						}
                print DEBUG "ip length value:$ip_length\n";
                $compare_flag = 1;
		$db_flag = 0;
		        $logger->debug("binory  $z1_binary_bit[0] : $ip_binary_bit[0] ");
                for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
                print DEBUG "entered for condition\n";
                        if ($db_flag == 0){
                        print DEBUG "DB entered if compare flag condition\n";
                        $z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
                        $ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
						$logger->debug("Z1 binory loop count $loop_count2 : $z1_binary_result ,   $ip_binary_result ");
                                if ($z1_binary_result != $ip_binary_result){
                                print DEBUG "DB binary ADD false\n";
                                print DEBUG "DB db flag 1\n";
				$db_flag = 1;
                                }
                                elsif ($z1_binary_result == $ip_binary_result)
                                {
                                print DEBUG "DB equal to binary add\n";
                                print DEBUG "DB db flag 0\n";
								$logger->debug("if else BNLOCK");
                                $db_flag = 0;}
                        }
                }#end of both if loops and for loop
                if ($db_flag == 1 && $compare_flag ==1)
                {
                print DEBUG "DB compare flag 1\n";
				$logger->debug("db falg 1");
                $compare_flag = 1;}
                else
                {
                print DEBUG "DB compare flag 0\n";
				$logger->debug("db flag 2");
                $compare_flag = 0;
                last;
                }
        }#end of r3 if condition
        else {
                #CMS_DB::disconnect_from_transaction();
                return 1;
        }#end of else r3
        }#end of match ip if condn
        }#end of for loop condition

        if ($compare_flag == 1) {
		$logger->debug("indide compare");
        print DEBUG "throw error\n";
        #CMS_DB::disconnect_from_transaction();
        return "Reverse Mapping cannot be proceeded with the entered IP block. Please Enter A New IP";
        }#end of if
        elsif ($compare_flag == 0){
		$logger->debug("else if compare");
         print DEBUG "DB final check of compare flag 0\n";
        print DEBUG "DB z2:$z2 ipprefix:$ipprefix\n";
		$logger->debug("DB z2:$z2 ipprefix:$ipprefix");
        if ($z2 < $ipprefix) {                     #####################
        print DEBUG "DB z2:$z2 ipprefix:$ipprefix\n";
        print DEBUG "DB entered IP prefix less thn Db ip prefix\n";
        print DEBUG "parent sub range\n";
			$logger->debug("DB $$r2{ip_block}, praram:$parm_val");
        if($$r2{ip_block} =~ /$parm_val/) { 
	if($r3->[0]->[0] < 1) {
                my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1, last_oct = '$ip_octets4' WHERE ptrrec like $ptr and customer_id like $accno";
                print DEBUG "$t_sql\n";
                CMS_DB::begin_transaction();
                my $sth = $dbh_tr->prepare ("$t_sql");
                my $result = $sth->execute;
                print DEBUG "result:$result\n";
                $error = $dbh_tr->errstr;
                print DEBUG "error:$error\n";
                return $result;
                #CMS_DB::disconnect_from_transaction();
                }
        }#nd of r2 if condn
        }#end of z2< condn
        else {
		$logger->debug("inside DB else part");
        print DEBUG "success\n";
        print DEBUG "sub range true\n";
        #CMS_DB::disconnect_from_transaction();
        my $sthr = $dbh_tr->prepare ("Select count(last_oct) from reverse where status = 1 and ptrrec like $ptr and last_oct like '$ip_octets4' and customer_id like $accno ");
	print DEBUG "Select count(last_oct) from reverse where status = 1 and ptrrec like $ptr and last_oct like
 '$ip_octets4' and accno like $accno \n";
        print DEBUG "variables for select query:ptr:$ptr\tlastst_oct_db:$ip_octets4\taccno:$accno";
		
        $sthr->execute();
        my $r_1 = $sthr->fetchall_arrayref;
        print DEBUG "outside r_1 condn\n";
        print DEBUG "($r_1->[0]->[0])\n";
        if($r_1->[0]->[0] < 1) {
        print DEBUG "inside r_1 condition\n";
	$logger->debug("inside DB mapping SELECT IP BLOCK $dom");
$logger->debug("variables for select query:ptr:$ptr\tlastst_oct_db:$ip_octets4\taccno:$accno");
        if($r3->[0]->[0] < 1) {
##below query added by uday with serviceid
##############################added by uday for expanded ip6 insertion#########################################
if($ip_version=~'4'){$ip_blk=uc$ip_blk;
                my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time, service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'PTR','$ip_octets4',$dom,1,1,current_timestamp, $service_id,$ip_version,$ip_blk)";
                $sth = $dbh_tr->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                #CMS_DB::disconnect_from_transaction();
		print DEBUG "success:$success\n";
                return $error if ($error);
        #        return $result;
                return $success;
				}else{
				        my $ipblk=$ip_blk;
						$ipblk=~ s/\'//g;
				        my @z = split(/\//,$ipblk);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $ipblk ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
						
                        $logger->debug("ipv666: $ipv6");
						$ipv6=uc $ipv6;
	                    #my $ipblock6 = join "/",$ipv6,$prefix;
				 my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time, service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'PTR','$ip_octets4',$dom,1,1,current_timestamp, $service_id,$ip_version,'$ipv6')";
                $sth = $dbh_tr->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                #CMS_DB::disconnect_from_transaction();
		print DEBUG "success:$success\n";
                return $error if ($error);
        #        return $result;
                return $success;
				
				}
                }
        }else {print DEBUG "IP already exists in DB\n";
                #return "The IP Address has already been requested. Please Cancel it and try again.";
		my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr and customer_id like $accno and last_oct like '$ip_octets4' and status = 1";
		print DEBUG "$t_sql\n";
                CMS_DB::begin_transaction();
                my $sth = $dbh_tr->prepare ("$t_sql");
                my $result = $sth->execute;
                print DEBUG "result:$result\n";
                $error = $dbh_tr->errstr;
                print DEBUG "error:$error\n";
                return $result;
                }
	#return 1;
        }
        }#end of elsif

        # ENDED_CHECK_3_OCTETS_SAME

        print DEBUG "FINAL ERROR: REQUEST cannot be proceeded with the Entered IP. Please Enter A New IP.\n";
        return "Reverse Mapping Request cannot be proceeded with the entered IP Block. Please Enter A New IP Address";

        }else {
        print DEBUG "IP Address block does not exist in ip_resource\n";
        return "Reverse Mapping Request cannot be proceeded with the entered IP. Please Enter A New IP Address";
        }
} else {
print DEBUG "Account does not exist.\n";
return "Account does not exist";
}
close (DEBUG);
}



# Select reverse dns record types
# Pre  : contact_email, host, record_parameter
# Prost: $result
sub rev_delegation {
CMS_DB::begin_transaction();

open (DEBUG, ">/tmp/rev_del");
print DEBUG "INSIDE sub rev_delegation\n";

$logger->debug("in reverse del db ");

my $new_last_oct = '';
#my $uid     = '';
#my $accno   = '';
#my $contact = '';
my $ptr     = '';
my $in      = '';
my $host    = '';
my $parm    = '';
my $parm_val = '';
my $uid     = $dbh_tr->quote(shift @_);
my $accno   = $dbh_tr->quote(shift @_);
my $contact = $dbh_tr->quote(shift @_);
$logger->debug("useful parameters like>> $uid,$accno,$contact << ");
#my $parm    = $dbh_tr->quote(shift @_);
my $ip_blk = $dbh_tr->quote(shift @_);
my $ip_version = $dbh_tr->quote(shift @_);
my $dom    = (shift @_);
my $service_id   = $dbh_tr->quote(shift @_);

$logger->debug("uid= $uid, accno=$accno,contact=$contact,ip_version =$ip_version, ip_blk= $ip_blk, dom =$dom ,serviceId=$service_id");
$dom = $dom.".";
$dom = "\'$dom\'";
my $parm_val = $ip_blk;
my $temp_sql = '';
my $success = 1;
my $ti = '';
my $match_ip = '';
my $r2 = '';
my $r3 = '';
my $r4 = '';
my $sth = '';
my $r1 = '';
my $r = '';
my $temp_x  = '';
my $div_val = '';
my $host = '';
my $ip_flag = 1;
my @z1_octets = "";
my $z1_octet1 = "";
my $z1_octet2 = "";
my $z1_octet3 = "";
my $z1_octet4 = "";
	my $z1_octet5 = "";
	my $z1_octet6 = "";
	my $z1_octet7 = "";
	my $z1_octet8 = "";
my $z1_bin_1 = "";
my $z1_bin_2 = "";
my $z1_bin_3 = "";
my $z1_bin_4 = "";
my $z1_bin_5 = "";
	my $z1_bin_6 = "";
	my $z1_bin_7 = "";
	my $z1_bin_8 = "";
my @z1_binary = "";
my $z1_binary_stat = "";
my @z1_binary_bit = "";
my @ip_octets = "";
	my $ip_octets1 = "";
	my $ip_octets2 = "";
	my $ip_octets3 = "";
	my $ip_octets4 = "";
	my $ip_octets5 = "";
	my $ip_octets6 = "";
	my $ip_octets7 = "";
	my $ip_octets8 = "";
my $ip_octet1 = "";
my $ip_octet2 = "";
my $ip_octet3 = "";
my $ip_octet4 = "";
	my $ip_octet5 = "";
	my $ip_octet6 = "";
	my $ip_octet7 = "";
	my $ip_octet8 = "";
my $ip_bin_1 = "";
my $ip_bin_2 = "";
my $ip_bin_3 = "";
my $ip_bin_4 = "";
	my $ip_bin_5 = "";
	my $ip_bin_6 = "";
	my $ip_bin_7 = "";
	my $ip_bin_8 = "";
my @ip_binary = "";
my $ip_binary_stat = "";
my @ip_binary_bit = "";
my $ip_length = "";
my $ip_binary_result = "";
my $z1_binary_result = "";
my $loop_count2 = 0;
my $ipprefix = "";
my $compare_flag=0;
my $last_oct_db = '';
my $last_oct = '';
my $new_count_ip ='';
my $new_v_hop ='';
my $z1= '';
my $z2= '';
my $v_hop='';
my $ipnew_chk1='';
$logger->debug("octect created ");

	if($ip_version =~ '4'){
$parm_val =~ s/\'//g;
my @y = split(/\//,$parm_val);
$z1= shift(@y);
$z2= shift(@y);
@ip_octets = split(/\./,$z1);

#code to split the stored IPs into octets
my $ip_octet1 = shift(@ip_octets);
my $ip_octet2 = shift(@ip_octets);
my $ip_octet3 = shift(@ip_octets);
my $ip_octet4 = shift(@ip_octets);
$ip_octets4 = $ip_octet4;
$last_oct = join "/",$ip_octet4,$z2;
print DEBUG "last oct:$last_oct\n";
print DEBUG "parm_val:$parm_val\n";
my $ip_ptr = join ".",$ip_octet3,$ip_octet2,$ip_octet1;
$ptr = join ".",$ip_octet3,$ip_octet2,$ip_octet1,"in-addr.arpa";
$ptr = join ".", $ip_ptr,"in-addr.arpa";
$ptr = "\'$ptr\'";
$logger->debug("PTR4: $ptr");
$ti = rindex($z1,".");
$match_ip  = substr($z1,0,$ti);
print DEBUG "match ip:$match_ip\n";
	}

	if($ip_version  =~ 6){
		$parm_val =~ s/\'//g;
		my @y = split(/\//,$parm_val);
		$z1= shift(@y);
		$logger->debug("z1: $z1");
		$z2= shift(@y);
		
        my $ipv6 = Net::IP::ip_expand_address($z1, 6);
		$logger->debug("last oct is ipv6 :$ipv6 ");
		@ip_octets = split(/\:/,$ipv6);
		$logger->debug("ip octres :$#ip_octets");
		my $rev6=new Net::IP($z1);
	    my $ptrval=($rev6->reverse_ip());
		#code to split the stored IPs into octets
		my $ip_octet1 = shift(@ip_octets);
		my $ip_octet2 = shift(@ip_octets);
		my $ip_octet3 = shift(@ip_octets);
		my $ip_octet4 = shift(@ip_octets);
		my $ip_octet5 = shift(@ip_octets);
		my $ip_octet6 = shift(@ip_octets);
		my $ip_octet7= shift(@ip_octets);
		my $ip_octet8 = shift(@ip_octets);

           
		#foreach my $i(3 .. $#ip_octets){
			 #$ip_octets4 = join ":",$ip_octets[$i];

		#}
		#$logger->debug("last oct is 111111 :$ip_octets4 ");
		#$last_oct = join "/",$ip_octets4,$z2;
		   my $ip_octets45=substr($ipv6,15);
			$last_oct = join "/",$ip_octets45,$z2;
		
	$logger->debug("last oct is 222222 :$last_oct");
		print DEBUG "last oct:$last_oct\n";
		print DEBUG "parm_val:$parm_val\n";
		my $ip_ptr = join ":",$ip_octet3,$ip_octet2,$ip_octet1;
		 my $ptrsub = join ":",$ip_octet3,$ip_octet2,$ip_octet1,".ip6.arpa";
		#my $ptr = join ":", $ip_ptr,"in-addr.arpa";
		#$ptr = "\'$ptr\'";
        $ptr= substr($ptrval,44,-1); 
		$ptr = "\'$ptr\'";
		#$match_ip = join ":",$ip_octet1,$ip_octet2,$ip_octet3;
		$match_ip  = substr($z1,0,14);
		print DEBUG "match ip:$match_ip\n";
		$logger->debug("match_ip:$match_ip PTRRRR $ptr");
	}
$logger->debug("checking account number in olss_cust_access_acc table with acc no $accno ");

# check for whether the accno exist or not
#my $sth = $dbh_tr->prepare ("SELECT count(customer_id) FROM olss_cust_access_acc WHERE customer_id=$accno "); 
#$sth->execute();
$logger->debug("executed ");
#my $r = $sth->fetchall_arrayref;
my $r = 1;
$logger->debug("result is $r ");
$logger->debug("row o is >> $r ");

if($r >= 1) {
	$logger->debug("check in ip_resource block with match ip as $match_ip");
	#check in ip_resource block
	my $sth3 = $dbh_tr->prepare ("SELECT count(ip_block) FROM ip_resource WHERE ip_block like '$match_ip%' AND customer_id like $accno");
	$sth3->execute();
        my $rt = $sth3->fetchall_arrayref;
	$logger->debug("IP count result is $rt->[0]->[0]");

	if ($rt->[0]->[0] >= 1){
	my $sth = $dbh_tr->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$parm_val'");
        $sth->execute();
        my $r2 = $sth->fetchrow_hashref;
        # check for whether the IP already exist in reverse
	$logger->debug("last oct is $last_oct");
	$logger->debug("ptr >>>> is $ptr");
        my $sth1 = $dbh_tr->prepare ("SELECT count(last_oct) FROM reverse WHERE last_oct like '$last_oct' and ptrrec like $ptr and status =1 and customer_id like $accno and status = 1");
        $sth1->execute();
        $r3 = $sth1->fetchall_arrayref;


	$logger->debug("existing record in db $r3");
	#  START_CHECK_4_OCTETS_SAME
	my $sth = $dbh_tr->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$z1%' and customer_id like $accno");
	$sth->execute();
        my $r4 = $sth->fetchall_arrayref;
	$logger->debug("IPBLOCK in db $r4");

        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
	for my $lp_count (0 .. $#{$r4}) {
        my $Ar_chk = $r4->[$lp_count]->[0];
        my $ip_block_chk = $Ar_chk;
        print DEBUG "DB ip from db is $ip_block_chk\n";
	my @ip_chk = split(/\//,$ip_block_chk);
        my $ipnew_chk  = shift(@ip_chk);#ipnew_chk has ip 
        my $ipprefixip_chk = shift(@ip_chk);#has prefix
	print DEBUG "DB ipnew_chk: $ipnew_chk\t \t ipprefixip_chk: $ipprefixip_chk\n";
	$logger->debug("DB ipnew_chk: $ipnew_chk\t \t ipprefixip_chk: $ipprefixip_chk \t z1 :$z1  \t z2 :$z2 ");

	# CHECK WHEN 4 Octets are SAME and IP PREFIX is 24
	
 	if ( ($ipnew_chk =~ /$z1/) && ($ipprefixip_chk == 24) ) {	
		print DEBUG "ipprefixip_chk 24: $ipprefixip_chk\n";
		$logger->debug("ipprefixip_chk 24: $ipprefixip_chk ");
		if ($z2 > $ipprefixip_chk) {
			print DEBUG "entered z2> loop\n";
			$logger->debug("entered z2> loop ");
			my $ct_ip = ($z2 - $ipprefixip_chk);
			print DEBUG "ct_ip:$ct_ip\n";
			my $ct_ip1 = 0;
			my $new_z2 = 0;
			print DEBUG "z2:$z2\n";
			my $z3 = $z2;
				for ($ct_ip1 = 0; $ct_ip1 <= $ct_ip; $ct_ip1++){
					print DEBUG "ct_ip1:$ct_ip1\n";
					$logger->debug("ct_ip1:$ct_ip1 ");

					$new_z2 = ($z3 - 1);
					$z3 = ($z3 - 1);
					my $x2 = `$z2 - 1`;
					print DEBUG "x2 : $x2\n";
					print DEBUG "new_z2:$new_z2\n";
					print DEBUG "z3:$z3\n";
					my $new_last = join "/",$ip_octet4, $new_z2;
					print DEBUG "new_last:$new_last\n";
					$logger->debug("new_last:$new_last ");
					my $sth = $dbh_tr -> prepare("SELECT count(last_oct) from reverse where last_oct like '$new_last' and ptrrec like '$ptr' and customer_id like $accno and status = 1");
					$sth->execute();
					my $query = $sth->fetchall_arrayref;
					if ($query->[0]->[0]>=1){
						return "The entered IP Block has already been requested. Please Cancel it and try again\n";
						print DEBUG "already exists 24\n";
					}#end of if query loop
				}#end of for loop
			
			}#end of ip z2 if loop
				if ( ($ipnew_chk =~ /$z1/) && ($ipprefixip_chk == 48) ) {	
					print DEBUG "ipprefixip_chk 24: $ipprefixip_chk\n";
	
					if ($z2 > $ipprefixip_chk) {
						print DEBUG "entered z2> loop\n";
						my $ct_ip = ($z2 - $ipprefixip_chk);
						print DEBUG "ct_ip:$ct_ip\n";
						my $ct_ip1 = 0;
						my $new_z2 = 0;
						print DEBUG "z2:$z2\n";
						my $z3 = $z2;
						for ($ct_ip1 = 0; $ct_ip1 <= $ct_ip; $ct_ip1++){
							print DEBUG "ct_ip1:$ct_ip1\n";
							$new_z2 = ($z3 - 1);
							$z3 = ($z3 - 1);
							my $x2 = `$z2 - 1`;
							print DEBUG "x2 : $x2\n";
							print DEBUG "new_z2:$new_z2\n";
							print DEBUG "z3:$z3\n";
							my $new_last = join "/",$ip_octets4, $new_z2;
					
							print DEBUG "new_last:$new_last\n";
							my $sth = $dbh_tr -> prepare("SELECT count(last_oct) from reverse where last_oct like '$new_last' and ptrrec like $ptr and customer_id like $accno and status = 1");
							$sth->execute();
							my $query = $sth->fetchall_arrayref;
							if ($query->[0]->[0]>=1){
								return "The entered IP Block has already been requested. Please Cancel it and try again\n";
								print DEBUG "already exists 48\n";
							}#end of if query loop
						}#end of for loop
			
					}#end of ip z2 loop
				}	
		my $sth = $dbh_tr->prepare("SELECT count(*) from reverse where ptrrec like $ptr and customer_id like $accno");
		$sth -> execute();
		my $count_ptr = $sth->fetchall_arrayref;
		print DEBUG "count ptr:$count_ptr->[0]->[0]\n";
		$logger->debug("count ptr:$count_ptr->[0]->[0] ");

		if ($count_ptr->[0]->[0] > 0) {
			if($r3->[0]->[0] >= 1) {
			print DEBUG "already exists in the DB\n";
			$logger->debug("already exists in the DB $ip_version");
##below query added by uday with serviceid
######################added by uday for ipv6 uncompressed insertion#########################################
if($ip_version=~'4'){$ip_blk=uc$ip_blk;
			 my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time, service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp, $service_id,$ip_version,$ip_blk)";
                        $sth = $dbh_tr->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         #CMS_DB::disconnect_from_transaction();
                         return $error if ($error);
			 #return $result;
				$logger->debug("SUCCESSS : $success ");

			 return $success;
			 }else{
			 my @z = split(/\//,$ip_blk);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $ip_blk ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc$ipblock6;
						my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time, service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp, $service_id,$ip_version,$ipblock6)";
                        $sth = $dbh_tr->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         #CMS_DB::disconnect_from_transaction();
                         return $error if ($error);
			 #return $result;
				$logger->debug("SUCCESSS : $success ");

			 return $success;
			 
			 }
			}
		#	my $sth = $dbh_tr->prepare("UPDATE reverse set request_time = current_timestamp, contact = $contact, last_oct = $last_oct, fqdn = $dom where status = 1 and ptrrec like $ptr and accno like $accno and rec_type like 'NS' and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'");
			#my $sth = $dbh->prepare("UPDATE reverse set request_time = current_timestamp, contact = $contact, last_oct = $last_oct, fqdn = $dom where status = 1 and ptrrec like $ptr and customer_id like $accno and rec_type like 'NS'");
		 	#my $success = $sth->execute;
                        #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                        #my $result = ($success ? $dbh->commit : $dbh->rollback);
                        #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                        #CMS_DB::disconnect_from_transaction();
                        #return $error if ($error);
                        #return $result;
		} else {
			print DEBUG "4 oct before insert1\n";
##below query added by uday with serviceid
 ######################added by uday for ipv6 uncompressed insertion#########################################
            $logger->debug(" not in reverse db before insert $uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp,$service_id,$ip_version");
			if($ip_version=~'4'){$ip_blk=uc$ip_blk;
			 my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,Last_oct,fqdn,status,zone_status,request_time,service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp,$service_id,$ip_version,$ip_blk)";
                        $sth = $dbh_tr->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         #CMS_DB::disconnect_from_transaction();
                         return $error if ($error);
                         #return $result;
                         return $success;
						 }else{
						 my @z = split(/\//,$ip_blk);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $ip_blk ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc$ipblock6;
						 my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,Last_oct,fqdn,status,zone_status,request_time,service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp,$service_id,$ip_version,$ipblock6)";
                        $sth = $dbh_tr->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         #CMS_DB::disconnect_from_transaction();
                         return $error if ($error);
                         #return $result;
                         return $success;
						 
						 }
		}#end of if else count ptr condn
		if($r3->[0]->[0] < 1) {
			my $sth = $dbh_tr->prepare("SELECT count(last_oct) from reverse where ptrrec like $ptr and customer_id like $accno and rec_type like 'NS' and status = 1 and last_oct not like $last_oct  and fqdn not like 'ns03.telstraglobal.net.' and fqdn not like 'ns04.telstraglobal.net.' and fqdn not like 'unknown.telstraglobal.net.' and fqdn not like 'static.telstraglobal.net.'");
			print DEBUG "sth:$sth\n";
 		 	$sth->execute();
			my $result = $sth->fetchall_arrayref;
			if (($result->[0]->[0]>=1) && ($z2 == 24)){
				#print DEBUG "most recently added update\n";
				print DEBUG "UPDATE1error\n";
				#my $temp_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1,last_oct = $last_oct where ptrrec like $ptr and customer_id like $accno and rec_type like 'NS' and status = 1 and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.'  and fqdn not ilike 'static.telstraglobal.net.'";
				#$sth = $dbh->prepare ("$temp_sql");
                                #my $success = $sth->execute;
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #my $result = ($success ? $dbh->commit : $dbh->rollback);
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                #return $error if ($error);
                                #return $result;
				return "The sub range for the entered IP Block has already been requested.";
                                }else {
								
			if ($z2 != 24){
print DEBUG "inside z2 ne 24 loop\n";
my %eq_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);

my $eq_initial = 0;
my $eq_count_ip = (32 - $z2);
my $eq_count_ip1 = 0;
my $eq_latestz2 = $z2;
my $check_rs = 0;
for ($eq_count_ip1 = 0; $eq_count_ip1 <= $eq_count_ip; $eq_count_ip1++) {
print DEBUG "eq_count_ip:$eq_count_ip\teq_count_ip1:$eq_count_ip1\n";
my $v_next = $eq_latestz2 + 1;
my $eq_w_range = $eq_range{$v_next};
my $eq_hop = 256/$eq_w_range;
my $eq_i = 0;

	for ($eq_i = 1; $eq_i <= $eq_hop; $eq_i++) {		
		print DEBUG "inside hop loop for z2 ne 24 condition\n";
		my $sth = $dbh_tr-> prepare("SELECT count(last_oct) from reverse where last_oct like '$eq_initial/$v_next' and ptrrec like $ptr and customer_id like $accno and status = 1");
		$sth->execute();
		my $rs = $sth->fetchall_arrayref;
		print DEBUG "eq_initial/:$eq_initial\t$eq_w_range\tv_next:$v_next\trs:$rs->[0]->[0]\n";
			if ($rs->[0]->[0]>=1){
				print DEBUG "ip_octets4:$ip_octets4 before if check\n";
				if (($eq_initial >= $ip_octets4) && ($ip_octets4 <= ($eq_initial+$eq_w_range))){
				$check_rs = $check_rs + 1;
				#print DEBUG  "prefix not 24, update over sub range\n";
				print DEBUG "UPDATE2error\n";
                                #my $t_sql = "UPDATE reverse SET fqdn = $dom, last_oct = $last_oct, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and customer_id like $accno and last_oct like '$eq_initial/$v_next'";
                                #print DEBUG "$t_sql\n";
                                #CMS_DB::begin_transaction();
                                #my $sth = $dbh->prepare ("$t_sql");
                                #my $result = $sth->execute;
										$check_rs = $check_rs+1;
								#print DEBUG "result:$result\n";
                                #$error = $dbh->errstr;
                                #print DEBUG "error:$error\n";
								#print DEBUG "inside rs condn-check_rs:$check_rs\n";
									}#end of if ipoct check
								}#end of rs >= 1 
	
								$eq_initial = $eq_initial+$eq_w_range;		
		
							}#end for for i

							$eq_initial = 0;
							$eq_w_range = $eq_w_range+$eq_w_range;
							$eq_latestz2 = $eq_latestz2 +1;
							if ($eq_latestz2 == 32) { 
								last;
							}
							print DEBUG "eq_latestz2:$eq_latestz2\teq_w_range:$eq_w_range\n";

						}#end of for count ip
						if ($check_rs >= 1){
							return "The sub range for the entered IP Block has already been requested";
						}else {
							print DEBUG "4 oct before insert2 prefix not 24\n";
							 ######################added by uday for ipv6 uncompressed insertion#########################################
							if($ip_version=~'4'){$ip_blk=uc$ip_blk;
							my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time,service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp,$service_id,$ip_version,$ip_blk)";
							my $sth = $dbh_tr->prepare ("$temp_sql");
							$check_rs = $check_rs+1;
							my $success = $sth->execute;
							$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
							my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
							$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
							#CMS_DB::disconnect_from_transaction();
							return $error if ($error);
							#return $result;
							return $success;
							}else{
							my @z = split(/\//,$ip_blk);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $ip_blk ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc$ipblock6;
						my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time,service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp,$service_id,$ip_version,$ipblock6)";
							my $sth = $dbh_tr->prepare ("$temp_sql");
							$check_rs = $check_rs+1;
							my $success = $sth->execute;
							$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
							my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
							$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
							#CMS_DB::disconnect_from_transaction();
							return $error if ($error);
							#return $result;
							return $success;
							}
						}

					}#end of z2 if
				}#end of result else
				
				
				
							
				if (($result->[0]->[0]>=1) && ($z2 == 48)){
					#print DEBUG "most recently added update\n";
					print DEBUG "UPDATE1error\n";
					#my $temp_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1,last_oct = $last_oct where ptrrec like $ptr and accno like $accno and rec_type like 'NS' and status = 1 and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.'  and fqdn not ilike 'static.telstraglobal.net.'";
					#$sth = $dbh->prepare ("$temp_sql");
                                #my $success = $sth->execute;
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #my $result = ($success ? $dbh->commit : $dbh->rollback);
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                #return $error if ($error);
                                #return $result;
					return "The sub range for the entered IP Block has already been requested.";
				}
				else {
					if ($z2 != 48){
						print DEBUG "inside z2 ne 48 loop\n";
						my %eq_range = (48=>65536, 49=>32768, 50=>16384, 51=>8192, 52=>4096, 53=>2048, 54=>1024, 55=>512, 56=>256, 57=>128, 58=>64, 59=>32, 60=>16, 61=>8, 62=>4, 63=>2, 64=>1);

						my $eq_initial = 0;
						my $eq_count_ip = (64 - $z2);
						my $eq_count_ip1 = 0;
						my $eq_latestz2 = $z2;
						my $check_rs = 0;
						for ($eq_count_ip1 = 0; $eq_count_ip1 <= $eq_count_ip; $eq_count_ip1++) {
							print DEBUG "eq_count_ip:$eq_count_ip\teq_count_ip1:$eq_count_ip1\n";
							$logger->debug("eq w  eq z2:$eq_latestz2 ");
							my $v_next = $eq_latestz2 + 1;
								$logger->debug("eq w range v next $v_next ");
							my $eq_w_range = $eq_range{$v_next};
							$logger->debug("eq w range : $eq_w_range,  ");
							my $eq_hop = 65536/$eq_w_range;
							my $eq_i = 0;

							for ($eq_i = 1; $eq_i <= $eq_hop; $eq_i++) {		
								print DEBUG "inside hop loop for z2 ne 48 condition\n";
								my $sth = $dbh_tr-> prepare("SELECT count(last_oct) from reverse where last_oct like '$eq_initial/$v_next' and ptrrec like $ptr and customer_id like $accno and status = 1");
								$sth->execute();
								my $rs = $sth->fetchall_arrayref;
								print DEBUG "eq_initial/:$eq_initial\t$eq_w_range\tv_next:$v_next\trs:$rs->[0]->[0]\n";
								if ($rs->[0]->[0]>=1){
									print DEBUG "ip_octets4:$ip_octets4 before if check\n";
									if (($eq_initial >= $ip_octets4) && ($ip_octets4 <= ($eq_initial+$eq_w_range))){
										$check_rs = $check_rs + 1;
										#print DEBUG  "prefix not 48, update over sub range\n";
										print DEBUG "UPDATE2error\n";
                                #my $t_sql = "UPDATE reverse SET fqdn = $dom, last_oct = $last_oct, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$eq_initial/$v_next'";
                                #print DEBUG "$t_sql\n";
                                #CMS_DB::begin_transaction();
                                #my $sth = $dbh->prepare ("$t_sql");
                                #my $result = $sth->execute;
									$check_rs = $check_rs+1;
                                #print DEBUG "result:$result\n";
                                #$error = $dbh->errstr;
                                #print DEBUG "error:$error\n";
				#print DEBUG "inside rs condn-check_rs:$check_rs\n";
				}#end of if ipoct check
				}#end of rs >= 1 
	
		$eq_initial = $eq_initial+$eq_w_range;		
		
	}#end for for i

	$eq_initial = 0;
        $eq_w_range = $eq_w_range+$eq_w_range;
        $eq_latestz2 = $eq_latestz2 +1;
							if ($eq_latestz2 == 64) { 
								last;}
	print DEBUG "eq_latestz2:$eq_latestz2\teq_w_range:$eq_w_range\n";


}#end of for count ip
	if ($check_rs >= 1){
                    return "The sub range for the entered IP Block has already been requested";
		}else {
			print DEBUG "4 oct before insert2 prefix not 24\n";
##below query added by uday with serviceid
 ######################added by uday for ipv6 uncompressed insertion#########################################
if($ip_version=~'4'){$ip_blk=uc$ip_blk;
                                 my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time, service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp,$service_id,$ip_version,$ip_blk)";
                                $sth = $dbh_tr->prepare ("$temp_sql");
                                $check_rs = $check_rs+1;
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                                my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                return $error if ($error);
                                #return $result;
                                return $success;
								}else{
								my @z = split(/\//,$ip_blk);
                                my $ip= shift(@z);
	                            my $prefix= shift(@z);
                                $logger->debug("iparm value $ip_blk ");
                                my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                               $logger->debug("ipv666: $ipv6");
	                           my $ipblock6 = join "/",$ipv6,$prefix;
							   $ipblock6=uc$ipblock6;
								 my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time, service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp,$service_id,$ip_version,$ipblock6)";
                                $sth = $dbh_tr->prepare ("$temp_sql");
                                $check_rs = $check_rs+1;
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                                my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                return $error if ($error);
                                #return $result;
                                return $success;
								}
                                }

		}#end of z2 if
			}#end of result else
                }else {
			#print DEBUG "inside 24 ipp update cond\n";
			print DEBUG "UPDATE3error\n";
			#my $temp_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1,last_oct = $last_oct WHERE ptrrec like $ptr and customer_id like $accno and rec_type like 'NS' and status = 1 and last_oct like $last_oct";
                                #$sth = $dbh->prepare ("$temp_sql");
                                #my $success = $sth->execute;
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #my $result = ($success ? $dbh->commit : $dbh->rollback);
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                #return $error if ($error);
                                #return $result;
				return "The sub range for the entered IP Block has already been requested.";
                                }

			
	#CMS_DB::disconnect_from_transaction();
        #return 1;
        }

	# CHECK WHEN 4 Octets are SAME and IP PREFIX is different
elsif ( ($ipnew_chk =~ /$z1/) && ($ipprefixip_chk != $z2) ) {
	print DEBUG "DB 4 Octets are SAME and IP PREFIX is different\n";
	print DEBUG "DB ipprefixip_chk: $ipprefixip_chk\n";
	print DEBUG "DB before check z2:$z2\n";
	$logger->debug("EWLSE IFFF DB 4 Octets are SAME and IP PREFIX is different -- ipprefixip_chk: $ipprefixip_chk -- DB before check z2:$z2 ");

		if($z2 >= $ipprefixip_chk) {
		print DEBUG "DB after check z2:$z2\n";
		$new_last_oct = join "/",$ip_octet4,$ipprefixip_chk;
		my $sth5 = $dbh_tr->prepare("SELECT count(last_oct) from reverse where last_oct like '$new_last_oct' and customer_id like $accno");
	 	$sth5->execute();
       		my $rt1 = $sth5->fetchall_arrayref;
		$logger->debug("EWLSE IFFF count of last octect $rt1->[0]->[0]");
        	if ($rt1->[0]->[0] < 1) {
                	if($r3->[0]->[0] < 1) {
			# check for whether the IP exist or not in ip_resource table
                	my $sth = $dbh_tr->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$parm_val'");
                	$sth->execute();
                	my $r2 = $sth->fetchrow_hashref;
			$logger->debug("EWLSE IFFF ipblock is $$r2{ip_block} and parm val is $parm_val");

			if($$r2{ip_block} =~ /$parm_val/) {
				$logger->debug("EWLSE IFFF ipblock is $$r2{ip_block} and parm val is $parm_val uid: $uid, accno:  $accno,contact: $contact, ptr: $ptr,lastoct:$last_oct,dom:$dom");
          ##below query added by uday with serviceid
		   ######################added by uday for ipv6 uncompressed insertion#########################################
		  if($ip_version=~'4'){$ip_blk=uc$ip_blk;
				my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time, service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp, $service_id,$ip_version,$ip_blk)";
                        	$sth = $dbh_tr->prepare ("$temp_sql");
                        	my $success = $sth->execute;
                         	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         	my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                         	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         	#CMS_DB::disconnect_from_transaction();
                         	return $error if ($error);
                         	#return $result;
                         	return $success;
				}else{
				my @z = split(/\//,$ip_blk);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $ip_blk ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc$ipblock6;
						my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time, service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS',$last_oct,$dom,1,1,current_timestamp, $service_id,$ip_version,$ipblock6)";
                        	$sth = $dbh_tr->prepare ("$temp_sql");
                        	my $success = $sth->execute;
                         	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         	my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                         	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                         	#CMS_DB::disconnect_from_transaction();
                         	return $error if ($error);
                         	#return $result;
                         	return $success;
				
				}
				}
				print DEBUG "4 octets ip not matchin ip resource bloack\n";
	#			return "REVERSE DELEGATION REQUEST cannot be proceeded with the Entered IP Block. Please Enter A New IP block.\n";
 	               }
		}
		}# end of z2 if condn
	}#end of elsif condition
	}#end of loop

	# ENDED_CHECK_4_OCTETS_SAME

	#  START_CHECK_3_OCTETS_SAME
	print DEBUG "DB CHECK_3_OCTETS_SAME\n";
	$logger->debug("end of z2 if condn,end of elsif condition,end of loop matvh ip:$match_ip");
	my $db_flag = 0;
	my $sth = $dbh_tr->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$match_ip%' and customer_id like $accno");
	$sth->execute();
        my $r5 = $sth->fetchall_arrayref;
	$logger->debug("r5 result $r5");
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
	print DEBUG "DB 3 temp_x:$temp_x\n";
	$logger->debug("DB 3 temp_x:$temp_x  R5: $#{$r5}");

        for my $lp_count (0 .. $#{$r5}) {
	print DEBUG "lp_count:$lp_count\n";
	$logger->debug("lp_count:$lp_count");

        my $Ar_chk1 = $r5->[$lp_count]->[0];
        my $ip_block_chk1 = $Ar_chk1;
        print DEBUG "DB ip from db is $ip_block_chk1\n\n";
		$logger->debug("DB ip from db is $ip_block_chk1");

			if ($ip_version =~ '4'){
        my @ip_chk1 = split(/\//,$ip_block_chk1);
        $ipnew_chk1  = shift(@ip_chk1);#ipnew_chk has ip
	$logger->debug("DB ip new check1 from db is $ipnew_chk1");

	my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
	$logger->debug("DB ip prefix check1 from db is $ipprefixip_chk1");

        my @ip_octets = split(/\./,$ipnew_chk1);
	$logger->debug("AFTER OCTET");

	#code to split the stored IPs into octets and changing them to binary
	$ip_octet1 = shift(@ip_octets);
	$ip_octet2 = shift(@ip_octets);
	$ip_octet3 = shift(@ip_octets);
	$ip_octet4 = shift(@ip_octets);
	$last_oct_db = join "/",$ip_octet4,$ipprefixip_chk1;
	print DEBUG "last oct from DB:$last_oct_db\n";

	$ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
	$ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
	$ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
	$ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

	#combining individual IPs into one variable/array
	@ip_binary = ();
	push(@ip_binary, $ip_bin_1);
	push(@ip_binary, $ip_bin_2);
	push(@ip_binary, $ip_bin_3);
	push(@ip_binary, $ip_bin_4);
	$ip_binary_stat = join("",@ip_binary);
	@ip_binary_bit = split(//,$ip_binary_stat);
	#print"ip binary is @ip_binary_bit\n";

	#code to split the entered IPs into octets and changing octets to binary.
	@z1_octets = split(/\./,$z1);
        $z1_octet1 = shift(@z1_octets);
        $z1_octet2 = shift(@z1_octets);
        $z1_octet3 = shift(@z1_octets);
        $z1_octet4 = shift(@z1_octets);

        $z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
        $z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
        $z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
        $z1_bin_4 = unpack("B8", pack('C',$z1_octet4));
	
	#combining the individual IPs into one variable/array
        @z1_binary = ();
        push(@z1_binary,$z1_bin_1);
        push(@z1_binary,$z1_bin_2);
        push(@z1_binary,$z1_bin_3);
        push(@z1_binary,$z1_bin_4);
        $z1_binary_stat = join("",@z1_binary);
        @z1_binary_bit = split(//,$z1_binary_stat);
        print DEBUG "input is @z1_binary_bit\n";
        $ipprefix = $ipprefixip_chk1;
			}
			if ($ip_version =~ '6'){
				my @ip_chk1 = split(/\//,$ip_block_chk1);
			    $ipnew_chk1= shift(@ip_chk1);#ipnew_chk has ip
				$logger->debug("DB ip check $ipnew_chk1 ");
				my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
			    my $ipv6db = Net::IP::ip_expand_address($ipnew_chk1, 6);
				my @ip_octets = split(/\:/,$ipv6db);

			#code to split the stored IPs into octets and changing them to binary
				$ip_octet1 = shift(@ip_octets);
				$ip_octet2= shift(@ip_octets);
				$ip_octet3 = shift(@ip_octets);   
				$ip_octet4 = shift(@ip_octets);      
				$ip_octet5 = shift(@ip_octets);      
				$ip_octet6 = shift(@ip_octets);    
				$ip_octet7 = shift(@ip_octets);     
				$ip_octet8 = shift(@ip_octets);
     
				$last_oct_db = join "/",$ip_octet8,$ipprefixip_chk1;
         #print $last_oct_db;
				print DEBUG "last oct from DB:$last_oct_db\n";
		#close STDOUT;

				$ip_bin_1 = unpack("B16", pack('H4',$ip_octet1));
				$ip_bin_2 = unpack("B16", pack('H4',$ip_octet2));
				$ip_bin_3= unpack("B16", pack('H4',$ip_octet3));
				$ip_bin_4 = unpack("B16", pack('H4',$ip_octet4));
				$ip_bin_5 = unpack("B16", pack('H4',$ip_octet5));
				$ip_bin_6 = unpack("B16", pack('H4',$ip_octet6));
				$ip_bin_7 = unpack("B16", pack('H4',$ip_octet7));
				$ip_bin_8= unpack("B16", pack('H4',$ip_octet8));
        #combining individual IPs into one variable/array
				@ip_binary = ();
				push(@ip_binary, $ip_bin_1);
				push(@ip_binary, $ip_bin_2);
				push(@ip_binary, $ip_bin_3);
				push(@ip_binary, $ip_bin_4);
				push(@ip_binary, $ip_bin_5);
				push(@ip_binary, $ip_bin_6);
				push(@ip_binary, $ip_bin_7);
				push(@ip_binary, $ip_bin_8);
				$ip_binary_stat = join("",@ip_binary);
				@ip_binary_bit = split(//,$ip_binary_stat);
				my @y = split(/\//,$parm_val);
		$z1= shift(@y);
		$logger->debug("z1: $z1");
        my $ipv6 = Net::IP::ip_expand_address($z1, 6);
                   $logger->debug("IPbinory $ip_binary_stat ");
		#code to split the entered IPs into octets and changing octets to binary.
		$logger->debug("IPv6  $ipv6 ");
				@z1_octets = split(/\:/,$ipv6);
				$z1_octet1 = shift(@z1_octets);
				$z1_octet2 = shift(@z1_octets);
				$z1_octet3 = shift(@z1_octets);
				$z1_octet4 = shift(@z1_octets);
				$z1_octet5 = shift(@z1_octets);
				$z1_octet6 = shift(@z1_octets);
				$z1_octet7 = shift(@z1_octets);
				$z1_octet8 = shift(@z1_octets);

				$z1_bin_1 = unpack("B16", pack('H4',$z1_octet1));
				$z1_bin_2 = unpack("B16", pack('H4',$z1_octet2));
				$z1_bin_3 = unpack("B16", pack('H4',$z1_octet3));
				$z1_bin_4 = unpack("B16", pack('H4',$z1_octet4));
				$z1_bin_5 = unpack("B16", pack('H4',$z1_octet5));
				$z1_bin_6 = unpack("B16", pack('H4',$z1_octet6));
				$z1_bin_7 = unpack("B16", pack('H4',$z1_octet7));
				$z1_bin_8 = unpack("B16", pack('H4',$z1_octet8));

        #combining the individual IPs into one variable/array
				@z1_binary = ();
				push(@z1_binary,$z1_bin_1);
				push(@z1_binary,$z1_bin_2);
				push(@z1_binary,$z1_bin_3);
				push(@z1_binary,$z1_bin_4);
				push(@z1_binary,$z1_bin_5);
				push(@z1_binary,$z1_bin_6);
				push(@z1_binary,$z1_bin_7);
				push(@z1_binary,$z1_bin_8);
				$z1_binary_stat = join("",@z1_binary);
				@z1_binary_bit = split(//,$z1_binary_stat);
				print DEBUG "input is @z1_binary_bit\n";
				$logger->debug("Binary : $z1_binary_stat ");
				$ipprefix = $ipprefixip_chk1;
			}
		$logger->debug("DB ip from db is $ipnew_chk1 , match ip is $match_ip   ");

	 # CHECK WHEN 3 Octets are SAME and IP PREFIX is diff
        if  ($ipnew_chk1 =~ /$match_ip/)  {
        print DEBUG "DB entered match ip case\n";
		$logger->debug("if entered ");
        if($r3->[0]->[0] < 1) {
                print DEBUG "DB does not already exist in DB\n";
		$logger->debug("DB does not already exist in DB z2:$ipnew_chk1\tipprefix:$match_ip ");

                print DEBUG "z2:$z2\tipprefix:$ipprefix\n";
		$logger->debug("z2:$z2\tipprefix:$ipprefix ");
                if($z2 > $ipprefix){$ip_length = $ipprefix;}
                else {$ip_length = $z2;}
                print DEBUG "ip length value:$ip_length\n";
		$logger->debug("ip length value:$ip_length ");
                $compare_flag = 1;
		$db_flag = 0;
                for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
                print DEBUG "entered for condition\n";
			$logger->debug("lopp count : $loop_count2 ");

                        if ($db_flag == 0){
                        print DEBUG "DB entered if compare flag condition\n";
				$logger->debug("DB entered if compare flag condition ");

                        $z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
                        $ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
						    $logger->debug( " OUTSIDE if loop DB db flag 1   bin rsult = $z1_binary_result  ip_bin rsult = $ip_binary_result  \n");
                                if ($z1_binary_result != $ip_binary_result){
                                print DEBUG "DB binary ADD false\n";
                                print DEBUG "DB db flag 1\n";
				    $logger->debug( "DB db flag 1   bin rsult = $z1_binary_result  ip_bin rsult = $ip_binary_result  \n");
                                $db_flag = 1;
                                }
                                elsif ($z1_binary_result == $ip_binary_result)
                                {
                                print DEBUG "DB equal to binary add\n";
                                print DEBUG "DB db flag 0\n";
                                $db_flag = 0;}
                        }
                }#end of both if loops and for loop
				    $logger->debug( "FLAG checks db flag:$db_flag, compare flag:$compare_flag ");
		if ($db_flag == 1 && $compare_flag ==1)
                {
                print DEBUG "DB compare flag 1\n";
                $compare_flag = 1;}
        	else
                {
                print DEBUG "DB compare flag 0\n";
                $compare_flag = 0;
		last;
		}
        }#end of r3 if condition
	else {
		#CMS_DB::disconnect_from_transaction();
        	return 1;
	}#end of else r3
	}#end of match ip if condn
	}#end of for loop condition

 	if ($compare_flag == 1) {
	print DEBUG "throw error\n";
	#CMS_DB::disconnect_from_transaction();
	return "Reverse Delegation cannot be proceeded with the entered IP block. Please Enter A New IP";
	}#end of if
	elsif ($compare_flag == 0){
	 $logger->debug( " compare flag:$compare_flag ");
	 print DEBUG "DB final check of compare flag 0\n";
	print DEBUG "DB z2:$z2\tipprefix:$ipprefix\n";
	 $logger->debug( " Z2:$z2 , prefix:$ipprefix");
        if ($z2 < $ipprefix) {
	print DEBUG "DB z2:$z2 ipprefix:$ipprefix\n";
        print DEBUG "DB entered IP prefix less thn Db ip prefix\n";
        print DEBUG "parent sub range\n";
        if($$r2{ip_block} =~ /$parm_val/) {
                if($r3->[0]->[0] < 1) {
        	print DEBUG "UPDATE4error\n";
                #my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1, last_oct = $last_oct WHERE ptrrec like $ptr customer_id like $accno and rec_type like 'NS'";
	 	#print DEBUG "$t_sql\n";
                #CMS_DB::begin_transaction();
                #my $sth = $dbh->prepare ("$t_sql");
                #my $result = $sth->execute;
                #print DEBUG "result:$result\n";
                #$error = $dbh->errstr;
                #print DEBUG "error:$error\n";
                #return $result;
                #CMS_DB::disconnect_from_transaction();
		return "The sub range for the entered IP Block has already been requested.";
                }
	}#nd of r2 if condn
	}#end of z2< condn
	elsif ($z2 == $ipprefix) {
		print DEBUG "entered z2 = condn\n";
		$logger->debug("entered z2 = condn \n ");
			if($ip_version =~ '4'){
		my %eq_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2,32=>1);
			}
			elsif($ip_version =~ '6' ){
				my %eq_range_v6 = (48=>65536, 49=>32768, 50=>16384, 51=>8192, 52=>4096, 53=>2048, 54=>1024, 55=>512, 56=>256, 57=>128, 58=>64, 59=>32, 60=>16, 61=>8, 62=>4, 63=>2);
				$logger->debug("VERSION 6 ");
			}
my $eq_initial = 0;
			if($ip_version =~ '4' ){
my $eq_count_ip = (32 - $z2);
			}
			elsif($ip_version =~ '6'){
				my $eq_count_ip = (64 - $z2);
				$logger->debug("VERSION 6:  $eq_count_ip");
			}
my $eq_count_ip1 = 0;
my $eq_latestz2 = $z2;

for (my $eq_count_ip=0; $eq_count_ip1 <= $eq_count_ip; $eq_count_ip1++) {
$logger->debug("VERSION looop $ip_version");
my $eq_hop;
my $eq_w_range;
my $v_next; 
## below code to include 64 by uday
if($eq_latestz2 eq '64' or $eq_latestz2 eq '32' ){
$v_next = $eq_latestz2;
}else{
$v_next = $eq_latestz2 + 1;
}
	$logger->debug("$v_next");
	if($ip_version =~ '4'){
	my %eq_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);
$eq_w_range = $eq_range{$v_next};
$logger->debug("VERSION 4 $eq_w_range");
}else{
my %eq_range_v6 = (48=>65536, 49=>32768, 50=>16384, 51=>8192, 52=>4096, 53=>2048, 54=>1024, 55=>512, 56=>256, 57=>128, 58=>64, 59=>32, 60=>16, 61=>8, 62=>4, 63=>2, 64=>1);
$eq_w_range = $eq_range_v6{$v_next};
$logger->debug("VERSION 6 $eq_w_range");
}
$logger->debug("$eq_w_range");
				if($ip_version =~ '4' ){
 $eq_hop = 256/$eq_w_range;
$logger->debug("VERSION 4 $ip_version");
				}
		
				elsif($ip_version =~ '6' ){
				$logger->debug("gukjhkli");
					$logger->debug("$eq_w_range");
					$eq_hop = 65536/$eq_w_range;
					$logger->debug("version first 6");
				}
my $eq_i = 0;

	for ($eq_i = 1; $eq_i <= $eq_hop; $eq_i++) {		
		my $sth = $dbh_tr-> prepare("SELECT count(last_oct) from reverse where last_oct like '$eq_initial/$v_next' and ptrrec like $ptr and customer_id like $accno and status = 1");
		$sth->execute();
		my $rs = $sth->fetchall_arrayref;
$logger->debug("entered loop ");
			if ($rs->[0]->[0]>=1){
			if (($eq_initial >= $ip_octets4) && ($ip_octets4 <= ($eq_initial+$eq_w_range))){
					#print DEBUG "update when z2 = ipprefix\n";	
						print DEBUG "UPDATE5error\n";	
						#my $t_sql = "UPDATE reverse SET fqdn = $dom, last_oct = $last_oct, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$eq_initial/$v_next'";
                				#print DEBUG "$t_sql\n";
                				#CMS_DB::begin_transaction();
                				#my $sth = $dbh->prepare ("$t_sql");
               	 				#my $result = $sth->execute;
                				#3print DEBUG "result:$result\n";
                				#$error = $dbh->errstr;
                				#print DEBUG "error:$error\n";
                				#return $result;
						return "The sub range for the entered IP Block has already been requested.";
				}#end of if ip oct condition	
			}else {
			    $logger->debug("if else enetered ");
				print DEBUG "insert when z2 = ipprefix\n";
##below query added by uday with serviceid
 ######################added by uday for ipv6 uncompressed insertion#########################################
if($ip_version=~'4'){$ip_blk=uc$ip_blk;
				 my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time,service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp,$service_id,$ip_version,$ip_blk)";
                                $sth = $dbh_tr->prepare ("$temp_sql");
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                                my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                return $error if ($error);
                                #return $result;
                                return $success;
                                }else{
								my @z = split(/\//,$ip_blk);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $ip_blk ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc$ipblock6;
						 my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time,service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp,$service_id,$ip_version,$ipblock6)";
                                $sth = $dbh_tr->prepare ("$temp_sql");
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                                my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                return $error if ($error);
                                #return $result;
                                return $success;
								}
								}

				
	
		$eq_initial = $eq_initial+$eq_w_range;		
		
	}#end for for i

	$eq_latestz2 = $eq_latestz2 -1;
	$eq_initial = 0;
        $eq_w_range = $eq_w_range+$eq_w_range;
        $eq_latestz2 = $eq_latestz2 +1;
					if ($eq_latestz2 == 32 or $eq_latestz2 == 64 ) { 
						last;
						}

}#end of for count ip
	}#end of z2 eq condition
	elsif ($z2 > $ipprefix) {
	  $logger->debug("in side id PTRRRR $ptr last oct $last_oct_db");
        print DEBUG "success\n";
	print DEBUG "sub range true\n";
        #CMS_DB::disconnect_from_transaction();
	my $sthr = $dbh_tr->prepare ("Select count(last_oct) from reverse where status = 1 and ptrrec like $ptr and last_oct like '$last_oct_db' and customer_id like $accno ");
	print DEBUG "variables for select query:ptr:$ptr\tlastst_oct_db:$last_oct_db\taccno:$accno";
	$sthr->execute();
	my $r_1 = $sthr->fetchall_arrayref;
	print DEBUG "outside r_1 condn\n";
	print DEBUG "($r_1->[0]->[0])\n";
	if($r_1->[0]->[0] < 1) {
	print DEBUG "inside r_1 condition\n";
 	if($r3->[0]->[0] < 1) {
	my %v_range;
	print DEBUG "entered r3 loop\n";
#my %v_range = (25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2);
					if ($ip_version =~ '4' ){
%v_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2,32=>1);
					}
					elsif($ip_version =~ '6' ){
					      $logger->debug(" version 6 vrange ");
						 %v_range = (48=>65536, 49=>32768, 50=>16384, 51=>8192, 52=>4096, 53=>2048, 54=>1024, 55=>512, 56=>256, 57=>128, 58=>64, 59=>32, 60=>16, 61=>8, 62=>4, 63=>2,64=>1);
					}
					my $v_initial = 0;

my $count_ip = ($z2 - $ipprefix);
print DEBUG "ipprefix:$ipprefix\n";
my $count_ip1 = 0;
my $latestz2 = $z2;

for ($count_ip1 = 1; $count_ip1 <= $count_ip; $count_ip1++) {
my $v_previous;
if($latestz2 eq '48'){
 $v_previous = $latestz2;
}else {
 $v_previous = $latestz2 -1;
}
  $logger->debug(" v range:$v_range{$v_previous}");
my $w_range = $v_range{$v_previous};
my $v_hop;
print DEBUG "w_range:$w_range\n";
						if ($ip_version =~ '4' ){
 $v_hop = (256/($w_range));
						}
						elsif($ip_version =~ '6' ){
						 $logger->debug(" v hop verson 6: $w_range ");
							 $v_hop = (65536/($w_range));
						}
print DEBUG "v_previous:$v_previous\tw_range:$w_range\tv_hop:$v_hop\n";
$logger->debug(" entered v_previous:$v_previous\tw_range:$w_range\tv_hop:$v_hop ");
my $i = 0;
print DEBUG "beforei loop\n";
print DEBUG "count_ip:$count_ip\tcount_ip1:$count_ip1\n";
	for ($i = 1; $i <= $v_hop; $i++) {		
		print DEBUG "inside i-v_hop loop\n";
		my $sth = $dbh_tr-> prepare("SELECT count(last_oct) from reverse where last_oct like '$v_initial/$v_previous' and ptrrec like $ptr and customer_id like $accno and status = 1"); 

		$sth->execute();
		print DEBUG "i inside :$i\n";
		my $rs = $sth->fetchall_arrayref;
			print DEBUG  "SELECT count(last_oct) from reverse where last_oct like '$v_initial/$v_previous' and ptrrec like $ptr and customer_id like $accno and status = 1\n";
			print DEBUG "rs:$rs->[0]->[0]\n";
			print DEBUG "ip_octets4:$ip_octets4 before if check\n";
			if ($rs->[0]->[0]>=1){
				print DEBUG "v_initial:$v_initial\tw_range:$w_range\n";
				if (($v_initial <= $ip_octets4) && ($ip_octets4 < ($v_initial+$w_range))){
						print DEBUG "just before throwin error..newly removed compare condition\n";
						return "The IP Block that has been requested is already present. Please Cancel it and try again."
				}#end of ipoct if
			}#end of if rs
	
		$v_initial = $v_initial+$w_range;		
		print DEBUG "after rs loop:v_initial:$v_initial\tw_range:$w_range\n";
		print DEBUG "for i loop ends\n";	
	}#end for for i
	$v_initial = 0;
	
	if($latestz2 eq '48'){
 $latestz2 = $latestz2;
}else {
 $latestz2 = $latestz2 -1;
}
	
	$w_range = $w_range+$w_range;

}#end of for count

#to check for parent IP
my %new_v_range ;
					if ($ip_version =~ '4' ){
 %new_v_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);
					}
					elsif($ip_version =~ '6' ){
						 %new_v_range = (48=>65536, 49=>32768, 50=>16384, 51=>8192, 52=>4096, 53=>2048, 54=>1024, 55=>512, 56=>256, 57=>128, 58=>64, 59=>32, 60=>16, 61=>8, 62=>4, 63=>2 ,64=>1);
					}
my $check_rs = 0;
my $new_v_initial = 0;
my $new_count_ip;
					if ($ip_version =~ '4' ){
 $new_count_ip = (32 - $z2);
					}
					elsif($ip_version =~ '6' ){
						 $new_count_ip = (64 - $z2);
					}
my $new_count_ip1 = 0;
my $new_latestz2 = $z2;
print DEBUG "new_latestz2:$new_latestz2\n";
$logger->debug("new_latestz2:$new_latestz2 ");



for ($new_count_ip1 = 0; $new_count_ip1 <= $new_count_ip; $new_count_ip1++) {
print DEBUG "new_count_ip1:$new_count_ip1\tnew_count_ip:$new_count_ip\n";
#my %new_v_range ='';
my $v_next;
if($new_latestz2 eq '64' or $new_latestz2 eq '32' or $new_latestz2 eq '24' or $new_latestz2 eq '48' ){
  $v_next = $new_latestz2;
  $logger->debug("vnext inside....1:$v_next ");
}else{
 $v_next = $new_latestz2 + 1;
  $logger->debug("vnext inside....2:$v_next ");
 }
 
 ############################################
 my %new_range;
  my $new_w_range;
   $logger->debug("shruti $v_next");
					if ($ip_version =~ '4' ){
 %new_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);
  $new_w_range = $new_range{$v_next};
					}
					elsif($ip_version =~ '6' ){
					 $logger->debug("VERsION 6 $v_next");
						 %new_range = (48=>65536, 49=>32768, 50=>16384, 51=>8192, 52=>4096, 53=>2048, 54=>1024, 55=>512, 56=>256, 57=>128, 58=>64, 59=>32, 60=>16, 61=>8, 62=>4, 63=>2 , 64=>1);
						  $new_w_range = $new_range{$v_next};
						  $logger->debug("new w range outside6666:$new_w_range,$v_next ");
					}
##############################################					

$logger->debug("new w range outside:$new_w_range ");
my $new_v_hop='';
						if ($ip_version =~ '4' ){
 $new_v_hop = 256/$new_w_range;
						}
						elsif($ip_version =~ '6' ){
						 $new_v_hop = 65536/$new_w_range;
						}
my $new_i = 0;
print DEBUG "v_next:$v_next\tnew_w_range:$new_w_range\tnew_v_hop:$new_v_hop\n";
#############################################for loop v6 & v4differentiated################################################	
       if ($ip_version =~ '4'){
	for ($new_i = 1; $new_i <= $new_v_hop; $new_i++) {		
		print DEBUG "new_i:$new_i\tnew_v_hop:$new_v_hop\n";
		print DEBUG "before select count:new_v_initial:$new_v_initial\tv_next:$v_next\n";
		
		print DEBUG "before select count:ptr:$ptr\taccno:$accno\n";
		my $t_r = "SELECT count(last_oct) from reverse where last_oct like '$new_v_initial/$v_next' and ptrrec like $ptr and customer_id like $accno and status = 1";
		#my $sth_r = $dbh-> prepare("SELECT count(*) from reverse where last_oct like $new_v_initial\/$v_next and ptrrec like $ptr and accno like $accno and status = 1"); 
		print DEBUG "t_r:$t_r\n";
		my $sth_r = $dbh_tr->prepare("$t_r");
		$sth_r->execute();
		my $rs_r = $sth_r->fetchall_arrayref;
				print DEBUG "rs:$rs_r->[0]->[0]\n";
			print DEBUG "ip_octets4:$ip_octets4 before if check\n";
			if ($rs_r->[0]->[0]>=1){
				print DEBUG "inside rs 1 condition rs:$rs_r->[0]->[0]\tip_octets4:$ip_octets4\n";
				if (($new_v_initial >= $ip_octets4) && ($ip_octets4 <= ($new_v_initial+$new_w_range))){
						print DEUBG "UPDATE6error\n";
						#my $t_sql = "UPDATE reverse SET last_oct = $last_oct, fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$new_v_initial/$v_next' and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'";
						#my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$new_v_initial/$v_next'";
						$check_rs = $check_rs+1;
						#print DEBUG "check_rs:$check_rs\n";
                				#print DEBUG "t sql:$t_sql\n";
                				#3CMS_DB::begin_transaction();
                				#my $sth = $dbh->prepare ("$t_sql");
               	 				#my $result = $sth->execute;
                				#print DEBUG "result:$result\n";
                				#$error = $dbh->errstr;
                				#print DEBUG "error:$error\n";
                		#		return $result;

				}#end of if oct 
			}#end of if rs
	
		$new_v_initial = $new_v_initial+$new_w_range;		

	}#end for for i for V4 
    }#V4 end
	else{
	for ($new_i = 1; $new_i <= 2; $new_i++) {		
		print DEBUG "new_i:$new_i\tnew_v_hop:$new_v_hop\n";
		print DEBUG "before select count:new_v_initial:$new_v_initial\tv_next:$v_next\n";
		
		print DEBUG "before select count:ptr:$ptr\taccno:$accno\n";
		my $t_r = "SELECT count(last_oct) from reverse where last_oct like '$new_v_initial/$v_next' and ptrrec like $ptr and customer_id like $accno and status = 1";
		#my $sth_r = $dbh-> prepare("SELECT count(*) from reverse where last_oct like $new_v_initial\/$v_next and ptrrec like $ptr and accno like $accno and status = 1"); 
		print DEBUG "t_r:$t_r\n";
		my $sth_r = $dbh_tr->prepare("$t_r");
		$sth_r->execute();
		my $rs_r = $sth_r->fetchall_arrayref;
				print DEBUG "rs:$rs_r->[0]->[0]\n";
			print DEBUG "ip_octets4:$ip_octets4 before if check\n";
			if ($rs_r->[0]->[0]>=1){
				print DEBUG "inside rs 1 condition rs:$rs_r->[0]->[0]\tip_octets4:$ip_octets4\n";
				if (($new_v_initial >= $ip_octets4) && ($ip_octets4 <= ($new_v_initial+$new_w_range))){
						print DEUBG "UPDATE6error\n";
						#my $t_sql = "UPDATE reverse SET last_oct = $last_oct, fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$new_v_initial/$v_next' and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'";
						#my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$new_v_initial/$v_next'";
						$check_rs = $check_rs+1;
						#print DEBUG "check_rs:$check_rs\n";
                				#print DEBUG "t sql:$t_sql\n";
                				#3CMS_DB::begin_transaction();
                				#my $sth = $dbh->prepare ("$t_sql");
               	 				#my $result = $sth->execute;
                				#print DEBUG "result:$result\n";
                				#$error = $dbh->errstr;
                				#print DEBUG "error:$error\n";
                		#		return $result;

				}#end of if oct 
			}#end of if rs
	
		$new_v_initial = $new_v_initial+$new_w_range;		

	}### for loop end V6
	}####V6 end
#############################################for loop v6 & v4differentiated################################################	
	$new_v_initial = 0;
	$new_w_range = $new_w_range+$new_w_range;
	$new_latestz2 = $new_latestz2 +1;
						if ($new_latestz2 == 32 or $new_latestz2 == 64 ) {
							last;}

}#end of for count ip
if ($check_rs >= 1){ #return 1;
	return "The sub range for the entered IP Block has already been requested.";
	 }

		print DEBUG "before insert\n";
	$logger->debug("before insert \n ");
##below query added by uday with serviceid
 ######################added by uday for ipv6 uncompressed insertion#########################################
if($ip_version=~'4'){$ip_blk=uc$ip_blk;
		my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time,service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp,$service_id,$ip_version,$ip_blk)";
		$sth = $dbh_tr->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                CMS_DB::disconnect_from_transaction();
                return $error if ($error);
                #return $result;
                return $success;
				}else{
				my @z = split(/\//,$ip_blk);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $ip_blk ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc$ipblock6;
						my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time,service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp,$service_id,$ip_version,$ipblock6)";
		$sth = $dbh_tr->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                CMS_DB::disconnect_from_transaction();
                return $error if ($error);
                #return $result;
                return $success;
				
				}
                } else { print DEBUG "IP block already exists in the DB, else of r3\n";
##below query added by uday with serviceid
 ######################added by uday for ipv6 uncompressed insertion#########################################
if($ip_version=~'4'){$ip_blk=uc$ip_blk;
			my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time,service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp,$service_id,$ip_version,$ip_blk)";
                	$sth = $dbh_tr->prepare ("$temp_sql");
                	my $success = $sth->execute;
                	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                	my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                	CMS_DB::disconnect_from_transaction();
                	return $error if ($error);
                	#return $result;
                	return $success;
					}else{
					my @z = split(/\//,$ip_blk);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $ip_blk ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc$ipblock6;
						my $temp_sql = "INSERT INTO reverse (username,customer_id,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time,service_id,ip_version,value) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp,$service_id,$ip_version,$ip_blk)";
                	$sth = $dbh_tr->prepare ("$temp_sql");
                	my $success = $sth->execute;
                	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                	my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
                	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
                	CMS_DB::disconnect_from_transaction();
                	return $error if ($error);
                	#return $result;
                	return $success;
					}
			}#else of r3		

	}else {print DEBUG "range already exists in DB\n";
		if ($last_oct_db eq $last_oct){
		print DEBUG "UPDATE7error\n";
		#my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like $last_oct";
                #print DEBUG "$t_sql\n";
                #CMS_DB::begin_transaction();
                #my $sth = $dbh->prepare ("$t_sql");
                #my $result = $sth->execute;
                #print DEBUG "result:$result\n";
                #$error = $dbh->errstr;
                #print DEBUG "error:$error\n";
                #return $result;
                #CMS_DB::disconnect_from_transaction();
		return "The sub range for the entered IP Block has already been requested.";
		} else {
		print DEBUG "sub range case..throw error\n";
		return "The IP Block has already been requested. Please Cancel it and try again.";
		}
	}
        #return 1;
	print DEBUG "r3 neither <1 nor else condition\n";
        }
	}#end of elsif
	
	# ENDED_CHECK_3_OCTETS_SAME
	
	print DEBUG "FINAL ERROR: REQUEST cannot be proceeded with the Entered IP. Please Enter A New IP.\n";
        return "Reverse Delegation Request cannot be proceeded with the Entered IP Block. Please Enter A New IP block";

	}else {
	print DEBUG "IP Address block does not exist in ip_resource\n";
	return "Reverse Delegation Request cannot be proceeded with the Entered IP Block. Please Enter A New IP block";
	}
} else {
print DEBUG "Account does not exist.\n";
return "Account does not exist";
}

close (DEBUG);
}

open(DEBUG,">>/tmp/tmp_reverse_ajax");
sub check_ipRev { 
my ($accno,$ip_reverse) = @_;
CMS_DB::connect_to_database;
$accno = "\'$accno\'";
        print DEBUG "accno:$accno\n";
$ip_reverse =~ s/\'//g;
#my @y = split(/\//,$ip_reverse);
#my $z1= shift(@y);
#my $z2= shift(@y);
my @ip_octets = split(/\./,$ip_reverse);

#code to split the stored IPs into octets
my $ip_octets1 = shift(@ip_octets);
my $ip_octets2 = shift(@ip_octets);
my $ip_octets3 = shift(@ip_octets);
my $ip_octets4 = shift(@ip_octets);
$ip_octets4 = "\'$ip_octets4\'";
print DEBUG "ip_octets4:$ip_octets4\n";

my $ptr = join ".",$ip_octets3,$ip_octets2,$ip_octets1,"in-addr.arpa";
$ptr = "\'$ptr\'";
print DEBUG "ptr: $ptr\n\n";


        # check for whether the IP already exist in reverse
        my $sth1 = $dbh_tr->prepare ("SELECT count(last_oct) FROM reverse WHERE last_oct ilike $ip_octets4 and ptrrec like $ptr
and customer_id like $accno and status = 1");
        $sth1->execute();
        my $r3 = $sth1->fetchall_arrayref;

        if ($r3->[0]->[0] < 1) {
        print DEBUG "NO MATCH found\n";
        my $retString = 4;
	#CMS_DB::disconnect_from_database;
        return $retString;
        } else {
        print DEBUG "MATCH found\n";
        my $retString = 3;
	#CMS_DB::disconnect_from_database;
        return $retString;
        }
close (DEBUG);
}

open (DBG, ">/tmp/view_rev");
print DBG "CHECK1\n";

sub view_dns_reverse {
print DBG "CHECK2\n";
$logger->debug("view_dns_reverse rev_del_db accno: ");
my $accno = $dbh->quote(shift @_);
print DBG "CHECK3 accno: $accno\n";
$logger->debug("CHECK3 accno: $accno");
my $sth = $dbh->prepare
("SELECT distinct customer_id, contact, value,rec_type,fqdn,last_oct,request_time FROM reverse WHERE customer_id = $accno and status = 1 and fqdn != 'ns03.telstraglobal.net.' and fqdn != 'ns04.telstraglobal.net.' and fqdn != 'unknown.telstraglobal.net.' and fqdn != 'ns03.customer.ap.net.reach.com.' and fqdn != 'ns04.customer.ap.net.reach.com.'  and fqdn != 'static.telstraglobal.net.' ORDER BY request_time desc"); 

        unless ($sth->execute) {
                $error = $dbh->errstr;
		  $logger->debug("CHECK3 error: $error");
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
	print DBG "result: $result->[0]->[0] \n";
        $error = $dbh->errstr;
	close (DBG);
        return $result;
}
	
sub reverse_cancel {
open (DEBUG, ">/tmp/rev_db_cancel");
print DEBUG "ENTERED\n";
CMS_DB::begin_transaction();
my $uid     = $dbh_tr->quote(shift @_);
my $accno   = $dbh_tr->quote(shift @_);
my $contact = $dbh_tr->quote(shift @_);
my $parm    = $dbh_tr->quote(shift @_);
print DEBUG "before domain\nuid:$uid\taccno:$accno\tcontact:$contact\tparm:$parm\n";
#my $domain    = $dbh->(shift @_);
my $ip_version = (shift @_);
my $domain    = (shift @_);
$domain = $domain.".";
$domain = "\'$domain\'";
my $parm_val = $parm;
my $result = '';
print DEBUG "uid: $uid\n accno: $accno\n contact: $contact\n domain: $domain\n parm: $parm\n parm_val: $parm_val\n\n";
$parm_val =~ s/\'//g;

my @ip_octets_v6 = '';
my @ip_octets = '';
my $ip_octet1 = '';
my $ip_octet2 = '';
my $ip_octet3 = '';
my $ip_octet4 = '';
my $last_oct = '';
my $ip_ptr = ''; 
my $ptr = '';
my @y = '';
my $z1 = '';
my $z2 = '';

if ($ip_version =~ '4' ){
if ($parm_val =~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/){
 @y = split(/\//,$parm_val);
 $z1= shift(@y);
 $z2= shift(@y);
print DEBUG "z1: $z1\n z2: $z2\n";
 @ip_octets = split(/\./,$z1);
 $ip_octet1 = shift(@ip_octets);
 $ip_octet2 = shift(@ip_octets);
 $ip_octet3 = shift(@ip_octets);
 $ip_octet4 = shift(@ip_octets);
 $last_oct = join "/",$ip_octet4,$z2;
 $ip_ptr = join ".",$ip_octet3,$ip_octet2,$ip_octet1;
 $ptr = join ".", $ip_ptr,"in-addr.arpa";
}
elsif ($parm_val =~ /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/){
 @ip_octets = split(/\./,$parm_val);
 $ip_octet1 = shift(@ip_octets);
 $ip_octet2 = shift(@ip_octets);
 $ip_octet3 = shift(@ip_octets);
 $ip_octet4 = shift(@ip_octets);
 $last_oct = $ip_octet4;
 $ip_ptr = join ".",$ip_octet3,$ip_octet2,$ip_octet1;
 $ptr = join ".", $ip_ptr,"in-addr.arpa";
}
}
elsif ($ip_version =~ '6' ){				 
	if ($parm_val =~ /\/\d{1,2}$/){
 		@y = split(/\//,$parm_val);
 		$z1= shift(@y);
		$z2= shift(@y);
 
 		$z1 = Net::IP::ip_expand_address($z1, 6);  # exapnd compressed ipv6  			
  		my @ip_octets_v6 = split(/\:/,$z1);
   		foreach my $count (3 .. $#ip_octets_v6){
			$ip_octet4 = join ":",$ip_octets_v6[$count];						
		}
		 $logger->debug("ip oct:$ip_octet4 ");
		  my $ip_octets45=substr($z1,15);
		$last_oct = join "/",$ip_octets45,$z2;
		  $logger->debug("LAST OCTECT in if:$last_oct ");
		my $rev6=new Net::IP ($z1); 				# Net:: ip function for ipv6.arpa 
		my $ptrval=$rev6->reverse_ip();
		$ptr= substr($ptrval,44,-1); 
		$logger->debug("PTR:$ptr in if");
		#$ptrval= substr $ptrval,1,-1 ; 	# strip of the leading & trailing dots
		#$ptrval= substr $ptrval,0,-1 ; 	# strip of the leading & trailing dots
		#$ptr=substr $ptrval,0,-33;		# splitting ipv6.arpa & last_oct 
		#$ip_octet4 =substr $ptrval,40;
 		#$last_oct = join "/",$ip_octet4,$z2;
	}
	else {
		$z1 = Net::IP::ip_expand_address($parm_val, 6);
 		my @ip_octets_v6 = split(/\:/,$z1);
   		foreach my $count (3 .. $#ip_octets_v6){
			$ip_octet4 = join ":",$ip_octets_v6[$count];					
		}
		 my $ip_octets45=substr($z1,15);
		$last_oct = $ip_octets45;
		  $logger->debug("LAST OCTECT:$last_oct ");
		my $rev6=new Net::IP ($z1);
 		my $ptrval=$rev6->reverse_ip();
		$ptr= substr($ptrval,44,-1); 
		#$ptr=substr($ptr,1);
		
		#$ptrval= substr $ptrval,1,-1 ; 	# strip of the leading & trailing dots
		#$ptrval= substr $ptrval,0,-1 ; 	# strip of the leading & trailing dots
 		#$ptr=substr $ptrval,0,-33;		
 		#$ip_octet4 =substr $ptrval,40;
 	
	}
}

$ptr = "\'$ptr\'";
$z1 = "\'$z1\'";
$ip_octet4 = "\'$ip_octet4\'";
$z2 = "\'$z2\'";
$last_oct = "\'$last_oct\'";

print DEBUG "ptr:$ptr\tz1:$z1\tip_octet4:$ip_octet4\tz2:$z2\tlast_oct:$last_oct\n";
# Users owho have requested for DNS Service are only allowed to cancel the Service.

#my $sth = $dbh->prepare ("SELECT count(p.accno) FROM  reverse p, olss_cust_access_acc o where o.accno = p.accno");
#my $sth = $dbh_tr->prepare ("SELECT count(p.customer_id) FROM  reverse p, olss_cust_access_acc o where o.customer_id= p.accno and p.customer_idlike $accno");
#my $sth = $dbh->prepare ("SELECT count(p.username) FROM  reverse p, olss_cust_access_acc o where o.accno = p.accno and p.username ilike $uid");
#$sth->execute();
#my $r1 = $sth->fetchall_arrayref;
my $r1 = 1;
$logger->debug("before insert before if : $r1 ,last ct: $last_oct , ptr:$ptr, dom:$domain");
if($r1 >= 1) {
	my $sth1 = $dbh_tr->prepare ("SELECT count(last_oct) from reverse where last_oct like $last_oct and ptrrec like $ptr and fqdn like $domain");
	$sth1->execute();
        my $r2 = $sth1->fetchall_arrayref;
	print DEBUG "DB r2:$r2\n";
	print DEBUG "DB inside r1 loop\n";
$logger->debug("before after before if : $r2->[0]->[0] ");
        if($r2->[0]->[0] >= 1){
		my $success = 1;
		$logger->debug("INSIDE IF ############### ");
		#$sth = $dbh_tr->prepare("UPDATE reverse SET status = 3, request_time = CURRENT_TIMESTAMP  WHERE ptrrec like $ptr and last_oct like $last_oct and accno like $accno and username like $uid"); 
		my $sth = $dbh_tr->prepare("UPDATE reverse SET status = 3, request_time = CURRENT_TIMESTAMP  WHERE ptrrec like $ptr and fqdn like $domain and last_oct like $last_oct and customer_id like $accno"); 
 		$success &&= $sth->execute;
        	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
		print HKG "error: $error\n";
        	$result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
		print HKG "error: $error\t result: $result\n";
		print HKG "OUTSIDE1 result: $result\n ";
              CMS_DB::disconnect_from_transaction();
        	return $error if ($error);
        	#return $result;
				$logger->debug("ERROR   succ $success");
		return $success;
	}else{
	$result = 0;
	#print HKG "OUTSIDE2 result: $result\n ";
     
	return $result;
	}
} else {
$result = "User $uid does not have the rights to cancel the REVERSE Service for this IP Block\n";;
print DEBUG "OUTSIDE2 result: $result\n ";
return $result;
}
close (DEBUG);
}

sub rev_range {
my $val = @_;
my $range = 0;
if ($val ==24) {
                $range = 255;
        }elsif ($val == 25) {
                 $range = 127;
        }elsif ($val == 26) {
                 $range = 63;
        }elsif ($val == 27 ){
                 $range = 31;
        }elsif ($val == 28 ){
                 $range = 15;
        }elsif ($val == 29 ){
                 $range = 7;
        }elsif ($val == 30 ){
                 $range = 3;
        }elsif ($val == 31 ){
                 $range = 1;
        }elsif ($val == 32 ){
                 $range = 0;
        }
return $range;
}

sub rev_range6{
my ($rev_val) = @_;
my $range = 0;
if ($rev_val ==48) {
                $range = 1208925819614629174706176;
        }elsif ($rev_val == 49) {
                 $range = 604462909807314587353088;
        }elsif ($rev_val == 50) {
                 $range = 302231454903657293676544;
        }elsif ($rev_val == 51 ){
                 $range = 151115727451828646838272;
        }elsif ($rev_val == 52 ){
                 $range = 75557863725914323419136;
        }elsif ($rev_val == 53 ){
                 $range = 37778931862957161709568;
        }elsif ($rev_val == 54 ){
                 $range = 18889465931478580854784;
        }elsif ($rev_val == 55 ){
                 $range = 9444732965739290427392;
        }elsif ($rev_val == 56) {
                $range = 4722366482869645213696;
        }elsif ($rev_val == 57) {
                 $range = 2361183241434822606848;
        }elsif ($rev_val == 58) {
                 $range = 1180591620717411303424;
        }elsif ($rev_val == 59 ){
                 $range = 590295810358705651712;
        }elsif ($rev_val == 60 ){
                 $range = 295147905179352825856;
        }elsif ($rev_val == 61 ){
                 $range = 147573952589676412928;
        }elsif ($rev_val == 62 ){
                 $range = 73786976294838206464;
        }elsif ($rev_val == 63 ){
                 $range = 36893488147419103232;
        }elsif ($rev_val == 64 ){
                 $range = 18446744073709551616;
        }
        return $range;
}
sub sel_ptr1{
open (HKG, ">/tmp/rev_ptr");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my ($ptr) = $dbh_tr->quote(shift @_); 
my $res = '';
my $temp = "SELECT count(last_oct) from reverse where ptrrec like $ptr";
my $sth = $dbh_tr->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub sel_ptr{
open (HKG, ">/tmp/rev_sel");
my ($result) = $dbh_tr->quote(shift @_);
print HKG "result:$result";
my $res = '';
my $sth = $dbh_tr->prepare("SELECT ptrrec,last_oct,rec_type,fqdn,request_time,refresh_time,status from reverse where ptrrec like $result and (status =1 or status =3)  and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.' order by last_oct");
$sth->execute();
$res = $sth->fetchall_arrayref();
print HKG "result2:$res\n";
return $res;
close (HKG);
}

sub lastoct {
open (HKG, ">/tmp/lastoct");
my ($ptr) = $dbh_tr->quote(shift @_);
my ($last_oct) = $dbh_tr->quote(shift @_);
my $temp = "SELECT distinct ptrrec,last_oct from reverse where ptrrec like $ptr and last_oct like $last_oct and status = 1";
print HKG "temp:$temp\n";
my $sth = $dbh_tr->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub request_t {
open (HKG, ">/tmp/request");
print HKG "ENTERED\n";
my $ptr1 =  @_[0];
#my ($ptr) = $dbh_tr->quote(shift @_);
print HKG "ptr:$ptr1\n";
#my $temp = "SELECT request_time from reverse where ptrrec like $ptr and status = 1 and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'";
my $temp = "SELECT request_time from reverse where ptrrec like '$ptr1' and status = 1 and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'";
print HKG "temp:$temp\n";
my $sth = $dbh_tr->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}


sub rev_name{
open (HKG, ">/tmp/rev_name");
print HKG "ENTERED\n";
my ($ptr) = $dbh_tr->quote(shift @_);
my ($req_time) = $dbh_tr->quote(shift @_);
print HKG "req_time:$req_time\n";
my $temp = "SELECT distinct(fqdn) from reverse where ptrrec like $ptr and status = 1 and request_time like $req_time and rec_type like 'NS' and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'";
print HKG "temp:$temp\n";
my $sth = $dbh_tr->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}



sub rev_req_time {
open (HKG, ">/tmp/rev_req_time");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my ($ptr) = $dbh_tr->quote(shift @_);
print HKG "ptr:$ptr\n";
my $temp = "SELECT request_time from reverse where ptrrec like $ptr and status = 1 and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'";
print HKG "temp:$temp\n";
my $sth = $dbh_tr->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub all_ptr {
open (HKG, ">/tmp/all_ptr");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $temp = "SELECT distinct ptrrec from reverse ";
my $sth = $dbh_tr->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}


sub time_db {
open (HKG, ">/tmp/revdns_time");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $temp = "SELECT request_time from reverse";
my $sth = $dbh_tr->prepare ("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub all_values {
open (HKG, ">/tmp/revdns_all");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $temp = "SELECT ptrrec,last_oct,rec_type,fqdn from reverse where status =1";
my $sth = $dbh_tr->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub count_ptr {
open (HKG, ">/tmp/revdns_count");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $ptr = $dbh_tr->quote (shift @_);
print HKG "ptr:$ptr\n";
my $temp = "SELECT count(last_oct) from reverse where ptrrec like $ptr and status = 1";
my $sth = $dbh_tr->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub values_db {
open (HKG, ">/tmp/revdns_vald");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $ptr = $dbh_tr->quote (shift @_);
print HKG "ptr:$ptr\n";
my $temp = "SELECT request_time from reverse where ptrrec like $ptr and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.' and request_time is not null order by request_time desc";
print HKG "temp:$temp\n";
my $sth = $dbh_tr->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub maildetails {
open (HKG, ">/tmp/mail_details");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $req_time = $dbh_tr->quote (shift @_);
my $temp = "SELECT ptrrec,last_oct,rec_type,fqdn,status,contact,customer_id, username from reverse where request_time like $req_time";
my $sth = $dbh_tr->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}
 


sub binary_add {#binary AND of ip blocks
my @z1_octets = "";
my $z1_octet1 = "";
my $z1_octet2 = "";
my $z1_octet3 = "";
my $z1_octet4 = "";
my $z1_bin_1 = "";
my $z1_bin_2 = "";
my $z1_bin_3 = "";
my $z1_bin_4 = "";
my @z1_binary = "";
my $z1_binary_stat = "";
my @z1_binary_bit = "";
my @ip_octets = "";
my $ip_octet1 = "";
my $ip_octet2 = "";
my $ip_octet3 = "";
my $ip_octet4 = "";
my $ip_bin_1 = "";
my $ip_bin_2 = "";
my $ip_bin_3 = "";
my $ip_bin_4 = "";
my @ip_binary = "";
my $ip_binary_stat = "";
my @ip_binary_bit = "";
my $ip_length = "";
my $ip_binary_result = "";
my $z1_binary_result = "";
my $loop_count2 = 0;
my $ipprefix = "";
my $compare_flag=0;
my $db_flag=0;
my ($parm_val,$z1,$z2) = @_;

my @ip_chk1 = split(/\//,$parm_val);
my $ipnew_chk1  = shift(@ip_chk1);#ipnew_chk has ip
my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
my @ip_octets = split(/\./,$ipnew_chk1);
#code to split the stored IPs into octets and changing them to binary
$ip_octet1 = shift(@ip_octets);
$ip_octet2 = shift(@ip_octets);
$ip_octet3 = shift(@ip_octets);
$ip_octet4 = shift(@ip_octets);

$ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
$ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
$ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
$ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

#combining individual IPs into one variable/array
my @ip_binary = ();
push(@ip_binary, $ip_bin_1);
push(@ip_binary, $ip_bin_2);
push(@ip_binary, $ip_bin_3);
push(@ip_binary, $ip_bin_4);
$ip_binary_stat = join("",@ip_binary);
@ip_binary_bit = split(//,$ip_binary_stat);
#print"ip binary is @ip_binary_bit\n";

#code to split the entered IPs into octets and changing octets to binary.
        @z1_octets = split(/\./,$z1);
        $z1_octet1 = shift(@z1_octets);
        $z1_octet2 = shift(@z1_octets);
        $z1_octet3 = shift(@z1_octets);
        $z1_octet4 = shift(@z1_octets);

        $z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
        $z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
        $z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
        $z1_bin_4 = unpack("B8", pack('C',$z1_octet4));

#combining the individual IPs into one variable/array
        @z1_binary = ();
        push(@z1_binary,$z1_bin_1);
        push(@z1_binary,$z1_bin_2);
        push(@z1_binary,$z1_bin_3);
        push(@z1_binary,$z1_bin_4);
        $z1_binary_stat = join("",@z1_binary);
        @z1_binary_bit = split(//,$z1_binary_stat);
        print DEBUG "input is @z1_binary_bit\n";
        $ipprefix = $ipprefixip_chk1;
if($z2 > $ipprefix){$ip_length = $ipprefix;}
 else {$ip_length = $z2;}
                print DEBUG "ip length value:$ip_length\n";
                $compare_flag = 1;
                for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
                print DEBUG "entered for condition\n";
                        if ($db_flag == 0){
                        print DEBUG "DB entered if compare flag condition\n";
                        $z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
                        $ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
                                if ($z1_binary_result != $ip_binary_result){
                                print DEBUG "DB binary ADD false\n";
                                print DEBUG "DB db flag 1\n";
                                $db_flag = 1;
                                }
                                elsif ($z1_binary_result == $ip_binary_result)
                                {
                                print DEBUG "DB equal to binary add\n";
                                print DEBUG "DB db flag 0\n";
                                $db_flag = 0;}
                        }
                }#end of both if loops and for loop
        if ($db_flag == 1 && $compare_flag ==1)
                {
                print DEBUG "DB compare flag 1\n";
                $compare_flag = 1;}
        else
                {
                print DEBUG "DB compare flag 0\n";
                $compare_flag = 0;}
return $compare_flag;
}

# Get reverse request types for View
# Prost: $result
sub get_rev_view {
open (DEBUG,">/tmp/rev_mng_map");
#my $temp_sql = "SELECT rev_type,rev_id FROM reverse_type ORDER BY rev_type,rev_id";
my $sth1 = $dbh_tr->prepare ("SELECT rev_type,rev_id FROM reverse_type ORDER BY rev_type,rev_id");
#my $sth1 = $dbh_tr->prepare ("$temp_sql");
print DEBUG "sth:$sth1\n";
$sth1->execute();
my $result = $sth1->fetchall_arrayref;
print DEBUG "result:$result\n";
$error = $dbh_tr->errstr;
print DEBUG "error:$error\n";
return $result;
close DEBUG;
}

# Get reverse request types
# Pre  : $search_by (0=none, 1=type_name, 2=type_id), $val
# Prost: $result
sub get_reverse_type{
CMS_DB::begin_transaction();
open (DEBUG,">/tmp/rev_mng_map");
my ($search_by, $val) = @_;
print DEBUG "search_by:$search_by\tval:$val\n";

$search_by = 0 if ($search_by eq '');
my $cond = ' WHERE ';
my $temp_sql = '';

#if ($search_by == 1) {
#print DEBUG "search by 1\n";
#       $cond .= " type ilike '$val' ";
#$temp_sql = "SELECT rev_type,rev_id FROM reverse_type $cond ORDER BY rev_type,rev_id";
if ($search_by == 2) {
print DEBUG "search by 2\n";
        my $x = $val + 0;
        $cond .= " rev_id = $x ";
$temp_sql = "SELECT rev_type,rev_id FROM reverse_type $cond ORDER BY rev_type,rev_id";
}

print DEBUG "cond:$cond\n";
print DEBUG "SELECT rev_type,rev_id FROM reverse_type $cond ORDER BY rev_type,rev_id\n";
#my $temp_sql = "SELECT rev_type,rev_id FROM reverse_type $cond ORDER BY rev_type,rev_id";
#my $sth = $dbh_tr->prepare ("SELECT rev_type,rev_id FROM reverse_type $cond ORDER BY rev_type,rev_id");
my $sth1 = $dbh_tr->prepare ("$temp_sql");
print DEBUG "sth:$sth1\n";
#$unless ($sth->execute) {
        #$error = $dbh_tr->errstr;

#print DEBUG "error:$error\n";
        #return 0;
        #}
$sth1->execute();
my $result = $sth1->fetchall_arrayref;
print DEBUG "result:$result\n";
$error = $dbh_tr->errstr;
print DEBUG "error:$error\n";
return $result;
close DEBUG;
#CMS_DB::disconnect_from_transaction();
}
1;

sub all_values {
open (HKG, ">/tmp/revdns_all");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $temp = "SELECT ptrrec,last_oct,rec_type,fqdn,ip_version from reverse where status =1";
}

sub sel_ptr1{
open (HKG, ">/tmp/rev_ptr");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my ($ptr) = $dbh_tr->quote(shift @_); 
my $res = '';
my $temp = "SELECT count(last_oct),ip_version from reverse where ptrrec like $ptr";
}
