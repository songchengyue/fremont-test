/*
 * An XML document type.
 * Localname: pullTopologyMapChunksResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one pullTopologyMapChunksResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class PullTopologyMapChunksResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponseDocument
{
    
    public PullTopologyMapChunksResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PULLTOPOLOGYMAPCHUNKSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "pullTopologyMapChunksResponse");
    
    
    /**
     * Gets the "pullTopologyMapChunksResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse getPullTopologyMapChunksResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse)get_store().find_element_user(PULLTOPOLOGYMAPCHUNKSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "pullTopologyMapChunksResponse" element
     */
    public void setPullTopologyMapChunksResponse(com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse pullTopologyMapChunksResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse)get_store().find_element_user(PULLTOPOLOGYMAPCHUNKSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse)get_store().add_element_user(PULLTOPOLOGYMAPCHUNKSRESPONSE$0);
            }
            target.set(pullTopologyMapChunksResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "pullTopologyMapChunksResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse addNewPullTopologyMapChunksResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse)get_store().add_element_user(PULLTOPOLOGYMAPCHUNKSRESPONSE$0);
            return target;
        }
    }
}
