/*
 * XML Type:  Parameters
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.Parameters
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML Parameters(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ParametersImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.Parameters
{
    
    public ParametersImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DATEPARAMS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "dateParams");
    private static final javax.xml.namespace.QName DOUBLEPARAMS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "doubleParams");
    private static final javax.xml.namespace.QName FLOATPARAMS$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "floatParams");
    private static final javax.xml.namespace.QName INTLISTPARAMS$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intListParams");
    private static final javax.xml.namespace.QName INTPARAMS$8 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intParams");
    private static final javax.xml.namespace.QName STRPARAMS$10 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strParams");
    private static final javax.xml.namespace.QName STRLISTPARAMS$12 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strListParams");
    private static final javax.xml.namespace.QName LONGPARAMS$14 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "longParams");
    private static final javax.xml.namespace.QName BYTESPARAMS$16 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "bytesParams");
    private static final javax.xml.namespace.QName XMLPARAMS$18 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "xmlParams");
    private static final javax.xml.namespace.QName BOOLEANPARAMS$20 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "booleanParams");
    
    
    /**
     * Gets the "dateParams" element
     */
    public com.hp.schemas.ucmdb._1.types.DateParams getDateParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateParams)get_store().find_element_user(DATEPARAMS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "dateParams" element
     */
    public void setDateParams(com.hp.schemas.ucmdb._1.types.DateParams dateParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateParams)get_store().find_element_user(DATEPARAMS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.DateParams)get_store().add_element_user(DATEPARAMS$0);
            }
            target.set(dateParams);
        }
    }
    
    /**
     * Appends and returns a new empty "dateParams" element
     */
    public com.hp.schemas.ucmdb._1.types.DateParams addNewDateParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateParams)get_store().add_element_user(DATEPARAMS$0);
            return target;
        }
    }
    
    /**
     * Gets the "doubleParams" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleParams getDoubleParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleParams)get_store().find_element_user(DOUBLEPARAMS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "doubleParams" element
     */
    public void setDoubleParams(com.hp.schemas.ucmdb._1.types.DoubleParams doubleParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleParams)get_store().find_element_user(DOUBLEPARAMS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.DoubleParams)get_store().add_element_user(DOUBLEPARAMS$2);
            }
            target.set(doubleParams);
        }
    }
    
    /**
     * Appends and returns a new empty "doubleParams" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleParams addNewDoubleParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleParams)get_store().add_element_user(DOUBLEPARAMS$2);
            return target;
        }
    }
    
    /**
     * Gets the "floatParams" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatParams getFloatParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatParams)get_store().find_element_user(FLOATPARAMS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "floatParams" element
     */
    public void setFloatParams(com.hp.schemas.ucmdb._1.types.FloatParams floatParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatParams)get_store().find_element_user(FLOATPARAMS$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.FloatParams)get_store().add_element_user(FLOATPARAMS$4);
            }
            target.set(floatParams);
        }
    }
    
    /**
     * Appends and returns a new empty "floatParams" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatParams addNewFloatParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatParams)get_store().add_element_user(FLOATPARAMS$4);
            return target;
        }
    }
    
    /**
     * Gets the "intListParams" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListParams getIntListParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListParams)get_store().find_element_user(INTLISTPARAMS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "intListParams" element
     */
    public void setIntListParams(com.hp.schemas.ucmdb._1.types.IntListParams intListParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListParams)get_store().find_element_user(INTLISTPARAMS$6, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IntListParams)get_store().add_element_user(INTLISTPARAMS$6);
            }
            target.set(intListParams);
        }
    }
    
    /**
     * Appends and returns a new empty "intListParams" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListParams addNewIntListParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListParams)get_store().add_element_user(INTLISTPARAMS$6);
            return target;
        }
    }
    
    /**
     * Gets the "intParams" element
     */
    public com.hp.schemas.ucmdb._1.types.IntParams getIntParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntParams)get_store().find_element_user(INTPARAMS$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "intParams" element
     */
    public void setIntParams(com.hp.schemas.ucmdb._1.types.IntParams intParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntParams)get_store().find_element_user(INTPARAMS$8, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IntParams)get_store().add_element_user(INTPARAMS$8);
            }
            target.set(intParams);
        }
    }
    
    /**
     * Appends and returns a new empty "intParams" element
     */
    public com.hp.schemas.ucmdb._1.types.IntParams addNewIntParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntParams)get_store().add_element_user(INTPARAMS$8);
            return target;
        }
    }
    
    /**
     * Gets the "strParams" element
     */
    public com.hp.schemas.ucmdb._1.types.StrParams getStrParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrParams)get_store().find_element_user(STRPARAMS$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "strParams" element
     */
    public void setStrParams(com.hp.schemas.ucmdb._1.types.StrParams strParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrParams)get_store().find_element_user(STRPARAMS$10, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrParams)get_store().add_element_user(STRPARAMS$10);
            }
            target.set(strParams);
        }
    }
    
    /**
     * Appends and returns a new empty "strParams" element
     */
    public com.hp.schemas.ucmdb._1.types.StrParams addNewStrParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrParams)get_store().add_element_user(STRPARAMS$10);
            return target;
        }
    }
    
    /**
     * Gets the "strListParams" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListParams getStrListParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListParams)get_store().find_element_user(STRLISTPARAMS$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "strListParams" element
     */
    public void setStrListParams(com.hp.schemas.ucmdb._1.types.StrListParams strListParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListParams)get_store().find_element_user(STRLISTPARAMS$12, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrListParams)get_store().add_element_user(STRLISTPARAMS$12);
            }
            target.set(strListParams);
        }
    }
    
    /**
     * Appends and returns a new empty "strListParams" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListParams addNewStrListParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListParams)get_store().add_element_user(STRLISTPARAMS$12);
            return target;
        }
    }
    
    /**
     * Gets the "longParams" element
     */
    public com.hp.schemas.ucmdb._1.types.LongParams getLongParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongParams)get_store().find_element_user(LONGPARAMS$14, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "longParams" element
     */
    public void setLongParams(com.hp.schemas.ucmdb._1.types.LongParams longParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongParams)get_store().find_element_user(LONGPARAMS$14, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.LongParams)get_store().add_element_user(LONGPARAMS$14);
            }
            target.set(longParams);
        }
    }
    
    /**
     * Appends and returns a new empty "longParams" element
     */
    public com.hp.schemas.ucmdb._1.types.LongParams addNewLongParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongParams)get_store().add_element_user(LONGPARAMS$14);
            return target;
        }
    }
    
    /**
     * Gets the "bytesParams" element
     */
    public com.hp.schemas.ucmdb._1.types.BytesParams getBytesParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesParams)get_store().find_element_user(BYTESPARAMS$16, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "bytesParams" element
     */
    public void setBytesParams(com.hp.schemas.ucmdb._1.types.BytesParams bytesParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesParams)get_store().find_element_user(BYTESPARAMS$16, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.BytesParams)get_store().add_element_user(BYTESPARAMS$16);
            }
            target.set(bytesParams);
        }
    }
    
    /**
     * Appends and returns a new empty "bytesParams" element
     */
    public com.hp.schemas.ucmdb._1.types.BytesParams addNewBytesParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesParams)get_store().add_element_user(BYTESPARAMS$16);
            return target;
        }
    }
    
    /**
     * Gets the "xmlParams" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlParams getXmlParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlParams)get_store().find_element_user(XMLPARAMS$18, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "xmlParams" element
     */
    public void setXmlParams(com.hp.schemas.ucmdb._1.types.XmlParams xmlParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlParams)get_store().find_element_user(XMLPARAMS$18, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.XmlParams)get_store().add_element_user(XMLPARAMS$18);
            }
            target.set(xmlParams);
        }
    }
    
    /**
     * Appends and returns a new empty "xmlParams" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlParams addNewXmlParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlParams)get_store().add_element_user(XMLPARAMS$18);
            return target;
        }
    }
    
    /**
     * Gets the "booleanParams" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanParams getBooleanParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanParams)get_store().find_element_user(BOOLEANPARAMS$20, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "booleanParams" element
     */
    public void setBooleanParams(com.hp.schemas.ucmdb._1.types.BooleanParams booleanParams)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanParams)get_store().find_element_user(BOOLEANPARAMS$20, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.BooleanParams)get_store().add_element_user(BOOLEANPARAMS$20);
            }
            target.set(booleanParams);
        }
    }
    
    /**
     * Appends and returns a new empty "booleanParams" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanParams addNewBooleanParams()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanParams target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanParams)get_store().add_element_user(BOOLEANPARAMS$20);
            return target;
        }
    }
}
