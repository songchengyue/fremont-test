/*
 * XML Type:  LongConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.LongConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML LongConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class LongConditionsImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.LongConditions
{
    
    public LongConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LONGCONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "longCondition");
    
    
    /**
     * Gets array of all "longCondition" elements
     */
    public com.hp.schemas.ucmdb._1.types.LongCondition[] getLongConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(LONGCONDITION$0, targetList);
            com.hp.schemas.ucmdb._1.types.LongCondition[] result = new com.hp.schemas.ucmdb._1.types.LongCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "longCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.LongCondition getLongConditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongCondition)get_store().find_element_user(LONGCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "longCondition" element
     */
    public int sizeOfLongConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(LONGCONDITION$0);
        }
    }
    
    /**
     * Sets array of all "longCondition" element
     */
    public void setLongConditionArray(com.hp.schemas.ucmdb._1.types.LongCondition[] longConditionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(longConditionArray, LONGCONDITION$0);
        }
    }
    
    /**
     * Sets ith "longCondition" element
     */
    public void setLongConditionArray(int i, com.hp.schemas.ucmdb._1.types.LongCondition longCondition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongCondition)get_store().find_element_user(LONGCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(longCondition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "longCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.LongCondition insertNewLongCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongCondition)get_store().insert_element_user(LONGCONDITION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "longCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.LongCondition addNewLongCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongCondition)get_store().add_element_user(LONGCONDITION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "longCondition" element
     */
    public void removeLongCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(LONGCONDITION$0, i);
        }
    }
}
