/*
 * XML Type:  updateCIsAndRelations
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update;


/**
 * An XML updateCIsAndRelations(@http://schemas.hp.com/ucmdb/1/params/update).
 *
 * This is a complex type.
 */
public interface UpdateCIsAndRelations extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(UpdateCIsAndRelations.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("updatecisandrelationsa957type");
    
    /**
     * Gets the "cmdbContext" element
     */
    com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext();
    
    /**
     * Sets the "cmdbContext" element
     */
    void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext);
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext();
    
    /**
     * Gets the "dataStore" element
     */
    java.lang.String getDataStore();
    
    /**
     * Gets (as xml) the "dataStore" element
     */
    org.apache.xmlbeans.XmlString xgetDataStore();
    
    /**
     * True if has "dataStore" element
     */
    boolean isSetDataStore();
    
    /**
     * Sets the "dataStore" element
     */
    void setDataStore(java.lang.String dataStore);
    
    /**
     * Sets (as xml) the "dataStore" element
     */
    void xsetDataStore(org.apache.xmlbeans.XmlString dataStore);
    
    /**
     * Unsets the "dataStore" element
     */
    void unsetDataStore();
    
    /**
     * Gets the "CIsAndRelationsUpdates" element
     */
    com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates getCIsAndRelationsUpdates();
    
    /**
     * Sets the "CIsAndRelationsUpdates" element
     */
    void setCIsAndRelationsUpdates(com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates cIsAndRelationsUpdates);
    
    /**
     * Appends and returns a new empty "CIsAndRelationsUpdates" element
     */
    com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates addNewCIsAndRelationsUpdates();
    
    /**
     * Gets the "ignoreValidation" element
     */
    boolean getIgnoreValidation();
    
    /**
     * Gets (as xml) the "ignoreValidation" element
     */
    org.apache.xmlbeans.XmlBoolean xgetIgnoreValidation();
    
    /**
     * Tests for nil "ignoreValidation" element
     */
    boolean isNilIgnoreValidation();
    
    /**
     * True if has "ignoreValidation" element
     */
    boolean isSetIgnoreValidation();
    
    /**
     * Sets the "ignoreValidation" element
     */
    void setIgnoreValidation(boolean ignoreValidation);
    
    /**
     * Sets (as xml) the "ignoreValidation" element
     */
    void xsetIgnoreValidation(org.apache.xmlbeans.XmlBoolean ignoreValidation);
    
    /**
     * Nils the "ignoreValidation" element
     */
    void setNilIgnoreValidation();
    
    /**
     * Unsets the "ignoreValidation" element
     */
    void unsetIgnoreValidation();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations newInstance() {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
