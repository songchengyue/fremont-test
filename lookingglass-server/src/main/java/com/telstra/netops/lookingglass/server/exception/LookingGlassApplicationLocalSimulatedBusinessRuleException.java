package com.telstra.netops.lookingglass.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Simulated business rule error (are you testing?)")
public class LookingGlassApplicationLocalSimulatedBusinessRuleException extends
		RuntimeException {

	public LookingGlassApplicationLocalSimulatedBusinessRuleException(
			String message) {
		super(message);
	}

}
