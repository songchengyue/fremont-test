# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
#
# $Id: CMS_SECDNS.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $

package CMS_SECDNS;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_SECDNS_DB;

our @ISA = qw (Exporter);

our $error;

sub get_secdns_requests {
  $CMS::html_values{requests}{table} =
    CMS_SECDNS_DB::search_secdns_entries(@_);
  unshift(@{$CMS::html_values{requests}{table}},
	  ["Operations Handle", "Domain Name", "Primary IP", "Status",
           "Request time"]);
  $CMS::html_values{requests}{header_rows} = 1;
}

sub start {
  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} eq "Cancel"))) {
    # default to view_recent_requests
    
    $CMS::html_values{command}{value} = "view_recent_requests";
    
    
    
  }

  $CMS::html_values{search_status}{options} = 
    CMS_SECDNS_DB::get_secdns_status;

 
  if ($CMS::html_values{command}{value} eq "view_recent_requests") {
    get_secdns_requests;

    TI_HTML::set_datetime_search_values
	(\%CMS::html_values, "search_date",
	 "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");


    CMS::output_html("secdns_list");
  
  
  } elsif ($CMS::html_values{command}{value} eq "search_requests") {

    if (defined($CMS::html_values{submit}{value}) &&
	$CMS::html_values{submit}{value} eq "Search") {
      my ($start,$end) = TI_HTML::get_datetime_search_values
	(\%CMS::html_values, "search_date");
      get_secdns_requests
	($CMS::html_values{search_accno}{value},
	 $CMS::html_values{search_domain}{value},
	 $CMS::html_values{search_primaryip}{value},
	 $CMS::html_values{search_status}{value},
	 $start, $end);
	
    } else {


      $CMS::html_values{search_accno}{value} = "";
      $CMS::html_values{search_domain}{value} = "";
      $CMS::html_values{search_primaryip}{value} = "";
      $CMS::html_values{search_status}{value} = "";

      TI_HTML::set_datetime_search_values
	  (\%CMS::html_values, "search_date",
	   "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");

    }

    # nothing entered
    CMS::output_html("secdns_list");
  }

}
1;
