/*
 * XML Type:  ChunkInfo
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ChunkInfo
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ChunkInfo(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ChunkInfoImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ChunkInfo
{
    
    public ChunkInfoImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NUMBEROFCHUNKS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "numberOfChunks");
    private static final javax.xml.namespace.QName CHUNKSKEY$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "chunksKey");
    
    
    /**
     * Gets the "numberOfChunks" element
     */
    public int getNumberOfChunks()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NUMBEROFCHUNKS$0, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "numberOfChunks" element
     */
    public org.apache.xmlbeans.XmlInt xgetNumberOfChunks()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(NUMBEROFCHUNKS$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "numberOfChunks" element
     */
    public void setNumberOfChunks(int numberOfChunks)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NUMBEROFCHUNKS$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NUMBEROFCHUNKS$0);
            }
            target.setIntValue(numberOfChunks);
        }
    }
    
    /**
     * Sets (as xml) the "numberOfChunks" element
     */
    public void xsetNumberOfChunks(org.apache.xmlbeans.XmlInt numberOfChunks)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(NUMBEROFCHUNKS$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(NUMBEROFCHUNKS$0);
            }
            target.set(numberOfChunks);
        }
    }
    
    /**
     * Gets the "chunksKey" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkKey getChunksKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkKey target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkKey)get_store().find_element_user(CHUNKSKEY$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "chunksKey" element
     */
    public void setChunksKey(com.hp.schemas.ucmdb._1.types.ChunkKey chunksKey)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkKey target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkKey)get_store().find_element_user(CHUNKSKEY$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ChunkKey)get_store().add_element_user(CHUNKSKEY$2);
            }
            target.set(chunksKey);
        }
    }
    
    /**
     * Appends and returns a new empty "chunksKey" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkKey addNewChunksKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkKey target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkKey)get_store().add_element_user(CHUNKSKEY$2);
            return target;
        }
    }
}
