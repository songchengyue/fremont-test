/*
 * XML Type:  IPRange
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/types
 * Java type: com.hp.schemas.ucmdb.discovery._1.types.IPRange
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.types.impl;
/**
 * An XML IPRange(@http://schemas.hp.com/ucmdb/discovery/1/types).
 *
 * This is a complex type.
 */
public class IPRangeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.types.IPRange
{
    
    public IPRangeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName START$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/types", "Start");
    private static final javax.xml.namespace.QName END$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/types", "End");
    
    
    /**
     * Gets the "Start" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.IP getStart()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IP target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().find_element_user(START$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Start" element
     */
    public void setStart(com.hp.schemas.ucmdb.discovery._1.types.IP start)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IP target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().find_element_user(START$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().add_element_user(START$0);
            }
            target.set(start);
        }
    }
    
    /**
     * Appends and returns a new empty "Start" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.IP addNewStart()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IP target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().add_element_user(START$0);
            return target;
        }
    }
    
    /**
     * Gets the "End" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.IP getEnd()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IP target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().find_element_user(END$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "End" element
     */
    public void setEnd(com.hp.schemas.ucmdb.discovery._1.types.IP end)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IP target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().find_element_user(END$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().add_element_user(END$2);
            }
            target.set(end);
        }
    }
    
    /**
     * Appends and returns a new empty "End" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.IP addNewEnd()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IP target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().add_element_user(END$2);
            return target;
        }
    }
}
