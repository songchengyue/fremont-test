package CMS_IP_BLOCK;
use Exporter;

use strict;
use warnings;
use CMS_IP_BLOCK_DB;
use CMS_ROUTING_QUEUE_DB;

sub start {
  if ((not defined($CMS::html_values{command}{value})) || 
     ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} =~ /cancel/i))) {
    # default to add_ip_block

    CMS::clear_html_values();
    $CMS::html_values{subsystem}{value} = "ipblock";
    $CMS::html_values{command}{value} = "add_ip_block";
  }

  if (defined $CMS::html_values{stage}{value}) {
    unless (CMS::check_screen($CMS::html_values{current_screen}{value})){
      $CMS::html_values{stage}{value}--;
    }
  }


  ######################## Add Reach IP Block ###########################

  if($CMS::html_values{command}{value} eq "add_ip_block") {
     # add a new ip block
     if (not defined ($CMS::html_values{stage}{value})) {
       CMS::output_html("ip_block_add", "html");
     } elsif ($CMS::html_values{stage}{value} == 2) {
       if ($CMS::html_values{cidr}{value} !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/) {
         CMS::add_error("Fail to add New IP Block.");
         CMS::add_error("Invalid CIDR format");
         CMS::output_html("ip_block_add", "html");
       } elsif(($CMS::html_values{cidr}{value} =~ /\/(.*?)$/) && (($1 > 24) || ($1 < 8)) ){
         CMS::add_error("Fail to add New IP Block.");
         CMS::add_error("Error - CIDR mask must be between /8 and /24.");
         CMS::output_html("ip_block_add", "html");
       } elsif (($CMS::html_values{cidr}{value} =~ /\.(.*?)\.(.*?)\.(.*?)\/(.*?)$/) && (($4 < 24) && ($4 > 16)) && (($2 % (2 ** (24-$4))) != 0)) {
         CMS::add_error("$3 Fail to add New IP Block.");
         CMS::add_error("Invalid CIDR format");
         CMS::output_html("ip_block_add", "html");
       } elsif(($CMS::html_values{cidr}{value} =~ /\.(.*?)\.(.*?)\.(.*?)\/(.*?)$/) && (($4 < 16) && ($4 > 8)) && (($1 % (2 ** (16-$4))) != 0)){
         CMS::add_error("Fail to add New IP Block.");
         CMS::add_error("Invalid CIDR format");
         CMS::output_html("ip_block_add", "html");
       } elsif ($CMS::html_values{cidr}{value} =~ /^(0\.|10\.|14\.|24\.|39\.|127\.|128\.0\.|169.254\.|191\.255\.|192\.0\.0\.|192\.0\.2\.|192\.168\.|255\.255\.255\.)|^(172\.(1[6-9]|2[0-9]|31)\.)|^(2(2[4-9]|3[0-9]|4[0-9]|5[0-5])\.)/) {
            CMS::add_error("Fail to add New IP Block.");
            CMS::add_error("Private / experimental addresses");
            CMS::output_html("ip_block_add", "html");
       } else {
         my $res = CMS_IP_BLOCK_DB::add_ip_block($CMS::html_values{cidr}{value});
         if ($res eq "1") {
           CMS::add_error("IP Block added successfully.");
         } else {
           CMS::add_error($res);
         }
         CMS::output_html("ip_block_add", "html");
       }
     }

  ######################## Delete Reach IP Block ###########################

  } elsif ($CMS::html_values{command}{value} eq "delete_ip_block") {
    if (not defined ($CMS::html_values{stage}{value}))  {
      $CMS::html_values{cidr}{options} = CMS_IP_BLOCK_DB::get_ip_block;
      unshift(@{$CMS::html_values{cidr}{options}}, [""]);
      CMS::output_html("ip_block_delete");
    } elsif ($CMS::html_values{stage}{value} == 2) {
      if ($CMS::html_values{cidr}{value} ne "") {
        my $res = CMS_IP_BLOCK_DB::delete_ip_block($CMS::html_values{cidr}{value});
        if ($res =~ /^1$/) {
          CMS::add_error("IP Block deleted successfully.");
        } else {
          CMS::add_error("Deletion failed.");
          CMS::add_error($res);
        }
      } 
      $CMS::html_values{cidr}{options} = CMS_IP_BLOCK_DB::get_ip_block;
      unshift(@{$CMS::html_values{cidr}{options}}, [""]);
      CMS::output_html("ip_block_delete");
    }

  ######################## List Reach IP Block ###########################

  } elsif ($CMS::html_values{command}{value} eq "list_ip_block") {
    # list out existing ip block
    $CMS::html_values{ip_block}{table} = CMS_IP_BLOCK_DB::get_ip_block;
    if (scalar @{$CMS::html_values{ip_block}{table}}) {
      unshift(@{$CMS::html_values{ip_block}{table}}, ["IP Block"]);
      $CMS::html_values{ip_block}{header_rows} = 1;
    } else {
      $CMS::html_values{noresultsmessage}{value} = "No entries.";
    }
    CMS::output_html("ip_block_list", "html");

  ######################## Add Bad Host ###########################
  
  } elsif ($CMS::html_values{command}{value} eq "add_bad_host") {

     # add a new bad host
     if (not defined ($CMS::html_values{stage}{value})) {
       CMS::output_html("bad_host_add");
     } elsif ($CMS::html_values{stage}{value} == 2) {
       if ($CMS::html_values{cidr}{value} !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/) {
          CMS::add_error("Fail to add New Bad Host.");
          CMS::add_error("Invalid CIDR format");
          CMS::output_html("bad_host_add", "html");
       } elsif (($CMS::html_values{cidr}{value} =~ /\/(.*?)$/) && (($1 > 24) || ($1 < 8)) ){
          CMS::add_error("Fail to add New Bad Host.");
          CMS::add_error("Error - CIDR mask must be between /8 and /24.");
          CMS::output_html("bad_host_add", "html");
       } elsif ( ($CMS::html_values{cidr}{value} =~ /\.(.*?)\.(.*?)\.(.*?)\/(.*?)$/) && (($4 < 24) && ($4 > 16)) && (($2 % (2 ** (24-$4))) != 0)){
          CMS::add_error("Fail to add New Bad Host.");
          CMS::add_error("Invalid CIDR format");
          CMS::output_html("bad_host_add", "html");
       } elsif(($CMS::html_values{cidr}{value} =~ /\.(.*?)\.(.*?)\.(.*?)\/(.*?)$/) && (($4 < 16) && ($4 > 8)) && (($1 % (2 ** (16-$4))) != 0)){
          CMS::add_error("Fail to add New Bad Host.");
          CMS::add_error("Invalid CIDR format");
          CMS::output_html("bad_host_add", "html");
       } elsif ( ($CMS::html_values{as_no}{value} < 0) || ($CMS::html_values{as_no}{value} =~ m/[a-z]|[A-Z]/) || (($CMS::html_values{as_no}{value} >= 64512) && ($CMS::html_values{as_no}{value} <= 65535))) {
          CMS::add_error("Failed to add New Bad Host.");
          CMS::add_error("Invalid AS number");
          CMS::output_html("bad_host_add", "html");
       } elsif ($CMS::html_values{cidr}{value} =~ /^(0\.|10\.|14\.|24\.|39\.|127\.|128\.0\.|169.254\.|191\.255\.|192\.0\.0\.|192\.0\.2\.|192\.168\.|255\.255\.255\.)|^(172\.(1[6-9]|2[0-9]|31)\.)|^(2(2[4-9]|3[0-9]|4[0-9]|5[0-5])\.)/) {
            CMS::add_error("Fail to add New Bad Host.");
            CMS::add_error("Private / experimental addresses");
            CMS::output_html("bad_host_add", "html");
       } else {
         my $res = CMS_IP_BLOCK_DB::add_bad_host($CMS::html_values{cidr}{value}, $CMS::html_values{as_no}{value});
         if ($res eq "1") {
           CMS::add_error("Bad host added successfully.");
         } else {
           CMS::add_error($res);
         } 
         CMS::output_html("bad_host_add");
       }
    }

  ######################## Delete Bad Host ###########################

  } elsif ($CMS::html_values{command}{value} eq "delete_bad_host") {
    if (not defined ($CMS::html_values{stage}{value}))  {
      my $bad_host_list = CMS_IP_BLOCK_DB::get_bad_host;
      foreach my $host (@{$bad_host_list}) {
        $host = ["$$host[0]|$$host[1]",  "$$host[0] ($$host[1])"];
      }

      $CMS::html_values{del_host}{options} = $bad_host_list;
      unshift(@{$CMS::html_values{del_host}{options}}, [""]);
      CMS::output_html("bad_host_delete");
    } elsif ($CMS::html_values{stage}{value} == 2) {
      if ($CMS::html_values{del_host}{value} ne "") {
        my ($cidr, $as_no) = split (/\|/, $CMS::html_values{del_host}{value});
        my $res = CMS_IP_BLOCK_DB::delete_bad_host($cidr, $as_no);
        if ($res =~ /^1$/) {
          CMS::add_error("Bad Host deleted successfully.");
        } else {
          CMS::add_error("Deletion failed.");
          CMS::add_error($res);
        }
      }
      my $bad_host_list = CMS_IP_BLOCK_DB::get_bad_host;
      foreach my $host (@{$bad_host_list}) {
        $host = ["$$host[0]|$$host[1]",  "$$host[0] ($$host[1])"];
      }

      $CMS::html_values{del_host}{options} = $bad_host_list;
      unshift(@{$CMS::html_values{del_host}{options}}, [""]);
      CMS::output_html("bad_host_delete");
    }

  ######################## List Bad Host ###########################

  } elsif ($CMS::html_values{command}{value} eq "list_bad_host") {
    # list out existing bad hosts
    $CMS::html_values{bad_host}{table} = CMS_IP_BLOCK_DB::get_bad_host;
    if (scalar @{$CMS::html_values{bad_host}{table}}) {
      unshift(@{$CMS::html_values{bad_host}{table}}, ["Bad Host", "AS No.", "Time Entered"]);
      $CMS::html_values{bad_host}{header_rows} = 1;
    } else {
      $CMS::html_values{noresultsmessage}{value} = "No entries.";
    }
    CMS::output_html("bad_host_list", "html");

  ######################## Search Bad Routes ###########################

  } elsif ($CMS::html_values{command}{value} eq "search_bad_route") {
    if ((defined $CMS::html_values{submit}{value}) &&
        ($CMS::html_values{submit}{value} eq "Search")) {

       if ($CMS::html_values{search_ip_block}{value} !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/) {
         CMS::add_error("Invalid CIDR format");
         CMS::output_html("list_bad_route", "html");
       } else {
         $CMS::html_values{bad_route}{table} = CMS_ROUTING_QUEUE_DB::get_bad_route($CMS::html_values{search_ip_block}{value});
         if (scalar @{$CMS::html_values{bad_route}{table}}) {
           $CMS::html_values{noresultsmessage}{value} = "";
           unshift(@{$CMS::html_values{bad_route}{table}}, 
             ["Tracking Number", "Service ID", "Action", "Route", "AS No", "Time Entered", "Time Completed"]);

            $CMS::html_values{bad_route}{header_rows} = 1;
          } else {
            $CMS::html_values{noresultsmessage}{value} = "No entries.";
         }
      }
    } else {
      $CMS::html_values{search_ip_block}{value} = "";
    }
    CMS::output_html("list_bad_route", "html");
  }

}

1
