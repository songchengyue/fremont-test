/*
 * XML Type:  PredefinedTypedProperties
 * Namespace: http://schemas.hp.com/ucmdb/1/types/props
 * Java type: com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.props.impl;
/**
 * An XML PredefinedTypedProperties(@http://schemas.hp.com/ucmdb/1/types/props).
 *
 * This is a complex type.
 */
public class PredefinedTypedPropertiesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties
{
    
    public PredefinedTypedPropertiesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUALIFIERPROPERTIES$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "qualifierProperties");
    private static final javax.xml.namespace.QName SIMPLETYPEDPREDEFINEDPROPERTIES$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "simpleTypedPredefinedProperties");
    
    
    /**
     * Gets the "qualifierProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.QualifierProperties getQualifierProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.QualifierProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.QualifierProperties)get_store().find_element_user(QUALIFIERPROPERTIES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "qualifierProperties" element
     */
    public boolean isSetQualifierProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(QUALIFIERPROPERTIES$0) != 0;
        }
    }
    
    /**
     * Sets the "qualifierProperties" element
     */
    public void setQualifierProperties(com.hp.schemas.ucmdb._1.types.props.QualifierProperties qualifierProperties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.QualifierProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.QualifierProperties)get_store().find_element_user(QUALIFIERPROPERTIES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.QualifierProperties)get_store().add_element_user(QUALIFIERPROPERTIES$0);
            }
            target.set(qualifierProperties);
        }
    }
    
    /**
     * Appends and returns a new empty "qualifierProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.QualifierProperties addNewQualifierProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.QualifierProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.QualifierProperties)get_store().add_element_user(QUALIFIERPROPERTIES$0);
            return target;
        }
    }
    
    /**
     * Unsets the "qualifierProperties" element
     */
    public void unsetQualifierProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(QUALIFIERPROPERTIES$0, 0);
        }
    }
    
    /**
     * Gets the "simpleTypedPredefinedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection getSimpleTypedPredefinedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection)get_store().find_element_user(SIMPLETYPEDPREDEFINEDPROPERTIES$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "simpleTypedPredefinedProperties" element
     */
    public boolean isSetSimpleTypedPredefinedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SIMPLETYPEDPREDEFINEDPROPERTIES$2) != 0;
        }
    }
    
    /**
     * Sets the "simpleTypedPredefinedProperties" element
     */
    public void setSimpleTypedPredefinedProperties(com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection simpleTypedPredefinedProperties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection)get_store().find_element_user(SIMPLETYPEDPREDEFINEDPROPERTIES$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection)get_store().add_element_user(SIMPLETYPEDPREDEFINEDPROPERTIES$2);
            }
            target.set(simpleTypedPredefinedProperties);
        }
    }
    
    /**
     * Appends and returns a new empty "simpleTypedPredefinedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection addNewSimpleTypedPredefinedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection)get_store().add_element_user(SIMPLETYPEDPREDEFINEDPROPERTIES$2);
            return target;
        }
    }
    
    /**
     * Unsets the "simpleTypedPredefinedProperties" element
     */
    public void unsetSimpleTypedPredefinedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SIMPLETYPEDPREDEFINEDPROPERTIES$2, 0);
        }
    }
}
