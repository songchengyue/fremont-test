/*
 * An XML document type.
 * Localname: ImpactPathTopology
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactPathTopologyDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * A document containing one ImpactPathTopology(@http://schemas.hp.com/ucmdb/1/types/impact) element.
 *
 * This is a complex type.
 */
public class ImpactPathTopologyDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactPathTopologyDocument
{
    
    public ImpactPathTopologyDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTPATHTOPOLOGY$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "ImpactPathTopology");
    
    
    /**
     * Gets the "ImpactPathTopology" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactTopology getImpactPathTopology()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactTopology target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactTopology)get_store().find_element_user(IMPACTPATHTOPOLOGY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ImpactPathTopology" element
     */
    public void setImpactPathTopology(com.hp.schemas.ucmdb._1.types.impact.ImpactTopology impactPathTopology)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactTopology target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactTopology)get_store().find_element_user(IMPACTPATHTOPOLOGY$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactTopology)get_store().add_element_user(IMPACTPATHTOPOLOGY$0);
            }
            target.set(impactPathTopology);
        }
    }
    
    /**
     * Appends and returns a new empty "ImpactPathTopology" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactTopology addNewImpactPathTopology()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactTopology target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactTopology)get_store().add_element_user(IMPACTPATHTOPOLOGY$0);
            return target;
        }
    }
}
