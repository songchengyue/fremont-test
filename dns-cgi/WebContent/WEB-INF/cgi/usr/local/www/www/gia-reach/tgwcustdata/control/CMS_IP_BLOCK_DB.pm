package CMS_IP_BLOCK_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;

our $error;

sub add_ip_block {
  my $ip_block = $dbh->quote(shift @_);

  my $sth = $dbh->prepare("INSERT INTO reach_ip_block (ip_block) VALUES ($ip_block)");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return $error;
  }
  return "1";
}

sub get_ip_block {
  
  my $sth = $dbh->prepare("SELECT ip_block FROM reach_ip_block ORDER BY ip_block");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  
  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}

sub delete_ip_block {
  my $ip_block = $dbh->quote(shift @_);
  
  my $sth = $dbh->prepare("DELETE FROM reach_ip_block WHERE ip_block = $ip_block");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return $error;
  }

  return "1";
}

sub add_bad_host {
  my $ip_block = $dbh->quote(shift @_);
  my $as = $dbh->quote(shift @_);

  my $sth = $dbh->prepare("INSERT INTO bad_host_list (ip_block, as_no, time_entered) VALUES ($ip_block, $as, CURRENT_TIMESTAMP)");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return $error;
  }
  return "1";
}

sub delete_bad_host {
  my $ip_block = $dbh->quote(shift @_);
  my $as = $dbh->quote(shift @_);
  
  my $sth = $dbh->prepare("DELETE FROM bad_host_list WHERE ip_block = $ip_block and as_no = $as");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return $error;
  }

  return "1";
}

sub get_bad_host {
 
  my $sth = $dbh->prepare("SELECT ip_block, as_no, time_entered FROM bad_host_list ORDER BY ip_block, as_no");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
 
  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}

1;
