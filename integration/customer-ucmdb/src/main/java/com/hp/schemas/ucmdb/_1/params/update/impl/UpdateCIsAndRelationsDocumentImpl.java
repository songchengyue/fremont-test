/*
 * An XML document type.
 * Localname: updateCIsAndRelations
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * A document containing one updateCIsAndRelations(@http://schemas.hp.com/ucmdb/1/params/update) element.
 *
 * This is a complex type.
 */
public class UpdateCIsAndRelationsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsDocument
{
    
    public UpdateCIsAndRelationsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName UPDATECISANDRELATIONS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "updateCIsAndRelations");
    
    
    /**
     * Gets the "updateCIsAndRelations" element
     */
    public com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations getUpdateCIsAndRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations)get_store().find_element_user(UPDATECISANDRELATIONS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "updateCIsAndRelations" element
     */
    public void setUpdateCIsAndRelations(com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations updateCIsAndRelations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations)get_store().find_element_user(UPDATECISANDRELATIONS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations)get_store().add_element_user(UPDATECISANDRELATIONS$0);
            }
            target.set(updateCIsAndRelations);
        }
    }
    
    /**
     * Appends and returns a new empty "updateCIsAndRelations" element
     */
    public com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations addNewUpdateCIsAndRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelations)get_store().add_element_user(UPDATECISANDRELATIONS$0);
            return target;
        }
    }
}
