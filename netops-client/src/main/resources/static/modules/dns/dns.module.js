(function(){
angular.module('netops.dnsModule', ['ngRoute', 'netops.utilModule']);

angular.module('netops.dnsModule')

.config(function (moduleRegistryProvider) {
	moduleRegistryProvider.register({ name: 'dns', baseUrl: '/dns-server-1.0/'});	
})

.controller('PrimaryDnsAddController',function($scope,$http,Data,$rootScope,$location,$routeParams) {
	 $scope.name = "PrimaryDnsAddController";
	 $scope.params = $routeParams;
 
	  // service id start

     function _getServices(productLine) { 
    	 
         $scope.Product=productLine.serviceCode;//added to fetcjh the product line by uday
         console.log('DNS product line is ' + $scope.Product);
         console.log( $scope.Product);

         $http.get('/dns-server-1.0/service?productLine=' + $scope.Product)
         .then(function (response) {
                   // this callback will be called asynchronously
                   // when the response is available
                      console.log(response.data);
                      console.log(response.status);
                      
                      $scope.services = response.data;

                 },  function (response) {
               		// called asynchronously if an error occurs
                	    // or server returns response with an error status.
         				$scope.loading=false;
                		console.log("An error encountered"+response);
                		if (response.status == 422) {
              			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
                   		console.log('err from server ');	             		 
                   		$location.path('/ServerError');
                		} else {
                			Data.setErrorCode(response.status);
                 			console.log('after location');
                 			$location.path('/ServerError');  
                		}
                   
                 });
         }
     
     $scope.getServices = function() { 
                
            if (Data.getProductLine() != null) {
            	console.log('DNS product line - available immediately!');
            	_getServices(Data.getProductLine());
            } else {
            	console.log('DNS product line - we must wait!');
            	Data.getProductLineAsync().then(function (productLine) {
            		_getServices(productLine);
            	});
     		}
            
     }
            
          // service id end
     
//fuction to clear all the data on the screen once the user hits reset.
     $scope.reset = function()
		{
    	 
    	 $scope.user = {};
    	 $scope.user.contactEmailAddress="";
    	 $scope.myForm.$setPristine();
    	
		}
     ///added on 20june.
   
 $scope.clearRecordType=function(){
    	 $scope.user.aRecordIpV4Address="";
    	 $scope.user.aRecordIpV6Address="";
    	 $scope.user.cnameRecordName="";
    	 $scope.user.mxRecordPrefix="";
    	 $scope.user.primarymxpv="";
    	 $scope.user.nsRecordServer="";
    	 $scope.user.textRecordText="";
     }
     
     ///added on 20june.
     ///to pass the data entered from the front end to the back end.
     $scope.submitDNSForm = function(){
    	 $scope.loading=true;
         	  //http call to backend
    	 $http.post('/dns-server-1.0/primary',$scope.user)
    	 .then(function (response) {
         	    // this callback will be called asynchronously
         	    // when the response is available
    		 //console.log(user.serviceid);
    		console.log(response.data);
			$scope.loading=false;
      		var result=response.data.message;
     		console.log(result);
     		Data.setSuccessCode(result);
     		$location.path('/Success');
      	  }, function (response) {
      		// called asynchronously if an error occurs
       	    // or server returns response with an error status.
				$scope.loading=false;
       		console.log("An error encountered"+response.status);
       		if (response.status == 422) {
     			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
          		console.log('err from server ');	             		 
          		$location.path('/ServerError');
       		} else {
       			Data.setErrorCode(response.status);
        			console.log('after location');
        			$location.path('/ServerError');  
       		}
           		
         	    
         	 });
       }
 })
 

 .controller('PrimaryDnsViewController',function($scope,$http,Data,$location,$routeParams){
	 $scope.name = 'PrimaryDnsViewController';
	 $scope.params = $routeParams;
	 console.log("in PrimaryDnsViewController");
	  var array=[];
	  $scope.arrayToDelete;

	  $scope.checked=0;
	 $scope.message;

	 $scope.viewdetails=function(){
         $scope.loading=true;
         $http.get('/dns-server-1.0/primary')
         .then(function (response) {
                     $scope.loading=false;
                     console.log(response.data);   
               if (response.data.length <= 0){
                     
                     $scope.error="There are no records to be displayed";
                    
         }else{
               $scope.table = true;
               $scope.dnsRecords=response.data;
               console.log($scope.dnsRecords);
               console.log(response.data[0].Contact);
               
         }
     }, function (response) {
         // called asynchronously if an error occurs
             // or server returns response with an error status.
               //console.log("An error encountered"+response);
               //$scope.error="There are no records to be displayed";
               Data.setErrorFromServer(response.status);
               $location.path('/ServerError');
               $scope.loading=false; 
     });
  
    }

	  
	 $scope.addToList = function($event,id) {
         $scope.id=id;
         // alert('id'+id);
   
         console.log("addToList(): id: " + id);
         console.log($event.target.checked+'checkbox checked ');
         if($event.target.checked==true){
               array.push(id); 
               arrayToDelete=array;
               //alert(arrayToDelete+'final');
               console.log("final list add list");
               console.log(arrayToDelete);
               $scope.checked=arrayToDelete.length;
               console.log(' checked'+ $scope.checked);

         }
         else {
               arrayToDelete=$scope.removeUncheckedData(array,id);
               //alert(arrayToDelete+'final');
                     console.log("final list remove in else");
                     console.log(arrayToDelete);
               $scope.checked=arrayToDelete.length;
               console.log(' checked'+ $scope.checked);

         }

        
      };

	  
      $scope.removeUncheckedData = function(array,id) {
 
          console.log(id+'to be deleted');
    for (var i=array.length-1; i>=0; i--) {
           if (array[i] === id) {
             //alert('insearch');
               array.splice(i, 1);
              // alert(array);
               return array;
               // break;       //<-- Uncomment  if only the first term has to be removed
           }
       }
    }

      $scope.deleteRecords = function() {
          $scope.loading=true;
          console.log("in delete record ajax call");
          console.log("going for deletion to spring"+arrayToDelete);
          $http.delete('/dns-server-1.0/primary?ids=' + arrayToDelete.join())
             .then(function (response) {
                   var result=response.data.message;   
                   console.log(result);
             Data.setSuccessCode(result);
             $location.path('/Success');
               }, function (response) {
                   console.log(response.data);
                   if (response.status == 422) {
                   Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
                   console.log('err from server ');                             
                   $location.path('/ServerError');
             } else {
                   Data.setErrorFromServer(response.status);
                   console.log('after location');
                   $location.path('/ServerError');  
             } 
               });
    
         };

         $scope.clearAll = function() {      
             //alert('hi');
             arrayToDelete.length=0;  //removing the values from the array if there are any before hitting clear
             $scope.checked=0;//setting this to zero so that delete and clear buttons are not visible until atleast one record is checked.
           // alert('checked'+checked);
             console.log('checked in clear'+$scope.checked);

             };
    


})

 
 
 
.controller('SecondaryDnsAddController', function($scope,$http,Data,$location, $rootScope,$routeParams) {
     $scope.name = "SecondaryDnsAddController";
     $scope.params = $routeParams;
     $scope.reset = function()
     {
    	$scope.user = {};
    	//user.contactEmailAddress="";
    	$scope.user.contactEmailAddress="";
    	 //$scope.myForm={};
     $scope.myForm.$setPristine();
    	
     }
     
     // service id start

     function _getServices(productLine) { 
    	 
         $scope.Product=productLine.serviceCode;//added to fetcjh the product line by uday
         console.log('DNS product line is ' + $scope.Product);
         console.log( $scope.Product);

         $http.get('/dns-server-1.0/service?productLine=' + $scope.Product)
         .then(function (response) {
                   // this callback will be called asynchronously
                   // when the response is available
                      console.log(response.data);
                      console.log(response.status);
                      
                      $scope.services = response.data;

                 },  function (response) {
               		// called asynchronously if an error occurs
                	    // or server returns response with an error status.
         				$scope.loading=false;
                		console.log("An error encountered"+response);
                		if (response.status == 422) {
              			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
                   		console.log('err from server ');	             		 
                   		$location.path('/ServerError');
                		} else {
                			Data.setErrorCode(response.status);
                 			console.log('after location');
                 			$location.path('/ServerError');  
                		}
                   
                 });
         }
     
     $scope.getServices = function() { 
                
            if (Data.getProductLine() != null) {
            	console.log('DNS product line - available immediately!');
            	_getServices(Data.getProductLine());
            } else {
            	console.log('DNS product line - we must wait!');
            	Data.getProductLineAsync().then(function (productLine) {
            		_getServices(productLine);
            	});
     		}
            
     }
            
          // service id end
     
     ///to pass the data entered from the front end to the back end.
     $scope.submitSecondaryServerForm = function(){
    	 $scope.loading=true;
    	 console.log($scope.user.domain);
    	 $http.post('/dns-server-1.0/secondary',$scope.user)
    	 .then(function (response) {
         	    // this callback will be called asynchronously
         	    // when the response is available
				$scope.loading=false; // this callback will be called asynchronously
        	    // when the response is available
        		console.log(response.data);
				var result=response.data.message;
        		console.log(result);
        		Data.setSuccessCode(result);
        		$location.path('/Success');         		
        	}, function (response) {
         		// called asynchronously if an error occurs
           	    // or server returns response with an error status.
        		 if (response.status == 422) {
	        			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
	             		console.log('err from server ');	             		 
	             		$location.path('/ServerError');
	          		} else {
	          			Data.setErrorFromServer(response.status);
	           			console.log('after location');
	           			$location.path('/ServerError');  
	          		} 
         	    
         	  });
       }   
     
 })

 

 .controller('SecondaryDnsViewAndRemoveController',function($scope,$http,Data,$location,$rootScope,$routeParams){
	 
	$scope.name = 'SecondaryDnsViewAndRemoveController';
	 $scope.params = $routeParams;
	 
	 /**/
	 
	 var init=function(){
	     ///to pass the data entered from the front end to the back end.
	     
	      	 //http call to backend
			 $http.get('/dns-server-1.0/secondary')
			 .then(function (response) {
					$scope.loading=false;
	     		if (response.data.length<=0){
	     			$scope.error="There are no records to be displayed";
	     			//todo --- response.data will be empty as its an array 
	     			
	      			/*Data.setErrorFromServer(response.data.result[0].Error);//this sets the err code/message in the service so that it can be displayed on the screen.
						console.log('error from cgi-server ');
						$location.path('/ServerError');*/
	  		}else{
	  			$scope.table=true;
	  			$scope.demos=response.data;
		         console.log($scope.demos);
		         	
		         	
	  		}
	  	  }, function (response) {
	  		// called asynchronously if an error occurs
	    	    // or server returns response with an error status.
	    		console.log("An error encountered"+response);
	    		$scope.error="There are no records to be displayed";
	    		Data.setErrorFromServer(response.status);
				$scope.loading=false; 
	  	  });
		
			
		 }
		init();  
		 
	 
	 
	 /**/
	 
	 
	 
	 //alert($rootScope.custid+'id');
	 
	  //var array=[];
	  //$scope.arrayToDelete;
	 $scope.valueToDelete;
	  //$scope.checked=0;
	 $scope.message;
	 $scope.loading=true;
	 $scope.user = {};
	 $scope.sendToBackendForDeletion = function(){
	 $http.delete('/dns-server-1.0/secondary?ids=' +$scope.valueToDelete+'&domain='+$scope.domain)//secondaryRemove
	 .then(function (response) {
		 console.log(response.data);
			$scope.loading=false;
  		var result=response.data.message;
 		console.log(result);
 		Data.setSuccessCode(result);
 		$location.path('/Success');
 	  }, function (response) {
 		  $scope.loading=false;
       		console.log("An error encountered"+response);
       		if (response.status == 422) {
     			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
          		console.log('err from server ');	             		 
          		$location.path('/ServerError');
       		} else {
       			//$scope.table=true;
       			Data.setErrorFromServer(response.status);
        			console.log('after location');
        			$location.path('/ServerError');  
       		}    
         	  });
	 };
       //
	
$scope.addToList = function($event,id,domain) {
		 
		 console.log($event.target.checked)
		 if($event.target.checked==true){
			//change made for view page single record
			 $scope.valueToDelete=id;
			 $scope.domain=domain;
			 console.log($scope.valueToDelete+'add:valueToDelete'+ $scope.domain+'add:domain');
			 //alert($scope.valueToDelete);
			 $scope.checked=true;
		 }
		
		
	     
	   };
	  

		  $scope.clearAll = function() {      
			  //alert('hi');
			  $scope.valueToDelete='';  //change made for view page single record
			  $scope.domain='';
			  $scope.checked=false; //change made for view page single record
			  console.log($scope.valueToDelete+'valueToDelete'+ $scope.domain+'domain');
			 // alert('checked'+checked);
			  };
	 
	 
 })
 

 .controller('DnsResolverAddController',function($scope,$http,Data,$location) {
	 $scope.name = 'DnsResolverAddController';
	 //$scope.params = $routeParams;
	// service id start

     function _getServices(productLine) { 
    	 
         $scope.Product=productLine.serviceCode;//added to fetcjh the product line by uday
         console.log('DNS product line is ' + $scope.Product);
         console.log( $scope.Product);

         $http.get('/dns-server-1.0/service?productLine=' + $scope.Product)
         .then(function (response) {
                   // this callback will be called asynchronously
                   // when the response is available
                      console.log(response.data);
                      console.log(response.status);
                      
                      $scope.services = response.data;

                 },  function (response) {
               		// called asynchronously if an error occurs
                	    // or server returns response with an error status.
         				$scope.loading=false;
                		console.log("An error encountered"+response);
                		if (response.status == 422) {
              			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
                   		console.log('err from server ');	             		 
                   		$location.path('/ServerError');
                		} else {
                			Data.setErrorCode(response.status);
                 			console.log('after location');
                 			$location.path('/ServerError');  
                		}
                   
                 });
         }
     
     $scope.getServices = function() { 
                
            if (Data.getProductLine() != null) {
            	console.log('DNS product line - available immediately!');
            	_getServices(Data.getProductLine());
            } else {
            	console.log('DNS product line - we must wait!');
            	Data.getProductLineAsync().then(function (productLine) {
            		_getServices(productLine);
            	});
     		}
            
     }
            
          // service id end
	 //resetting the form fields
	 $scope.resetPage = function()
     {
         
       $scope.user = {};
       $scope.user.contactEmailAddress="";
       $scope.user.ipAddress="";
       $scope.user.contactEmailAddressIpv6="";
       $scope.user.ipAddressIpv6="";
       $scope.myFormipv4.$setPristine();
       $scope.myFormipv6.$setPristine();
       
     }

               

	 var submit = function(){
	 //rename this scope as submitDNSResolverFormIpV4
		 $scope.loading=true;
     	  //http call to backend
		 console.log($scope.user.serviceid);//added to check if service id is getting fectched
		 $http.post('/dns-server-1.0/resolver',$scope.user)
        	.then(function (response) {
        	    // this callback will be called asynchronously
        	    // when the response is available
        		console.log(response.data);
				$scope.loading=false;
         		var result=response.data.message;
        		console.log(result);
        		Data.setSuccessCode(result);
        		$location.path('/Success');
        	  }, function (response) {
        		  $scope.loading=false;
	          		console.log("An error encountered"+response);
	          		if (response.status == 422) {
	          			console.log("Anerror"+response.data.message);
	        			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
	             		console.log('err from server ');	             		 
	             		$location.path('/ServerError');
	          		} else {
	          			//$scope.table=true;
	          			Data.setErrorFromServer(response.status);
	           			console.log('after location');
	           			$location.path('/ServerError');  
	          		}
        	  });
      }
	  $scope.submitipv4 = function(){
		   $scope.user.ipVersion= "IPv4";
		   submit();
	   }
	     ///to pass the data entered from the front end to the back end.
	   $scope.submitipv6 = function(){
		   $scope.user.ipVersion= "IPv6";
		   submit();
	   }
	   var cancel = function(){
		 $scope.loading=true;
    	  //http call to backend
		 console.log("inside cancel");
		 $http.post('/dns-server-1.0/resolvercancel',$scope.user)
		 .then(function (response) {
       	    // this callback will be called asynchronously
       	    // when the response is available
			 console.log(response.data);
				$scope.loading=false;
      		var result=response.data.message;
     		console.log(result);
     		Data.setSuccessCode(result);
     		$location.path('/Success');
			 
       	  }, function (response) {
       		 $scope.loading=false;
       		 console.log("An error encountered"+response);
       		if (response.status == 422) {
     			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
          		console.log('err from server ');	             		 
          		$location.path('/ServerError');
       		} else {
       				Data.setErrorFromServer(response.status);
        			console.log('after location');
        			$location.path('/ServerError');  
       		}
       	  });
     }
	   $scope.cancelipv4 = function(){
		   $scope.user.ipVersion= "IPv4";
		   cancel();
	   }
	   $scope.cancelipv6 = function(){
		   $scope.user.ipVersion= "IPv6";
		   cancel();
	   }
	 
    
    
 })
 .controller('DnsResolverViewController', function($scope,$http,Data,$location, $rootScope,$routeParams) {
     $scope.name = "DnsResolverViewController";
     $scope.params = $routeParams;
	 $scope.loading=true;
     //alert('in viewdnscontroller');
   
	 var init = function () {
	     	  $scope.loading=true;
			      $http.get('/dns-server-1.0/resolver')
			      .then(function (response) {
						$scope.loading=false;
		        		if (response.data.length <=0){
		        			
		        			//todo --- response.data will be empty as its an array 
		        			$scope.error="There are no records to be displayed";
		         			/*Data.setErrorFromServer(response.data.result[0].Error);//this sets the err code/message in the service so that it can be displayed on the screen.
							console.log('error from cgi-server ');
							$location.path('/ServerError');*/
	         		}else{
	         			$scope.table =true;
	         			$scope.viewrecords=response.data;
			         	console.log($scope.viewrecords);
			         	console.log(response.data[0].Contact);
			         	
	         		}
	         	  }, function (response) {
	         		// called asynchronously if an error occurs
	           	    // or server returns response with an error status.
	           		console.log("An error encountered"+response);
	           		Data.setErrorFromServer(response.status);
	           		$location.path('/ServerError');
					$scope.loading=false; 
	         	  });
	      	
	         	 //////end
	       }
	init();     

   
 })

 
.controller('ReverseDelegationAndMappingViewController',function($scope,$http,Data,$location, $rootScope,$routeParams) {
     $scope.name = "ReverseDelegationAndMappingViewController";
     $scope.params = $routeParams;
   	 var init = function () {
     	  $scope.loading=true;
	     	  //http call to backend
		      $http.get('/dns-server-1.0/reversedns')
		      .then(function (response) {
					$scope.loading=false;
	        		if (response.data.length <=0){
	        			 
	        			$scope.error ="There is No Records to be Displayed.";
	         			
         		}else{
         			$scope.table=true;
         			$scope.views=response.data;
		         	console.log($scope.views);
		         	console.log(response.data[0].Contact);
		         	
         		}
         	  }, function (response) {
         		// called asynchronously if an error occurs
           	    // or server returns response with an error status.
           		console.log("An error encountered"+response);
           		Data.setErrorFromServer(response.status);
           		$location.path('/ServerError');
				$scope.loading=false; 
         	  });
      	
         	 //////end
       }
init();     
   
 })
 
 
 
.controller('ReverseMappingAddController', function($scope,$rootScope,$http,Data,$location,$routeParams) {
	   $scope.name = 'ReverseMappingAddController';
	 
	  
	   
	   //for resetting the form fields
	   $scope.resetPage = function()
       {
           
         $scope.user = {};
         $scope.user.contactEmailAddress="";
       
         $scope.user.contactEmailAddressIpv6="";
         
         $scope.myFormipv4.$setPristine();
         $scope.myFormipv6.$setPristine();
         
       }
	   
	   
	// service id start

	     function _getServices(productLine) { 
	    	 
	         $scope.Product=productLine.serviceCode;//added to fetcjh the product line by uday
	         console.log('DNS product line is ' + $scope.Product);
	         console.log( $scope.Product);

	         $http.get('/dns-server-1.0/service?productLine=' + $scope.Product)
	         .then(function (response) {
	                   // this callback will be called asynchronously
	                   // when the response is available
	                      console.log(response.data);
	                      console.log(response.status);
	                      
	                      $scope.services = response.data;

	                 },  function (response) {
	               		// called asynchronously if an error occurs
	                	    // or server returns response with an error status.
	         				$scope.loading=false;
	                		console.log("An error encountered"+response);
	                		if (response.status == 422) {
	              			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
	                   		console.log('err from server ');	             		 
	                   		$location.path('/ServerError');
	                		} else {
	                			Data.setErrorCode(response.status);
	                 			console.log('after location');
	                 			$location.path('/ServerError');  
	                		}
	                   
	                 });
	         }
	     
	     $scope.getServices = function() { 
	                
	            if (Data.getProductLine() != null) {
	            	console.log('DNS product line - available immediately!');
	            	_getServices(Data.getProductLine());
	            } else {
	            	console.log('DNS product line - we must wait!');
	            	Data.getProductLineAsync().then(function (productLine) {
	            		_getServices(productLine);
	            	});
	     		}
	            
	     }
	            
	          // service id end

	   //submitting to back-end
	   var submit = function(){
	    	 $scope.loading=true;
	    	 $http.post('/dns-server-1.0/reversemapping',$scope.user)
	    	 .then(function (response) {
	         	    // this callback will be called asynchronously
	         	    // when the response is available
	    		console.log(response.data);
				$scope.loading=false;
	      		var result=response.data.successMessage;
	     		console.log(result);
	     		Data.setSuccessCode(result);
	     		$location.path('/Success');
	      	  }, function (response) {
	      		// called asynchronously if an error occurs
	       	    // or server returns response with an error status.
					$scope.loading=false;
	       		console.log("An error encountered"+response);
	       		if (response.status == 422) {
	     			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
	          		console.log('err from server ');	             		 
	          		$location.path('/ServerError');
	       		} else {
	       			Data.setErrorFromServer(response.status);
	        			console.log('after location');
	        			$location.path('/ServerError');  
	       		}
	           		
	         	    
	         	 });
	      	 
	       }
	   $scope.requestservicemappingipv4 = function(){
		   $scope.user.ipVersion= "IPv4";
		   submit();
	   }
	   
	   $scope.requestservicemappingIPv6 = function(){ 
		   $scope.user.ipVersion= "IPv6";
		   submit();
	   }

	     var cancel= function(){
		 		$scope.loading=true;
		 		$http.post('/dns-server-1.0/reversemappingCancel',$scope.user)
				 .then(function (response) {
			       	    // this callback will be called asynchronously
			       	    // when the response is available
						 console.log(response.data);
							$scope.loading=false;
			      		var result=response.data.successMessage;
			     		console.log(result);
			     		Data.setSuccessCode(result);
			     		$location.path('/Success');
						 
			       	  }, function (response) {
			       		 $scope.loading=false;
			       		 console.log("An error encountered"+response.data);
			       		if (response.status == 422) {
			     			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
			          		console.log('err from server ');	             		 
			          		$location.path('/ServerError');
			       		} else {
			       			Data.setErrorFromServer(response.status);
			        			console.log('after location');
			        			$location.path('/ServerError');  
			       		}
			       	  });
		     }
	     $scope.cancelserviceipv4 = function(){
			   $scope.user.ipVersion= "IPv4";
			   cancel();
		   }
		   $scope.cancelserviceipv6 = function(){
			   $scope.user.ipVersion= "IPv6";
			   cancel();
		   }
	     
	   
 })   
 .controller('RemoveSecondaryDNSController',['$scope','$rootScope','$http','Data','$location', function($scope,$rootScope,$http,Data,$location,$routeParams) {
 //data to be fetched from back end.   
$scope.name = 'RemoveSecondaryDNSController';
	 $scope.params = $routeParams;
	 $scope.cancelservice = function()
     {
    	 $scope.user = {};
    	 $scope.myForm.$setPristine();
    	 
     }
	 $scope.RemoveSecondaryServerRecords = function(){
	 
	   var obj={
				 contactEmailAddress	:"",
    			 domainName				:$scope.user.domain,
    			 primaryServerIpAddress	:""
				} ;
		 $scope.loading=true;
	   console.log(RemoveRecords);
	  $http({
         	   method	   : 'DELETE',
         	   url		   : '/dns-server-1.0/secondary',
         	   data		   : JSON.stringify(obj)
   	}).then(function (data) {
   	    // this callback will be called asynchronously
   	    // when the response is available
		$scope.loading=false;
        if (response.data.error){
         	//$scope.error=data.data.error;
         	Data.setErrorCode(response.data.result[0].Error);//this sets the err code/message in the service so that it can be displayed on the screen.
     		console.log('err from server ');
     		$location.path('/ServerError');
   		}else{
 			console.log(data+'a');
     		Data.setSuccessCode(response.data.result[0].Success_message);//set waht ever success message you want to display
      		console.log('after location');
      		$location.path('/Success');
   		}
   	  }, function (error) {
   		// called asynchronously if an error occurs
     	    // or server returns response with an error status.
     		console.log("An error encountered"+error);
    
     		Data.setErrorCode(error.status);
     		console.log('after location');
     		 $scope.loading=false;
   		$location.path('/Error');
   	    
   	  });
 
	  
	 } }]) 
	

 
.controller('ReverseDelegationAddController', function($scope,$rootScope,$http,Data,$location,$routeParams) {
	   $scope.name = 'ReverseDelegationAddController';
	  
	   
	   //resetting the form fields
	   $scope.resetPage = function()
       {
         $scope.user ={}; 
         $scope.user.contactEmailAddress="";
         $scope.user.ipBlock="";
         $scope.user.contactEmailAddressIpv6="";
         $scope.user.ipBlockIpv6="";
         $scope.myFormipv4.$setPristine();
         $scope.myFormipv6.$setPristine();
         $scope.user.nameServer="";
         
       }
	   
	// service id start

	     function _getServices(productLine) { 
	    	 
	         $scope.Product=productLine.serviceCode;//added to fetcjh the product line by uday
	         console.log('DNS product line is ' + $scope.Product);
	         console.log( $scope.Product);

	         $http.get('/dns-server-1.0/service?productLine=' + $scope.Product)
	         .then(function (response) {
	                   // this callback will be called asynchronously
	                   // when the response is available
	                      console.log(response.data);
	                      console.log(response.status);
	                      
	                      $scope.services = response.data;

	                 },  function (response) {
	               		// called asynchronously if an error occurs
	                	    // or server returns response with an error status.
	         				$scope.loading=false;
	                		console.log("An error encountered"+response);
	                		if (response.status == 422) {
	              			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
	                   		console.log('err from server ');	             		 
	                   		$location.path('/ServerError');
	                		} else {
	                			Data.setErrorCode(response.status);
	                 			console.log('after location');
	                 			$location.path('/ServerError');  
	                		}
	                   
	                 });
	         }
	     
	     $scope.getServices = function() { 
	                
	            if (Data.getProductLine() != null) {
	            	console.log('DNS product line - available immediately!');
	            	_getServices(Data.getProductLine());
	            } else {
	            	console.log('DNS product line - we must wait!');
	            	Data.getProductLineAsync().then(function (productLine) {
	            		_getServices(productLine);
	            	});
	     		}
	            
	     }
	            
	          // service id end
	             
	             
	     
	     ///to pass the data entered from the front end to the back end.
	   
	   var submit = function(){
	    	 $scope.loading=true;	     
	    	 
	    	 $http.post('/dns-server-1.0/reversedelegation',$scope.user)
	        	.then(function (response) {
	        	    // this callback will be called asynchronously
	        	    // when the response is available
	        		console.log(response.data);
					$scope.loading=false;
	         		var result=response.data.message;
	        		console.log(result);
	        		Data.setSuccessCode(result);
	        		$location.path('/Success');
	         	  }, function (response) {
	         		// called asynchronously if an error occurs
	          	    // or server returns response with an error status.
					$scope.loading=false;
	          		console.log("An error encountered"+response);
	          		if (response.status == 422) {
	        			Data.setErrorFromServer(response.data.message);//this sets the err code/message in the service so that it can be displayed on the screen.
	             		console.log('err from server ');	             		 
	             		$location.path('/ServerError');
	          		} else {
	          			Data.setErrorFromServer(response.status);
	           			console.log('after location');
	           			$location.path('/ServerError');  
	          		}
	         	  });
	       }
	   
	   $scope.submitipv4 = function(){
		   $scope.user.ipVersion= "IPv4";
		   submit();
	   }
	     ///to pass the data entered from the front end to the back end.
	   $scope.submitipv6 = function(){
		   $scope.user.ipVersion= "IPv6";
		   submit();
	   }
	     
	
	   
	     var cancel= function(){
		 		$scope.loading=true;
	     	  //http call to backend
		 		$http.post('/dns-server-1.0/reversedelegationCancel',$scope.user)
	        	.then(function (response) {
	        	    // this callback will be called asynchronously
	        	    // when the response is available
	        		console.log(response.data);
					$scope.loading=false;
	         		var result=response.data.message;
	        		console.log(result);
	        		Data.setSuccessCode(result);
	        		$location.path('/Success');

		    	  }, function (response) {
		    		// called asynchronously if an error occurs
		      	    // or server returns response with an error status.
		    		  if (response.status == 422) {
		    			  //status 422 is business exception thrown from java layer whenever a perl exception occures
		    			  //this sets the err code/message in the service so that it can be displayed on the screen.
		    			  Data.setErrorFromServer(response.data.message);
		             		console.log('err from server ');	             		 
		             		$location.path('/ServerError');
		          		} else {
		          			Data.setErrorCode(response.status);
		           			console.log('after location');
		           			$location.path('/ServerError');  
		          		} 
		    	  });
		     }
	     $scope.cancelipv4 = function(){
			   $scope.user.ipVersion= "IPv4";
			   cancel();
		   }
		   $scope.cancelipv6 = function(){
			   $scope.user.ipVersion= "IPv6";
			   cancel();
		   }
	     
	   
}) 

  .config(function($routeProvider, $locationProvider) {
  $routeProvider
  .when('/DNS_PrimaryDnsAdd', {
	    templateUrl: 'modules/dns/templates/PrimaryDnsAdd.html',
	  })
	  .when('/DNS_SecondaryDnsAdd', {
	   templateUrl: 'modules/dns/templates/SecondaryDnsAdd.html',
	  })
	  .when('/DNS_SecondaryDnsRemove', {
	    templateUrl: 'modules/dns/templates/SecondaryDnsRemove.html',
	  })
	   .when('/DNS_PrimaryDnsView',{
		  templateUrl:'modules/dns/templates/PrimaryDnsView.html'		  
	  })
	  .when('/DNS_SecondaryDnsView',{
		  templateUrl:'modules/dns/templates/SecondaryDnsView.html',
	  })
	  .when('/DNS_DnsResolverAdd',{
		  templateUrl:'modules/dns/templates/DnsResolverAdd.html',	  
	  })
	  .when('/DNS_DnsResolverView',{
		  templateUrl:'modules/dns/templates/DnsResolverView.html',	  
	  })
	  .when('/DNS_ReverseMappingAdd',{
		  templateUrl:'modules/dns/templates/ReverseMappingAdd.html',
	  })
	  .when('/DNS_ReverseDelegationAdd',{
		  templateUrl:'modules/dns/templates/ReverseDelegationAdd.html',
	  })
	  .when('/DNS_ReverseDelegationAndMappingView',{
		  templateUrl:'modules/dns/templates/ReverseDelegationAndMappingView.html',		  
	  })	 
	  .when('',{})
 });

})();