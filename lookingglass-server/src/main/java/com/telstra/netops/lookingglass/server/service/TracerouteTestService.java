package com.telstra.netops.lookingglass.server.service;

import java.util.List;

import com.telstra.netops.lookingglass.server.model.ClientNodeTestRequest;
import com.telstra.netops.lookingglass.server.model.TracerouteHop;

public interface TracerouteTestService {

	List<TracerouteHop> performTracerouteTest(ClientNodeTestRequest request);

}
