/*
 * An XML document type.
 * Localname: getCredentialsEntriesIDsResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getCredentialsEntriesIDsResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetCredentialsEntriesIDsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponseDocument
{
    
    public GetCredentialsEntriesIDsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCREDENTIALSENTRIESIDSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getCredentialsEntriesIDsResponse");
    
    
    /**
     * Gets the "getCredentialsEntriesIDsResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse getGetCredentialsEntriesIDsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse)get_store().find_element_user(GETCREDENTIALSENTRIESIDSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCredentialsEntriesIDsResponse" element
     */
    public void setGetCredentialsEntriesIDsResponse(com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse getCredentialsEntriesIDsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse)get_store().find_element_user(GETCREDENTIALSENTRIESIDSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse)get_store().add_element_user(GETCREDENTIALSENTRIESIDSRESPONSE$0);
            }
            target.set(getCredentialsEntriesIDsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getCredentialsEntriesIDsResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse addNewGetCredentialsEntriesIDsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse)get_store().add_element_user(GETCREDENTIALSENTRIESIDSRESPONSE$0);
            return target;
        }
    }
}
