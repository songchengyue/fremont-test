# Telstra NetOps Project Development Guidelines

This document outlines the design guidelines for the NetOps project.

## Version Control

* All code in Git DURING development, NOT AFTER. Code should be committed/pushed each day
* EVERYTHING that is part of the running system should be push to Git. Not just Java code. e.g. cron jobs

## All Java Applications

### General Code Quality and Principle

* Class and method names should be clear and concise. In particular names should reflect their function NOT how they are used
* All code must be formatted as per the Eclipse format rules.
* Use Java exceptions to indicate exceptional conditions, don't return a string "error", or similar
* Code should pass automated quality checks. TBA - possibly using (SonarQube)[http://www.sonarqube.org/]
* No commented out code! If you need something for later, create a branch!
* Do not use Java primitives. Use their object equivalent e.g. use "Long" not "long"
* Avoid copy-and-paste, refactor where possible.

### Domain Model

* Constuct an domain model, modeling the concept in your application with Java object
* Use appropriate types, use enums, not "magic number"
* Model relationships, included many-to-one, one-to-many relationsship in your domain model where appropriate
* Do not use String for all fields, use the most appropriate type

### Modular Design

* All code should be in the "com.telstra.netops" package
* Code should be split into separate projects where appropriate
* Each project should have its own subpackage under "com.telstra.netops"
* Package structure under the main project subpackage should be will thought out:
** domain model should be separate from other code
** application layers should have their own packages
* Should use "plugin" style where appropriate
* Each project should be documented with it's own README.md file


### Spring

* Should be Spring Boot applications with main class named "Application"
* Spring annotations to be used (not XML)
* Spring dependency injection to be used

### Configuration

* All configuration values should be externalized an injected using the Spring @Value annotation
* Make use of Spring Profiles to be used to active configuration for different sites. Each site should have its own Spring Profile
* Internal configuration to be maintained in the application.yml file
* Internal configuration for a specific sites to be maintained in the application-<profile>.yml file
* Configuration options that need to be set be system administrators should be exposes using a site-application.yml that is NOT packaged in JAR/WAR file. The file should reside in the /etc/netops folder and should be configured using as follows:
```
-Dspring.profiles.active=<profile> -Dspring.config.location="file:/etc/netops/site-application.yml,classpath:"
```

### Database/JPA

* No database vendor specific code in the application.
* Use Spring JPA for database access, not direct JDBC database access without a specific reason
* Use surrogate keys as primary keys, not natural keys. Key should map to Java long type.
* Database schema should be auto-generated for JPA entity annotated classes
* NO pre/post-processing information from the database in application Repository/Service layer. Change the database query/design/data.
* Use [Spring Data Repositories](http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories) for repository layer

### Logging and Debugging

* Use SLF4J for logging, NO println statements allowed

### Libraries

* The latest available stable release for third-party libraries should be used
* Libraries used should be under active maintenance by third party
* Prefer features implemented directly in Java over third-party equivalents

### Testing

* All applications should provide a "local" mode configured with the Spring Profile "local". In this mode the application should run from Eclipse will all major external dependencies "stubbed"
* Automated unit tests should we written. We should aim for at least 70% coverage.
* Unit tests should use [mockito](http://mockito.org/) library for mocking dependencies
* Automated integration tests should be written.

### Build

* All deliverables to be build by Maven
* 'Fat' jars/wars should be built using spring-boot-maven-plugin
* All projects should be added the the parent project build

### Performance

* Use database connection pooling for server type applications for best performance (default if JPA is used)

### Deployment

* Third-party infrastructure should be deployed via standard Red Hat RPM packages managed via "yum", not manually! e.g. Tomcat, perl etc
* Package our own applications!

## Spring Web Applications

### Design

* Design should be layered: Controller, Service, Repository
* Should support [Content Negotation](https://spring.io/blog/2013/05/11/content-negotiation-using-spring-mvc). Model should be POJO based with the Spring web framework handling the convertion to/from different representations (e.g. JSON, XML)
* Propagate error conditions to clients via exceptions annotated with @ResponseStatus
* Use JSP for templating
* Templates should have ".jsp" suffix
* Use Spring RestTemplate for calling RESTful web services

### Deployment

* Applications will be deployed in Tomcat 8

## AngularJS

* Modularize code into different files, not all in one big file
* Templates should have ".html" suffix
* Make use of partial template to avoid copy-and-paste
* Use HTTP interceptors for code common to all micro-service calls

## Integration Modules

* Separate modules for "reader" and "writer"
* Model integration data as "events" of interest to the NetOps system
* Avoid direct database access for integration. Prefer web services.

