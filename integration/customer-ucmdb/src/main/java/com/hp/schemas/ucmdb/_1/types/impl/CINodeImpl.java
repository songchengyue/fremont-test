/*
 * XML Type:  CINode
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.CINode
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML CINode(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class CINodeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.CINode
{
    
    public CINodeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LABEL$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "label");
    private static final javax.xml.namespace.QName CIS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "CIs");
    
    
    /**
     * Gets the "label" element
     */
    public java.lang.String getLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LABEL$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "label" element
     */
    public org.apache.xmlbeans.XmlString xgetLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LABEL$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "label" element
     */
    public void setLabel(java.lang.String label)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LABEL$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LABEL$0);
            }
            target.setStringValue(label);
        }
    }
    
    /**
     * Sets (as xml) the "label" element
     */
    public void xsetLabel(org.apache.xmlbeans.XmlString label)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LABEL$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(LABEL$0);
            }
            target.set(label);
        }
    }
    
    /**
     * Gets the "CIs" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs getCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(CIS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CIs" element
     */
    public void setCIs(com.hp.schemas.ucmdb._1.types.CIs cIs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(CIS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(CIS$2);
            }
            target.set(cIs);
        }
    }
    
    /**
     * Appends and returns a new empty "CIs" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs addNewCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(CIS$2);
            return target;
        }
    }
}
