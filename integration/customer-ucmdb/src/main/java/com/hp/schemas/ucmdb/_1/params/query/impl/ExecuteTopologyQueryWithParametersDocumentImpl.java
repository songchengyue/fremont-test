/*
 * An XML document type.
 * Localname: executeTopologyQueryWithParameters
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParametersDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one executeTopologyQueryWithParameters(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class ExecuteTopologyQueryWithParametersDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParametersDocument
{
    
    public ExecuteTopologyQueryWithParametersDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EXECUTETOPOLOGYQUERYWITHPARAMETERS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryWithParameters");
    
    
    /**
     * Gets the "executeTopologyQueryWithParameters" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters getExecuteTopologyQueryWithParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters)get_store().find_element_user(EXECUTETOPOLOGYQUERYWITHPARAMETERS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "executeTopologyQueryWithParameters" element
     */
    public void setExecuteTopologyQueryWithParameters(com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters executeTopologyQueryWithParameters)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters)get_store().find_element_user(EXECUTETOPOLOGYQUERYWITHPARAMETERS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters)get_store().add_element_user(EXECUTETOPOLOGYQUERYWITHPARAMETERS$0);
            }
            target.set(executeTopologyQueryWithParameters);
        }
    }
    
    /**
     * Appends and returns a new empty "executeTopologyQueryWithParameters" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters addNewExecuteTopologyQueryWithParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters)get_store().add_element_user(EXECUTETOPOLOGYQUERYWITHPARAMETERS$0);
            return target;
        }
    }
}
