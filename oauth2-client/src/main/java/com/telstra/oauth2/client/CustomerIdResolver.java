package com.telstra.oauth2.client;

import org.springframework.stereotype.Component;

@Component
public class CustomerIdResolver extends AuthorityResolver {
	
	// must match same value in SAMLTokenGranter class
	private static final String CUSTOMER_ID_ROLE_NAME_PREFIX = "ROLE_CUSTOMER_ID_";
		
	public CustomerIdResolver() {
		super(CUSTOMER_ID_ROLE_NAME_PREFIX, "Customer Id");
	}

}
