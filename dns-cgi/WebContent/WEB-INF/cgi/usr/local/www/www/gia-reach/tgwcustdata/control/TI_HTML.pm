# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 6 June 2001
# File: TI_HTML.pm
#
# This file contains procedures to allow the interfacing of perl scripts
# and HTML interfaces.
# The goal of this is to allow a programmer complete control of variables
# and control over what gets displayed, but to allow a (possibly) separate
# person to control the formatting of the HTML.
#
# $Id: TI_HTML.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $

package TI_HTML;
use Exporter;
use strict;
use warnings;
use VALIDATE;

our @ISA     = qw(Exporter);

our $base_template_path = ".";
our $error_message;


########################### OUTPUT CODE ##############################





# sub generate_existing_values(\%values)
# This subroutine generates an array containing existing elements for
# use in <ti_existing_values> tags
#
# The current screenname is also taken and added
# an existing value is hash element of %values which itself has an 
# "value" element

sub generate_existing_values {
  my $values = shift @_;
  my $screenname = shift @_;

  my $newline;
  foreach my $key (keys %$values) {
    if (ref($$values{$key}) eq "HASH") {
      foreach my $key2 (keys %{$$values{$key}}) {

	if ($key2 eq "value") {
	  if (defined ($$values{$key}{value})) {
	    $$values{$key}{value} =~ s/<BR>/\n/g;
	  }
	  if ($key =~ /^existing\./) {
	    # this is already an existing value
	    # is their a newer value in existence?
	    my $new_value_key = $key;
	    $new_value_key =~ s/^existing\.//g;
	    if (defined ($$values{$new_value_key}{value})) {
	      # this value has been defined (may be same)
	      if ($$values{$new_value_key}{value} ne 
		  $$values{$key}{value}) {
		
		push @{$$values{existing_values}{options}}, 
		  ["previous.$new_value_key", $$values{$key}{value}];
	      }
	    } else {
	      
	      # preserve old value
	      push @{$$values{existing_values}{options}}, 
		["$key", $$values{$key}{value}];
	      
	    }
	    
	  } elsif ($key =~ /^previous\./) {
	    # this was a previous value that was updated
	    # only preserve previous values for one iteration
	  } else {
	    # add as long as not a password, error. or errormark
	    if ($key !~ /(?:password|error\.|errormark\.)/) {
	      push @{$$values{existing_values}{options}}, 
		["existing.$key", $$values{$key}{value}];
	    }
	  }
	
	}
	
      }
      
    }
  }
  push @{$$values{existing_values}{options}}, ["current_screen", $screenname];
}





# sub substitute_html (\$line, \%values);
# This subroutine takes a line of code containing a <ti_WIDGET> tag
# and inserts the correct substitues the correct HTML code
sub substitute_html {
  my $line = shift @_;
  my $values = shift @_;

  my $newline;

 
  
  if ($$line =~ /<(ti_[\w_]+?)(?: (.+))?>/is) {
    my $widget_type = uc($1);
    my $widget_remainder;
    my %widget_values;

    unless ($widget_remainder = $2) {
      $widget_values{name} = "page";
    }

    my $count = 0;
    while ((defined $widget_remainder) && ($widget_remainder =~ /([\w\d\_]+?)\=(?:\"(.*?)\"|([\w\d]*) ?)/s) && ($count++ <100)){

      
      my $option_name = lc($1);
      my $option_value = $2;

      if (not defined $option_value) {
	$option_value = $3;
      }
      
      if ($option_name !~ /OPTIONS/i) {

	if (!defined ($widget_values{$option_name})) {
	  # only get the first if defined multiple times
	  # this allows the embedding of mtultiple 
	  # <ti> tags within <ti> tags
	  $widget_values{$option_name} = $option_value;
	}
	
	$widget_remainder =~ s/$option_name\=\"?$option_value\"? ?//is;
      } else {
	# This is an OPTION set
	
	my @option_set = split(/\;/, $option_value);
	foreach my $option_pair (@option_set) {
	  push @{$$values{$widget_values{name}}{options}}, [split(/\,/,$option_pair)];
	}

      }
    
      $widget_remainder =~ s/$option_name\=\"?$option_value\"? ?//is;
    }


    # FIND WIDGETS
    my $custom_widget_path = $ENV{SCRIPT_FILENAME};
    $custom_widget_path 
      =~ s/^((?:\/.+?)+)\/cgi-bin\/(.+?)\/.+/$1\/$2/;
    my $widget_path;
    if (-e "$custom_widget_path/widgets/$widget_type.widget") {
      $widget_path = "$custom_widget_path/widgets/$widget_type.widget";
    } else {
      my $generic_widget_path = $ENV{SCRIPT_FILENAME};
      $generic_widget_path =~ s/^((?:\/.+?){3})\/.+/$1/;
	if (-e "$generic_widget_path/modules/TI_HTML/widgets/$widget_type.widget") {
      $widget_path = "$generic_widget_path/modules/TI_HTML/".
	"widgets/$widget_type.widget";
	} else {
		$generic_widget_path = $ENV{SCRIPT_FILENAME};
      		$generic_widget_path =~ s/^((?:\/.+?){4})\/.+/$1/;
$widget_path = "$generic_widget_path/modules/TI_HTML/".
        "widgets/$widget_type.widget";


	}	
    }
    
    
    if (open (WIDGET, $widget_path)) {
      # get first line which should be of the form !WIDGET_TYPE

      # get all lines from file (note .=  : keeps appending until EOF)
      my $widget_line;

      while ($widget_line = readline(*WIDGET)) {
	if ($widget_line !~ /^\#/) {
	  last;
	}
      }
      
      if ($widget_line =~ /!SINGLE/) {

	while ($widget_line = readline(*WIDGET)) {

	  # single widget
	  # read in every line and exchange value where needed
	  $widget_line =~ s/\#.+$//g; # strip commenets
	  #chomp ($widget_line);
	  while ($widget_line =~ /\[\[\[(.*?)(?:\|(.*?))?\]\]\]/) {

	    if (defined($$values{$widget_values{name}}{$1})) {
	      my $key = $1;
	      
	      $widget_line =~ s/\[\[\[$key.*?\]\]\]/$$values{$widget_values{name}}{$key}/is;
	     
	    } elsif (defined($widget_values{$1})) {

	      my $key = $1;
	      
	      $widget_line =~ s/\[\[\[$key.*?\]\]\]/$widget_values{$key}/is;
	      
	    } elsif (defined($2)) {
	      
	      my $key = $1;
	      my $value = $2;

	     
	      $widget_line =~ s/\[\[\[$key.*?\]\]\]/$value/is;
	      
	    } else {
	      my $key = $1;

	      $widget_line =~ s/(?: [\w\d]+=\"?)?\[\[\[$key.*?\]\]\]\"?//is;
	      
	    
	    }
	  }

	  if ((defined($$values{$widget_values{name}}{value})) && 
	      (defined $widget_values{option}) && 
	      ($$values{$widget_values{name}}{value} eq 
			$widget_values{option})) {
	    $widget_line =~ s/\+\+\+//g;
	  } else {
	    
	    $widget_line =~ s/\+\+\+.+?\+\+\+//g;
	    
	  }
	 
	  $newline .= $widget_line;

	  

	}
	
      } elsif ($widget_line =~ /!MULTIPLE/) {

	
	# multiple line widget
	# lines to be repeated are marked by * or +

	# loop through widget until get to * or +
	while ($widget_line = readline(*WIDGET)) {

	  $widget_line =~ s/\#.+$//g; # strip commenets

	  if ($widget_line =~ /^[\+\*]/) {
	    # found + or *
	    $widget_line =~ s/^[\+\*]//g; # strip * or +
	    last;
	  }

	  # replace single values
	  while ($widget_line =~ /\[\[\[.+\]\]\]/) {
	    $widget_line =~ s/\[\[\[(.+?)\]\]\]/$widget_values{$1}/i;
	  }
	  $newline .= $widget_line;
	}

	my $newblock = $widget_line;

	my $looped = 0;
 	while ($widget_line = readline(*WIDGET)) {
	  $looped = 1;
	  if ($widget_line =~ /^[\+\*]/) {
	    $widget_line =~ s/\#.+$//g; # strip commenets
	    $widget_line =~ s/^[\+\*]//g; # strip * or +

	    $newblock .= $widget_line;

	  } else {
	    # have found end of block
	    
	    last;
	  }
	}


	#Now need to make block for each value
	if (defined ($$values{$widget_values{name}}{options})) {
	  # if options were passed
	  foreach my $row (@{$$values{$widget_values{name}}{options}}) {
	    my $block = $newblock;
	    no warnings;
	    $block =~ s/\[\[\[name\]\]\]/$widget_values{name}/gi;
	    $block =~ s/\[\[\[value\]\]\]/$$row[0]/gi;


	   
	    if (not defined ($$row[1])) {
	      $$row[1] = $$row[0];
	    } 
	    $block =~ s/\[\[\[string\]\]\]/$$row[1]/gi;
	    
	    my %selected_values = ();
            foreach my $v (split("\0", $$values{$widget_values{name}}{value})) {
		$selected_values{$v} = '';
            }
#	    if ((defined($$row[2]) && $$row[2]) || 
#		((defined($$values{$widget_values{name}}{value})) && 
#		  ($$row[0] eq $$values{$widget_values{name}}{value}))){
	    if ((defined($$row[2]) && $$row[2]) || 
		((defined($$values{$widget_values{name}}{value})) && 
		  (defined($selected_values{$$row[0]})))){

	      # if defined and not 0 means that the value in between
	      # two sets of +++ is to be retained.
	      
	      # remove +++'s
	      $block =~ s/\+\+\+//g;
	    } else {

	      # remove everything between +++ as well as +++
	      $block =~ s/\+\+\+.+?\+\+\+//g;

	    }
	    $newline .= $block;
	  }
	  # add after block line if any
	  if (defined($widget_line)){$newline .= $widget_line;}

	} else {
	  # no options passed
	  $newline = "<!--TI_HTML ERROR: no options for $widget_values{name}-->";
	}

	while ($widget_line = readline(*WIDGET)) {
	  $widget_line =~ s/\#.+$//g; # strip commenets
	  $newline .= $widget_line;


	}

      } elsif ($widget_line =~ /^!TABLE/) {
	# Table
	
	my %format;
	while ($widget_line = readline(*WIDGET)) {
	  if ($widget_line =~ /^\+(\w+)\s+(.+)$/) {
	    $format{$1} = $2;
	  } #else {
	    # ignore
	  #}

	}



	# assume that $$values{$widget_values{name}}{table} is square
	
	if ((defined $$values{$widget_values{name}}{table}) && 
	    (ref($$values{$widget_values{name}}{table}) eq "ARRAY") &&
	    (defined @{$$values{$widget_values{name}}{table}}[0])) {

	  my $num_header_rows = 
	    $$values{$widget_values{name}}{header_rows} || 0;
	  my $num_footer_rows = 
	    $$values{$widget_values{name}}{footer_rows} || 0;
	  my $num_left_columns = 
	    $$values{$widget_values{name}}{left_columns} || 0;
	  my $num_right_columns = 
	    $$values{$widget_values{name}}{right_columns} || 0;

	  my $table = $$values{$widget_values{name}}{table};
	  my $height = scalar (@$table);
	  my $width = scalar (@{$$table[0]});
	  
	  $newline .= $format{STARTTABLE};
	  
	  # HEADER
	  $newline .= $format{STARTHEADERS};
	  for my $row (0..$num_header_rows-1) {
	    $newline .= $format{STARTHEADERROW};
	    for my $column (0..$num_left_columns-1) {
	      my $cell = $format{LEFTHEADER};
	      $cell =~ s/\[\[\[value\]\]\]/$$table[$row][$column]/i;
	      $newline .= $cell;
	    }
	    
	    for my $column ($num_left_columns..
			    ($width-$num_right_columns-1)) {
	      my $cell = $format{CENTREHEADER};
	      $cell =~ s/\[\[\[value\]\]\]/$$table[$row][$column]/i;
	      $newline .= $cell;
	    }
	    for my $column ($width-$num_right_columns..$width-1) {
	      my $cell = $format{RIGHTHEADER};
	      $cell =~ s/\[\[\[value\]\]\]/$$table[$row][$column]/i;
	      $newline .= $cell;
	    }
	    $newline .= $format{ENDHEADERROW};
	    
	  }
	  $newline .= $format{ENDHEADERS};
	  
	  # BODY
	  $newline .= $format{STARTBODY};
	  foreach my $row ($num_header_rows..
			   ($height-$num_footer_rows-1)) {
	    if ($row%2) {
	      $newline .= $format{STARTBODYROW0};
	    } else {
	      $newline .= $format{STARTBODYROW1};
	    }
	    for my $column (0..$num_left_columns-1) {
	      my $cell;
	      if ($row%2){
		$cell = $format{LEFTBODY0};
	      } else {
		$cell = $format{LEFTBODY1};
	      }
	      $cell =~ s/\[\[\[value\]\]\]/$$table[$row][$column]/i;
	      $newline .= $cell;
	    }
	    for my $column ($num_left_columns..
			    ($width-$num_right_columns-1)) {
	      my $cell;
	      if ($row%2){
		$cell = $format{CENTREBODY0};
	      } else {
		$cell = $format{CENTREBODY1};
	      }
	      {
		no warnings; # don't complain about empty cells
		$cell =~ s/\[\[\[value\]\]\]/$$table[$row][$column]/i;
	      }
	      $newline .= $cell;
	    }
	    for my $column ($width-$num_right_columns..$width-1) {
	      my $cell;
	      if ($row%2){
		$cell = $format{RIGHTBODY0};
	      } else {
		$cell = $format{RIGHTBODY1};
	      }
	      $cell =~ s/\[\[\[value\]\]\]/$$table[$row][$column]/i;
	      $newline .= $cell;
	    }
	    
	    if ($row%2){
	      $newline  .= $format{ENDBODYROW0};
	    } else {
	      $newline .= $format{ENDBODYROW1};
	    }
	  }
	  
	  
	  if ($height >= $num_footer_rows + $num_header_rows) {
	    # FOOTER
	    $newline .= $format{STARTFOOTERS};
	    foreach my $row ($height-$num_footer_rows..$height-1) {
	      $newline .= $format{STARTFOOTERROW};
	      for my $column (0..$num_left_columns-1) {
		my $cell = $format{LEFTFOOTER};
		$cell =~ s/\[\[\[value\]\]\]/$$table[$row][$column]/i;
		$newline .= $cell;
	      }
	      for my $column ($num_left_columns..
			      ($width-$num_right_columns-1)) {
		my $cell = $format{CENTREFOOTER};
		$cell =~ s/\[\[\[value\]\]\]/$$table[$row][$column]/i;
		$newline .= $cell;
	      }
	      for my $column ($width-$num_right_columns..$width-1) {
		my $cell = $format{RIGHTFOOTER};
		$cell =~ s/\[\[\[value\]\]\]/$$table[$row][$column]/i;
		$newline .= $cell;
	      }
	      $newline .= $format{ENDFOOTERROW};
	    }
	    $newline .= $format{ENDFOOTERS};
	  }	  
	  $newline .= $format{ENDTABLE};
	  
	} else {
	  $newline = "<!--TI_HTML ERROR: no table for $widget_values{name}-->";
	}
      } else {
	$newline = "<!--TI_HTML ERROR: unhandled widget type-->";
      }

      close (WIDGET);
    } else {
      # failed to find widget
      $newline = "<!--TI_HTML ERROR: missing widget file for $widget_type-->";
    }
  } else {
    # failed to find properly formatted tag
    $newline = "<!--TI_HTML ERROR: incorrect formatting -->";
  }
  # substitute text from widget file into $line

  $$line =~ s/<ti_.+?>/$newline/i;


}


sub evaluate_expression {
  my $values = shift @_;
  my $logic = shift @_;
  my $result;

 

  if ($logic =~ /\((.+?)\s*( gt | lt | ge | le | eq | ne |>=|<=|>|<|==|!=|=~)\s*(.+?)\)/){
    # is a valid logic statement
    # evaluate the logic statement
    
    my $passed_value = $$values{$1}{value} || 0;
    my $op = $2;
    my $matching_value = $3;
    
    $matching_value =~s /[\"\']//gs;
    $op =~ s/ //g;
    
    

    if ($op eq "gt") {
      $result = ($passed_value gt $matching_value);
    } elsif ($op eq "lt") {
      $result = ($passed_value lt $matching_value);
    } elsif ($op eq "ge") {
      $result = ($passed_value ge $matching_value);
    } elsif ($op eq "le") {
      $result = ($passed_value le $matching_value);
    } elsif ($op eq "eq") {
      $result = ($passed_value eq $matching_value);
    } elsif ($op eq "ne") {
      $result = ($passed_value ne $matching_value);
    } elsif ($op eq ">") {
      $result = ($passed_value > $matching_value);
    } elsif ($op eq "<") {
      $result = ($passed_value < $matching_value);
    } elsif ($op eq ">=") {
      $result = ($passed_value >= $matching_value);
    } elsif ($op eq "<=") {
      $result = ($passed_value <= $matching_value);
    } elsif ($op eq "==") {
      $result = ($passed_value == $matching_value);
    } elsif ($op eq "!=") {
      $result = ($passed_value != $matching_value);
    } elsif ($op eq "=~") {
      
      # regular expression
      # it is necessary to preserve any \/ slashes
      # assume only two / exist at most
      # value between two / is string
      # value (if any) after second / are options
      $matching_value =~ s/\\\//MKOMKOMKO/g; # preserve \/
      my $options = "";
      if ($matching_value =~ /^\/(.+?)\/(.*?)$/) {
	$matching_value =~ s/^\/(.+?)\/(.*?)$/($1)/;
	$options = $2;
      }
      $matching_value =~ s/MKOMKOMKO/\\\//g; # restore \/
 
      $result = ($passed_value =~ /(?$options:$matching_value)/);
      
    } else {

    }
  }

  return $result;
}



# EXPORTED SUBROUTINES


# sub clear (\%values) 
# this subroutine clears the values hash but preserves username and
# code and all page values
sub clear {
  my $values = shift @_;

  

  my $username = $$values{username}{value};
  my $code = $$values{authentication_code}{value};
  my $page = $$values{page};

  undef %$values;

  $$values{username}{value} = $username;
  $$values{authentication_code}{value} = $code;
  $$values{page} = $page;

}

# sub add_error (\%values, $error_message, $fieldname)
# This subroutine takes an error and creates the relevant error messages
sub add_error {
  my $values = shift @_;
  my $error = shift @_;
  my $fieldname = shift @_;

  push @{$$values{ALL_ERROR}{options}}, [ $error ];
  if (defined ($fieldname)) {
    $$values{"error.$fieldname"}{value} = $error;
    $$values{"errormark.$fieldname"}{value} = 1;
  }
}

# sub prepare screen ($screenname, \%values, $type) 
# This subroutine is used to prepare a html file.
#
# $screenname - the name of the screen
#
# The template should be able to be found at:
#    $base_template_path.$screenname.template.
# The rules file should be able to be found at:
#    $base_template_path.$screenname.rules.
#
# %values - contains the information needed to populate the page.
#
#
# On success, generated output is returned.
# On failure, undef is returned.

sub output_html {

  unless ((scalar(@_) == 3)){
    # return undef if wrong number of inputs
    warn "TI_HTML ERROR: Incorrect number of inputs to output_html\n$@";
    $error_message = 
      "TI_HTML ERROR: Incorrect number of inputs passed to output_html\n$@";
    return undef;
  }

  my $screenname = shift @_;
  my $values = shift @_;
  my $type = shift @_;
  
  my $output = "Content-type: ";
  if ($type eq "html") {
    $output .= "text/html\n\n";
  } elsif ($type eq "csv") {
    $output .= "application/vnd.ms-excel\n\n";
  } 

  # open template file
  unless (open(TEMPLATE, "$base_template_path/$screenname.template")) {
    # failed to open file
    warn "TI_HTML ERROR: Failed to open $base_template_path/$screenname.template\n$@";
    $error_message = 
      "TI_HTML ERROR: Failed to open $base_template_path/$screenname.template\n$@";
    return undef;
  }

  while (my $line = readline(*TEMPLATE)) {
   
    # this is the start of a <ti_x> tag
    # maybe mutliple tags on line
    while ($line =~ /<ti_/i) {
      # need to get all of the first tag before processing
      # note partial tags may exist after this
      my $count = 0;
      until ($line =~ /<ti_.+>/s) {

	$line .= readline(*TEMPLATE);
	if ($count++ == 20) {die;};
      }

      
      
      if ($line =~ /<ti_existing_values>/i) {
	# special case of <ti_exisitng_values tag
	generate_existing_values ($values, $screenname);
	$line =~ s/<ti_existing_values>/<ti_existing_values name=\"existing_values\">/i;
      }
      
      if ($line =~ /<ti_all_errormessages>/i) {
	# special case of <ti_all_errormessages>
	$line =~ s/<ti_all_errormessages>/<ti_all_errormessages name=\"ALL_ERROR\">/i;
      }
      
      while ($line =~ /<ti_if /i) {

	# got a <ti_if> tag
	# format:  <ti_if key X value> where X is perl logic operator
	#              HTML
	#          [<ti_else>
	#              HTML]
	#          <ti_endif>
	
	# need to get all of the tag before processing
	# note partial tags may exist after this
	until ($line =~ /<ti_if .+>/is) {
	  $line .= readline(*TEMPLATE);
	 
	}
	
	# have entire <ti_if> tag
	# need to extract logic
	
	$line =~ s/<ti_if (.+?)>/<ti_if $1>/is; # purely to get $logic
	my $result = evaluate_expression ($values,$1);
	
	
	# need to get everything until <ti_endif>
	$count = 0;
	until ($line =~ /<ti_endif>/is) {
	  if ($count++ > 100) { die "infinite loop in TI_HTML"; }
	  $line .= readline(*TEMPLATE);
	}
	
	if ($result) {
	  # logic matches
	  

	 

	  if ($line =~ /<ti_if(.+?)<ti_endif>/is) {
	    # used to extract middle
	    my $middle = $1;
	    if ($middle =~ /<ti_else>/) {
	      
	      # remove anything between <ti_else> and <ti_endif>
	      $line =~ s/<ti_else>.+?<ti_endif>//is;
	  
	      # remove <ti_if>
	      $line =~ s/<ti_if .+?>//is;
	    } else {

	      # remove <ti_if>
	      $line =~ s/<ti_if .+?>//is;
	      # remove <ti_endif> in the case of no <ti_else>
	      $line =~ s/<ti_endif>//is;
	    }
	  }
	  
	} else {
	  # logic does not match
	  
	  if ($line =~ /<ti_if(.+?)<ti_endif>/is) {
	    # used to extract middle
	    my $middle = $1;
	    if ($middle =~ /<ti_else>/) {

	      # remove anything between <ti_if> and <ti_else>
	      $line =~ s/<ti_if .+?<ti_else>//is;
	  
	      # remove <ti_endif> 
	      $line =~ s/<ti_endif>//is;
	    } else {
	      # remove everything between <ti_if> and <ti_endif>
	      $line =~ s/<ti_if .+?<ti_endif>//is;
	    }
	  }
	}
	
      }
      substitute_html (\$line, $values);
    }
    # put in any CGI strings
    if ($line =~ /GET_STRING/) {
     
      if ((defined $$values{username}{value})
	  && (defined $$values{authentication_code}{value})) {
	$line =~ 
	  s/\?GET_STRING\&/\?username=$$values{username}{value}\&authentication_code=$$values{authentication_code}{value}\&/g;
      } else {
	$line =~ s/\?GET_STRING\&/\?NO_INFO_FOR_GET_STRING\&/g;
      }
    }
    $output .= $line; # append line(s) to $output;
  }

  close (TEMPLATE);

  

  print $output;
  return \$output;

}


# sub output_html_to_file ($screenname, \%values, $filename)
# This subroutine is a wrapper for output_to_file
# It's usage is basically the same with minor additions:

# $outputfile - the output will be written to it, as well as
#               being returned.

sub output_html_to_file {

  unless (scalar(@_) == 3){
    # return undef if wrong number of inputs
    $error_message = 
      "TI_HTML ERROR: Incorrect number of inputs passed to output_html_to_file\n$@";
    return undef;
  }

  my $filename = pop @_; # (pop is like shift but at other end of array)

  unless (open (OUTPUT, "$filename")) {
    # if can't open file there is no point creating HTML
    $error_message = 
      "TI_HTML ERROR: Unable to create file $filename\n$@";
    return undef;
  }

  my $output;
  
  if(my $output = output_html($_[0], $_[1])) {
    # if sucessful print the output to the file
    print OUTPUT $output;
  }

  return $output; # return the reference to the output array 
                  # (or undef if failed, error_message already set)

  close (OUTPUT);
}





################################# INPUT CODE #########################

# INTERNAL SUBROUTINES

# This subroutine fetches the CGI key-value pairs from the relevant 
# enviroment and STDIN
sub get_key_value_pairs {

  my $values = shift @_;

  my (@list, $cgi, $boundary, $buffer);

#print "Content-type: text/html\n\n";
#print "<P>XX ($ENV{'CONTENT_TYPE'})\n";
#  if($ENV{'CONTENT_TYPE'} =~ /multipart\/form-data; boundary=(.+)$/) {
#	use CGI;
#	my $cgi = new CGI;
#	@list = $cgi->param;
#	print "<P>XXX (@list)\n";
#	return;
#  }

  if (defined($ENV{'CONTENT_LENGTH'})) { 
    # GET data exists
    # read in POST data
    #read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});

    if($ENV{'CONTENT_TYPE'} =~ /multipart\/form-data; boundary=(.+)$/) {
	use CGI;
	$cgi = new CGI;
	@list = $cgi->param;
	$boundary = 1;
    	#$boundary = '--'.$1;
    	#@list = split(/$boundary/, $buffer);
    } else {
    	read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
    }

    # append GET data
    $buffer .= "&$ENV{'QUERY_STRING'}";
  } else {

    # get POST data
    $buffer.= $ENV{'QUERY_STRING'};
  }

  #print "<P> XX ($boundary) (@list) \n";

  if($boundary) { # form multi-part
	foreach my $l (@list) {
	        #print "<P>XX ($l)\n";
		#$l =~ /\"([^"]+)"/;
		#my $name = $1;
		my $name = $l;
		#my $value = $';

		my $value;
		if($name =~ /^file$/i) {
			my $f = $cgi->param('file');
			$f =~ m/^.*(\\|\/)(.*)/;
			my $n = $2;
			open(my $ot, ">/tmp/tmp_upload.$$");
			while(<$f>) {
				print $ot $_;
			}
			$value = "/tmp/tmp_upload.$$";
		} else {
			$value = $cgi->param($name);
			#$value =~ s/\n|\r//g unless $name =~ /^file$/i;
		}

		$$values{$name}{value} = $value;
	}

  } else {
  	#split pairs by the & which divides the vars
  	my @nuid = split(/&/, $buffer);
  	foreach my $nuid (@nuid)
    	{
      		my ($name, $value) = split(/=/, $nuid);
      		$value =~ tr/+/ /;
      		$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
		$value =~ s/\s+$//g;
		$value =~ s/^\s+//g;
      		$name =~ s/\%2C/\,/g;
#    		$$values{$name}{value} = $value;
                if (defined($$values{$name}{value})) {
                        $$values{$name}{value} = join("\0", $$values{$name}{value}, $value) ;
                } else {
                        $$values{$name}{value} = $value ;
                }
	}

  }

  # now need to go through and find all existing values
  # if there is no new value, these need to be upgraded

  foreach my $name (%$values) {
    if ($name =~ /^existing\./) {
      my $name2 = $name;
      $name2 =~ s/^existing\.//; # remove .existing
      if (not defined($$values{$name2}{value})) {
	# if no newer value move passed value to current
	$$values{$name2}{value} = $$values{$name}{value};
      }
    }
  }


}

# EXPORTED SUBROUTINES

# sub check_screen ($screenname, \%values)
# This subroutine is used to check whether required entries have
# been entered by a user.
# 
# $screenname - the name of the screen
#
# The template should be able to be found at:
#    $base_template_path.$screenname.template.
# The rules file should be able to be found at:
#    $base_template_path.$screenname.rules.
#
# If the values entered meet the rules, then the procedure will
# return true.
# If the values entered do not meet the rules, then the procedure
# will return undef or, if option output is set, reprint the screen and exit
#

sub check_screen {
  my $screenname = shift @_;
  my $values = shift @_;

  my @rules;

  # get rules


  unless (open (RULES, "$base_template_path/$screenname.rules")) {
    # can't find rules file - return successful
    return 1;
  }

  while (my $line = readline(*RULES)) {
    # line must be in format:
    # ^FIELD(s)\t+RULE\t+ERRORMESSAGE\n$
    # Field, rule and error message can be anything
    # Multiple tabs are allowed

    # FIELD(s) can be in the following formats:
    # - field                   - refers to $values{field}{value}
    # - field()                 - refers to $values{field}{value}
    # - field(x,y)              - refers to $values{field.x}{value} and
    #                                       $values{field.y}{value}
    # - field, field2           - refers to $values{field}{value} and
    #                                       $values{field2}{value}
    # - field(x,y), field2      - refers to $values{field.x}{value},
    #                                       $values{field.y}{value} and
    #                                       $values{field2}{value}
    # There is no limit to the number of fields nor to the number of sub
    # entries within brackets.


    if (($line !~ /^\#|^$/) && ($line =~ /(.+?)\t+(.+?)\t+(.+)\n$/)) 
      {
	my %rules;
	my $field = $1;
	my $rule = $2;
	my $message =$3;

	if ($field !~ /,|\(\)/) {

	  $rules{rule} = $rule;
	  $rules{message} = $message;
	  push @{$rules{fields}}, $field;
	  
	} else {

	  $field =~ s/\s+//g;

	  my @fields;
	  my @parts = split (/;/, $field);


	  foreach my $part (@parts) {
	
	    if ($part =~ /^(.+?)\((.+?)\)/) {
	      my $root = $1;
	      my @subs = split (/,/, $2);
	      foreach my $sub (@subs) {
		
		push @fields, "$root.$sub";
	      }
	    } elsif ($part =~ /^[\w\d\_\-]+$/) {
	      push @fields, $part;
	    }
	  }

	 
	  
	  $rules{rule} = $rule;
	  $rules{message} = $message;
	  $rules{fields} = \@fields;
	  
	}

	push @rules, \%rules;
      }
    
  }
  
  
  close (RULES);
  
  
  # process rules

  foreach my $rule (@rules) {

    

    if ($$rule{rule} =~ /required/) {
      # check whether $$values{$field} is not defined but is required

      my $exists = 1;
      
      foreach my $part (@{$$rule{fields}}) {
	
	$exists = $exists && (defined ($$values{$part}{value})) &&
	  ($$values{$part}{value} ne "");

      }

      if (!$exists) {
	add_error ($values, $$rule{message});
      }
    

   } else {
      my $exists = 0;

      foreach my $part (@{$$rule{fields}}) {
	$exists = $exists || ((defined ($$values{$part}{value})) &&
	  ($$values{$part}{value} ne ""));

      }

      if ($exists) {
	# field is defined
	# a blank field is okay (if it is not it should have an exists rule)
	# an erroneously filled in non-required fields is not

	# CHECK WHETHER OBEYS RULES;
	my $success;

	if ($rule =~ /^regex\((.+)\)$/) {
	  my $regex = "\"$$values{$$rule{field}}\" =~ /$1/";
	  $success = eval $regex;
	} else {

	  my @values;
	  foreach my $field (@{$$rule{fields}}) {
	    push @values, $$values{$field}{value};
	  }

	  {
	    no warnings;
	    $success = eval ("is_$$rule{rule}(\"". join("\",\"",@values)."\")");
	  }

	  if ($@) {
	    warn "don't know what do to with $$rule{rule} - ignoring $@";
	    $success = 1;
	  }
	} 
      

	if (!$success) {
	  add_error ($values, $$rule{message});
	}
      }
    } 
  }


  if (defined ($$values{ALL_ERROR}{options})) {
    return undef;
  } else {
    return 1;
  }
  
}

# This routine takes the name of a TI_DATETIME_SEARCH widget and
# takes the relevant information to form an array containing two
# strings, namely the beginning and end of the search range.
# The dates are presented in ISO8601 format.

sub get_datetime_search_values {
  my $values = shift @_;
  my $name = shift @_;

  my @result;


 

  # get current
  $result[0] = 
    $$values{"$name.startyear"}{value}."-".
      $$values{"$name.startmonth"}{value}."-".
	$$values{"$name.startday"}{value}." ".
	  $$values{"$name.starthour"}{value}.":".
	    $$values{"$name.startminute"}{value}.":".
	      $$values{"$name.startsecond"}{value};

  $result[1] = 
    $$values{"$name.endyear"}{value}."-".
      $$values{"$name.endmonth"}{value}."-".
	$$values{"$name.endday"}{value}." ".
	  $$values{"$name.endhour"}{value}.":".
	    $$values{"$name.endminute"}{value}.":".
	      $$values{"$name.endsecond"}{value};
  
 

  return @result;
}

# This subroutine takes the name of TI_DATEIMTE_SEARCH widget
# and sets the relevant information

sub set_datetime_search_values {
  my $values = shift @_;
  my $name = shift @_;
  my $start = shift @_; # ISO 8601 Date
  my $end = shift @_;   # ISO 8601 Date

  if ($start =~ /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/) {
    $$values{"$name.startyear"}{value} = $1;
    $$values{"$name.startmonth"}{value} = $2;
    $$values{"$name.startday"}{value} = $3;
    $$values{"$name.starthour"}{value} = $4;
    $$values{"$name.startminute"}{value} = $5;
    $$values{"$name.startsecond"}{value} = $6;
  } else {
    warn "Badly formatted date in TI_HTML::set_date_search_values: ",
      $start, "\n";
  }
  
  if ($end =~ /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/) {
    $$values{"$name.endyear"}{value} = $1;
    $$values{"$name.endmonth"}{value} = $2;
    $$values{"$name.endday"}{value} = $3;
    $$values{"$name.endhour"}{value} = $4;
    $$values{"$name.endminute"}{value} = $5;
    $$values{"$name.endsecond"}{value} = $6;
  }else {
    warn "Badly formatted date in TI_HTML::set_date_search_values: ",
      $end, "\n";
  }
}

1;
