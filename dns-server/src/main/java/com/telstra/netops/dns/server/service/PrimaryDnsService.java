package com.telstra.netops.dns.server.service;

import java.util.List;

import com.telstra.netops.dns.server.model.ClientDnsRemoveRequest;
import com.telstra.netops.dns.server.model.ClientPrimaryDnsRequest;
import com.telstra.netops.dns.server.model.DnsPrimaryRecord;
import com.telstra.netops.dns.server.model.Success;

public interface PrimaryDnsService {
	
	Success add(ClientPrimaryDnsRequest request);

	List<DnsPrimaryRecord> list(String customerId);
	
	Success remove(ClientDnsRemoveRequest request);

}
