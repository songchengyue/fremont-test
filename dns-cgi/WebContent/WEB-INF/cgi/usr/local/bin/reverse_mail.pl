#!/usr/bin/perl

use strict;
use warnings;

use lib "/usr/local/www/wanchai/netops/modules";
use lib "/usr/local/www/wanchai/modules";
use CMS_DB;
use CMS_REVERSEDEL_DB;
use CMS_OLSS;

#use constant FILE_PATH_ZONE => "/usr/local/www/wanchai/cms/data/test/";
use constant FILE_PATH_ZONE => "/usr/local/www/wanchai/netops/dns/test/";

#use constant FILE_PATH_CONF_TEMPL => "/usr/local/www/wanchai/cms/scripts/make_revdns_conf.template.conf";
use constant FILE_PATH_CONF_TEMPL => "/usr/local/www/wanchai/netops/dns/revdns/template/make_revdns_conf.template.conf";

#use constant FILE_PATH_ZONE_TEMPL => "/usr/local/www/wanchai/cms/scripts/make_revdns_conf.template.zone";
use constant FILE_PATH_ZONE_TEMPL => "/usr/local/www/wanchai/netops/dns/revdns/template/make_revdns_conf.template.zone";

#use constant FILE_PATH_SN => "/usr/local/www/wanchai/cms/scripts/make_revdns_conf.serialno";
use constant FILE_PATH_SN => "/usr/local/www/wanchai/netops/dns/revdns/template/make_revdns_conf.serialno";


open (PRE,">/tmp/make_rev_pl");
my $reverse_del_hostname = "hhtred278.in.sa.telstrainternational.com";
my $file_build_date = `date -u`;
chomp ($file_build_date);
my @now = gmtime;
my ($yr,$mth,$mday,$second, $minute, $hour) = ($now[5], $now[4], $now[3],$now[0],$now[1],$now[2]);
$yr += 1900;
$mth += 1;
if ($mth <= 9) { $mth = "0".$mth; }
if ($mday <= 9) { $mday = "0".$mday; }
my $cur_sec = (($hour*3600)+($minute*60)+$second);
my $cur_time = "$hour:$minute:$second";
my @ar_time = '';
my $hr_format = '';
my @db_hr1 = '';
my $ar_ptr = '';
my $count_ptr = '';
my $req_time = '';
my $ref_time = '';
my @db_sec1 = '';
my @db_hr = '';
my $db_sec = '';
my $sec_format = '';
my @db_date1 = '';
my $db_mon = '';
my $db_day = '';
my $db_year = '';
my @req_time ='';
my @records = '';
my %rec = ();
my $cname='';
my $c_name='';
my $lpct = 0;
my (%names,%cnm) = '';
my $ns = '';
my $cnm_ipp = '';
my $temp_name = '';
my $type_rec = '';
my $zone_hash = '';
my $zone_count = 0;
my $revptr = '';
my $rev_ptr = '';
my $valuesdb = ''; 
my $ptr = '';
my $last_oct = '';
my $domain = '';
my $rec_type = '';
my $domain_new = '';
my @cnm_pre = '';
my $range = '';
my $cname_var = '';
my $ip_range = '';
my $host = "IN";
my $tempo = '';
my $cnm_tmp = '';
$reverse_del_hostname = "hhtred278.in.sa.telstrainternational.com";
$file_build_date = `date -u`;
my $base = '';
my @ar_ptr = '';
my $flag_ptr = 0;
my $i_ptr = 0;
my $countptr = '';
my $sel_ptr = '';
my $sel_ptr1 = '';
my $tempname = '';
my $name_s = '';
my $prev_last_oct = '';
my $status = '';

my $serial_number = "$yr$mth$mday";

CMS_DB::connect_to_database;
print "connected to db ....\n";

my @ptrvalue = '';
my $record = '';
my $dnstime = CMS_REVERSEDEL_DB::time_db;
my $allvalues = CMS_REVERSEDEL_DB::all_values;

my $ar1_ptr = '';
my $i = 0;
#get all distinct ptrrec from DBm
my $all_ptr = CMS_REVERSEDEL_DB::all_ptr_version(4);
foreach my $allptr (@$all_ptr){
	push (@ar_ptr, $$allptr[0]);
	$ar1_ptr = ($all_ptr->[0]->[0]);
	#print PRE "all ptr1:$ar1_ptr\n";
	print PRE "all ptr:$ar_ptr[1]\n";
	my $arptr = $ar_ptr[1];
#	$count_ptr = CMS_REVERSEDEL_DB::count_ptr($arptr);
	#$countptr = ($count_ptr->[0]->[0]);
	#print PRE "countptr:$countptr\n";
	#for (1 .. $countptr) {
	$sel_ptr1 = CMS_REVERSEDEL_DB::sel_ptr1($arptr);
	$sel_ptr1 = ($sel_ptr1->[0]->[0]);
	for $i (0 .. $sel_ptr1) {
		if ($i == $sel_ptr1){
		print PRE "inside i condn\n";
		$tempo = '';
		$cnm_tmp = '';
  		$temp_name = '';
		$base = '';
		$name_s = '';
		$ns = '';
		}#end of if loop for i
		print PRE "inside countptr loop\n";
		$sel_ptr = CMS_REVERSEDEL_DB::sel_ptr($arptr);
		print PRE "VALUE i:$i\n";
		print PRE "SEL1:$sel_ptr1\n";
		print PRE "SEL:$sel_ptr\n";
		#$valuesdb = CMS_REVERSEDEL_DB::values_db($arptr);
		$ptr = $sel_ptr->[$i]->[0];
		$last_oct = $sel_ptr->[$i]->[1];
		$rec_type = $sel_ptr->[$i]->[2];
		$domain = $sel_ptr->[$i]->[3];
		$req_time = $sel_ptr->[$i]->[4];
		$ref_time = $sel_ptr->[$i]->[5];
		$status = $sel_ptr->[$i]->[6];
		if ( defined $ptr ) {
		print PRE "ptr:$ptr\tlast_oct:$last_oct\trec_type:$rec_type\tdomain:$domain\treq_time:$req_time\tref_time:$ref_time\n"; 
		@db_hr1 = split(/\+/,$req_time);
		@db_hr = split(/ /,$db_hr1[0]);
		@db_sec1 = split(/\./,$db_hr[1]);
		$db_sec = (($db_sec1[0]*3600)+ ($db_sec1[1]*60) + ($db_sec1[2]));
		@db_date1 = split (/-/,$db_hr[0]);
		$sec_format = $db_sec;
		$hr_format = $db_hr1[0];
		@db_date1 = split (/-/,$db_hr[0]);
		$db_year = $db_date1[0];
		$db_mon = $db_date1[1];
		$db_day = $db_date1[2];
		

		#checkin request time against current time
		$mday =~ s/^\s+|\s+$//g;
		$mth =~ s/^\s+|\s+$//g;
		$mth =~ s/^\s+|\s+$//g;
		$sec_format =~ s/^\s+|\s+$//g;
		$hr_format =~ s/^\s+|\s+$//g;
		$db_year =~ s/^\s+|\s+$//g;
		$db_mon =~ s/^\s+|\s+$//g;
		$db_day =~ s/^\s+|\s+$//g;

		#time check against current time 
		print PRE "mday:$mday\tdb_day:$db_day\tmth:$mth\tdb_mon:$db_mon\tcur_sec:$cur_sec\tsec_format:$sec_format\n";

		if (($mday eq $db_day) && ($mth eq $db_mon) && ($yr eq $db_year) && (($cur_sec - $sec_format)<=3600) ){

			my $maildetails = CMS_REVERSEDEL_DB::maildetails($req_time);
			#gettin the details for mail
			my $mail_ptr =($maildetails->[0]->[0]);
			my $mail_last_oct = ($maildetails->[0]->[1]);
			my $mail_rec_type = ($maildetails->[0]->[2]);
			my $mail_domain = ($maildetails->[0]->[3]);
			my $mail_status = ($maildetails->[0]->[4]);
			my $mail_contact = ($maildetails->[0]->[5]);
			my $mail_accno = ($maildetails->[0]->[6]);
			my $mail_uid = ($maildetails->[0]->[7]);
			 		
			#gettin the IP block
			my @mail_last1 = split(/\./,$mail_ptr);
			my $mail_oct3 = shift(@mail_last1);
			my $mail_oct2 = shift(@mail_last1);
			my $mail_oct1 = shift(@mail_last1);
			my $mail_ip_blk = join ".",$mail_oct1,$mail_oct2,$mail_oct3,$mail_last_oct;

		 	print PRE "mail_ip_blk:$mail_ip_blk\tmail_domain:$mail_domain\tmail_contact:$mail_contact\tmail_uid:$mail_uid\tmail_accno:$mail_accno\n";		
			my $mail_req_type = '';
			if ($mail_status==1){ 
			$mail_req_type = "REQUESTED DNS SERVICE";
			}elsif ($mail_status==3) {
			$mail_req_type = "CANCELLED DNS SERVICE";
			}
			#open (FILE_MAIL,">>/usr/local/www/wanchai/cms/data/revdns/rev_mail/mail_details"); 
			
			open (FILE_MAIL,">>/usr/local/www/wanchai/netops/dns/revdns/rev_mail/mail_details/mail_details.$serial_number"); 
			print FILE_MAIL "$mail_req_type;$mail_contact;$mail_uid;$mail_ip_blk;$mail_last_oct;$mail_domain;$mail_accno;$mail_rec_type\n";
			close (FILE_MAIL);
			
			}
		 }#end of if req time condn 
		}#end of for	
	

shift @ar_ptr;
}#end of distinct ptr
print "mail_details.$serial_number is created.\n " ;

sub rev_range {
my ($rev_val) = @_;
print PRE "$rev_val: value in sub\n";
my $range = 0;
if ($rev_val ==24) {
                $range = 255;
        }elsif ($rev_val == 25) {
                 $range = 127;
        }elsif ($rev_val == 26) {
                 $range = 63;
        }elsif ($rev_val == 27 ){
                 $range = 31;
        }elsif ($rev_val == 28 ){
                 $range = 15;
        }elsif ($rev_val == 29 ){
                 $range = 7;
        }elsif ($rev_val == 30 ){
                 $range = 3;
        }elsif ($rev_val == 31 ){
                 $range = 1;
        }elsif ($rev_val == 32 ){
                 $range = 0;
        }
return $range;
}

