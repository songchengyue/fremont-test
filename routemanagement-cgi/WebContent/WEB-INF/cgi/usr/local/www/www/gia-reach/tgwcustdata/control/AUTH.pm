# (C) Telstra 2001
#
# Author: Troy Wooden (troy@telstra.net)
# Date: 4 July 2001
# File: AUTH.pm
#
# This file contains generic authentication procedures.
#
# $Id: AUTH.pm,v 1.1.1.1 2003/02/20 04:37:32 rchew Exp $

package AUTH;
use Exporter;
use strict;
use AUTH_DB;
use Time::localtime;

our @ISA	= qw(Exporter);
our @EXPORT_OK	= qw(authenticate_user generate_code);

my $timeout = 3000;			#Time out value in seconds
use Digest::MD5 qw(md5_hex);

# Error Codes
use constant SUCCESS => 1;
use constant ERROR => undef;

open(LOG, ">/tmp/leo-cms-AUTH.pm.log");

####Auth Stuff###################################################################################################################################################################
# Functions
#
# Authenticate User takes encrypted username, password pair and task
# level and checks with entry in database for this username.
#
# Requires:
#	$code		encrypted username, password pair.
#	$task		task level requested.
#
# Returns:
#	$authenticate
#	passed		correct code and permissions.
#	not allowed	no permissions for this task.
#	failed		incorrect code.
#
#
sub authenticate_user 
{
	my($username, $passedcode, $system, $task) = @_;
print LOG "sub-authenticate_user: username($username) passedcode($passedcode) system($system) task($task)\n";

	if (!$username || !$passedcode || !$system || !$task) 
	{
		return ERROR;
	}
	my $sysid=AUTH_DB::query_system($system);

	## First check if the user is allowed to login
	if(AUTH_DB::check_permission($username, $sysid, "login")==SUCCESS)
	{
		## Then check if the user is allowed to do the action specified in $task	
	        if(AUTH_DB::check_permission($username, $sysid, $task)==SUCCESS)
	        {
print LOG "sub-authenticate_user: user allowed to login and task($task) is allowed\n";

			my $code = AUTH_DB::get_code($username);
			my $currentcode = make_code_current($code);
			my $previouscode = make_code_previous($code);
print LOG "sub-authenticate_user: code($code) currentcode($currentcode) previouscode($previouscode)\n";

			#check the passedcode and currentcode to see if they match
			if($passedcode eq $currentcode)
			{
				return($currentcode);
			}

			#check the passedcode and previouscode to see if they match
			if($passedcode eq $previouscode)
			{
				return($currentcode);
			}
		}
	}
	return ERROR;
}

####Password Stuff###################################################################################################################################################################
# Generate Code takes clear text username and password 
# and generates encrypted code.
#
# Uses local sub routine encrypt.
#
# Requires:
#	$username	clear text username.
#	$password	clear text password.
#	$new		A flag that indiated if a new passord is to be generated without a timeout.
#			if new != "NEW" then generate a password with a timeout else generate a password with a timeout
#
# Returns:
#	$code		encrypted username, password pair.
#
#
sub generate_code 
{
	my($username, $password, $new) = @_;
print LOG "sub-generate_code: username($username) password($password) new($new)\n";

	if (!$username || !$password) 
	{
		return ERROR;
	}

	my $code = &encrypt($username, $password);

	if(!($new=~/NEW/))
	{
		$code = make_code_current($code);
	}
print LOG "sub-generate_code: code($code) after encrypt\n";

	return($code);
}

####Local Stuff###################################################################################################################################################################
# Local Sub Routines
#
# Encrypt clear text username and encrypted password pair and return
# encrypted code.
#
# Requires:
#	$username	clear text username.
#	$password	encrypted password.
#
# Returns:
#	$code		encrypted username, password pair.
#
#
sub encrypt {
	my($username, $password) = @_;
	my($usernamepassword)=sprintf("%s%s",$username,$password);
	my($code)=md5_hex($usernamepassword);
	return($code);
}

sub make_code_current
{
	my($code) = @_;

	my $unixtime = time();
	$unixtime=int($unixtime/$timeout);	
	$unixtime=$unixtime*$timeout;	

	my($currentcodestring)=sprintf("%s%s",$code,$unixtime);
	my($currentcode)=md5_hex($currentcodestring);

	return $currentcode;
}

sub make_code_previous
{
	my($code) = @_;

	my $unixtime = time();
	$unixtime=int($unixtime/$timeout);	
	$unixtime=$unixtime*$timeout;	
	$unixtime=$unixtime-$timeout;

	my($previouscodestring)=sprintf("%s%s",$code,$unixtime);
	my($previouscode)=md5_hex($previouscodestring);

	return $previouscode;
}

sub get_superuser
{
       my($username) = @_;
       open(DEBUG,">/tmp/super");
       print DEBUG "username is $username\n";
       my $authorized = AUTH_DB::get_auth($username); 
       print DEBUG "value of authorized is $authorized\n";
       if ($authorized){
       return 1;
       }
       return 0;
}
1;
