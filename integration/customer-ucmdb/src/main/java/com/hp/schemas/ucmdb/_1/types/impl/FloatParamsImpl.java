/*
 * XML Type:  FloatParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.FloatParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML FloatParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class FloatParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.FloatParams
{
    
    public FloatParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FLOATPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "floatParam");
    
    
    /**
     * Gets array of all "floatParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.FloatProp[] getFloatParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(FLOATPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.FloatProp[] result = new com.hp.schemas.ucmdb._1.types.FloatProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "floatParam" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatProp getFloatParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().find_element_user(FLOATPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "floatParam" element
     */
    public int sizeOfFloatParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FLOATPARAM$0);
        }
    }
    
    /**
     * Sets array of all "floatParam" element
     */
    public void setFloatParamArray(com.hp.schemas.ucmdb._1.types.FloatProp[] floatParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(floatParamArray, FLOATPARAM$0);
        }
    }
    
    /**
     * Sets ith "floatParam" element
     */
    public void setFloatParamArray(int i, com.hp.schemas.ucmdb._1.types.FloatProp floatParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().find_element_user(FLOATPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(floatParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "floatParam" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatProp insertNewFloatParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().insert_element_user(FLOATPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "floatParam" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatProp addNewFloatParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().add_element_user(FLOATPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "floatParam" element
     */
    public void removeFloatParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FLOATPARAM$0, i);
        }
    }
}
