/*
 * An XML document type.
 * Localname: addCIsAndRelations
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * A document containing one addCIsAndRelations(@http://schemas.hp.com/ucmdb/1/params/update) element.
 *
 * This is a complex type.
 */
public class AddCIsAndRelationsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsDocument
{
    
    public AddCIsAndRelationsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDCISANDRELATIONS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "addCIsAndRelations");
    
    
    /**
     * Gets the "addCIsAndRelations" element
     */
    public com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations getAddCIsAndRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations)get_store().find_element_user(ADDCISANDRELATIONS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "addCIsAndRelations" element
     */
    public void setAddCIsAndRelations(com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations addCIsAndRelations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations)get_store().find_element_user(ADDCISANDRELATIONS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations)get_store().add_element_user(ADDCISANDRELATIONS$0);
            }
            target.set(addCIsAndRelations);
        }
    }
    
    /**
     * Appends and returns a new empty "addCIsAndRelations" element
     */
    public com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations addNewAddCIsAndRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations)get_store().add_element_user(ADDCISANDRELATIONS$0);
            return target;
        }
    }
}
