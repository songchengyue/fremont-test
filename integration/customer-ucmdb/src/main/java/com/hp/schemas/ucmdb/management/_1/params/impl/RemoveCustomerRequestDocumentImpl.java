/*
 * An XML document type.
 * Localname: removeCustomerRequest
 * Namespace: http://schemas.hp.com/ucmdb/management/1/params
 * Java type: com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.management._1.params.impl;
/**
 * A document containing one removeCustomerRequest(@http://schemas.hp.com/ucmdb/management/1/params) element.
 *
 * This is a complex type.
 */
public class RemoveCustomerRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequestDocument
{
    
    public RemoveCustomerRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REMOVECUSTOMERREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/management/1/params", "removeCustomerRequest");
    
    
    /**
     * Gets the "removeCustomerRequest" element
     */
    public com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest getRemoveCustomerRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest)get_store().find_element_user(REMOVECUSTOMERREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "removeCustomerRequest" element
     */
    public void setRemoveCustomerRequest(com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest removeCustomerRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest)get_store().find_element_user(REMOVECUSTOMERREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest)get_store().add_element_user(REMOVECUSTOMERREQUEST$0);
            }
            target.set(removeCustomerRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "removeCustomerRequest" element
     */
    public com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest addNewRemoveCustomerRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest)get_store().add_element_user(REMOVECUSTOMERREQUEST$0);
            return target;
        }
    }
}
