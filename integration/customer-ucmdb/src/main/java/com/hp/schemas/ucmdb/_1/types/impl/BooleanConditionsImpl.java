/*
 * XML Type:  BooleanConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.BooleanConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML BooleanConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class BooleanConditionsImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.BooleanConditions
{
    
    public BooleanConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName BOOLEANCONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "booleanCondition");
    
    
    /**
     * Gets array of all "booleanCondition" elements
     */
    public com.hp.schemas.ucmdb._1.types.BooleanCondition[] getBooleanConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(BOOLEANCONDITION$0, targetList);
            com.hp.schemas.ucmdb._1.types.BooleanCondition[] result = new com.hp.schemas.ucmdb._1.types.BooleanCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "booleanCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanCondition getBooleanConditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanCondition)get_store().find_element_user(BOOLEANCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "booleanCondition" element
     */
    public int sizeOfBooleanConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BOOLEANCONDITION$0);
        }
    }
    
    /**
     * Sets array of all "booleanCondition" element
     */
    public void setBooleanConditionArray(com.hp.schemas.ucmdb._1.types.BooleanCondition[] booleanConditionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(booleanConditionArray, BOOLEANCONDITION$0);
        }
    }
    
    /**
     * Sets ith "booleanCondition" element
     */
    public void setBooleanConditionArray(int i, com.hp.schemas.ucmdb._1.types.BooleanCondition booleanCondition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanCondition)get_store().find_element_user(BOOLEANCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(booleanCondition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "booleanCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanCondition insertNewBooleanCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanCondition)get_store().insert_element_user(BOOLEANCONDITION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "booleanCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanCondition addNewBooleanCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanCondition)get_store().add_element_user(BOOLEANCONDITION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "booleanCondition" element
     */
    public void removeBooleanCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BOOLEANCONDITION$0, i);
        }
    }
}
