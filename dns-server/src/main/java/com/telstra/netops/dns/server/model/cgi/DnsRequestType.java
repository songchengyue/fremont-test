package com.telstra.netops.dns.server.model.cgi;

public enum DnsRequestType {

	RESOLVER(31), //
	PRIMARY(41), //
	REVERSE(51), //
	SECONDARY(61);
	
	private final Integer value;
	
	private DnsRequestType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}
	
	public static DnsRequestType findForValue(Integer value) {
		for (DnsRequestType ipVersion: DnsRequestType.values()) {
			if (value.equals(ipVersion.getValue())) {
				return ipVersion;
			}
		}
		
		throw new IllegalArgumentException("not such DnsRequestType for value " + value);
	}

}
