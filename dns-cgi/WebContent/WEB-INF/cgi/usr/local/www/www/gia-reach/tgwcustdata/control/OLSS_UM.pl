#!/usr/bin/perl
#
# Lib file : OLSS_UM.pl
# Written by: Richard Chew (Maettr Pty Ltd)
#		rchew@maettr.bpa.nu
# (C) Maettr Pty Ltd
#
# $Id: OLSS_UM.pl,v 1.3 2016/07/07 15:19:16 d804709 Exp $
#


############## FUNCTIONS ##################

# Generate the product table
# Pre: account number
# Post: @vals of html code
sub generate_product_table {
	my $accno = shift;

	my $products;
	my @ret;

	eval {
		$products = CMS_OLSS::get_product_codes_for_accno($accno);
		foreach my $k (sort keys %$products) {
			my $l = "<tr valign=\"top\"> ";
			$l .= "<td width=\"6\"><img src=\"images/bullet2.gif\" width=\"4\" height=\"7\" vspace=\"3\"></td>";
			$l .= "<td class=\"txtbody\">";
			$l .= "<a href=\"\*MP-$k\*\">$$products{$k}{name}</a></td></tr>\n";
			push @ret, $l;
		}
	};

	return @ret;

}

# Specific link-formatting for single sign-on
# Pre: line, code, product, level
# Post: line
sub product_link_format {
	my ($line, $code) = @_;

	if($line =~ /\*(MP-\w*)\*/) {
		my $a = $1;
		my $b = $a;
		$a =~ s/MP\-//g;
		$a = &ecrypt($a);
		$line =~ s/\*$b\*/tgwciss-login.cgi?code=$oenc&level=6&et=$a/g;
	}

	return $line;

}

1;
