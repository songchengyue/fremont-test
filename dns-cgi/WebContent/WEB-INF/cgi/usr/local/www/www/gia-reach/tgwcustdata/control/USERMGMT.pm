# (C) Telstra 2001
#
# Author: Ash Garg (ash.garg@telstra.net)
# Date: 20 Aug 2001
# File: USERMGMT.pm
#
# This file contains generic authentication procedures.
#
# $Id: USERMGMT.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $

package USERMGMT;
use Exporter;
use strict;
use AUTH;
use AUTH_DB;

our @ISA	= qw(Exporter);
our @EXPORT_OK	= qw(add_user change_password delete_user add_permission
		    delete_permission list_permissions);

# Error Codes
use constant SUCCESS => 1;
use constant ERROR => undef;


####User Mgmt stuff###################################################################################################################################################################
# Add user takes the current users username and code and the
# new users username and unencrypted password.
#
# It authenticates the current user and then inserts the new
# user into the database 
#
# Requires:
#	$current_username	clear text username.
#	$current_code	        encrypted password.
#
#       $add_username           clear text username
#       $add_password           clear text password
#
# Returns:
#	SUCCESS
#	ERROR
#
#
sub add_user 
{
	my($current_username, $current_code, $add_username, $add_password, $firstname, $lastname, $empno, $emailaddress, $phone, $workgroup) = @_;

	if (!defined $current_username ||  !defined $current_code ||  !defined $add_username || !defined $add_password || !defined $firstname || !defined $lastname || !defined $empno || !defined $emailaddress || !defined $phone || !defined $workgroup) 
	{
		return ERROR;
	}

	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "add_user")) 
	{
		  # adduser to database

		  my $add_code = AUTH::generate_code ($add_username, $add_password, "NEW");

		  return AUTH_DB::add_user($add_username, $add_code, $firstname, $lastname, $empno, $emailaddress, $phone, $workgroup);
	} 
		  return ERROR;
}

# Update User takes the username and code of the current user and clear
# text username and encrypted password pair and sends code, clear text username
# and encrypted password to the database.
#
# Requires:
#	$code		encrypted username, password pair.
#	$username	clear text username.
#	$password	encrypted password.
#
# Returns:
#	SUCCESS
#	ERROR
#
#
sub change_password 
{
        my($current_username, $current_code, $username, $password) = @_;

        if (!defined $current_username ||  !defined $current_code || !defined $username || !defined $password)
        {
                return ERROR;
        }

	if($username eq $current_username)
	{
		##User is trying to change their own password
		if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "change_my_password"))
       		{
			  my $add_code = AUTH::generate_code ($username, $password, "NEW");
			  return AUTH_DB::update_password($username, $add_code);
		}

	}
	else
	{
		##Someone else is trying to change $usernames password
		if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "change_password"))
       		{
			  my $add_code = AUTH::generate_code ($username, $password, "NEW");
			  return AUTH_DB::update_password($username, $add_code);
		}
	}	
	return ERROR;
}


# Delete user takes the username and code of the current user
# and the username of the user to be deleted.
# If authentication is approved, the user is deleted
#
# Requires:
#	$current_username	clear text username.
#       $current_code           encrypted code
#       $delete_username        user to be deleted
#
# Returns:
#	SUCCESS	
#	ERROR
#
#
sub delete_user 
{
	my($current_username, $current_code, $delete_username) = @_;

	if (!$current_code || !$current_username || !$delete_username) 
	{
		return ERROR;
	}
  
  
	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "delete_user")) 
	{
		# delete user from  database
		return AUTH_DB::delete_user($delete_username);
	} 
	return ERROR;
}

sub list_users
{
	my($current_username, $current_code, $orderby) = @_;

	if (!defined $current_code || !defined $current_username) 
	{
		return ERROR;
	}

	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "list_user")) 
	{
		if( !defined $orderby || $orderby=~/username/)
		{
			return AUTH_DB::list_user("username");
		}
		elsif($orderby=~/lastname/)
		{
			return AUTH_DB::list_user("lastname");
		}
		elsif($orderby=~/workgroup/)
		{
			return AUTH_DB::list_user("workgroup");
		}
		elsif($orderby=~/employeenumber/)
		{
			return AUTH_DB::list_user("employeenumber");
		}
	} 
	return ERROR;
}

sub list_user_details
{
	my($current_username, $current_code, $username) = @_;

	if (!defined $current_code || !defined $current_username || !defined $username) 
	{
		return ERROR;
	}

	if($username eq $current_username)
	{
		if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "list_my_details")) 
		{
			return AUTH_DB::list_user_details($username);
		}	 
	}
	else
	{
		if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "list_user_details")) 
		{
			return AUTH_DB::list_user_details($username);
		}	 
	}
	return ERROR;
}

sub update_user_details
{
	my($current_username, $current_code, $username, $firstname, $lastname, $workgroup, $employeeno, $emailaddress, $phone) = @_;

	if (!defined $current_code || !defined $current_username || !defined $username || !defined $firstname || !defined $lastname || !defined $workgroup || !defined $employeeno || !defined $emailaddress || !defined $phone) 
	{
		return ERROR;
	}

	if($username eq $current_username)
	{
		##User is trying to change their own details 
		if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "update_my_details")) 
		{
			return AUTH_DB::update_user_details($username, $firstname, $lastname, $workgroup, $employeeno, $emailaddress, $phone);
		}
	}
	else
	{
		##User is trying to change $usernames details 
		if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "update_user_details")) 
		{
			return AUTH_DB::update_user_details($username, $firstname, $lastname, $workgroup, $employeeno, $emailaddress, $phone);
		} 
	}
	return ERROR;
}


####Group Mgmt Stuff###################################################################################################################################################################

sub add_group
{
	my($current_username, $current_code, $add_group) = @_;

	if (!$current_username ||  !$current_code || !$add_group) 
	{
		return ERROR;
	}

	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "add_group")) 
	{
		  return AUTH_DB::add_group($add_group);
	} 
	return ERROR;
}

sub delete_group
{
	my($current_username, $current_code, $remove_group) = @_;

	if (!$current_username ||  !$current_code || !$remove_group) 
	{
		return ERROR;
	}

	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "remove_group")) 
	{
		  return AUTH_DB::remove_group($remove_group);
	} 
	return ERROR;
}

sub get_group_list
{
	my($current_username, $current_code) = @_;


	if (!$current_username ||  !$current_code ) 
	{
		return ERROR;
	}


	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "list_group")) 
	{
		return AUTH_DB::get_group_list();
	}
	return ERROR;
}

## returns an array with the following (system, task, 1|0)
## 1 = the system/task combo is part of the group 
## 0 = the system/task combo is NOT part of the group
sub list_task_for_group 
{
        my($current_username, $current_code, $group) = @_;


        if (!defined $current_username ||  !defined $current_code || !defined $group)
        {
                return ERROR;
        }


        if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "list_group_tasks"))
        {
                my $array_ref = AUTH_DB::get_group_task_list($group);
		foreach my $row (@$array_ref)
		{
			if(@$row[2] eq $group)
			{
				@$row[2]=1;
			}
			else
			{
				@$row[2]=0;
			}
		}
		return $array_ref;
        }
        return ERROR;
}
	
####Task Mgmt Stuff###############################################################################################################################################################

#Add user to Group
sub add_user_to_group
{
	my($current_username, $current_code, $add_username, $add_group) = @_;

	if (!$current_username ||  !$current_code ||  !$add_username || !$add_group) 
	{
		return ERROR;
	}

	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "add_user_to_group")) 
	{
		my $groupid=AUTH_DB::query_group($add_group);
		if($groupid!=ERROR)
		{
			return AUTH_DB::add_user_to_group($add_username, $groupid);
		}
	} 
	return ERROR;
}

#delete user from Group
sub delete_user_from_group
{
	my($current_username, $current_code, $remove_username, $remove_group) = @_;

	if (!$current_username ||  !$current_code ||  !$remove_username || !$remove_group) 
	{
		return ERROR;
	}

	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "remove_user_from_group")) 
	{
		my $groupid=AUTH_DB::query_group($remove_group);
		if($groupid!=ERROR)
		{
			return AUTH_DB::remove_user_from_group($remove_username, $groupid);
		}
	} 
	return ERROR;
}

## returns an array with the following (group, 1|0)
## 1 = the group is part of the username 
## 0 = the group is NOT part of the username
sub list_group_for_username
{
        my($current_username, $current_code, $username) = @_;


        if (!defined $current_username ||  !defined $current_code || !defined $username)
        {
                return ERROR;
        }


        if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "list_user_groups"))
        {
                my $array_ref = AUTH_DB::get_user_group_list($username);
		foreach my $row (@$array_ref)
		{
			if(@$row[1] eq $username)
			{
				@$row[1]=1;
			}
			else
			{
				@$row[1]=0;
			}
		}
		return $array_ref;
        }
        return ERROR;
}
	
sub add_task_to_group
{
	my($current_username, $current_code, $group, $systemname, $task) = @_;

	if (!$current_code || !$current_username || !$group || !$systemname || !$task)
	{
		return ERROR;
	}


	if (AUTH::authenticate_user ($current_username, $current_code, "usermgmt", "add_task_to_group"))
	{
		my $sysid=AUTH_DB::query_system($systemname);
		if($sysid!=ERROR)
		{
			my $systaskid=AUTH_DB::query_systemtask($sysid, $task);
			if($systaskid!=ERROR)
			{
				my $groupid=AUTH_DB::query_group($group);
				if($groupid!=ERROR)
				{
						AUTH_DB::add_task_to_group($groupid, $systaskid);
						return SUCCESS;
				}
			}
		}
	}
		return ERROR;
}

sub delete_task_from_group
{
	my($current_username, $current_code, $group, $systemname, $task) = @_;

	if (!$current_code || !$current_username || !$group || !$systemname || !$task)
	{
		return ERROR;
	}


	if (AUTH::authenticate_user ($current_username, $current_code, "usermgmt", "add_task_to_group"))
	{
		my $sysid=AUTH_DB::query_system($systemname);
		if($sysid!=ERROR)
		{
			my $systaskid=AUTH_DB::query_systemtask($sysid, $task);
			if($systaskid!=ERROR)
			{
				my $groupid=AUTH_DB::query_group($group);
				if($groupid!=ERROR)
				{
					AUTH_DB::remove_task_from_group($groupid, $systaskid);
					return SUCCESS;
				}
			}
		}
	}
		return ERROR;
}

####System Mgmt Stuff###################################################################################################################################################################
#Add System
sub add_system
{
	my($current_username, $current_code, $systemname) = @_;

	if (!$current_username ||  !$current_code ||  !$systemname) 
	{
		return ERROR;
	}

	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt" , "add_system")) 
	{
		  # adduser to group

		  return AUTH_DB::add_system($systemname);
	} 
	return ERROR;
}

#remove System
sub delete_system
{
	my($current_username, $current_code, $systemname) = @_;


	if (!$current_username ||  !$current_code || !$systemname) 
	{
		return ERROR;
	}


	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "remove_system")) 
	{
		  # adduser to group

		my $sysid=AUTH_DB::query_system($systemname);
		return AUTH_DB::remove_system($sysid);
	} 
	return ERROR;
}

#Getsystem List 
sub get_system_list
{
	my($current_username, $current_code) = @_;


	if (!$current_username ||  !$current_code ) 
	{
		return ERROR;
	}


	if (AUTH::authenticate_user($current_username, $current_code, "usermgmt", "list_system")) 
	{
		return AUTH_DB::get_system_list();
	}
	return ERROR;
}
	
####Task Mgmt Stuff###############################################################################################################################################################
# Add Permission takes code (encrypted username, password pair) and clear
# text username and task level and sends to the database.
#
# Requires:
#	$current_username	clear text username.
#	$current_code		encrypted username.
#	$group_name	        
#	$system
#	$task		        permission.
#
# Returns:
#	SUCCESS
#	ERROR
#

sub add_task
{
	my($current_username, $current_code, $systemname, $task) = @_;

	if (!$current_code || !$current_username || !$systemname || !$task)
	{
		return ERROR;
	}


	if (AUTH::authenticate_user ($current_username, $current_code, "usermgmt", "add_task"))
	{
		my $sysid=AUTH_DB::query_system($systemname);
		if($sysid!=ERROR)
		{
			AUTH_DB::add_task($sysid, $task);
			return SUCCESS;
		}
	}
		return ERROR;
}


# Requires:
#	$current_username	clear text username.
#	$current_code		encrypted username, password pair.
#	$add_username	        clear text username.
#	$task		        permission level.
#
# Returns:
#	SUCCESS
#	ERROR
#

sub delete_task
{
	my($current_username, $current_code, $systemname, $task) = @_;

	if (!$current_code || !$current_username || !$systemname || !$task) 
	{
		return ERROR;
	}


	if (AUTH::authenticate_user ($current_username, $current_code, "usermgmt", "delete_task")) 
	{
		my $sysid=AUTH_DB::query_system($systemname);
		if($sysid!=ERROR)
		{
			return AUTH_DB::delete_task($sysid, $task);
		}
		return ERROR;
	}
}




# List Permissions returns a 2D array containing the permissions for the
# user. Every permissions is listed with 1 or 0 in second column for 
# whether user has permission
#
# Requires:
#	$current_username	clear text username.
#	$current_code		encrypted username, password pair.
#	$username
#
# Returns:
#	SUCCESS
#	ERROR
#
sub list_tasks_for_user
{
	my($current_username, $current_code, $username) = @_;

	if (!$current_code || !$current_username || !$username) 
	{
		return ERROR;
	}

	if (AUTH::authenticate_user ($current_username, $current_code, "usermgmt", "list_task")) 
	{
		return AUTH_DB::list_task_by_username($username);
	} 
	return ERROR;
}

1;
