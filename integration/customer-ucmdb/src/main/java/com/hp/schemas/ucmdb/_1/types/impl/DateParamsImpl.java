/*
 * XML Type:  DateParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.DateParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML DateParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class DateParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.DateParams
{
    
    public DateParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DATEPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "dateParam");
    
    
    /**
     * Gets array of all "dateParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.DateProp[] getDateParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DATEPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.DateProp[] result = new com.hp.schemas.ucmdb._1.types.DateProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "dateParam" element
     */
    public com.hp.schemas.ucmdb._1.types.DateProp getDateParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().find_element_user(DATEPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "dateParam" element
     */
    public int sizeOfDateParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DATEPARAM$0);
        }
    }
    
    /**
     * Sets array of all "dateParam" element
     */
    public void setDateParamArray(com.hp.schemas.ucmdb._1.types.DateProp[] dateParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(dateParamArray, DATEPARAM$0);
        }
    }
    
    /**
     * Sets ith "dateParam" element
     */
    public void setDateParamArray(int i, com.hp.schemas.ucmdb._1.types.DateProp dateParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().find_element_user(DATEPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(dateParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "dateParam" element
     */
    public com.hp.schemas.ucmdb._1.types.DateProp insertNewDateParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().insert_element_user(DATEPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "dateParam" element
     */
    public com.hp.schemas.ucmdb._1.types.DateProp addNewDateParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().add_element_user(DATEPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "dateParam" element
     */
    public void removeDateParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DATEPARAM$0, i);
        }
    }
}
