package com.telstra.netops.routemanagement.server.model;

import java.io.Serializable;

public class RouterInformationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String telstraServiceId;

	private String customerId;

	private String productLine;

	private String router;

	private String routerSoftware;

	private String routerInterface;

	private String parentSiteId;

	private String asNumber;

	private String bgpNeighborIp;

	private String irrdObject;

	private String bhaBgpNeighborIp;

	private String blackHoleRouter;

	private String bgpNeighborIpV6;

	private String bhaBgpNeighborIpV6;

	public RouterInformationDTO() {
		super();
	}

	public RouterInformationDTO(String id, String telstraServiceId, String customerId, String productLine,
			String router, String router_software, String routerInterface, String parentSiteId, String asNumber,
			String bgpNeighborIp, String irrdObject, String bhaBgpNeighborIp, String blackHoleRouter,
			String bgpNeighborIpV6, String bhaBgpNeighborIpV6) {
		super();
		this.id = id;
		this.telstraServiceId = telstraServiceId;
		this.customerId = customerId;
		this.productLine = productLine;
		this.router = router;
		this.routerSoftware = router_software;
		this.routerInterface = routerInterface;
		this.parentSiteId = parentSiteId;
		this.asNumber = asNumber;
		this.bgpNeighborIp = bgpNeighborIp;
		this.irrdObject = irrdObject;
		this.bhaBgpNeighborIp = bhaBgpNeighborIp;
		this.blackHoleRouter = blackHoleRouter;
		this.bgpNeighborIpV6 = bgpNeighborIpV6;
		this.bhaBgpNeighborIpV6 = bhaBgpNeighborIpV6;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTelstraServiceId() {
		return telstraServiceId;
	}

	public void setTelstraServiceId(String telstraServiceId) {
		this.telstraServiceId = telstraServiceId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProductLine() {
		return productLine;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}

	public String getRouter() {
		return router;
	}

	public void setRouter(String router) {
		this.router = router;
	}

	public String getRouterSoftware() {
		return routerSoftware;
	}

	public void setRouterSoftware(String routerSoftware) {
		this.routerSoftware = routerSoftware;
	}

	public String getRouterInterface() {
		return routerInterface;
	}

	public void setRouterInterface(String routerInterface) {
		this.routerInterface = routerInterface;
	}

	public String getParentSiteId() {
		return parentSiteId;
	}

	public void setParentSiteId(String parentSiteId) {
		this.parentSiteId = parentSiteId;
	}

	public String getAsNumber() {
		return asNumber;
	}

	public void setAsNumber(String asNumber) {
		this.asNumber = asNumber;
	}

	public String getBgpNeighborIp() {
		return bgpNeighborIp;
	}

	public void setBgpNeighborIp(String bgpNeighborIp) {
		this.bgpNeighborIp = bgpNeighborIp;
	}

	public String getIrrdObject() {
		return irrdObject;
	}

	public void setIrrdObject(String irrdObject) {
		this.irrdObject = irrdObject;
	}

	public String getBhaBgpNeighborIp() {
		return bhaBgpNeighborIp;
	}

	public void setBhaBgpNeighborIp(String bhaBgpNeighborIp) {
		this.bhaBgpNeighborIp = bhaBgpNeighborIp;
	}

	public String getBlackHoleRouter() {
		return blackHoleRouter;
	}

	public void setBlackHoleRouter(String blackHoleRouter) {
		this.blackHoleRouter = blackHoleRouter;
	}

	public String getBgpNeighborIpV6() {
		return bgpNeighborIpV6;
	}

	public void setBgpNeighborIpV6(String bgpNeighborIpV6) {
		this.bgpNeighborIpV6 = bgpNeighborIpV6;
	}

	public String getBhaBgpNeighborIpV6() {
		return bhaBgpNeighborIpV6;
	}

	public void setBhaBgpNeighborIpV6(String bhaBgpNeighborIpV6) {
		this.bhaBgpNeighborIpV6 = bhaBgpNeighborIpV6;
	}

}
