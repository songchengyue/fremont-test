/*
 * XML Type:  ChunkRequest
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ChunkRequest
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ChunkRequest(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ChunkRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ChunkRequest
{
    
    public ChunkRequestImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHUNKNUMBER$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "chunkNumber");
    private static final javax.xml.namespace.QName CHUNKINFO$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "chunkInfo");
    
    
    /**
     * Gets the "chunkNumber" element
     */
    public int getChunkNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHUNKNUMBER$0, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "chunkNumber" element
     */
    public org.apache.xmlbeans.XmlInt xgetChunkNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(CHUNKNUMBER$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "chunkNumber" element
     */
    public void setChunkNumber(int chunkNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHUNKNUMBER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CHUNKNUMBER$0);
            }
            target.setIntValue(chunkNumber);
        }
    }
    
    /**
     * Sets (as xml) the "chunkNumber" element
     */
    public void xsetChunkNumber(org.apache.xmlbeans.XmlInt chunkNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(CHUNKNUMBER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(CHUNKNUMBER$0);
            }
            target.set(chunkNumber);
        }
    }
    
    /**
     * Gets the "chunkInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkInfo getChunkInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().find_element_user(CHUNKINFO$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "chunkInfo" element
     */
    public void setChunkInfo(com.hp.schemas.ucmdb._1.types.ChunkInfo chunkInfo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().find_element_user(CHUNKINFO$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().add_element_user(CHUNKINFO$2);
            }
            target.set(chunkInfo);
        }
    }
    
    /**
     * Appends and returns a new empty "chunkInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkInfo addNewChunkInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().add_element_user(CHUNKINFO$2);
            return target;
        }
    }
}
