# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 15 October 2001
# File: CMS_TROUBLE_TICKET.pm
#
# This module contains subroutines for the trouble ticket subroutine
#
# $Id: CMS_TROUBLE_TICKET.pm,v 1.1.1.1 2003/10/19 13:49:52 rxc Exp $

package CMS_TROUBLE_TICKET;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_TROUBLE_TICKET_DB;

use lib "../../modules";

our @ISA        = qw(Exporter);




sub start {

  if (not defined $CMS::html_values{command}{value}) {
    $CMS::html_values{command}{value} = "view_current_tickets";
  }

 if (defined $CMS::html_values{submit}{value} && 
     $CMS::html_values{submit}{value} =~ /clear/i) {
   $CMS::html_values{command}{value} = "search_trouble_tickets";
 }

  if (($CMS::html_values{command}{value} eq "view_current_tickets") ||
      ($CMS::html_values{command}{value} eq "search_trouble_tickets")) {


    my $temp = $CMS::html_values{command}{value};
    CMS::clear_html_values();
    $CMS::html_values{subsystem}{value} = "troubleticket";
    $CMS::html_values{command}{value} = $temp;
    
    if ($CMS::html_values{command}{value} eq "view_current_tickets") {
      
      $CMS::html_values{trouble_tickets}{table} = 
	CMS_TROUBLE_TICKET_DB::get_current_trouble_tickets();
      
      foreach my $entry (@{$CMS::html_values{trouble_tickets}{table}}) {
	$$entry[0] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=troubleticket&command=open&trackno=$$entry[0]\">$$entry[0]</a>"
      }
      
      unshift(@{$CMS::html_values{trouble_tickets}{table}},
	      ["Tracking Number", "Reported", "Acc. No.", "Op's Handle",
	       "Status", "Fault"]);
      $CMS::html_values{trouble_tickets}{header_rows} = 1;
    }
    
    $CMS::html_values{"search_reported.endyear"}{value} = ((gmtime(time))[5]+1900);
    $CMS::html_values{"search_reported.endmonth"}{value} = 12;
    $CMS::html_values{"search_reported.endday"}{value} = 31;
    $CMS::html_values{"search_reported.endhour"}{value} = 23;
    $CMS::html_values{"search_reported.endminute"}{value} = 59;
    $CMS::html_values{"search_reported.endsecond"}{value} = 59;
    

    $CMS::html_values{search_status}{options} = 
      CMS_TROUBLE_TICKET_DB::get_status;


    CMS::output_html("trouble_ticket_list");


  } elsif ($CMS::html_values{command}{value} eq "search") {
    # search
    my ($search_reported_start, $search_reported_end) = 
      TI_HTML::get_datetime_search_values(\%CMS::html_values, 
					  "search_reported");
    $CMS::html_values{trouble_tickets}{table}=
      CMS_TROUBLE_TICKET_DB::search_trouble_tickets
	  ($CMS::html_values{search_trackno}{value},
	   $search_reported_start, $search_reported_end,
	   $CMS::html_values{search_accno}{value},
	   $CMS::html_values{search_opshandle}{value},
	   $CMS::html_values{search_status}{value},
	   $CMS::html_values{search_fault}{value});
    
    foreach my $entry (@{$CMS::html_values{trouble_tickets}{table}}) {
      $$entry[0] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=troubleticket&command=open&trackno=$$entry[0]\">$$entry[0]</a>"
    }
    
    unshift(@{$CMS::html_values{trouble_tickets}{table}},
	    ["Tracking Number", "Reported", "Acc. No.", "Op's Handle",
	     "Status", "Fault"]);
    $CMS::html_values{trouble_tickets}{header_rows} = 1;
    
    $CMS::html_values{search_status}{options} = 
      CMS_TROUBLE_TICKET_DB::get_status;


    $CMS::html_values{othermenu}{value} = "Search Results";

    CMS::output_html("trouble_ticket_list");
  
    
    
  } elsif (($CMS::html_values{command}{value} eq "open") || 
	   ($CMS::html_values{command}{value} eq "update")) {
    
    if ($CMS::html_values{command}{value} eq "update") {

      if ($CMS::html_values{add_comment}{value} ne "") {
	
	CMS_TROUBLE_TICKET_DB::add_comment
	    ($CMS::html_values{trackno}{value},
	     $CMS::html_values{username}{value},
	     $CMS::html_values{add_comment}{value});
	$CMS::html_values{add_comment}{value} = "";
      } 
      # this is a bit of a fudge because it always updates
      # even when no change. FIX THIS!!
      CMS_TROUBLE_TICKET_DB::update_status
	  ($CMS::html_values{trackno}{value},
	   $CMS::html_values{status}{value});
    
      CMS::log("trouble_ticket", "updated");
    }

    $CMS::html_values{status}{options} = 
      CMS_TROUBLE_TICKET_DB::get_status;

    ($CMS::html_values{trackno}{value},
     $CMS::html_values{contact_name}{value},
     $CMS::html_values{contact_phone}{value},
     $CMS::html_values{contact_email}{value},
     $CMS::html_values{company_name}{value},
     $CMS::html_values{accno}{value},
     $CMS::html_values{opshandle}{value},
     $CMS::html_values{fault}{value},
     $CMS::html_values{reported}{value},
     $CMS::html_values{status}{value},
     $CMS::html_values{fault}{value}) = 
       @{CMS_TROUBLE_TICKET_DB::get_trouble_ticket
	   ($CMS::html_values{trackno}{value})};

    $CMS::html_values{othermenu}{value} = "View Trouble Ticket";

    
    $CMS::html_values{comments}{table} = 
      CMS_TROUBLE_TICKET_DB::get_comments
	($CMS::html_values{trackno}{value});
    unshift(@{$CMS::html_values{comments}{table}},
	    ["Username", "Time", "Comment"]);
    
    $CMS::html_values{comments}{header_rows} = 1;

    CMS::output_html("trouble_ticket");

	   
 
  }

  
} 

1;
