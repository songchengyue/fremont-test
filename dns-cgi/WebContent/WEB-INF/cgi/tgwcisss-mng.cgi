#!/usr/local/bin/perl

#
# Written by: Meena Prakash 
# TGW Custdata Manage Your Services script
#
# $Id: tgwcisss-mng.cgi,v 1.8 2016/07/20 15:35:21 d804709 Exp $
#

no warnings;
#use lib 'D:\TG\DNSapp\DNSApplication workspace\DNSResolver\WebContent\WEB-INF\cgi\usr\local\www\www\gia-reach\tgwcustdata\control';
use lib './usr/local/www/www/gia-reach/tgwcustdata/control';


#use DB_File;
$| = 1;
use Net::Telnet;
use Net::Telnet::Cisco;
require "ctime.pl";
use Time::Local;
use Socket;
use JSON::PP;      
use POSIX ":sys_wait_h";
use OS_PARA;

use Log::Log4perl;
#Logger Initialisation
my $log_conf =$OS_PARA::values{logfile}{value}; 
Log::Log4perl::init($log_conf);
our $logger = Log::Log4perl->get_logger();


#IPT_ph5
use CGI;
use File::Basename;
use CGI::Carp qw ( fatalsToBrowser );
use OAuth2Token;

my $req = new CGI;

# check authorization

sub oauth2_error($) {
    my ($message) = @_;
    print "Status: 401 Unauthorized\n";
    print "Content-type: application/json\n\n";
    my %error = ('error' => $message);
    my $json = encode_json \%error;
    print $json;
    $logger->error($message);
}

# validate token
my $token = $req->http('OAuth2-Bearer-Token');
if (!$token) {
    oauth2_error("Missing HTTP header \"OAuth2-Bearer-Token\"");
    exit;
}

my $invalid = OAuth2Token::invalid($token);
if ($invalid) {
    oauth2_error("Invalid token $token: $invalid");
    exit;
} else {
    $logger->debug("OAuth2 token check successful");
}

my $input = decode_json( $req->param('POSTDATA') );

open(DEBUG,">/data1/tmp_log/Ph5_DEBUG_mng");
my $custId =  $input->{'customerId'};
my $svc = $input->{'svc'};
my $level = $input->{'level'};
my $service_id = $input->{'service_id'};
my $et = $input->{'et'};
my $term = $input->{'term'};
my $level_mng = $input->{'levelmng'};
my $ip_version=$input->{'ip_version'};
my $DNS_CONTACT =  $input->{'DNS_CONTACT'};

my $DNS_HOST =  $input->{'DNS_HOST'};

my $dom =  $input->{'dom'};
my $code =  $input->{'code'};
my $ip_version =  $input->{'ip_version'};

my $DNS_IP =  $input->{'DNS_IP'};
my $btnCheck =  $input->{'btnCheck'};
my $PDNS_CONTACT = $input->{'PDNS_CONTACT'};
my $PDNS_DOM = $input->{'PDNS_DOM'};
my $TYPE_ID = $input->{'TYPE_ID'};
my $PDNS_HOST = $input->{'PDNS_HOST'};
my $PDNS_AIP4 = $input->{'PDNS_AIP4'};
my $PDNS_AIP6 = $input->{'PDNS_AIP6'};
my $PDNS_CNHOST = $input->{'PDNS_CNHOST'};
my $PDNS_MXPV = $input->{'PDNS_MXPV'};
my $PDNS_MXEX = $input->{'PDNS_MXEX'};
my $PDNS_NS = $input->{'PDNS_NS'};
my $PDNS_TTTEXT = $input->{'PDNS_TTTEXT'};
my $PDNS_DBROWID = $input->{'PDNS_DBROWID'};
my $btnCheck3 = $input->{'btnCheck3'};
my $RevD_CONTACT = $input->{'RevD_CONTACT'};
my $REV_TYPE = $input->{'REV_TYPE'};
my $RevD_IP = $input->{'RevD_IP'};
my $RevD_Dom = $input->{'RevD_Dom'};
my $RevD_IPB = $input->{'RevD_IPB'};
my $RevD_NS = $input->{'RevD_NS'};
my $btnCheck1 = $input->{'btnCheck1'};
my $btnCheck9 = $input->{'btnCheck9'};
my $SDNS_CONTACT = $input->{'SDNS_CONTACT'};
my $SDNS_DOM = $input->{'SDNS_DOM'};
my $SDNS_IP = $input->{'SDNS_IP'};
my $SDNS_DBROWID = $input->{'SDNS_DBROWID'};
my $btnChecksec = $input->{'btnChecksec'};
my $NTP_CONTACT = $input->{'NTP_CONTACT'};
my $NTP_HOST = $input->{'NTP_HOST'};
my $NTP_IP = $input->{'NTP_IP'};
my $btnCheck2 = $input->{'btnCheck2'};
my $revd_cont = $input->{'revd_cont'};
my $rev_type = $input->{'rev_type'};
my $rev_dom = $input->{'rev_dom'};
my $btnCheck3 = $input->{'btnCheck3'};
my $ip_rev = $input->{'ip_rev'};
my $REV_VIEW = $input->{'REV_VIEW'};
my $VIEWDNS_SUBMIT = $input->{'VIEWDNS_SUBMIT'};
my $DNS_SUBMIT = $input->{'DNS_SUBMIT'};
my $PRIMARY_ADD_SUBMIT = $input->{'PRIMARY_ADD_SUBMIT'};
my $PRIMARY_VIEW_SUBMIT = $input->{'PRIMARY_VIEW_SUBMIT'};
my $PRIMARY_REM_SUBMIT = $input->{'PRIMARY_REM_SUBMIT'};
my $PRIMARY_RELOAD_SUBMIT = $input->{'PRIMARY_RELOAD_SUBMIT'};
my $SEC_ADD_SUBMIT = $input->{'SEC_ADD_SUBMIT'};
my $SEC_VIEW_SUBMIT = $input->{'SEC_VIEW_SUBMIT'};
my $SEC_RELOAD_SUBMIT = $input->{'SEC_RELOAD_SUBMIT'};
my $VIEWNTP_SUBMIT = $input->{'VIEWNTP_SUBMIT'};
my $NTP_SUBMIT = $input->{'NTP_SUBMIT'};
#end IPT_ph5
#IPSA_CR Maneet kaur 
my $RevM_CONTACT = $input->{'RevM_CONTACT'};
our $ptrval='';

our $dns_code;
our $final_ip_val = '';
our $final_dom_val = '';
our $mrw_no = '';

# Print errors to browser, rather than server 500 internal errors

use CGI::Carp qw/fatalsToBrowser/;


require "$OS_PARA::values{libfile}{value}";
print "\n";

# Referrer re-write URLNAME
if($ENV{HTTP_HOST} =~ /myservices.telstra-global.com|pccw-services.pccw.com|134.159.2.123/) {
        $urlname = $ENV{HTTP_HOST};
        $rmask = 1; #used for remasking as DocRoot is reseller-telstra/reseller-pccw
}


## Definitions ##
our %formels;
our $custfrom = &parse_form_data(*formels);

#IPT_ph5
open(DEBUG2,">/data1/tmp_log/Ph5_DEBUG2");
print DEBUG2 " before setting formels{'level'}:$formels{'level'}\n";
my $route_te = $req->param("route");
print DEBUG2 " route:$route_te\n";
if ($formels{'level'} eq ""){
$formels{'ip_version'}=$ip_version;
$formels{'level'} = $level;
$formels{'customerId'} = $custId;
$formels{'service_id'} = $service_id;
$formels{'svc'} = $svc;
$formels{'et'} = $et;
$formels{'terms'} = $term;
$formels{'levelmng'} = $level_mng;
$formels{'DNS_CONTACT'} = $DNS_CONTACT;
$formels{'DNS_HOST'} = $DNS_HOST;
$formels{'DNS_IP'} = $DNS_IP;
$formels{'btnCheck'} = $btnCheck;
$formels{'PDNS_CONTACT'} = $PDNS_CONTACT;
$formels{'PDNS_DOM'} = $PDNS_DOM;
$formels{'TYPE_ID'} = $TYPE_ID;
$formels{'PDNS_HOST'} = $PDNS_HOST;
$formels{'PDNS_AIP4'} = $PDNS_AIP4;
$formels{'PDNS_AIP6'} = $PDNS_AIP6;
$formels{'PDNS_CNHOST'} = $PDNS_CNHOST;
$formels{'PDNS_MXPV'} = $PDNS_MXPV;
$formels{'PDNS_MXEX'} = $PDNS_MXEX;
$formels{'PDNS_NS'} = $PDNS_NS;
$formels{'PDNS_TTTEXT'} = $PDNS_TTTEXT;
$formels{'PDNS_DBROWID'} = $PDNS_DBROWID;
$formels{'btnCheck3'} = $btnCheck3;
$formels{'RevD_CONTACT'} = $RevD_CONTACT;
$formels{'REV_TYPE'} = $REV_TYPE;
$formels{'RevD_IP'} = $RevD_IP;
$formels{'RevD_Dom'} = $RevD_Dom;
$formels{'RevD_IPB'} = $RevD_IPB;
$formels{'RevD_NS'} = $RevD_NS;
$formels{'btnCheck1'} = $btnCheck1;
$formels{'btnCheck3'} = $btnCheck3;
$formels{'btnCheck9'} = $btnCheck9;
$formels{'SDNS_CONTACT'} = $SDNS_CONTACT;
$formels{'SDNS_DOM'} = $SDNS_DOM;
$formels{'SDNS_IP'} = $SDNS_IP;
$formels{'SDNS_DBROWID'} = $SDNS_DBROWID;
$formels{'btnChecksec'} = $btnChecksec;
$formels{'NTP_CONTACT'} = $NTP_CONTACT;
$formels{'NTP_HOST'} = $NTP_HOST;
$formels{'NTP_IP'} = $NTP_IP;
$formels{'btnCheck2'} = $btnCheck2;
$formels{'revd_cont'} = $revd_cont;
$formels{'rev_type'} = $rev_type;
$formels{'rev_dom'} = $rev_dom;
$formels{'btnCheck3'} = $btnCheck3;
$formels{'ip_rev'} = $ip_rev;
$formels{'REV_VIEW'} = $REV_VIEW;
$formels{'VIEWDNS_SUBMIT'} = $VIEWDNS_SUBMIT;
$formels{'DNS_SUBMIT'} = $DNS_SUBMIT;
$formels{'PRIMARY_ADD_SUBMIT'} = $PRIMARY_ADD_SUBMIT;
$formels{'PRIMARY_VIEW_SUBMIT'} = $PRIMARY_VIEW_SUBMIT;
$formels{'PRIMARY_REM_SUBMIT'} = $PRIMARY_REM_SUBMIT;
$formels{'PRIMARY_RELOAD_SUBMIT'} = $PRIMARY_RELOAD_SUBMIT;
$formels{'SEC_ADD_SUBMIT'} = $SEC_ADD_SUBMIT;
$formels{'SEC_VIEW_SUBMIT'} = $SEC_VIEW_SUBMIT;
$formels{'SEC_RELOAD_SUBMIT'} = $SEC_RELOAD_SUBMIT;
$formels{'VIEWNTP_SUBMIT'} = $VIEWNTP_SUBMIT;
$formels{'NTP_SUBMIT'} = $NTP_SUBMIT;


print DEBUG2 " formels{'level'}:$formels{'level'}\n";
#IPSA_CR Maneet Kaur
$formels{'RevM_CONTACT'} = $RevM_CONTACT;
}
#end IPT_ph5


our $currtime = time;


our $result = $formels{'customerId'};
$logger->debug("customerId------->>>>>>>>>>>: $result");
if($result =~ /FAIL/) {
        &print_error("Cannot validate user: $result");
}

# Set some meaningful vars
our $uid = $result;
our $userid = $uid;
our $oenc = &encrypt($uid,$upass,$currtime);
our ($accno, $staffid) = &ex_staffid($uid);
our $logo_accno = $accno;
our $tmplroot = $tgwr."control/";

&log_entry($custlogfile, "MNGSERV", "$uid");


our $tdate = gmtime(time);
$tdate .= " GMT";


open(DEBUG,">/tmp/h999151_tnc");

our $etproduct = $formels{et};

my $ac = ($mrwaccno) ? $mrwaccno : $accno;


my $letproduct = lc($etproduct);

$tmplroot .= "/" . $letproduct;


our $ctrlname = "manage_services";

##Level is set here
my $menu_level = $formels{'level'}; 


SWITCH: {

        ($formels{'level'} == 1) && do {
                print DEBUG "Entering level 1\n";
                print DEBUG "level 1 code:$oenc \n";
                &local_print1($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        };

	($formels{'level'} == 2) && do {
                $dns_code = $oenc;
                print DEBUG "Entering level 2\n";
                print DEBUG "level 2 code:$oenc\n";
                &local_print_s2($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        };


        ($formels{'level'} == 3) && do {
		#DNS Resolver
                print DEBUG "Entering level 3\n";
                print DEBUG "level 3 code:$oenc\n";
                #print "Location: https://www-uat.net.reach.com/cgi-bin/gia-termsofuse.cgi?code=$oenc&level=1\n\n";
                #&local_print_s3($userid, $oenc, $accno, $tmplroot);
                &local_printg1($userid, $oenc, $accno, $tmplroot,$menu_level);
                last SWITCH;
        };
        ($formels{'level'} == 4) && do {
		#Primary Name Server
                print DEBUG "Entering level 4\n";
                print DEBUG "level 4 menu_level:$menu_level\n";
                #&local_print_s4($userid, $oenc, $accno, $tmplroot);
                &local_printg1($userid, $oenc, $accno, $tmplroot,$menu_level);
                last SWITCH;
        };
        ($formels{'level'} == 5) && do {
		#Reverse Delegation/Mapping
                print DEBUG "Entering level 5\n";
                print DEBUG "level 5 code:$oenc\n";
                #&local_print_s5($userid, $oenc, $accno, $tmplroot);
                &local_printg1($userid, $oenc, $accno, $tmplroot,$menu_level);
                last SWITCH;
        };
        ($formels{'level'} == 6) && do {
		#Secondary Name Server
		$mrw_no = $mrwaccno;
                print DEBUG "Entering level 6\n";
                print DEBUG "level 6 code:$oenc\n";
                #&local_print_s6($userid, $oenc, $accno, $tmplroot);
                &local_printg1($userid, $oenc, $accno, $tmplroot,$menu_level);
                last SWITCH;
        };
	($formels{'level'} == 7) && do {
		#NTP 
		$ntp_code = $oenc;
                print DEBUG "Entering Level 7\n";
                #&local_print_s7($userid, $oenc, $accno, $tmplroot);
                &local_printg1($userid, $oenc, $accno, $tmplroot,$menu_level);
                last SWITCH;
        };
	($formels{'level'} == 8) && do {
		print DEBUG "Entering Level 8\n";
		$level_mng = $formels{'levelmng'};
		print DEBUG "level_mng in level 8:$level_mng\n";
		&local_printg2($userid, $oenc, $accno, $tmplroot,$menu_level);
		last SWITCH;
	};
#IPT_ph5
	($formels{'level'} == 5002) && do {
                &local_print2($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        };
	#end IPT_ph5



        # sub-levels
        $level = $formels{'level'};
	$logger->debug("level------->>>>>>>>>>>: $level");
        ($formels{'level'} == 31) && do {
                #print DEBUG "Entering level 31\n";  #edited to store values line no.369
			$logger->debug("level resolver------->>>>>>>>>>>: $formels{'service_id'}");
	            my $dnsresolverList = &dns_trace($uid, $accno, $formels{'DNS_CONTACT'}, $formels{'DNS_HOST'}, $formels{'DNS_IP'}, $mrwaccno, $oenc, $formels{'btnCheck'},$formels{'ip_version'},$formels{'service_id'});
				print "Content-Type: application/json\n\n";
				print $dnsresolverList;
				last SWITCH;
        };
        ($formels{'level'} == 41) && do {
	my $level = $formels{'level'};
                #print DEBUG "Entering level 41\n";
			# $oenc,
                 
                  # $logger->debug("level primary------->>>>>>>>>>>: $formels{'service_id'}");
                my $pri_dns_output = &primary_trace($uid, $accno, $level, $formels{'PDNS_CONTACT'}, $formels{'PDNS_DOM'}, $formels{'TYPE_ID'}, $formels{'PDNS_HOST'}, $formels{'PDNS_AIP4'},$formels{'PDNS_AIP6'}, $formels{'PDNS_CNHOST'}, $formels{'PDNS_MXPV'}, $formels{'PDNS_MXEX'}, $formels{'PDNS_NS'},  $formels{'PDNS_TTTEXT'}, $formels{'PDNS_DBROWID'},$formels{'service_id'}, $mrwaccno);
				
				print "Content-Type: application/json\n\n";
				print $pri_dns_output;
                last SWITCH;
        };
#IPSA_CR Maneet Kaur
	($formels{'level'} == 51) && do {
		# $formels{'btnCheck3'} = 10;
                print DEBUG "Entering level 51\n";
			$logger->debug("level-------in 51>: $level");

	my $rev_dns_output;
		if($formels{'REV_TYPE'} == 10){
			$logger->debug("level-------in 10");
         $logger->debug("level rev------->>>>>>>>>>>: $formels{'service_id'}");

			$rev_dns_output = &rev_trace($uid,$accno, $formels{'RevM_CONTACT'}, $formels{'REV_TYPE'}, $formels{'RevD_IP'}, $formels{'RevD_Dom'}, $formels{'RevD_IPB'}, $formels{'RevD_NS'}, $mrwaccno, $oenc, $formels{'btnCheck1'}, $formels{'btnCheck3'}, $formels{'btnCheck9'},$formels{'ip_version'},$formels{'service_id'});
		}
		else{

			$logger->debug("level rev-------NOT in 10");

			$rev_dns_output = &rev_trace($uid,$accno, $formels{'RevD_CONTACT'}, $formels{'REV_TYPE'}, $formels{'RevD_IP'}, $formels{'RevD_Dom'}, $formels{'RevD_IPB'}, $formels{'RevD_NS'}, $mrwaccno, $oenc, $formels{'btnCheck1'}, $formels{'btnCheck3'}, $formels{'btnCheck9'},$formels{'ip_version'},$formels{'service_id'});	
		}
##--IPSA_CR
		print "Content-Type: application/json\n\n";
		print $rev_dns_output;
        last SWITCH;
        };
        ($formels{'level'} == 61) && do {
        my $level = $formels{'level'};
        my $service_id = $formels{'service_id'};
                print DEBUG "Entering level 61\n";
		 $logger->debug("Secondary DNS at level------->>>>>>>>>>>: $uid, $oenc, $accno, $level, $formels{'SDNS_CONTACT'}, $formels{'SDNS_DOM'}, $formels{'SDNS_IP'}, $formels{'SDNS_DBROWID'}, $mrwaccno,$formels{'btnChecksec'}, $formels{'service_id'}");
                 my $sec_dns_output = &sec_trace($uid, $oenc, $accno, $level, $formels{'SDNS_CONTACT'}, $formels{'SDNS_DOM'}, $formels{'SDNS_IP'}, $formels{'SDNS_DBROWID'}, $mrwaccno,$formels{'btnChecksec'},$formels{'service_id'});
		$logger->debug("Secondary DNS------->>>>>>>>>>>: $sec_dns_output");
	print "Content-Type: application/json\n\n";
	print $sec_dns_output;
                last SWITCH;
	};
	
        &print_error("Level unrecognised");
        exit;
};



# REQUEST FOR DNS RESOLUTION
# Pre: $accno,$pdns_contact, $pdns_dom, $pdns_aip
# Post: 0 failmsg, 1 okmsg
sub get_dns_resolver {
       
        my ($uid, $accno, $dns_contact, $dns_dom, $dns_ip, $ip_version,$service_id) = @_;
      
        my ($ref, @contactvals, $email);
$logger->debug("in sub : sub out------->>>>>>>>>>>:$uid, $accno, $dns_contact, $dns_dom, $dns_ip, $ip_version");
      
        #my $p = "p class=header";
        #my $pt = "p class=text";

    

		$email = $dns_contact;
        my @vals = ();
		
        my $res = '';
		my $output;

        $res = CMS_OLSS::get_dns_resolver_entry($uid, $accno, $dns_contact, $dns_dom, $dns_ip,$service_id,$ip_version);

         $logger->debug("in dns_trace : out------->>>>>>>>>>>:$res");

        if($res == 1) {
              		
		my $parent_site_id = CMS_OLSS::get_parent_site_id($service_id,$accno);
		$logger->debug("in pridns add parentsiteID  ----->>>$parent_site_id");
		
		my $dns_name;
		if($parent_site_id == 44892){
			$dns_name = "Primary DNS server: dns01.reach.net.id(202.47.192.70) and Secondary DNS server: dns02.reach.net.id(202.47.192.41)";
		}elsif($parent_site_id == 44889){
			$dns_name = "Primary DNS server: BOMRED08(210.57.201.46) and Secondary DNS server: MASRED02(202.41.237.6)";
		}else{
			$dns_name = "Dns servers:ns2.telstraglobal.net(134.159.4.144),ns3.telstraglobal.net(134.159.2.144)";
		};
         if($ip_version=~'4'){
		 $dns_ip=uc $dns_ip;
		push(@vals, {success_message => "Requested DNS Resolver service is successful."." ACL file is written."." The domain record $dns_dom  has been verified for $dns_ip."." The DNS server reloads once every 2 hours."." The following information is required for you to set up your server. "."$dns_name"});
		return encode_json({result => \@vals});	
         }else{
		 my @z = split(/\//,$dns_ip);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $dns_ip ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc $ipblock6;
		 push(@vals, {success_message => "Requested DNS Resolver service is successful."." ACL file is written."." The domain record $dns_dom  has been verified for $ipblock6."." The DNS server reloads once every 2 hours."." The following information is required for you to set up your server. "."$dns_name"});
		return encode_json({result => \@vals});	
		 }		
        }
		else 
		{
          
			push(@vals, {Error =>"$res".". Requested DNS Resolver service was not successful."});
			return encode_json({result => \@vals});
	   }

        return @vals;
		

}

# CANCEL DNS RESOLUTION
# Pre: $accno,$pdns_contact, $pdns_dom, $pdns_aip
# Post: 0 failmsg, 1 okmsg
sub cancel_dns_resolver {
        my ($uid, $accno, $dns_contact, $dns_dom, $dns_ip) = @_;

        my ($ref, @contactvals, $email);

        my $p = "p class=header";
        my $pt = "p class=text";



        $email = $dns_contact;
        my @vals = ();
		

        my $res = '';

        $res = CMS_OLSS::cancel_dns_resolver($uid, $accno, $dns_contact, $dns_dom, $dns_ip,$ip_version);
	$logger->debug("cancel_dns_resolver------->>>>>>>>>>>:$res");



        if($res >= 1) {

              	if($ip_version=~'4'){
            $dns_ip=uc $dns_ip;				
				push(@vals, {success_message =>"Cancelled DNS Resolver service"." ACL file is updated."." The domain record $dns_dom  has been verified for $dns_ip."." The DNS server reloads once every 2 hours."});
				return encode_json({result => \@vals});
				}else{
				 my @z = split(/\//,$dns_ip);
				 my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $dns_ip ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc $ipblock6;
						push(@vals, {success_message =>"Cancelled DNS Resolver service"." ACL file is updated."." The domain record $dns_dom  has been verified for $ipblock6."." The DNS server reloads once every 2 hours."});
				       return encode_json({result => \@vals});
				
				}

        } else {

				push(@vals, {Error =>"Error Encountered:"." Requested DNS Resolver service was not successful."});
				return encode_json({result => \@vals});
        }

        return @vals;		
}


# View DNS RESOLVER
# Pre: $accno, $mrwaccno
# Post: @vals
sub view_dns_resolv {
        my ($uid, $mrwaccno, $view_type) = @_;
        my($accno, $staffid) = &ex_staffid($uid);

        my $result;
                eval {
                my $a = ($mrwaccno) ? $mrwaccno : $accno;
                $result = CMS_OLSS::view_dns_resolver($uid);
                };
		$logger->debug("incgi view sub : out------->>>>>>>>>>>:$#{$result}");		

        my @retvals=();
                #push @retvals, "<b class=header>List of Allowed IPs for DNS Resolver Service. </b>";               
				#my $header='List of Allowed IPs for DNS Resolver Service.';
                    
        #if($#{$result} <= 0) {
if($result->[0]->[0] ne $accno){
				push(@retvals, {Error => "No record found on the DNS Resolver Service."});
				return encode_json({result => \@retvals});
        } else {
				my $view_dns_res;
				my $count=1;
                for my $row (@$result) {
								
                        next if($$row[3] =~ /^(3|4)$/);

                        my $accno     = $$row[0];
			   my $contact   = $$row[1];
                        my $host      = $$row[2];
                        my $parm      = $$row[3];
       
						push(@retvals, {Account => $accno, Contact => $contact,Hostname => $host, IPBlock => $parm,header => "List of Allowed IPs for DNS Resolver Service."});		
                }
				$view_dns_res = encode_json({result => \@retvals});
				return $view_dns_res;     
        }
	
        	
        return @retvals;
}

# REQUEST For Reverse Delegation
# Pre: $accno,$rdns_contact, $rdns_ip, $rdns_ptr, $rdns_host, $rdns_dom
# Post: 0 failmsg, 1 okmsg
sub get_dns_reverse {
open (DEBUG, ">/tmp/rev_mng_map");
        #my ($uid, $accno,$rdns_contact, $rdns_ip, $rdns_dom,$service_id) = @_;
        my ($customerId,$uid,$rdns_contact, $rev_type, $rip, $rdom, $r_ip_blk, $rns, $btn_chk3,$ip_version,$service_id) = @_; 
       $logger->debug("in get_dns_reverse uid=$uid, custId=$customerId,contact=$rdns_contact,revtype= $rev_type, rip=$rip,rdom= $rdom,rip_blk= $r_ip_blk,rns= $rns,btncheck3= $btn_chk3,ipversion=$ip_version,serviceid=$service_id ");
	
	$uid = $customerId;	
	my $div_val = '';
	print DEBUG "Entered\n";
	print DEBUG "uid:$uid\taccno:$accno\t,rdns_contact:$rdns_contact\tbtn_chk3: $btn_chk3\n 
	\trip:$rip\trdom:$rdom\tr_ip_blk:$r_ip_blk\trns:$rns\n";
        my ($ref, @contactvals, $email);
		my $rev_type = 0;
       
        $email = $rdns_contact;


        my @val = ();

       $logger->debug("before check call  ");
	if($btn_chk3 == 10) {
	  $rev_type = 10;	 
	} else {
       	$rev_type = 11;
	}
	my $rdns_ip = $rip;
	my $rdns_dom = $rdom;
	my $rdns_ip_blk = $r_ip_blk;
	my $rdns_ns = $rns;
	#if ($selected_type =~ /^Reverse Mapping$/) {
	if ($rev_type == 10) {
                my $ok = 0;
	print DEBUG "inside reverse mapping loop\n";

	#added by Hari
     # IP Address to be removed 
     $logger->debug("before DB call to  ");
           

        
        my $res = '';
       print DEBUG "before calling DB function\n"; 
	


	$logger->debug("before DB call to CMS_REVERSEDEL_DB uid:  $uid, CUSTID: $customerId, CONTACT: $rdns_contact, IP: $rdns_ip, IPV: $ip_version,DOM: $rdns_dom");
    	$res = CMS_REVERSEDEL_DB::req_reverse_mapping($uid, $customerId,$rdns_contact, $rdns_ip,$ip_version,$rdns_dom,$service_id);

	$logger->debug("after DB call to CMS_REVERSEDEL_DB : $res");



	if($res == 1) {
	   if($ip_version=~'4'){
	   $rip=uc $rip;
		push(@val, {success_message => "Requested Reverse Mapping is successful."." The IP address block for $rip has been verified for the domain $rdom"." The DNS server reloads once every 2 hours. "});
		return encode_json({result => \@val});	
       }else{
	     my @z = split(/\//,$rip);
                         my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $rip ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
						$ipv6=uc $ipv6;
	                    #my $ipblock6 = join "/",$ipv6,$prefix;
	   push(@val, {success_message => "Requested Reverse Mapping is successful."." The IP address block for $ipv6 has been verified for the domain $rdom"." The DNS server reloads once every 2 hours. "});
		return encode_json({result => \@val});	
	   }		
    } else {

        push(@val, {Error =>"$res".". Requested Reverse Mapping was not successful."});
		return encode_json({result => \@val}); 
        }

    return @val;
	} #end of reverse mapping



	#elsif ($selected_type =~ /^Reverse Delegation$/){
	#start of reverse mapping

elsif ($rev_type == 11){

$logger->debug("inside reverse delgation loop. rdns_ip_blk = $rdns_ip_blk  ");
print DEBUG "inside reverse delgation loop\n";


     
# IP BLOCK  Octet Validation
        $rdns_ip_blk =~ s/'//;
        my @y1 = split(/\//,$rdns_ip_blk);
        my $z5= shift(@y1);#z5 has ip
        my $z6= shift(@y1);#z6 has prefix
        $z6 =~ s/'//;
         $logger->debug(" ZZZ666 $z6");
	#added by hari
        if ($ip_version eq "4")  {
	 $logger->debug("in ip version $ip_version and ip is $z5");						
        my @z5_octets = split(/\./,$z5);
        my $z5_octet1 = shift(@z5_octets);
        my $z5_octet2 = shift(@z5_octets);
        my $z5_octet3 = shift(@z5_octets);
        my $z5_octet4 = shift(@z5_octets);
        my $diff_prefix = $z6-24;               
        my $h = 2**($diff_prefix);
        #calculate number of intervals within subrange.
        $div_val = 256/$h;
	
                  
        }
 
        #ipv6 changes added by hari 
        elsif($ip_version eq "6"){ 
        $logger->debug("ipversion is six");
        my $ipv6 = Net::IP::ip_expand_address($z5, 6);
        $logger->debug("IPV6 : $ipv6  ");		
       	my @z5_octets = split(/\:/,$ipv6);
        my $z5_octet1 = shift(@z5_octets);
        my $z5_octet2 = shift(@z5_octets);
        my $z5_octet3 = shift(@z5_octets);
        my $z5_octet4 = shift(@z5_octets);
        my $z5_octet5 = shift(@z5_octets);
        my $z5_octet6 = shift(@z5_octets);
        my $z5_octet7 = shift(@z5_octets);
        my $z5_octet8 = shift(@z5_octets);
        my $z5_fourthoct =hex($z5_octet4);   # fourth octet to be used 
        $logger->debug("I@@@@@@@@@ $z5_octet4    Z555 $z5_fourthoct");		
        my $diff_prefix = $z6-48;               
        my $h = 2**($diff_prefix);
        #calculate number of intervals within subrange.
        $div_val = 65536/$h;
         $logger->debug("I!!!!!!!!!!!!!!!!!!!!  $h");			
        	#if($z6 > 48 || $z6 < 64) {
		#		push(@val, {Error =>"Invalid IP Format. Prefix length must be between 48 and 64."});
		#		return encode_json({result => \@vals});	   }
       
		##$z5_fourthoct;
		$logger->debug(" out side IPV6invalid $z5_fourthoct%$div_val");	
       	 if(($z5_fourthoct%$div_val) != 0) {
		     $logger->debug("IPV6invalid $z5_fourthoct%$div_val");		
			push(@val, {Error =>"Invalid IP Block Entered."});
			return encode_json({result => \@val});
        	}
       	 
        }
        
	
	
	
	$logger->debug("trying to write success message ");
	#add logic to display new success msg
	if ($ip_version eq "4")  {                                                   #added by hari
		my $rdns_blk = $rdns_ip_blk;
		my @cnm_pre = split (/\//,$rdns_blk);
		my $cnm_ip_pre = $cnm_pre[1];
		my $all_oct = $cnm_pre[0];
		my @ip_oct = split (/\./,$all_oct);
		my $ip_oct1 = shift(@ip_oct);
		my $ip_oct2 = shift(@ip_oct);
		my $ip_oct3 = shift(@ip_oct);
		my $ip_oct4 = shift(@ip_oct);
	print DEBUG "ip_oct1:$ip_oct1\tip_oct2:$ip_oct2\tip_oct3:$ip_oct3\tip_oct4:$ip_oct4\n";
		my $arpa = "in-addr.arpa";
		$ptrval = join ".",$ip_oct3,$ip_oct2,$ip_oct1,$arpa;
		my $cname_var = &rev_range($cnm_ip_pre);
		my $ip_range = ($cname_var+ $ip_oct4);
		$temp_msg = "$ip_oct4 IN CNAME $ip_oct4.$ip_oct4-$ip_range.$ptrval.";
		my $prev_ip_range = ($ip_range - 1);
		$temp_msg2 = "$ip_range IN CNAME $ip_range.$ip_oct4-$ip_range.$ptrval."; 
	print DEBUG "temp_msg:$temp_msg\ntemp_msg2:$temp_msg2\n";
		$temp_msg3 = "$ip_oct4-$ip_range.$ptrval. IN NS $rdns_ns.";
	}	
	#ipv6 changes added by hari for NS records
	elsif($ip_version eq "6"){                                        
	
		my $str = '';
		my @nib =();
		my $rdns_blk = $rdns_ip_blk;
		my $exp_ip = Net::IP::ip_expand_address($rdns_blk, 6);
		my @cnm_pre = split (/\//,$exp_ip);
		my $ip_prefix = $cnm_pre[1];
		my $all_oct = $cnm_pre[0];
		$logger->debug("IPV6 address $all_oct & prefix $ip_prefix");

		my $rev6=new Net::IP($all_oct); 				# Net:: ip function for ipv6.arpa 
		my $ptrval=$rev6->reverse_ip();
		$logger->debug(" reverse IPV6 address $ptrval ");
			
			#for substring nibble boundary
			my $ns_str_full = '';
			if ($ip_prefix >= 49 && $ip_prefix <= 52){
                                $str = substr($ptrval,40,-30);
				$ns_str_full = substr($ptrval,38,-30);
                        }elsif ($ip_prefix >= 53 && $ip_prefix <= 56){
                                $str = substr($ptrval,38,-30);
				$ns_str_full = substr($ptrval,36,-30);
                        }elsif ($ip_prefix >= 57 && $ip_prefix <= 60){
                                $str = substr($ptrval,36,-30);
				$ns_str_full = substr($ptrval,34,-30);
                        }elsif ($ip_prefix >= 61 && $ip_prefix <= 64){
                                $str = substr($ptrval,34,-30);
				$ns_str_full = substr($ptrval,32,-30);
                        }elsif ($ip_prefix == 48){
                                $str = substr($ptrval,42,-30);
				$ns_str_full = substr($ptrval,40,-30);
                        }

			#determining the last nibble for the prefix 
			my $ns_str = $str;
			$str = substr($ns_str_full,0,1);
                        $str = lc $str;

                        if ($ip_prefix%4 == 1){
                                if ($str >= 0 && $str <= 7){
                                        @nib=(0,1,2,3,4,5,6,7);
                                }else { @nib=(8,9,'a','b','c','d','e','f');
                                }
                        }elsif ($ip_prefix%4 == 2){
                                if ($str >= 0 && $str <= 3){
                                        @nib = (0,1,2,3);
                                }elsif ($str >= 4 && $str <= 7){
                                        @nib = (4,5,6,7);
                                }elsif ($str == 8 || $str == 9 || $str eq 'a' || $str eq 'b'){
                                        @nib = (8,9,'a','b');
                                }else { @nib = ('c','d','e','f');
                                }
                        }elsif($ip_prefix%4 == 3){
                                if ($str == 0 || $str == 1){
                                        @nib = (0,1);
                                }elsif($str == 2 || $str == 3){
                                        @nib = (2,3);
                                }elsif($str == 4 || $str == 5){
                                        @nib = (4,5);
                                }elsif($str == 6 || $str == 7){
                                        @nib = (6,7);
                                }elsif($str == 8 || $str == 9){
                                        @nib = (8,9);
                                }elsif($str eq 'a' || $str eq 'b'){
                                        @nib = ('a','b');
                                }elsif($str eq 'c' || $str eq 'd'){
                                        @nib = ('c','d');
                                }elsif($str eq 'e' || $str eq 'f'){
                                        @nib = ('e','f');
                                }
                        }elsif($ip_prefix%4 == 0){
                                @nib=($str);
                        }

			#appending to the substring
			my @msg = ();
			
			foreach my $pre(@nib){
			@cnm_pre=join("\.",$pre,$ns_str); 
   			
			#creating nibble record for NS	
			foreach my $oct(@cnm_pre){
							$temp_name = "$oct IN NS $rdns_ns.";
							$logger->debug(" the entries r $temp_name  ");
							
						   }
						   push (@msg,$temp_name);
							$message=join(" ", @msg);
							$logger->debug("msg succes message : $message  ");
							
			}							
	$logger->debug("reverse succes message : $message  ");	
	}
		
        my $res = '';
        
	$logger->debug("going to reverse del db ");
	$logger->debug("uid =$uid, custId =$customerId,rdns_contact =$rdns_contact,rdns = $rdns_ip_blk,ipversion =$ip_version,rdns_ns =$rdns_ns ");	
	$res = CMS_REVERSEDEL_DB::rev_delegation($uid, $customerId,$rdns_contact, $rdns_ip_blk,$ip_version,$rdns_ns,$service_id);
	$logger->debug("result res >>>>>>>>>>> $res ");

	if($res == 1) {
	if($ip_version=~'4'){
	    $rdns_ip_blk=uc$rdns_ip_blk;
		push(@val, {success_message => "Requested Reverse Delegation is successful : The IP address block for $rdns_ip_blk has been verified for the domain $rdns_ns. The following records will be created in our DNS Server: $temp_msg  $temp_msg2 $temp_msg3 The DNS server reloads once every 2 hours."});
		return encode_json({result => \@val});	
}		else{
 my @z = split(/\//,$rdns_ip_blk);
                         my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $rdns_ip_blk ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc $ipblock6;
						push(@val, {success_message => "Requested Reverse Delegation is successful : The IP address block for $ipblock6 has been verified for the domain $rdns_ns. The following records will be created in our DNS Server: $temp_msg  $temp_msg2 $temp_msg3 The DNS server reloads once every 2 hours."});
		return encode_json({result => \@val});	

}
        } else { 
		push(@val, {Error =>" $res".". Requested Reverse Delegation was not successful."});
		return encode_json({result => \@val});
        }

        return @val;
	}#end of reverse Delegation

}
       
# CANCEL DNS REVERSE DELEGATION
# Pre: $accno,$rdns_contact, $rdns_ip, $rdns_dom
# Post: 0 failmsg, 1 okmsg
sub reverse_cancel {
open (DEBUG,">/tmp/rev_cancel");

        #my ($uid, $accno,$rdns_contact, $rdns_ip,$rdns_dom) = @_;
	my ($uid, $customerId,$rdns_contact, $rev_type, $rip, $rdom, $r_ip_blk, $rns, $btn_chk3,$ip_version) = @_;
	print DEBUG "Entered\n";
        print DEBUG "uid:$uid\taccno:$accno\t,rdns_contact:$rdns_contact\tbtn_chk3: btn_chk3\n 
\trip:$rip\trdom:$rdom\tr_ip_blk:$r_ip_blk\trns:$rns\n";
		
		

        my ($ref, @contactvals, $email);

       

        $email = $rdns_contact;
		
		my @vals;
		my %cancel_hash;

        my $rdns_ip = $rip;
        my $rdns_dom = $rdom;
        my $rdns_ip_blk = $r_ip_blk;
        my $rdns_ns = $rns;

	if($btn_chk3 == 10) {
	#print DEBUG "btn_chk3: $btn_chk3 INSIDE REV_MAP\n\n";
        $rev_type = 10;
	}elsif ($btn_chk3 == 11) {
	#print DEBUG "btn_chk3: $btn_chk3 INSIDE REV_DEL\n";
	$rev_type = 11;
	}

	if ($rev_type == 10) {
        my $ok = 0;
        print DEBUG "inside reverse mapping loop\n";


       

        my $res = '';
        $res = CMS_REVERSEDEL_DB::reverse_cancel($uid, $customerId,$rdns_contact, $rdns_ip,$ip_version,$rdns_dom);

							
        if($res == 1) {
				if($ip_version=~'4'){
				$rdns_ip=uc $rdns_ip;
				push(@vals, {success_message => "Cancelled Reverse Mapping service."." The domain record of $rdns_dom  has been verified for $rdns_ip."." The DNS server reloads once every 2 hours."});
				return encode_json({result => \@vals});
	}else{
	my @z = split(/\//,$rdns_ip);
                         my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $rdns_ip ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
						$ipv6=uc $ipv6;
	                   # my $ipblock6 = join "/",$ipv6,$prefix;
						push(@vals, {success_message => "Cancelled Reverse Mapping service."." The domain record of $rdns_dom  has been verified for $ipv6."." The DNS server reloads once every 2 hours."});
				return encode_json({result => \@vals});
	}
        } else {

                push(@vals, {Error =>"Error Encountered:"." Requested Reverse Mapping Service was not successful."});
				return encode_json({result => \@vals});

        }

        return @vals;
}#end of reverse mapping

	elsif ($rev_type == 11){
print DEBUG "inside reverse delgation loop\n";

        

# IP BLOCK  Octet Validation
        $rdns_ip_blk =~ s/'//;
        my @y1 = split(/\//,$rdns_ip_blk);
        my $z5= shift(@y1);#z5 has ip
        my $z6= shift(@y1);#z6 has prefix
        $z6 =~ s/'//;
        
         if ($ip_version eq "4")  {  						#added by hari
        my @z5_octets = split(/\./,$z5);
        my $z5_octet1 = shift(@z5_octets);
        my $z5_octet2 = shift(@z5_octets);
        my $z5_octet3 = shift(@z5_octets);
        my $z5_octet4 = shift(@z5_octets);
        my $diff_prefix = $z6-24;
        my $h = 2**($diff_prefix);
        #calculate number of intervals within subrange.
        $div_val = 256/$h;

         #if($z6 < 24 || $z6 > 31) {
	#		push(@vals, {Error =>"Invalid IP Format. Prefix length must be between 24 and 31."});
	#		return encode_json({result => \@vals});}

         if(($z5_octet4%$div_val) != 0) {
			push(@vals, {Error =>"Invalid IP Block Entered."});
			return encode_json({result => \@vals});
        }
      }
	
	#ipv6 changes added by hari 
        elsif($ip_version eq "6"){ 
        
        my $ipv6 = Net::IP::ip_expand_address($z5, 6);		
       	my @z5_octets = split(/\:/,$ipv6);
        my $z5_octet1 = shift(@z5_octets);
        my $z5_octet2 = shift(@z5_octets);
        my $z5_octet3 = shift(@z5_octets);
        my $z5_octet4 = shift(@z5_octets);
        my $z5_octet5 = shift(@z5_octets);
        my $z5_octet6 = shift(@z5_octets);
        my $z5_octet7 = shift(@z5_octets);
        my $z5_octet8 = shift(@z5_octets);
        my $z5_lastoct =hex($z5_octet4);   # fourth octet to be used 
        
        my $diff_prefix = $z6-48;               
        my $h = 2**($diff_prefix);
        #calculate number of intervals within subrange.
        $div_val = 65536/$h;
        	
        	#if($z6 > 48 || $z6 < 64){
		#	push(@vals, {Error =>"Invalid IP Format. Prefix length must be between 48 and 64."});
		#	return encode_json({result => \@vals});}
       	 
       	 if(($z5_lastoct%$div_val) != 0) {
			push(@vals, {Error =>"Invalid IP Block Entered."});
			return encode_json({result => \@vals});
        	}
       	 
        }


        my $res = '';
	print DEBUG "before callin DB cancel\n";
        $res = CMS_REVERSEDEL_DB::reverse_cancel($uid, $customerId,$rdns_contact, $rdns_ip_blk,$ip_version,$rdns_ns);
       

	   if($res == 1) {
	   if($ip_version=~'4'){
	   $rdns_ip_blk=uc $rdns_ip_blk;
			push(@vals, {success_message => "Cancelled Reverse Delegation service."." The domain record of $rdns_ns  has been verified for $rdns_ip_blk."." The DNS server reloads once every 2 hours."});
			return encode_json({result => \@vals});		
}else{
my @z = split(/\//,$rdns_ip_blk);
  my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $rdns_ip_blk ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6= uc $ipblock6;
						push(@vals, {success_message => "Cancelled Reverse Delegation service."." The domain record of $rdns_ns  has been verified for $ipblock6."." The DNS server reloads once every 2 hours."});
			return encode_json({result => \@vals});	
}			

        } else {
			push(@vals, {Error =>"Error Encountered:"."Requested Reverse Delegation service was not successful."});
			return encode_json({result => \@vals});

        }

        return @vals;
}#end of reverse Delegation

close (DEBUG);
}
       
       
# View DNS REVERSE
# Pre: $accno, $mrwaccno
# Post: @vals
sub view_dns_reverse {
        my ($uid, $mrwaccno, $view_type, $rdns_ipadd, $rdns_ip_blk, $btn_chk3) = @_;
	$logger->debug("view_dns_reverse >>>>>>>>>>> uid=$uid, mr_accno=$mrwaccno, view_type=$view_type,rdns_ip= $rdns_ipadd, rdns_ipblk=$rdns_ip_blk, btcheck3=$btn_chk3 ");

        my($accno, $staffid) = &ex_staffid($uid);

        my $result = 0;
                eval {
                #my $a = ($mrwaccno) ? $mrwaccno : $accno;
			my $a	 = $uid;
		$logger->debug("view_dns_reverse account >>>>>>>>>>> $a");

                $result = CMS_OLSS::view_dns_reverse($a);
                };
	$logger->debug("view_dns_reverse result >>>>>>>>>>> $#{$result}");

   	my @retvals;

               

        if($#{$result} < 0) {
               
		  push(@retvals, {Error =>"No record found on the Reverse Delegation Records."});
		  return encode_json({result => \@retvals});
        } else {
               
                for my $row (@$result) {
                        #next if($$row[3] =~ /^(3|4)$/);

                     my $accno     = $$row[0];
                     my $contact   = $$row[1];
                     my $ptrrec      = $$row[2];
			my $rec_type     = $$row[3];
                     my $fqdn      = $$row[4];
			my $last_oct  = $$row[5];
				
	$logger->debug("ptrec >>>>>>>>>>> $ptrrec");
		 if($ptrrec ne ""){	
		 		if ($ip_version == 4){	
			my @ip_rev = split(/\./,$ptrrec);
			my $ip1_reverse = shift(@ip_rev);
			my $ip2_reverse = shift(@ip_rev);
			my $ip3_reverse = shift(@ip_rev);
			my @comb_ip = ("$ip3_reverse.","$ip2_reverse.","$ip1_reverse.","$last_oct");
			my $orig_ip = join(",", @comb_ip);
			$orig_ip =~ s/,//g;
			$ptrrec = $orig_ip;
		 							}
		 		elsif ($ip_version == 6){
		 								#string manipulation to convert ipv6 arpa record to ipv6 address
		 								$ipv6=$last_oct.".".$ptrrec;
		 								my $ip_rev=scalar reverse("$ipv6");
 										$ip_rev =~ s/\.//g;
 										#print "Reversed Value is $ip_rev \n";
 										my $ip=substr $ip_rev,7;			#remove "ip6.arpa" 
										#print "reverse ipv6 is $ip\n";
										my @comb_ip = $ip =~ /(.{1,4})/g;
										$orig_ip=join(":", @comb_ip);
										$ptrrec = $orig_ip;
		 								}
			}

			if ($rec_type eq "NS") {
			$rec_type = "Reverse Delegation";
			} elsif ($rec_type eq "PTR") {
			$rec_type = "Reverse Mapping";
			}
			push(@retvals, {Account => $accno, Contact => $contact,IPaddress => $ptrrec, Hostname => $fqdn,RecordType => $rec_type});
			

                }
			return encode_json({result => \@retvals});

        }

        return @retvals;

}

#replaced Old_rev_trace with rev_trace as part of olss replacement
sub rev_trace {

$logger->debug("in Rev_Trace:>>>>>>>>>>>>>>>>>>");

		my ($uid, $accno, $rdns_contact, $rev_type, $rdns_ipadd, $rdns_dom, $rdns_ip_blk, $rdns_ns, $mrwaccno, $oenc, $btn_chk1, $btn_chk3, 
$btn_chk9,$ip_version,$service_id) = @_;

$logger->debug("in Rev_Trace:>>>>>>>>>>>>>>>>>>$uid, $accno, $rdns_contact, $rev_type, $rdns_ipadd, $rdns_dom, $rdns_ip_blk, $rdns_ns, $mrwaccno, $oenc, $btn_chk1, $btn_chk3, 
$btn_chk9,$ip_version,$service_id");

if ($formels{'REV_VIEW'}) 
						{
						$logger->debug("in Rev_Trace: in view>>>>>>>>>>>>>>>>>>");
							
						#below code added by shruti 
						my $view_reverse_out = &view_dns_reverse($uid, $accno, $mrwaccno, $rdns_ipadd, $rdns_ip_blk, $btn_chk3);
						$logger->debug("in Rev_Trace: output>>>>>>>>>>>>>>>>>>$view_reverse_out");				
						return $view_reverse_out;
						} 
						elsif ($btn_chk9 == 1) 
						{															
						print DEBUG "btn_chk3: $btn_chk3 - INSIDE fn rev_trace\n";
						$logger->debug("in Rev_Trace: submit buttoin check 1----->>>>>> $service_id");
						my $submit_reverse_out = &get_dns_reverse($uid, $accno, $rdns_contact, $rev_type, $rdns_ipadd,$rdns_dom,$rdns_ip_blk,$rdns_ns,$btn_chk3,$ip_version,$service_id);
						$logger->debug("in Rev_Trace submit: output>>>>>>>>>>>>>>>>>>$submit_reverse_out");	
 						return $submit_reverse_out;
                        } 
						elsif ($btn_chk1 == 1) 														
						{
                        ##this is for cancelling request
						$logger->debug("in Rev_Trace cancel: CANcel    uid: $uid,accno: $accno,contact: $rdns_contact,type: $rev_type, ipadd: $rdns_ipadd, dom: $rdns_dom , ip: $rdns_ip_blk, ns: $rdns_ns, btn3: $btn_chk3,version: $ip_version");			
						my $cancel_reverse_out = &reverse_cancel($uid, $accno, $rdns_contact, $rev_type, $rdns_ipadd,$rdns_dom ,$rdns_ip_blk,$rdns_ns, $btn_chk3,$ip_version);
						$logger->debug("in Rev_Trace cancel: output>>>>>>>>>>>>>>>>>>$cancel_reverse_out");
						return $cancel_reverse_out ;	
                        }
}


#replaced below Old_dns_trace with above dns_trace code as part of OLSS-Replacement

sub dns_trace {
	open (DEBUG,">/tmp/dns_trace");
	my ($uid, $accno, $dns_contact, $dns_dom, $dns_ip, $mrwaccno, $code, $btn_chk,$ip_version,$service_id) = @_;
	print DEBUG "uid:$uid\taccno:$accno\n";
	$logger->debug("in dns_trace------->>>>>>>>>>>: $uid, $accno, $dns_contact, $dns_dom, $dns_ip, $mrwaccno, $code, $btn_chk,$ip_version ,service id - $service_id");
	$logger->debug("in dns_trace : view out------->>>>>>>>>>>:$formels{'DNS_SUBMIT'}");
if ($formels{'VIEWDNS_SUBMIT'}) {
					             #edited line no.2067,2068,2069 to store and print,return encode to json
                                 my $view_output =  &view_dns_resolv($uid, $accno, $mrwaccno);
				     $logger->debug("in dns_trace : view out------->>>>>>>>>>>:$view_output");
								 return $view_output;
                                } 		
								elsif ($formels{'DNS_SUBMIT'}){
								
								my $submit_output = &get_dns_resolver($uid, $accno, $dns_contact, $dns_dom, $dns_ip,$ip_version,$service_id);
								$logger->debug("in dns_trace : submit out------->>>>>>>>>>>:$submit_output");
								return $submit_output;
										
                                } elsif ($btn_chk == 1) {
										
                                my $cancel_output =  &cancel_dns_resolver($uid, $accno, $dns_contact, $dns_dom, $dns_ip,$ip_version);
				    $logger->debug("in dns_trace : cancel out------->>>>>>>>>>>:$cancel_output");
								return $cancel_output;
                                }
}


#replaced Old_primary_trace with primary_trace as part of OLSS-Replacement

sub primary_trace {
my($uid,$accno, $level, $pdns_contact, $pdns_dom, $pdns_type_id, $pdns_host, $pdns_aip4,$pdns_aip6, $pdns_cnhost, $pdns_mxpv, $pdns_mxex, $pdns_ns, $pdns_tttext, $pdns_dbrowid, $service_id) = @_;
$logger->debug("in primary trace before query test------->>>>>>>>>>>: uid===$uid,account==$accno,level== $level, pdnscontafct == $pdns_contact,pdns dome = $pdns_dom,pdns ype = $pdns_type_id, pdns host == $pdns_host,pdns_ipv4 = $pdns_aip4, ipv6 =$pdns_aip6, cnhost =$pdns_cnhost, mxpv = $pdns_mxpv, mxex = $pdns_mxex,ns = $pdns_ns,tttext=  $pdns_tttext, dbrowi = $pdns_dbrowid, serviceid= $service_id");


if ($formels{'PRIMARY_ADD_SUBMIT'}) {
									my $add_pridns_out = &add_pridns($uid,$accno, $pdns_contact, $pdns_dom, $pdns_type_id, $pdns_host, $pdns_aip4,$pdns_aip6, $pdns_cnhost, $pdns_mxpv, $pdns_mxex, $pdns_ns, $pdns_tttext, $service_id);
									$logger->debug("in primary trace submit-------->>>>>>>>>>>:$add_pridns_out");
									return $add_pridns_out;
                                    } elsif ($formels{'PRIMARY_VIEW_SUBMIT'}) {
									my $view_pridns_out = &view_pridns($uid, $accno);
									$logger->debug("in primary trace view-------->>>>>>>>>>>:$view_pridns_out");
									return $view_pridns_out;
                                    } elsif ($formels{'PRIMARY_REM_SUBMIT'}) {
									my $del_pridns_out = &del_pridns($uid,$accno, $pdns_dbrowid);
									$logger->debug("in primary trace remove-------->>>>>>>>>>>:$del_pridns_out");
									return $del_pridns_out;
                                    }elsif ($formels{'PRIMARY_RELOAD_SUBMIT'}) {
									my $reload_pridns_out = &reload_pridns($uid, $dom);
									return $reload_pridns_out;
                                    }
}


#replaced Old_sec_trace with sec_trace as part of OLSS-Replacement.


sub sec_trace {

 my($uid, $code, $accno, $level,$sdns_contact, $sdns_dom, $sdns_ip, $sdns_dbrowid, $mrwaccno, $btn_chk,$service_id ) = @_;
 	
$logger->debug("Secondary DNS trace ------->>>>>>>>>>>: uid = $uid,code = $code,acno = $accno,level = $level, dns contact= $sdns_contact, dns dom = $sdns_dom,dns ip = $sdns_ip,dns rowid= $sdns_dbrowid, mrcacc=  $mrwaccno, button check= $btn_chk,serviceid=$service_id");
	if ($formels{'btnChecksec'} == 1) {
                                    my $add_secdns_out = &add_secdns($uid, $code, $accno, $sdns_contact, $sdns_dom, $sdns_ip, $mrwaccno,$service_id);
						return $add_secdns_out;
                                    } elsif ($formels{'btnChecksec'} == 2) {
					$logger->debug("buttonCheck2 ------->>>>>>>>>>>:$accno, $mrwaccno ");
                                    my $view_secdns_out = &view_secdns($accno, $mrwaccno);
                                    return $view_secdns_out;
                                    } elsif  ($formels{'btnChecksec'} == 3) {
                                    my $del_secdns_out = &del_secdns($uid, $code, $accno, $sdns_dom, $mrwaccno);
                                    return $del_secdns_out;
                                    }

}

sub rev_range {
my ($rev_val) = @_;
my $range = 0;
if ($rev_val ==24) {
                $range = 255;
        }elsif ($rev_val == 25) {
                 $range = 127;
        }elsif ($rev_val == 26) {
                 $range = 63;
        }elsif ($rev_val == 27 ){
                 $range = 31;
        }elsif ($rev_val == 28 ){
                 $range = 15;
        }elsif ($rev_val == 29 ){
                 $range = 7;
        }elsif ($rev_val == 30 ){
                 $range = 3;
        }elsif ($rev_val == 31 ){
                 $range = 1;
        }elsif ($rev_val == 32 ){
                 $range = 0;
        }
return $range;
}


##### IPv6 reverse by Hari 
sub rev_range6{
	my ($rev_val) = @_;
my $range = 0;
if ($rev_val ==48) {
                $range = 1208925819614629174706176;
        }elsif ($rev_val == 49) {
                 $range = 604462909807314587353088;
        }elsif ($rev_val == 50) {
                 $range = 302231454903657293676544;
        }elsif ($rev_val == 51 ){
                 $range = 151115727451828646838272;
        }elsif ($rev_val == 52 ){
                 $range = 75557863725914323419136;
        }elsif ($rev_val == 53 ){
                 $range = 37778931862957161709568;
        }elsif ($rev_val == 54 ){
                 $range = 18889465931478580854784;
        }elsif ($rev_val == 55 ){
                 $range = 9444732965739290427392;
        }elsif ($rev_val == 56) {
                $range = 4722366482869645213696;
        }elsif ($rev_val == 57) {
                 $range = 2361183241434822606848;
        }elsif ($rev_val == 58) {
                 $range = 1180591620717411303424;
        }elsif ($rev_val == 59 ){
                 $range = 590295810358705651712;
        }elsif ($rev_val == 60 ){
                 $range = 295147905179352825856;
        }elsif ($rev_val == 61 ){
                 $range = 147573952589676412928;
        }elsif ($rev_val == 62 ){
                 $range = 73786976294838206464;
        }elsif ($rev_val == 63 ){
                 $range = 36893488147419103232;
        }elsif ($rev_val == 64 ){
                 $range = 18446744073709551616;
        }
}
