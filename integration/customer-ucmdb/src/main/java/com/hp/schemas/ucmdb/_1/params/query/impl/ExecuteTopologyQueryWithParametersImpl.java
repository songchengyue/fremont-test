/*
 * XML Type:  executeTopologyQueryWithParameters
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML executeTopologyQueryWithParameters(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class ExecuteTopologyQueryWithParametersImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParameters
{
    
    public ExecuteTopologyQueryWithParametersImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "cmdbContext");
    private static final javax.xml.namespace.QName QUERYXML$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "queryXml");
    private static final javax.xml.namespace.QName PARAMETERIZEDNODES$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "parameterizedNodes");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets the "queryXml" element
     */
    public java.lang.String getQueryXml()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUERYXML$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "queryXml" element
     */
    public org.apache.xmlbeans.XmlString xgetQueryXml()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUERYXML$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "queryXml" element
     */
    public void setQueryXml(java.lang.String queryXml)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUERYXML$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUERYXML$2);
            }
            target.setStringValue(queryXml);
        }
    }
    
    /**
     * Sets (as xml) the "queryXml" element
     */
    public void xsetQueryXml(org.apache.xmlbeans.XmlString queryXml)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUERYXML$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(QUERYXML$2);
            }
            target.set(queryXml);
        }
    }
    
    /**
     * Gets array of all "parameterizedNodes" elements
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedNode[] getParameterizedNodesArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(PARAMETERIZEDNODES$4, targetList);
            com.hp.schemas.ucmdb._1.types.ParameterizedNode[] result = new com.hp.schemas.ucmdb._1.types.ParameterizedNode[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "parameterizedNodes" element
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedNode getParameterizedNodesArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedNode)get_store().find_element_user(PARAMETERIZEDNODES$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "parameterizedNodes" element
     */
    public int sizeOfParameterizedNodesArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PARAMETERIZEDNODES$4);
        }
    }
    
    /**
     * Sets array of all "parameterizedNodes" element
     */
    public void setParameterizedNodesArray(com.hp.schemas.ucmdb._1.types.ParameterizedNode[] parameterizedNodesArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(parameterizedNodesArray, PARAMETERIZEDNODES$4);
        }
    }
    
    /**
     * Sets ith "parameterizedNodes" element
     */
    public void setParameterizedNodesArray(int i, com.hp.schemas.ucmdb._1.types.ParameterizedNode parameterizedNodes)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedNode)get_store().find_element_user(PARAMETERIZEDNODES$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(parameterizedNodes);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "parameterizedNodes" element
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedNode insertNewParameterizedNodes(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedNode)get_store().insert_element_user(PARAMETERIZEDNODES$4, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "parameterizedNodes" element
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedNode addNewParameterizedNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedNode)get_store().add_element_user(PARAMETERIZEDNODES$4);
            return target;
        }
    }
    
    /**
     * Removes the ith "parameterizedNodes" element
     */
    public void removeParameterizedNodes(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PARAMETERIZEDNODES$4, i);
        }
    }
}
