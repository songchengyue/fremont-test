/*
 * An XML document type.
 * Localname: rediscoverViewCIsResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one rediscoverViewCIsResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class RediscoverViewCIsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponseDocument
{
    
    public RediscoverViewCIsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REDISCOVERVIEWCISRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "rediscoverViewCIsResponse");
    
    
    /**
     * Gets the "rediscoverViewCIsResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponse getRediscoverViewCIsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponse)get_store().find_element_user(REDISCOVERVIEWCISRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "rediscoverViewCIsResponse" element
     */
    public void setRediscoverViewCIsResponse(com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponse rediscoverViewCIsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponse)get_store().find_element_user(REDISCOVERVIEWCISRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponse)get_store().add_element_user(REDISCOVERVIEWCISRESPONSE$0);
            }
            target.set(rediscoverViewCIsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "rediscoverViewCIsResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponse addNewRediscoverViewCIsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsResponse)get_store().add_element_user(REDISCOVERVIEWCISRESPONSE$0);
            return target;
        }
    }
}
