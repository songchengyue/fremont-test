package com.telstra.netops.routemanagement.server.model.cgi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telstra.netops.routemanagement.server.model.PrefixRecord;

/**
 * Wrapper class for View Prefix
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ViewPrefixListWrapper {

	@JsonProperty
	private List<PrefixRecord> result;

	@JsonIgnore
	public boolean isEmpty() {
		return result == null || result.get(0) == null || result.get(0).notARecord();
	}

	public List<PrefixRecord> getResult() {
		return result;
	}

}
