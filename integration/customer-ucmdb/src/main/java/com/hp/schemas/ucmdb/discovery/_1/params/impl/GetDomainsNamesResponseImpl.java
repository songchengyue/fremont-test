/*
 * XML Type:  getDomainsNamesResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML getDomainsNamesResponse(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class GetDomainsNamesResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse
{
    
    public GetDomainsNamesResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DOMAINNAMES$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "domainNames");
    
    
    /**
     * Gets the "domainNames" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList getDomainNames()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(DOMAINNAMES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "domainNames" element
     */
    public void setDomainNames(com.hp.schemas.ucmdb._1.types.StrList domainNames)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(DOMAINNAMES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(DOMAINNAMES$0);
            }
            target.set(domainNames);
        }
    }
    
    /**
     * Appends and returns a new empty "domainNames" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList addNewDomainNames()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(DOMAINNAMES$0);
            return target;
        }
    }
}
