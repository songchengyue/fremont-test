/*
 * XML Type:  Conditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.Conditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types;


/**
 * An XML Conditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public interface Conditions extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Conditions.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("conditions93c4type");
    
    /**
     * Gets the "booleanConditions" element
     */
    com.hp.schemas.ucmdb._1.types.BooleanConditions getBooleanConditions();
    
    /**
     * True if has "booleanConditions" element
     */
    boolean isSetBooleanConditions();
    
    /**
     * Sets the "booleanConditions" element
     */
    void setBooleanConditions(com.hp.schemas.ucmdb._1.types.BooleanConditions booleanConditions);
    
    /**
     * Appends and returns a new empty "booleanConditions" element
     */
    com.hp.schemas.ucmdb._1.types.BooleanConditions addNewBooleanConditions();
    
    /**
     * Unsets the "booleanConditions" element
     */
    void unsetBooleanConditions();
    
    /**
     * Gets the "dateConditions" element
     */
    com.hp.schemas.ucmdb._1.types.DateConditions getDateConditions();
    
    /**
     * True if has "dateConditions" element
     */
    boolean isSetDateConditions();
    
    /**
     * Sets the "dateConditions" element
     */
    void setDateConditions(com.hp.schemas.ucmdb._1.types.DateConditions dateConditions);
    
    /**
     * Appends and returns a new empty "dateConditions" element
     */
    com.hp.schemas.ucmdb._1.types.DateConditions addNewDateConditions();
    
    /**
     * Unsets the "dateConditions" element
     */
    void unsetDateConditions();
    
    /**
     * Gets the "doubleConditions" element
     */
    com.hp.schemas.ucmdb._1.types.DoubleConditions getDoubleConditions();
    
    /**
     * True if has "doubleConditions" element
     */
    boolean isSetDoubleConditions();
    
    /**
     * Sets the "doubleConditions" element
     */
    void setDoubleConditions(com.hp.schemas.ucmdb._1.types.DoubleConditions doubleConditions);
    
    /**
     * Appends and returns a new empty "doubleConditions" element
     */
    com.hp.schemas.ucmdb._1.types.DoubleConditions addNewDoubleConditions();
    
    /**
     * Unsets the "doubleConditions" element
     */
    void unsetDoubleConditions();
    
    /**
     * Gets the "floatConditions" element
     */
    com.hp.schemas.ucmdb._1.types.FloatConditions getFloatConditions();
    
    /**
     * True if has "floatConditions" element
     */
    boolean isSetFloatConditions();
    
    /**
     * Sets the "floatConditions" element
     */
    void setFloatConditions(com.hp.schemas.ucmdb._1.types.FloatConditions floatConditions);
    
    /**
     * Appends and returns a new empty "floatConditions" element
     */
    com.hp.schemas.ucmdb._1.types.FloatConditions addNewFloatConditions();
    
    /**
     * Unsets the "floatConditions" element
     */
    void unsetFloatConditions();
    
    /**
     * Gets the "intConditions" element
     */
    com.hp.schemas.ucmdb._1.types.IntConditions getIntConditions();
    
    /**
     * True if has "intConditions" element
     */
    boolean isSetIntConditions();
    
    /**
     * Sets the "intConditions" element
     */
    void setIntConditions(com.hp.schemas.ucmdb._1.types.IntConditions intConditions);
    
    /**
     * Appends and returns a new empty "intConditions" element
     */
    com.hp.schemas.ucmdb._1.types.IntConditions addNewIntConditions();
    
    /**
     * Unsets the "intConditions" element
     */
    void unsetIntConditions();
    
    /**
     * Gets the "intListConditions" element
     */
    com.hp.schemas.ucmdb._1.types.IntListConditions getIntListConditions();
    
    /**
     * True if has "intListConditions" element
     */
    boolean isSetIntListConditions();
    
    /**
     * Sets the "intListConditions" element
     */
    void setIntListConditions(com.hp.schemas.ucmdb._1.types.IntListConditions intListConditions);
    
    /**
     * Appends and returns a new empty "intListConditions" element
     */
    com.hp.schemas.ucmdb._1.types.IntListConditions addNewIntListConditions();
    
    /**
     * Unsets the "intListConditions" element
     */
    void unsetIntListConditions();
    
    /**
     * Gets the "strConditions" element
     */
    com.hp.schemas.ucmdb._1.types.StrConditions getStrConditions();
    
    /**
     * True if has "strConditions" element
     */
    boolean isSetStrConditions();
    
    /**
     * Sets the "strConditions" element
     */
    void setStrConditions(com.hp.schemas.ucmdb._1.types.StrConditions strConditions);
    
    /**
     * Appends and returns a new empty "strConditions" element
     */
    com.hp.schemas.ucmdb._1.types.StrConditions addNewStrConditions();
    
    /**
     * Unsets the "strConditions" element
     */
    void unsetStrConditions();
    
    /**
     * Gets the "strListConditions" element
     */
    com.hp.schemas.ucmdb._1.types.StrListConditions getStrListConditions();
    
    /**
     * True if has "strListConditions" element
     */
    boolean isSetStrListConditions();
    
    /**
     * Sets the "strListConditions" element
     */
    void setStrListConditions(com.hp.schemas.ucmdb._1.types.StrListConditions strListConditions);
    
    /**
     * Appends and returns a new empty "strListConditions" element
     */
    com.hp.schemas.ucmdb._1.types.StrListConditions addNewStrListConditions();
    
    /**
     * Unsets the "strListConditions" element
     */
    void unsetStrListConditions();
    
    /**
     * Gets the "xmlConditions" element
     */
    com.hp.schemas.ucmdb._1.types.XmlConditions getXmlConditions();
    
    /**
     * True if has "xmlConditions" element
     */
    boolean isSetXmlConditions();
    
    /**
     * Sets the "xmlConditions" element
     */
    void setXmlConditions(com.hp.schemas.ucmdb._1.types.XmlConditions xmlConditions);
    
    /**
     * Appends and returns a new empty "xmlConditions" element
     */
    com.hp.schemas.ucmdb._1.types.XmlConditions addNewXmlConditions();
    
    /**
     * Unsets the "xmlConditions" element
     */
    void unsetXmlConditions();
    
    /**
     * Gets the "longConditions" element
     */
    com.hp.schemas.ucmdb._1.types.LongConditions getLongConditions();
    
    /**
     * True if has "longConditions" element
     */
    boolean isSetLongConditions();
    
    /**
     * Sets the "longConditions" element
     */
    void setLongConditions(com.hp.schemas.ucmdb._1.types.LongConditions longConditions);
    
    /**
     * Appends and returns a new empty "longConditions" element
     */
    com.hp.schemas.ucmdb._1.types.LongConditions addNewLongConditions();
    
    /**
     * Unsets the "longConditions" element
     */
    void unsetLongConditions();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.types.Conditions newInstance() {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.Conditions parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.Conditions) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
