/*
 * An XML document type.
 * Localname: checkDiscoveryProgressRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one checkDiscoveryProgressRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class CheckDiscoveryProgressRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequestDocument
{
    
    public CheckDiscoveryProgressRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHECKDISCOVERYPROGRESSREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "checkDiscoveryProgressRequest");
    
    
    /**
     * Gets the "checkDiscoveryProgressRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequest getCheckDiscoveryProgressRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequest)get_store().find_element_user(CHECKDISCOVERYPROGRESSREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "checkDiscoveryProgressRequest" element
     */
    public void setCheckDiscoveryProgressRequest(com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequest checkDiscoveryProgressRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequest)get_store().find_element_user(CHECKDISCOVERYPROGRESSREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequest)get_store().add_element_user(CHECKDISCOVERYPROGRESSREQUEST$0);
            }
            target.set(checkDiscoveryProgressRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "checkDiscoveryProgressRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequest addNewCheckDiscoveryProgressRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressRequest)get_store().add_element_user(CHECKDISCOVERYPROGRESSREQUEST$0);
            return target;
        }
    }
}
