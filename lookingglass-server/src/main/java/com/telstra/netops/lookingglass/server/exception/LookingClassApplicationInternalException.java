package com.telstra.netops.lookingglass.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Throw and log this exception for any server-side problem.
 * 
 * @author d772392
 *
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "There was an internal application error")
public class LookingClassApplicationInternalException extends RuntimeException {
	
	public LookingClassApplicationInternalException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public LookingClassApplicationInternalException(String message) {
		super(message);
	}

}
