package com.telstra.netops.client.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Token {

	@JsonProperty("access_token")
	private String accessToken;

	@JsonProperty("access_type")
	private String tokenType;

	@JsonProperty("expires_in")
	private Integer expiresIn;

	private String scope;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public Integer getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String toString() {
		return new ToStringBuilder(this)
				.append("accessToken", accessToken)
				.append("tokenType", tokenType)
				.append("expiresIn", expiresIn)
				.append("scope", scope)
				.toString();
	}

}
