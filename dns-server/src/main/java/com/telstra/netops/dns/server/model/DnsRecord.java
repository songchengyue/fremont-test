package com.telstra.netops.dns.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DnsRecord {
	
	@JsonProperty
	private DnsRecordType recordType;
	
	@JsonProperty
	private String contactEmailAddress;
	
	@JsonProperty
	private String domain;
	
	@JsonProperty
	private String host;
	
	@JsonProperty
	private String parameter;
	
	@JsonProperty
	private String request;

	public DnsRecord() {
	}
	
	public DnsRecord(DnsRecordType recordType, String contactEmailAddress, String domain,
			String host, String parameter, String request) {
		super();
		this.recordType = recordType;
		this.contactEmailAddress = contactEmailAddress;
		this.domain = domain;
		this.host = host;
		this.parameter = parameter;
		this.request = request;
	}

	public DnsRecordType getRecordType() {
		return recordType;
	}

	public void setRecordType(DnsRecordType recordType) {
		this.recordType = recordType;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

}
