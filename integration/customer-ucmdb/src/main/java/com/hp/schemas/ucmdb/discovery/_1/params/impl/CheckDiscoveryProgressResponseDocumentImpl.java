/*
 * An XML document type.
 * Localname: checkDiscoveryProgressResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one checkDiscoveryProgressResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class CheckDiscoveryProgressResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponseDocument
{
    
    public CheckDiscoveryProgressResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHECKDISCOVERYPROGRESSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "checkDiscoveryProgressResponse");
    
    
    /**
     * Gets the "checkDiscoveryProgressResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponse getCheckDiscoveryProgressResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponse)get_store().find_element_user(CHECKDISCOVERYPROGRESSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "checkDiscoveryProgressResponse" element
     */
    public void setCheckDiscoveryProgressResponse(com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponse checkDiscoveryProgressResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponse)get_store().find_element_user(CHECKDISCOVERYPROGRESSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponse)get_store().add_element_user(CHECKDISCOVERYPROGRESSRESPONSE$0);
            }
            target.set(checkDiscoveryProgressResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "checkDiscoveryProgressResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponse addNewCheckDiscoveryProgressResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckDiscoveryProgressResponse)get_store().add_element_user(CHECKDISCOVERYPROGRESSRESPONSE$0);
            return target;
        }
    }
}
