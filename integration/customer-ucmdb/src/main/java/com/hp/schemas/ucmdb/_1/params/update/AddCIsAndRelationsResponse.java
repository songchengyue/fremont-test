/*
 * XML Type:  addCIsAndRelationsResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update;


/**
 * An XML addCIsAndRelationsResponse(@http://schemas.hp.com/ucmdb/1/params/update).
 *
 * This is a complex type.
 */
public interface AddCIsAndRelationsResponse extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AddCIsAndRelationsResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("addcisandrelationsresponse8652type");
    
    /**
     * Gets array of all "CreatedIDsMap" elements
     */
    com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID[] getCreatedIDsMapArray();
    
    /**
     * Gets ith "CreatedIDsMap" element
     */
    com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID getCreatedIDsMapArray(int i);
    
    /**
     * Returns number of "CreatedIDsMap" element
     */
    int sizeOfCreatedIDsMapArray();
    
    /**
     * Sets array of all "CreatedIDsMap" element
     */
    void setCreatedIDsMapArray(com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID[] createdIDsMapArray);
    
    /**
     * Sets ith "CreatedIDsMap" element
     */
    void setCreatedIDsMapArray(int i, com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID createdIDsMap);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "CreatedIDsMap" element
     */
    com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID insertNewCreatedIDsMap(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "CreatedIDsMap" element
     */
    com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID addNewCreatedIDsMap();
    
    /**
     * Removes the ith "CreatedIDsMap" element
     */
    void removeCreatedIDsMap(int i);
    
    /**
     * Gets the "comments" element
     */
    com.hp.schemas.ucmdb._1.types.ResComments getComments();
    
    /**
     * Sets the "comments" element
     */
    void setComments(com.hp.schemas.ucmdb._1.types.ResComments comments);
    
    /**
     * Appends and returns a new empty "comments" element
     */
    com.hp.schemas.ucmdb._1.types.ResComments addNewComments();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse newInstance() {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
