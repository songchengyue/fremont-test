package com.telstra.netops.routemanagement.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Modal class for Delete Prefix
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeletePrefixRecord {

	private String customerId;

	private String[] ids;
	private String service;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String[] getIds() {
		return ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

}
