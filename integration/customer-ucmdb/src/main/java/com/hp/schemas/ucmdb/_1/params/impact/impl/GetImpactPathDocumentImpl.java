/*
 * An XML document type.
 * Localname: getImpactPath
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactPathDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * A document containing one getImpactPath(@http://schemas.hp.com/ucmdb/1/params/impact) element.
 *
 * This is a complex type.
 */
public class GetImpactPathDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactPathDocument
{
    
    public GetImpactPathDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETIMPACTPATH$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactPath");
    
    
    /**
     * Gets the "getImpactPath" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactPath getGetImpactPath()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactPath target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactPath)get_store().find_element_user(GETIMPACTPATH$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getImpactPath" element
     */
    public void setGetImpactPath(com.hp.schemas.ucmdb._1.params.impact.GetImpactPath getImpactPath)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactPath target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactPath)get_store().find_element_user(GETIMPACTPATH$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactPath)get_store().add_element_user(GETIMPACTPATH$0);
            }
            target.set(getImpactPath);
        }
    }
    
    /**
     * Appends and returns a new empty "getImpactPath" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactPath addNewGetImpactPath()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactPath target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactPath)get_store().add_element_user(GETIMPACTPATH$0);
            return target;
        }
    }
}
