package com.hp.ucmdb.generated;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator.Enum;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeDocument;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponseDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponseDocument;
import com.hp.schemas.ucmdb._1.types.props.CustomProperties;
import com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties;
import com.hp.schemas.ucmdb._1.types.props.PropertiesList;
import com.hp.schemas.ucmdb._1.types.props.TypedProperties;
import com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection;
import com.hp.schemas.ucmdb._1.types.*;
import com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator;



public class UcmdbRfsResource {

	static String configPath;

	private static final Logger LOGGER = LoggerFactory.getLogger(UcmdbNetopsSynch.class);
	static Properties props = new Properties();
	static Connection dbConnection;
	static Statement statement;
	static UcmdbServiceStub stub = null;
	static int numberOfChunks;
	static int chunkNumber;

	static String key1;
	static String key2;
	static int syncId = 1;
	static String syncStatus = "";
	static String oprtnStatus = "Y";
	static String syncType = "";

	static String className3 ="rfs_to_resource_relation";


	static String propertyName2 = "service_id";
	static String propertyName3 = "resource_id";
	static String conditionName3 = "service_id";


	static String callerApplication = "netops";
	static String cndtnLgclOprtrOr = "OR";
	static String strOprtr = "EqualIgnoreCase";


	static List<String> cmdbIds = new ArrayList<String>();
	static List<String> serviceIds = new ArrayList<String>();
	static List<String> serviceIdsNew = new ArrayList<String>();
	static List<String> serviceIdsTemp ;
	static List<String> resourceIds = new ArrayList<String>();
	static List<Timestamp> fullLastUpdatedDate = new ArrayList<Timestamp>();
	
	static int serviceIdSize ;
	static int serviceIdTempSize;
	
	static String cmdbId = "";
	static String serviceId = "";
	static String resourceId = "";

	static String getName;
	static String getValue;
	
	public static void getResourceIds(UcmdbServiceStub ucmdbStub,String mainConfigPath) {
		stub = ucmdbStub;
		configPath = mainConfigPath;
		try {
			loadParams(configPath); 
			dbConnection = getDBConnection();
			
			//Deleting previous entries from RFS_TO_RSRC_RLTN_1_STG
			Statement delstmt = dbConnection.createStatement();
			delstmt.executeQuery("delete from RFS_TO_RSRC_RLTN_1_STG");
			LOGGER.info("deleted");
			
			statement = dbConnection.createStatement();

			String selectSQL = "SELECT distinct(A_SERVICE_ID) FROM CDM_RFS_1_STG " ;

			try {
				dbConnection = getDBConnection();
				statement = dbConnection.createStatement();

				////LOGGER.info(selectSQL);
				ResultSet rSet = statement.executeQuery(selectSQL);
				while(rSet.next())
				{
					////LOGGER.info("within select loop");
					serviceId = rSet.getString("A_SERVICE_ID");
					serviceIds.add(serviceId);
					////LOGGER.info("serviceId : " +serviceId);
				}
				serviceIdSize = serviceIds.size();
		
		LOGGER.info("serviceIdSize : " + serviceIdSize);
		//Dividing into batches of 50
		//for(int i=0; i< serviceIdSize;i++)
		int i=0;
		while(i<serviceIdSize)
		{
			int count = 0;
			serviceIdsTemp = new ArrayList<String>();
					while((count<50) && (i<serviceIdSize))
					{
						////LOGGER.info("i : " +i);
						////LOGGER.info(cfsIds.get(i));
						serviceIdsTemp.add(serviceIds.get(i));
					i++;
					count++;
					////LOGGER.info("i : " +i);
					////LOGGER.info("count : " +count);
					}
					serviceIdTempSize = serviceIdsTemp.size();
					//LOGGER.info("helloo");
					//LOGGER.info("serviceIdTempSize : " +serviceIdTempSize);
					getFilteredCIsByTypeDemo();
		}
		//LOGGER.info("serviceIdSize : " + serviceIdSize);
		insertIntoTable();
				
			} catch (Exception e) {
				 LOGGER.error("exception inside getServiceIdList Division", e);
			}
	
			
		} catch (Exception e) {
			 LOGGER.error("exception inside getServiceIdList", e);
		}
	}
	
	/* getFilteredCIsByType Operation*/
	public static void getFilteredCIsByTypeDemo() {	
		try {	
			LOGGER.info("inside getFilteredCIsByTypeDemo method");
			////LOGGER.info("serviceIdTempSize : " + serviceIdTempSize);
			
			Enum conditionsLogicalOperator = Enum.forString(cndtnLgclOprtrOr);

			StrCondition[] strConditionArray = new StrCondition[serviceIdTempSize];
			for(int j=0; j<serviceIdTempSize;j++)
			{
				StrCondition strCondition =  StrCondition.Factory.newInstance();
				StrProp strProp = StrProp.Factory.newInstance();
				strProp.setName(conditionName3);
				strProp.setValue(serviceIdsTemp.get(j));
				strCondition.setCondition(strProp);
				strCondition.setStrOperator(StrOperator.Enum.forString(strOprtr));
				strConditionArray[j]= strCondition;
				
			}
	
			Conditions conditions = Conditions.Factory.newInstance();
			StrConditions strConditions= StrConditions.Factory.newInstance();
			
			
			strConditions.setStrConditionArray(strConditionArray);
			conditions.setStrConditions(strConditions);
			
			CmdbContext cntxt = CmdbContext.Factory.newInstance();
			cntxt.setCallerApplication(callerApplication);
			
			GetFilteredCIsByType getFilteredCIsByType = GetFilteredCIsByType.Factory.newInstance();
			getFilteredCIsByType.setCmdbContext(cntxt);
			getFilteredCIsByType.setType(className3);
			getFilteredCIsByType.setConditions(conditions);
			getFilteredCIsByType.setConditionsLogicalOperator(conditionsLogicalOperator);
			

			CustomProperties properties = CustomProperties.Factory.newInstance();
			PropertiesList propertiesList= PropertiesList.Factory.newInstance();
			propertiesList.addPropertyName(propertyName2);
			propertiesList.addPropertyName(propertyName3);
			properties.setPropertiesList(propertiesList);
			getFilteredCIsByType.setProperties(properties);

			GetFilteredCIsByTypeDocument request = GetFilteredCIsByTypeDocument.Factory.newInstance();
			request.setGetFilteredCIsByType(getFilteredCIsByType);
			
			////LOGGER.info("getFilteredCIsByType request : " + request);					
			////LOGGER.info("set inputs");
			
			HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
			basicAuthentication.setUsername("admin");
			basicAuthentication.setPassword("admin");
			int so_timeout = 500000;
			int con_timeout = 70000;
			stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
			////LOGGER.info("authentication passed");

			GetFilteredCIsByTypeResponseDocument response = stub.getFilteredCIsByType(request);
			////LOGGER.info("getFilteredCIsByType response : " + response);
						
			ChunkInfo chunkInfo = response.getGetFilteredCIsByTypeResponse().getChunkInfo();
			numberOfChunks = chunkInfo.getNumberOfChunks();
			ChunkKey chunkKey = chunkInfo.getChunksKey();
			key1 = chunkKey.getKey1();
			key2 = chunkKey.getKey2();
			
			LOGGER.info("numberOfChunks : " + numberOfChunks);
			////LOGGER.info("key1 : " + key1);
			////LOGGER.info("key2 : " + key2);
			if(numberOfChunks==0)
			{
			CI[] CI = response.getGetFilteredCIsByTypeResponse().getCIs().getCIArray();
			//LOGGER.info("Within zero chunks :CI.length : " +CI.length);
			java.util.Date date= new java.util.Date();
			for(int j=0 ; j < CI.length ; j++)
			{
				cmdbId = CI[j].getID().getStringValue();
				cmdbIds.add(cmdbId);
				fullLastUpdatedDate.add(new Timestamp(date.getTime()));
				StrProp[] strprop = CI[j].getProps().getStrProps().getStrPropArray();
				//LOGGER.info("strprop.length : " + strprop.length);
				if(strprop.length!=2)
				{
					LOGGER.info("cmdbIdNoRsrc : "+cmdbId);
					serviceIdsNew.add("");
					resourceIds.add("");
				}
				else
				{
				for (int p = 0; p < strprop.length; p++)
				{
					getName = strprop[p].getName();
					getValue = strprop[p].getValue();
					if(getName.equalsIgnoreCase("service_id")){
						serviceId = getValue;
						serviceIdsNew.add(serviceId);
						////LOGGER.info("serviceId : " + serviceId);
					}
					if(getName.equalsIgnoreCase("resource_id")){
						resourceId = getValue;
						resourceIds.add(resourceId);
						////LOGGER.info("resourceId : " + resourceId);
					}
				}
			}
			}
			//LOGGER.info(resourceIds.size());
			//LOGGER.info(cmdbIds.size());
			//LOGGER.info("SUCCESS");
			//insertIntoTable();
			}
			else
			{
				pullTopologyMapChunksFull();
			}
			
		
		}catch (RemoteException rme) {
			LOGGER.error("RemoteException inside getFilteredCIsByTypeDemo", rme);
		} catch (UcmdbFault ufe) {
			LOGGER.error("UcmdbFaultException inside getFilteredCIsByTypeDemo", ufe);
		} catch (Exception e) {
			  LOGGER.error("exception inside getFilteredCIsByTypeDemo", e);
		}

	}

	 // pullTopologyMapChunksFull Operation
		public static void pullTopologyMapChunksFull() {
			LOGGER.info("inside pullTopologyMapChunksFull method");

			try {
				
				//LOGGER.info("numberOfChunks : " + numberOfChunks);
				////LOGGER.info("key1 : " + key1);
				////LOGGER.info("key2 : " + key2);
				for(int k=1 ; k <= numberOfChunks ; k++)
				{
					chunkNumber = k;
					////LOGGER.info("chunkNumber:" +chunkNumber);
					
					CmdbContext cntxt = CmdbContext.Factory.newInstance();
					cntxt.setCallerApplication(callerApplication);
					ChunkRequest chunkRequest = ChunkRequest.Factory.newInstance();
					ChunkInfo chunkInfo = ChunkInfo.Factory.newInstance();
					
					chunkRequest.setChunkNumber(chunkNumber);
					chunkInfo.setNumberOfChunks(numberOfChunks);

					ChunkKey chunksKey = ChunkKey.Factory.newInstance();
					chunksKey.setKey1(key1);
					chunksKey.setKey2(key2);
					
					chunkInfo.setChunksKey(chunksKey);
					chunkRequest.setChunkInfo(chunkInfo );
					
					
					TypedPropertiesCollection queryTypedProperties = TypedPropertiesCollection.Factory.newInstance();
					TypedProperties typedProperties = TypedProperties.Factory.newInstance();
					typedProperties.setType(className3);
					CustomTypedProperties properties = CustomTypedProperties.Factory.newInstance();
					PropertiesList propertiesList = PropertiesList.Factory.newInstance();
					String[] propertyNameArray = {propertyName2,propertyName3};
					propertiesList.setPropertyNameArray(propertyNameArray );

					properties.setPropertiesList(propertiesList);
					typedProperties.setProperties(properties);
					
					TypedProperties[] typedPropertiesArray ={typedProperties};
					queryTypedProperties.setTypedPropertiesArray(typedPropertiesArray );
					
					PullTopologyMapChunks pullTopologyMapChunks = PullTopologyMapChunks.Factory.newInstance();
					
					pullTopologyMapChunks.setCmdbContext(cntxt);
					pullTopologyMapChunks.setChunkRequest(chunkRequest);
					pullTopologyMapChunks.setQueryTypedProperties(queryTypedProperties);
					PullTopologyMapChunksDocument request = PullTopologyMapChunksDocument.Factory.newInstance();
					request.setPullTopologyMapChunks(pullTopologyMapChunks);
					////LOGGER.info("pullTopologyMapChunks request" +request);					
					
					HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
					basicAuthentication.setUsername("admin");
					basicAuthentication.setPassword("admin");
					int so_timeout = 500000;
					int con_timeout = 70000;
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
					////LOGGER.info("authentication passed");

					PullTopologyMapChunksResponseDocument response = stub.pullTopologyMapChunks(request);
					////LOGGER.info("pullTopologyMapChunks response : " + response);
					
					
					CINode[] ciNode = response.getPullTopologyMapChunksResponse().getTopologyMap().getCINodes().getCINodeArray();
					CI[] ci = ciNode[0].getCIs().getCIArray();
					////LOGGER.info("ci : " + ci);
					////LOGGER.info(ci.length);
					java.util.Date date= new java.util.Date();
					for(int j=0 ; j < ci.length ; j++)
					{						
						cmdbId = ci[j].getID().getStringValue();							
						cmdbIds.add(cmdbId);
						fullLastUpdatedDate.add(new Timestamp(date.getTime()));
							
						StrProp[] strprop = ci[j].getProps().getStrProps().getStrPropArray();
						//LOGGER.info("strprop.lengthInChunk : " + strprop.length);
						if(strprop.length!=2)
						{
							LOGGER.info("cmdbIdNoRsrc : "+cmdbId);
							serviceIdsNew.add("");
							resourceIds.add("");
						}
						else
						{
							for (int p = 0; p < strprop.length; p++)
							{
								getName = strprop[p].getName();
								getValue = strprop[p].getValue();

								////LOGGER.info("value of p : "+p);
								////LOGGER.info("cmdbId : " + cmdbId);
								////LOGGER.info("getName : " + getName);
								if(getName.equalsIgnoreCase("service_id")){
									serviceId = getValue;
									serviceIdsNew.add(serviceId);
									////LOGGER.info("serviceId : " + serviceId);
								}
								if(getName.equalsIgnoreCase("resource_id")){
									resourceId = getValue;
									resourceIds.add(resourceId);
									////LOGGER.info("resourceId : " + resourceId);
								}
								
							}		
						}
					}


				}
			LOGGER.info("OUTSIDE CHUNK LOOP");
			////LOGGER.info("serviceIdsNewSize" +serviceIdsNew.size());
			//LOGGER.info("resourceIdsSize" +resourceIds.size());
			//LOGGER.info("cmdbIdsSize" +cmdbIds.size());
			//LOGGER.info("SUCCESS");
			//insertIntoTable();
			}
			catch (RemoteException rme) {
				LOGGER.error("RemoteException inside pullTopologyMapChunks", rme);
			} catch (UcmdbFault ufe) {
				LOGGER.error("UcmdbFaultException inside pullTopologyMapChunks", ufe);
			}		
			 catch (Exception e) {
				  LOGGER.error("exception inside pullTopologyMapChunks", e);
			}

		}
	
	private static void insertIntoTable() {
		//Inserting Values into RFS_TO_RSRC_RLTN_1_STG TABLE
		try {
			LOGGER.info("Inserting");
			LOGGER.info("serviceIdsNewSize" +serviceIdsNew.size());
			LOGGER.info("resourceIdsSize" +resourceIds.size());
			LOGGER.info("cmdbIdsSize" +cmdbIds.size());
			
			dbConnection = getDBConnection();
			String stringSQL = "INSERT INTO RFS_TO_RSRC_RLTN_1_STG (CMDB_ID,A_SERVICE_ID,A_RESOURCE_ID,TIMESTAMP) VALUES (?,?,?,?)" ;
			PreparedStatement ps = dbConnection.prepareStatement(stringSQL);
			
			for (int k = 0; k < resourceIds.size(); k++){
				/*LOGGER.info(cmdbIds.get(k));
				LOGGER.info(resourceIds.get(k));*/
				ps.setString(1, cmdbIds.get(k));
				ps.setString(2, serviceIdsNew.get(k));
				ps.setString(3, resourceIds.get(k));
				ps.setTimestamp(4, fullLastUpdatedDate.get(k));									
				ps.addBatch();
			}		
			////LOGGER.info("completed batch execution");
			ps.executeBatch();
			ps.close();
			dbConnection.close();
			////LOGGER.info("connection closed");
		} 
		catch (SQLException sqle) {
			LOGGER.info(sqle.getMessage());			
			LOGGER.error("SQL exception inside insertIntoTable", sqle);
			//System.exit(1);
		}
		catch (Exception e) {
			 LOGGER.error("exception inside insertIntoTable", e);
		}	
		
	}

	public static void loadParams(String configPath) {
	    InputStream is = null;
	 
	    try {
	    	LOGGER.info("configPath : " +configPath);
	        File f = new File (configPath);
	    	//File f = new File("E:\\Ashwini\\workspaceAhana\\ucmdbSrv\\src\\config.txt");
	        is = new FileInputStream( f );
	    }
	    catch ( Exception e ) { is = null; }
	 
	    try {
	        props.load( is );
	    }
	    catch ( Exception e ) {
	    	LOGGER.error("exception inside loadParams", e);
	    }

	   
	}
	
	private static Connection getDBConnection() {
		//Using config file to remove hardcoding
		String DB_DRIVER = props.getProperty("DB_DRIVER");
		String DB_CONNECTION = props.getProperty("DB_CONNECTION");
		String DB_USER = props.getProperty("DB_USER");
		String DB_PASSWORD = props.getProperty("DB_PASSWORD");
		try {			
			Class.forName(DB_DRIVER);
			
		} catch (ClassNotFoundException e) {
			LOGGER.info(e.getMessage());
		}		
		
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
			return dbConnection;		
		
		} catch (SQLException e) {
			LOGGER.info(e.getMessage());
		}
		return dbConnection;
	}

	
}
