/*
 * XML Type:  TypedProperties
 * Namespace: http://schemas.hp.com/ucmdb/1/types/props
 * Java type: com.hp.schemas.ucmdb._1.types.props.TypedProperties
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.props.impl;
/**
 * An XML TypedProperties(@http://schemas.hp.com/ucmdb/1/types/props).
 *
 * This is a complex type.
 */
public class TypedPropertiesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.props.TypedProperties
{
    
    public TypedPropertiesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TYPE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "type");
    private static final javax.xml.namespace.QName PROPERTIES$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "properties");
    
    
    /**
     * Gets the "type" element
     */
    public java.lang.String getType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "type" element
     */
    public org.apache.xmlbeans.XmlString xgetType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TYPE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "type" element
     */
    public void setType(java.lang.String type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TYPE$0);
            }
            target.setStringValue(type);
        }
    }
    
    /**
     * Sets (as xml) the "type" element
     */
    public void xsetType(org.apache.xmlbeans.XmlString type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TYPE$0);
            }
            target.set(type);
        }
    }
    
    /**
     * Gets the "properties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties getProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties)get_store().find_element_user(PROPERTIES$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "properties" element
     */
    public void setProperties(com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties properties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties)get_store().find_element_user(PROPERTIES$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties)get_store().add_element_user(PROPERTIES$2);
            }
            target.set(properties);
        }
    }
    
    /**
     * Appends and returns a new empty "properties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties addNewProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties)get_store().add_element_user(PROPERTIES$2);
            return target;
        }
    }
}
