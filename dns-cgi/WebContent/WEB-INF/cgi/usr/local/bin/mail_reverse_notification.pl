#!/usr/bin/perl

use strict;
use warnings;
use Mail::Sendmail;
use Net::IP;
my $mailagent = "/usr/sbin/sendmail -t";

use lib "/usr/local/www/wanchai/netops/modules";
use lib "/usr/local/www/wanchai/modules";
my $file1 = "/tmp/file1";
open (DEBUG,">/tmp/mail_rev");

my $i = '';
my ($state, $rec_type, $req_type, $contact, $uid, $ip_blk, $domain, $accno,$oct) = '';
my (@arr, @arr1, $cnt, $first_line,@details, @chk, @msg, $em_id) = '';
my $date = `date`;
print "$date\n";

#to calculate the serial number
my @now = gmtime;
my ($yr,$mth,$mday,$second, $minute, $hour) = ($now[5], $now[4],$now[3], $now[0], $now[1], $now[2]);
$yr += 1900;
$mth += 1;
if ($mth <= 9) { $mth = "0".$mth; }
if ($mday <= 9) { $mday = "0".$mday; }
my $serial_number = "$yr$mth$mday";


#open FILE_MAIL, "</usr/local/www/wanchai/cms/data/revdns/rev_mail/mail_details" or die "$!";
open FILE_MAIL, "</usr/local/www/wanchai/netops/dns/revdns/rev_mail/mail_details/mail_details.$serial_number" or die "$!";

my @line = <FILE_MAIL>;
# Good practice to always strip the trailing
# newline from the line.
chomp(@line);
my $len = @line;




for ($i=0; $i<=$len; $i++) {
print DEBUG "i:$i\tlen:$len\n";
$first_line = $line[$i];
print DEBUG "first line:$first_line\n";
@details = split (/;/,$first_line);
$req_type = $details[0];
$contact = $details[1];
$uid = $details[2];
$ip_blk = $details[3]; 
$oct=$details[4];
$domain = $details[5];
$accno = $details[6];
$rec_type = $details[7];

#print "\n\nreq_type:$req_type\tcontact:$contact\tuid:$uid\tip_blk:$ip_blk\tdomain:$domain\taccno:$accno\t rec_type: $rec_type\n\n";
#$contact = '$contact';
$cnt = $contact;
@chk = split('@',$contact);
#print "$chk[0] \n";
#print "$chk[1] \n\n";
@msg = ("$chk[0]","\\@","$chk[1]");
$em_id  = join(",", @msg);
$em_id  =~ s/,//g;
$contact = $em_id;
print "contact bfr if $contact \n ";
if ($rec_type =~ m/PTR/i) {
print "hi ptr\n";
#`nslookup $ip_blk | grep -i "name" > /tmp/CHK_PTR`;
`nslookup $ip_blk 192.168.39.57 | grep -i "name" > /tmp/CHK_PTR`; # for internal testing
#print "ENTERED PTR PTR PTR\n";
if(-s "/tmp/CHK_PTR") {
print "FILE EXISTS\n";
open(MYINPUTFILE, "</tmp/CHK_PTR");
while(<MYINPUTFILE>) {
 # Good practice to store $_ value because
# subsequent operations may change it.
 my($line) = $_;
# Good practice to always strip the trailing
# newline from the line.
chomp($line);
 # Print the line to the screen and add a newline
my $flag = 0;
          if (($line =~ m/name =/i) || ($line =~ m/Name:/i)) {
                $flag = 1;
                my @arr = split(/=/, $line);
		my @arr1 = split(/:/, $line);
                #print "DOM1:$arr[1]\n DOM2:$domain\n";
		#print "DOM1_arr1:$arr1[1]\n DOM2:$domain\n";
		if (($arr[1] =~ m/$domain/i) || ($arr1[1] =~ m/$domain/i)) {
		print "SUCCESS\n";
		$state  = "SUCCESSFUL";
		my $mail = REVERSE_mail($ip_blk,$req_type,$cnt,$domain,$accno,$state);
		print "inside PTR succes $contact \n";
		#exit;
		}
		else {
                if ($req_type =~ /^CANCELLED/){
                        print "CANCELLED SUCCESSFUL\n";
                        $state  = "SUCCESSFUL";
                        my $mail = REVERSE_mail($ip_blk,$req_type,$cnt,$domain,$accno,$state);
                        } else {
                        print "NO MATCH \n";
                        $state  = "NOT SUCCESSFUL";
                        my $mail = REV_mail($ip_blk,$req_type,$cnt,$domain,$accno,$state);
				print "inside PTR no match $contact \n";
                        }
		}
          } else
          {
                $flag = 0;
          }
#print "FLAG : $flag\n";
}
close(MYINPUTFILE);
}
else {
print "NO PTR\n";
my $mail = REV_mail($ip_blk, $req_type, $cnt, $domain, $accno,$state);
}
}
elsif ($rec_type eq 'NS') {
print "ENTERED NS NS NS\n";
my @y = split(/\//,$ip_blk);
my $z1= shift(@y);
my $z2= shift(@y);

if ($z1 =~ m/\./) {
my @ip_octets = split(/\./,$z1);
print "ip is $z1 \n";

#code to split the stored IPs into octets
my $ip_octets1 = shift(@ip_octets);
my $ip_octets2 = shift(@ip_octets);
my $ip_octets3 = shift(@ip_octets);
my $ip_octets4 = shift(@ip_octets);
my $last_oct = $ip_octets4;
my $ptr = join ".",$ip_octets3,$ip_octets2,$ip_octets1,"in-addr.arpa";
#`dig -x $z1 | grep CNAME > /tmp/CHK_NS`;
`dig \@192.168.39.57 -x $z1 CNAME | grep -i CNAME | grep -v ^";" > /tmp/CHK_NS`; 
if(-s "/tmp/CHK_NS") {
print "FILE EXISTS\n";
open(MYINPUTFILE, "</tmp/CHK_NS");
while(<MYINPUTFILE>) {
 # Good practice to store $_ value because
# subsequent operations may change it.
 my($line) = $_;
# Good practice to always strip the trailing
# newline from the line.
chomp($line);
 # Print the line to the screen and add a newline
my $flag = 0;
          if ($line =~ m/CNAME/i) {
                $flag = 1;
                my @arr = split(/CNAME/, $line);
		my %eq_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);
		my $v_next = $z2;
		my $dom_chk = $eq_range{$v_next};
		my $last_val = ($dom_chk + $last_oct) - 1;
		my $range  = "$last_oct"."-"."$last_val";
		my $dom_chk1 = "$last_oct"."."."$range"."."."$ptr";
			
                print "DOM1:$arr[1]\n DOM2:$dom_chk1\n";
                if ($arr[1] =~ m/$dom_chk1/i) {
                print "SUCCESS\n";
		$state  = "SUCCESSFUL";
                my $mail = REVERSE_mail($ip_blk,$req_type,$cnt,$domain,$accno,$state);
                #exit;
                        if ($req_type =~ /^CANCELLED/){
                        print "CANCELLED SUCCESSFUL\n";
                        $state  = "SUCCESSFUL";
                        my $mail = REVERSE_mail($ip_blk,$req_type,$cnt,$domain, $accno, $state);
                        }

                }
                else {
                print "NO MATCH \n";
		$state  = "NOT SUCCESSFUL";
		my $mail = REV_mail($ip_blk, $req_type, $cnt, $domain, $accno, $state);
                }
          } else
          {
                $flag = 0;
          }
print "FLAG : $flag\n";
}
close(MYINPUTFILE);
} else {
 if ($req_type =~ /^CANCELLED/){
                print "CANCELLED SUCCESSFUL\n";
                $state  = "SUCCESSFUL";
                my $mail = REVERSE_mail($ip_blk, $req_type,$cnt,$domain, $accno, $state);
        }else {
                print "FILE DOES NOt EXIST - PTR REC \n";
                $state  = "NOT SUCCESSFUL";
                my $mail = REV_mail($ip_blk, $req_type, $cnt, $domain, $accno, $state);
        }

}
}

##ip version 6 check 
elsif  ($z1 =~ m/\:/) {

#################################### begin of last code change

		my $str = '';
		my $nib ='';
		my $temp_name = '';
		my $ns_str_full = '';
		my $rdns_blk = $ip_blk;
		print "inside ipv6 checkc for NS record &&& IP_blk is $rdns_blk \n";		


		my $exp_ip = Net::IP::ip_expand_address($rdns_blk, 6);
		my @cnm_pre = split (/\//,$exp_ip);
		my $ip_prefix = $cnm_pre[1];
		my $all_oct = $cnm_pre[0];
		print "IPV6 address $all_oct & prefix $ip_prefix \n" ;

		my $rev6=new Net::IP($all_oct); 				# Net:: ip function for ipv6.arpa 
		my $ptrval=$rev6->reverse_ip();
		 
		
		#$logger->debug(" reverse IPV6 address $ptrval ");
			
			#for substring nibble boundary
			if ($ip_prefix >= 49 && $ip_prefix <= 52){
                                $str = substr($ptrval,40,-1);
				$ns_str_full = substr($ptrval,38,-1);
                        }elsif ($ip_prefix >= 53 && $ip_prefix <= 56){
                                $str = substr($ptrval,38,-1);
				$ns_str_full = substr($ptrval,36,-1);
                        }elsif ($ip_prefix >= 57 && $ip_prefix <= 60){
                                $str = substr($ptrval,36,-1);
				$ns_str_full = substr($ptrval,34,-1);
                        }elsif ($ip_prefix >= 61 && $ip_prefix <= 64){
                                $str = substr($ptrval,34,-1);
				$ns_str_full = substr($ptrval,32,-1);
                        }elsif ($ip_prefix == 48){
                                $str = substr($ptrval,42,-1);
				$ns_str_full = substr($ptrval,40,-30);
                        }

			#determining the last nibble for the prefix 
			my $ns_str = $str;
			$str = substr($ns_str_full,0,1);
                        $str = lc $str;

                        if ($ip_prefix%4 == 1){
                                if ($str >= 0 && $str <= 7){
                                        $nib=(0);
                                }else { $nib=(8);
                                }
                        }elsif ($ip_prefix%4 == 2){
                                if ($str >= 0 && $str <= 3){
                                        $nib = (0);
                                }elsif ($str >= 4 && $str <= 7){
                                        $nib = (4);
                                }elsif ($str == 8 || $str == 9 || $str eq 'a' || $str eq 'b'){
                                        $nib = (8);
                                }else { $nib = ('c');
                                }
                        }elsif($ip_prefix%4 == 3){
                                if ($str == 0 || $str == 1){
                                        $nib = (0);
                                }elsif($str == 2 || $str == 3){
                                        $nib = (2);
                                }elsif($str == 4 || $str == 5){
                                        $nib = (4);
                                }elsif($str == 6 || $str == 7){
                                        $nib = (6);
                                }elsif($str == 8 || $str == 9){
                                        $nib = (8);
                                }elsif($str eq 'a' || $str eq 'b'){
                                        $nib = ('a');
                                }elsif($str eq 'c' || $str eq 'd'){
                                        $nib = ('c');
                                }elsif($str eq 'e' || $str eq 'f'){
                                        $nib = ('e');
                                }
                        }elsif($ip_prefix%4 == 0){
                                $nib=($str, 0, 1, '0');
                        }

			#appending to the substring
			my $arpa=join("\.",$nib,$ns_str);
			print "===> inside NS the arpa record is $arpa \n"; 
   			
### end of arpa record generation for NS 

		
#command to check if changes arre reflected in DNS server 
my $result=`dig \@192.168.39.57 $arpa NS +short | wc -l`;

print "result from dig command is $result \n";

#result returns '0' incase the record is not found in DNS server
if($result != '0') {
print " inside result condition check \n";
                if ($req_type =~ /^REQUESTED/) {
                print " request SUCCESS NS record \n";
				$state  = "SUCCESSFUL";
                my $mail = REVERSE_mail($ip_blk,$req_type,$cnt,$domain,$accno,$state); 
				}
                elsif ($req_type =~ /^CANCELLED/){
                        print "CANCELLED SUCCESSFUL NS record \n";
                        $state  = "SUCCESSFUL";
                        my $mail = REVERSE_mail($ip_blk,$req_type,$cnt,$domain, $accno, $state);
                }     
} else {
                print "FILE DOES NOt EXIST - NS REC \n";
                $state  = "NOT SUCCESSFUL";
                my $mail = REV_mail($ip_blk, $req_type, $cnt, $domain, $accno, $state);
        }


}#end of ipv6 check 
#}
close (FILE_MAIL);
} # end of for loop 

sub REVERSE_mail {
 my ($ip_blk, $req_type, $contact, $domain, $accno, $state) = @_;

print "inside Reverse mail $contact \n";


 open (OUTFILE,">$file1") or die "Cannot open $file1: $!";
 print OUTFILE '<table width=100% border=0 cellpadding=0 cellspacing=0 >'."\n".'<b>'."$req_type $state".'</b><br>'."\n".'<b>The details are as follow:</b><br>'."\n".'<tr><td>'."\n".'<table  border=1 cellpadding=2 cellspacing=0 bgcolor="#FFFFFF" align=left>'."\n".'<tr><td><b>IP ADDR / IP_BLOCK</b>:'."\t$ip_blk".'</td></tr>'."\n".'<tr><td><b>Email:</b>'."\t\t$contact".'</td></tr>'."\n".'<tr><td><b>Account</b>:'."\t\t$accno".'</td></tr>'."\n".'<tr><td><b>FQDN:</b>'."\t\t$domain".'</td></tr>'."\n".'</table></table>';
 close (OUTFILE);

  my $message = "\n\nNote: This email has been automatically generated. Please do not reply to this message as it is unattended.";
  
  my $subj = "Subject: $req_type SUCCESSFUL \n";
  my $to = "To: "."$contact\n";
  #my $to = "To:Hari_V02\@infosys.com\n";
  #my $cc = "Cc: "."Jajati-Keshari.Samal\@team.telstra.com,meena.prakash\@team.telstra.com,meena_prakash\@infosys.com,karuna.ballal\@team.telstra.com,anjan-babu.etha\@team.telstra.com \n";
  #my $cc = "Cc: "."TI.DL-GL-TI_EM\@team.telstra.com, TI.infosyscms\@team.telstra.com \n";
  my $cc = "Cc:Hari_V02\@infosys.com\n";
  my $from = "From: "."tgos\@TelstraInternational.com \n";

        open(SENDMAIL, "|$mailagent") or die "Cannot open $mailagent: $!";
        print SENDMAIL $cc;
        print SENDMAIL $subj;
        print SENDMAIL $from;
        print SENDMAIL $to;
        print SENDMAIL $subj;
        print SENDMAIL "Content-type: text/html\n\n";
        open(FILE1, "$file1") or die "Cannot open $file1: $!";
        print SENDMAIL <FILE1>;
        print SENDMAIL $message;
        close(SENDMAIL);
print "mail sent to $contact\n\n";
}

sub REV_mail {
 my ($ip_blk, $req_type, $contact, $domain, $accno, $state) = @_;

print "inside not successful Reverse mail $contact \n";

 open (OUTFILE,">$file1") or die "Cannot open $file1: $!";
 print OUTFILE '<table width=100% border=0 cellpadding=0 cellspacing=0 >'."\n".'<b>'."$req_type $state".'</b><br>'."\n".'<b>The details are as follow:</b><br>'."\n".'<tr><td>'."\n".'<table  border=1 cellpadding=2 cellspacing=0 bgcolor="#FFFFFF" align=left>'."\n".'<tr><td><b>IP ADDR / IP_BLOCK</b>:'."\t$ip_blk".'</td></tr>'."\n".'<tr><td><b>Email:</b>'."\t\t$contact".'</td></tr>'."\n".'<tr><td><b>Account</b>:'."\t\t$accno".'</td></tr>'."\n".'<tr><td><b>FQDN:</b>'."\t\t$domain".'</td></tr>'."\n".'</table></table>';
 close (OUTFILE);

  my $message = "\n\nNote: This email has been automatically generated. Please do not reply to this message as it is unattended.";
  
  my $subj = "Subject: $req_type IS NOT SUCCESSFUL \n";
  my $to = "To: "."$contact\n";
  #my $cc = "Cc: "."Jajati-Keshari.Samal\@team.telstra.com,meena.prakash\@team.telstra.com,meena_prakash\@infosys.com,karuna.ballal\@team.telstra.com,anjan-babu.etha\@team.telstra.com \n";
  my $cc = "Cc:Hari_V02\@infosys.com \n";
 # my $cc = "Cc: "."TI.DL-GL-TI_EM\@team.telstra.com, TI.infosyscms\@team.telstra.com \n";
  my $from = "From: "."tgos\@TelstraInternational.com \n";

        open(SENDMAIL, "|$mailagent") or die "Cannot open $mailagent: $!";
        print SENDMAIL $cc;
        print SENDMAIL $subj;
        print SENDMAIL $from;
        print SENDMAIL $to;
        print SENDMAIL $subj;
        print SENDMAIL "Content-type: text/html\n\n";
        open(FILE1, "$file1") or die "Cannot open $file1: $!";
        print SENDMAIL <FILE1>;
        print SENDMAIL $message;
        close(SENDMAIL);
print "mail sent to $contact\n\n";
}

#taking backup and deleting the source file
#my $mail_file = "/usr/local/www/wanchai/cms/data/revdns/rev_mail/mail_details";
my $mail_file = "/usr/local/www/wanchai/netops/dns/revdns/rev_mail/mail_details";
if (-e $mail_file){
print "Taking backup..\n";
#`cp -p /usr/local/www/wanchai/cms/data/revdns/rev_mail/mail_details /usr/local/www/wanchai/cms/data/revdns/rev_mail/rev_mail_backup/mail_details.$serial_number`;
`cp -p /usr/local/www/wanchai/netops/dns/revdns/rev_mail/mail_details/mail_details.$serial_number /usr/local/www/wanchai/netops/dns/revdns/rev_mail/rev_mail_backup/mail_details.$serial_number`;

print "Removing source file for mail..\n";
#`rm /usr/local/www/wanchai/cms/data/revdns/rev_mail/mail_details`;
#`rm /usr/local/www/wanchai/netops/dns/revdns/rev_mail/mail_details/mail_details.$serial_number`; #commented out for testing 
}
else {
print "Mail source file does not exist\n";
}
close(DEBUG);
}
