/*
 * An XML document type.
 * Localname: addCIsAndRelationsResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * A document containing one addCIsAndRelationsResponse(@http://schemas.hp.com/ucmdb/1/params/update) element.
 *
 * This is a complex type.
 */
public class AddCIsAndRelationsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponseDocument
{
    
    public AddCIsAndRelationsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDCISANDRELATIONSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "addCIsAndRelationsResponse");
    
    
    /**
     * Gets the "addCIsAndRelationsResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse getAddCIsAndRelationsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse)get_store().find_element_user(ADDCISANDRELATIONSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "addCIsAndRelationsResponse" element
     */
    public void setAddCIsAndRelationsResponse(com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse addCIsAndRelationsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse)get_store().find_element_user(ADDCISANDRELATIONSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse)get_store().add_element_user(ADDCISANDRELATIONSRESPONSE$0);
            }
            target.set(addCIsAndRelationsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "addCIsAndRelationsResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse addNewAddCIsAndRelationsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse)get_store().add_element_user(ADDCISANDRELATIONSRESPONSE$0);
            return target;
        }
    }
}
