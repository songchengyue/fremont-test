package com.telstra.netops.routemanagement.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "netops_productline")
public class ProductLine {

	@Id
	@Column(name = "Product_Line")
	private String code;

	@Column(name = "Product_Name")
	private String name;

	@Column(name = "Service_Product_Line")
	private String serviceCode;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

}
