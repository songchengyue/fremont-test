package com.hp.ucmdb.generated;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator.Enum;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeDocument;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponseDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponseDocument;
import com.hp.schemas.ucmdb._1.types.props.CustomProperties;
import com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties;
import com.hp.schemas.ucmdb._1.types.props.PropertiesList;
import com.hp.schemas.ucmdb._1.types.props.TypedProperties;
import com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection;
import com.hp.schemas.ucmdb._1.types.*;
import com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator;

public class UcmdbTiSite {
	private static final Logger LOGGER = LoggerFactory.getLogger(UcmdbNetopsSynch.class);
	static  UcmdbServiceStub stub =null;
	static int numberOfChunks;
	static int chunkNumber;

	static String key1;
	static String key2;
	
	static int syncId = 1;
	static String syncStatus = "";
	static String oprtnStatus = "Y";
	static String syncType = "FULL";
	static String sync_start_date = "";
	private static final String FULL_OR_CHANGED_LOAD = "full";
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	static String className5 ="ti_site";
	static String propertyName5 = "site_parent_site_id";
	static String propertyName6 = "site_city";
	static String propertyName7 = "site_country";
	static String conditionName5 = "site_id";
	
	static String callerApplication = "netops";
	static String cndtnLgclOprtrOr = "OR";
	static String strOprtr = "EqualIgnoreCase";


	static List<String> cmdbIds = new ArrayList<String>();
	static List<String> siteIds = new ArrayList<String>();
	static List<String> siteIdsTemp ;
	static List<String> siteIdsNew = new ArrayList<String>();
	static List<String> parentSiteIds = new ArrayList<String>();
	static List<String> siteCitys = new ArrayList<String>();
	static List<String> siteCountrys = new ArrayList<String>();
	static List<Timestamp> fullLastUpdatedDate = new ArrayList<Timestamp>();

	static int siteIdSize;
	static int siteIdTempSize;
	
	static String cmdbId = "";
	static String siteId = "";
	static String parentSiteId = "";
	static String siteCity = "";
	static String siteCountry = "";

	static String getName;
	static String getValue;
	static String configPath;

	static Properties props = new Properties();
	static Connection dbConnection;
	static Statement statement;
	
	public static void getParentSiteIds(UcmdbServiceStub ucmdbStub,String mainConfigPath,String syncStartDate) {
		stub = ucmdbStub;
		configPath = mainConfigPath;
		sync_start_date = syncStartDate;
		try {
			loadParams(configPath); 
			dbConnection = getDBConnection();
			
			//Deleting previous entries from CDM_TI_SITE_1_STG
			Statement delstmt = dbConnection.createStatement();
			delstmt.executeQuery("delete from CDM_TI_SITE_1_STG");
			LOGGER.info("deleted");
			
			statement = dbConnection.createStatement();

			String selectSQL = "SELECT distinct(A_A_SITE_ID) FROM CDM_WOL_RSRC_1_STG " ;

			try {
				dbConnection = getDBConnection();
				statement = dbConnection.createStatement();

				//LOGGER.info(selectSQL);
				ResultSet rSet = statement.executeQuery(selectSQL);
				while(rSet.next())
				{
					//LOGGER.info("within select loop");
					siteId = rSet.getString("A_A_SITE_ID");
					siteIds.add(siteId);
					//LOGGER.info("siteId : " +siteId);
				}
/*				//SiteCity null
				siteIds.add("37544");
				siteIds.add("53812");
				//ParentSIteIdNull
				siteIds.add("32792");
				siteIds.add("32793");
				//AllValuesavailable
				siteIds.add("41325");*/
				siteIdSize = siteIds.size();
		
		LOGGER.info("siteIdSize : " + siteIdSize);
		//Dividing into batches of 50
		//for(int i=0; i< siteIdSize;i++)
		int i=0;
		while(i<siteIdSize)
		{
			int count = 0;
			siteIdsTemp = new ArrayList<String>();
					while((count<50) && (i<siteIdSize))
					{
						//LOGGER.info("i : " +i);
						siteIdsTemp.add(siteIds.get(i));
						i++;
						count++;
						//LOGGER.info("i : " +i);
						//LOGGER.info("count : " +count);
					}
					siteIdTempSize = siteIdsTemp.size();
					//LOGGER.info("siteIdTempSize : " +siteIdTempSize);
					getFilteredCIsByTypeDemo();
		}
		insertIntoTable();
				
			} catch (Exception e) {
				LOGGER.error("exception inside getSiteIdList Division", e);
			}
	
			
		} catch (Exception e) {
			LOGGER.error("exception inside getSiteIdList", e);
		}
		
	}
	
	/* getFilteredCIsByType Operation*/
	public static void getFilteredCIsByTypeDemo() {	
		try {	
			LOGGER.info("inside getFilteredCIsByTypeDemo method");
			LOGGER.info("siteIdTempSize : " + siteIdTempSize);
			
			Enum conditionsLogicalOperator = Enum.forString(cndtnLgclOprtrOr);

			StrCondition[] strConditionArray = new StrCondition[siteIdTempSize];
			for(int j=0; j<siteIdTempSize;j++)
			{
				StrCondition strCondition =  StrCondition.Factory.newInstance();
				StrProp strProp = StrProp.Factory.newInstance();
				strProp.setName(conditionName5);
				strProp.setValue(siteIdsTemp.get(j));
				strCondition.setCondition(strProp);
				strCondition.setStrOperator(StrOperator.Enum.forString(strOprtr));
				strConditionArray[j]= strCondition;
				
			}
	
			Conditions conditions = Conditions.Factory.newInstance();
			StrConditions strConditions= StrConditions.Factory.newInstance();
			
			
			strConditions.setStrConditionArray(strConditionArray);
			conditions.setStrConditions(strConditions);
			
			CmdbContext cntxt = CmdbContext.Factory.newInstance();
			cntxt.setCallerApplication(callerApplication);
			
			GetFilteredCIsByType getFilteredCIsByType = GetFilteredCIsByType.Factory.newInstance();
			getFilteredCIsByType.setCmdbContext(cntxt);
			getFilteredCIsByType.setType(className5);
			getFilteredCIsByType.setConditions(conditions);
			getFilteredCIsByType.setConditionsLogicalOperator(conditionsLogicalOperator);
			

			CustomProperties properties = CustomProperties.Factory.newInstance();
			PropertiesList propertiesList= PropertiesList.Factory.newInstance();
			propertiesList.addPropertyName(conditionName5);
			propertiesList.addPropertyName(propertyName5);
			propertiesList.addPropertyName(propertyName6);
			propertiesList.addPropertyName(propertyName7);
			properties.setPropertiesList(propertiesList);
			getFilteredCIsByType.setProperties(properties);

			GetFilteredCIsByTypeDocument request = GetFilteredCIsByTypeDocument.Factory.newInstance();
			request.setGetFilteredCIsByType(getFilteredCIsByType);
			
			//LOGGER.info("getFilteredCIsByType request : " + request);					
			//LOGGER.info("set inputs");
			
			HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
			basicAuthentication.setUsername("admin");
			basicAuthentication.setPassword("admin");
			int so_timeout = 480000;
			int con_timeout = 60000;
			stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
			//LOGGER.info("authentication passed");

			GetFilteredCIsByTypeResponseDocument response = stub.getFilteredCIsByType(request);
			//LOGGER.info("getFilteredCIsByType response : " + response);
						
			ChunkInfo chunkInfo = response.getGetFilteredCIsByTypeResponse().getChunkInfo();
			numberOfChunks = chunkInfo.getNumberOfChunks();
			ChunkKey chunkKey = chunkInfo.getChunksKey();
			key1 = chunkKey.getKey1();
			key2 = chunkKey.getKey2();
			
			LOGGER.info("numberOfChunks : " + numberOfChunks);
			//LOGGER.info("key1 : " + key1);
			//LOGGER.info("key2 : " + key2);
			if(numberOfChunks==0)
			{
			CI[] CI = response.getGetFilteredCIsByTypeResponse().getCIs().getCIArray();
			//LOGGER.info("Within zero chunks :CI.length : " +CI.length);
			java.util.Date date= new java.util.Date();
			for(int j=0 ; j < CI.length ; j++)
			{
				cmdbId = CI[j].getID().getStringValue();
				cmdbIds.add(cmdbId);
				fullLastUpdatedDate.add(new Timestamp(date.getTime()));
				StrProp[] strprop = CI[j].getProps().getStrProps().getStrPropArray();
				LOGGER.info("strprop.length : " + strprop.length);

				if(strprop.length==4)
				{
					for (int p = 0; p < strprop.length; p++)
					{
						getName = strprop[p].getName();
						getValue = strprop[p].getValue();
						if(getName.equalsIgnoreCase("site_id")){
							siteId = getValue;
							siteIdsNew.add(siteId);
							//LOGGER.info("siteId : " + siteId);
						}
						if(getName.equalsIgnoreCase("site_parent_site_id")){
							parentSiteId = getValue;
							parentSiteIds.add(parentSiteId);
							//LOGGER.info("parentSiteId : " + parentSiteId);
						}
						if(getName.equalsIgnoreCase("site_city")){
							siteCity = getValue;
							siteCitys.add(siteCity);
							//LOGGER.info("siteCity : " + siteCity);
						}
						if(getName.equalsIgnoreCase("site_country")){
							siteCountry = getValue;
							siteCountrys.add(siteCountry);
							//LOGGER.info("siteCountry : " + siteCountry);
						}
					}
				}
				else if(strprop.length==3)
				{
					for (int p = 0; p < strprop.length; p++)
					{
						getName = strprop[p].getName();
						getValue = strprop[p].getValue();
						if(getName.equalsIgnoreCase("site_id")){
							siteId = getValue;
							siteIdsNew.add(siteId);
							//LOGGER.info("siteId : " + siteId);
						}
						if(getName.equalsIgnoreCase("site_country")){
							siteCountry = getValue;
							siteCountrys.add(siteCountry);
							//LOGGER.info("siteCountry : " + siteCountry);
						}
						if(getName.equalsIgnoreCase("site_parent_site_id")){
							parentSiteId = getValue;
							parentSiteIds.add(parentSiteId);
							siteCitys.add("");
							//LOGGER.info("parentSiteId : " + parentSiteId);
						}
						else if(getName.equalsIgnoreCase("site_city")){
						
							siteCity = getValue;
							siteCitys.add(siteCity);
							parentSiteIds.add("");
							//LOGGER.info("siteCity : " + siteCity);
						}
					}
				}
				else if(strprop.length==2)
				{
					for (int p = 0; p < strprop.length; p++)
					{
						getName = strprop[p].getName();
						getValue = strprop[p].getValue();
						if(getName.equalsIgnoreCase("site_id")){
							siteId = getValue;
							siteIdsNew.add(siteId);
							//LOGGER.info("siteId : " + siteId);
						}
						if(getName.equalsIgnoreCase("site_country")){
							siteCountry = getValue;
							siteCountrys.add(siteCountry);
							//LOGGER.info("siteCountry : " + siteCountry);
						}
					}
					parentSiteIds.add("");
					siteCitys.add("");
				}
			}
			LOGGER.info("siteIdsNewSize : " +siteIdsNew.size());
			LOGGER.info("parentSiteIdsSize : " +parentSiteIds.size());
			LOGGER.info("siteCitysSize : " +siteCitys.size());
			LOGGER.info("siteCountrysSize : " +siteCountrys.size());
			LOGGER.info("cmdbIdsSize : " +cmdbIds.size());
			LOGGER.info("siteIdsNew : " +siteIdsNew);
			LOGGER.info("parentSiteIds : " +parentSiteIds);
			LOGGER.info("siteCitys : " +siteCitys);
			LOGGER.info("siteCountrys : " +siteCountrys);
			LOGGER.info("cmdbIds : " +cmdbIds);
			LOGGER.info("SUCCESS FROM GET FILTERED");
			//insertIntoTable();
			}
			else
			{
				pullTopologyMapChunksFull();
			}
			
		
		}catch (RemoteException rme) {
			LOGGER.error("RemoteException inside getFilteredCIsByType", rme);
		} catch (UcmdbFault ufe) {
			LOGGER.error("UcmdbFaultException inside getFilteredCIsByType", ufe);
		} catch (Exception e) {
			 LOGGER.error("exception inside getFilteredCIsByTypeDemo", e);
		}

	}

	 // pullTopologyMapChunksFull Operation
		public static void pullTopologyMapChunksFull() {
			LOGGER.info("inside pullTopologyMapChunksFull method");

			try {
				
				LOGGER.info("numberOfChunks : " + numberOfChunks);
				//LOGGER.info("key1 : " + key1);
				//LOGGER.info("key2 : " + key2);
				for(int i=1 ; i <= numberOfChunks ; i++)
				{
					chunkNumber = i;
					//LOGGER.info("chunkNumber:" +chunkNumber);

					CmdbContext cntxt = CmdbContext.Factory.newInstance();
					cntxt.setCallerApplication(callerApplication);
					ChunkRequest chunkRequest = ChunkRequest.Factory.newInstance();
					ChunkInfo chunkInfo = ChunkInfo.Factory.newInstance();
					
					chunkRequest.setChunkNumber(chunkNumber);
					chunkInfo.setNumberOfChunks(numberOfChunks);

					ChunkKey chunksKey = ChunkKey.Factory.newInstance();
					chunksKey.setKey1(key1);
					chunksKey.setKey2(key2);
					
					chunkInfo.setChunksKey(chunksKey);
					chunkRequest.setChunkInfo(chunkInfo );
					
					
					TypedPropertiesCollection queryTypedProperties = TypedPropertiesCollection.Factory.newInstance();
					TypedProperties typedProperties = TypedProperties.Factory.newInstance();
					typedProperties.setType(className5);
					CustomTypedProperties properties = CustomTypedProperties.Factory.newInstance();
					PropertiesList propertiesList = PropertiesList.Factory.newInstance();
					String[] propertyNameArray = {conditionName5,propertyName5,propertyName6,propertyName7};
					propertiesList.setPropertyNameArray(propertyNameArray );

					properties.setPropertiesList(propertiesList);
					typedProperties.setProperties(properties);
					
					TypedProperties[] typedPropertiesArray ={typedProperties};
					queryTypedProperties.setTypedPropertiesArray(typedPropertiesArray );
					
					PullTopologyMapChunks pullTopologyMapChunks = PullTopologyMapChunks.Factory.newInstance();
					
					pullTopologyMapChunks.setCmdbContext(cntxt);
					pullTopologyMapChunks.setChunkRequest(chunkRequest);
					pullTopologyMapChunks.setQueryTypedProperties(queryTypedProperties);
					PullTopologyMapChunksDocument request = PullTopologyMapChunksDocument.Factory.newInstance();
					request.setPullTopologyMapChunks(pullTopologyMapChunks);
					//LOGGER.info("pullTopologyMapChunks request" +request);					
					
					HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
					basicAuthentication.setUsername("admin");
					basicAuthentication.setPassword("admin");
					int so_timeout = 480000;
					int con_timeout = 60000;
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
					//LOGGER.info("authentication passed");

					PullTopologyMapChunksResponseDocument response = stub.pullTopologyMapChunks(request);
					//LOGGER.info("pullTopologyMapChunks response : " + response);
					
					
					CINode[] ciNode = response.getPullTopologyMapChunksResponse().getTopologyMap().getCINodes().getCINodeArray();
					CI[] ci = ciNode[0].getCIs().getCIArray();
					//LOGGER.info("ci : " + ci);
					//LOGGER.info(ci.length);
					java.util.Date date= new java.util.Date();
					for(int j=0 ; j < ci.length ; j++)
					{						
						cmdbId = ci[j].getID().getStringValue();							
						cmdbIds.add(cmdbId);
						fullLastUpdatedDate.add(new Timestamp(date.getTime()));
							
						StrProp[] strprop = ci[j].getProps().getStrProps().getStrPropArray();
						LOGGER.info("strprop.length : " + strprop.length);
						if(strprop.length==4)
						{
							for (int p = 0; p < strprop.length; p++)
							{
								getName = strprop[p].getName();
								getValue = strprop[p].getValue();
								if(getName.equalsIgnoreCase("site_id")){
									siteId = getValue;
									siteIdsNew.add(siteId);
									//LOGGER.info("siteId : " + siteId);
								}
								if(getName.equalsIgnoreCase("site_parent_site_id")){
									parentSiteId = getValue;
									parentSiteIds.add(parentSiteId);
									//LOGGER.info("parentSiteId : " + parentSiteId);
								}
								if(getName.equalsIgnoreCase("site_city")){
									siteCity = getValue;
									siteCitys.add(siteCity);
									//LOGGER.info("siteCity : " + siteCity);
								}
								if(getName.equalsIgnoreCase("site_country")){
									siteCountry = getValue;
									siteCountrys.add(siteCountry);
									//LOGGER.info("siteCountry : " + siteCountry);
								}
							}
						}
						else if(strprop.length==3)
						{
							for (int p = 0; p < strprop.length; p++)
							{
								getName = strprop[p].getName();
								getValue = strprop[p].getValue();
								if(getName.equalsIgnoreCase("site_id")){
									siteId = getValue;
									siteIdsNew.add(siteId);
									//LOGGER.info("siteId : " + siteId);
								}
								if(getName.equalsIgnoreCase("site_country")){
									siteCountry = getValue;
									siteCountrys.add(siteCountry);
									//LOGGER.info("siteCountry : " + siteCountry);
								}
								if(getName.equalsIgnoreCase("site_parent_site_id")){
									parentSiteId = getValue;
									parentSiteIds.add(parentSiteId);
									siteCitys.add("");
									//LOGGER.info("parentSiteId : " + parentSiteId);
								}
								else if(getName.equalsIgnoreCase("site_city")){
								
									siteCity = getValue;
									siteCitys.add(siteCity);
									parentSiteIds.add("");
									//LOGGER.info("siteCity : " + siteCity);
								}
							}
						}
						else if(strprop.length==2)
						{
							for (int p = 0; p < strprop.length; p++)
							{
								getName = strprop[p].getName();
								getValue = strprop[p].getValue();
								if(getName.equalsIgnoreCase("site_id")){
									siteId = getValue;
									siteIdsNew.add(siteId);
									//LOGGER.info("siteId : " + siteId);
								}
								if(getName.equalsIgnoreCase("site_country")){
									siteCountry = getValue;
									siteCountrys.add(siteCountry);
									//LOGGER.info("siteCountry : " + siteCountry);
								}
							}
							parentSiteIds.add("");
							siteCitys.add("");
						}
						
					}


				}
			//LOGGER.info("OUTSIDE CHUNK LOOP");
				LOGGER.info("siteIdsNewSize : " +siteIdsNew.size());
				LOGGER.info("parentSiteIdsSize : " +parentSiteIds.size());
				LOGGER.info("siteCitysSize : " +siteCitys.size());
				LOGGER.info("siteCountrysSize : " +siteCountrys.size());
				LOGGER.info("cmdbIdsSize : " +cmdbIds.size());
				LOGGER.info("siteIdsNew : " +siteIdsNew);
				LOGGER.info("parentSiteIds : " +parentSiteIds);
				LOGGER.info("siteCitys : " +siteCitys);
				LOGGER.info("siteCountrys : " +siteCountrys);
				LOGGER.info("cmdbIds : " +cmdbIds);
				LOGGER.info("SUCCESS FROM PULL CHUNKS");
			//insertIntoTable();
			}
			catch (RemoteException rme) {
				LOGGER.error("RemoteException inside pullTopologyMapChunks", rme);
			} catch (UcmdbFault ufe) {
				LOGGER.error("UcmdbFaultException inside pullTopologyMapChunks", ufe);
			}			
			 catch (Exception e) {
				 LOGGER.error("exception inside pullTopologyMapChunksFull", e);
			}

		}
	
	private static void insertIntoTable() {
		//Inserting Values into CDM_WOL_RSRC_1_STG TABLE
		try {
			
			LOGGER.info("Inserting");
			LOGGER.info("siteIdsNewSize : " +siteIdsNew.size());
			LOGGER.info("parentSiteIdsSize : " +parentSiteIds.size());
			LOGGER.info("siteCitysSize : " +siteCitys.size());
			LOGGER.info("siteCountrysSize : " +siteCountrys.size());
			LOGGER.info("cmdbIdsSize : " +cmdbIds.size());
			
			dbConnection = getDBConnection();
			String stringSQL = "INSERT INTO CDM_TI_SITE_1_STG (CMDB_ID,A_SITE_ID,A_SITE_PARENT_SITE_ID,A_SITE_CITY,A_SITE_COUNTRY,TIMESTAMP) VALUES (?,?,?,?,?,?)" ;
			PreparedStatement ps = dbConnection.prepareStatement(stringSQL);
			
			for (int k = 0; k < parentSiteIds.size(); k++){
				ps.setString(1, cmdbIds.get(k));
				ps.setString(2, siteIdsNew.get(k));
				ps.setString(3, parentSiteIds.get(k));
				ps.setString(4, siteCitys.get(k));
				ps.setString(5, siteCountrys.get(k));
				ps.setTimestamp(6, fullLastUpdatedDate.get(k));									
				ps.addBatch();
			}		
			//LOGGER.info("completed batch execution");
			ps.executeBatch();
			ps.close();
			dbConnection.close();
			LOGGER.info("connection closed");
			updateRecordInSyncStatus();
		} 
		catch (SQLException sqle) {
			//LOGGER.info(sqle.getMessage());
			LOGGER.error("SQL exception inside insertIntoTable", sqle);
		}
		catch (Exception e) {
			LOGGER.error("exception inside insertIntoTable", e);
		}	
		
	}

	private static void updateRecordInSyncStatus() {
  		LOGGER.info("inside updateRecordInSyncStatus method");
  	//Correct is COMPLETED.USING THIS FOR TESTING
  		syncStatus = "COMPLETED";
  		//syncStatus = "COMPLETE";
  		//not required to call as trigger created on sync_status table
  		//CallableStatement cstmt = null;
  		if(oprtnStatus.equalsIgnoreCase("N"))
  		{
  			syncStatus = "ERROR";
  		}
  		LOGGER.info("syncStatus : " + syncStatus);
  		LOGGER.info("syncIdAfter : " + syncId);
  		LOGGER.info("UPDATING");
  		String insertSQL = "INSERT INTO SYNC_STATUS " ;
  		insertSQL = insertSQL + "(SYNC_ID,SYNC_START,SYNC_END,SYNC_STATUS,SYNC_TYPE) VALUES ";
  		insertSQL = insertSQL +  "(SYNC_ID.nextval," + "to_date('"+ sync_start_date + "', 'yyyy/mm/dd hh24:mi:ss'), " + "to_date('"+ getCurrentTimeStamp() + "', 'yyyy/mm/dd hh24:mi:ss'),'"+syncStatus+"','"+syncType+"'  )" ;

			try {
				
				dbConnection = getDBConnection();
				statement = dbConnection.createStatement();

				LOGGER.info(insertSQL);
				statement.executeUpdate(insertSQL);
				LOGGER.info("Record inserted in sync status!");
				
			} catch (SQLException sqle) {
				LOGGER.error("SQLException inside updateRecordInSyncStatus", sqle);
			} catch (Exception e) {
				/*oprtnStatus = "N";
				updateRecordInSyncStatus();*/
				LOGGER.error("exception inside updateRecordInSyncStatus", e);
			}
		}
	
	private static String getCurrentTimeStamp() {
		java.util.Date today = new java.util.Date();
		return dateFormat.format(today.getTime());
	}
	
	public static void loadParams(String configPath) {
	    InputStream is = null;
	 
	    try {
	    	LOGGER.info("configPath : " +configPath);
	    	File f = new File (configPath);
	    	//File f = new File("E:\\Ashwini\\workspaceAhana\\ucmdbSrv\\src\\config.txt");
	    	//File f = new File("E:\\Ahana\\Workspace5\\Ucmdb5\\src\\config.txt");
	        is = new FileInputStream( f );
	    }
	    catch ( Exception e ) { is = null; }
	 
	    try {
	        props.load( is );
	    }
	    catch ( Exception e ) {
	    	LOGGER.error("exception inside loadParams", e);
	    }

	   
	}


	private static Connection getDBConnection() {
		//Using config file to remove hardcoding
		String DB_DRIVER = props.getProperty("DB_DRIVER");
		String DB_CONNECTION = props.getProperty("DB_CONNECTION");
		String DB_USER = props.getProperty("DB_USER");
		String DB_PASSWORD = props.getProperty("DB_PASSWORD");
		try {			
			Class.forName(DB_DRIVER);
			
		} catch (ClassNotFoundException e) {
			LOGGER.info(e.getMessage());
		}		
		
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
			return dbConnection;		
		
		} catch (SQLException e) {
			LOGGER.info(e.getMessage());
		}
		return dbConnection;
	}

}

