package com.telstra.netops.lookingglass.server.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pop_country")
public class Country implements Serializable {
	
	@Id
	@Column(name = "Pop_Country_ID")
	private String id;
	
	@Column(name = "Pop_Country")
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
