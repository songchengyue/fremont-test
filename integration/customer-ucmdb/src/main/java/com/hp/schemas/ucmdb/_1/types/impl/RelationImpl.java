/*
 * XML Type:  Relation
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.Relation
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML Relation(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class RelationImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.Relation
{
    
    public RelationImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ID$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "ID");
    private static final javax.xml.namespace.QName TYPE$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "type");
    private static final javax.xml.namespace.QName END1ID$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "end1ID");
    private static final javax.xml.namespace.QName END2ID$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "end2ID");
    private static final javax.xml.namespace.QName PROPS$8 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "props");
    
    
    /**
     * Gets the "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID getID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ID" element
     */
    public void setID(com.hp.schemas.ucmdb._1.types.ID id)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ID$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ID$0);
            }
            target.set(id);
        }
    }
    
    /**
     * Appends and returns a new empty "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID addNewID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ID$0);
            return target;
        }
    }
    
    /**
     * Gets the "type" element
     */
    public java.lang.String getType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "type" element
     */
    public org.apache.xmlbeans.XmlString xgetType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "type" element
     */
    public void setType(java.lang.String type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TYPE$2);
            }
            target.setStringValue(type);
        }
    }
    
    /**
     * Sets (as xml) the "type" element
     */
    public void xsetType(org.apache.xmlbeans.XmlString type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TYPE$2);
            }
            target.set(type);
        }
    }
    
    /**
     * Gets the "end1ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID getEnd1ID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(END1ID$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "end1ID" element
     */
    public void setEnd1ID(com.hp.schemas.ucmdb._1.types.ID end1ID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(END1ID$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(END1ID$4);
            }
            target.set(end1ID);
        }
    }
    
    /**
     * Appends and returns a new empty "end1ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID addNewEnd1ID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(END1ID$4);
            return target;
        }
    }
    
    /**
     * Gets the "end2ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID getEnd2ID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(END2ID$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "end2ID" element
     */
    public void setEnd2ID(com.hp.schemas.ucmdb._1.types.ID end2ID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(END2ID$6, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(END2ID$6);
            }
            target.set(end2ID);
        }
    }
    
    /**
     * Appends and returns a new empty "end2ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID addNewEnd2ID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(END2ID$6);
            return target;
        }
    }
    
    /**
     * Gets the "props" element
     */
    public com.hp.schemas.ucmdb._1.types.CIProperties getProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().find_element_user(PROPS$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "props" element
     */
    public boolean isSetProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PROPS$8) != 0;
        }
    }
    
    /**
     * Sets the "props" element
     */
    public void setProps(com.hp.schemas.ucmdb._1.types.CIProperties props)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().find_element_user(PROPS$8, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().add_element_user(PROPS$8);
            }
            target.set(props);
        }
    }
    
    /**
     * Appends and returns a new empty "props" element
     */
    public com.hp.schemas.ucmdb._1.types.CIProperties addNewProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().add_element_user(PROPS$8);
            return target;
        }
    }
    
    /**
     * Unsets the "props" element
     */
    public void unsetProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PROPS$8, 0);
        }
    }
}
