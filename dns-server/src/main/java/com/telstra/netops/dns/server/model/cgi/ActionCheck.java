package com.telstra.netops.dns.server.model.cgi;

public enum ActionCheck {

	SUBMIT(1), //
	VIEW(2), //
	CANCEL(3),
	NO_VALUE(0),
	REV_MAPPING(10),
	REV_DELEGATION(11);
	
	private final Integer value;
	
	private ActionCheck(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}
	
	public static ActionCheck findForValue(Integer value) {
		for (ActionCheck ipVersion: ActionCheck.values()) {
			if (value.equals(ipVersion.getValue())) {
				return ipVersion;
			}
		}
		
		throw new IllegalArgumentException("not such ActionCheck for value " + value);
	}

}
