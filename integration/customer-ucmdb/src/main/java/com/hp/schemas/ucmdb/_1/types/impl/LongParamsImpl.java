/*
 * XML Type:  LongParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.LongParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML LongParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class LongParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.LongParams
{
    
    public LongParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LONGPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "longParam");
    
    
    /**
     * Gets array of all "longParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.LongProp[] getLongParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(LONGPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.LongProp[] result = new com.hp.schemas.ucmdb._1.types.LongProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "longParam" element
     */
    public com.hp.schemas.ucmdb._1.types.LongProp getLongParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().find_element_user(LONGPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "longParam" element
     */
    public int sizeOfLongParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(LONGPARAM$0);
        }
    }
    
    /**
     * Sets array of all "longParam" element
     */
    public void setLongParamArray(com.hp.schemas.ucmdb._1.types.LongProp[] longParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(longParamArray, LONGPARAM$0);
        }
    }
    
    /**
     * Sets ith "longParam" element
     */
    public void setLongParamArray(int i, com.hp.schemas.ucmdb._1.types.LongProp longParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().find_element_user(LONGPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(longParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "longParam" element
     */
    public com.hp.schemas.ucmdb._1.types.LongProp insertNewLongParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().insert_element_user(LONGPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "longParam" element
     */
    public com.hp.schemas.ucmdb._1.types.LongProp addNewLongParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().add_element_user(LONGPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "longParam" element
     */
    public void removeLongParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(LONGPARAM$0, i);
        }
    }
}
