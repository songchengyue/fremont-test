/*
 * XML Type:  IPList
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/types
 * Java type: com.hp.schemas.ucmdb.discovery._1.types.IPList
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.types.impl;
/**
 * An XML IPList(@http://schemas.hp.com/ucmdb/discovery/1/types).
 *
 * This is a complex type.
 */
public class IPListImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.types.IPList
{
    
    public IPListImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/types", "IP");
    
    
    /**
     * Gets array of all "IP" elements
     */
    public com.hp.schemas.ucmdb.discovery._1.types.IP[] getIPArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IP$0, targetList);
            com.hp.schemas.ucmdb.discovery._1.types.IP[] result = new com.hp.schemas.ucmdb.discovery._1.types.IP[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "IP" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.IP getIPArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IP target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().find_element_user(IP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "IP" element
     */
    public int sizeOfIPArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IP$0);
        }
    }
    
    /**
     * Sets array of all "IP" element
     */
    public void setIPArray(com.hp.schemas.ucmdb.discovery._1.types.IP[] ipArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(ipArray, IP$0);
        }
    }
    
    /**
     * Sets ith "IP" element
     */
    public void setIPArray(int i, com.hp.schemas.ucmdb.discovery._1.types.IP ip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IP target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().find_element_user(IP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(ip);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "IP" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.IP insertNewIP(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IP target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().insert_element_user(IP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "IP" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.IP addNewIP()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IP target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IP)get_store().add_element_user(IP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "IP" element
     */
    public void removeIP(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IP$0, i);
        }
    }
}
