# Date: 14 November 2001
# File: CMS_CUSTOMER_DB.pm
#
# $Id: CMS_CUSTOMER_DB.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $
#
###################################################################
# File        : CMS_CUSTOMER_DB.pm
# Description : All functions which accesses the DB		
# Output of the script
# Input File  : 
# Output File :
# Author      : PRADEEP THANRAJ (Infosys Tech. Ltd., Bangalore )
# Started On  : 07 July 2009
# Modification History :
#  Date               Name             Change/Description
# ------------------------------------------------------------------
# 07Jul2009    	     Pradeep			Baselined for EVPL
# 07Jul2009    	     Pradeep			changes for EVPL
# 07Feb2009    	     Chandini			Baselined for EPL
# 13Sep2010    	     Chandini			Baselined for IP-Transit
####################################################################

package CMS_CUSTOMER_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;

our $error;
my $DEFAULT_PRODUCT_CODE = 'GIA';
my $EPORTAL_PASSWORD = 'RePorta!198';

sub get_contract_status {
	# input: accno (customer)
	# output: ref to hash containing contract_ended attribute in customer table
	my $accno = shift @_;
	my $sth = $dbh->prepare("SELECT contract_ended FROM customer
				where accno = ? ");
	$sth->execute($accno);
	my $result = $sth->fetchrow_arrayref;

	$sth->finish;

	return $result;
}


sub make_customer_into_wholesaler {
  my $accno = $dbh->quote(shift @_);
  my $template_path = $dbh->quote(shift @_);
  my $email = $dbh->quote(shift @_);
  my $contactinfo = $dbh->quote(shift @_);
  my $logoname = $dbh->quote(shift @_);
 
  my $sth = $dbh->prepare
    ("INSERT INTO wholesaler (accno, template_path, email, logoname, contactinfo) 
           VALUES ($accno, $template_path, $email, $logoname, $contactinfo)");
 
  my $result = $sth->execute;
  $error = $dbh->errstr; 
  return $result;


}

sub update_wholesaler_path {
  my $accno = $dbh->quote(shift @_);
  my $template_path = $dbh->quote(shift @_);
  my $email = $dbh->quote(shift @_);
  my $contactinfo = $dbh->quote(shift @_);
  my $logoname = $dbh->quote(shift @_);

  my $sth = $dbh->prepare("UPDATE wholesaler
                              SET template_path = $template_path,
                                  email = $email,
				  logoname = $logoname,
				  contactinfo = $contactinfo
                            WHERE accno = $accno");

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;
}

sub get_wholesalers {
  my $sth = $dbh->prepare ("SELECT accno FROM wholesaler ORDER BY accno");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;

  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }

}

sub get_slareportcode {
	my $sth = $dbh->prepare("SELECT slareportcodeid, slareportcode from slareportcode order by sort DESC");

	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

	my $result = $sth->fetchall_arrayref;

	if ($result) {
		$error = $dbh->errstr;
		$sth->finish;
		return $result;
	} else {
		$error = $dbh->errstr;
		return 0;
	}
}

sub get_accountattribute {
	my $sth = $dbh->prepare("SELECT accountattributeid, attributename FROM accountattribute");

	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

	my $result = $sth->fetchall_arrayref;
	if ($result) {
		$error = $dbh->errstr;
		$sth->finish;
		return $result;
	} else {
		$error = $dbh->errstr;
		return 0;
	}
}

sub get_mrwlink {
	my $sth = $dbh->prepare("SELECT accno FROM customer
				WHERE accountattributeid = 'MRW'
				ORDER BY accno");
	unless ($sth->execute) {
		$error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


sub get_wholesalers_and_paths {
 my $sth = $dbh->prepare ("SELECT accno, template_path, email,
				logoname, contactinfo
				 FROM wholesaler");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;

  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub verify_encrypted_password {
  my $accno = $dbh->quote(shift @_);
  my $password = shift @_;

#  my $sth = $dbh->prepare
#    ("SELECT password
#      FROM customer
#      WHERE accno = $accno");

   my $sth = $dbh->prepare
    ("SELECT password
      FROM customer
      WHERE accno = $accno AND ep_access = 't'");

  if (my $result = $sth->execute) {
    my $dbpassword = ($sth->fetchrow_array())[0];

    if( ($dbpassword) && ($password) ) {

    	if ($dbpassword =~ /^$password$/) {
      		return $dbpassword;
    	} else {
      		$error = "Incorrect username and password.";
      		return 0;
    	}
    } else {
	$error = "Incorrect username and password.";
	return 0;
    }
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub verify_plaintext_password {
  my $accno = $dbh->quote(shift @_);
  my $password = shift @_;
  $password = crypt $password, $password;

#  my $sth = $dbh->prepare
#    ("SELECT password
#      FROM customer
#      WHERE accno = $accno");

   my $sth = $dbh->prepare
    ("SELECT password
      FROM customer
      WHERE accno = $accno AND ep_access = 't'");
 
  if (my $result = $sth->execute) {
    my $dbpassword = ($sth->fetchrow_array())[0];

    if ($dbpassword eq $password) {
      return $dbpassword;
    } else {
      $error = "Incorrect username and password.";
      return 0;
    }
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub change_password {
  my $accno = shift @_; # don't adjust yet as passing ontelnet t
  my $oldpassword = shift @_; # don't adjust as just passing on
  my $password = shift @_;

  # NEW REQUIREMENT : If customer is ePortal enabled, password is fixed and
  #                   cannot be changed
  if(access_through_EP($accno) eq 'TRUE'){
    if($password ne $EPORTAL_PASSWORD){
      $error = 'Invalid password for ePortal customer';
      return 0;
    }
  }
  
  $password = $dbh->quote(crypt ($password, $password));

  if (not verify_plaintext_password($accno, $oldpassword)) {
    return 0;
  }

  $accno = $dbh->quote($accno);
  

  my $sth = $dbh->prepare
    ("UPDATE customer SET password = $password
      WHERE accno = $accno");

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;
}

sub is_wholesaler {
  my $accno = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT count(*) 
      FROM wholesaler
      WHERE accno = $accno");

  if (my $result = $sth->execute) {
    return ($sth->fetchrow_array())[0];
  } else {
    $error = $dbh->errstr;
    return $result;
  }
}

sub get_template_path {
  my $accno = $dbh->quote(shift @_);
  my $sth = $dbh->prepare
    ("SELECT template_path
      FROM wholesaler
      WHERE accno = (SELECT wholesaler_accno
                     FROM customer
                     WHERE accno = $accno)");


  if ($sth->execute) {
    if (my @result = $sth->fetchrow_array()) {
      return $result[0];
    } else {
      $error = "Invalid account number.\n";
      return 0;
    }
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub get_wholesaler_logo {
	my $accno = shift @_;
	my $sth = $dbh->prepare("SELECT logoname FROM wholesaler
				WHERE accno = (SELECT wholesaler_accno
						FROM customer
						WHERE accno = ?)");
	if($sth->execute($accno)) {
		if (my @result = $sth->fetchrow_array()) {
			return $result[0];
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

sub get_wholesaler_email {
  my $accno = $dbh->quote(shift @_);
#  my $sth = $dbh->prepare
#    ("SELECT email
#      FROM wholesaler
#      WHERE accno = $accno");
  my $sth = $dbh->prepare
    ("SELECT email
      FROM wholesaler
      WHERE accno = (SELECT wholesaler_accno
                     FROM customer
                     WHERE accno = $accno)");

  if ($sth->execute) {
    if (my @result = $sth->fetchrow_array()) {
      return $result[0];
    } else {
      $error = "Invalid account number.\n";
      return 0;
    }
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub get_wholesaler_contactinfo {
	my $accno = $dbh->quote(shift @_);
  	my $sth = $dbh->prepare
    		("SELECT contactinfo
      		FROM wholesaler
      		WHERE accno = (SELECT wholesaler_accno
       		              FROM customer
       		              WHERE accno = $accno)");
	if ($sth->execute) {
    		if (my @result = $sth->fetchrow_array()) {
			$result[0] =~ s/\n/\<BR\>/g;
      			return $result[0];
    		} else {
      			$error = "Cannot locate contact information for $accno.\n";
      			return $error;
    		}
  	} else {
    		$error = $dbh->errstr;
    		return $error;
  	}
}

sub add_customer {

  my $accno = $dbh->quote(shift @_);
  my $fnn = $dbh->quote(shift @_);
  my $company_name = $dbh->quote(shift @_);
  my $accattr = $dbh->quote(shift @_);
  my $wholesaler_accno = $dbh->quote(shift @_);
  my $mrw_accno = $dbh->quote(shift @_);
  my $slareportcodeid = $dbh->quote(shift @_);
  my $epaccess = $dbh->quote(shift @_);
  my $password = shift @_ || "CUSTOMER";

  # NEW REQUIREMENT : If customer is ePortal enabled, password is fixed and
  #                   cannot be changed
  # Libauer:6/12/03
  if($epaccess =~ /t|1/i){
    $password = $EPORTAL_PASSWORD;
  }

  $password = $dbh->quote(crypt($password, $password));
  my $billing_optionid = 1; 

  $fnn =~ tr/A-Z/a-z/;
  $company_name =~ tr/A-Z/a-z/;

  # if account = RO - need to add mrw_accno
  my $roattr = ($accattr =~ /^RO$/) ? ", mrw_accno" : "";

  my $sth;

  if($accattr =~ /^\'RO\'$/) {	
	$sth = $dbh->prepare
    	("INSERT INTO customer (accno, fnn, company_name, password,
                            billing_optionid, wholesaler_accno,
			    accountattributeid, mrw_accno, slareportcodeid, ep_access)
	                  VALUES ($accno, $fnn, $company_name, $password,
                            $billing_optionid, $wholesaler_accno, $accattr,
			    $mrw_accno, $slareportcodeid, $epaccess)");
  } else {
	$sth = $dbh->prepare
        ("INSERT INTO customer (accno, fnn, company_name, password,
                            billing_optionid, wholesaler_accno,
                            accountattributeid, slareportcodeid, ep_access)
                          VALUES ($accno, $fnn, $company_name, $password,  
                            $billing_optionid, $wholesaler_accno, $accattr, 
                            $slareportcodeid, $epaccess)");
  }

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;

}

sub update_customer {
  my $accno = $dbh->quote(shift @_);
  my $fnn = $dbh->quote(shift @_);
  my $company_name = $dbh->quote(shift @_);
  my $billing_optionid = shift @_; # not used yet
  my $wholesaler_accno = $dbh->quote(shift @_);
  my $reset_password = shift @_;
  my $attrid = $dbh->quote(shift @_);
  my $slareportcodeid = $dbh->quote(shift @_);
  my $epaccess = $dbh->quote(shift @_);

  my $command;
  my $sth;

  $sth = $dbh->prepare ("SELECT ep_access, contract_ended FROM customer WHERE accno = $accno");
  $sth->execute();
  my $r = $sth->fetchrow_hashref;

  if (($epaccess =~ /t|1/i) || (($epaccess !~ /t|1/i) && ($$r{ep_access} =~ /t|1/i)) || ($reset_password)) {
    my $password;
    my $contract_ended = $$r{contract_ended};

    # NEW REQUIREMENT : If customer is ePortal enabled, password is fixed and
    #                   cannot be changed
    # Libauer:6/12/03

    if($epaccess =~ /t|1/i){
      $password = $EPORTAL_PASSWORD;
    } else {
      $password = 'CUSTOMER';
    }
  
    if ($reset_password eq "terminate") {
        $password = 'TERMINATED';
        $contract_ended = 't';
    } elsif ($reset_password eq "reactivate") {
        # NEW REQUIREMENT : If customer is ePortal enabled, password is fixed and
        #                   cannot be changed
        # Libauer:6/12/03
	if($epaccess =~ /t|1/i){
	      $password = $EPORTAL_PASSWORD;
    	} else {
	      $password = 'CUSTOMER';
	}
        $contract_ended = '';
    }
    
    $password = $dbh->quote(crypt ($password,$password));
    $contract_ended = $dbh->quote($contract_ended);

    $sth= $dbh->prepare
      ("UPDATE customer 
         SET fnn = $fnn,
             company_name = $company_name,
             wholesaler_accno = $wholesaler_accno,
             password = $password,
	     accountattributeid = $attrid,
	     slareportcodeid = $slareportcodeid,
             ep_access = $epaccess,
	     contract_ended = $contract_ended
       WHERE accno = $accno");
  } else {
     $sth= $dbh->prepare
      ("UPDATE customer 
         SET fnn = $fnn,
             company_name = $company_name,
             wholesaler_accno = $wholesaler_accno,
	     accountattributeid = $attrid,
	     slareportcodeid = $slareportcodeid,
             ep_access = $epaccess
       WHERE accno = $accno");
  }
  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;

}

sub delete_customer {
  my $accno_raw = shift @_;
  my $accno = $dbh->quote($accno_raw);
   my $sth1 = $dbh->prepare("select count(*) from service where accno = $accno");
   unless ($sth1->execute) {
    $error = $dbh->errstr;
  }
  my $count= $sth1->fetchall_arrayref;
  my $count1=$$count[0];
  if ($$count1[0])
  {
  return 0;
  }
  else 
  {
  my $sth = $dbh->prepare
    ("DELETE FROM customer WHERE accno = $accno");

  my $result = $sth->execute;
  $error = $dbh->errstr;

  CMS_OLSS_CUSTACC_DB::delete_access_acc($accno_raw);

  return $result;
  }
}

sub get_customer {
  my $accno = $dbh->quote(shift @_);
  
  my $sth = $dbh->prepare
    ("SELECT accno, fnn, company_name, 
             billing_optionid, wholesaler_accno, accountattributeid, mrw_accno, 
             slareportcodeid, ep_access, contract_ended
      FROM customer
      WHERE accno = $accno");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchrow_arrayref;

  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }

}

sub search_customer {
  my $accno = shift @_;
  my $fnn = shift @_;
  my $company = shift @_;
  my $wholesaler = shift @_;

  my $command = "SELECT accno, fnn, company_name, wholesaler_accno,
		 accountattributeid, mrw_accno, slareportcode
                 FROM customer, slareportcode ";

 
  if ($accno ne "") {
    $command .= "AND accno ilike '%$accno%' "; 
  }
  if ($fnn ne "") { 
    $command .= "AND fnn ilike '%$fnn%' "; 
  }
  if ($company ne "") { 
    $command .= "AND company_name ilike '%$company%' "; 
  }
  if ($wholesaler ne "") { 
    $command .= "AND wholesaler_accno ilike '%$wholesaler%' "; 
  }
  $command .= "AND customer.slareportcodeid=slareportcode.slareportcodeid ";

  $command =~ s/AND/WHERE/; # change first AND
  $command .= "ORDER by accno";

  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }

}


sub add_billing_options {
  my $billing_optionid = shift @_;
  my $billing_option = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("INSERT INTO billing_option (billing_optionid, billing_option)
                        VALUES ($billing_optionid, $billing_option)");

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;

  }

## FOR OLSS

sub get_maxbw_for_service {
  # inputs: serviceid
  # outputs: bandwidth 

  my $serviceid = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT SUM(bw) 
      FROM link
      WHERE serviceid = $serviceid;");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my @result = $sth->fetchrow_array;
  
  $error = $dbh->errstr;
  $sth->finish;
  return $result[0];
  
}

sub get_currbw_for_service {
  # inputs: serviceid
  # output: bandwidth | undef

  my $serviceid = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT currentbw 
      FROM service
      WHERE serviceid = $serviceid;");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my @result = $sth->fetchrow_array;
  
  $error = $dbh->errstr;
  $sth->finish;
  return $result[0];
  
}

sub set_currbw_for_service {
  # inputs: serviceid, bandwidth
  # output: 1 on success, 0 on failure

  my $serviceid = $dbh->quote(shift @_);
  my $bw = shift @_;
  
  my $sth = $dbh->prepare
    ("UPDATE service
      SET currentbw = $bw
      WHERE serviceid = $serviceid;");

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;
  
}

# Get MRW accno if RO account
sub get_mrwaccno_for_ro {
	#imputs: ro accno
	#output: ref to has containing mrwaccno (assume 1)

	my $accno = shift @_;
	my $sth = $dbh->prepare("SELECT mrw_accno FROM customer
				where accno = ?");
	unless($sth->execute($accno)) {
		$error = $dbh->errstr;
		return 0;
	}

	my $result = $sth->fetchrow_hashref;

	$error = $dbh->errstr;
	$sth->finish;
	return $result;
}

# Is customer MRW or RO?
sub get_account_attribute {
	# input: accno
	# output: ref to hash containing accountattribute
	my $accno = shift @_;
	my $sth = $dbh->prepare("SELECT accountattributeid FROM customer
				where accno = ? ");
	$sth->execute($accno);
	my $result = $sth->fetchrow_hashref;

	$sth->finish;

	return $result;
}

#Chandini_EPL (directly useing this without any modification)
#Chandini_Admin_Search
#Query to list the EPL Services
sub get_serviceid_for_accno {
  # inputs: accno and productcode (default to 'GIA' if no productcode supplied)
  # output: reference to 2D array containing [serviceid, as_no, bgp_neighbor_ip, svclabel, serviceid_alt]

  my $accno = shift @_;
  my $product_code = shift @_;
  my $search_label = shift @_;

  #$product_code = $DEFAULT_PRODUCT_CODE unless $product_code;
  $product_code = "%" unless $product_code;
  
  open (DEBUG_SD, ">/data1/tmp_log/get_serviceid_for_accno.txt");
 
  # First check if RO account
  my $ro = get_account_attribute($accno);

  #print DEBUG_SD "Accno = $accno;PROD Code = $product_code; RO = $ro.\n";

  if($$ro{accountattributeid} =~ /^RO$/) { # RO Account
  	my $mrwaccno = get_mrwaccno_for_ro($accno);
  	$accno = $$mrwaccno{mrw_accno};
  }

#  my $sth = $dbh->prepare
#    ("SELECT serviceid, as_no, bgp_neighbor_ip, svclabel, serviceid_alt
#      FROM service
#      WHERE accno ilike '%$accno%' AND productcodeid ilike '%$product_code%'
#      ORDER BY as_no DESC, serviceid ASC");

##++ EVPL - SD	
my $sth;
  if ($product_code =~ /EVPL/i) {
	        $sth = $dbh->prepare
		("SELECT trim(trailing 'a' from serviceid), as_no, bgp_neighbor_ip, svclabel, serviceid_alt
		FROM service
		WHERE accno ilike '$accno' AND productcodeid ilike 'EVPL' AND serviceid NOT like '%z'
		ORDER BY as_no DESC, serviceid ASC");	 	 
	
  } elsif ($product_code =~ /VPLS/i) {
		$sth = $dbh->prepare
		("SELECT serviceid, as_no, bgp_neighbor_ip, svclabel, serviceid_alt
		FROM service
		WHERE accno ilike '$accno' AND productcodeid ilike '$product_code' AND serviceid NOT like '%sw' 
		ORDER BY as_no DESC, serviceid ASC");
  } else {
#Chandini_Admin_Search
	if($search_label){
		$search_label = "%$search_label%";
		$sth = $dbh->prepare
                ("SELECT serviceid, as_no, bgp_neighbor_ip, svclabel, serviceid_alt
                FROM service
                WHERE accno ilike '$accno' AND productcodeid ilike '$product_code' AND serviceid ilike '$search_label'
                ORDER BY as_no DESC, serviceid ASC");	
	}else {
		$sth = $dbh->prepare
                ("SELECT serviceid, as_no, bgp_neighbor_ip, svclabel, serviceid_alt
                FROM service
                WHERE accno ilike '$accno' AND productcodeid ilike '$product_code'
                ORDER BY as_no DESC, serviceid ASC");
	}
  }
  
  #print DEBUG_SD "STH = $sth.\n";
##-- EVPL - SD  
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  #print DEBUG_SD "Result = $result.\n";
  return $result;
  close DEBUG_SD;
}

##################### Chandini###########################
#Chandini_EPL (directly useing this without any modification)
#Chandini_IPT-20100730 (modified the query to fetch costype) 
# Sayantan_IPT-07-Sep-2010 (modified the query to fetch only MSIDs which are burstable)
#Query to list the EPL Master Services
sub get_master_services_for_accno {
	# inputs: accno
	# output: reference to 2D array containing [master_serviceid,master_service_level]

	my ($accno,$pcet) = @_;
	#  open (FHQ,">/data1/tmp_log/logq");
	#  print FHQ "Inside query ---->$accno,$pcet \n";
	# First check if RO account
	my $ro = get_account_attribute($accno);
	if($$ro{accountattributeid} =~ /^RO$/) { # RO Account
		my $mrwaccno = get_mrwaccno_for_ro($accno);
		$accno = $$mrwaccno{mrw_accno};
	  }
	#print FHQ "After RO check ---->$accno,$pcet \n";
	#check for iptransit and a new query
	my $sth;
	if ($pcet =~ /IPTRANSIT/i) {
		$sth = $dbh->prepare
		("SELECT distinct(mst.master_serviceid),mst.svclabel,ipt.cos_type FROM master_service mst,service srv,ipt_master ipt where srv.master_serviceid != '' and mst.master_serviceid = srv.master_serviceid and srv.productcodeid ilike '$pcet' and srv.accno ilike '$accno' and mst.master_serviceid = ipt.master_sid");
### Sayantan_IPT-07-Sep-2010 - Start
	} elsif ($pcet =~ /IPTBURST/i) {
		$sth = $dbh->prepare
		("SELECT distinct(mst.master_serviceid),mst.svclabel,ipt.cos_type FROM master_service mst,service srv,ipt_master ipt where srv.master_serviceid != '' and mst.master_serviceid = srv.master_serviceid and srv.productcodeid ilike 'IPTRANSIT' and srv.accno ilike '$accno' and ipt.rate_type = '95percentile' and mst.master_serviceid = ipt.master_sid");
### Sayantan_IPT-07-Sep-2010 - End
	} else {
		$sth = $dbh->prepare
		#("SELECT distinct(master_serviceid),master_service_level,svclabel FROM master_service where master_serviceid != ''");
		("SELECT distinct(mst.master_serviceid),mst.svclabel FROM master_service mst,service srv where srv.master_serviceid != '' and mst.master_serviceid = srv.master_serviceid  and srv.productcodeid ilike '$pcet' and srv.accno ilike '$accno'"); 
	}
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	my $result = $sth->fetchall_arrayref;
	$error = $dbh->errstr;
	$sth->finish;
	return $result;
}

#Chandini_EPL(used the same with out change)
#Chandini_IPT-20100730 (modified the query to fetch costype)
# Sayantan_IPT-07-Sep-2010 (modified the query to fetch only MSIDs which are burstable)
#Query to list the EPl Master Services for wholesaler account
sub get_master_serv_for_wholesaler_accno {
  # input: wholesaler accno and productcode (default to 'all' if no productcode supplied)
  # output: 2D array containing [master_serviceid and svclabel]

  open LOG123,">/data1/tmp_log/test_pr_mst_123";
  my $accno = shift @_;
  my $product_code = shift @_;
  #print LOG123 "product_code ---->$product_code\n";
  my $result = CMS_CUSTOMER_DB::get_wholesalers();
  my %allwholesalers = ();
  foreach my $rec (@$result) {
      $allwholesalers{$$rec[0]} = ' ';
  }
  $result = CMS_CUSTOMER_DB::search_customer('','','','');
  my %allcustomers = {};
  foreach my $rec (@$result) {
      if (($$rec[0] ne $$rec[3]) && ($$rec[0]) && ($$rec[3])) {
          $allcustomers{$$rec[0]} = $$rec[3];
      }
  }
  my @wholesalers = ();
  push @wholesalers, $accno;
  for(my $i=0; $i<=$#wholesalers; $i++) {
      foreach my $a (keys%allcustomers) {
          if ($allcustomers{$a} =~ /^$wholesalers[$i]$/) {
              if ($allwholesalers{$a}) {
                  push @wholesalers, $a;
              }
          }
      }
  }
  my $ws_accno = "'".join("' OR customer.wholesaler_accno = '", @wholesalers)."'";
  $product_code = "%" unless $product_code;
  
  #Check for IP-Transit product and execute a different query
  my $command;
  if ($product_code =~ /IPTRANSIT/i) {
	$command = "SELECT distinct(mst.master_serviceid),mst.reseller_svclabel,ipt.cos_type
	FROM master_service mst,service, customer,ipt_master ipt
	WHERE service.accno = customer.accno
        AND (customer.wholesaler_accno = $ws_accno)
        AND service.master_serviceid != ''
        AND service.master_serviceid = mst.master_serviceid
        AND service.productcodeid ilike '$product_code'
	AND mst.master_serviceid = ipt.master_sid";
### Sayantan_IPT-07-Sep-2010 - Start
  } elsif ($product_code =~ /IPTBURST/i) {
	$command = "SELECT distinct(mst.master_serviceid),mst.reseller_svclabel,ipt.cos_type
        FROM master_service mst,service, customer,ipt_master ipt
        WHERE service.accno = customer.accno
        AND (customer.wholesaler_accno = $ws_accno)
        AND service.master_serviceid != ''
        AND service.master_serviceid = mst.master_serviceid
        AND service.productcodeid ilike 'IPTRANSIT'
	AND ipt.rate_type = '95percentile'
        AND mst.master_serviceid = ipt.master_sid";
### Sayantan_IPT-07-Sep-2010 - End
  } else {
  	$command = "SELECT distinct(mst.master_serviceid),mst.reseller_svclabel
        FROM master_service mst,service, customer
        WHERE service.accno = customer.accno
      	AND (customer.wholesaler_accno = $ws_accno)
      	AND service.master_serviceid != ''
      	AND service.master_serviceid = mst.master_serviceid
        AND service.productcodeid ilike '$product_code'";
  }
  #print LOG123 "command--->$command\n";
  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
  $error = $dbh->errstr;
  return 0;
  }
  $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  $sth->finish;
  #print LOG123 "result--->$result\n";
  #close LOG123;
  return $result;
}

#Chandini_EPL used the same for EPL to list the Services associated to Master Service selected
sub get_services_of_master_for_accno {
  # inputs: master service id , accno
  # output: reference to 2D array containing [serviceid, as_no, bgp_neighbor_ip, svclabel, serviceid_alt]

  my $accno = shift @_;
  my $pcet = shift @_;
  my $master_servid = shift @_;

  #open (FH44,">/data1/tmp_log/log44");
  #print FH44 " In get_services_of_master_for_accno and Id:$master_servid \n ";

  # First check if RO account
  my $ro = get_account_attribute($accno);
  my $command;
  if($$ro{accountattributeid} =~ /^RO$/) { # RO Account
        my $mrwaccno = get_mrwaccno_for_ro($accno);
        $accno = $$mrwaccno{mrw_accno};
  }
  my $iswholesale=CMS_CUSTOMER_DB::is_wholesaler($accno);
  #print FH44 "iswholesale---->$iswholesale\n";
  if($iswholesale =~ /^1$/i) {
  	my $result = CMS_CUSTOMER_DB::get_wholesalers();
  	my %allwholesalers = ();
  	foreach my $rec (@$result) {
        	$allwholesalers{$$rec[0]} = ' ';
  	}

  	$result = CMS_CUSTOMER_DB::search_customer('','','','');
  	my %allcustomers = {};
  	foreach my $rec (@$result) {
      		if (($$rec[0] ne $$rec[3]) && ($$rec[0]) && ($$rec[3])) {
          		$allcustomers{$$rec[0]} = $$rec[3];
        	}
   	}

  	my @wholesalers = ();
  	push @wholesalers, $accno;

  	for(my $i=0; $i<=$#wholesalers; $i++) {
      		foreach my $a (keys%allcustomers) {
          		if ($allcustomers{$a} =~ /^$wholesalers[$i]$/) {
              			if ($allwholesalers{$a}) {
                  			push @wholesalers, $a;
              			}
          		}
      		}
  	}

  	my $ws_accno = "'".join("' OR customer.wholesaler_accno = '", @wholesalers)."'";
  	#print FH44 "ws_accno--->$ws_accno\n";
  
  	$pcet = "%" unless $pcet;
  	if ($pcet =~ /VPLS/i) {
  		$command="SELECT serviceid, as_no, bgp_neighbor_ip, service.reseller_svclabel, serviceid_alt, pollingtype
      		FROM service, customer,master_service mst
      		WHERE service.master_serviceid = '$master_servid'
      		AND service.accno = customer.accno
      		AND (customer.wholesaler_accno = $ws_accno)
      		AND service.master_serviceid != ''
      		AND service.master_serviceid = mst.master_serviceid
        	AND service.productcodeid ilike '$pcet'
		AND service.serviceid NOT ilike '%sw'";
  	}
	else {
		$command="SELECT serviceid, as_no, bgp_neighbor_ip, service.reseller_svclabel, serviceid_alt, pollingtype
      		FROM service, customer,master_service mst
      		WHERE service.master_serviceid = '$master_servid'
      		AND service.accno = customer.accno
      		AND (customer.wholesaler_accno = $ws_accno)
      		AND service.master_serviceid != ''
      		AND service.master_serviceid = mst.master_serviceid
        	AND service.productcodeid ilike '$pcet'";
	}
  }
  else {
	if ($pcet =~ /VPLS/i) {
	 	$command = "SELECT serviceid, as_no, bgp_neighbor_ip, svclabel, serviceid_alt, pollingtype, accno, productcodeid FROM service WHERE master_serviceid = '$master_servid' and productcodeid ilike '$pcet' and accno ilike '$accno' and serviceid not like '%sw' ORDER BY as_no DESC, serviceid_alt ASC";
	}
	else {
		$command = "SELECT serviceid, as_no, bgp_neighbor_ip, svclabel, serviceid_alt, pollingtype, accno, productcodeid FROM service WHERE master_serviceid = '$master_servid' and productcodeid ilike '$pcet' and accno ilike '$accno' ORDER BY as_no DESC, serviceid_alt ASC";
	}
  }
  #print FH44 "command----->$command\n";

  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
  $error = $dbh->errstr;
  return 0;
  }

  my $result = $sth->fetchall_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}

sub get_aggregate_services_for_accno {
  # inputs: master service id , accno, logical service id
  # output: reference to 2D array containing [serviceid, as_no, bgp_neighbor_ip, svclabel, serviceid_alt]

  my $accno = shift @_;
  my $pcet = shift @_;
  my $master_servid = shift @_;
  my $logical_servid = shift @_;
my $command;
  #print log44 "--------> $accno,$pcet ,$master_servid,$logical_servid \n";
  # First check if RO account
  my $ro = get_account_attribute($accno);

  if($$ro{accountattributeid} =~ /^RO$/) { # RO Account
        my $mrwaccno = get_mrwaccno_for_ro($accno);
        $accno = $$mrwaccno{mrw_accno};
  }
my $iswholesale=CMS_CUSTOMER_DB::is_wholesaler($accno);
if($iswholesale =~ /^1$/i) {
	$command = "SELECT srv.aggregate_serviceid,agsrv.reseller_svclabel,agsrv.aggregate_service_full FROM service srv,aggregate_service agsrv WHERE srv.serviceid = '$logical_servid' and srv.aggregate_serviceid = agsrv.aggregate_serviceid and srv.productcodeid ilike '$pcet'";


}else{
$command = "SELECT srv.aggregate_serviceid,agsrv.svclabel,agsrv.aggregate_service_full FROM service srv,aggregate_service agsrv WHERE srv.serviceid = '$logical_servid' and srv.aggregate_serviceid = agsrv.aggregate_serviceid and srv.productcodeid ilike '$pcet'";
}

  #print log44 "command---->$command\n";

  my $sth = $dbh->prepare($command);

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}
###################### End ######## Chandini ###############

#++EVPL
sub get_evpl_services {
  # inputs: accno
  # output: reference to 2D array containing [serviceid,svclabel, serviceid_alt]
  my $accno = shift @_;
  
  # First check if RO account
  my $ro = get_account_attribute($accno);

  if($$ro{accountattributeid} =~ /^RO$/) { # RO Account
  	my $mrwaccno = get_mrwaccno_for_ro($accno);
  	$accno = $$mrwaccno{mrw_accno};
  }

  my $sth = $dbh->prepare
    ("SELECT trim(trailing 'a' from serviceid), svclabel, serviceid_alt, pollingtype
      FROM service
      WHERE accno = '$accno' AND productcodeid = 'EVPL' and serviceid not like '%z'
      ORDER BY serviceid ASC");
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}

sub get_evplservices_wholesaler {
  # input: wholesaler accno
  # output: 2D array containing [serviceid, accno, svclabel, reseller_svclabel, serviceid_alt]
  open (DEBUG,">/data1/tmp_log/tmp_traf_prad");
  my $accno = shift @_;
  #print DEBUG "accno---->$accno\n";
	
  my $result = CMS_CUSTOMER_DB::get_wholesalers();
  my %allwholesalers = ();
  foreach my $rec (@$result) {
      $allwholesalers{$$rec[0]} = ' ';
  }

  $result = CMS_CUSTOMER_DB::search_customer('','','','');
  my %allcustomers = {};
  foreach my $rec (@$result) {
      if (($$rec[0] ne $$rec[3]) && ($$rec[0]) && ($$rec[3])) {
          $allcustomers{$$rec[0]} = $$rec[3];
      }
  }

  my @wholesalers = ();
  push @wholesalers, $accno;

  for(my $i=0; $i<=$#wholesalers; $i++) {
      foreach my $a (keys%allcustomers) {
          if ($allcustomers{$a} =~ /^$wholesalers[$i]$/) {
              if ($allwholesalers{$a}) {
                  push @wholesalers, $a;
              }
          }
      }
  }

  my $ws_accno = "'".join("' OR customer.wholesaler_accno = '", @wholesalers)."'";
  #print DEBUG "wsaccno---->$ws_accno\n";
  my $sth = $dbh->prepare
    ("SELECT trim(trailing 'a' from service.serviceid), service.reseller_svclabel, service.serviceid_alt,service.pollingtype
      FROM service, customer
      WHERE service.accno = customer.accno
        AND (customer.wholesaler_accno = $ws_accno)
        AND service.productcodeid = 'EVPL' and service.serviceid not like '%z'
        order by service.serviceid ASC");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  $result = $sth->fetchall_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}
#--EVPL

#Chandini_EPL
#To get the Master service id associated to the service
sub get_masterid {
  # inputs: sid
  # output: master_serviceid,master_service_level
 
	my ($sid, $pcet) = @_;
	open (FHQ,">/data1/tmp_log/logepl");
	#print FHQ "Inside query ---->$sid,pcet : $pcet \n";
	my $sth = $dbh->prepare
        #("SELECT master_serviceid FROM service where serviceid like '$sid' and productcodeid like '$pcet'");
	("SELECT srv.master_serviceid,mst.reseller_svclabel FROM service srv,master_service mst where srv.serviceid like '$sid' and srv.productcodeid like '$pcet' AND srv.master_serviceid !='' AND srv.master_serviceid = mst.master_serviceid");
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	my $result = $sth->fetchall_arrayref;
	#print FHQ "Result from the query: $result \n";
	$error = $dbh->errstr;
	$sth->finish;
	return $result;
}

#Chandini_EPL
#To get the Master service id associated to the service
sub get_masterid_wholesaler {
  # inputs: sid
  # output: master_serviceid,master_service_level

        my ($sid, $pcet) = @_;
        open (FHQ,">/data1/tmp_log/logepl");
        #print FHQ "Inside query ---->$sid,pcet : $pcet \n";
        my $sth = $dbh->prepare
        ("SELECT srv.master_serviceid,mst.svclabel FROM service srv,master_service mst where srv.serviceid like '$sid' and srv.productcodeid like '$pcet' AND srv.master_serviceid !='' AND srv.master_serviceid = mst.master_serviceid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
        my $result = $sth->fetchall_arrayref;
        #print FHQ "Result from the query: $result \n";
        $error = $dbh->errstr;
        $sth->finish;
        return $result;
}

#Chandini_EPL
#To get the Master service id Label
sub get_reseller_label_for_masterservice {
        my $sid = $dbh->quote(shift @_);
 open (FHQ,">/data1/tmp_log/logepl");
        my $sth = $dbh->prepare("SELECT reseller_svclabel as svclabel FROM master_service WHERE master_serviceid = $sid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#Chandini_EPL
#To get the Master service id Label
sub get_reseller_label_for_masterservice_wholesaler {
        my $sid = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT svclabel as svclabel FROM master_service WHERE master_serviceid = $sid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#Chandini_IPT-20100813
#To fetch the cos_type of Master service from ipt_master table 
#input:Master service id
#output:Costype
sub get_costype_masterserv {
	my $sid = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT cos_type as cos_type FROM ipt_master WHERE master_sid = $sid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


#Chandini_IPT-20100729(calling a new query with the search parameter for displaying the list of master service in the dropdown in the Administration page)
sub get_serviceid_for_accno_aggrmast {
  # inputs: accno and productcode (default to 'GIA' if no productcode supplied)
  # output: reference to 2D array containing [serviceid, as_no, bgp_neighbor_ip, svclabel, serviceid_alt]

  my $accno = shift @_;
  my $product_code = shift @_;
  #$product_code = $DEFAULT_PRODUCT_CODE unless $product_code;
  $product_code = "%" unless $product_code;
  my $mastersearch_label = shift @_;
  open (IPF52,">/data1/tmp_log/IPT_lo53");
 
  # First check if RO account
  my $ro = get_account_attribute($accno);

  if($$ro{accountattributeid} =~ /^RO$/) { # RO Account
  	my $mrwaccno = get_mrwaccno_for_ro($accno);
  	$accno = $$mrwaccno{mrw_accno};
  }
  #Chandini_IPT-20100729
  #variable declaraion
  my $sth;

  #("SELECT master_serviceid,svclabel FROM service 
  #  WHERE accno ilike '$accno' AND productcodeid ilike '$product_code' AND master_serviceid !=''
  #  UNION
  #  SELECT aggregate_serviceid,svclabel FROM service
  #  WHERE accno ilike '$accno' AND productcodeid ilike '$product_code' AND aggregate_serviceid !=''");
  #Chandini_IPT-20100729
  #if search parameter is defined then the query to the executed
  if ($mastersearch_label) {
        $mastersearch_label = "%$mastersearch_label%";
	$sth = $dbh->prepare      	
      ("SELECT srv.master_serviceid,mst.svclabel FROM service srv,master_service mst
        WHERE srv.accno ilike '$accno' AND productcodeid ilike '$product_code' AND srv.master_serviceid !='' AND 
	srv.master_serviceid = mst.master_serviceid AND srv.master_serviceid like '$mastersearch_label'
	UNION
	SELECT srv.aggregate_serviceid,aggr.svclabel FROM service srv, aggregate_service aggr
	WHERE srv.accno ilike '$accno' AND productcodeid ilike '$product_code' AND srv.aggregate_serviceid !='' AND
	srv.aggregate_serviceid = aggr.aggregate_serviceid");
   } else {
	#without search condition
	$sth = $dbh->prepare
        ("SELECT srv.master_serviceid,mst.svclabel FROM service srv,master_service mst
        WHERE srv.accno ilike '$accno' AND productcodeid ilike '$product_code' AND srv.master_serviceid !='' AND
        srv.master_serviceid = mst.master_serviceid
        UNION
        SELECT srv.aggregate_serviceid,aggr.svclabel FROM service srv, aggregate_service aggr
        WHERE srv.accno ilike '$accno' AND productcodeid ilike '$product_code' AND srv.aggregate_serviceid !='' AND
        srv.aggregate_serviceid = aggr.aggregate_serviceid");
   }
 
    unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}

#Chandini_EPL (directly useing this without any modification)
#Chandini_Admin_Search
#Query to list the EPL wholesaler Services
sub get_serviceid_for_wholesaler_accno {
  # input: wholesaler accno and productcode (default to 'all' if no productcode supplied)
  # output: 2D array containing [serviceid, accno, as_no, bgp_neighbor_ip, svclabel, reseller_svclabel, serviceid_alt]

  my $accno = shift @_;
  my $product_code = shift @_;
  my $search_label = shift @_;

  my $result = CMS_CUSTOMER_DB::get_wholesalers();
  my %allwholesalers = ();
  foreach my $rec (@$result) {
      $allwholesalers{$$rec[0]} = ' ';
  }

  $result = CMS_CUSTOMER_DB::search_customer('','','','');
  my %allcustomers = {};
  foreach my $rec (@$result) {
      if (($$rec[0] ne $$rec[3]) && ($$rec[0]) && ($$rec[3])) {
          $allcustomers{$$rec[0]} = $$rec[3];
      }
  }

  my @wholesalers = ();
  push @wholesalers, $accno;

  for(my $i=0; $i<=$#wholesalers; $i++) {
      foreach my $a (keys%allcustomers) {
          if ($allcustomers{$a} =~ /^$wholesalers[$i]$/) {
              if ($allwholesalers{$a}) {
                  push @wholesalers, $a;
              }
          }
      }
  }

open DEBUG,">/data1/tmp_log/prad12";
  my $ws_accno = "'".join("' OR customer.wholesaler_accno = '", @wholesalers)."'";
  $product_code = "%" unless $product_code;
print DEBUG "ws_accno: $ws_accno\n";
print DEBUG "product_code : $product_code\n";

   #print DEBUG "wsaccno---->$ws_accno\n";
#  my $sth = $dbh->prepare
#    ("SELECT service.serviceid, service.accno, service.as_no, service.bgp_neighbor_ip, service.svclabel, service.reseller_svclabel, service.serviceid_alt
#      FROM service, customer
#      WHERE service.accno = customer.accno
#        AND (customer.wholesaler_accno = $ws_accno)
#        AND service.productcodeid ilike '%$product_code%'
#        order by service.as_no DESC, service.serviceid ASC");

##++ EVPL - SD
#Chandini
my $sth;
	if ($product_code =~ /EVPL/i) {
		$sth = $dbh->prepare
		("SELECT trim(trailing 'a' from service.serviceid), service.accno, service.as_no, service.bgp_neighbor_ip, service.svclabel, service.reseller_svclabel, service.serviceid_alt
		FROM service, customer
		WHERE service.accno = customer.accno
		AND (customer.wholesaler_accno = $ws_accno)
		AND service.productcodeid = 'EVPL' and service.serviceid not like '%z'
		order by service.as_no DESC, service.serviceid ASC");
	} elsif ($product_code =~ /VPLS/i) {
		$sth = $dbh->prepare
                ("SELECT service.serviceid, service.accno, service.as_no, service.bgp_neighbor_ip, service.svclabel, service.reseller_svclabel, service.serviceid_alt
                FROM service, customer
                WHERE service.accno = customer.accno
                AND (customer.wholesaler_accno = $ws_accno)
                AND service.productcodeid ilike '$product_code' AND service.serviceid NOT like '%sw'
                order by service.as_no DESC, service.serviceid ASC");
	} else {
#Chandini_Admin_Search
	if($search_label){
                $search_label = "%$search_label%";
		$sth = $dbh->prepare
                ("SELECT service.serviceid, service.accno, service.as_no, service.bgp_neighbor_ip, service.svclabel, service.reseller_svclabel, service.serviceid_alt
                FROM service, customer
                WHERE service.accno = customer.accno AND service.accno not like ''
                AND (customer.wholesaler_accno = $ws_accno)
                AND service.productcodeid ilike '$product_code' AND service.serviceid ilike '$search_label'
                order by service.as_no DESC, service.serviceid ASC");
	} else {
		$sth = $dbh->prepare
		("SELECT service.serviceid, service.accno, service.as_no, service.bgp_neighbor_ip, service.svclabel, service.reseller_svclabel, service.serviceid_alt
		FROM service, customer
		WHERE service.accno = customer.accno AND service.accno not like ''
		AND (customer.wholesaler_accno = $ws_accno)
		AND service.productcodeid ilike '$product_code'
		order by service.as_no DESC, service.serviceid ASC");
	}
	}
##-- EVPL - SD

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  $result = $sth->fetchall_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}

#Chandini_IPT-20100729
#change in the query called if search parameter is present for master service
sub get_serviceid_for_wholesaler_accno_aggrmast {
  # input: wholesaler accno and productcode (default to 'all' if no productcode supplied) and mastersearch_label
  # output: 2D array containing [serviceid, accno, as_no, bgp_neighbor_ip, svclabel, reseller_svclabel, serviceid_alt]

  my $accno = shift @_;
  my $product_code = shift @_;
  my $mastersearch_label = shift @_;
  open DEBUG,">/data1/tmp_log/prad1";
  my $result = CMS_CUSTOMER_DB::get_wholesalers();
  my %allwholesalers = ();
  foreach my $rec (@$result) {
      $allwholesalers{$$rec[0]} = ' ';
  }

  $result = CMS_CUSTOMER_DB::search_customer('','','','');
  my %allcustomers = {};
  foreach my $rec (@$result) {
      if (($$rec[0] ne $$rec[3]) && ($$rec[0]) && ($$rec[3])) {
          $allcustomers{$$rec[0]} = $$rec[3];
      }
  }

  my @wholesalers = ();
  push @wholesalers, $accno;

  for(my $i=0; $i<=$#wholesalers; $i++) {
      foreach my $a (keys%allcustomers) {
          if ($allcustomers{$a} =~ /^$wholesalers[$i]$/) {
              if ($allwholesalers{$a}) {
                  push @wholesalers, $a;
              }
          }
      }
  }

  my $ws_accno = "'".join("' OR customer.wholesaler_accno = '", @wholesalers)."'";
  $product_code = "%" unless $product_code;
  #Chandini_IPT-20100729
  #declared a common variable
  #my $sth = $dbh->prepare 
  my $sth;
   #Check for master service search label and call a different query including the search parameter
   if ($mastersearch_label){
	  $mastersearch_label = "%$mastersearch_label%"; 
	  $sth = $dbh->prepare
	  ("(SELECT serv1.aggregate_serviceid,aggr.svclabel,aggr.reseller_svclabel FROM service serv1, customer,aggregate_service aggr
             WHERE serv1.accno = customer.accno AND (customer.wholesaler_accno = $ws_accno)
             AND serv1.productcodeid ilike '$product_code' AND serv1.aggregate_serviceid != ''
             AND aggr.aggregate_serviceid=serv1.aggregate_serviceid	order by serv1.aggregate_serviceid ASC)
             UNION
            (SELECT serv2.master_serviceid,mast.svclabel,mast.reseller_svclabel FROM service serv2, customer ,master_service mast
             WHERE serv2.accno = customer.accno AND (customer.wholesaler_accno = $ws_accno)
             AND serv2.productcodeid ilike '$product_code' AND serv2.master_serviceid != '' 
	     AND mast.master_serviceid=serv2.master_serviceid AND serv2.master_serviceid like '$mastersearch_label' order by serv2.master_serviceid ASC)");
    } else {
	  $sth = $dbh->prepare
	  ("(SELECT serv1.aggregate_serviceid,aggr.svclabel,aggr.reseller_svclabel FROM service serv1, customer ,aggregate_service aggr
             WHERE serv1.accno = customer.accno AND (customer.wholesaler_accno = $ws_accno)
             AND serv1.productcodeid ilike '$product_code' AND serv1.aggregate_serviceid != ''
             AND aggr.aggregate_serviceid=serv1.aggregate_serviceid     order by serv1.aggregate_serviceid ASC)
             UNION
            (SELECT serv2.master_serviceid,mast.svclabel,mast.reseller_svclabel FROM service serv2, customer,master_service mast
             WHERE serv2.accno = customer.accno AND (customer.wholesaler_accno = $ws_accno)
             AND serv2.productcodeid ilike '$product_code' AND serv2.master_serviceid != ''
             AND mast.master_serviceid=serv2.master_serviceid order by serv2.master_serviceid ASC)");   
    }

  #("(SELECT serv1.aggregate_serviceid FROM service serv1, customer cust1
  #       WHERE serv1.accno = cust1.accno AND (cust1.wholesaler_accno = $ws_accno) 
  #       AND serv1.productcodeid ilike '$product_code' AND serv1.aggregate_serviceid != '' order by serv1.aggregate_serviceid ASC)
#	 UNION
#	 (SELECT serv2.master_serviceid FROM service serv2, customer cust2
#         WHERE serv2.accno = cust2.accno AND (cust2.wholesaler_accno = $ws_accno)
#         AND serv2.productcodeid ilike '$product_code' AND serv2.master_serviceid != '' order by serv2.master_serviceid ASC)");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  $result = $sth->fetchall_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}

#sub get_serviceid_for_wholesaler_accno {
#  # input: wholesaler accno and productcode (default to 'all' if no productcode supplied)
#  # output: 2D array containing [serviceid, accno, as_no]
#
#  my $accno = $dbh->quote(shift @_);
#  my $product_code = shift @_;
#  #$product_code = $DEFAULT_PRODUCT_CODE unless $product_code;
#  $product_code = "%" unless $product_code;
#
#  my @joined_result;
#
#  # get children
#  my $sth = $dbh->prepare
#    ("SELECT service.serviceid, service.accno, service.as_no, service.bgp_neighbor_ip
#      FROM service, customer
#      WHERE service.accno = customer.accno
#        AND customer.wholesaler_accno = $accno 
#        AND service.productcodeid ilike '%$product_code%'
#	order by service.as_no DESC");
#  
#  unless ($sth->execute) {
#    $error = $dbh->errstr;
#    return 0;
#  }
#  
#  if (my $result = $sth->fetchall_arrayref) {
#    push @joined_result, @$result;
#  } else {
#    $error = $dbh->errstr;
#    $sth->finish;
#    return 0;
#  }
#
#  ## get grand children
#  $sth = $dbh->prepare
#    ("SELECT service.serviceid, service.accno, service.as_no, service.bgp_neighbor_ip
#      FROM service, customer as child, customer as grand
#      WHERE service.accno = grand.accno
#        AND grand.wholesaler_accno = child.accno
#        AND child.wholesaler_accno = $accno
#        AND service.productcodeid ilike '%$product_code%'
#	order by service.as_no DESC");
#
#  unless ($sth->execute) {
#    $error = $dbh->errstr;
#    return 0;
#  }
#  
#  if (my $result = $sth->fetchall_arrayref) {
#    push @joined_result, @$result;
#  } else {
#    $error = $dbh->errstr;
#    $sth->finish;
#    return 0;
#  }
#
#  # get great grand children
#  $sth = $dbh->prepare
#    ("SELECT service.serviceid, service.accno, service.as_no, service.bgp_neighbor_ip
#      FROM service, customer as child, customer as grand, customer as great
#      WHERE service.accno = great.wholesaler_accno
#        AND great.wholesaler_accno = grand.accno
#        AND grand.wholesaler_accno = child.accno
#        AND child.wholesaler_accno = $accno
#        AND service.productcodeid ilike '%$product_code%'
#	order by service.as_no DESC");
#  
#  unless ($sth->execute) {
#    $error = $dbh->errstr;
#    return 0;
#  }
#  
#  if (my $result = $sth->fetchall_arrayref) {
#    push @joined_result, @$result;
#  } else {
#    $error = $dbh->errstr;
#    $sth->finish;
#    return 0;
#  }
#
#  return \@joined_result;
#}

sub get_contact_details {
  
  my $accno = $dbh->quote(shift @_);
  my $typeid = shift @_;

  my $sth = $dbh->prepare
    ("SELECT firstname, surname, telephone, fax, email, mobile
      FROM contact
      NATURAL JOIN contact_type
      WHERE accno = $accno
        AND typeid = $typeid");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  
  $error = $dbh->errstr;
  $sth->finish;
  return $result;

}

sub update_contact_details {
  
  my $accno = $dbh->quote(shift @_);
  my $typeid = shift @_;
  my $firstname = $dbh->quote(shift @_);
  my $surname = $dbh->quote(shift @_);
  my $telephone = $dbh->quote(shift @_);
  my $fax = $dbh->quote(shift @_);
  my $email = $dbh->quote(shift @_);
  my $mobile = $dbh->quote(shift @_); 

  my $sth = $dbh->prepare
    ("DELETE FROM contact
      WHERE accno = $accno
        AND typeid = $typeid");
  my $result = $sth->execute;
  $error = $dbh->errstr;
  $sth->finish;
  
  $sth = $dbh->prepare
     ("INSERT INTO contact(accno, typeid, firstname, surname, telephone, fax, 
                          email, mobile)
         VALUES($accno, $typeid, $firstname, $surname, $telephone, $fax,
                          $email, $mobile)");


  $result = $sth->execute;
  $error = $dbh->errstr;
  $sth->finish;
  return $result;

}


sub get_subscription {
  my $accno = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT subscriptionid
      FROM customer
      WHERE accno = $accno");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchrow_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $$result[0];
}

sub update_subscription {
  my $accno = $dbh->quote(shift @_);
  my $subscription;

  my $sth = $dbh->prepare
    ("UPDATE customer
         SET subsciptionid = $subscription");

  my $result = $sth->execute;
  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}

sub get_notice_emails {
  my $sth = $dbh->prepare
    ("SELECT firstname || ' ' || surname, email FROM contact NATURAL JOIN contact_type
      WHERE type = 'billing'");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  
  $error = $dbh->errstr;
  $sth->finish;
  return $result;

}

sub get_slacode_for_all_customer {
        my $sth = $dbh->prepare("Select accno, slareportcodeid from customer 
                                 where accno<>'' order by accno");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
        
        my $result = $sth->fetchall_arrayref;
  
        $error = $dbh->errstr;
        $sth->finish;  
        return $result;
}

#---------------------------------------------------------------------

sub epaccess_as_true_or_false {
  my $current = shift;

  return [['TRUE'],['FALSE']] if $current;
  return [['FALSE'],['TRUE']];
}

sub access_through_EP {
  my $accno = $dbh->quote(shift @_);
  
  my $sth = $dbh->prepare
    ("SELECT ep_access FROM customer WHERE accno = $accno");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 'FALSE';
  }
  
  my @result = $sth->fetchrow_array;
  $error = $dbh->errstr;
  $sth->finish;
  
  if($result[0]){
    return 'TRUE';
  }
  return 'FALSE';
}

#Stavan-Aug10-IPTRANSIT_CHINA_METERING
sub get_customers_for_wholesaler  {
        my $accno = $dbh->quote(shift @_);
     	my $wholesalerDirectIPC = 0;
	my $sth = $dbh->prepare ("select distinct customer from ipc_china where dir_traffic = 'direct' and productcode = 'IPTRANSIT';");
	unless ($sth->execute) {
                $error = $dbh->errstr;
        }
	my @allcustomers = ();
	my $custList = "";

        my $result = $sth->fetchall_arrayref;
        foreach my $custids (@$result) {
                if($$custids[0] eq $accno) {
			$wholesalerDirectIPC = 1;
                }
		push (@allcustomers,$dbh->quote($$custids[0]));
        }
        $custList = join(",",@allcustomers);

        $sth = $dbh->prepare ("select accno from customer where wholesaler_accno = $accno and accno IN ($custList);");
        unless ($sth->execute) {
                $error .= $dbh->errstr;
                return 0;
        }
        $result = $sth->fetchall_arrayref;
		if ($result) {
                $sth->finish;
		if($wholesalerDirectIPC) {
				push(@$result,$accno);
		}
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_ipc_services_for_retailer {
	my $accno = $dbh->quote(shift @_);
	my $sth = $dbh->prepare ("select distinct ipc_servid from ipc_china where customer = $accno and dir_traffic = 'direct';");
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	my $result = $sth->fetchall_arrayref;
	if ($result) {
	       $sth->finish;
	       return $result;
	} else {
	       $error = $dbh->errstr;
	       return 0;
	}
}


sub get_asnos_for_chinametering_customers {
	my $accno = $dbh->quote(shift @_);
	my $sth = $dbh->prepare ("select distinct as_nos from ipc_china where customer = $accno and dir_traffic = 'direct';");
 	unless ($sth->execute) {
        	$error = $dbh->errstr;
        	return 0;
 	}
	my $result = $sth->fetchall_arrayref;
 	if ($result) {
 	       $sth->finish;
 	       return $result;
 	} else {
 	       $error = $dbh->errstr;
 	       return 0;
 	}

}

sub get_asnos_for_ipc_service {
	my $serviceid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare ("select distinct as_nos from ipc_china where ipc_servid = $serviceid and dir_traffic = 'direct';");
 	unless ($sth->execute) {
        	$error = $dbh->errstr;
        	return 0;
 	}
	my $result = ($sth->fetchrow_array)[0];
 	if ($result) {
 	       $sth->finish;
 	       return $result;
 	} else {
 	       $error = $dbh->errstr;
 	       return 0;
 	}

}


######################################################################################
#Change Request Number:- CRQ000000004261
#Reason :-              Looking Glass based on loopback address
#                       GIA,IPTRANSIT--- Loopback0 and IPVPN,VPLS,EVPL ----- Loopback11
#Date:-                 22-Sep-2011
########################################################################################

sub get_router_os_looking_glass {
        #my $router = $dbh->quote(shift @_);
         #my $sql_command = "select router_software from router_software where routername like $router";#changed to below query by abhi
        
		my $router = $dbh->quote(shift @_);
		#$router	="'$router'";
        my $sth = $dbh->prepare ("select software.Router_Software from Router_Software software, Looking_Glass_Node lg where software.Router_Software_ID =lg.Router_Software_ID AND (lg.Router_Name like $router)");
        unless ($sth->execute) {
                $error = $dbh::err;
                return 0;
        }
        my $result = ($sth->fetchrow_array)[0];
        $sth->finish;
        return $result;
}

################################################################
sub get_router_name_looking_glass{
	my $routerIP = $dbh->quote(shift @_);
		my $sql_command = "select Router_Name from Looking_Glass_Node where IP_Address like $routerIP";
        my $sth = $dbh->prepare ($sql_command);
        unless ($sth->execute) {
                $error = $dbh::err;
                return 0;
        }
        my $result = ($sth->fetchrow_array)[0];
        $sth->finish;
        return $result;
}

1;






