package com.telstra.netops.routemanagement.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import com.telstra.oauth2.client.MethodSecurityConfig;
import com.telstra.oauth2.client.OAuth2ResourceConfig;
import com.telstra.oauth2.client.TelstraConfig;

@EnableAutoConfiguration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, /* securedEnabled = true, */proxyTargetClass = true)
@Import({ OAuth2ResourceConfig.class, MethodSecurityConfig.class, TelstraConfig.class })
@ComponentScan(basePackages = { "com.telstra.netops.routemanagement.server" })
public class Application extends SpringBootServletInitializer {

	public static void main(String[] args) throws Exception {

		SpringApplication.run(Application.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

}