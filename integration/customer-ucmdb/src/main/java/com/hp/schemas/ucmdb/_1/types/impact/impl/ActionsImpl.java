/*
 * XML Type:  Actions
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.Actions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML Actions(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ActionsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.Actions
{
    
    public ActionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ACTION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "action");
    
    
    /**
     * Gets array of all "action" elements
     */
    public com.hp.schemas.ucmdb._1.types.impact.Action[] getActionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ACTION$0, targetList);
            com.hp.schemas.ucmdb._1.types.impact.Action[] result = new com.hp.schemas.ucmdb._1.types.impact.Action[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "action" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.Action getActionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.Action target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.Action)get_store().find_element_user(ACTION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "action" element
     */
    public int sizeOfActionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ACTION$0);
        }
    }
    
    /**
     * Sets array of all "action" element
     */
    public void setActionArray(com.hp.schemas.ucmdb._1.types.impact.Action[] actionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(actionArray, ACTION$0);
        }
    }
    
    /**
     * Sets ith "action" element
     */
    public void setActionArray(int i, com.hp.schemas.ucmdb._1.types.impact.Action action)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.Action target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.Action)get_store().find_element_user(ACTION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(action);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "action" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.Action insertNewAction(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.Action target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.Action)get_store().insert_element_user(ACTION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "action" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.Action addNewAction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.Action target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.Action)get_store().add_element_user(ACTION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "action" element
     */
    public void removeAction(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ACTION$0, i);
        }
    }
}
