package com.telstra.netops.client.exception;

public class LdapPasswordCheckException extends RuntimeException {

	public LdapPasswordCheckException(String message) {
		super(message);
	}
	
	public LdapPasswordCheckException(String message, Throwable t) {
		super(message, t);
	}
	
}
