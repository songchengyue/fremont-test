/*
 * An XML document type.
 * Localname: getTopologyQueryExistingResultByNameResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getTopologyQueryExistingResultByNameResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetTopologyQueryExistingResultByNameResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponseDocument
{
    
    public GetTopologyQueryExistingResultByNameResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETTOPOLOGYQUERYEXISTINGRESULTBYNAMERESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getTopologyQueryExistingResultByNameResponse");
    
    
    /**
     * Gets the "getTopologyQueryExistingResultByNameResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponse getGetTopologyQueryExistingResultByNameResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponse)get_store().find_element_user(GETTOPOLOGYQUERYEXISTINGRESULTBYNAMERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getTopologyQueryExistingResultByNameResponse" element
     */
    public void setGetTopologyQueryExistingResultByNameResponse(com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponse getTopologyQueryExistingResultByNameResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponse)get_store().find_element_user(GETTOPOLOGYQUERYEXISTINGRESULTBYNAMERESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponse)get_store().add_element_user(GETTOPOLOGYQUERYEXISTINGRESULTBYNAMERESPONSE$0);
            }
            target.set(getTopologyQueryExistingResultByNameResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getTopologyQueryExistingResultByNameResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponse addNewGetTopologyQueryExistingResultByNameResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponse)get_store().add_element_user(GETTOPOLOGYQUERYEXISTINGRESULTBYNAMERESPONSE$0);
            return target;
        }
    }
}
