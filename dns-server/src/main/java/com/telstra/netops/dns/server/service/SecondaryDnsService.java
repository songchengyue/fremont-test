package com.telstra.netops.dns.server.service;

import java.util.List;

import com.telstra.netops.dns.server.model.ClientDnsRemoveRequest;
import com.telstra.netops.dns.server.model.ClientSecondaryDnsRequest;
import com.telstra.netops.dns.server.model.SecondaryDnsRecord;
import com.telstra.netops.dns.server.model.Success;
public interface SecondaryDnsService {

	Success add(ClientSecondaryDnsRequest request);
	
	List<SecondaryDnsRecord> list(String accountId);
	
	Success remove(ClientDnsRemoveRequest request);	

}
