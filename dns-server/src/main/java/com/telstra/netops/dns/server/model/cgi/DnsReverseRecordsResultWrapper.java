package com.telstra.netops.dns.server.model.cgi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telstra.netops.dns.server.model.DnsReverseRecord;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DnsReverseRecordsResultWrapper {
	
	@JsonProperty
	private List<DnsReverseRecord> result;
	
	@JsonIgnore
	public boolean isEmpty() {
		return result == null || result.get(0) == null || result.get(0).notARecord();
	}

	public List<DnsReverseRecord> getResult() {
		return result;
	}

}
