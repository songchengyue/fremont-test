/*
 * XML Type:  RelationCountNodes
 * Namespace: http://schemas.hp.com/ucmdb/1/types/query
 * Java type: com.hp.schemas.ucmdb._1.types.query.RelationCountNodes
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.query.impl;
/**
 * An XML RelationCountNodes(@http://schemas.hp.com/ucmdb/1/types/query).
 *
 * This is a complex type.
 */
public class RelationCountNodesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.query.RelationCountNodes
{
    
    public RelationCountNodesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RELATIONCOUNTNODE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/query", "relationCountNode");
    
    
    /**
     * Gets array of all "relationCountNode" elements
     */
    public com.hp.schemas.ucmdb._1.types.query.RelationCountNode[] getRelationCountNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(RELATIONCOUNTNODE$0, targetList);
            com.hp.schemas.ucmdb._1.types.query.RelationCountNode[] result = new com.hp.schemas.ucmdb._1.types.query.RelationCountNode[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "relationCountNode" element
     */
    public com.hp.schemas.ucmdb._1.types.query.RelationCountNode getRelationCountNodeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.RelationCountNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.RelationCountNode)get_store().find_element_user(RELATIONCOUNTNODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "relationCountNode" element
     */
    public int sizeOfRelationCountNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RELATIONCOUNTNODE$0);
        }
    }
    
    /**
     * Sets array of all "relationCountNode" element
     */
    public void setRelationCountNodeArray(com.hp.schemas.ucmdb._1.types.query.RelationCountNode[] relationCountNodeArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(relationCountNodeArray, RELATIONCOUNTNODE$0);
        }
    }
    
    /**
     * Sets ith "relationCountNode" element
     */
    public void setRelationCountNodeArray(int i, com.hp.schemas.ucmdb._1.types.query.RelationCountNode relationCountNode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.RelationCountNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.RelationCountNode)get_store().find_element_user(RELATIONCOUNTNODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(relationCountNode);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "relationCountNode" element
     */
    public com.hp.schemas.ucmdb._1.types.query.RelationCountNode insertNewRelationCountNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.RelationCountNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.RelationCountNode)get_store().insert_element_user(RELATIONCOUNTNODE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "relationCountNode" element
     */
    public com.hp.schemas.ucmdb._1.types.query.RelationCountNode addNewRelationCountNode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.RelationCountNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.RelationCountNode)get_store().add_element_user(RELATIONCOUNTNODE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "relationCountNode" element
     */
    public void removeRelationCountNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RELATIONCOUNTNODE$0, i);
        }
    }
}
