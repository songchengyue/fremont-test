/*
 * An XML document type.
 * Localname: isJobActiveResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one isJobActiveResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class IsJobActiveResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponseDocument
{
    
    public IsJobActiveResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ISJOBACTIVERESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "isJobActiveResponse");
    
    
    /**
     * Gets the "isJobActiveResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponse getIsJobActiveResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponse)get_store().find_element_user(ISJOBACTIVERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "isJobActiveResponse" element
     */
    public void setIsJobActiveResponse(com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponse isJobActiveResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponse)get_store().find_element_user(ISJOBACTIVERESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponse)get_store().add_element_user(ISJOBACTIVERESPONSE$0);
            }
            target.set(isJobActiveResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "isJobActiveResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponse addNewIsJobActiveResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveResponse)get_store().add_element_user(ISJOBACTIVERESPONSE$0);
            return target;
        }
    }
}
