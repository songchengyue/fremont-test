/*
 * XML Type:  updateCredentialsEntryRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML updateCredentialsEntryRequest(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class UpdateCredentialsEntryRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest
{
    
    public UpdateCredentialsEntryRequestImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DOMAINNAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "domainName");
    private static final javax.xml.namespace.QName PROTOCOLNAME$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "protocolName");
    private static final javax.xml.namespace.QName CREDENTIALSENTRYID$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "credentialsEntryID");
    private static final javax.xml.namespace.QName CREDENTIALSENTRYPARAMETERS$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "credentialsEntryParameters");
    private static final javax.xml.namespace.QName CMDBCONTEXT$8 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "CmdbContext");
    
    
    /**
     * Gets the "domainName" element
     */
    public java.lang.String getDomainName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOMAINNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "domainName" element
     */
    public org.apache.xmlbeans.XmlString xgetDomainName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DOMAINNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "domainName" element
     */
    public void setDomainName(java.lang.String domainName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOMAINNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DOMAINNAME$0);
            }
            target.setStringValue(domainName);
        }
    }
    
    /**
     * Sets (as xml) the "domainName" element
     */
    public void xsetDomainName(org.apache.xmlbeans.XmlString domainName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DOMAINNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DOMAINNAME$0);
            }
            target.set(domainName);
        }
    }
    
    /**
     * Gets the "protocolName" element
     */
    public java.lang.String getProtocolName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROTOCOLNAME$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "protocolName" element
     */
    public org.apache.xmlbeans.XmlString xgetProtocolName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PROTOCOLNAME$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "protocolName" element
     */
    public void setProtocolName(java.lang.String protocolName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROTOCOLNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROTOCOLNAME$2);
            }
            target.setStringValue(protocolName);
        }
    }
    
    /**
     * Sets (as xml) the "protocolName" element
     */
    public void xsetProtocolName(org.apache.xmlbeans.XmlString protocolName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PROTOCOLNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PROTOCOLNAME$2);
            }
            target.set(protocolName);
        }
    }
    
    /**
     * Gets the "credentialsEntryID" element
     */
    public java.lang.String getCredentialsEntryID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREDENTIALSENTRYID$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "credentialsEntryID" element
     */
    public org.apache.xmlbeans.XmlString xgetCredentialsEntryID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CREDENTIALSENTRYID$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "credentialsEntryID" element
     */
    public void setCredentialsEntryID(java.lang.String credentialsEntryID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREDENTIALSENTRYID$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CREDENTIALSENTRYID$4);
            }
            target.setStringValue(credentialsEntryID);
        }
    }
    
    /**
     * Sets (as xml) the "credentialsEntryID" element
     */
    public void xsetCredentialsEntryID(org.apache.xmlbeans.XmlString credentialsEntryID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CREDENTIALSENTRYID$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CREDENTIALSENTRYID$4);
            }
            target.set(credentialsEntryID);
        }
    }
    
    /**
     * Gets the "credentialsEntryParameters" element
     */
    public com.hp.schemas.ucmdb._1.types.CIProperties getCredentialsEntryParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().find_element_user(CREDENTIALSENTRYPARAMETERS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "credentialsEntryParameters" element
     */
    public void setCredentialsEntryParameters(com.hp.schemas.ucmdb._1.types.CIProperties credentialsEntryParameters)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().find_element_user(CREDENTIALSENTRYPARAMETERS$6, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().add_element_user(CREDENTIALSENTRYPARAMETERS$6);
            }
            target.set(credentialsEntryParameters);
        }
    }
    
    /**
     * Appends and returns a new empty "credentialsEntryParameters" element
     */
    public com.hp.schemas.ucmdb._1.types.CIProperties addNewCredentialsEntryParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().add_element_user(CREDENTIALSENTRYPARAMETERS$6);
            return target;
        }
    }
    
    /**
     * Gets the "CmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$8, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$8);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "CmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$8);
            return target;
        }
    }
}
