/*
 * XML Type:  CmdbContext
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.CmdbContext
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML CmdbContext(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class CmdbContextImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.CmdbContext
{
    
    public CmdbContextImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CALLERAPPLICATION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "callerApplication");
    
    
    /**
     * Gets the "callerApplication" element
     */
    public java.lang.String getCallerApplication()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CALLERAPPLICATION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "callerApplication" element
     */
    public org.apache.xmlbeans.XmlString xgetCallerApplication()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CALLERAPPLICATION$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "callerApplication" element
     */
    public void setCallerApplication(java.lang.String callerApplication)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CALLERAPPLICATION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CALLERAPPLICATION$0);
            }
            target.setStringValue(callerApplication);
        }
    }
    
    /**
     * Sets (as xml) the "callerApplication" element
     */
    public void xsetCallerApplication(org.apache.xmlbeans.XmlString callerApplication)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CALLERAPPLICATION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CALLERAPPLICATION$0);
            }
            target.set(callerApplication);
        }
    }
}
