package com.telstra.netops.lookingglass.server.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.telstra.netops.lookingglass.server.exception.LookingClassApplicationInternalException;
import com.telstra.netops.lookingglass.server.exception.LookingGlassApplicationLocalSimulatedBusinessRuleException;

@Component
@Profile("local")
public class LocalErrorThrowingService  {
	
	@Value("${throw.internal.exception:false}")
	private boolean throwInternalException;
	
	@Value("${throw.businessRule.exception:false}")
	private boolean throwBusinessRuleException;

	public void throwError() {
		if (throwInternalException) {
			throw new LookingClassApplicationInternalException("simulating an internal error for testing purposes");
		}

		if (throwBusinessRuleException) {
			throw new LookingGlassApplicationLocalSimulatedBusinessRuleException("simulating a business rule error for testing purposes");
		}
	}
	
}
