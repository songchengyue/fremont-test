/*
 * XML Type:  ShallowCIs
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ShallowCIs
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ShallowCIs(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ShallowCIsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ShallowCIs
{
    
    public ShallowCIsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SHALLOWCI$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "shallowCI");
    
    
    /**
     * Gets array of all "shallowCI" elements
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCI[] getShallowCIArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SHALLOWCI$0, targetList);
            com.hp.schemas.ucmdb._1.types.ShallowCI[] result = new com.hp.schemas.ucmdb._1.types.ShallowCI[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "shallowCI" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCI getShallowCIArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCI target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCI)get_store().find_element_user(SHALLOWCI$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "shallowCI" element
     */
    public int sizeOfShallowCIArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SHALLOWCI$0);
        }
    }
    
    /**
     * Sets array of all "shallowCI" element
     */
    public void setShallowCIArray(com.hp.schemas.ucmdb._1.types.ShallowCI[] shallowCIArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(shallowCIArray, SHALLOWCI$0);
        }
    }
    
    /**
     * Sets ith "shallowCI" element
     */
    public void setShallowCIArray(int i, com.hp.schemas.ucmdb._1.types.ShallowCI shallowCI)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCI target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCI)get_store().find_element_user(SHALLOWCI$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(shallowCI);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "shallowCI" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCI insertNewShallowCI(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCI target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCI)get_store().insert_element_user(SHALLOWCI$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "shallowCI" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCI addNewShallowCI()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCI target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCI)get_store().add_element_user(SHALLOWCI$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "shallowCI" element
     */
    public void removeShallowCI(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SHALLOWCI$0, i);
        }
    }
}
