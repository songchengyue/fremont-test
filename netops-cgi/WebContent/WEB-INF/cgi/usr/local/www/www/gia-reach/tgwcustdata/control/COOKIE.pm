#
# Author: Tony Tam
# Date: 22 July 2003
# File: COOKIE.pm
#
# This file contains generic cookies handle procedures.
#

package COOKIE;
use Exporter;
use strict;
use CGI qw/:standard/;
use CGI::Cookie;

our @ISA	= qw(Exporter);
our @EXPORT_OK	= qw(create_cookie, set_cookie, read_cookie, read_cookies, delete_cookie, get_cookie);

my $timeout = 300;			#Time out value in seconds

# Error Codes
use constant SUCCESS => 1;
use constant ERROR => undef;

sub create_cookie  {
  my ($name, $value, $expires) = @_;
  my $cookie_value = new CGI::Cookie(-name => $name, 
                                     -values => $value,
                                     -expires => $expires);
#                                     -secure => 1);
  print "Set-Cookie: $cookie_value\n";
}

sub set_cookie {
  my ($name, $value) = @_;
  my %cookies = fetch CGI::Cookie;
  if (exists $cookies{$name}) {
    $cookies{$name}->value($value);
    return 1;
  } else {
    return 0;
  }  
}

sub delete_cookie {
  my ($name) = @_;
  my %cookies = fetch CGI::Cookie;
  if (exists $cookies{$name}) {
    $cookies{name}->expires('1s');
    return 1;
  } 
  return 0; 
}

sub get_cookie {
  my ($name) = @_;
  my %cookies = fetch CGI::Cookie;
  if (exists $cookies{$name}) {
    return $cookies{$name}->value;
  }
  return; 
}
