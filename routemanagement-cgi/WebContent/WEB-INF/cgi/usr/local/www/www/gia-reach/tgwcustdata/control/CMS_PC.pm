# Copyright Maettr Pty Ltd.
#
# File: CMS_PC.pm
#
# $Id: CMS_PC.pm,v 1.2 2003/10/27 11:53:39 paull Exp $
# $Log: CMS_PC.pm,v $
# Revision 1.2  2003/10/27 11:53:39  paull
# In module CMS_PC_DB.pm, forced all productcodeid's to be uppercase and
# ordered output of methods alphabetically based on productcodeid.
#
# Changed return value for get_product_codes_for_accno() to be reference
# to hash of hash references.
#
# Added directory 'eportal' which lists changes made and includes psql
# script to update the cms database.
#
# Revision 1.1  2003/10/20 15:00:22  paull
# Changed modules and new modules by Paul Libauer
#

package CMS_PC;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_PC_DB;

our @ISA = qw (Exporter);

our $error;

sub start {
  
  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} =~ /cancel/i))) {

    # default to edit_product_code
    CMS::clear_html_values();
    $CMS::html_values{subsystem}{value} = "productcodes";
    $CMS::html_values{command}{value} = "edit_productcodes";
    
  }

  if (defined $CMS::html_values{stage}{value}) {
    unless (CMS::check_screen($CMS::html_values{current_screen}{value})){
      $CMS::html_values{stage}{value}--;
    }
  }

  #-----------------------------------------------------------------------
  
  if($CMS::html_values{command}{value} eq "add_productcodes") {
    # Add a new product code
    if (not defined ($CMS::html_values{stage}{value})) {
      CMS::output_html("productcodes_add");
    }
    elsif ($CMS::html_values{stage}{value} == 2) {
      $CMS::html_values{product_code}{value} =~ s/^\s+|\s+$//g;
      $CMS::html_values{product_name}{value} =~ s/^\s+|\s+$//g;
      
      if($CMS::html_values{product_code}{value} =~ /^$/ or 
	 $CMS::html_values{product_name}{value} =~ /^$/){
	CMS::add_error("Product Code addition failed.");
	CMS::add_error("Neither a product code or a product name can be null".
		       " or consist of whitespace characters only.");
	CMS::output_html("productcodes_add");	
      }
      else {
	if(CMS_PC_DB::add_productcode
	   ($CMS::html_values{product_code}{value},
	    $CMS::html_values{product_name}{value})){
	  
	  CMS::add_error("Product Code added successfully.");
	  CMS::output_html("productcodes_add");	
	}
	else {
	  CMS::add_error("Product Code addition failed.");
	  CMS::add_error($CMS_PC_DB::error);
	  CMS::output_html("productcodes_add");	
	}
      }
    }
  }
  
  #-----------------------------------------------------------------------
 
  elsif ($CMS::html_values{command}{value} eq "edit_productcodes") {
    # Edit an existing product code
    if (not defined ($CMS::html_values{stage}{value})) {
      # first stage
      if(my $codes = CMS_PC_DB::get_productcodes){
	$CMS::html_values{product_codes}{options} = $codes;
	CMS::output_html("productcodes_edit");
      }
      else {
	CMS::add_error("No product codes exist.");
	CMS::output_html("productcodes_edit");
      }
    }
    
    elsif ($CMS::html_values{stage}{value} == 2) {
      if(my $result = CMS_PC_DB::get_productname
	 ($CMS::html_values{product_codes}{value})){
	
	$CMS::html_values{product_code}{value} = 
	  $CMS::html_values{product_codes}{value};
	$CMS::html_values{product_name}{value} = $result;
	CMS::output_html("productcodes_edit2");
      }
      else {
	# Product code does not exist
	CMS::add_error("The product code entered does not exists.");
	CMS::output_html("productcodes_edit");
      }

    }
    elsif ($CMS::html_values{stage}{value} == 3) {
      if(CMS_PC_DB::update_productname
	 ($CMS::html_values{product_code}{value},
	  $CMS::html_values{product_name}{value})){
	
	my $codes = CMS_PC_DB::get_productcodes;
	$CMS::html_values{product_codes}{options} = $codes;
	
	CMS::add_error("Product Code updated successfully.");
	CMS::output_html("productcodes_edit");
      }
      else {
	CMS::add_error("Failed to edit Product Code.");
	CMS::add_error($CMS_PC_DB::error);
	CMS::output_html("productcodes_edit2");
      }
    }
  }
  
  #-----------------------------------------------------------------------
  
  elsif ($CMS::html_values{command}{value} eq "delete_productcodes") {
    if (not defined ($CMS::html_values{stage}{value})) {
      # first stage
      if(my $codes = CMS_PC_DB::get_productcodes){
	$CMS::html_values{product_codes}{options} = $codes;
	CMS::output_html("productcodes_delete");
      }
      else {
	CMS::add_error("No product codes exist.");
	CMS::output_html("productcodes_delete");
      }
    }
    elsif ($CMS::html_values{stage}{value} == 2) {
      # second stage
      

      if(CMS_PC_DB::delete_productcode($CMS::html_values{product_codes}{value})){
	CMS::add_error("Product code deleted successfully.");
	$CMS::html_values{product_codes}{options} = CMS_PC_DB::get_productcodes;
	CMS::output_html("productcodes_delete");
      }
      else {
	my $error = $CMS_PC_DB::error;
	$CMS::html_values{product_codes}{options} = CMS_PC_DB::get_productcodes;
	CMS::add_error("Deletion failed.");
	CMS::add_error($error);
	CMS::output_html("productcodes_delete");
      }
    }
  }

  #-----------------------------------------------------------------------
} 
  
1;
