package com.telstra.netops.routemanagement.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception class for All CGI Errors
 *
 */
public class RMApplicationCustomException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RMApplicationCustomException(String message, Throwable cause) {
		super(message, cause);
	}

	public RMApplicationCustomException(String message) {
		super(message);
	}

}
