/*
 * XML Type:  Scope
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/types
 * Java type: com.hp.schemas.ucmdb.discovery._1.types.Scope
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.types;


/**
 * An XML Scope(@http://schemas.hp.com/ucmdb/discovery/1/types).
 *
 * This is a complex type.
 */
public interface Scope extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Scope.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("scope2d0ftype");
    
    /**
     * Gets the "Exclude" element
     */
    com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude getExclude();
    
    /**
     * Sets the "Exclude" element
     */
    void setExclude(com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude exclude);
    
    /**
     * Appends and returns a new empty "Exclude" element
     */
    com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude addNewExclude();
    
    /**
     * Gets the "Include" element
     */
    com.hp.schemas.ucmdb.discovery._1.types.Scope.Include getInclude();
    
    /**
     * Sets the "Include" element
     */
    void setInclude(com.hp.schemas.ucmdb.discovery._1.types.Scope.Include include);
    
    /**
     * Appends and returns a new empty "Include" element
     */
    com.hp.schemas.ucmdb.discovery._1.types.Scope.Include addNewInclude();
    
    /**
     * An XML Exclude(@http://schemas.hp.com/ucmdb/discovery/1/types).
     *
     * This is a complex type.
     */
    public interface Exclude extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Exclude.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("exclude66b5elemtype");
        
        /**
         * Gets array of all "Ranges" elements
         */
        com.hp.schemas.ucmdb.discovery._1.types.IPRange[] getRangesArray();
        
        /**
         * Gets ith "Ranges" element
         */
        com.hp.schemas.ucmdb.discovery._1.types.IPRange getRangesArray(int i);
        
        /**
         * Returns number of "Ranges" element
         */
        int sizeOfRangesArray();
        
        /**
         * Sets array of all "Ranges" element
         */
        void setRangesArray(com.hp.schemas.ucmdb.discovery._1.types.IPRange[] rangesArray);
        
        /**
         * Sets ith "Ranges" element
         */
        void setRangesArray(int i, com.hp.schemas.ucmdb.discovery._1.types.IPRange ranges);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Ranges" element
         */
        com.hp.schemas.ucmdb.discovery._1.types.IPRange insertNewRanges(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Ranges" element
         */
        com.hp.schemas.ucmdb.discovery._1.types.IPRange addNewRanges();
        
        /**
         * Removes the ith "Ranges" element
         */
        void removeRanges(int i);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude newInstance() {
              return (com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.hp.schemas.ucmdb.discovery._1.types.Scope.Exclude) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML Include(@http://schemas.hp.com/ucmdb/discovery/1/types).
     *
     * This is a complex type.
     */
    public interface Include extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Include.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("includef703elemtype");
        
        /**
         * Gets array of all "Ranges" elements
         */
        com.hp.schemas.ucmdb.discovery._1.types.IPRange[] getRangesArray();
        
        /**
         * Gets ith "Ranges" element
         */
        com.hp.schemas.ucmdb.discovery._1.types.IPRange getRangesArray(int i);
        
        /**
         * Returns number of "Ranges" element
         */
        int sizeOfRangesArray();
        
        /**
         * Sets array of all "Ranges" element
         */
        void setRangesArray(com.hp.schemas.ucmdb.discovery._1.types.IPRange[] rangesArray);
        
        /**
         * Sets ith "Ranges" element
         */
        void setRangesArray(int i, com.hp.schemas.ucmdb.discovery._1.types.IPRange ranges);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Ranges" element
         */
        com.hp.schemas.ucmdb.discovery._1.types.IPRange insertNewRanges(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Ranges" element
         */
        com.hp.schemas.ucmdb.discovery._1.types.IPRange addNewRanges();
        
        /**
         * Removes the ith "Ranges" element
         */
        void removeRanges(int i);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.hp.schemas.ucmdb.discovery._1.types.Scope.Include newInstance() {
              return (com.hp.schemas.ucmdb.discovery._1.types.Scope.Include) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.hp.schemas.ucmdb.discovery._1.types.Scope.Include newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.hp.schemas.ucmdb.discovery._1.types.Scope.Include) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope newInstance() {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb.discovery._1.types.Scope parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb.discovery._1.types.Scope) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
