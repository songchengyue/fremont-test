#
# Author: Tony 
# Date: 30 June 2003 
# File: CMS_COMMUNITIES.pm
#
# $Id: CMS_COMMUNITIES.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $

package CMS_COMMUNITIES;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_COMMUNITIES_DB;

sub generate_view {
   # set defaults for search date time
  if (not defined $CMS::html_values{'search_datetime.startyear'}{value}) {
    TI_HTML::set_datetime_search_values
	(\%CMS::html_values, "search_datetime",
	 "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");
  }

  unless ($_[0] eq "nodata") {
    $CMS::html_values{communities_list}{table} = CMS_COMMUNITIES_DB::get_communities_list(@_);

    if (scalar @{$CMS::html_values{communities_list}{table}}) {     
      unshift @{$CMS::html_values{communities_list}{table}},
	["Prefix", "AS Path", "Next Hop", "Date Time (GMT)", "Communities", "Type"];

      $CMS::html_values{communities_list}{header_rows} = 1;
      $CMS::html_values{noresultsmessage}{value} = "";
    } else {
      $CMS::html_values{noresultsmessage}{value} = "No entries.";
    }
  } 

  CMS::output_html("communities_list");
}

sub export_result {
  $CMS::html_values{communities_list}{table} = CMS_COMMUNITIES_DB::get_communities_list(@_);
  unshift @{$CMS::html_values{communities_list}{table}},
	["Prefix", "AS Path", "Next Hop", "Date Time (GMT)", "Communities", "Type"];
  CMS::output_html("communities_export", "csv");
}
  
sub start {
  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} eq "Cancel"))) {

    #default 
    $CMS::html_values{command}{value} = "search_communities_cmd";
    $CMS::html_values{subsystem}{value} = "communities";
    $CMS::html_values{search_bgp_update_type}{options} = CMS_COMMUNITIES_DB::get_bgp_update_type;
    generate_view("nodata");
  } elsif ($CMS::html_values{command}{value} eq "search_communities_cmd") {
     if ($CMS::html_values{submit}{value} eq "Search") {
       $CMS::html_values{command}{value} = "search_communities_cmd";
       $CMS::html_values{subsystem}{value} = "communities";
       $CMS::html_values{search_bgp_update_type}{options} = CMS_COMMUNITIES_DB::get_bgp_update_type;
       generate_view($CMS::html_values{search_network_prefix}{value},
  		   $CMS::html_values{search_as_no}{value},
  		   $CMS::html_values{search_communities}{value},
  		   $CMS::html_values{search_next_hop}{value},
                   $CMS::html_values{search_bgp_update_type}{value},
  		   TI_HTML::get_datetime_search_values(\%CMS::html_values, "search_datetime")
  		  );
     } else {
        CMS::clear_html_values();
        $CMS::html_values{command}{value} = "search_communities_cmd";
        $CMS::html_values{subsystem}{value} = "communities";
        $CMS::html_values{search_bgp_update_type}{options} = CMS_COMMUNITIES_DB::get_bgp_update_type;
        generate_view("nodata");
     }
  } elsif ($CMS::html_values{command}{value} eq "export_result") {
     $CMS::html_values{command}{value} = "export_result";
     $CMS::html_values{subsystem}{value} = "communities";
     export_result($CMS::html_values{search_network_prefix}{value},
		   $CMS::html_values{search_as_no}{value},
		   $CMS::html_values{search_communities}{value},
		   $CMS::html_values{search_next_hop}{value},
                   $CMS::html_values{search_bgp_update_type}{value},
		   TI_HTML::get_datetime_search_values(\%CMS::html_values, "search_datetime")
		  );
  }
}

1;
