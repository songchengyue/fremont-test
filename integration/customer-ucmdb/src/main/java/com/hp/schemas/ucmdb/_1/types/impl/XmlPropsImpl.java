/*
 * XML Type:  XmlProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.XmlProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML XmlProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class XmlPropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.XmlProps
{
    
    public XmlPropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName XMLPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "xmlProp");
    
    
    /**
     * Gets array of all "xmlProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.XmlProp[] getXmlPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(XMLPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.XmlProp[] result = new com.hp.schemas.ucmdb._1.types.XmlProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "xmlProp" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlProp getXmlPropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().find_element_user(XMLPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "xmlProp" element
     */
    public int sizeOfXmlPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(XMLPROP$0);
        }
    }
    
    /**
     * Sets array of all "xmlProp" element
     */
    public void setXmlPropArray(com.hp.schemas.ucmdb._1.types.XmlProp[] xmlPropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(xmlPropArray, XMLPROP$0);
        }
    }
    
    /**
     * Sets ith "xmlProp" element
     */
    public void setXmlPropArray(int i, com.hp.schemas.ucmdb._1.types.XmlProp xmlProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().find_element_user(XMLPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(xmlProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "xmlProp" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlProp insertNewXmlProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().insert_element_user(XMLPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "xmlProp" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlProp addNewXmlProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().add_element_user(XMLPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "xmlProp" element
     */
    public void removeXmlProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(XMLPROP$0, i);
        }
    }
}
