package com.telstra.netops.lookingglass.server.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ClientNodeTestRequestType {
	
	NODE_TEST(21), //
	NODE_HOST_TEST(41), //
	NODE_TRACEROUTE_TEST(31);
	
	private final Integer code;
	
	private ClientNodeTestRequestType(Integer code) {
		this.code = code;
	}

	@JsonValue
	public Integer getCode() {
		return code;
	}
	
}
