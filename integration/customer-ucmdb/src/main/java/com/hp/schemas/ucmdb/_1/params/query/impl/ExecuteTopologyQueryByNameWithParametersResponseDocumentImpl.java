/*
 * An XML document type.
 * Localname: executeTopologyQueryByNameWithParametersResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one executeTopologyQueryByNameWithParametersResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class ExecuteTopologyQueryByNameWithParametersResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponseDocument
{
    
    public ExecuteTopologyQueryByNameWithParametersResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EXECUTETOPOLOGYQUERYBYNAMEWITHPARAMETERSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryByNameWithParametersResponse");
    
    
    /**
     * Gets the "executeTopologyQueryByNameWithParametersResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse getExecuteTopologyQueryByNameWithParametersResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse)get_store().find_element_user(EXECUTETOPOLOGYQUERYBYNAMEWITHPARAMETERSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "executeTopologyQueryByNameWithParametersResponse" element
     */
    public void setExecuteTopologyQueryByNameWithParametersResponse(com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse executeTopologyQueryByNameWithParametersResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse)get_store().find_element_user(EXECUTETOPOLOGYQUERYBYNAMEWITHPARAMETERSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse)get_store().add_element_user(EXECUTETOPOLOGYQUERYBYNAMEWITHPARAMETERSRESPONSE$0);
            }
            target.set(executeTopologyQueryByNameWithParametersResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "executeTopologyQueryByNameWithParametersResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse addNewExecuteTopologyQueryByNameWithParametersResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse)get_store().add_element_user(EXECUTETOPOLOGYQUERYBYNAMEWITHPARAMETERSRESPONSE$0);
            return target;
        }
    }
}
