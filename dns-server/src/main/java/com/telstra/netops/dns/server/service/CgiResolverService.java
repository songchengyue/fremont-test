package com.telstra.netops.dns.server.service;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.dns.server.exception.DnsApplicationGenericBusinessRuleException;
import com.telstra.netops.dns.server.model.ClientDnsResolverRequest;
import com.telstra.netops.dns.server.model.DnsResolverRecord;
import com.telstra.netops.dns.server.model.Success;
import com.telstra.netops.dns.server.model.cgi.CgiDnsRequest;
import com.telstra.netops.dns.server.model.cgi.DnsResolverRecordsResultWrapper;
import com.telstra.netops.dns.server.model.cgi.DnsResultWrapper;
import com.telstra.netops.dns.server.util.JsonSerializerDeserializer;
import com.telstra.oauth2.client.OAuth2TokenResolver;

@Service
@Profile("!local")
public class CgiResolverService implements ResolverService {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CgiResolverService.class);

	@Value("${dns.cgi.url}")
	private String cgiURL;
	
	@Autowired
	private JsonSerializerDeserializer jsonSerializerDeserializer;
	
	@Autowired
    private OAuth2TokenResolver tokenResolver;
	
	@Override
	public Success add(ClientDnsResolverRequest request) {

		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createResolverDnsAdd(request);
		
		CgiDnsRequester<DnsResultWrapper> requester = new CgiDnsRequester<DnsResultWrapper>(DnsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isError()) {
			throw new DnsApplicationGenericBusinessRuleException(wrapper.getErrorMessage());
		}
		
		LOGGER.debug("in create "+wrapper.toString());
		
		return new Success(wrapper.getSuccessMessage());
	}
	
	@Override
	public List<DnsResolverRecord> list(String customerId) {
		
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createResolverDnsList(customerId);
		
		CgiDnsRequester<DnsResolverRecordsResultWrapper> requester = new CgiDnsRequester<DnsResolverRecordsResultWrapper>(DnsResolverRecordsResultWrapper.class, jsonSerializerDeserializer, cgiURL);
		
		DnsResolverRecordsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isEmpty()) {
			return Collections.<DnsResolverRecord>emptyList();
		}
		
		LOGGER.debug("in list "+wrapper.toString());
		
		return wrapper.getResult();
	}
	
	@Override
	public Success remove(ClientDnsResolverRequest request) {
		
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createResolverDnsRemove(request);
		
		CgiDnsRequester<DnsResultWrapper> requester = new CgiDnsRequester<DnsResultWrapper>(DnsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isError()) {
			throw new DnsApplicationGenericBusinessRuleException(wrapper.getErrorMessage());
		}
		
		LOGGER.debug("in cancel CGI Resolver Service "+wrapper.toString());
		
		return new Success(wrapper.getSuccessMessage());
	}

}