/*
 * An XML document type.
 * Localname: getCIsByType
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getCIsByType(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetCIsByTypeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeDocument
{
    
    public GetCIsByTypeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCISBYTYPE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getCIsByType");
    
    
    /**
     * Gets the "getCIsByType" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCIsByType getGetCIsByType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsByType target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByType)get_store().find_element_user(GETCISBYTYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCIsByType" element
     */
    public void setGetCIsByType(com.hp.schemas.ucmdb._1.params.query.GetCIsByType getCIsByType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsByType target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByType)get_store().find_element_user(GETCISBYTYPE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByType)get_store().add_element_user(GETCISBYTYPE$0);
            }
            target.set(getCIsByType);
        }
    }
    
    /**
     * Appends and returns a new empty "getCIsByType" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCIsByType addNewGetCIsByType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsByType target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByType)get_store().add_element_user(GETCISBYTYPE$0);
            return target;
        }
    }
}
