/*
 * XML Type:  getCredentialsEntryResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML getCredentialsEntryResponse(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class GetCredentialsEntryResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse
{
    
    public GetCredentialsEntryResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CREDENTIALSENTRYPARAMETERS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "credentialsEntryParameters");
    
    
    /**
     * Gets the "credentialsEntryParameters" element
     */
    public com.hp.schemas.ucmdb._1.types.CIProperties getCredentialsEntryParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().find_element_user(CREDENTIALSENTRYPARAMETERS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "credentialsEntryParameters" element
     */
    public void setCredentialsEntryParameters(com.hp.schemas.ucmdb._1.types.CIProperties credentialsEntryParameters)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().find_element_user(CREDENTIALSENTRYPARAMETERS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().add_element_user(CREDENTIALSENTRYPARAMETERS$0);
            }
            target.set(credentialsEntryParameters);
        }
    }
    
    /**
     * Appends and returns a new empty "credentialsEntryParameters" element
     */
    public com.hp.schemas.ucmdb._1.types.CIProperties addNewCredentialsEntryParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().add_element_user(CREDENTIALSENTRYPARAMETERS$0);
            return target;
        }
    }
}
