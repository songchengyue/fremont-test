/*
 * XML Type:  RelationNodes
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.RelationNodes
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML RelationNodes(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class RelationNodesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.RelationNodes
{
    
    public RelationNodesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RELATIONNODE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "relationNode");
    
    
    /**
     * Gets array of all "relationNode" elements
     */
    public com.hp.schemas.ucmdb._1.types.RelationNode[] getRelationNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(RELATIONNODE$0, targetList);
            com.hp.schemas.ucmdb._1.types.RelationNode[] result = new com.hp.schemas.ucmdb._1.types.RelationNode[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "relationNode" element
     */
    public com.hp.schemas.ucmdb._1.types.RelationNode getRelationNodeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.RelationNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.RelationNode)get_store().find_element_user(RELATIONNODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "relationNode" element
     */
    public int sizeOfRelationNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RELATIONNODE$0);
        }
    }
    
    /**
     * Sets array of all "relationNode" element
     */
    public void setRelationNodeArray(com.hp.schemas.ucmdb._1.types.RelationNode[] relationNodeArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(relationNodeArray, RELATIONNODE$0);
        }
    }
    
    /**
     * Sets ith "relationNode" element
     */
    public void setRelationNodeArray(int i, com.hp.schemas.ucmdb._1.types.RelationNode relationNode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.RelationNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.RelationNode)get_store().find_element_user(RELATIONNODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(relationNode);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "relationNode" element
     */
    public com.hp.schemas.ucmdb._1.types.RelationNode insertNewRelationNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.RelationNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.RelationNode)get_store().insert_element_user(RELATIONNODE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "relationNode" element
     */
    public com.hp.schemas.ucmdb._1.types.RelationNode addNewRelationNode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.RelationNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.RelationNode)get_store().add_element_user(RELATIONNODE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "relationNode" element
     */
    public void removeRelationNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RELATIONNODE$0, i);
        }
    }
}
