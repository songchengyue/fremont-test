/*
 * XML Type:  DateProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.DateProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML DateProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class DatePropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.DateProps
{
    
    public DatePropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DATEPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "dateProp");
    
    
    /**
     * Gets array of all "dateProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.DateProp[] getDatePropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DATEPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.DateProp[] result = new com.hp.schemas.ucmdb._1.types.DateProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "dateProp" element
     */
    public com.hp.schemas.ucmdb._1.types.DateProp getDatePropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().find_element_user(DATEPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "dateProp" element
     */
    public int sizeOfDatePropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DATEPROP$0);
        }
    }
    
    /**
     * Sets array of all "dateProp" element
     */
    public void setDatePropArray(com.hp.schemas.ucmdb._1.types.DateProp[] datePropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(datePropArray, DATEPROP$0);
        }
    }
    
    /**
     * Sets ith "dateProp" element
     */
    public void setDatePropArray(int i, com.hp.schemas.ucmdb._1.types.DateProp dateProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().find_element_user(DATEPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(dateProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "dateProp" element
     */
    public com.hp.schemas.ucmdb._1.types.DateProp insertNewDateProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().insert_element_user(DATEPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "dateProp" element
     */
    public com.hp.schemas.ucmdb._1.types.DateProp addNewDateProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateProp)get_store().add_element_user(DATEPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "dateProp" element
     */
    public void removeDateProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DATEPROP$0, i);
        }
    }
}
