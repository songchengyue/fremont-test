# (C) Telstra 2009
#
# Author: Meena Prakash 
# Date: 31 March 2009
# File: CMS_PRIDNS_DB.pm
#
# $Id: CMS_PRIDNS_DB.pm,v 1.1.1.1 2003/10/19 13:49:52 rxc Exp $


package CMS_PRIDNS_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
use Mail::Sendmail;

# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;

our $error;

## some queries

# Insert primary dns record types
# Pre  : account_number, contact_email, domain_name, host, record_type_id, record_parameter
# Prost: $result
sub add_pridns_entry {
open (PRE, ">>/tmp/pridns_insideadd");
print PRE "INSIDE\n";
	my $uid     = $dbh->quote(shift @_);
	my $accno   = $dbh->quote(shift @_);
	my $contact = $dbh->quote(shift @_);
	my $domain  = $dbh->quote(shift @_);
	my $host    = $dbh->quote(shift @_);
	my $type_id = shift @_;
	my $parm    = $dbh->quote(shift @_);
	my $result = 0;
print PRE "accno: $accno\t contact: $contact\t domain: $domain\t host: $host\t type_id: $type_id\t parm: $parm\n";

	# Same User not allowed for Primary DNS Service for more than 10 requests.
	 my $sth = $dbh->prepare ("SELECT count(p.username) FROM  pridns p where p.username ilike $uid");
              $sth->execute();
              my $r1 = $sth->fetchall_arrayref;
	print PRE "r1: $r1->[0]->[0]\n";	

	#CRQ000000007324 -Increased the limit to 30
        #if($r1->[0]->[0] >= 10) {
        if($r1->[0]->[0] >= 30) {
	print PRE "ENTERED MAX\n";
    print PRE "User Exceeded Allowed Limit for Primary DNS Service - max. limit is 10\n";
	#return "User Exceeded Allowed Limit for Primary DNS Service - max. limit is 10";
	return 2;
	} else {
        # check for whether the IP exist or not
        $sth = $dbh->prepare ("SELECT count(value) FROM pridns WHERE value ilike $parm  and (status =1 or status=2)");
        $sth->execute();
        #my $r = $sth->fetchrow_hashref;
	my $r2 = $sth->fetchall_arrayref;
        #my $temp_x = $parm;
        #$temp_x =~ s/\'//g;
        #if($$r{value} =~ /^$temp_x$/) {
	print PRE "r2: $r2->[0]->[0]\n";
        if($r2->[0]->[0] >= 1) {
	print PRE "ERROR: Duplicated IP.\n";
                return 3;
	}

	#update in case the value has been deleted earlier
	$sth = $dbh->prepare ("SELECT DISTINCT value FROM pridns WHERE value like $parm and (status =3 or  status=4)");
	$sth->execute();
        my $r = $sth->fetchrow_hashref;
        my $temp_x = $parm;
        $temp_x =~ s/\'//g;
        if($$r{value} =~ /^$temp_x$/) {
	print PRE "inside r loop for update\n";
	print PRE "parm value: $parm\n";
	my $sth1 = $dbh->prepare("UPDATE pridns set status = 1,domain = $domain, host = $host, request_time = CURRENT_TIMESTAMP,contact=$contact,date_last_update=CURRENT_TIMESTAMP WHERE value like $parm");
	print PRE "QUERY: $sth1\n";
        $result = $sth1->execute;
	print PRE "RES: $result\n";
        $error = $dbh->errstr;
	print PRE "error: $error\n";
	close PRE;
	print PRE "update in case of deletion\n";
        return $result;
        }
	$domain = lc($domain);

		# status=1 is pending additions
	
        my $sth = $dbh->prepare
	("INSERT INTO pridns
	(accno, domain, host, type, value
	, request_time, date_assigned, date_last_update, contact, status, username)
	VALUES
	($accno, $domain, $host, $type_id, $parm
	, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $contact, 1, $uid)");

print PRE "QUERY: $sth\n";
	$result = $sth->execute;
print PRE "RES: $result\n";
	$error = $dbh->errstr;
print PRE "error: $error\n";
close PRE;
	return $result;
	}
}
open(DEBUG,">/tmp/CHK_DNS_UPD");
sub update_pridns_entry {
  my $uid = $dbh->quote(shift @_);
  my $accno = $dbh->quote(shift @_);
  my $contact = $dbh->quote(shift @_);
  my $domain_name = $dbh->quote(shift @_);
  my $host    = $dbh->quote(shift @_);
  my $type_id = shift @_;
  my $parm = $dbh->quote(shift @_);
print DEBUG "$uid\t $accno\t $contact\t $domain_name\t $parm\n";
# check for whether the parameter exist or not
        my $sth = $dbh->prepare ("SELECT count(accno) FROM pridns WHERE accno like $accno");
        $sth->execute();
        my $r = $sth->fetchall_arrayref;
   if($r->[0]->[0] >= 1) {

  my $sth = $dbh->prepare
    ("UPDATE pridns
         SET value = $parm,
             status = 1,
             request_time = CURRENT_TIMESTAMP,
             contact = $contact,
             domain = $domain_name,
             host = $host,
             type = $type_id
       WHERE username = $uid AND value = $parm 
         ");

  # 5 is pending changes

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;
 
} else {
return 0; 
}
}
close (DEBUG);

open(DEBUG,">/tmp/CHK_DNSDEL");
#print DEBUG "OUTSIDE PRIDNS delete_pridns_entry\n";
# Delete primary dns record types
# Pre  : db_row_id
# Prost: $result
sub del_pridns_entry {
	my $uid     = $dbh->quote(shift @_);
	my $accno =  $dbh->quote(shift @_);
	my $rid   =  $dbh->quote(shift @_);
print DEBUG "uid: $uid \t accno: $accno \t rid: $rid \n";
	my $sth = $dbh->prepare ("SELECT count(p.username) FROM  pridns p where p.username ilike $uid");
        $sth->execute();
        my $r1 = $sth->fetchall_arrayref;
print DEBUG "sth1: $sth\n";
if($r1->[0]->[0] >= 1) {
	
print DEBUG "uid: $uid \t accno: $accno \t rid: $rid \n";
	my $sth = $dbh->prepare("UPDATE pridns SET status = 3, date_last_update = CURRENT_TIMESTAMP WHERE username = $uid AND oid = $rid");
	
	# status=3 is pending deletions
	
	my $result = $sth->execute;
	$error = $dbh->errstr;
	return $error if ($error);
	return $result;

}else {
print DEBUG "ELSE EXCEEDED\n";
 my $result = 0;
return $result;
}
}
close (DEBUG);

# Get primary dns record list
# Pre  : account_number
# Prost: $result
sub get_pridns_entries_for_account {
	my $uid =  $dbh->quote(shift @_);
	
	my $sth = $dbh->prepare
		("SELECT accno, domain, host, type, value, request_time, date_assigned, date_last_update, contact, status
			, oid
			FROM pridns
			WHERE username = $uid AND status != '3' 
			ORDER BY accno, domain, type, host, value, request_time
		");
	
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	
	my $result = $sth->fetchall_arrayref;
	$error = $dbh->errstr;
	return $result;
}

# Get primary dns record accno by domain name
# Pre  : domain
# Prost: $result
sub get_accno_for_dns_domain {
  my $aip = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
	("SELECT DISTINCT accno
	FROM pridns
	WHERE value = $aip");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

 if (my @result = $sth->fetchrow_array) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result[0];
  } else {
    $error = $dbh->errstr;
    $sth->finish;
    return undef;
  }

}

# Get primary dns record list by db row id
# Pre  : db_row_id
# Prost: $result
sub get_pridns_entries_for_dbrowid {
	my $rid =  $dbh->quote(shift @_);
	
	my $sth = $dbh->prepare
		("SELECT accno, domain, host, type, value, request_time, date_assigned, date_last_update, contact, status
			, oid
			FROM pridns
			WHERE oid = $rid
			ORDER BY accno, domain, type, host, value, request_time
		");
	
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	
	my $result = $sth->fetchall_arrayref;
	$error = $dbh->errstr;
	return $result;
}



# Get account number from db row id 
# Pre  : db_row_id
# Prost: $result
sub get_accno_for_pridns_dbrowid {
	my $rid = $dbh->quote(shift @_);

	my $sth = $dbh->prepare
	("SELECT accno
		FROM pridns
		WHERE oid = $rid");
	
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	
	if (my @result = $sth->fetchrow_array) {
		$error = $dbh->errstr;
		$sth->finish;
		return $result[0];
	} else {
		$error = $dbh->errstr;
		$sth->finish;
		return undef;
	}
}


# Get entry which is Queued for addition or Current
# Pre  : 
# Prost: $result
sub get_pro_pridns_entries {
	my @target_status = (1, 2);  # Queued for addition, Current
	my $cond;
	foreach my $s (@target_status) {
		$cond .= " OR status = $s ";
	}
	$cond =~ s/^ OR //;
	$cond = "($cond)";

	my $sth = $dbh->prepare(
		"SELECT accno, domain, host, type, value, request_time, date_assigned, date_last_update, contact, status 
		, oid
		FROM pridns
		WHERE $cond");
	
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	
	my $result = $sth->fetchall_arrayref;
	$error = $dbh->errstr;
	return $result;
}


sub complete_pridns_entries {
	my $sth = $dbh->prepare("UPDATE pridns set status = 2 
				, date_last_update = CURRENT_TIMESTAMP
				, date_assigned = CURRENT_TIMESTAMP
				where status = 1");
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}   
 
	# $sth = $dbh->prepare("Delete from pridns where status = 3");
	$sth = $dbh->prepare("UPDATE pridns set status = 4 
				, date_last_update = CURRENT_TIMESTAMP
				where status = 3");
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}   
}

# Get primary dns status types
# Pre  : $search_by (0=none, 1=name, 2=id), $val
# Prost: $result
sub get_pridns_status {
        my ($search_by, $val) = @_;
        $search_by = 0 if ($search_by eq '');

        my $cond = ' WHERE ';
        if ($search_by == 1) {
                $cond .= " statustext ilike '$val' ";
        } elsif ($search_by == 2) {
                my $x = $val + 0;
                $cond .= " status = $x ";
        } else {
                $cond = ''; # clear it!
        }

        my $sth = $dbh->prepare
        ("SELECT status, statustext
                FROM pridns_status $cond ORDER BY status, statustext");


        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        $error = $dbh->errstr;
        return $result;
}

# Get primary dns record types
# Pre  : $search_by (0=none, 1=type_name, 2=type_id), $val
# Prost: $result
sub get_pridns_type {
        my ($search_by, $val) = @_;
        $search_by = 0 if ($search_by eq '');

        # reset $serach_by to 0 if not parameter passed
        # if ( ($search_by != 0) && ($val eq '') ) {
        #       $search_by = 0;
        # }

        my $cond = ' WHERE ';
        if ($search_by == 1) {
                $cond .= " type ilike '$val' ";
        } elsif ($search_by == 2) {
                my $x = $val + 0;
                $cond .= " id = $x ";
        } else {
                $cond = ''; # clear it!
        }

        my $sth = $dbh->prepare
        ("SELECT type, id
                FROM pridns_type $cond ORDER BY type, id");


        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        $error = $dbh->errstr;
        return $result;
}

#fetch request_time from DB
sub time_db {
my $temp = "SELECT request_time from pridns";
        my $sth = $dbh->prepare("$temp");
        $sth->execute();
        my $result = $sth->fetchall_arrayref;
        return $result;

}

#fetch all values from DB
sub all_values {
my ($reqtime) = $dbh->quote(shift @_);
my $temp = "SELECT username, accno, contact, domain, host, value, status FROM pridns where  request_time like $reqtime";
my $sth = $dbh->prepare("$temp");
$sth->execute();
my $result = $sth->fetchall_arrayref;
return $result;
}

#mail notification
sub PDNS_mail {
 my ($uid, $accno, $pdns_contact,$pdns_dom,$pdns_host,$pdns_ip,$value_type)= @_;
 my @email_ar = split('^^', $uid);
 my $em_ar = shift(@email_ar);
 my $note = "Note: This email has been automatically generated. Please do not reply to this message as it is unattended.";
 my $message = ("$value_type PRIMARY NAME SERVER Service for following:\n\nUsername:\t$em_ar\n\nEmail:\t\t$pdns_contact\nAccount:\t$accno\nHostname:\t$pdns_host\nDomain:\t\t$pdns_dom\n\nThank you.\n\n$note");
 my $subj = "$value_type PRIMARY NAME SERVER";
  my $to_mail = $pdns_contact;
  #my $cc_mail = "jajati-keshari.samal\@team.telstra.com;Anjan-Babu.Etha\@team.telstra.com;karuna.ballal\@team.telstra.com";
  my $bcc_mail = "TI.DL-GL-TI_EM\@team.telstra.com; TI.infosyscms\@team.telstra.com";
  my $from_mail = "\"Reach OLSS System\" (olss\@TelstraInternational.com)";
  my %mail = (
           To => $to_mail,
           Bcc => $bcc_mail,
           From => $from_mail,
           Subject => $subj
  );
  my $hostname = `hostname`;
  print "$hostname";
#  $mail{Smtp} = 'postoffice.net.reach.com';
   $mail{Smtp} = '$hostname';
  $mail{body} = $message;

  if (sendmail (%mail)) {
    print "\nMail sent success to $to_mail.\n" ;
  } else {
    print "Error sending mail to $to_mail: $Mail::Sendmail::error \n";
  }

}


open (DEB, ">/tmp/dns_primary");
print DEB "ENTER SEARCH CRITERIA\n";
# Search primary dns record
sub search_pridns_entries {
print DEB "ENTERED\n";
  my $accno = shift @_ || "";
  my $domain = shift @_ || "";
  my $primary_ip = shift @_ || "";
  my $status = shift @_ || "";
  my $timestamp_start = shift @_ || "";
  my $timestamp_end = shift @_ || "";

  my $command = "SELECT p.accno, domain, primary_ip,
	c.statustext,p.request_time
	FROM pridns p, secdns s,secdns_status c
	WHERE p.accno = s.accno AND s.status = c.status AND p.status <> 4";

print DEB "accno:$accno\n domain: $domain\n primary_ip: $primary_ip\n status: $status\n timestamp_start: $timestamp_start\n timestamp_end: $timestamp_end\n\n";

  my $extension = "";

  if ($timestamp_start ne "") {
    $extension .= "and p.request_time >= '$timestamp_start'
                   and p.request_time <= '$timestamp_end' ";
  }

  if ($accno ne "") {
    $extension .= "and p.accno ilike '%$accno%' ";
  }

  if ($domain ne "") {
    $extension .= "and domain ilike '%$domain%' ";
  }

  if ($primary_ip ne "") {
    $extension .= "and primary_ip ilike '%$primary_ip%' ";
  }


  if ($status ne "") {
    $extension .= "and p.status = $status ";
  }

  #$extension =~ s/and/where/;
print DEB "extension: $extension\n";
  $command .= "$extension order by p.request_time DESC ";
print DEB "command: $command\n";
  if ($extension eq "") {
    # limit by default
    $command .= "limit 20";
  }
print DEB "command1: $command\n";

  my $sth = $dbh->prepare($command);

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
print DEB "QUERY: $sth\n";
  my $result = $sth->fetchall_arrayref;

$error = $dbh->errstr;
print DEB "INSIDE SEARCH FUNC\n";
print DEB "result val: $result\n";
print DEB "result1: $result->[0]->[0]\n";
close (DEB);

  return $result;

}
1;
