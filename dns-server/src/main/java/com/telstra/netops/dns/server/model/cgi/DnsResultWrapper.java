package com.telstra.netops.dns.server.model.cgi;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DnsResultWrapper {

	@JsonProperty
	private List<DnsResult> result;
	
	@JsonIgnore 
	public boolean isError() {
		return getErrorMessage() != null;
	}
	
	@JsonIgnore 
	public String getErrorMessage() {
		return result.get(0).getErrorMessage();
	}
	
	public void setStatusMessage(String successMessage){
		if(result==null){
			result = new ArrayList<DnsResult>();
		}
		DnsResult resultDns = new DnsResult();
		resultDns.setSuccessMessage(successMessage);
		result.add(resultDns);
		
	}
	
	public void setErrorMessage(String errorMessage){
		if(result==null){
			result = new ArrayList<DnsResult>();
		}
		DnsResult resultDns = new DnsResult();
		resultDns.setErrorMessage(errorMessage);
		result.add(resultDns);
		
	}

	public String getSuccessMessage() {
		return result.get(0).getSuccessMessage();
	}
	
}
