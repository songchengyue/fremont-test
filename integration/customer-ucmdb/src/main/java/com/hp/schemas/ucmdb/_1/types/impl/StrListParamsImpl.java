/*
 * XML Type:  StrListParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.StrListParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML StrListParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class StrListParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.StrListParams
{
    
    public StrListParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STRLISTPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strListParam");
    
    
    /**
     * Gets array of all "strListParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.StrListProp[] getStrListParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(STRLISTPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.StrListProp[] result = new com.hp.schemas.ucmdb._1.types.StrListProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "strListParam" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListProp getStrListParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListProp)get_store().find_element_user(STRLISTPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "strListParam" element
     */
    public int sizeOfStrListParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STRLISTPARAM$0);
        }
    }
    
    /**
     * Sets array of all "strListParam" element
     */
    public void setStrListParamArray(com.hp.schemas.ucmdb._1.types.StrListProp[] strListParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(strListParamArray, STRLISTPARAM$0);
        }
    }
    
    /**
     * Sets ith "strListParam" element
     */
    public void setStrListParamArray(int i, com.hp.schemas.ucmdb._1.types.StrListProp strListParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListProp)get_store().find_element_user(STRLISTPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(strListParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "strListParam" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListProp insertNewStrListParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListProp)get_store().insert_element_user(STRLISTPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "strListParam" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListProp addNewStrListParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListProp)get_store().add_element_user(STRLISTPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "strListParam" element
     */
    public void removeStrListParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STRLISTPARAM$0, i);
        }
    }
}
