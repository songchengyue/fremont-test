/*
 * XML Type:  StrParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.StrParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML StrParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class StrParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.StrParams
{
    
    public StrParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STRPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strParam");
    
    
    /**
     * Gets array of all "strParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.StrProp[] getStrParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(STRPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.StrProp[] result = new com.hp.schemas.ucmdb._1.types.StrProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "strParam" element
     */
    public com.hp.schemas.ucmdb._1.types.StrProp getStrParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().find_element_user(STRPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "strParam" element
     */
    public int sizeOfStrParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STRPARAM$0);
        }
    }
    
    /**
     * Sets array of all "strParam" element
     */
    public void setStrParamArray(com.hp.schemas.ucmdb._1.types.StrProp[] strParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(strParamArray, STRPARAM$0);
        }
    }
    
    /**
     * Sets ith "strParam" element
     */
    public void setStrParamArray(int i, com.hp.schemas.ucmdb._1.types.StrProp strParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().find_element_user(STRPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(strParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "strParam" element
     */
    public com.hp.schemas.ucmdb._1.types.StrProp insertNewStrParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().insert_element_user(STRPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "strParam" element
     */
    public com.hp.schemas.ucmdb._1.types.StrProp addNewStrParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().add_element_user(STRPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "strParam" element
     */
    public void removeStrParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STRPARAM$0, i);
        }
    }
}
