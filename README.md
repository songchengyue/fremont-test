# Telstra NetOps Project

This project contains the following modules:

* [netops-client](netops-client/README.md)
* [lookingglass-server](lookingglass-server/README.md)
* [netops-cgi](netops-cgi/README.md)
* [dns-server](lookingglass-server/README.md)
* [dns-cgi](dns-cgi/README.md)
* [oauth2-server](oauth2-server/README.md)
* [customer-ucmdb](integration/customer-ucmdb/README.md)

This project follows these [Development Guidelines](DevelopmentGuidelines.md)

## How to run Netops from Eclipse

* Import all the above projects into Eclipse
* Create a Tomcat 8 run configuration and add all the above applications to it
* Edit your run configuration and add the following to your VM arguments:
```
-Dspring.profiles.active=local -Dspring.config.location="classpath:,file:${workspace_loc}/netops/site-application-local.yml"
```
* Open a test customer in your browser: http://localhost:8080/netops-client/test

## Configuring Netops

### Setting Up New Sites

If you are running Netops on a new site you must create a site-application-<site>.yml file. Add you site-specific config in there (like database password, oauth username/password etc, see the existing files for other sites to get an idea what you need to set). Then, before you start Tomcat, set JAVA_OPTS as follows:

Linux:
```
export JAVA_OPTS="-Dspring.profiles.active=<site> -Dspring.config.location=classpath:,file:/path/to/site-application-<site>.yml"
```

Windows:
```
set JAVA_OPTS=-Dspring.profiles.active=<site> -Dspring.config.location=classpath:,file:/path/to/site-application-<site>.yml
```

To making the change permanent on RedHat add the option in the /etc/sysconfig/tomcat* file.

### Adding New Configuration

If you are a developer and need to add a new configuration value for injection via the Spring @ Value annotation, then:

* if the value won't change for different sites and all sites have the same value, add it to application.yml
* if the value won't change but all sites have a different value, add it to application-<site>.yml
* if the value is subject to change at runtime, at any time, then add it your the site's site-application-<site>.yml

## Branching Model

### Creating a release

```
# checkout the latest development code
$ mkdir <release-folder>
$ cd <release-folder>
$ cvs -d :pserver:<your-d-number>@oxfred22.in.reach.com:/usr/local/CVS/cvsroot/CVSROOT checkout netops
$ cd netops

# check project is now up to date by checking a sample file
$ cvs status README.md
===================================================================
  File: README.md       Status: Up-to-date

     Working revision:    1.1.1.1
     Repository revision: 1.1.1.1     /usr/local/CVS/CVSROOT/netops/README.md,v
     Sticky Tag:          (none)
     Sticky Date:         (none)
     Sticky Options:      (none)

# test build
$ cd parent
$ mvn package
$ cd ..

# tag the base point of the branch
$ cvs tag -b netops-base-release-1-00

# create branch
$ cvs tag -r netops-base-release-1-00 -b netops-branch-release-1-00

# switch your working directory to be on the branch
$ cvs update -r netops-branch-release-1-00

# check you are on the branch
$ cvs status README.md
===================================================================
  File: README.md       Status: Up-to-date

     Working revision:    1.1.1.1
     Repository revision: 1.1.1.1     /usr/local/CVS/CVSROOT/netops/README.md,v
     Sticky Tag:          netops-branch-release-0-01 (branch: 1.1.1.1.6)
     Sticky Date:         (none)
     Sticky Options:      (none)


# build for release
$ cd parent
$ ./bump-version.bat 1.00
$ cd ..

# confirm changes
$ cvs -nq update
M dns-cgi/pom.xml
M integration/customer-ucmdb/pom.xml
M lookingglass-server/pom.xml
M netops-cgi/pom.xml
M netops-client/pom.xml
M oauth2-server/pom.xml
M parent/pom.xml

$ cvs commit -m "Release 1.00"

# do build
$ cd parent
$ mvn package
```

Now move all *-1.00.[jar|war] release artifacts to folder for release! Test, and release.

### Doing a hotfix

```
# checkout the netops code
$ mkdir <hotfix-folder>
$ cd <hotfix-folder>
$ cvs -d :pserver:<your-d-number>@oxfred22.in.reach.com:/usr/local/CVS/cvsroot/CVSROOT checkout netops
$ cd netops

# switch to the current release branch
$ cvs update -r netops-branch-release-1-00

# do your fix, edit files, test etc

# commit fix
$ cvs commit -m "my hotfix"

# now do the same steps as if you were doing a planned release, create an new branch, update pom.xml files with bump-version.bat etc!


# merge change to main development line for inclusion in the next non-hotfix release

# checkout the netops code
$ mkdir <dev-folder>
$ cd <dev-folder>
$ cvs -d :pserver:<your-d-number>@oxfred22.in.reach.com:/usr/local/CVS/cvsroot/CVSROOT checkout netops
$ cd netops
$ cvs update -j netops-branch-release-1-01 # be careful not to merge pom.xml version updates!

# check what change were merged
$ cvs -nq update

# commit them!
# cvs commit -m "merged from release-1-01"
```

### Future releases

Hopefully we can move this this project Git in the near future and use the [NVIE Branching Model](http://nvie.com/posts/a-successful-git-branching-model/).


