package com.telstra.netops.routemanagement.server.service;

import java.util.List;

import com.telstra.netops.routemanagement.server.model.DeletePrefixRecord;
import com.telstra.netops.routemanagement.server.model.PrefixRecord;
import com.telstra.netops.routemanagement.server.model.PrefixUploadRequest;
import com.telstra.netops.routemanagement.server.model.Success;

public interface PrefixService {

	List<PrefixRecord> list(PrefixUploadRequest request);

	Success remove(DeletePrefixRecord request);

}
