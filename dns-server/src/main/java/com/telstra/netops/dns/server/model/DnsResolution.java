package com.telstra.netops.dns.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DnsResolution {
    	
    @JsonProperty
	private String header;
	
    @JsonProperty("Account")
	private String account;
	
    @JsonProperty("Contact")
	private String contactEmailAddress;
    
    @JsonProperty("IPBlock")
	private String ipBlock;
    
    @JsonProperty("hostname")
	private String hostname;

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getIpBlock() {
		return ipBlock;
	}

	public void setIpBlock(String ipBlock) {
		this.ipBlock = ipBlock;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}


}
