/*
 * An XML document type.
 * Localname: getFilteredCIsByType
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getFilteredCIsByType(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetFilteredCIsByTypeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeDocument
{
    
    public GetFilteredCIsByTypeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETFILTEREDCISBYTYPE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getFilteredCIsByType");
    
    
    /**
     * Gets the "getFilteredCIsByType" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType getGetFilteredCIsByType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType)get_store().find_element_user(GETFILTEREDCISBYTYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getFilteredCIsByType" element
     */
    public void setGetFilteredCIsByType(com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType getFilteredCIsByType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType)get_store().find_element_user(GETFILTEREDCISBYTYPE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType)get_store().add_element_user(GETFILTEREDCISBYTYPE$0);
            }
            target.set(getFilteredCIsByType);
        }
    }
    
    /**
     * Appends and returns a new empty "getFilteredCIsByType" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType addNewGetFilteredCIsByType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType)get_store().add_element_user(GETFILTEREDCISBYTYPE$0);
            return target;
        }
    }
}
