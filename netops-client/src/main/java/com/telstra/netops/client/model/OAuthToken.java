package com.telstra.netops.client.model;

public class OAuthToken {
	
	private String token;
	
	public OAuthToken() {}
	
	public OAuthToken(String token) {
		super();
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
