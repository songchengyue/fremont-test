package com.telstra.netops.lookingglass.server.model.jpa;

import javax.persistence.AttributeConverter;

import com.telstra.netops.lookingglass.server.model.IPVersion;

public class IPVersionConverter implements AttributeConverter<IPVersion, Integer> {

	@Override
	public Integer convertToDatabaseColumn(IPVersion ipVersion) {
		return ipVersion.getValue();
	}

	@Override
	public IPVersion convertToEntityAttribute(Integer value) {
		return IPVersion.findForValue(value);
	}

}
