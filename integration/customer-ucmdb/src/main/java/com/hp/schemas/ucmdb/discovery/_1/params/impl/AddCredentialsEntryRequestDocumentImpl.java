/*
 * An XML document type.
 * Localname: addCredentialsEntryRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one addCredentialsEntryRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class AddCredentialsEntryRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequestDocument
{
    
    public AddCredentialsEntryRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDCREDENTIALSENTRYREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "addCredentialsEntryRequest");
    
    
    /**
     * Gets the "addCredentialsEntryRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequest getAddCredentialsEntryRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequest)get_store().find_element_user(ADDCREDENTIALSENTRYREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "addCredentialsEntryRequest" element
     */
    public void setAddCredentialsEntryRequest(com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequest addCredentialsEntryRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequest)get_store().find_element_user(ADDCREDENTIALSENTRYREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequest)get_store().add_element_user(ADDCREDENTIALSENTRYREQUEST$0);
            }
            target.set(addCredentialsEntryRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "addCredentialsEntryRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequest addNewAddCredentialsEntryRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryRequest)get_store().add_element_user(ADDCREDENTIALSENTRYREQUEST$0);
            return target;
        }
    }
}
