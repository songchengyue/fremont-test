/*
 * XML Type:  XmlParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.XmlParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML XmlParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class XmlParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.XmlParams
{
    
    public XmlParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName XMLPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "xmlParam");
    
    
    /**
     * Gets array of all "xmlParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.XmlProp[] getXmlParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(XMLPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.XmlProp[] result = new com.hp.schemas.ucmdb._1.types.XmlProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "xmlParam" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlProp getXmlParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().find_element_user(XMLPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "xmlParam" element
     */
    public int sizeOfXmlParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(XMLPARAM$0);
        }
    }
    
    /**
     * Sets array of all "xmlParam" element
     */
    public void setXmlParamArray(com.hp.schemas.ucmdb._1.types.XmlProp[] xmlParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(xmlParamArray, XMLPARAM$0);
        }
    }
    
    /**
     * Sets ith "xmlParam" element
     */
    public void setXmlParamArray(int i, com.hp.schemas.ucmdb._1.types.XmlProp xmlParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().find_element_user(XMLPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(xmlParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "xmlParam" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlProp insertNewXmlParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().insert_element_user(XMLPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "xmlParam" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlProp addNewXmlParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().add_element_user(XMLPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "xmlParam" element
     */
    public void removeXmlParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(XMLPARAM$0, i);
        }
    }
}
