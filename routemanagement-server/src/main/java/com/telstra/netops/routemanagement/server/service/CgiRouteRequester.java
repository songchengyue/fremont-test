package com.telstra.netops.routemanagement.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.telstra.netops.routemanagement.server.model.cgi.CgiRouteRequest;
import com.telstra.netops.routemanagement.server.util.JsonSerializerDeserializer;
import com.telstra.oauth2.client.OAuth2TokenResolver;

public class CgiRouteRequester<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(CgiRouteRequester.class);

	private final Class<T> typeParameterClass;
	private final JsonSerializerDeserializer jsonSerializerDeserializer;
	private final String cgiURL;

	public CgiRouteRequester(Class<T> typeParameterClass, JsonSerializerDeserializer jsonSerializerDeserializer,
			String cgiURL) {
		this.typeParameterClass = typeParameterClass;
		this.jsonSerializerDeserializer = jsonSerializerDeserializer;
		this.cgiURL = cgiURL;
	}

	protected T doRequest(CgiRouteRequest cgiRouteRequest, OAuth2TokenResolver tokenResolver) {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.add(getAuthorizationHeaderName(tokenResolver), getAuthorizationHeaderValue(tokenResolver));

		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();

		map.add("POSTDATA", jsonSerializerDeserializer.serialize(cgiRouteRequest));

		HttpEntity<MultiValueMap<String, Object>> httpRequest = new HttpEntity<MultiValueMap<String, Object>>(map,
				headers);

		ResponseEntity<T> responseEntity = restTemplate.postForEntity(cgiURL, httpRequest, typeParameterClass);
		T wrapper = responseEntity.getBody();
		return wrapper;
	}

	private String getAuthorizationHeaderName(OAuth2TokenResolver tokenResolver) {
		return tokenResolver.getAuthorizationHeaderName();
	}

	private String getAuthorizationHeaderValue(OAuth2TokenResolver tokenResolver) {
		return tokenResolver.getAuthorizationHeaderValue();
	}

}
