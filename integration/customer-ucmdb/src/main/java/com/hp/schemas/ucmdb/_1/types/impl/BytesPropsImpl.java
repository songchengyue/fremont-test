/*
 * XML Type:  BytesProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.BytesProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML BytesProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class BytesPropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.BytesProps
{
    
    public BytesPropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName BYTESPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "bytesProp");
    
    
    /**
     * Gets array of all "bytesProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.BytesProp[] getBytesPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(BYTESPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.BytesProp[] result = new com.hp.schemas.ucmdb._1.types.BytesProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "bytesProp" element
     */
    public com.hp.schemas.ucmdb._1.types.BytesProp getBytesPropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesProp)get_store().find_element_user(BYTESPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "bytesProp" element
     */
    public int sizeOfBytesPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BYTESPROP$0);
        }
    }
    
    /**
     * Sets array of all "bytesProp" element
     */
    public void setBytesPropArray(com.hp.schemas.ucmdb._1.types.BytesProp[] bytesPropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(bytesPropArray, BYTESPROP$0);
        }
    }
    
    /**
     * Sets ith "bytesProp" element
     */
    public void setBytesPropArray(int i, com.hp.schemas.ucmdb._1.types.BytesProp bytesProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesProp)get_store().find_element_user(BYTESPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(bytesProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "bytesProp" element
     */
    public com.hp.schemas.ucmdb._1.types.BytesProp insertNewBytesProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesProp)get_store().insert_element_user(BYTESPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "bytesProp" element
     */
    public com.hp.schemas.ucmdb._1.types.BytesProp addNewBytesProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BytesProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BytesProp)get_store().add_element_user(BYTESPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "bytesProp" element
     */
    public void removeBytesProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BYTESPROP$0, i);
        }
    }
}
