/*
 * XML Type:  IntListCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntListCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML IntListCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IntListConditionImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.IntListCondition
{
    
    public IntListConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "condition");
    private static final javax.xml.namespace.QName INTLISTOPERATOR$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intListOperator");
    
    
    /**
     * Gets the "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListProp getCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "condition" element
     */
    public void setCondition(com.hp.schemas.ucmdb._1.types.IntListProp condition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().add_element_user(CONDITION$0);
            }
            target.set(condition);
        }
    }
    
    /**
     * Appends and returns a new empty "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListProp addNewCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().add_element_user(CONDITION$0);
            return target;
        }
    }
    
    /**
     * Gets the "intListOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator.Enum getIntListOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INTLISTOPERATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "intListOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator xgetIntListOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator)get_store().find_element_user(INTLISTOPERATOR$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "intListOperator" element
     */
    public void setIntListOperator(com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator.Enum intListOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INTLISTOPERATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(INTLISTOPERATOR$2);
            }
            target.setEnumValue(intListOperator);
        }
    }
    
    /**
     * Sets (as xml) the "intListOperator" element
     */
    public void xsetIntListOperator(com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator intListOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator)get_store().find_element_user(INTLISTOPERATOR$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator)get_store().add_element_user(INTLISTOPERATOR$2);
            }
            target.set(intListOperator);
        }
    }
    /**
     * An XML intListOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.IntListCondition$IntListOperator.
     */
    public static class IntListOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator
    {
        
        public IntListOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IntListOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
