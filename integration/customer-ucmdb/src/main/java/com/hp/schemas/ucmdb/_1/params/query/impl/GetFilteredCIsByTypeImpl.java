/*
 * XML Type:  getFilteredCIsByType
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML getFilteredCIsByType(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class GetFilteredCIsByTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType
{
    
    public GetFilteredCIsByTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "cmdbContext");
    private static final javax.xml.namespace.QName TYPE$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "type");
    private static final javax.xml.namespace.QName PROPERTIES$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "properties");
    private static final javax.xml.namespace.QName CONDITIONS$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "conditions");
    private static final javax.xml.namespace.QName CONDITIONSLOGICALOPERATOR$8 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "conditionsLogicalOperator");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets the "type" element
     */
    public java.lang.String getType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "type" element
     */
    public org.apache.xmlbeans.XmlString xgetType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "type" element
     */
    public void setType(java.lang.String type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TYPE$2);
            }
            target.setStringValue(type);
        }
    }
    
    /**
     * Sets (as xml) the "type" element
     */
    public void xsetType(org.apache.xmlbeans.XmlString type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TYPE$2);
            }
            target.set(type);
        }
    }
    
    /**
     * Gets the "properties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.CustomProperties getProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.CustomProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.CustomProperties)get_store().find_element_user(PROPERTIES$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "properties" element
     */
    public boolean isSetProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PROPERTIES$4) != 0;
        }
    }
    
    /**
     * Sets the "properties" element
     */
    public void setProperties(com.hp.schemas.ucmdb._1.types.props.CustomProperties properties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.CustomProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.CustomProperties)get_store().find_element_user(PROPERTIES$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.CustomProperties)get_store().add_element_user(PROPERTIES$4);
            }
            target.set(properties);
        }
    }
    
    /**
     * Appends and returns a new empty "properties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.CustomProperties addNewProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.CustomProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.CustomProperties)get_store().add_element_user(PROPERTIES$4);
            return target;
        }
    }
    
    /**
     * Unsets the "properties" element
     */
    public void unsetProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PROPERTIES$4, 0);
        }
    }
    
    /**
     * Gets the "conditions" element
     */
    public com.hp.schemas.ucmdb._1.types.Conditions getConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Conditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.Conditions)get_store().find_element_user(CONDITIONS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "conditions" element
     */
    public void setConditions(com.hp.schemas.ucmdb._1.types.Conditions conditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Conditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.Conditions)get_store().find_element_user(CONDITIONS$6, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.Conditions)get_store().add_element_user(CONDITIONS$6);
            }
            target.set(conditions);
        }
    }
    
    /**
     * Appends and returns a new empty "conditions" element
     */
    public com.hp.schemas.ucmdb._1.types.Conditions addNewConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Conditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.Conditions)get_store().add_element_user(CONDITIONS$6);
            return target;
        }
    }
    
    /**
     * Gets the "conditionsLogicalOperator" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator.Enum getConditionsLogicalOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONDITIONSLOGICALOPERATOR$8, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "conditionsLogicalOperator" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator xgetConditionsLogicalOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator)get_store().find_element_user(CONDITIONSLOGICALOPERATOR$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "conditionsLogicalOperator" element
     */
    public void setConditionsLogicalOperator(com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator.Enum conditionsLogicalOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONDITIONSLOGICALOPERATOR$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONDITIONSLOGICALOPERATOR$8);
            }
            target.setEnumValue(conditionsLogicalOperator);
        }
    }
    
    /**
     * Sets (as xml) the "conditionsLogicalOperator" element
     */
    public void xsetConditionsLogicalOperator(com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator conditionsLogicalOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator)get_store().find_element_user(CONDITIONSLOGICALOPERATOR$8, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator)get_store().add_element_user(CONDITIONSLOGICALOPERATOR$8);
            }
            target.set(conditionsLogicalOperator);
        }
    }
    /**
     * An XML conditionsLogicalOperator(@http://schemas.hp.com/ucmdb/1/params/query).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType$ConditionsLogicalOperator.
     */
    public static class ConditionsLogicalOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator
    {
        
        public ConditionsLogicalOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ConditionsLogicalOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
