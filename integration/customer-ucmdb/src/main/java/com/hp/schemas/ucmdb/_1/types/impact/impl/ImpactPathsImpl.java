/*
 * XML Type:  ImpactPaths
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactPaths
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML ImpactPaths(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ImpactPathsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactPaths
{
    
    public ImpactPathsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTPATH$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "ImpactPath");
    
    
    /**
     * Gets array of all "ImpactPath" elements
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactPath[] getImpactPathArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IMPACTPATH$0, targetList);
            com.hp.schemas.ucmdb._1.types.impact.ImpactPath[] result = new com.hp.schemas.ucmdb._1.types.impact.ImpactPath[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "ImpactPath" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactPath getImpactPathArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactPath target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactPath)get_store().find_element_user(IMPACTPATH$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "ImpactPath" element
     */
    public int sizeOfImpactPathArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IMPACTPATH$0);
        }
    }
    
    /**
     * Sets array of all "ImpactPath" element
     */
    public void setImpactPathArray(com.hp.schemas.ucmdb._1.types.impact.ImpactPath[] impactPathArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(impactPathArray, IMPACTPATH$0);
        }
    }
    
    /**
     * Sets ith "ImpactPath" element
     */
    public void setImpactPathArray(int i, com.hp.schemas.ucmdb._1.types.impact.ImpactPath impactPath)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactPath target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactPath)get_store().find_element_user(IMPACTPATH$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(impactPath);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ImpactPath" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactPath insertNewImpactPath(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactPath target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactPath)get_store().insert_element_user(IMPACTPATH$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ImpactPath" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactPath addNewImpactPath()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactPath target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactPath)get_store().add_element_user(IMPACTPATH$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "ImpactPath" element
     */
    public void removeImpactPath(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IMPACTPATH$0, i);
        }
    }
}
