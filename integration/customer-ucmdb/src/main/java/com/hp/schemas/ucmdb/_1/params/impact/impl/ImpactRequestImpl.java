/*
 * XML Type:  impactRequest
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.ImpactRequest
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * An XML impactRequest(@http://schemas.hp.com/ucmdb/1/params/impact).
 *
 * This is a complex type.
 */
public class ImpactRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.ImpactRequest
{
    
    public ImpactRequestImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTCATEGORY$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "impactCategory");
    private static final javax.xml.namespace.QName IDS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "IDs");
    private static final javax.xml.namespace.QName IMPACTRULENAMES$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "ImpactRuleNames");
    private static final javax.xml.namespace.QName SEVERITY$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "severity");
    
    
    /**
     * Gets the "impactCategory" element
     */
    public java.lang.String getImpactCategory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IMPACTCATEGORY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "impactCategory" element
     */
    public org.apache.xmlbeans.XmlString xgetImpactCategory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IMPACTCATEGORY$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "impactCategory" element
     */
    public void setImpactCategory(java.lang.String impactCategory)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IMPACTCATEGORY$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IMPACTCATEGORY$0);
            }
            target.setStringValue(impactCategory);
        }
    }
    
    /**
     * Sets (as xml) the "impactCategory" element
     */
    public void xsetImpactCategory(org.apache.xmlbeans.XmlString impactCategory)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(IMPACTCATEGORY$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(IMPACTCATEGORY$0);
            }
            target.set(impactCategory);
        }
    }
    
    /**
     * Gets the "IDs" element
     */
    public com.hp.schemas.ucmdb._1.types.IDs getIDs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().find_element_user(IDS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "IDs" element
     */
    public void setIDs(com.hp.schemas.ucmdb._1.types.IDs iDs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().find_element_user(IDS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().add_element_user(IDS$2);
            }
            target.set(iDs);
        }
    }
    
    /**
     * Appends and returns a new empty "IDs" element
     */
    public com.hp.schemas.ucmdb._1.types.IDs addNewIDs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IDs target = null;
            target = (com.hp.schemas.ucmdb._1.types.IDs)get_store().add_element_user(IDS$2);
            return target;
        }
    }
    
    /**
     * Gets the "ImpactRuleNames" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames getImpactRuleNames()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames)get_store().find_element_user(IMPACTRULENAMES$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ImpactRuleNames" element
     */
    public void setImpactRuleNames(com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames impactRuleNames)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames)get_store().find_element_user(IMPACTRULENAMES$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames)get_store().add_element_user(IMPACTRULENAMES$4);
            }
            target.set(impactRuleNames);
        }
    }
    
    /**
     * Appends and returns a new empty "ImpactRuleNames" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames addNewImpactRuleNames()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNamesDocument.ImpactRuleNames)get_store().add_element_user(IMPACTRULENAMES$4);
            return target;
        }
    }
    
    /**
     * Gets the "severity" element
     */
    public int getSeverity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SEVERITY$6, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "severity" element
     */
    public org.apache.xmlbeans.XmlInt xgetSeverity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(SEVERITY$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "severity" element
     */
    public void setSeverity(int severity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SEVERITY$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SEVERITY$6);
            }
            target.setIntValue(severity);
        }
    }
    
    /**
     * Sets (as xml) the "severity" element
     */
    public void xsetSeverity(org.apache.xmlbeans.XmlInt severity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(SEVERITY$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(SEVERITY$6);
            }
            target.set(severity);
        }
    }
}
