package com.telstra.netops.routemanagement.server.model.cgi;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telstra.netops.routemanagement.server.model.DeletePrefixRecord;
import com.telstra.netops.routemanagement.server.model.PrefixUploadRequest;

/**
 * Requester class which creates request object for Single Add, Bulk upload,View
 * and Delete
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CgiRouteRequest {

	private static final Logger LOGGER = LoggerFactory.getLogger(CgiRouteRequest.class);

	private static final String SUBMIT = "SUBMIT";
	private static final String SingleUpload = "Single";
	private static final String BulkUpload = "Bulk";
	private static final String DELETE = "DELETE";

	@JsonProperty
	private Integer level;

	@JsonProperty("customerId")
	private String customerId;

	@JsonProperty("svc")
	private String service;

	@JsonProperty("et")
	private String product;

	@JsonProperty
	private String prefix;

	@JsonProperty("maintained_by")
	private String maintainedBy;

	@JsonProperty("customer_origin")
	private String origin;

	@JsonProperty("email_id")
	private String emailId;

	@JsonProperty("submit")
	private String submit;

	@JsonProperty("ip_version")
	private String ipVersion;

	@JsonProperty("input_file")
	private String fileContent;

	@JsonProperty()
	private String disclaimer;

	@JsonProperty("route")
	private String route;

	@JsonProperty("action")
	private String action;

	@JsonProperty("time_entered")
	private String timeEntered;

	@JsonProperty("time_completed")
	private String timeCompleted;

	@JsonProperty("status")
	private String status;

	@JsonProperty("as_no")
	private String asNo;

	@JsonProperty("track_no")
	private String trackNo;

	@JsonProperty("dbRowId")
	private String dbRowId;

	@JsonProperty("role")
	private String userProfile;

	/**
	 * Creates the request object for Single prefix Add
	 * 
	 * @param prefixUploadRequest
	 * @return
	 */
	public static CgiRouteRequest createSinglePrefixUploadRecords(PrefixUploadRequest prefixUploadRequest) {
		CgiRouteRequest request = new CgiRouteRequest();

		LOGGER.debug("in goig to create request objects createSinglePrefixUploadRecords ");
		request.setLevel(RouteManagementRequestType.SINGLE.getValue());
		request.setService(prefixUploadRequest.getService());
		request.setPrefix(SingleUpload);
		request.setRoute(prefixUploadRequest.getNetworkprefix());
		request.setMaintainedBy(prefixUploadRequest.getMaintainedby());
		request.setOrigin(prefixUploadRequest.getOrigin());
		request.setEmailId(prefixUploadRequest.getEmailId());
		request.setUserProfile(prefixUploadRequest.getUserProfile());
		request.setSubmit(SUBMIT);
		request.setCustomerId(prefixUploadRequest.getCustomerId());
		request.setIpVersion(prefixUploadRequest.getIpVersion());
		LOGGER.debug("in createSinglePrefixUploadRecords ");
		return request;
	}

	/**
	 * Creates the request object for Bulk prefix upload
	 * 
	 * @param prefixUploadRequest
	 * @return
	 */
	public static CgiRouteRequest createMultiplePrefixUploadRecords(PrefixUploadRequest prefixUploadRequest) {
		CgiRouteRequest request = new CgiRouteRequest();

		LOGGER.debug("in Multiple Prefix Upload ");
		request.setLevel(RouteManagementRequestType.BULK.getValue());
		request.setService(prefixUploadRequest.getService());
		request.setFileContent(prefixUploadRequest.getFileContent());
		request.setRoute(prefixUploadRequest.getRoute());
		request.setMaintainedBy(prefixUploadRequest.getMaintainedby());
		request.setOrigin(prefixUploadRequest.getOrigin());
		request.setEmailId(prefixUploadRequest.getEmailId());
		request.setUserProfile(prefixUploadRequest.getUserProfile());
		request.setSubmit(SUBMIT);
		request.setPrefix(BulkUpload);
		request.setIpVersion(prefixUploadRequest.getIpVersion());
		request.setCustomerId(prefixUploadRequest.getCustomerId());
		LOGGER.debug("in createMultiplePrefixUploadRecords ");
		return request;
	}

	/**
	 * Creates the request object for view prefix records
	 * 
	 * @param customerId
	 * @return
	 */
	public static CgiRouteRequest createViewPrefixRecords(PrefixUploadRequest prefixRequest) {
		System.out.println("PINKYY44" + prefixRequest.getService());
		CgiRouteRequest request = new CgiRouteRequest();
		LOGGER.debug("in goig to create request objects createViewPrefixRecords ");
		request.setLevel(RouteManagementRequestType.VIEW.getValue());
		request.setService(prefixRequest.getService());
		request.setCustomerId(prefixRequest.getCustomerId());
		request.setService(prefixRequest.getService());
		LOGGER.debug("in createViewPrefixRecords ");
		return request;
	}

	/**
	 * Creates the request object for deleting prefix records
	 * 
	 * @param request2
	 * @return
	 */
	public static CgiRouteRequest deleteViewPrefixRecords(DeletePrefixRecord request2) {
		CgiRouteRequest request = new CgiRouteRequest();
		System.out.println("PINKYYY3" + request2.getService());

		LOGGER.debug("Going to delete request objects");
		request.setCustomerId(request2.getCustomerId());
		request.setSubmit(CgiRouteRequest.DELETE);
		request.setLevel(RouteManagementRequestType.DELETE.getValue());
		request.setService(request2.getService());
		String records[] = request2.getIds();
		String route = Arrays.toString(records);
		route = route.substring(1, route.length() - 1);
		request.setDbRowId(route);
		LOGGER.debug("in deleteViewPrefixRecords ");
		return request;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getMaintainedBy() {
		return maintainedBy;
	}

	public void setMaintainedBy(String maintainedBy) {
		this.maintainedBy = maintainedBy;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(String ipVersion) {
		this.ipVersion = ipVersion;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public String getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getTimeEntered() {
		return timeEntered;
	}

	public void setTimeEntered(String timeEntered) {
		this.timeEntered = timeEntered;
	}

	public String getTimeCompleted() {
		return timeCompleted;
	}

	public void setTime_completed(String timeCompleted) {
		this.timeCompleted = timeCompleted;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAsNo() {
		return asNo;
	}

	public void setAs_no(String asNo) {
		this.asNo = asNo;
	}

	public String getTrackNo() {
		return trackNo;
	}

	public void setTrack_no(String trackNo) {
		this.trackNo = trackNo;
	}

	public String getDbRowId() {
		return dbRowId;
	}

	public void setDbRowId(String dbRowId) {
		this.dbRowId = dbRowId;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
}
