#!/usr/bin/perl
#
# Module : OS_REPORTS_DB.pm
# Desc   : DB Module for OLSS reporting system 
# Last Update : 20030212 by Leo
# Changes:-
#

package OS_REPORTS_DB;
use Exporter;

use strict;
use warnings;
use DBI;
use OS_REPORTS;
use OS_PARA;

my $debug = 0; # 0 - normal mode, 1 - debug mode

our @ISA        = qw(Exporter);
#our @EXPORT     = qw(@cms_atm_sids $atm_sid_cnt @atm_sids);
our @EXPORT     = qw($dbh);

#our (@cms_atm_sids, $atm_sid_cnt, @atm_sids);
our $dbh;
our $error;
my $dbname="cms";
my $dbuser="cms";
my $tgwr = "$OS_PARA::values{tgwrpath}{value}";
my $dbroot = $tgwr."dbase/";
my $svclabeldb = $dbroot."svclabel";

use constant SUCCESS => 1;
use constant FAIL => 0;

########################### OUTPUT CODE ##############################


sub hello {
        print "Hello from OS_REPORTS_DB.pm!\n";
}  # end of sub hello


# connects to database
sub connect_to_database {
  if (defined ($dbh)) {
    return FAIL;
  } else {
    $dbh = DBI->connect("dbi:Pg:dbname=$dbname",$dbuser);
    my $sth = $dbh->prepare("SET TIME ZONE 'UTC'");
    $sth->execute;
    return SUCCESS;
  }
}  # end of sub connect_to_database


# disconnect from database
sub disconnect_from_database {
  $dbh->disconnect;
  undef $dbh;
}  # end of sub disconnect_from_database


# get product code by service id
sub get_productcode_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT productcodeid FROM service
				WHERE serviceid = $sid");
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

	my $result = $sth->fetchrow_hashref;
	if ($result) {
		$error = $dbh->errstr;
		$sth->finish;
		return $result;
        } else {
		$error = $dbh->errstr;
		return 0;
	}
}


# get source and destination switch code by service id
# Pre : service id
# Post : switch source, switch destination
sub get_switchcode_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT switchsourcodeid, switchdestcodeid FROM service
				WHERE serviceid = $sid");
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

	my $result = $sth->fetchrow_hashref;
	#local_debug( "($sid)", $result );
	if ($result) {
		$error = $dbh->errstr;
		$sth->finish;
		return $result;
        } else {
		$error = $dbh->errstr;
		return 0;
	}
	#return "CKA2","ZAA2";  # for debug use only
}


# get link by serviceid
# Pre : serviceid
# Post : linkid, serviceid, type, routername, interface, bw, description
sub get_link_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT linkid, serviceid, type, routername, interface, bw, description FROM link 
				NATURAL JOIN link_type NATURAL JOIN routername WHERE serviceid = $sid");

	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

	my $result = $sth->fetchrow_hashref;
	if ($result) {
		$error = $dbh->errstr;
		$sth->finish;
		return $result;
        } else {
		$error = $dbh->errstr;
		return 0;
	}	
}

# Added by Prashant on 06/0ct/09 for mVPN N/W CLI
# is mVPN enabled for the service
# Pre : serviceid
# Post : mcast_option ("1" for enabled and "2" for disabled) 
sub get_mcast_enabled {
        my $sid = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT mcast_option FROM service WHERE serviceid = $sid");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


#End of: Added by Prashant on 06/0ct/09 for mVPN N/W CLI

# get link by routername and interface
# Pre : routername, interface
# Post : linkid, serviceid, type, routername, interface, bw, description
sub get_link_routername_interface {
	my $rtr = shift @_;
	my $int = shift @_;

	$rtr =~ s/.net.reach.com//i;
	$int =~ s/-aal5_layer//i;

	my $routername = $dbh->quote($rtr);
	my $alt_routername = $dbh->quote($rtr.".net.reach.com");
	my $interface = $dbh->quote($int);
        my $interface_vpls = $dbh->quote($int."\%");
	my $alt_interface = $dbh->quote($int."-aal5_layer");
	my $sth = "";

	if($rtr !~ /50$/){
		$sth = $dbh->prepare("SELECT linkid, serviceid, type, routername, interface, bw, description FROM link 
				NATURAL JOIN link_type NATURAL JOIN routername WHERE 
				(routername ilike $routername OR routername ilike $alt_routername) AND 
				(interface ilike $interface OR interface ilike $alt_interface)");
        }
        else {
       		$sth = $dbh->prepare("SELECT linkid, serviceid, type, routername, interface, bw, description FROM link
                                NATURAL JOIN link_type NATURAL JOIN routername WHERE
                                (routername ilike $routername OR routername ilike $alt_routername) AND
                                interface ilike $interface_vpls");
        }

	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	my $result = $sth->fetchrow_hashref;
	if ($result) {
		$error = $dbh->errstr;
		$sth->finish;
		return $result;
	} else {
		$error = $dbh->errstr;
		return 0;
	}	
}


# get routers location 
# Pre : serviceid
# Post : routername, popname, cityname, regionname
sub get_routers_location {
	my $sth = $dbh->prepare("select routername.routername, popname.popname, cityname.cityname,regionname.regionname from routername, popname, cityname, regionname where routername.popid = popname.popid and popname.cityid = cityname.cityid and cityname.regionid = regionname.regionid order by routername");

	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


# get city name by switch code
# Pre : switch code
# Post : switch code, city id, city name
sub get_city_switchcode {
        my $sid = $dbh->quote(shift @_);
        my (@data, $result);
        my ($switchcode, $cityid, $cityname);
        $switchcode = $cityid = $cityname = 'FAIL';
        #my $sth = $dbh->prepare("SELECT switchsourcodeid, switchdestcodeid FROM service
        #                       WHERE serviceid = $sid");
        my $sth = $dbh->prepare("SELECT switchcodeid, switchcode.cityid, cityname
                                FROM switchcode LEFT OUTER JOIN cityname
                                ON (switchcode.cityid = cityname.cityid)
                                WHERE switchcodeid = $sid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        while (@data = $sth->fetchrow_array() ) {
                $switchcode = $data[0];
                $cityid   = $data[1];
                $cityname = $data[2];
        }
        $error = $dbh->errstr;
        $sth->finish;
        $result = {
                'switchcode' => "$switchcode",
                'cityid'     => "$cityid",
                'cityname'   => "$cityname"
        };
        return $result;

##      my $result = $sth->fetchrow_hashref;
##      #local_debug( "($sid)", $result );
##      if ($result) {
##              $error = $dbh->errstr;
##              $sth->finish;
##              return $result;
##      } else {
##              $error = $dbh->errstr;
##              return 0;
##      }
}


# get switch code by fnn
# Pre : fnn
# Post : switch code, city id, city name, fnnmatch, fnntype
sub get_switchcity_fnn {
	#my $fnn = $dbh->quote(shift @_);
	#my $sth = $dbh->prepare("SELECT switchsourcodeid, switchdestcodeid FROM service
	#			WHERE serviceid = $sid");

	my $fnn = shift @_;
	my $type = shift @_;

	if ($type =~ /^ATM/i) {
		##$fnn = substr($fnn,5,4);
		$fnn = substr($fnn,4,5);  # get 5 chars starting from 5th char
		$fnn =~ s/ //g;
	}

	my (@data, $result, $fnnmatch, $fnntype);
	my $found = 0;
	my ($switchcode, $cityid, $cityname);
	$switchcode = $cityid = $cityname = 'FAIL';

	my $query = "select switchcodeid, switchcode.cityid, cityname, fnn, fnntype  
			from switchcode left outer join cityname on (switchcode.cityid = cityname.cityid) 
			where fnn is not null and fnntype like '$type%' 
			order by fnntype, fnn";
	my $sth = $dbh->prepare("$query");
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	while (@data = $sth->fetchrow_array() ) {
		$fnnmatch = $data[3];
		$fnntype  = $data[4];
		#$fnnpre =~ s/^ZZZ//;
		if ($fnn =~ /^$fnnmatch/) {
			$found = 1;
			$switchcode = $data[0];
			$cityid     = $data[1];
			$cityname   = $data[2];
			last;
		}
	}
	$error = $dbh->errstr;
	$sth->finish;
	if ($found) {
		$result = {
			'switchcode'	=> "$switchcode",
			'cityid'	=> "$cityid",
			'cityname'	=> "$cityname",
			'fnnmatch'	=> "$fnnmatch",
			'fnntype'	=> "$fnntype"
		};
		return $result;
	} else {
		$result = {
			'switchcode'	=> "FAIL"
		};
		return $result;
	}

##	my $result = $sth->fetchrow_hashref;
##	#local_debug( "($sid)", $result );
##	if ($result) {
##		$error = $dbh->errstr;
##		$sth->finish;
##		return $result;
##        } else {
##		$error = $dbh->errstr;
##		return 0;
##	}
##	#return "CKA2","ZAA2";  # for debug use only
}


# get label by service id
# Pre : service id
# Post : label
sub get_label_service {
	my $sid = shift @_;
	my @label = dbmread($sid, $svclabeldb);

	if ($label[0]) {
		return $label[0];
	}

	return 0;

##	my $sid = $dbh->quote(shift @_);
##	my $sth = $dbh->prepare("SELECT switchcodeid, switchcode.cityid, cityname 
##				FROM switchcode LEFT OUTER JOIN cityname
##				ON (switchcode.cityid = cityname.cityid)
##				WHERE switchcodeid = $sid");
##	unless ($sth->execute) {
##		$error = $dbh->errstr;
##		return 0;
##	}
##
##	my $result = $sth->fetchrow_hashref;
##	#local_debug( "($sid)", $result );
##	if ($result) {
##		$error = $dbh->errstr;
##		$sth->finish;
##		return $result;
##        } else {
##		$error = $dbh->errstr;
##		return 0;
##	}
}


# Read dbm file
# Pre : service id, dbm file 
# Post : 0 (fail), @vals(ok)
sub dbmread {
	my ($handle, $table) = @_;
	#my $sid = shift @_;

	my %cc;
	dbmopen(%cc, $table, undef) or return 0;
	if(not exists $cc{$handle}) {
		dbmclose(%cc);
		return 0;
	}
	my @x = split(/\^\^/, $cc{$handle});
	dbmclose(%cc);

	return @x;
}


# get ATM SLA data from db
# date in uxtime format
# output format:
# uxtime,key,logdate(dd/mm/yyyy),dailytxcell,dailyrxcell,dailyclr,dailymaxctd,dailyminctd,dailyavgctd,dailyavtime,dailytotaltime,dailyav
sub get_atm_sla {
	my ($sdate, $edate, $sid, $type) = @_;
	my ($indir, @out);
	my $cnt = 0;
	print "OS_REPORTS_DB/get_atm_sla receive ($sdate, $edate, $sid)\n" if $debug;

	if ($type =~ /^daily$/i ) {
		$indir = $tgwr.'dbase/ATM/dailysla/';
	} else {
		$indir = $tgwr.'dbase/ATM/hourlysla/';
	}

	my $infile = $indir.$sid;
	my @tt;
	my $tfmt;
	print "infile=$infile\n" if $debug;
	if (-e $infile) {
		open (IN, "$infile");
			while(my $line = <IN>) {
				chomp($line);
				# get first element (uxtime) to compare
				if (($line =~ /(.*?)\t/)&&($1 >= $sdate)) {
					if (($line =~ /(.*?)\t/)&&($1 <= $edate)) {
						$cnt++;

						@tt = split(/\t/, $line);

						# DDR expressed as %, 999.999999999 format
						$tfmt = '%11.8f';
						$tt[5]  = sprintf("$tfmt",$tt[5]); # a to b g
						$tt[8]  = sprintf("$tfmt",$tt[8]); # a to b ng
						$tt[11] = sprintf("$tfmt",$tt[11]); # b to a g
						$tt[14] = sprintf("$tfmt",$tt[14]); # b to a ng

						# change CTD unit from microsecond to millisecond
						$tt[15] /= 1000;
						$tt[16] /= 1000;
						$tt[17] /= 1000;

						# AV expressed as %, 999.9999 format
						$tfmt = '%7.4f';
						$tt[20] = sprintf("$tfmt",$tt[20]);

						$line = join ("\t", @tt);
						push (@out, $line);
					}
				}
			}
		close (IN);
	} else { # sid not match
		push (@out, "FAIL");
		push (@out, "Cannot extract report for $sid.");
		return @out;
	}
	if ($cnt == 0) { # no record match
		push (@out, "FAIL");
		push (@out, "No data found in this period for $sid.");
	}
	return @out;
}  # end of sub get_atm_sla

# GBS 
# date in uxtime format
# Richard Chew, rchew@maettr.bpa.nu 25/12/2005
sub get_gbs_sla {
	my ($rtype, $sdate, $edate, $sid) = @_;

	my $indir = $tgwr.'dbase/GBS/';
	my $infile = $indir;

	my $label;

open(my $in, ">>/tmp/xxrchew");
print $in "get_gbs_sla ($rtype) ";

	$sid =~ tr/A-Z/a-z/;

	SWITCH: {
		($rtype =~ /mthlyavail/i) && do {
			print $in "XX ($rtype)($1)\n";
			$infile .= "mthlyavail" . "/$sid";
			$label = "Monthly Availability";
			last;
		};
                ($rtype =~ /mthlyperf/i) && do {
			$infile .= "mthlyperf" . "/$sid";
			$label = "Monthly Performance";
			last;
                };
                ($rtype =~ /biperf/i) && do {
			$infile .= "biperf" . "/$sid";
			$label = "Bi-weekly Performance";
			last;
                };
                ($rtype =~ /weeklyperf/i) && do {
			$infile .= "weeklyperf" . "/$sid";
			$label = "Weekly Performance";
			last;
                };
                ($rtype =~ /dailyperf/i) && do {
			$infile .= "dailyperf" . "/$sid";
			$label = "Daily Performance";
			last;
                };
                ($rtype =~ /hourlyperf/i) && do {
			$infile .= "hourlyperf" . "/$sid";
			$label = "Hourly Performance";
			last;
                };
                ($rtype =~ /15minperf|5min/i) && do {
			$infile .= "15minperf" . "/$sid";
			$label = "15 Minute Performance";
			last;
                };
	};

	my @out;

print $in "($infile) ($rtype)\n";

	my ($yr, $mth, $day, $hr, $min, $sec) = OS_REPORTS::get_datetime($sdate);
	$mth = substr( '0'.$mth, -2, 2);
	$day = substr( '0'.$day, -2, 2);
	$hr = substr( '0'.$hr, -2, 2);
	$min = substr( '0'.$min, -2, 2);
	$sec = substr( '0'.$sec, -2, 2);

	my $s1date = "$yr$mth$day$hr$min$sec";

	($yr, $mth, $day, $hr, $min, $sec) = OS_REPORTS::get_datetime($edate);
	$mth = substr( '0'.$mth, -2, 2);
        $day = substr( '0'.$day, -2, 2);
	$hr = substr( '0'.$hr, -2, 2);
        $min = substr( '0'.$min, -2, 2);
        $sec = substr( '0'.$sec, -2, 2);

	my $e1date = "$yr$mth$day$hr$min$sec";

print $in "s ($s1date) - e ($e1date)\n";

	#if( (-e $infile) && ($infile =~ /mthlyavail/i) ) {
	if(-e $infile) {
		open(my $inf, "$infile");
		while(my $line = <$inf>) {
			chomp($line);
			my($dt) = (split(/\t/, $line))[0];
			if( ($dt >= $s1date) && ($dt <= $e1date) ) {
				push (@out, $line);
			}
		}
		close($inf);
	}
close($in);

	push @out, $label;
	
	return @out;

}


# get FR AVAIL data from db
# date in uxtime format
# output format:
# uxtime,key,logdate(dd/mm/yyyy),dailyoutage
sub get_fr_ar {
        my ($sdate, $edate, $sid) = @_;
        my (@out, @tout);
	my $cnt = 0;
	my %data = ();

        my $indir = $tgwr.'dbase/FR/dailyar/';
        my $infile = $indir.$sid;
        print "infile=$infile\n" if $debug;
        if (-e $infile) {
                open (IN, "$infile");
                        while(my $line = <IN>) {
                                chomp($line);
                                # get first element (uxtime) to compare
                                if (($line =~ /(.*?)\t/)&&($1 >= $sdate)) {
                                        if (($line =~ /(.*?)\t/)&&($1 <= $edate)) {
						$cnt++;
                                                push (@tout, $line);
						$data{(split(/\t/,$line))[0]} = $line;
                                        }
                                }
                        }
                close (IN);
	}

	my $i=$sdate;
	my ($yr, $mth, $day, $hr, $min, $sec);
	while ($i <= $edate) {
		if ($data{$i}) {
			push @out, $data{$i};
		} else {
			($yr, $mth, $day, $hr, $min, $sec) = OS_REPORTS::get_datetime($i);
			$mth = substr( '0'.$mth, -2, 2);
			$day = substr( '0'.$day, -2, 2);
			push @out, "$i\t$sid\t$day/$mth/$yr\t0";
		} 
		$i += 86400;  # advance one day
	}
	#local_debug( @tout, "|", @out );
#	} else { # sid not match
#		push (@out, "FAIL");
#		push (@out, "Cannot extract report for $sid.");
#		return @out;
#	}
#	if ($cnt == 0) { # no record match
#		push (@out, "FAIL");
#		push (@out, "No data found in this period for $sid.");
#	}
        return @out;
}  # end of sub get_fr_ar


# get FR round trip delay data from db
# date in uxtime format
# output format:
# uxtime,key,logdate(dd/mm/yyyy),dailymaxdelay,dailymindelay,dailyavgdelay
sub get_fr_delay {
        my ($sdate, $edate, $sid) = @_;
        my @out;
	my $cnt = 0;

open (OUT, ">/tmp/leo-debug~OS_REPORT_DB.pm~get_fr_delay");
print OUT "sid($sid)\n";

	my @t = split(/-/, $sid);

print OUT "t(@t)\n";

	my $ttype = ($t[1] =~ /\d+\.\d+/) ? 'ATM' : 'FR';
	my ($switch_s, $cityid_s, $cityname_s, $fnnmatch_s, $fnntype_s) = OS_REPORTS::get_switchcity_by_fnn($t[0], $ttype);

print OUT "t[0]($t[0]) switch_s($switch_s) cityid_s($cityid_s) cityname_s($cityname_s) fnnmatch_s($fnnmatch_s) fnntype_s($fnntype_s)\n";

	$ttype = ($t[3] =~ /\d+\.\d+/) ? 'ATM' : 'FR';
	my ($switch_d, $cityid_d, $cityname_d, $fnnmatch_d, $fnntype_d) = OS_REPORTS::get_switchcity_by_fnn($t[2], $ttype);

print OUT "t[2]($t[2]) switch_d($switch_d) cityid_d($cityid_d) cityname_d($cityname_d) fnnmatch_d($fnnmatch_d) fnntype_d($fnntype_d)\n";
	
	#my ($switch_s, $switch_d) = OS_REPORTS::get_switchcode_by_serviceid( $sid );
	#local_debug("($switch_s) ($switch_d)");

	if ($switch_s =~ /^FAIL$/i) {
		push (@out, "FAIL");
		push (@out, "Cannot extract switch info for $sid.");
		return @out;
	}


	#my $switchkey = "$switch_s"."-"."$switch_d";
	my @switchkeys = ("$switch_s"."-"."$switch_d", "$switch_d"."-"."$switch_s");
        #my $indir = $tgwr.'dbase/FR/dailydelay/';
        my $indir = $tgwr.'dbase/FR/hourlydelay/';
	my $keyfound = 0;

	foreach my $switchkey (@switchkeys) { 
	        my $infile = $indir.$switchkey;
		#local_debug( "infile=$infile" );
	        if (-e $infile) {
	                open (IN, "$infile");
	                        while(my $line = <IN>) {
	                                chomp($line);
	                                # get first element (uxtime) to compare
	                                if (($line =~ /(.*?)\t/)&&($1 >= $sdate)) {
	                                        if (($line =~ /(.*?)\t/)&&($1 <= $edate)) {
							$cnt++;
	                                                push (@out, $line);
	                                        }
	                                }
	                        }
	                close (IN);
			$keyfound = 1;
			print OUT "switchkey($switchkey) keyfound($keyfound)\n";
			last;
		}
	}

close (OUT);

	if (! $keyfound) { # sid not match
		push (@out, "FAIL");
		push (@out, "Cannot extract report for $sid.");
		return @out;
	}
	if ($cnt == 0) { # no record match
		push (@out, "FAIL");
		push (@out, "No data found in this period for $sid.");
	}
	#local_debug(@out);
        return @out;
}  # end of sub get_fr_delay


# get FR round trip delay and DDR data from db
# date in uxtime format
# output format: (tab delimited)
# uxtime,key,logdate(dd/mm/yyyy),dailymaxdelay,dailymindelay,dailyavgdelay
sub get_fr_sla {
        my ($sdate, $edate, $sid) = @_;
        my @out;
	my $cnt = 0;
	my %outs;

open (OUT, ">/tmp/leo-debug~OS_REPORT_DB.pm~get_fr_sla");
print OUT "sid($sid)\n";

	my @t = split(/-/, $sid);

print OUT "t(@t)\n";

	my $ttype = ($t[1] =~ /\d+\.\d+/) ? 'ATM' : 'FR';
	my ($switch_s, $cityid_s, $cityname_s, $fnnmatch_s, $fnntype_s) = OS_REPORTS::get_switchcity_by_fnn($t[0], $ttype);

print OUT "t[0]($t[0]) switch_s($switch_s) cityid_s($cityid_s) cityname_s($cityname_s) fnnmatch_s($fnnmatch_s) fnntype_s($fnntype_s)\n";

	$ttype = ($t[3] =~ /\d+\.\d+/) ? 'ATM' : 'FR';
	my ($switch_d, $cityid_d, $cityname_d, $fnnmatch_d, $fnntype_d) = OS_REPORTS::get_switchcity_by_fnn($t[2], $ttype);

print OUT "t[2]($t[2]) switch_d($switch_d) cityid_d($cityid_d) cityname_d($cityname_d) fnnmatch_d($fnnmatch_d) fnntype_d($fnntype_d)\n";
	
	#my ($switch_s, $switch_d) = OS_REPORTS::get_switchcode_by_serviceid( $sid );
	#local_debug("($switch_s) ($switch_d)");

	if ($switch_s =~ /^FAIL$/i) {
		push (@out, "FAIL");
		push (@out, "Cannot extract switch info for $sid.");
		return @out;
	}


	#my $switchkey = "$switch_s"."-"."$switch_d";
	my @switchkeys = ("$switch_s"."-"."$switch_d", "$switch_d"."-"."$switch_s");
        #my $indir = $tgwr.'dbase/FR/dailydelay/';
        my $indir = $tgwr.'dbase/FR/hourlydelay/';
	my $keyfound = 0;

	foreach my $switchkey (@switchkeys) { 
	        my $infile = $indir.$switchkey;
		#local_debug( "infile=$infile" );
	        if (-e $infile) {
	                open (IN, "$infile");
	                        while(my $line = <IN>) {
	                                chomp($line);
	                                # get first element (uxtime) to compare
	                                if (($line =~ /(.*?)\t/)&&($1 >= $sdate)) {
	                                        if (($line =~ /(.*?)\t/)&&($1 <= $edate)) {
							$cnt++;
	                                                #push (@out, $line);
							my $ttime = (split(/\t/,$line))[0];
#print OUT "cnt($cnt) ttime($ttime)\n";
							$outs{$ttime} = $line;
	                                        }
	                                }
	                        }
	                close (IN);
			#$keyfound = 1;
			$keyfound = $switchkey;
			print OUT "switchkey($switchkey) keyfound($keyfound)\n";
			last;
		}
	}

	# get ddr data
	$indir = $tgwr.'dbase/FR/hourly/';
	my $logsid = fr_sid4log($sid);
	my $infile = $indir.$logsid;
	if (-e $infile) {
		open (IN, "$infile");
			while(my $line = <IN>) {
				chomp($line);
                                # get first element (uxtime) to compare
                                if (($line =~ /(.*?)\t/)&&($1 >= $sdate)) {
                                        if (($line =~ /(.*?)\t/)&&($1 <= $edate)) {
						$cnt++;
						my @tvals = split(/\t/,$line);
						my $ttime = $tvals[0];
						if (exists $outs{$ttime}) {
#print OUT "ttime exists ($ttime)\n";
						} else {
#print OUT "ttime not found ($ttime)\n";
							$outs{$ttime} = "$tvals[0]\t$keyfound\t$tvals[2]\tUNDEF_MAXRTD\tUNDEF_MINRTD\tUNDEF_AVGRTD";
						}
						$outs{$ttime} .= "\t$line";
                                        }
                                }
				
			}
		close (IN);
	}

print OUT "infile2($infile)\n";

	my $last_maxrtd = 0;
	my $last_minrtd = 0;
	my $last_avgrtd = 0;
	# find first available rtd
	foreach my $tk (sort keys %outs) {
		my $tline = $outs{$tk};
		if ($tline !=~ /UNDEF/i) {
			my @tvals = split( /\t/, $tline);
			$last_maxrtd = $tvals[3];
			$last_minrtd = $tvals[4];
			$last_avgrtd = $tvals[5];
			last;
		}
	}
	foreach my $tk (sort keys %outs) {
		my $tline = $outs{$tk};
		my @tvals = split( /\t/, $tline);
#print OUT "key($tk) value($tline)\n";
		if ($tline =~ /UNDEF/i) {
			$tline =~ s/UNDEF_MAXRTD/$last_maxrtd/;
			$tline =~ s/UNDEF_MINRTD/$last_minrtd/;
			$tline =~ s/UNDEF_AVGRTD/$last_avgrtd/;
		} else {
			$last_maxrtd = $tvals[3];
			$last_minrtd = $tvals[4];
			$last_avgrtd = $tvals[5];
		}
		push (@out, $tline);
	}

close (OUT);

	#if (! $keyfound) { # sid not match
	if ((! $keyfound)&&($cnt == 0)) { # sid not match in both ddr and delay
		push (@out, "FAIL");
		push (@out, "Cannot extract report for $sid.");
		return @out;
	} elsif ($cnt == 0) { # no record match
		push (@out, "FAIL");
		push (@out, "No data found in this period for $sid.");
	}
	#local_debug(@out);
        return @out;
} 


sub fr_sid4log {
	my $sid = shift @_;

	if($sid =~ /\d+\.\d+/) { # FRATM services
		my @v = split(/\-/, $sid);
		$v[0] = "00" . $v[0];
		$v[2] = "505" . $v[2];
		$sid = join '-', @v;
	} elsif($sid !~ /^505/) { # ord FR service
		my @v = split(/\-/, $sid);
		$v[0] = "505" . $v[0];
		$v[2] = "505" . $v[2];
		$sid = join '-', @v;
	}

	return $sid;

}


sub local_debug {
        my @vals = @_;
        open (DEBUG, ">/tmp/debug-os_reports_db.pm");
                foreach my $t (@vals) {
                        print DEBUG "$t\n";
                }
        close (DEBUG);
}


1;

