/*
 * XML Type:  getCINeighboursResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML getCINeighboursResponse(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class GetCINeighboursResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse
{
    
    public GetCINeighboursResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TOPOLOGY$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "topology");
    private static final javax.xml.namespace.QName COMMENTS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "comments");
    
    
    /**
     * Gets the "topology" element
     */
    public com.hp.schemas.ucmdb._1.types.Topology getTopology()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Topology target = null;
            target = (com.hp.schemas.ucmdb._1.types.Topology)get_store().find_element_user(TOPOLOGY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "topology" element
     */
    public void setTopology(com.hp.schemas.ucmdb._1.types.Topology topology)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Topology target = null;
            target = (com.hp.schemas.ucmdb._1.types.Topology)get_store().find_element_user(TOPOLOGY$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.Topology)get_store().add_element_user(TOPOLOGY$0);
            }
            target.set(topology);
        }
    }
    
    /**
     * Appends and returns a new empty "topology" element
     */
    public com.hp.schemas.ucmdb._1.types.Topology addNewTopology()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Topology target = null;
            target = (com.hp.schemas.ucmdb._1.types.Topology)get_store().add_element_user(TOPOLOGY$0);
            return target;
        }
    }
    
    /**
     * Gets the "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments getComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "comments" element
     */
    public void setComments(com.hp.schemas.ucmdb._1.types.ResComments comments)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            }
            target.set(comments);
        }
    }
    
    /**
     * Appends and returns a new empty "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments addNewComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            return target;
        }
    }
}
