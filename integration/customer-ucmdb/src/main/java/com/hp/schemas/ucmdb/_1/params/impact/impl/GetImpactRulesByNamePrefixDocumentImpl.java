/*
 * An XML document type.
 * Localname: getImpactRulesByNamePrefix
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * A document containing one getImpactRulesByNamePrefix(@http://schemas.hp.com/ucmdb/1/params/impact) element.
 *
 * This is a complex type.
 */
public class GetImpactRulesByNamePrefixDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixDocument
{
    
    public GetImpactRulesByNamePrefixDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETIMPACTRULESBYNAMEPREFIX$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactRulesByNamePrefix");
    
    
    /**
     * Gets the "getImpactRulesByNamePrefix" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix getGetImpactRulesByNamePrefix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix)get_store().find_element_user(GETIMPACTRULESBYNAMEPREFIX$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getImpactRulesByNamePrefix" element
     */
    public void setGetImpactRulesByNamePrefix(com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix getImpactRulesByNamePrefix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix)get_store().find_element_user(GETIMPACTRULESBYNAMEPREFIX$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix)get_store().add_element_user(GETIMPACTRULESBYNAMEPREFIX$0);
            }
            target.set(getImpactRulesByNamePrefix);
        }
    }
    
    /**
     * Appends and returns a new empty "getImpactRulesByNamePrefix" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix addNewGetImpactRulesByNamePrefix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix)get_store().add_element_user(GETIMPACTRULESBYNAMEPREFIX$0);
            return target;
        }
    }
}
