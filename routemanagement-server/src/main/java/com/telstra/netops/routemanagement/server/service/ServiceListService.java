package com.telstra.netops.routemanagement.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telstra.netops.routemanagement.server.exception.RMApplicationCustomException;
import com.telstra.netops.routemanagement.server.model.CustomerRouterDataServiceDTO;
import com.telstra.netops.routemanagement.server.model.Deleted_Links_Routing;
import com.telstra.netops.routemanagement.server.model.Link;
import com.telstra.netops.routemanagement.server.model.RouterInformationDTO;
import com.telstra.netops.routemanagement.server.model.Router_Software;
import com.telstra.netops.routemanagement.server.model.Success;
import com.telstra.netops.routemanagement.server.repository.CustomerServiceRepository;
import com.telstra.netops.routemanagement.server.repository.DeletedLinksRoutingRepository;
import com.telstra.netops.routemanagement.server.repository.LinkRepository;
import com.telstra.netops.routemanagement.server.repository.RouterServiceRepository;

/**
 * This is the Service class for listing the Service /AS details of the customer
 *
 */
@Service
public class ServiceListService {
	@Autowired
	private CustomerServiceRepository serviceRepository;
	@Autowired
	private RouterServiceRepository routerRepository;
	@Autowired
	private DeletedLinksRoutingRepository deletedLinksRoutingRepository;
	@Autowired
	private LinkRepository linkRepository;

	/**
	 * This fetches the Service/AS details of the customer
	 * 
	 * @param customerId
	 * @param productLine
	 * @return
	 */
	public List<CustomerRouterDataServiceDTO> fetchServiceDetails(String customerId, String productLine) {
		CustomerRouterDataServiceDTO customerRouterDataService = null;
		List<CustomerRouterDataServiceDTO> customerRouterDataServiceList = new ArrayList<CustomerRouterDataServiceDTO>();
		if (null != customerId && !customerId.equals("")) {
			List<String> asNumberList = serviceRepository.findDistinctAsNumberByCustomerId(customerId, productLine);
			if (asNumberList.size() == 0) {
				throw new RMApplicationCustomException("There are no AS numbers associated with the customer");
			}
			for (int asNumCount = 0; asNumCount < asNumberList.size(); asNumCount++) {
				customerRouterDataService = new CustomerRouterDataServiceDTO();
				customerRouterDataService.setAsNumber(asNumberList.get(asNumCount));
				List<String> telstraServiceIdList = serviceRepository
						.findDistinctTelstraServiceIdByAsNumber(asNumberList.get(asNumCount), customerId, productLine);
				List<String> serviceIdList = serviceRepository
						.findDistinctServiceIdByAsNumber(asNumberList.get(asNumCount), customerId, productLine);
				customerRouterDataService.setTelstraServiceId(telstraServiceIdList);
				customerRouterDataService.setServiceId(serviceIdList);
				customerRouterDataServiceList.add(customerRouterDataService);
			}
		}
		return customerRouterDataServiceList;
	}

	public List<RouterInformationDTO> viewRouterDetails(String customerId, String productLine) {
		List<RouterInformationDTO> routerInformationList = new ArrayList<RouterInformationDTO>();
		RouterInformationDTO routerInformation = null;
		List<com.telstra.netops.routemanagement.server.model.Service> serviceList = null;
		List<Router_Software> routerSoftwareList = null;
		List<Link> linkList = null;

		if (null != customerId && !customerId.equals("")) {
			serviceList = serviceRepository.findAllByServiceId(customerId, productLine);
			for (int i = 0; i < serviceList.size(); i++) {
				routerInformation = new RouterInformationDTO();
				String serviceId = serviceList.get(i).getId();

				routerInformation.setId(serviceId);
				routerInformation.setAsNumber(serviceList.get(i).getAsNo());
				routerInformation.setBgpNeighborIp(serviceList.get(i).getBgpNeighborIP());
				routerInformation.setBhaBgpNeighborIp(serviceList.get(i).getBhaBgpNeighborIp());
				routerInformation.setIrrdObject(serviceList.get(i).getIrrdObject());
				routerInformation.setProductLine(serviceList.get(i).getProductLine());
				routerInformation.setTelstraServiceId(serviceList.get(i).getTelstraServiceId());
				routerInformation.setRouterInterface(serviceList.get(i).getRouterInterface());
				routerInformation.setBgpNeighborIpV6(serviceList.get(i).getBgpNeighborIpV6());
				routerInformation.setBhaBgpNeighborIpV6(serviceList.get(i).getBhaBgpNeighborIpV6());

				linkList = linkRepository.findAllByServiceid(serviceId);
				// setting the routername
				String routername = linkList.get(0).getRouterName();
				routerInformation.setRouter(routername);
				routerInformation.setBlackHoleRouter(linkList.get(0).getBhaRoutername());
				routerSoftwareList = routerRepository.findAllByRoutername_RouterSoftware(routername);
				if (routerSoftwareList.isEmpty()) {
					routerInformationList.add(routerInformation);
				} else {
					routerInformation.setRouterSoftware(routerSoftwareList.get(0).getRouter_Software());
					routerInformationList.add(routerInformation);
				}

			}
		}
		return routerInformationList;
	}

	public Success editRouterInformation(RouterInformationDTO routerInformation) {
		String bgpNeighborIp = routerInformation.getBgpNeighborIp();
		String irrdObject = routerInformation.getIrrdObject();
		String bhaBgpNeighborIp = routerInformation.getBhaBgpNeighborIp();
		String bgpNeighborIpV6 = routerInformation.getBgpNeighborIpV6();
		String bhaBgpNeighborIpV6 = routerInformation.getBhaBgpNeighborIpV6();
		// String blackHoleRouter = routerInformation.getBlackHoleRouter();

		com.telstra.netops.routemanagement.server.model.Service service = new com.telstra.netops.routemanagement.server.model.Service();
		service = serviceRepository.findOneByServiceId(routerInformation.getId(), routerInformation.getProductLine());
		service.setBGPNeighborIP(bgpNeighborIp);
		service.setIrrdObject(irrdObject);
		service.setBhaBgpNeighborIP(bhaBgpNeighborIp);
		service.setBgpNeighborIpV6(bgpNeighborIpV6);
		service.setBhaBgpNeighborIpV6(bhaBgpNeighborIpV6);
		serviceRepository.save(service);
		saveRouterInformation(routerInformation);
		saveDeletedLinksRouting(routerInformation);
		return new Success("SUCCESS");
	}

	private void saveRouterInformation(RouterInformationDTO routerInformation) {
		Router_Software routerSoftware = new Router_Software();
		routerSoftware = routerRepository.findOneByRoutername(routerInformation.getRouter());
		routerSoftware.setRouter_Software(routerInformation.getRouterSoftware());
		routerRepository.save(routerSoftware);
	}

	private void saveDeletedLinksRouting(RouterInformationDTO routerInformation) {
		Deleted_Links_Routing deletedLinksRouting = new Deleted_Links_Routing();
		deletedLinksRouting = deletedLinksRoutingRepository.findOneByServiceid(routerInformation.getId());
		deletedLinksRouting.setBgpNeighborIp(routerInformation.getBgpNeighborIp());
		deletedLinksRouting.setBhaBgpNeighborIp(routerInformation.getBhaBgpNeighborIp());
		deletedLinksRouting.setBgpNeighborIpV6(routerInformation.getBgpNeighborIpV6());
		deletedLinksRouting.setBhaBgpNeighborIpV6(routerInformation.getBhaBgpNeighborIpV6());
		deletedLinksRoutingRepository.save(deletedLinksRouting);
	}

}
