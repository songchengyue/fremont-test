#
# Author: Tony Tam
# Date: 30 June 2003
# File: CMS_COMMUNITIES_DB.pm
#
# $Id: CMS_COMMUNITIES_DB.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $

package CMS_COMMUNITIES_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;

our $error;

sub get_communities_list {

  my $prefix = shift @_ || "";
  my $as_no = shift @_ || "";
  my $communities = shift @_ || "";
  my $next_hop = shift @_ || "";
  my $update_type = shift @_ || "";
  my $date_time_start = shift @_ || "";
  my $date_time_end = shift @_ || "";

  my $sql_command = ("SELECT l.prefix, l.as_path, l.next_hop, l.update_time, l.communities, t.type 
                            FROM bgp_update_list as l, bgp_update_type as t where l.type = t.type_id 
                     ");

  $sql_command .= " AND update_time >= CURRENT_TIMESTAMP - interval '3 month' ";
  if ($date_time_start ne "") {
    $sql_command .= " AND update_time >= '$date_time_start'
                      AND update_time <= '$date_time_end' ";
  }
  if ($prefix ne "") { $sql_command .= "AND prefix like '$prefix%' "; }
  if ($as_no ne "") { $sql_command .= "AND as_path ~* '$as_no' "; }
  if ($communities ne "") { $sql_command .= "AND communities ~* '$communities' "; }
  if ($next_hop ne "") { $sql_command .= "AND next_hop like '$next_hop%' "; }
  if ($update_type ne "") { $sql_command .= "AND l.type = '$update_type' "; }

  $sql_command .= " order by update_time desc";
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}

sub get_bgp_update_type {
  my $sql_command = "select type_id, type from bgp_update_type";
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result; 
}

1;
