#!/usr/bin/perl

use strict;
use warnings;

use lib "/usr/local/www/wanchai/netops/modules";
use lib "/usr/local/www/wanchai/modules";
use CMS_DB;
use CMS_PRIDNS_DB;
use CMS_OLSS;
use constant FILE_PATH_CONF => "/usr/local/www/wanchai/netops/data/pridns/international/primary";
use constant FILE_PATH_ZONE => "/usr/local/www/wanchai/netops/data/pridns/international/";

use constant FILE_PATH_CONF_TEMPL => "/usr/local/www/wanchai/netops/scripts/make_pridns_conf.template.conf";
use constant FILE_PATH_ZONE_TEMPL => "/usr/local/www/wanchai/netops/scripts/make_pridns_conf.template.zone";
use constant FILE_PATH_SN => "/usr/local/www/wanchai/netops/scripts/make_pridns_conf.serialno";
#CRQ000000007085_DNS_zone_config
#Different zone file parameters for different domains
use constant ZONE_TEMP_CONF => "/usr/local/www/wanchai/netops/scripts/zone_file_config";

my $primary_dns_hostname = "ns4.net.reach.com";
#CRQ000000007324 Zone-file-correction DNS 31-Aug-2012
my $primary_ns_record =  "ns4.net.reach.com.";

my $file_build_date = `date`;
chomp ($file_build_date);
my @now = gmtime;
my ($yr,$mth,$mday,$second, $minute, $hour) = ($now[5], $now[4], $now[3],$now[0],$now[1],$now[2]);

$yr += 1900;
$mth += 1;
if ($mth <= 9) { $mth = "0".$mth; }
if ($mday <= 9) { $mday = "0".$mday; }
if ($hour<=9){$hour = "0".$hour;}
my $cur_sec = (($hour*3600)+($minute*60)+$second);
my $cur_time = "$hour:$minute:$second";
my @ar_time = '';
my @hr_format = '';
my @sec_format = '';
my @db_date1 = '';
my @db_mon = '';
my @db_day = '';
my @db_year = '';
my @req_time ='';
my @records = '';

my $serial_number = "$yr$mth$mday";

open(SN, FILE_PATH_SN);
my $last_sn = <SN>;
chomp $last_sn;
my $last_upd_date = substr($last_sn,0,8);
my $last_ver = substr($last_sn,8,2);
print "last update date: $last_upd_date  last version: $last_ver\n";
close(SN);

if ($last_upd_date ne $serial_number) {  # current date is diff from last update date
	$serial_number .= "01";
} else {
	$last_ver = $last_ver +0 +1;
	$last_ver = "0".$last_ver if ($last_ver <= 9);
	$last_ver = "01" if ($last_ver == 100);
	$serial_number .= $last_ver;
}
print "current serial number: $serial_number\n";

open(SN, ">".FILE_PATH_SN);
print SN "$serial_number";
close(SN);

CMS_DB::connect_to_database;

my $pridns = CMS_PRIDNS_DB::get_pro_pridns_entries_location(0);

my $pridnstime = CMS_PRIDNS_DB::time_db(0);


##open (NAMED, ">".FILE_PATH);

my %rec = ();
foreach my $entry (@$pridns) {
	my ($accno, $domain, $host, $type, $value, $request_time, $date_assigned, $date_last_update, $contact, $status, $oid) = @$entry;

	

	#CRQ000000007324 Zone-file-correction DNS 31-Aug-2012
	my $v = "$domain\t$host\t$type\t$value\t$request_time\t$status\t$oid\t$contact";

	my $a = $rec{$domain};
	push @$a, $v;
	$rec{$domain} = \@$a;

}

#get request_time from DB
foreach my $row (@$pridnstime){
	push (@ar_time, $$row[0]);
	
	push (@req_time, $$row[0]);
	
        my @db_hr1 = split(/\+/,$$row[0]);
	
        my @db_hr = split(/ /,$db_hr1[0]);
	
        my @db_sec1 = split(/\./,$db_hr[1]);
	
        my $db_sec = (($db_sec1[0]*3600)+ ($db_sec1[1]*60) + ($db_sec1[2]));
	
        my @db_date1 = split (/-/,$db_hr[0]);
	
        push (@sec_format, $db_sec);
        push (@hr_format, $db_hr[1]);
        push (@db_year,$db_date1[0]);
        push (@db_mon,$db_date1[1]);
        push (@db_day,$db_date1[2]);
	
	 
shift (@ar_time);
}

my $len_ar = scalar @hr_format;

CMS_DB::disconnect_from_database;

my $namedconf_data = '';

#CRQ000000007085_DNS_zone_config
#Different zone file for different domains
my %hash_zone_domain = ();
open(zone_conf, ZONE_TEMP_CONF) || die "can't open zone template configuration file: $!";
while (my $l = <zone_conf>) { 
	my @v = split(/\t/,$l);
	my $dom_name = $v[0];
	my $template = $v[1];
	chomp($dom_name);
	chomp($template);
	$hash_zone_domain{$dom_name} = $template;
}
close(zone_conf);


foreach my $hash_domain (keys %rec){
print "processing domain $hash_domain\n";
	my $array_records = $rec{$hash_domain};
	my %recs = ();
	# CRQ000000007324 Zone-file-correction DNS 31-Aug-2012
	my %contact_email = ();

	#CRQ000000007324 Zone-file-correction DNS 31-Aug-2012
	#my $tmp_ns = "IN\tNS\t$primary_ns_record";
	#push @{$recs{NS}}, $tmp_ns;
		
	foreach my $domain_record (@$array_records) {
#print "\n\n$hash_domain => $domain_record\n";

		my $tmp = '';

		my @line = split(/\t/, $domain_record);
		my $domain  = $line[0];
		my $host    = $line[1];
		my $type_id = $line[2];
		my $value   = $line[3];
		my $oid     = $line[6];

		#CRQ000000007324 Zone-file-correction DNS 31-Aug-2012
		my $email_contact = $line[7]; 
		$contact_email{$domain} = $email_contact;

		my $result =$type_id;
		my $type =$result;
		

		if ($type =~ /^A$/) {
			$tmp = "$host\tIN\tA\t$value";
			push @{$recs{$type}}, $tmp;
		} elsif ($type =~ /^AAAA$/) {
			$tmp = "$host\tIN\tAAAA\t$value";
			push @{$recs{$type}}, $tmp;
		}elsif ($type =~ /^CNAME$/) {
			$tmp = "$host\tIN\tCNAME\t$value";
			push @{$recs{$type}}, $tmp;
		} elsif ($type =~ /^NS$/) {
			#my $tmp_host = $host;
			#if ($tmp_host !~ /$domain\.$/) {
			#	$tmp_host .= "\.$domain\.";
			#}
			#CRQ000000007324 Zone-file-correction DNS 31-Aug-2012
			#if ($value ne $primary_ns_record){
			$tmp = "$host\tIN\tNS\t$value";
			push @{$recs{$type}}, $tmp;
			#}
		} elsif ($type =~ /^MX$/) {
			my ($tmp_pref, $tmp_exch) = split(/\|/, $value);
			$tmp = "$host\tIN\tMX\t$tmp_pref\t$tmp_exch";
			push @{$recs{$type}}, $tmp;
		} elsif ($type =~ /^TXT$/) {
			$tmp = "$host\tIN\tTXT\t\"$value\"";
			push @{$recs{$type}}, $tmp;
		}

	}  # end of all dns record of single domain


	my ($base, $data);

	### construct zone files

	#CRQ000000007085_DNS_zone_config
	#Different zone template
	if (exists $hash_zone_domain{$hash_domain}) {
	#if($hash_domain eq "mbkpartnerslp.com"){
	#	print "hash : $hash_zone_domain{$hash_domain} \n";
		my $TEMPLATE = $hash_zone_domain{$hash_domain};
	
	
		
		open(TEMPL_new, $TEMPLATE) || die "can't open zone template file: $!";
		while (my $l = <TEMPL_new>) { $base .= $l; }
		close(TEMPL_new);
	} else {
		open(TEMPL, FILE_PATH_ZONE_TEMPL) || die "can't open zone template file: $!";
		while (my $l = <TEMPL>) { $base .= $l; }
		close(TEMPL);
	}

	foreach my $ty (sort keys %recs) {
		$data .= "\;\n\; $ty RECORD\n\;\n";
		foreach my $v (@{$recs{$ty}}) {
			$data .= "$v\n";
		}
	}
#}

	#CRQ000000007324 Zone-file-correction DNS 31-Aug-2012
	my $email_con = $contact_email{$hash_domain}; 

my @email=split(/@/, $email_con);


my $email_cont=join('.',@email);

	$base =~ s/\*EMAIL_CONTACT\*/$email_cont/g;
	$base =~ s/\*SERIALNUMBER\*/$serial_number/;
	$base =~ s/\*FILEBUILDDATE\*/$file_build_date/;
	$base =~ s/\*PRIDNSHOSTNAME\*/$primary_dns_hostname/;
	$base =~ s/\*DOMAINNAME\*/$hash_domain/g;
	$base =~ s/\*DATA\*/$data/;

	print "writing zone file...\n";
	open(ZONE, ">".FILE_PATH_ZONE."db\.$hash_domain");
	print ZONE "$base\n";
	close(ZONE);

	### construct named.conf file

	$base = '';
	open(TEMPL, FILE_PATH_CONF_TEMPL) || die "can't open named config template file: $!";
	while (my $l = <TEMPL>) { $base .= $l; }
	close(TEMPL);
	
	$base =~ s/\*SERIALNUMBER\*/$serial_number/;
	$base =~ s/\*FILEBUILDDATE\*/$file_build_date/;
	$base =~ s/\*DOMAINNAME\*/$hash_domain/g;

	$namedconf_data .= $base;
	
}
CMS_DB::connect_to_database;
my $lpct = 0;

        for ($lpct = 0; $lpct <= ($len_ar-1); $lpct++)
        {
        $mday =~ s/^\s+|\s+$//g;
        my $db_day1 = shift @db_day;
	
        $db_day1 =~ s/^\s+|\s+$//g;

        $mth =~ s/^\s+|\s+$//g;
        my $db_mon1 = shift @db_mon;
        $db_mon1  =~ s/^\s+|\s+$//g;

        $yr =~ s/^\s+|\s+$//g;
        my $db_year1 = shift @db_year;
        $db_year1 =~ s/^\s+|\s+$//g;

        #my $trial1 = $db_mon1*5;#already commented
        my $sec_format1 = shift @sec_format;

        #update acl only if recently added/deleted entries
	
	 
        if (($mday eq $db_day1) && ($mth eq $db_mon1) && ($yr eq $db_year1) && (($cur_sec - $sec_format1)<=3600) ){
                my $reqtime = $req_time[$lpct];
                my $all_val = CMS_PRIDNS_DB::all_values($reqtime,0);
                foreach my $rec (@$all_val){

                        push (@records, $$rec[0]);
                        push (@records, $$rec[1]);
                        push (@records, $$rec[2]);
                        push (@records, $$rec[3]);
                        push (@records, $$rec[4]);
                        push (@records, $$rec[5]);
                        push (@records, $$rec[6]);
						push (@records, $$rec[7]);
						push (@records, $$rec[8]);


                        my $value_type = '';
                        if (($records[7] == 1)||($records[7] == 2)) { $value_type = "REQUESTED"; print $value_type; }
                        elsif (($records[7] == 3)||($records[7] == 4)) { $value_type = "CANCELLED"; print $value_type}
                        my $uid = $records[1];
                        my $accno = $records[2];
                        my $pdns_contact = $records[3];
                        my $pdns_dom = $records[4];
                        my $pdns_host = $records[5];
 			my $pdns_ip = $records[6];
			my $pdns_type = $records[8];

			print "sending mail";	
                        my $mail = CMS_PRIDNS_DB::PDNS_mail($uid, $accno, $pdns_contact,$pdns_dom,$pdns_host,$pdns_ip,$value_type,$pdns_type);
                        @records = '';

                }#end of foreach loop
        }#end of if loop
        }#end of for loop
CMS_DB::disconnect_from_database;

print "writing named config file...\n";
open(CONF, ">".FILE_PATH_CONF);
print CONF "$namedconf_data\n";
close(CONF);

CMS_DB::connect_to_database;
CMS_PRIDNS_DB::complete_pridns_entries;
CMS_DB::disconnect_from_database;


