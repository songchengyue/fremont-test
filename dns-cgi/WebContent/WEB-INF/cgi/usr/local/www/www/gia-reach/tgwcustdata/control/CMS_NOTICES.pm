# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 12 September 2001
# File: CMS_NOTICES.pm
#
# $Id: CMS_NOTICES.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $

package CMS_NOTICES;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_NOTICES_DB;
use NOTIFICATION;
use CMS_CUSTOMER_DB;

sub get_notice_list {
  $CMS::html_values{notices}{table} = 
    CMS_NOTICES_DB::get_notice_list(@_);
  

  foreach my $notice (@{$CMS::html_values{notices}{table}}) {

    my $timestamp = $$notice[0];
    $timestamp =~ s/[-:+ ]//g;

    $$notice[0] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=notices&command=view_notice&timestamp=$timestamp\">$$notice[0]</a>";
  }
  unshift (@{$CMS::html_values{notices}{table}},
	   ["Sent at", "Subject", "Message", "Sent by"]);


  $CMS::html_values{notices}{header_rows} = 1;
}


sub start {
  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} eq "Cancel"))) {
    # default to view_current_outages
    
    $CMS::html_values{command}{value} = "view_recent_notices";
    
  }


  if ($CMS::html_values{command}{value} eq "view_recent_notices") {
    get_notice_list;

    TI_HTML::set_datetime_search_values
	(\%CMS::html_values, "search_date",
	 "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");



    CMS::output_html("notice_view_list");
  } elsif ($CMS::html_values{command}{value} eq "create_notice") {
    if (defined($CMS::html_values{submit}{value}) &&
	$CMS::html_values{submit}{value} eq "Continue") {
      if (CMS::check_screen("notice_create")) {
	CMS_NOTICES_DB::add_notice
	    ($CMS::html_values{username}{value},
	     $CMS::html_values{subject}{value},
	     $CMS::html_values{message}{value});


	

	my $recipients = CMS_CUSTOMER_DB::get_notice_emails;

	# ALL Recipients get email for now.
	foreach my $recep (@$recipients) {
	  unshift (@$recep, "email");
	}


	unless (my $return = NOTIFICATION::send($recipients, 
			   $CMS::html_values{subject}{value},
			   $CMS::html_values{message}{value},
			   "Reach Notice System",
			       "info\@TelstraInternational.com")) {
	  
	  CMS::add_error($NOTIFICATION::error);
	}


	get_notice_list;
	CMS::log("create_notice", $CMS::html_values{subject}{value});
	$CMS::html_values{command}{value} = "view_recent_notices";
	CMS::output_html("notice_view_list");
      } else {
	CMS::output_html("notice_create");
      }
    } else {
      CMS::output_html("notice_create");
    }
  } elsif ($CMS::html_values{command}{value} eq "search_notices") {

    if (defined($CMS::html_values{submit}{value}) &&
	$CMS::html_values{submit}{value} =~ /Clear/i) {
      $CMS::html_values{search_timestamp}{value} = "";
      $CMS::html_values{search_subject}{value} = "";
      $CMS::html_values{search_message}{value} = "";
      $CMS::html_values{search_username}{value} = "";

       TI_HTML::set_datetime_search_values
	(\%CMS::html_values, "search_date",
	 "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");

    } elsif (defined($CMS::html_values{submit}{value}) &&
	$CMS::html_values{submit}{value} eq "Search") {
      my ($start,$end) = TI_HTML::get_datetime_search_values
	(\%CMS::html_values, "search_date");
      get_notice_list
	($start, $end,
	 $CMS::html_values{search_subject}{value},
	 $CMS::html_values{search_message}{value},
	 $CMS::html_values{search_username}{value});
    } else {
      $CMS::html_values{search_timestamp}{value} = "";
      $CMS::html_values{search_subject}{value} = "";
      $CMS::html_values{search_message}{value} = "";
      $CMS::html_values{search_username}{value} = "";

       TI_HTML::set_datetime_search_values
	(\%CMS::html_values, "search_date",
	 "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");
    }
    # nothing entered
    CMS::output_html("notice_view_list");
  } elsif ($CMS::html_values{command}{value} eq "view_notice") {
    
    
    ($CMS::html_values{timestamp}{value},
     $CMS::html_values{subject}{value},
     $CMS::html_values{message}{value},
     $CMS::html_values{sentby}{value}) = 
       CMS_NOTICES_DB::get_notice($CMS::html_values{timestamp}{value});

    
    $CMS::html_values{othermenu}{value} = "View Notice";
    CMS::output_html("notice_view");

  }



}
1;
