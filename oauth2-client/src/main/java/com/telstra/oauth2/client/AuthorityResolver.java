package com.telstra.oauth2.client;

import java.security.Principal;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import com.telstra.oauth2.client.exception.CustomerIdNotFoundException;

abstract public class AuthorityResolver {

	private final String prefix;
	private final String description;
	
	protected AuthorityResolver(String prefix, String description) {
		this.prefix = prefix;
		this.description = description;
	}
	
	public String resolve(Principal principal) {
		if (!(principal instanceof OAuth2Authentication)) {
			throw new CustomerIdNotFoundException("Don't know how to find customer id in object of class " + principal.getClass());
		}
		
		OAuth2Authentication auth = (OAuth2Authentication)principal;
		for (GrantedAuthority gAuth : auth.getAuthorities()) {
			if (gAuth.getAuthority().startsWith(prefix)) {
				return gAuth.getAuthority().substring(prefix.length());				
			}
		}

		throw new CustomerIdNotFoundException(description + " not found in granted authorities!");
	}
	
}
