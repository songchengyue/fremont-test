/*
 * XML Type:  intList
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntList
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML intList(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IntListImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.IntList
{
    
    public IntListImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INTVALUE1$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intValue");
    
    
    /**
     * Gets array of all "intValue" elements
     */
    public java.math.BigInteger[] getIntValue1Array()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INTVALUE1$0, targetList);
            java.math.BigInteger[] result = new java.math.BigInteger[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getBigIntegerValue();
            return result;
        }
    }
    
    /**
     * Gets ith "intValue" element
     */
    public java.math.BigInteger getIntValue1Array(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INTVALUE1$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getBigIntegerValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "intValue" elements
     */
    public org.apache.xmlbeans.XmlInteger[] xgetIntValue1Array()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INTVALUE1$0, targetList);
            org.apache.xmlbeans.XmlInteger[] result = new org.apache.xmlbeans.XmlInteger[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "intValue" element
     */
    public org.apache.xmlbeans.XmlInteger xgetIntValue1Array(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInteger target = null;
            target = (org.apache.xmlbeans.XmlInteger)get_store().find_element_user(INTVALUE1$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return (org.apache.xmlbeans.XmlInteger)target;
        }
    }
    
    /**
     * Returns number of "intValue" element
     */
    public int sizeOfIntValue1Array()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTVALUE1$0);
        }
    }
    
    /**
     * Sets array of all "intValue" element
     */
    public void setIntValue1Array(java.math.BigInteger[] intValue1Array)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(intValue1Array, INTVALUE1$0);
        }
    }
    
    /**
     * Sets ith "intValue" element
     */
    public void setIntValue1Array(int i, java.math.BigInteger intValue1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INTVALUE1$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setBigIntegerValue(intValue1);
        }
    }
    
    /**
     * Sets (as xml) array of all "intValue" element
     */
    public void xsetIntValue1Array(org.apache.xmlbeans.XmlInteger[]intValue1Array)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(intValue1Array, INTVALUE1$0);
        }
    }
    
    /**
     * Sets (as xml) ith "intValue" element
     */
    public void xsetIntValue1Array(int i, org.apache.xmlbeans.XmlInteger intValue1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInteger target = null;
            target = (org.apache.xmlbeans.XmlInteger)get_store().find_element_user(INTVALUE1$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(intValue1);
        }
    }
    
    /**
     * Inserts the value as the ith "intValue" element
     */
    public void insertIntValue1(int i, java.math.BigInteger intValue1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(INTVALUE1$0, i);
            target.setBigIntegerValue(intValue1);
        }
    }
    
    /**
     * Appends the value as the last "intValue" element
     */
    public void addIntValue1(java.math.BigInteger intValue1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(INTVALUE1$0);
            target.setBigIntegerValue(intValue1);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "intValue" element
     */
    public org.apache.xmlbeans.XmlInteger insertNewIntValue1(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInteger target = null;
            target = (org.apache.xmlbeans.XmlInteger)get_store().insert_element_user(INTVALUE1$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "intValue" element
     */
    public org.apache.xmlbeans.XmlInteger addNewIntValue1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInteger target = null;
            target = (org.apache.xmlbeans.XmlInteger)get_store().add_element_user(INTVALUE1$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "intValue" element
     */
    public void removeIntValue1(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTVALUE1$0, i);
        }
    }
}
