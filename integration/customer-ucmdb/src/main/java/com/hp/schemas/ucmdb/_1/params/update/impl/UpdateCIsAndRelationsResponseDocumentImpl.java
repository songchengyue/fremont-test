/*
 * An XML document type.
 * Localname: updateCIsAndRelationsResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * A document containing one updateCIsAndRelationsResponse(@http://schemas.hp.com/ucmdb/1/params/update) element.
 *
 * This is a complex type.
 */
public class UpdateCIsAndRelationsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponseDocument
{
    
    public UpdateCIsAndRelationsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName UPDATECISANDRELATIONSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "updateCIsAndRelationsResponse");
    
    
    /**
     * Gets the "updateCIsAndRelationsResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponse getUpdateCIsAndRelationsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponse)get_store().find_element_user(UPDATECISANDRELATIONSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "updateCIsAndRelationsResponse" element
     */
    public void setUpdateCIsAndRelationsResponse(com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponse updateCIsAndRelationsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponse)get_store().find_element_user(UPDATECISANDRELATIONSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponse)get_store().add_element_user(UPDATECISANDRELATIONSRESPONSE$0);
            }
            target.set(updateCIsAndRelationsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "updateCIsAndRelationsResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponse addNewUpdateCIsAndRelationsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponse)get_store().add_element_user(UPDATECISANDRELATIONSRESPONSE$0);
            return target;
        }
    }
}
