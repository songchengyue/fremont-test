package com.telstra.netops.integration.ipresource.ccms.ipam;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.telstra.netops.integration.ipresource.processor.IpResource;
import com.telstra.netops.integration.ipresource.processor.IpResourceProcessor;
import com.telstra.netops.integration.ipresource.processor.IpResourceStatus;

@Component
public class GetIpResources {
	
	private List<IpResource> ipResources = Arrays.asList(
				new IpResource[] {
						new IpResource(IpResourceStatus.ASSIGNED, "abc123", "8.8.8.8", 32),
						new IpResource(IpResourceStatus.ASSIGNED, "abc123", "4.4.4.4", 30)
				}
			);
	
	@Autowired
	private IpResourceProcessor processor;
	
	@Scheduled(fixedRate = 50000)
    public void run() {
		for (IpResource ipResource : ipResources) {
			System.out.println("Process IP resource " + ipResource );
			processor.process(ipResource);
		}		
	}

}
