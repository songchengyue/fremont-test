package com.telstra.netops.client.exception;

import org.springframework.web.bind.ServletRequestBindingException;

public class MissingRequestHeaderException extends ServletRequestBindingException {
	
	public MissingRequestHeaderException(String header) {
		super("Missing request header '" + header + "'");
	}
	
}
