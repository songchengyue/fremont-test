/*
 * An XML document type.
 * Localname: rediscoverViewCIsRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one rediscoverViewCIsRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class RediscoverViewCIsRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequestDocument
{
    
    public RediscoverViewCIsRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REDISCOVERVIEWCISREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "rediscoverViewCIsRequest");
    
    
    /**
     * Gets the "rediscoverViewCIsRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequest getRediscoverViewCIsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequest)get_store().find_element_user(REDISCOVERVIEWCISREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "rediscoverViewCIsRequest" element
     */
    public void setRediscoverViewCIsRequest(com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequest rediscoverViewCIsRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequest)get_store().find_element_user(REDISCOVERVIEWCISREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequest)get_store().add_element_user(REDISCOVERVIEWCISREQUEST$0);
            }
            target.set(rediscoverViewCIsRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "rediscoverViewCIsRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequest addNewRediscoverViewCIsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverViewCIsRequest)get_store().add_element_user(REDISCOVERVIEWCISREQUEST$0);
            return target;
        }
    }
}
