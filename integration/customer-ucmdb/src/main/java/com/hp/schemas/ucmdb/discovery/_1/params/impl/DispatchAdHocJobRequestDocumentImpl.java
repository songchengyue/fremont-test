/*
 * An XML document type.
 * Localname: dispatchAdHocJobRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one dispatchAdHocJobRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class DispatchAdHocJobRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequestDocument
{
    
    public DispatchAdHocJobRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DISPATCHADHOCJOBREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "dispatchAdHocJobRequest");
    
    
    /**
     * Gets the "dispatchAdHocJobRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequest getDispatchAdHocJobRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequest)get_store().find_element_user(DISPATCHADHOCJOBREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "dispatchAdHocJobRequest" element
     */
    public void setDispatchAdHocJobRequest(com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequest dispatchAdHocJobRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequest)get_store().find_element_user(DISPATCHADHOCJOBREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequest)get_store().add_element_user(DISPATCHADHOCJOBREQUEST$0);
            }
            target.set(dispatchAdHocJobRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "dispatchAdHocJobRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequest addNewDispatchAdHocJobRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobRequest)get_store().add_element_user(DISPATCHADHOCJOBREQUEST$0);
            return target;
        }
    }
}
