package com.telstra.netops.routemanagement.server.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.routemanagement.server.exception.RMApplicationCustomException;
import com.telstra.netops.routemanagement.server.model.PrefixUploadRequest;
import com.telstra.netops.routemanagement.server.model.Success;

@Service
@Profile("local")
public class LocalMultiplePrefixUploadService implements MultiplePrefixUploadService {
	@Value("${routemanagement.localtest.success}")
	private boolean success;

	@Override
	public Success upload(PrefixUploadRequest request) {
		if (success) {
			return new Success("SUCCESS!");
		} else {
			throw new RMApplicationCustomException("An error occurred while bulk prefix upload");
		}
	}

}
