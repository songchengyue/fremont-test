# (C) Telstra 2001
#
#
# Author: Ash Garg
# Date: 07/04/01
# File: validate.pm
#
# This file is used to check for consistancy across variables
#
# $Id: VALIDATE.pm,v 1.1.1.1 2003/02/20 04:37:32 rchew Exp $

package VALIDATE;
use Exporter;
use strict;

our @ISA = qw(Exporter);
our @EXPORT = qw(is_string is_username is_password is_integer is_email is_time is_timearray
		is_date is_datetime is_ipaddr is_dateafter is_in_array is_exists
		is_key_in_hash is_email_list is_phone
		SUCCESS ERROR);

#Error codes
use constant SUCCESS => 1;
use constant ERROR => 0;

#This function checks the input to see if there is an input
sub is_exists
{
	my $inputstring = shift @_;
	
	if($inputstring)
	{
		return SUCCESS;
	}
	return ERROR;
}

#This function checks the input to see if it has valid characters.
sub is_string
{
	my $inputstring = shift @_;
	
	if($inputstring=~/^.+$/)
	{
		return SUCCESS;
	}
	return ERROR;
}

#A valid username can only contain chars, digits or a '.'
#A valid username should be a maximum of 15 characters
sub is_username
{
	my $inputstring = shift @_;
	my $len=length($inputstring);
	
	if($len>15)
	{
		return ERROR;
	}
	
	if($inputstring=~/^[a-zA-Z0-9\.]+$/)
	{
		return SUCCESS;
	}
	return ERROR;
}

#A valid password can contain any characters except a whitespace.
sub is_password
{
	my $inputstring = shift @_;
	
	if($inputstring=~/^\S+$/)
	{
		return SUCCESS;
	}
	return ERROR;
}

#The input should be only digits and should not have a '.' as part of it.
#A max value can be added if required
sub is_integer
{
	my $inputstring = shift @_;

	if($inputstring=~/\./)
	{
		return ERROR;
        }

	if(is_numbers($inputstring)==ERROR)
	{
		return ERROR;
	}

	return SUCCESS;
}

#check for an email address. A valid address must contain a '@' and a '.'.
#This function can be later improved to do DNS validation and SMTP validation.
sub is_email
{
	my $inputstring = shift @_;

        if($inputstring=~/\@/)
        {
                if($inputstring=~/\./)
                {
                        return SUCCESS;
                }
        }
	
	return ERROR;
}

#check for a valid phone no. It can include "+() -0123456789"
#The "+" should be at the start of the string
#The ( must be followed by a )
sub is_phone   
{
        my $inputstring = shift @_;

        if($inputstring=~/^[0-9\+\(\)-]+$/)
        {
           	if($inputstring=~/^\+/)
	       	{
			if($inputstring=~/\(\d+\)/)
			{
				return SUCCESS;
			}
		}
		if($inputstring=~/\(\d+\)/)
		{
			return SUCCESS;
		}
        }
        return ERROR;
}


##The format for time should only be HH:MM:SS or HH:MM in 24 hour clock if a string is passed
##The format for time should only be HH,MM,SS or HH,MM in 24 hour clock
##It does not have to be zero padded, ie 02:02:20 and 2:2:20 will pass
##
##During the use of this function is was noticed that if HH:MM:NULL was passed then the function will return an ERROR.
##This functionality may need to be looked at in the future if different operation is required.
sub is_time
{
	my $no_of_var=scalar @_;
	my @fields;
	my $hour;
	my $minute;
	my $second;
	my $length;

	if ($no_of_var==1)
	{
		my $inputstring = shift @_;

		if(length($inputstring)>=9)
		{
			return ERROR;
		}
		@fields=split(":",$inputstring);
	
		
		$length=@fields;
		if($length>3)
		{
			return ERROR;
		}
	
		$hour=$fields[0];
		$minute=$fields[1];
		if($length==3)
		{
			$second=$fields[2];
		}
	}

	if($no_of_var==3)
	{
		$hour = shift @_;
		$minute = shift @_;
		$second = shift @_;
		$length=3;
	}

        if($no_of_var==2)
        {
		$hour = shift @_;
		$minute = shift @_;
		$length=2;
        }
	
		if(is_numbers($hour)==ERROR || is_numbers($minute)==ERROR)
		{
			return ERROR;
		}
	
		if($hour>=24)
		{
			return ERROR;
		}
		
		if($minute>=60)
		{
			return ERROR;
		}
	
	        if($minute !~ /^\d\d$/) {
		  return ERROR;
		}

		if($length==3)
		{
			if(is_numbers($second)==ERROR)
			{
				return ERROR;
			}
	
			if($second>=60)
			{
				return ERROR;
			}
                        
			if($minute!~/^\d\d$/) {
			  return ERROR;
			}

		}
	return SUCCESS;
}

##Check a string to see if it contains zero or more numbers
sub is_numbers
{
	my $inputstring = shift @_;

	if($inputstring=~/^\d+$/)
	{
		return SUCCESS;
	}
	return ERROR;
}

##Check for date of the format dd/mm/yyyy and its validity
##The date does not have to be zero padded, ie 02/02/2001 and 2/2/2001 will pass
sub is_date
{
	my $no_of_var=scalar @_;
	my @fields;
	my $day;
	my $month;
	my $year;
	my $length;

	if ($no_of_var==1)
	{
		my $inputstring = shift @_;
		if(length($inputstring)>=11)
		{
			return ERROR;
		}
		@fields=split("\/",$inputstring);

		$length=@fields;
		if($length!=3)
		{
			return ERROR;
		}
		$day=$fields[0];
		$month=$fields[1];
		$year=$fields[2];
	}

	if($no_of_var==3)
	{
		$day = shift @_;
		$month = shift @_;
		$year = shift @_;
		$length=3;
	}

	if(is_numbers($day)==ERROR || is_numbers($year)==ERROR)
	{
		return ERROR;
	}
	
	# Accept three letter abbreviations for dates
	# Change to numbers for futher checking
	# if is not a number and doesn't match a 3 letter
	# abbreviation then abort.
	# Added PJM 15 August 2001
	if(is_numbers($month)==ERROR) {
	  if($month eq "Jan") {$month = 1;}
	  elsif($month eq "Feb") {$month = 2;}
	  elsif($month eq "Mar") {$month = 3;}
	  elsif($month eq "Apr") {$month = 4;}
	  elsif($month eq "May") {$month = 5;}
	  elsif($month eq "Jun") {$month = 6;}
	  elsif($month eq "Jul") {$month = 7;}
	  elsif($month eq "Aug") {$month = 8;}
	  elsif($month eq "Sep") {$month = 9;}
	  elsif($month eq "Oct") {$month = 10;}
	  elsif($month eq "Nov") {$month = 11;}
	  elsif($month eq "Dec") {$month = 12;}
	  else { return ERROR; }
	}

	if($year<1900 || $year >3000)
	{
		return ERROR;
	}

	if($month<=0 || $month>=13)
	{
		return ERROR;
	}

	if($month==1 || $month==3 || $month==5 || $month==7 || $month==8 || $month==10 || $month==12)
	{
		        if($day<=0 || $day>=32)
		        {
                		return ERROR;
        		}
	}

	if($month==4 || $month==6 || $month==9 || $month==11)
	{
		        if($day<=0 || $day>=31)
		        {
                		return ERROR;
        		}
	}

	##check for Feb and leapyears 
	if($month==2)
	{
		if($day == 29) 
		{	
    			if (!((($year%4) == 0)||((($year%100) == 0)&&(($year%400) == 0)))) 
			{
				return ERROR;
    			}
		}
		else
		{
			 if($day > 28)
			{
				return ERROR;
			}
		}	
	}
	return SUCCESS;
}

##Check for "dd/mm/yy hh:mm:ss". See is_date and is_time for more information
sub is_datetime
{
	my $inputstring = shift @_;
	my @fields;
	my $date;
	my $time;
	my $length;

	if(length($inputstring)>=20)
	{
		return ERROR;
	}

	@fields=split(" ",$inputstring);
	$length=@fields;

	if($length!=2)
	{
		return ERROR;
	}

	$date=$fields[0];
	$time=$fields[1];

	if(is_date($date)==ERROR || is_time($time)==ERROR)
	{
		return ERROR;
	}

	return SUCCESS;
}

sub is_dateafter
{
	return SUCCESS;
}

#Check to see if the element is in the array
sub is_in_array
{
	my $element = shift @_;
	my $inputlist = shift @_;
	my $key;

	foreach $key (@$inputlist)
    	{
		if($key==$element)
		{
			return SUCCESS;
		}
    	}
	return ERROR;
}

sub is_key_in_hash
{
        my $element = shift @_;
        my $inputlist = shift @_;
        my $key;
 
        foreach $key (keys %$inputlist)
        {
                if($key eq $element)
                {
                        return SUCCESS;
                }
        }
        return ERROR;
}

sub is_keys_in_hash
{
        my $elements = shift @_;
        my $inputlist = shift @_;
        my $arraykey;
        my $hashkey;
	my $i=0;
	my @returnarray;

	foreach $arraykey (@$elements)
        {
		$returnarray[$i]=$arraykey;
        	foreach $hashkey (keys %$inputlist)
        	{
               		 if($hashkey eq $arraykey)
               		 {
				$returnarray[$i]="";
				$i--;
               		 }
		}
		$i++;
       	 }
        return \@returnarray;
}


#Check each item in list for a valid email address
sub is_email_list
{
	my $inputlist = shift @_;
	my $email;

	foreach $email (@$inputlist)
    	{
		if(is_email($email)==ERROR)
		{
			return ERROR;
		}
    	}
	return SUCCESS;
}

# By convention, end a package file with 1,
# so the use or require command succeeds.
1;
