/*
 * An XML document type.
 * Localname: getDomainTypeRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getDomainTypeRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetDomainTypeRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequestDocument
{
    
    public GetDomainTypeRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETDOMAINTYPEREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDomainTypeRequest");
    
    
    /**
     * Gets the "getDomainTypeRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequest getGetDomainTypeRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequest)get_store().find_element_user(GETDOMAINTYPEREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getDomainTypeRequest" element
     */
    public void setGetDomainTypeRequest(com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequest getDomainTypeRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequest)get_store().find_element_user(GETDOMAINTYPEREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequest)get_store().add_element_user(GETDOMAINTYPEREQUEST$0);
            }
            target.set(getDomainTypeRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "getDomainTypeRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequest addNewGetDomainTypeRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeRequest)get_store().add_element_user(GETDOMAINTYPEREQUEST$0);
            return target;
        }
    }
}
