package com.telstra.netops.dns.server.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.dns.server.exception.DnsApplicationGenericBusinessRuleException;
import com.telstra.netops.dns.server.model.ClientDnsRemoveRequest;
import com.telstra.netops.dns.server.model.ClientPrimaryDnsRequest;
import com.telstra.netops.dns.server.model.DnsPrimaryRecord;
import com.telstra.netops.dns.server.model.Success;

@Service
@Profile("local")
public class LocalPrimaryDnsService implements PrimaryDnsService {

	private static final java.lang.String HEADER = "List of Allowed IPs for DNS Resolver Service";
	private static final java.lang.String RECORDTYPE = "M";
	private static final java.lang.String PARAMETER = "DONT KNOW";
	private static final java.lang.String REQUEST = "EXPECTED";
	Date date = new Date();
	
	@Value("${dns.localtest.success}")
	private boolean success;
	@Override
	public Success add(ClientPrimaryDnsRequest request) {
		if (success) {
			return new Success("SUCCESS!");
		} else {
			throw new DnsApplicationGenericBusinessRuleException("ERROR!!!");
		}
	}


	@Override
	public Success remove(ClientDnsRemoveRequest request) {
		if (success) {
			return new Success("SUCCESS!");
		} else {
			throw new DnsApplicationGenericBusinessRuleException("ERROR!!!");
		}
	}


	@Override
	public List<DnsPrimaryRecord> list(String customerId) {
		List<DnsPrimaryRecord> records = new ArrayList<DnsPrimaryRecord>();
		records.add(new DnsPrimaryRecord("AEQW3433", "yy1234", "123.19.2.0/24", "raj@gmail.com", customerId, HEADER,RECORDTYPE,PARAMETER,RECORDTYPE,"xyz" ,date));
		records.add(new DnsPrimaryRecord("AEQW3433", "ttttt1234wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww", "123.18.2.0/24", "ram@gmail.com", customerId, HEADER,RECORDTYPE,PARAMETER,RECORDTYPE,"gmail", date));
		records.add(new DnsPrimaryRecord("AEQW3433", "ssss1234", "123.17.2.0/24", "uday@gmail.com", customerId, HEADER,RECORDTYPE,PARAMETER,RECORDTYPE,"Abc", date));
		return records;
	}

	/*@Override
	public List<DnsRecord> addRecord(ClientPrimaryDnsRequest request) {
		return getRecordList(null);
	}

	@Override
	public List<DnsRecord> getRecordList(String accountId) {
		List<DnsRecord> records = Arrays.asList(
				new DnsRecord[] {
						new DnsRecord(DnsRecordType.A, "abc@xyz.com", "abc.com", "x.abc.com", "198.10.23.34", null),
						new DnsRecord(DnsRecordType.A, "abc@xyz.com", "abc.com", "y.abc.com", "198.10.23.35", null),
						new DnsRecord(DnsRecordType.A, "abc@xyz.com", "abc.com", "z.abc.com", "198.10.23.36", null),
				}
				);
		
		return records;
	}*/

}
