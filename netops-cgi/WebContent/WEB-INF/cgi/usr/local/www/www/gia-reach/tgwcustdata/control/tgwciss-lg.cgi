#!/usr/local/bin/perl

#
# Written by: Richard Chew, rchew@telstra.net
# TGW Custdata Looking Glass script
#
# $Id: tgwciss-lg.cgi,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $
# 

no warnings;
use DB_File;
$| = 1;
use Net::Telnet;
use Net::Telnet::Cisco;
require "ctime.pl";
use Time::Local;
use Socket;
use POSIX ":sys_wait_h";
use OS_PARA;
my $MB = 1024 * 1024;
my $user = "natm001";
my $passwd = "Reachatm";

our $hostname = $OS_PARA::values{inthostname}{value}; #www.telstraglobal.net
chop($hostname);
$hostname = ($hostname =~ /reachdev.telstra.net/) ? "reachdev.telstra.net" : $hostname;

# First determine which lib file to use
if($hostname =~ /net\.reach\.com/) {
        #require "/usr/local/www/data/gia-reach/tgwcustdata/control/tgwcisslib.pl";
        #require "/usr/local/www/www/gia-reach/tgwcustdata/control/tgwcisslib.pl";
	require "$OS_PARA::values{libfile}{value}";
} else {
        #require "/friends/hthome/htdocs/gia-reach/tgwcustdata/control/tgwcisslib.pl";
        #require "/usr/local/www/www/gia-reach/tgwcustdata/control/tgwcisslib.pl";
	require "$OS_PARA::values{libfile}{value}";
}
our $urlname = $OS_PARA::values{inthostname}{value};

# Referrer re-write URLNAME
if($ENV{HTTP_HOST} =~ /myservices.telstra-global.com|pccw-services.pccw.com|134.159.2.123/) {
        $urlname = $ENV{HTTP_HOST};
        $rmask = 1; #used for remasking as DocRoot is reseller-telstra/reseller-pccw
}



## Definitions ##

our %formels;
our $custfrom = &parse_form_data(*formels);
our $currtime = time;

# first validate user
open(LGDBG, ">/tmp/lookinggl");
print LGDBG "code val user: $formels{'code'}";
our ($result, $vfg, $upass) = &decrypt($formels{'code'});
print LGDBG "L $result: result vfg: $vfg  $upass: upass\n"; 
if($result =~ /FAIL/) {
	&print_error("Cannot validate user: $result");
}

# Set some meaningful vars
our $uid = $result;
our $userid = $uid;
our $oenc = &encrypt($uid,$upass,$currtime);
our ($accno, $staffid) = &ex_staffid($uid);  
our $logo_accno = $accno;

our $tmplroot = $tgwr."control/";

&log_entry($custlogfile, "LG", "$uid");     

# now get template path
our $tmplp = &get_template($accno);
$tmplroot .= $tmplp;

# single sign-on start - Maettr 20/10/2003
our $tdate = gmtime(time);
$tdate .= " GMT";
our $acctype = CMS_OLSS::get_accountattribute_for_accno($accno);
our $mrwaccno;
if($acctype =~ /^RO$/) {
        $mrwaccno = CMS_OLSS::get_mrwaccno_for_ro_acc($accno);
        $mrwaccno = (!$mrwaccno) ? $acctype : $mrwaccno;
}

our $encet = $formels{et};
our $etproduct = &dcrypt($formels{et});
print LGDBG "etproduct:$etproduct\n";
my $ac = ($mrwaccno) ? $mrwaccno : $accno;
if(CMS_OLSS::verify_accno_product($ac, $etproduct) =~ /false/i) {
        &print_error("Account does not have such product.");
}
 
my $letproduct = lc($etproduct);

$tmplroot .= "/" . $letproduct;
# complete set up of variables for single sign-on



our $ctrlname = "looking_glass";

##

SWITCH: {

	($formels{'level'} == 1) && do {
		&local_print1($userid, $oenc, $accno, $tmplroot);
		last SWITCH;
	};
	($formels{'level'} == 2) && do {
		&local_print2($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        }; 
        ($formels{'level'} == 3) && do {
                &local_print3($userid, $oenc, $accno, $tmplroot);
		 last SWITCH;
        }; 
        ($formels{'level'} == 4) && do {
                &local_print4($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        }; 
        ($formels{'level'} == 5) && do {
                &local_print5($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        }; 
        ($formels{'level'} == 6) && do {
                &local_print6($userid, $oenc, $accno, $tmplroot);
                last SWITCH;
        };

	# sub-levels
	$level = $formels{'level'};
        ($formels{'level'} == 41) && do {
		local_trace($oenc, $userid,
			$formels{'POP0'}, $formels{'POP1'}, "Node", $tmplroot, $level);
                last SWITCH;
        };
        ($formels{'level'} == 21) && do {
		&local_trace($oenc, $userid,
			$formels{'POP0'}, $formels{'DEST'}, "PING", $tmplroot, $level);
		last SWITCH;
	};
	($formels{'level'} == 31) && do {
		&local_trace($oenc, $userid,
			$formels{'POP0'}, $formels{'DEST'}, "TRACE", $tmplroot, $level);
		last SWITCH;
	};
		

	&print_error("Level unrecognised");
	exit;
};


# For main service page
# Pre: uid, code, accno, tmplroot
sub local_print1 {
        my($uid, $code, $accno, $tmplroot) = @_;

        chdir($tmplroot);
        
        print "Content-type: text/html\n\n";
        if(-e "$lgctrl") {
		my $II;
                open($II, "$lgctrl");
                while(my $line = <$II>) {
                        $line = &link_format($line, $code, $ctrlname);
                        print $line;
                }
		close($II);
        } else {
                &print_error("Cannot locate service level 1");
        }
}

# Node to host Test Ping
# Pre: uid, code, accno, tmplroot
sub local_print2 {
	my($uid, $code, $accno, $tmplroot) = @_;

	chdir($tmplroot);

	print "Content-type: text/html\n\n";
	my $tmplfile = "$ctrlname/$lgt[0]";
	if(-e "$tmplfile") {
		my $II;
		open($II, "$tmplfile");
		while(my $line = <$II>) {
			$line = &link_format($line, $code, $ctrlname);
                        if($line =~ /\*DATA\*/) {
                                my @vals = &screen_nodetohost($uid, $code, $accno, $tmplroot, $lookglassscript, "2");
                                print "@vals\n";
                        } else {
                                print $line;
                        }
		}
	} else {
		&print_error("Cannot locate service level 2");
	}
}

# Node to host trace - Level 3
# Pre: uid, code, accno, tmplroot
sub local_print3 {
        my($uid, $code, $accno, $tmplroot) = @_;

        chdir($tmplroot);
                         
        print "Content-type: text/html\n\n";
        my $tmplfile = "$ctrlname/$lgt[1]"; 
        if(-e "$tmplfile") {
		my $II;
                open($II, "$tmplfile");
                while(my $line = <$II>) {
                        $line = &link_format($line, $code, $ctrlname);
                        if($line =~ /\*DATA\*/) {

                                my @vals = &screen_nodetohostt($uid, $code, $accno, $tmplroot, $lookglassscript, "3");   
                                print "@vals\n";
                        } else {
                                print $line;
                        }
                }
        } else {
                &print_error("Cannot locate service level 3 ($tmplfile) ($tmplroot)");
        }
}

# Node to Node Test - Reach Node Test
# Pre: uid, code, accno, tmplroot
sub local_print4 {
        my($uid, $code, $accno, $tmplroot) = @_;
 
        chdir($tmplroot);

        print "Content-type: text/html\n\n";
        my $tmplfile = "$ctrlname/$lgt[2]";
        if(-e "$tmplfile") {
		print LGDBG "template file in localprint4: $tmplfile\n";
		my $II;
                open($II, "$tmplfile");
                while(my $line = <$II>) {
                        $line = &link_format($line, $code, $ctrlname);
                        if($line =~ /\*DATA\*/) {
                                my @vals = &screen_nodetonode($uid, $code, $accno, $tmplroot, $lookglassscript, "4");
                                print "@vals\n";
                        } else {
                                print $line;
                        }
                }
        } else {
                &print_error("Cannot locate service level 4 ($tmplfile) ($tmplroot)");
        }
}
close (DEBUG);
# For LG specific routines

### Proc ###

sub local_trace {
	print LGDBG "Inside local trace\n";
	my($code, $uid, $pop, $target, $action, $tmplroot, $level) = @_;

        chdir($tmplroot);
	print LGDBG "template root: $tmplroot\n";

        print "Content-type: text/html\n\n";

        my $tmplfile;

	if($level =~ /^4/) {
		$tmplfile = "$ctrlname/$lgt[2]";
        print LGDBG "Inside lvl4\n";
	} elsif($level =~ /^3/) {
	print LGDBG "Inside lvl3\n";
		$tmplfile = "$ctrlname/$lgt[1]";
	} elsif($level =~ /^2/) {
	print LGDBG "Inside lvl2\n";
		$tmplfile = "$ctrlname/$lgt[0]";
	} else {
	print LGDBG "Inside else(cannot locate lg lvl\n";
		&print_error("Cannot locate lg level", 600, $tmplroot);
	}

        if(-e "$tmplfile") {
	print LGDBG "template file exists: $tmplfile\n";
	my $II;
                open($II, "$tmplfile");
                while(my $line = <$II>) {
			LGDBG->autoflush;
			print LGDBG "Inside while, line is >>>$line<<<\n";
			print LGDBG "code: $code,\n cntrlname: $ctrlname\n";
                        $line = &link_format($line, $code, $ctrlname);
			print LGDBG "line : >>>$line<<<\n";
                        if($line =~ /\*DATA\*/) {
				print LGDBG "Inside if 1:\n";
                                my @vals = &screen_trace($code, $uid, $pop, $target, $action, $tmplroot, $level) ;
				if($vals[0] =~ /error/i) {
					print "@vals\n";
				}
                        } else {  
                                print $line;
                        }
                }
		close($II);
        } else {
                &print_error("Cannot locate service level trace ($tmplfile) ($tmplroot)");
        }
}

####################
#CRQ000000004261
####################


sub screen_trace {
	my($code, $uid, $pop, $target, $action, $tmplroot) = @_;   
        print LGDBG "Inside screen trace : pop:$pop,target:$target,action:$action,tmplroot:$tmplroot\n";
	my $errmsg = "<p><p class=header>Error</p><p class=text>";
	
	if($target =~ /\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\:|\;|\"|\<|\>|\?|\/|\\|\_|\=|\[|\]|\{|\}|\+/) {
		return $errmsg."Reserved characters!";
	}

	%loop_back = ();
	my $vls = &load_pop(\%loop_back);
 open DDD, ">/data1/tmp_log/deep.txt";
	# First get src Reach Node
        # vls has key (digit) => "USA City^router"
        if(not exists $$vls{$pop}) {
        	return $errmsg."Cannot get POP location ($pop)";
        }
                
        my ($rpop, $host) = split(/\^/, $$vls{$pop});
	my $routername = $host;

	if($host !~ /\.$/) {
        	#$host .= ".net.reach.com";
                $host .= ".telstraglobal.net";
	}
	# Now get target - either host or Reach node
	# See if action is node - then target is a reach node
	my $tip = "";
	$target =~ s/ //g;

	my $origtarget = $target;
	if($action =~ /node/i) {
		if(not exists $$vls{$target}) {
			return $errmsg."Cannot get Target Node.";
	}
		my ($rnd, $thost) = split(/\^/, $$vls{$target});
		$origtarget = $rnd;
		if(not exists $loop_back{$pop}) {
			return $errmsg."Cannot get Host Node.";
		}
		else {
			$host = "$loop_back{$pop}";		
		}
		
		if(not exists $loop_back{$target}) {
       		            return $errmsg."Cannot get Target Node.";
	          }
               else {
                      $target = "$loop_back{$target}";
               }

	}
	if($target !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1.3}$/) {
		eval {
			$tip = inet_aton("$target");
		};
		if(!$tip) {
			return $errmsg."Cannot get IP for $origtarget";
		}
		eval { $tip = inet_ntoa($tip); };
	} else {
		$tip = $target;
	}
	print LGDBG "HOST:$host,DEST:$tip\n";

	# Instantiate object
	my $ok = 0;
        my $cs;
	$routername .= '%';
	my $router_software = &get_router_os_looking_glass($routername);
	eval {
	print LGDBG "router : $routername , software : $router_software\n";
                if ($router_software =~ /juniper/i){
			$cs = new Net::Telnet (Timeout => 30);
			$cs->open("$host");
			$cs->login("$user", "$passwd");
			$cs->max_buffer_length(100 * $MB);
	        } else{
			$cs = Net::Telnet::Cisco->new( Host => "$host",
					Timeout => 300 );
			# Login to router
			$cs->login(     Name => 'natm001',
				Password => 'Reachatm',
				Timeout => 300 );
			my @cmd_output = $cs->cmd( 'terminal length 0' );
                }
		$cmd = "trace $tip";
		my @routerval = ();
		my @screenvals = ();
		if($action =~ /trace/i) {
#	        	my @trace = $cs->cmd( String => $cmd,
#        	                      Timeout => 600);
#	        	foreach my $v (@trace) {
#        			next if($v !~ /^\s+\d/);
#	                	push @routerval, $v;
#		        }
			my $ctr = 0;
                        $cs->print("$cmd");
                        while(my $l = $cs->getline(Timeout => 25)) {
                                chomp($l);
				my $countfail = 0;
                                next if($l =~ /(Type|Trac)/i);
				next if($l !~ /(^\s+\d+|msec)/i);
				if($ctr == 0) {
		                        &rt_print_results("$action", $rpop, $tip, $origtarget, "$action from $rpop to $tip ($target):", 1, 0, $l);
		                } else {
		                	&rt_print_results("$action", $rpop, $tip, $origtarget, "$action from $rpop to $tip ($target):", 0, 0, $l);
		                }
				$ctr++;
				$count++ if($l =~ /\*\s+\*\s+\*/);
				last if($l =~ /\s+30\s+/);
				last if($l =~ /$tip/i);
				if($count > 4) {
					$l = "Destination host unreachable";
					&rt_print_results("$action", $rpop, $tip, $origtarget, "$action from $rpop to $tip ($target):", 0, 0, $l);
					last;
				}
                        }
			&rt_print_results("$action", $rpop, $tip, $origtarget, "$action from $rpop to $tip ($target):", 0, 1, $l);

		} else {
			if ($router_software =~ /juniper/i){
                                $cs->max_buffer_length(100 * $MB);
				my $ppcmd = "ping $tip source $host count 10 size 64";
				print DDD "\n $ppcmd\n"; 	
				my @test = $cs->cmd( String => $ppcmd,
                      				Timeout => 10);
                                foreach my $v (@test) {
					next if($v =~ /^\n/);
					next if($v =~ /ping/i);
					next if($v =~ /olss/i);
					next if($v =~ /\!/i);
					push @routerval, $v;
				}
			} else {	
				print LGDBG "Loeeged in to router. Next execute ping\n";
				if($host =~ /^(pad|nzsx|wel|per|syd)/) {
					$cs->print ("enable"); $cs->waitfor('/User.*$/');
					$cs->print("cisadmin"); $cs->waitfor('/Pass.*$/');
					$cs->print("two24pm"); $cs->waitfor('/.*#$/');
				}
				$cs->print("ping"); $cs->waitfor('/Protocol.*$/');
				$cs->print("ip"); $cs->waitfor('/Target.*$/');
				$cs->print("$tip");
				$cs->waitfor(Match => '/Repeat.*$/', Timeout => 10);
				$cs->print("10"); $cs->waitfor('/Datagram.*$/');   
				$cs->print("64"); $cs->waitfor('/Timeout.*$/');
				$cs->print("");
				print LGDBG "before interval prompt\n";
				$cs->waitfor(Match => '/Interval.*$|Extended.*$/');
				my $prmptmatch = $cs->last_prompt;
				print LGDBG "prompt: $prmptmatch\n";
				if($prmptmatch =~ /Interval.*$/){ 
				print LGDBG "matched prompt\n";
				$cs->print("");
				$cs->waitfor('/Extended.*$/');
				$cs->print("y");}
				else {$cs->print("y");}
				if($action =~ /node/i) {
					print LGDBG "Action is node\n";
                                        $cs->waitfor('/Source.*$/'); $cs->print("$host");
                                } else {
                                        $cs->waitfor('/Source.*$/'); $cs->print("");
                                }
				$cs->waitfor('/Type.*$/'); $cs->print("");
				$cs->waitfor('/Set.*$/'); $cs->print("");
				$cs->waitfor('/Validate.*$/'); $cs->print("");
				$cs->waitfor('/Data.*$/'); $cs->print("");
				$cs->waitfor('/Loose.*$/');
				$cs->print("Verbose");
				$cs->waitfor('/Loose.*$/'); $cs->print("");
				$cs->waitfor('/Sweep.*$/'); $cs->print("");
				my $last = 0;
				while( (!$last) && (my $v = $cs->getline(Timeout => 300)) )  {
					next if($v =~ /^\n/);
					next if($v =~ /type escape/i);
					print DDD "\n $v\n";	
					push @routerval, $v;
					$last = 1 if($v =~ /^Succ/);
				}
			}		
		}
		$cs->close;
		$ok = 1;
		if($action !~ /trace/i) {
			&print_results("$action", $rpop, $tip, $origtarget,
			"$action from $rpop to $tip ($target):", $router_software, @routerval);
		}
	};
	if($action =~ /trace/i) {
		if( (!$ok) || ($@) ) {
			&log_entry($internalerror, "LG TRACE", "$@");
			if ($@ !~ /read timed-out*/i) {
				return "</pre>\n\t".$errmsg."Host $target ($tip) not reachable.  This may be due to the existence of a firewall at the target site.";
			}
		}
	} else {
		&log_entry($internalerror, "LG", "$@");
		my $te = "The gateway in $target has currently reached its capacity limit and will temporarily be unavailable from this location.";
		$te .= " Please select another PoP location or retry this location later.";
		return $errmsg.$te
		if( (!$ok) || ($@) );
	}
	close(DDD);
}

#IPSA_CR
sub rt_print_results {
	my($action, $src, $tip, $target, $msg, $init, $end, $val) = @_;
	my $tracestring;
        if($action =~ /trace/i) {
                $tracestring = '<!-- Added by Gary (AGENDA): Start --><div id="mainContent">';
                $tracestring .= '<script language="JavaScript"> pageName = \'ls-screen-4\';</script>';
                $tracestring .= '<!-- Added by Gary (AGENDA): End -->';
        }
        if($init) {
                print <<pr;
$tracestring
<p class="header_IPSA">Results of $action</p>
<p class="text">
$msg
</p><p class="text">
<PRE>
$val

pr
        } elsif($end) {
                print "$val\n";
                print "</pre>\n";
		print "<!-- Added by Gary (AGENDA): Start -->\n </div>\n <!-- Added by Gary (AGENDA): End -->\n";
        }  else {
                print "$val\n";
        }

}

####################
#CRQ000000004261
####################
#IPSA_CR
sub print_results {
	my($action, $src, $tip, $target, $msg, $router_software,@vals) = @_;
	if($action =~ /trace/i) {
		print <<pr;
<p class=header_IPSA>Results of $action</p>
<p class=text>
$msg
<P>
<table cellpadding=3>
pr
		foreach my $l (@vals) {
			chomp($l);
			print "<TR><td class=text>$l</TD></TR>\n";
		}

		print "</table>\n";
	} else {
		$tip = ($action =~ /node/i) ? "" : "($tip)";
		if($router_software =~ /juniper/i) {
			&process_ping_juniper($src, $target, $tip, @vals);
                } else {
			&process_ping($src, $target, $tip, @vals);
                }                
	}
}
#Added by deepti on 02-10-2009 
# Input Parameters : ping command output
# Output Parameters : %loss,min latency,max latency and average latency
sub process_ping_juniper {
my($src, $target, $tip, @vals) = @_;
my @testresults = ();
        my($loss, $min, $avg, $max);
my $date = gmtime(time);
        chomp($date);
        $date .= " GMT";
foreach my $v (@vals) {
        print LGDBG "v is $v\n";
                chomp($v);
if($v =~ /icmp_seq/i){
                my($var1,$var2,$var3,$var4) = split(/=/, $v);
                my @t1 = split(/ /, $var4);
                my $val1 = $t1[$#t1-1];
                push @testresults, $val1;
                }
if($v =~ /^round-trip/i) {
#call function proc_rtt_juniper if the ping output consists of round-trip
                ($min, $avg, $max, $stddev) = &proc_rtt_juniper($v);
                }
                 if($v =~ /packets/i){
                 $loss = &proc_loss_juniper($v);
}
}
print<<pp;
<!-- Added by Gary (AGENDA): Start -->
<div id="mainContent">
<script language="JavaScript">
  pageName = 'ls-screen-2';
</script>
<!-- Added by Gary (AGENDA): End -->
<!-- IPSA_CR Maneet Kaur -->
<p class=header_IPSA>Node to Target Statistics</p>
<p class=text>
The real-time statistics between $src and $target $tip are displayed below.
<P>
<table border=1 cellpadding=3 width=70% BORDERCOLOR=#FFFFFF>
<TR><td class=text width=100% colspan=10 ALIGN=CENTER BGCOLOR=#007AC9><font face="Verdana" color=#FFFFFF>$date</font></TD></TR>
<TR><td class=text width=100% colspan=10 ALIGN=CENTER BGCOLOR=#007AC9><font face="Verdana" color=#FFFFFF><B>$src</B> to <B>$target</B></font></TD></TR>
<TR><TD class=text ALIGN=CENTER WIDTH=50% colspan=5>Min Latency (mS): $min</TD>
<TD class=text ALIGN=CENTER WIDTH=50% colspan=5>Max Latency (mS): $max</TD></TR>
<TR><TD class=text ALIGN=CENTER WIDTH=50% colspan=5 BGCOLOR=#EFEFEF>Avg Latency (mS): $avg</TD>
<TD class=text ALIGN=CENTER WIDTH=50% colspan=5 BGCOLOR=#EFEFEF>Std Deviation (mS): $stddev</TD></TR>
<TR><td class=text width=100% colspan=10 ALIGN=CENTER>Avg Loss (%): $loss</TD></TR>
<TR><td class=text width=100% colspan=10 ALIGN=CENTER BGCOLOR=#007AC9><font face="Verdana" color=#FFFFFF><B>Test Results (mS)</B></font></TD></TR>
pp

        my $idx = 0;

        if($#testresults != 9) {
                for my $i ($#testresults+1 .. 9) {
                        push @testresults, '-';
                }
        }

        foreach my $vx (@testresults) {
                if($idx == 0) {
                        print "<TR><td class=text colspan=2 WIDTH=20% ALIGN=CENTER>";
                } elsif($idx == 5) {
			print "</TR>\n<TR BGCOLOR=#EFEFEF><td class=text colspan=2 WIDTH=20% ALIGN=CENTER>";
                        $idx = 0;
                } else {
                        print "<td class=text colspan=2 ALIGN=CENTER WIDTH=20%>";
                }
                $idx++;
                print "$vx</TD>";
        }
        print "</TR></table></div>\n";
}

sub process_ping {
	my($src, $target, $tip, @vals) = @_;

	my @testresults = ();
	my($loss, $min, $avg, $max);

	my $date = gmtime(time);
	chomp($date);
	$date .= " GMT";
	foreach my $v (@vals) {
		chomp($v);
		if($v =~ /Reply to request/i) {
			my @t = split(/\s+/, $v);
			my $val = $t[$#t-1];
			$val =~ s/\(//g;
			push @testresults, $val;
		}
		if($v =~ /^Success rate/i) {
                ($loss, $min, $avg, $max) = &proc_rtt($v);
                }
	}
	my $stddev = &calc_stddev(@testresults);

	print<<pp;
<!-- Added by Gary (AGENDA): Start -->
<div id="mainContent">
<script language="JavaScript">
  pageName = 'ls-screen-2';
</script>
<!-- Added by Gary (AGENDA): End -->
<p class=header_IPSA>Node to Target Statistics</p>
<p class=text>
The real-time statistics between $src and $target $tip are displayed below.
<P>
<table border=1 cellpadding=3 width=70% BORDERCOLOR=#FFFFFF>
<!-- IPSA_CR Chandini -->
<TR><td class=text width=100% colspan=10 ALIGN=CENTER BGCOLOR=#007AC9><font face="Verdana" color=#FFFFFF>$date</font></TD></TR>
<TR><td class=text width=100% colspan=10 ALIGN=CENTER BGCOLOR=#007AC9><font face="Verdana" color=#FFFFFF><B>$src </B>to <B> $target</B></font></TD></TR>
<TR><TD class=text ALIGN=CENTER WIDTH=50% colspan=5>Min Latency (mS): $min</TD>
<TD class=text ALIGN=CENTER WIDTH=50% colspan=5>Max Latency (mS): $max</TD></TR>
<TR><TD class=text ALIGN=CENTER WIDTH=50% colspan=5 BGCOLOR=#EFEFEF>Avg Latency (mS): $avg</TD>
<TD class=text ALIGN=CENTER WIDTH=50% colspan=5 BGCOLOR=#EFEFEF>Std Deviation (mS): $stddev</TD></TR>
<TR><td class=text width=100% colspan=10 ALIGN=CENTER>Avg Loss (%): $loss</TD></TR>
<TR><td class=text width=100% colspan=10 ALIGN=CENTER BGCOLOR=#007AC9><font face="Verdana" color=#FFFFFF><B>Test Results (mS)</B></font></TD></TR>
pp

	my $idx = 0;

	if($#testresults != 9) {
		for my $i ($#testresults+1 .. 9) {
			push @testresults, '-';
		}
	}

	foreach my $vx (@testresults) {
		if($idx == 0) {
			print "<TR><td class=text colspan=2 WIDTH=20% ALIGN=CENTER>";
		} elsif($idx == 5) {
			print "</TR>\n<TR BGCOLOR=#EFEFEF><td class=text colspan=2 WIDTH=20% ALIGN=CENTER>";
			$idx = 0;
		} else {
			print "<td class=text colspan=2 ALIGN=CENTER WIDTH=20%>";
		}
		$idx++;
		print "$vx</TD>";
	}
	print "</TR></table></div>\n";
		
}
sub proc_loss_juniper {
my($line) = shift;
chomp($line);
my($a,$b,$c) = split(/\,/, $line);
print LGDBG "$c\n";
my($c,$d,$e) = split(/ /,$c);
$d =~ s/%//;
return $d;
}
#Added by Deepti on 03-10-2009 for juniper routers(VPLS and EVPL) to calculate min,max,avg latency and std deviation 
sub proc_rtt_juniper {
        my($line) = shift;
        chomp($line);
        my($a, $b) = split(/\=/, $line);

        # calculate RTT
        $b =~ s/ //g; $b =~ s/ms//g;
        my($min, $avg, $max, $stddev) = split(/\//, $b);
        $min = sprintf("%2.2f", $min);
        $avg = sprintf("%2.2f", $avg);
        $max = sprintf("%2.2f", $max);
        $stddev = sprintf("%2.2f", $stddev);
        return ($min, $avg, $max, $stddev);
}

#
# Pre: line from ping output from Cisco
# Post: percentage_loss, minrtt, avgrtt, maxrtt
#
sub proc_rtt {
        my($line) = shift;
        chomp($line);
        my($a, $b) = split(/\,/, $line);
        # Calculate percentage success
        $a = (split(/\(/, $a))[1];
        $a =~ s/\)//g;
        my($sent, $count) = split(/\//, $a);
        my $rate = ($count - $sent) / 10;
        $rate = $rate * 100;                        
 
        # calculate RTT  
        $b = (split(/\=/, $b))[1];
        $b =~ s/ //g; $b =~ s/ms//g;
        my($min, $avg, $max) = split(/\//, $b);
        $min = sprintf("%2.2f", $min);
        $avg = sprintf("%2.2f", $avg);
        $max = sprintf("%2.2f", $max); 
        
        return ($rate, $min, $avg, $max);
}               

sub calc_stddev {
        my(@vals) = @_; 

	return 0 if($#vals < 0);

        #my @vals = split(/\s+/, $line); 
        # 0 = timestamp - not required
        #shift(@vals);
        my $i = ($#vals) ? $#vals + 1 : 1;
        my $avg = my $sum = my $variance = my $stddev = 0;
        foreach my $v (@vals) { 
                $sum += $v;
        }
	#print "($avg) ($sum) ($i) (@vals) ($#vals)\n";
        $avg = $sum / $i;

        $sum = 0;
        foreach my $v (@vals) {
                $v -= $avg;
                $v = $v * $v;
                $sum += $v;
        }

        $variance = $sum / $i;

        $stddev = sqrt($variance);
        $stddev = sprintf("%2.2g", $stddev);
        return $stddev;  
}
 
sub p_error {
	my($action, $msg, $exit, $tmplroot) = @_;

        chdir($tmplroot);
        
        if(-e "$errortmpl") {
		my $II;
                open($II, $errortmpl);
                print "Content-type: text/html\n\n";
                while(my $line = <$II>) {
                        $line = &link_format($line, $code, $ctrlname);
                        if($line =~ /\*DATA\*/) {
                                print "$errmsg\n";
                        } else {
                                print $line;
                        }
                }
        } else {
                print "Content-type: text/html\n\n";
                print "<HEAD><TITLE>Error !</TITLE></HEAD>\n";
                print "<BODY BGCOLOR=#FFFFFF TEXT=#000000 LINK=#FF0000>\n";
                print "<h3><I>Error !</I></h3><P><HR><P>\n";
                print "An error was encountered. <P>";
                print "The error is : $errmsg. ";
                #print "<P><HR><P><A HREF=\"http://www.net.reach.com/index.html\">Back to Login</A>";
                #print "<P><HR><P><A HREF=\"http://www.net.reach.com/index.html\">Back to Login</A>";
                print "<P><HR><P><A HREF=\"$urlname\">Back to Login</A>";
                print "<P><HR><P><I>olss\@net.reach.com</I></BODY>";
        }

        exit(0); 
}

sub sortnum { $a <=> $b }

sub gen_trace {
	my ($v, $digit) = @_;

	my @trace = ();
	push @trace, '<select name="POP'.$digit.'">';

	foreach my $k (sort sortnum  keys %$v) {
		my $pop = (split(/\^/, $$v{$k}))[0];
		push @trace, '<option value="'.$k.'">'."$pop</option>";
	}
	push @trace, "</select>";

	return @trace;
}
