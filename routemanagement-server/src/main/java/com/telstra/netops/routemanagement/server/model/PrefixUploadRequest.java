package com.telstra.netops.routemanagement.server.model;

/**
 * Modal class for Single prefix upload
 *
 */
public class PrefixUploadRequest {

	private int level;
	private String service;
	private String networkprefix;
	private String maintainedby;
	private String origin;
	private String emailId;
	private String route;
	private String ipVersion;
	private String submit;
	private String customerId;
	private String fileContent;
	private String userProfile;

	public int getLevel() {
		return level;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getNetworkprefix() {
		return networkprefix;
	}

	public void setNetworkprefix(String networkprefix) {
		this.networkprefix = networkprefix;
	}

	public String getMaintainedby() {
		return maintainedby;
	}

	public void setMaintainedby(String maintainedby) {
		this.maintainedby = maintainedby;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(String ipVersion) {
		this.ipVersion = ipVersion;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
}
