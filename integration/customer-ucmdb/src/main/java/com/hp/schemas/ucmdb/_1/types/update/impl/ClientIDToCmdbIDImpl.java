/*
 * XML Type:  ClientIDToCmdbID
 * Namespace: http://schemas.hp.com/ucmdb/1/types/update
 * Java type: com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.update.impl;
/**
 * An XML ClientIDToCmdbID(@http://schemas.hp.com/ucmdb/1/types/update).
 *
 * This is a complex type.
 */
public class ClientIDToCmdbIDImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID
{
    
    public ClientIDToCmdbIDImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLIENTID$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/update", "clientID");
    private static final javax.xml.namespace.QName CMDBID$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/update", "cmdbID");
    
    
    /**
     * Gets the "clientID" element
     */
    public java.lang.String getClientID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "clientID" element
     */
    public org.apache.xmlbeans.XmlString xgetClientID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CLIENTID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "clientID" element
     */
    public void setClientID(java.lang.String clientID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLIENTID$0);
            }
            target.setStringValue(clientID);
        }
    }
    
    /**
     * Sets (as xml) the "clientID" element
     */
    public void xsetClientID(org.apache.xmlbeans.XmlString clientID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CLIENTID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CLIENTID$0);
            }
            target.set(clientID);
        }
    }
    
    /**
     * Gets the "cmdbID" element
     */
    public java.lang.String getCmdbID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CMDBID$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "cmdbID" element
     */
    public org.apache.xmlbeans.XmlString xgetCmdbID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CMDBID$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "cmdbID" element
     */
    public void setCmdbID(java.lang.String cmdbID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CMDBID$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CMDBID$2);
            }
            target.setStringValue(cmdbID);
        }
    }
    
    /**
     * Sets (as xml) the "cmdbID" element
     */
    public void xsetCmdbID(org.apache.xmlbeans.XmlString cmdbID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CMDBID$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CMDBID$2);
            }
            target.set(cmdbID);
        }
    }
}
