/*
 * An XML document type.
 * Localname: getImpactRulesByNamePrefixResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * A document containing one getImpactRulesByNamePrefixResponse(@http://schemas.hp.com/ucmdb/1/params/impact) element.
 *
 * This is a complex type.
 */
public class GetImpactRulesByNamePrefixResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponseDocument
{
    
    public GetImpactRulesByNamePrefixResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETIMPACTRULESBYNAMEPREFIXRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactRulesByNamePrefixResponse");
    
    
    /**
     * Gets the "getImpactRulesByNamePrefixResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse getGetImpactRulesByNamePrefixResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse)get_store().find_element_user(GETIMPACTRULESBYNAMEPREFIXRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getImpactRulesByNamePrefixResponse" element
     */
    public void setGetImpactRulesByNamePrefixResponse(com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse getImpactRulesByNamePrefixResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse)get_store().find_element_user(GETIMPACTRULESBYNAMEPREFIXRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse)get_store().add_element_user(GETIMPACTRULESBYNAMEPREFIXRESPONSE$0);
            }
            target.set(getImpactRulesByNamePrefixResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getImpactRulesByNamePrefixResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse addNewGetImpactRulesByNamePrefixResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse)get_store().add_element_user(GETIMPACTRULESBYNAMEPREFIXRESPONSE$0);
            return target;
        }
    }
}
