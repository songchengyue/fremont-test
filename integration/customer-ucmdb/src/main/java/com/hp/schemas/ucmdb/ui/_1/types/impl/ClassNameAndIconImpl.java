/*
 * XML Type:  ClassNameAndIcon
 * Namespace: http://schemas.hp.com/ucmdb/ui/1/types
 * Java type: com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.ui._1.types.impl;
/**
 * An XML ClassNameAndIcon(@http://schemas.hp.com/ucmdb/ui/1/types).
 *
 * This is a complex type.
 */
public class ClassNameAndIconImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcon
{
    
    public ClassNameAndIconImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLASSNAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/ui/1/types", "className");
    private static final javax.xml.namespace.QName ICONRELATIVEPATH$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/ui/1/types", "iconRelativePath");
    
    
    /**
     * Gets the "className" element
     */
    public java.lang.String getClassName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLASSNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "className" element
     */
    public org.apache.xmlbeans.XmlString xgetClassName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CLASSNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "className" element
     */
    public void setClassName(java.lang.String className)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLASSNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLASSNAME$0);
            }
            target.setStringValue(className);
        }
    }
    
    /**
     * Sets (as xml) the "className" element
     */
    public void xsetClassName(org.apache.xmlbeans.XmlString className)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CLASSNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CLASSNAME$0);
            }
            target.set(className);
        }
    }
    
    /**
     * Gets the "iconRelativePath" element
     */
    public java.lang.String getIconRelativePath()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ICONRELATIVEPATH$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "iconRelativePath" element
     */
    public org.apache.xmlbeans.XmlString xgetIconRelativePath()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ICONRELATIVEPATH$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "iconRelativePath" element
     */
    public void setIconRelativePath(java.lang.String iconRelativePath)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ICONRELATIVEPATH$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ICONRELATIVEPATH$2);
            }
            target.setStringValue(iconRelativePath);
        }
    }
    
    /**
     * Sets (as xml) the "iconRelativePath" element
     */
    public void xsetIconRelativePath(org.apache.xmlbeans.XmlString iconRelativePath)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ICONRELATIVEPATH$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ICONRELATIVEPATH$2);
            }
            target.set(iconRelativePath);
        }
    }
}
