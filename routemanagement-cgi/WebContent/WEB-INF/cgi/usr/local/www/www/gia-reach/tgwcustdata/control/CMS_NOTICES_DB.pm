#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 12 September 2001
# File: CMS_NOTICES_DB.pm
#
# $Id: CMS_NOTICES_DB.pm,v 1.1.1.1 2003/10/19 13:49:52 rxc Exp $

package CMS_NOTICES_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;

sub add_notice {
  
  my $username = $dbh->quote(shift @_);
  my $subject  = $dbh->quote(shift @_);
  my $message  = $dbh->quote(shift @_);

  my @now = gmtime(time);

 
  my $sth = $dbh->prepare("insert into notice
                                 (timestamp, subject, message, username)
                           values(CURRENT_TIMESTAMP, $subject, $message, 
                                  $username)");
  $sth->execute;

 


}



sub get_notice_list {

  my $timestamp_start = shift @_ || "";
  my $timestamp_end = shift @_ || "";
  my $subject = shift @_ || "";
  my $message = shift @_ || "";
  my $username = shift @_ || "";

  my $command = "select timestamp, subject, substring(message for 40), username from notice ";

  my $extension = "";

  if ($timestamp_start ne "") {
    $extension .= "and timestamp >= '$timestamp_start'
                   and timestamp <= '$timestamp_end' ";
  }

  if ($subject ne "") {
    $extension .= "and subject ilike '%$subject%' ";
  }
 
  if ($message ne "") {
    $extension .= "and message ilike '%$message%' ";
  }
  
  if ($username ne "") {
    $extension .= "and username ilike '%$username%' ";
  }

 
  $extension =~ s/and/where/;
  
  $command .= "$extension order by timestamp DESC ";
  
  if ($extension eq "") {
    # limit by default
    $command .= "limit 20";
  }
  
  
  my $sth = $dbh->prepare($command);

  

  $sth->execute;

  my $values = $sth->fetchall_arrayref;
 
  return $values;

}

sub get_notice {
  my $timestamp = shift @_;

  if ($timestamp =~ /(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/) {
    $timestamp = "'$1-$2-$3 $4:$5:$6+$7'";
  }

  warn $timestamp;
  
  my $sth = $dbh->prepare("select  timestamp, subject, message, username
                           from notice
                           where timestamp = $timestamp");

  $sth->execute;

  my @values = $sth->fetchrow_array;
  $sth->finish;
 
  return @values;

}



1;
