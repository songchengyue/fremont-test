package com.hp.ucmdb.generated;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.hp.schemas.ucmdb._1.types.Conditions;
import com.hp.schemas.ucmdb._1.types.StrCondition;
import com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator;
import com.hp.schemas.ucmdb._1.types.StrConditions;
import com.hp.schemas.ucmdb._1.types.StrProp;
import com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.schemas.ucmdb._1.params.history.GetChangedCIs;
import com.hp.schemas.ucmdb._1.params.history.GetChangedCIsDocument;
import com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponseDocument;
import com.hp.schemas.ucmdb._1.params.query.GetCIsById;
import com.hp.schemas.ucmdb._1.params.query.GetCIsByIdDocument;
import com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponseDocument;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator.Enum;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeDocument;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponseDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponseDocument;
import com.hp.schemas.ucmdb._1.params.query.ReleaseChunks;
import com.hp.schemas.ucmdb._1.params.query.ReleaseChunksDocument;
import com.hp.schemas.ucmdb._1.types.props.CustomProperties;
import com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties;
import com.hp.schemas.ucmdb._1.types.props.PropertiesList;
import com.hp.schemas.ucmdb._1.types.props.TypedProperties;
import com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection;
import com.hp.schemas.ucmdb._1.types.*;

public class UcmdbLg {
	private static final Logger LOGGER = LoggerFactory.getLogger(UcmdbNetopsSynch.class);
	static String configPath;
	static  UcmdbServiceStub stub = null;
	static int numberOfChunks;
	static String key1;
	static String key2;
	static int chunkNumber = 1;
	private static final String CLASS_NAME = "customer_facing_service";
	private static final String CALLER_APPLICATION = "netops";
	static int arrayLength ;
	static String cndtnLgclOprtrAnd = "AND";
	private static final String CNDTN_LGCL_OPRTR_OR = "OR";
	private static final String CONDITION_NAME = "product_name";
	static String conditionValue1 = "EVPL";
	static String conditionValue2 = "EVPL Transparent Service";
	static String conditionValue3 = "EVPL VLAN Port";
	static String conditionValue4 = "EVPL VLAN Virtual Circuit";
	private static final String CONDITION_VALUE5 = "Global Internet Direct  ";
	private static final String CONDITION_VALUE6 = "Global Internet Direct Economy  ";
	private static final String CONDITION_VALUE7 = "Global Internet Direct Economy Plus  ";
	private static final String CONDITION_VALUE8 = "IP Transit Premium ";
	private static final String CONDITION_VALUE9 = "IP Transit Standard ";
	private static final String STR_OPRTR = "EqualIgnoreCase";
	private static final String PROPERTY_NAME1 = "product_line";
	private static final String PROPERTY_NAME2 = "service_id";
	private static final String PROPERTY_NAME3 = "telstra_service_id";;
	private static final String PROPERTY_NAME4 = "as_number";
	private static final String PROPERTY_NAME5 = "cust_id";
	private static final String PROPERTY_NAME6 = "product_name";
	private static final String PROPERTY_NAME7 = "last_modified_time";
	//private static final String PROPERTY_NAME8 = "cfs_id";
	
	static String label;
	static String strngId;
	static String type ;
	static ID Id;
	static List<String> listHolder = new ArrayList<String>();
	static List<String> getCIsArray = new ArrayList<String>();
	static String getCIsId;
	static String getCIsType;
	static String getCIsName;
	static String getCIsValue;
	static String getName;
	static String getValue;
	static int getCIsChunkNo;
	static String getCIsKey1;
	static String getCIsKey2;
	static String strngCIsId;
	
	//Considering Full Load
	//static String fullOrChangedLoad = "delta";
	private static final String FULL_OR_CHANGED_LOAD = "full";
	static String tableName = "";

	static Properties props = new Properties();
	
	static Connection dbConnection;
	static Statement statement;
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	//Initializing column values
	static String serviceId = "";
	static String custId = "";
	static String telstraId = "";
	static String productline = "";
	static String asNo = "";
	static String productName = "";
	static String cfsId = "";
	static String cmdbId = "";
	static int syncId = 1;
	static String syncStatus = "";
	static String oprtnStatus = "Y";
	static String syncType = "";
	
	//Initializing to default values
	static int frmYear = 2016;
	static int frmMonth = 05;
	static int frmDate = 10;
	static int frmHourOfDay = 00;
	static int frmMinute = 00;
	static int frmSecond = 00;
	
	static int toYear = 2016;
	static int toMonth = 05;
	static int toDte = 14;
	static int toHourOfDay = 00;
	static int toMinute = 00;
	static int toSecond = 00;
	
	static String sync_start_date = "";
	
	static String fullloadinserttimestamp = "";
	static String deltaloadinserttimestamp = "";
	
//public static void getCfsDataLg(UcmdbServiceStub ucmdbStub,String mainConfigPath) {
	public static void getCfsDataLg(UcmdbServiceStub ucmdbStub,String mainConfigPath) {
		stub = ucmdbStub;
		configPath = mainConfigPath;

		
    	try {
    		loadParams(configPath);   
    		insertRecordIntoSyncStatusInitial();
        	//getFilteredCIsByTypeDemo();
        	if(FULL_OR_CHANGED_LOAD.equalsIgnoreCase("delta"))
        	{
        	LOGGER.info("oprtnStatus : " + oprtnStatus);
        	/*updateRecordInSyncStatus();*/
        	}
		} catch (Exception e) {
			LOGGER.error("exception inside main", e);
		}
    	
	}

private static void insertRecordIntoSyncStatusInitial() {
		LOGGER.info("inside insertRecordIntoSyncStatusInitial method");
		syncStatus = "STARTED";
		syncType = "DELTA";
		if(FULL_OR_CHANGED_LOAD.equalsIgnoreCase("full"))
		{
			syncType = "FULL";
		}
		sync_start_date = getCurrentTimeStamp();
		
			try {
				dbConnection = getDBConnection();
				statement = dbConnection.createStatement();
				getFilteredCIsByTypeDemo();				
				
			} catch (SQLException sqle) {
				LOGGER.error("exception inside insertRecordIntoSyncStatusInitial", sqle);
			} catch (Exception e){
			/*	oprtnStatus = "N";
				updateRecordInSyncStatus();*/
				LOGGER.error("exception inside insertRecordIntoSyncStatusInitial", e);
			}
		
}

			/* getFilteredCIsByType Operation*/
      	public static void getFilteredCIsByTypeDemo() {
      		LOGGER.info("inside getFilteredCIsByTypeDemo method");

				try {
					Enum conditionsLogicalOperator = Enum.forString(CNDTN_LGCL_OPRTR_OR);
					
					StrCondition strCondition5 =  StrCondition.Factory.newInstance();
					StrProp strProp5 = StrProp.Factory.newInstance();
					strProp5.setName(CONDITION_NAME);
					strProp5.setValue(CONDITION_VALUE5);
					strCondition5.setCondition(strProp5);
					strCondition5.setStrOperator(StrOperator.Enum.forString(STR_OPRTR));
					
					StrCondition strCondition6 =  StrCondition.Factory.newInstance();
					StrProp strProp6 = StrProp.Factory.newInstance();
					strProp6.setName(CONDITION_NAME);
					strProp6.setValue(CONDITION_VALUE6);
					strCondition6.setCondition(strProp6);
					strCondition6.setStrOperator(StrOperator.Enum.forString(STR_OPRTR));
					
					StrCondition strCondition7 =  StrCondition.Factory.newInstance();
					StrProp strProp7 = StrProp.Factory.newInstance();
					strProp7.setName(CONDITION_NAME);
					strProp7.setValue(CONDITION_VALUE7);
					strCondition7.setCondition(strProp7);
					strCondition7.setStrOperator(StrOperator.Enum.forString(STR_OPRTR));
					
					StrCondition strCondition8 =  StrCondition.Factory.newInstance();
					StrProp strProp8 = StrProp.Factory.newInstance();
					strProp8.setName(CONDITION_NAME);
					strProp8.setValue(CONDITION_VALUE8);
					strCondition8.setCondition(strProp8);
					strCondition8.setStrOperator(StrOperator.Enum.forString(STR_OPRTR));
					
					StrCondition strCondition9 =  StrCondition.Factory.newInstance();
					StrProp strProp9 = StrProp.Factory.newInstance();
					strProp9.setName(CONDITION_NAME);
					strProp9.setValue(CONDITION_VALUE9);
					strCondition9.setCondition(strProp9);
					strCondition9.setStrOperator(StrOperator.Enum.forString(STR_OPRTR));
					
					
					Conditions conditions = Conditions.Factory.newInstance();
					StrConditions strConditions= StrConditions.Factory.newInstance();
					StrCondition[] strConditionArray = {strCondition5,strCondition6,strCondition7,strCondition8,strCondition9};
					
					
					strConditions.setStrConditionArray(strConditionArray);
					conditions.setStrConditions(strConditions);
					
					CmdbContext cntxt = CmdbContext.Factory.newInstance();
					cntxt.setCallerApplication(CALLER_APPLICATION);
					
					GetFilteredCIsByType getFilteredCIsByType = GetFilteredCIsByType.Factory.newInstance();
					getFilteredCIsByType.setCmdbContext(cntxt);
					getFilteredCIsByType.setType(CLASS_NAME);
					getFilteredCIsByType.setConditions(conditions);
					getFilteredCIsByType.setConditionsLogicalOperator(conditionsLogicalOperator);
					
					//Considering Full Load
					if(FULL_OR_CHANGED_LOAD.equalsIgnoreCase("full"))
					{
						LOGGER.info("within set inputs for full load");
					CustomProperties properties = CustomProperties.Factory.newInstance();
					PropertiesList propertiesList= PropertiesList.Factory.newInstance();
					propertiesList.addPropertyName(PROPERTY_NAME1);
					propertiesList.addPropertyName(PROPERTY_NAME2);
					propertiesList.addPropertyName(PROPERTY_NAME3);
					propertiesList.addPropertyName(PROPERTY_NAME4);
					propertiesList.addPropertyName(PROPERTY_NAME5);
					propertiesList.addPropertyName(PROPERTY_NAME6);
					propertiesList.addPropertyName(PROPERTY_NAME7);
					//propertiesList.addPropertyName(PROPERTY_NAME8);
					properties.setPropertiesList(propertiesList);
					getFilteredCIsByType.setProperties(properties);
					}
					//Considering Full Load
					
					GetFilteredCIsByTypeDocument request = GetFilteredCIsByTypeDocument.Factory.newInstance();
					request.setGetFilteredCIsByType(getFilteredCIsByType);
					
					LOGGER.info("set inputs");
					
					HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
					basicAuthentication.setUsername("admin");
					basicAuthentication.setPassword("admin");
					int so_timeout = 240000;
					int con_timeout = 10000; 
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
					LOGGER.info("authentication passed");
	
					GetFilteredCIsByTypeResponseDocument response = stub.getFilteredCIsByType(request);
										
					ChunkInfo chunkInfo = response.getGetFilteredCIsByTypeResponse().getChunkInfo();
					numberOfChunks = chunkInfo.getNumberOfChunks();
					ChunkKey chunkKey = chunkInfo.getChunksKey();
					key1 = chunkKey.getKey1();
					key2 = chunkKey.getKey2();
					
					LOGGER.info("numberOfChunks : " + numberOfChunks);
					LOGGER.info("key1 : " + key1);
					LOGGER.info("key2 : " + key2);

					
					
					//Considering Delta Load
					if(FULL_OR_CHANGED_LOAD.equalsIgnoreCase("delta"))
					{
						LOGGER.info("fullOrChangedLoad : "+FULL_OR_CHANGED_LOAD);
						for(int i=1 ; i <= numberOfChunks ; i++)
						{
							chunkNumber = i;
							LOGGER.info("chunkNumber:" +chunkNumber);
						pullTopologyMapChunksDemo();
						}
					}
					else
					{
						//Number Of Chunks is zero
						if(numberOfChunks==0)
						{
							LOGGER.info("within zero numberOfChunks : " + numberOfChunks);
							CI[] CIs = response.getGetFilteredCIsByTypeResponse().getCIs().getCIArray();

							
							String[] fullcmdbId = new String[600];
							String[] fullserviceId = new String[600];
							String[] fullcustId = new String[600];
							String[] fulltelstraId = new String[600];
							String[] fullproductLine = new String[600];
							String[] fullproductName = new String[600];
							String[] fullasNo = new String[600];
							Timestamp[] fulllastModTime = new Timestamp[600];
							//String[] fullcfsId = new String[600];
							
							for(int s=0 ; s < CIs.length ; s++)
							{						
								{						
									fullcmdbId[s] = CIs[s].getID().getStringValue();
									listHolder.add(strngId);
									type = CIs[s].getType();
									
									//Considering Full Load
									if(FULL_OR_CHANGED_LOAD.equalsIgnoreCase("full"))
									{
										StrProp[] strprop = CIs[s].getProps().getStrProps().getStrPropArray();
										DateProp[] dateprp = CIs[s].getProps().getDateProps().getDatePropArray();
										
										for (int p = 0; p < strprop.length; p++)
										{									
											getName = strprop[p].getName();
											getValue = strprop[p].getValue();
											
											if(getName.equalsIgnoreCase("service_id")){
												fullserviceId[s] = getValue;
											}
											if(getName.equalsIgnoreCase("cust_id")){
												fullcustId[s] = getValue;
											}
											if(getName.equalsIgnoreCase("telstra_service_id")){
												fulltelstraId[s] = getValue;
											}
											if(getName.equalsIgnoreCase("product_line")){
												fullproductLine[s] = getValue;
											}
											if(getName.equalsIgnoreCase("product_name")){
												fullproductName[s] = getValue;
											}
											if(getName.equalsIgnoreCase("as_no")){
												fullasNo[s] = getValue;
											}
											/*if(getName.equalsIgnoreCase("cfs_id")){
												fullcfsId[s] = getValue;
											}*/
											//fulllastModTime
										}
										for (int q = 0; q < dateprp.length; q++){
											if(dateprp[q].getName().equalsIgnoreCase("last_modified_time")){
												fulllastModTime[s] = new Timestamp(dateprp[q].getValue().getTime().getTime());
											}
										}
											//mergeRecordIntoTable();																		
									}
									
								}
							}
							
							LOGGER.info("DELETING");
								try {
									dbConnection = getDBConnection();
									Statement delstmt = dbConnection.createStatement();
									delstmt.executeQuery("delete from CDM_CFS_1_FULLLOAD_STG");
									
									LOGGER.info("preparing zero chunk batch");
									String stringSQL = "insert into CDM_CFS_1_FULLLOAD_STG (cmdb_id, a_service_id, a_cust_id, a_telstra_service_id, a_product_line, a_product_name, a_as_number, a_cfs_id, timestamp) values (?,?,?,?,?,?,?,?,?)";
									PreparedStatement ps = dbConnection.prepareStatement(stringSQL);

									for (int k = 0; k < CIs.length; k++){
										ps.setString(1, fullcmdbId[k]);
										ps.setString(2, fullserviceId[k]);
										ps.setString(3, fullcustId[k]);
										ps.setString(4, fulltelstraId[k]);
										ps.setString(5, fullproductLine[k]);
										ps.setString(6, fullproductName[k]);
										ps.setString(7, fullasNo[k]);
										ps.setString(8,null);
										ps.setTimestamp(9, fulllastModTime[k]);	
										ps.addBatch();
									}				
									LOGGER.info("executing zero chunk batch");
									ps.executeBatch();
									ps.close();
									dbConnection.close();
								} catch (java.sql.BatchUpdateException bue) {
									/*oprtnStatus = "N";
									updateRecordInSyncStatus();*/
				 					LOGGER.error("BatchUpdateException inside getFilteredCIsByTypeDemo insertIntoFull", bue);	
								} catch (SQLException sqle) {
									//LOGGER.error("SQLException inside getFilteredCIsByTypeDemo insertIntoFull", sqle);
								} catch (Exception e) {
									/*oprtnStatus = "N";
									updateRecordInSyncStatus();*/
				 					LOGGER.error("exception inside getFilteredCIsByTypeDemo insertIntoFull", e);
								}
							LOGGER.info("inserted in array");
							LOGGER.info("CIs.length : " + CIs.length);
							//LOGGER.info("listHolder.length after : " + listHolder.size());
						/*	updateRecordInSyncStatus();*/
						}
						//Number Of Chunks is zero
						else {
						LOGGER.info("fullOrChangedLoad : "+FULL_OR_CHANGED_LOAD);
						pullTopologyMapChunksFull();
						}
					}

				} catch (RemoteException rme) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("RemoteException inside getFilteredCIsByTypeDemo", rme);
				} catch (UcmdbFault ufe) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("UcmdbFaultException inside getFilteredCIsByTypeDemo", ufe);
				} catch (Exception e) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("exception inside getFilteredCIsByTypeDemo", e);
				}

      	}



      	 /* pullTopologyMapChunksFull Operation*/
      	public static void pullTopologyMapChunksFull() {
      		LOGGER.info("inside pullTopologyMapChunksFull method");

				try {
					
					LOGGER.info("numberOfChunks : " + numberOfChunks);
					LOGGER.info("key1 : " + key1);
					LOGGER.info("key2 : " + key2);
										
					dbConnection = getDBConnection();
					Statement delstmt = dbConnection.createStatement();
					delstmt.executeQuery("delete from CDM_CFS_1_FULLLOAD_STG");
					for(int i=1 ; i <= numberOfChunks ; i++)
					{
						chunkNumber = i;
						LOGGER.info("chunkNumber:" +chunkNumber);
						
						CmdbContext cntxt = CmdbContext.Factory.newInstance();
						cntxt.setCallerApplication(CALLER_APPLICATION);
						ChunkRequest chunkRequest = ChunkRequest.Factory.newInstance();
						ChunkInfo chunkInfo = ChunkInfo.Factory.newInstance();
						
						chunkRequest.setChunkNumber(chunkNumber);
						chunkInfo.setNumberOfChunks(numberOfChunks);

						ChunkKey chunksKey = ChunkKey.Factory.newInstance();
						chunksKey.setKey1(key1);
						chunksKey.setKey2(key2);
						
						chunkInfo.setChunksKey(chunksKey);
						chunkRequest.setChunkInfo(chunkInfo );
						
						
						TypedPropertiesCollection queryTypedProperties = TypedPropertiesCollection.Factory.newInstance();
						TypedProperties typedProperties = TypedProperties.Factory.newInstance();
						typedProperties.setType(CLASS_NAME);
						CustomTypedProperties properties = CustomTypedProperties.Factory.newInstance();
						PropertiesList propertiesList = PropertiesList.Factory.newInstance();
						//Considering Full Load
						String[] propertyNameArray = {"service_id","telstra_service_id","product_line","product_name","as_no","cust_id"};
						propertiesList.setPropertyNameArray(propertyNameArray );
						//Considering Full Load
						
						properties.setPropertiesList(propertiesList);
						typedProperties.setProperties(properties);
						
						TypedProperties[] typedPropertiesArray ={typedProperties};
						queryTypedProperties.setTypedPropertiesArray(typedPropertiesArray );
						
						PullTopologyMapChunks pullTopologyMapChunks = PullTopologyMapChunks.Factory.newInstance();
						
						pullTopologyMapChunks.setCmdbContext(cntxt);
						pullTopologyMapChunks.setChunkRequest(chunkRequest);
						pullTopologyMapChunks.setQueryTypedProperties(queryTypedProperties);
						PullTopologyMapChunksDocument request = PullTopologyMapChunksDocument.Factory.newInstance();
						request.setPullTopologyMapChunks(pullTopologyMapChunks);
						//LOGGER.info("pullTopologyMapChunks request" +request);					
						
						HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
						basicAuthentication.setUsername("admin");
						basicAuthentication.setPassword("admin");
						
						int so_timeout = 480000;
						int con_timeout = 60000;
						
						stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
						stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
						stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
						stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
						LOGGER.info("authentication passed");
		
						TimeUnit.SECONDS.sleep(3);
						
						PullTopologyMapChunksResponseDocument response = stub.pullTopologyMapChunks(request);

						CINode[] ciNode = response.getPullTopologyMapChunksResponse().getTopologyMap().getCINodes().getCINodeArray();

						
						CI[] ci = ciNode[0].getCIs().getCIArray();

						arrayLength = ci.length;
						LOGGER.info("arrayLength : " + arrayLength);						
						
						String[] fullcmdbId = new String[600];
						String[] fullserviceId = new String[600];
						String[] fullcustId = new String[600];
						String[] fulltelstraId = new String[600];
						String[] fullproductLine = new String[600];
						String[] fullproductName = new String[600];
						String[] fullasNo = new String[600];
						Timestamp[] fulllastModTime = new Timestamp[600];
						//String[] fullcfsId = new String[600];
						
						for(int j=0 ; j < arrayLength ; j++)
						{						
							fullcmdbId[j] = ci[j].getID().getStringValue();
							listHolder.add(strngId);
							type = ci[j].getType();

							//Considering Full Load
							if(FULL_OR_CHANGED_LOAD.equalsIgnoreCase("full"))
							{
								StrProp[] strprop = ci[j].getProps().getStrProps().getStrPropArray();
								DateProp[] dateprp = ci[j].getProps().getDateProps().getDatePropArray();
								
								for (int p = 0; p < strprop.length; p++)
								{									
									getName = strprop[p].getName();
									getValue = strprop[p].getValue();
									
									if(getName.equalsIgnoreCase("service_id")){
										fullserviceId[j] = getValue;
									}
									if(getName.equalsIgnoreCase("cust_id")){
										fullcustId[j] = getValue;
									}
									if(getName.equalsIgnoreCase("telstra_service_id")){
										fulltelstraId[j] = getValue;
									}
									if(getName.equalsIgnoreCase("product_line")){
										fullproductLine[j] = getValue;
									}
									if(getName.equalsIgnoreCase("product_name")){
										fullproductName[j] = getValue;
									}
									if(getName.equalsIgnoreCase("as_number")){
										fullasNo[j] = getValue;
									}
									/*if(getName.equalsIgnoreCase("cfs_id")){
										fullcfsId[j] = getValue;
									}*/
								}
								for (int q = 0; q < dateprp.length; q++){
									if(dateprp[q].getName().equalsIgnoreCase("last_modified_time")){
										fulllastModTime[j] = new Timestamp(dateprp[q].getValue().getTime().getTime());
									}
								}
									//mergeRecordIntoTable();																		
							}
							
						}

						try {
							dbConnection = getDBConnection();
							
							LOGGER.info("preparing batch" + i);
							String stringSQL = "insert into CDM_CFS_1_FULLLOAD_STG (cmdb_id, a_service_id, a_cust_id, a_telstra_service_id, a_product_line, a_product_name, a_as_number, a_cfs_id, timestamp) values (?,?,?,?,?,?,?,?,?)";
							PreparedStatement ps = dbConnection.prepareStatement(stringSQL);
							for (int k = 0; k < arrayLength; k++){
								ps.setString(1, fullcmdbId[k]);
								ps.setString(2, fullserviceId[k]);
								ps.setString(3, fullcustId[k]);
								ps.setString(4, fulltelstraId[k]);
								ps.setString(5, fullproductLine[k]);
								ps.setString(6, fullproductName[k]);
								ps.setString(7, fullasNo[k]);
								ps.setString(8, null);
								ps.setTimestamp(9, fulllastModTime[k]);									
								ps.addBatch();
							}				
							LOGGER.info("executing batch " + i);
							ps.executeBatch();
							ps.close();
							dbConnection.close();
						} catch (SQLException sqle) {
							LOGGER.error("SQLException inside insertFullTableQuery", sqle);
						} catch (Exception e) {
						/*	oprtnStatus = "N";
							updateRecordInSyncStatus();*/
		 					LOGGER.error("exception inside insertFullTableQuery", e);
						}
						LOGGER.info("INSERTING");
						if (i == numberOfChunks) {
							LOGGER.info("About to release chunks");
							ReleaseChunks relreq = ReleaseChunks.Factory.newInstance();							
							relreq.setChunksKey(chunksKey);
							relreq.setCmdbContext(cntxt);
							
							ReleaseChunksDocument reldoc = ReleaseChunksDocument.Factory.newInstance();
							reldoc.setReleaseChunks(relreq);
							
							stub.releaseChunks(reldoc);							
						}
					}
					
					/*updateRecordInSyncStatus();*/
					
				} catch (RemoteException rme) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("RemoteException inside pullTopologyMapChunksFull", rme);
				} catch (UcmdbFault ufe) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("UcmdbFaultException inside pullTopologyMapChunksFull", ufe);
				} catch (Exception e) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("exception inside pullTopologyMapChunksFull", e);
				}
      	}


			/* pullTopologyMapChunks Operation*/
      	public static void pullTopologyMapChunksDemo() {
      		LOGGER.info("inside pullTopologyMapChunksDemo method");

				try {
					
					LOGGER.info("numberOfChunks : " + numberOfChunks);
					LOGGER.info("key1 : " + key1);
					LOGGER.info("key2 : " + key2);
					
					CmdbContext cntxt = CmdbContext.Factory.newInstance();
					cntxt.setCallerApplication(CALLER_APPLICATION);
					ChunkRequest chunkRequest = ChunkRequest.Factory.newInstance();
					ChunkInfo chunkInfo = ChunkInfo.Factory.newInstance();
					
					chunkRequest.setChunkNumber(chunkNumber);
					chunkInfo.setNumberOfChunks(numberOfChunks);

					ChunkKey chunksKey = ChunkKey.Factory.newInstance();
					chunksKey.setKey1(key1);
					chunksKey.setKey2(key2);
					
					chunkInfo.setChunksKey(chunksKey);
					chunkRequest.setChunkInfo(chunkInfo );
					
					
					TypedPropertiesCollection queryTypedProperties = TypedPropertiesCollection.Factory.newInstance();
					TypedProperties typedProperties = TypedProperties.Factory.newInstance();
					typedProperties.setType(CLASS_NAME);
					
					
					CustomTypedProperties properties = CustomTypedProperties.Factory.newInstance();
					PropertiesList propertiesList = PropertiesList.Factory.newInstance();

					properties.setPropertiesList(propertiesList);
				 	typedProperties.setProperties(properties);
					
					TypedProperties[] typedPropertiesArray ={typedProperties};
					queryTypedProperties.setTypedPropertiesArray(typedPropertiesArray );
					
					PullTopologyMapChunks pullTopologyMapChunks = PullTopologyMapChunks.Factory.newInstance();
					
					pullTopologyMapChunks.setCmdbContext(cntxt);
					pullTopologyMapChunks.setChunkRequest(chunkRequest);
					pullTopologyMapChunks.setQueryTypedProperties(queryTypedProperties);
					
					
					PullTopologyMapChunksDocument request = PullTopologyMapChunksDocument.Factory.newInstance();
					request.setPullTopologyMapChunks(pullTopologyMapChunks);
					//LOGGER.info("pullTopologyMapChunks request" +request);					
					
					HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
					basicAuthentication.setUsername("admin");
					basicAuthentication.setPassword("admin");
					
					int so_timeout = 240000;
					int con_timeout = 10000;
					
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
					LOGGER.info("authentication passed");
	
					PullTopologyMapChunksResponseDocument response = stub.pullTopologyMapChunks(request);
					
					CINode[] ciNode = response.getPullTopologyMapChunksResponse().getTopologyMap().getCINodes().getCINodeArray();
					
					LOGGER.info("ciNode : " + ciNode);
					
					label = ciNode[0].getLabel();
					LOGGER.info("label : " + label);
					
					CI[] ci = ciNode[0].getCIs().getCIArray();
					LOGGER.info("ci : " + ci);
					//LOGGER.info(ci.length);
					arrayLength = ci.length;
					LOGGER.info("arrayLength : " + arrayLength);

					
					for(int j=0 ; j < arrayLength ; j++)
					{						
						strngId = ci[j].getID().getStringValue();	
						listHolder.add(strngId);
						type = ci[j].getType();
				
					}
					getChangedCIsDemo();

					
				} catch (RemoteException rme) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("RemoteException inside pullTopologyMapChunksDemo", rme);
				} catch (UcmdbFault ufe) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("UcmdbFaultException inside pullTopologyMapChunksDemo", ufe);
				} catch (Exception e) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("exception inside pullTopologyMapChunksDemo", e);
				}

      	}

      	 /* getChangedCIs Operation*/
      	public static void getChangedCIsDemo() {
      		LOGGER.info("inside getChangedCIsDemo method");

				try {		
					CmdbContext cntxt = CmdbContext.Factory.newInstance();
					cntxt.setCallerApplication(CALLER_APPLICATION);
					IDs ids = IDs.Factory.newInstance();
					
				
					for (int k = 0; k < listHolder.size(); k++) 
					{					
						ID id2 = ids.addNewID();
						id2.setStringValue(listHolder.get(k));
						
					}
					//555
					getFromToDates();
					
					Calendar frmCalendar = Calendar.getInstance();
					frmCalendar.set(frmYear, frmMonth, frmDate, frmHourOfDay, frmMinute, frmSecond);
					Calendar fromDate = frmCalendar;
					
					Calendar toCalendar = Calendar.getInstance();
					toCalendar.set(toYear, toMonth, toDte, toHourOfDay, toMinute, toSecond);
					Calendar toDate = toCalendar;					
					
					GetChangedCIs getChangedCIs = GetChangedCIs.Factory.newInstance();
					getChangedCIs.setCmdbContext(cntxt);
					getChangedCIs.setIds(ids);
					getChangedCIs.setFromDate(fromDate);
					getChangedCIs.setToDate(toDate );
					
					GetChangedCIsDocument request = GetChangedCIsDocument.Factory.newInstance();
					request.setGetChangedCIs(getChangedCIs);
			
					LOGGER.info("set inputs");
					
					HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
					basicAuthentication.setUsername("admin");
					basicAuthentication.setPassword("admin");
					
					int so_timeout = 240000;
					int con_timeout = 10000;
					
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
					LOGGER.info("authentication passed");
	
					GetChangedCIsResponseDocument response = stub.getChangedCIs(request);
					
					ChangedDataInfo[] changedDataInfo = response.getGetChangedCIsResponse().getChangeDataInfoArray();
					LOGGER.info("changedDataInfo.length : " + changedDataInfo.length);
					for (int l = 0; l < changedDataInfo.length; l++) 
					{
						com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType.Enum changeType = changedDataInfo[l].getChangeType();
						ID rootIds = changedDataInfo[l].getRootID();	
						String rootId = rootIds.getStringValue();
						ID[] changedIds = changedDataInfo[l].getChangedIDs().getIDArray();
						LOGGER.info("changedIds.length : " +changedIds.length);

							if(changeType.toString().equalsIgnoreCase("ROOT_CHANGED")){
								strngCIsId = rootId;
								getCIsArray.add(strngCIsId);
							}
					}
					
					LOGGER.info("777 : " +getCIsArray.size());
					getCIsByIdDemo();
					
				} catch (RemoteException rme) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("RemoteException inside getChangedCIsDemo", rme);
				} catch (UcmdbFault ufe) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("UcmdbFaultException inside getChangedCIsDemo", ufe);
				} catch (Exception e) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("exception inside getChangedCIsDemo", e);
				}				

      	}
      	
      	 private static void getFromToDates() {
      		 
      			LOGGER.info("inside getFromToDates method");
      			LOGGER.info("syncId : " +syncId);
      			syncStatus = "COMPLETED";
      			String insertSQL = "SELECT TO_CHAR(SYNC_START,'yyyy'),TO_CHAR(SYNC_START,'mm'),TO_CHAR(SYNC_START,'dd'),EXTRACT(HOUR FROM SYNC_START), ";
      			insertSQL = insertSQL + "EXTRACT(MINUTE FROM SYNC_START),EXTRACT(SECOND FROM SYNC_START) FROM SYNC_STATUS ";
      			insertSQL = insertSQL + "where  UPPER(SYNC_STATUS) = '"+syncStatus+"'  ";
      			insertSQL = insertSQL + "AND SYNC_START = ";
      			insertSQL = insertSQL + " (SELECT MAX(SYNC_START) FROM SYNC_STATUS where UPPER(SYNC_STATUS)='"+syncStatus+"')";
      			
      			try {
      				
      				dbConnection = getDBConnection();
						statement = dbConnection.createStatement();

						LOGGER.info(insertSQL);
						ResultSet rs = statement.executeQuery(insertSQL);
						
						
					if(rs.next())
					{
						//the rows exists, get values
						LOGGER.info("row exists");
						LOGGER.info("int1 : " +rs.getInt(1));
						LOGGER.info("string : " +rs.getString(1));

						frmYear = rs.getInt(1);
						frmMonth = rs.getInt(2);
						frmDate = rs.getInt(3);
						frmHourOfDay = rs.getInt(4);
						frmMinute = rs.getInt(5);
						frmSecond = rs.getInt(6);
						
						String sysDateSQL = "SELECT TO_CHAR(SYSDATE,'yyyy'),TO_CHAR(SYSDATE,'mm'),TO_CHAR(SYSDATE,'dd'),EXTRACT(HOUR FROM SYSTIMESTAMP), ";
						sysDateSQL = sysDateSQL + "EXTRACT(MINUTE FROM SYSTIMESTAMP),EXTRACT(SECOND FROM SYSTIMESTAMP) FROM DUAL ";
						ResultSet rSet = null;
						try {
							
							dbConnection = getDBConnection();
							statement = dbConnection.createStatement();

							LOGGER.info(sysDateSQL);
							rSet = statement.executeQuery(sysDateSQL);
							if(rSet.next()){
							toYear = rSet.getInt(1);
							toMonth = rSet.getInt(2);
							toDte = rSet.getInt(3);
							toHourOfDay = rSet.getInt(4);
							toMinute = rSet.getInt(5);
							toSecond = rSet.getInt(6);
							LOGGER.info("within sysdate values");
							LOGGER.info("toMonthDate : " + toMonth + "h" +toDte + "h" +toYear);
							LOGGER.info("toTime : " + toHourOfDay + "h" +toMinute + "h" +toSecond);
							}
														
						} catch (SQLException sqle) {
							LOGGER.error("SQLException inside getToDates", sqle);
						} catch (Exception e) {
							/*oprtnStatus = "N";
							updateRecordInSyncStatus();*/
							LOGGER.error("exception inside getToDates", e);
						}
												
						LOGGER.info("frmMonthDate : " + frmMonth +"h" + frmDate +"h" +frmYear);
						LOGGER.info("fromTime : " + frmHourOfDay +"h" + frmMinute +"h" +frmSecond);
						LOGGER.info("toMonthDate : " + toMonth + "h" +toDte + "h" +toYear);
						LOGGER.info("toTime : " + toHourOfDay + "h" +toMinute + "h" +toSecond);
					}
					else
					{
						//assign default values
						LOGGER.info("within default values");
						LOGGER.info("frmMonthDate : " + frmMonth + frmDate);
						LOGGER.info("toMonthDate : " + toMonth + toDte);
					}
					rs.close();
					
      				
						
					} catch (SQLException sqle) {
						LOGGER.error("SQLException inside getFromDates", sqle);
					}  catch (Exception e) {
						/*oprtnStatus = "N";
						updateRecordInSyncStatus();*/
						LOGGER.error("exception inside getFromDates", e);
					}
				
			}


			/* getCIsById Operation*/
      	public static void getCIsByIdDemo() {
      		LOGGER.info("inside getCIsByIdDemo method");

				try {
					
					CmdbContext cntxt = CmdbContext.Factory.newInstance();
					cntxt.setCallerApplication(CALLER_APPLICATION);
					
					IDs iDs = IDs.Factory.newInstance();
					LOGGER.info("getCIsArray.size() : " +getCIsArray.size());					
					for (int n = 0; n < getCIsArray.size(); n++) 
					{					
						ID id2 = iDs.addNewID();
						id2.setStringValue(getCIsArray.get(n));
						LOGGER.info("getCIsArray : " +getCIsArray.get(n));
						
					}
					
					TypedPropertiesCollection cIsTypedProperties = TypedPropertiesCollection.Factory.newInstance();
					TypedProperties typedProperties = TypedProperties.Factory.newInstance();
					CustomTypedProperties properties =  CustomTypedProperties.Factory.newInstance();
					PropertiesList propertiesList = PropertiesList.Factory.newInstance();
					propertiesList.addPropertyName(PROPERTY_NAME1);
					propertiesList.addPropertyName(PROPERTY_NAME2);
					propertiesList.addPropertyName(PROPERTY_NAME3);
					propertiesList.addPropertyName(PROPERTY_NAME4);
					propertiesList.addPropertyName(PROPERTY_NAME5);
					propertiesList.addPropertyName(PROPERTY_NAME6);
					properties.setPropertiesList(propertiesList );
					typedProperties.setProperties(properties );
					typedProperties.setType(CLASS_NAME);
					TypedProperties[] typedPropertiesArray = {typedProperties};
					cIsTypedProperties.setTypedPropertiesArray(typedPropertiesArray );
				
					GetCIsById getCIsById = GetCIsById.Factory.newInstance();
					getCIsById.setCmdbContext(cntxt);
					getCIsById.setIDs(iDs);
					getCIsById.setCIsTypedProperties(cIsTypedProperties);
					
					
					GetCIsByIdDocument request = GetCIsByIdDocument.Factory.newInstance();
					request.setGetCIsById(getCIsById);
					LOGGER.info("getCIsById request : " +request);
					
					HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
					basicAuthentication.setUsername("admin");
					basicAuthentication.setPassword("admin");
					
					int so_timeout = 240000;
					int con_timeout = 10000;
					
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
					//stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(240000);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
					stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
					LOGGER.info("authentication passed");

					GetCIsByIdResponseDocument response = stub.getCIsById(request);
					//LOGGER.info("getCIsById response : " +response);
					
					CI[] ci = response.getGetCIsByIdResponse().getCIs().getCIArray();
					LOGGER.info("ci.length : " + ci.length);
					for (int p = 0; p < ci.length; p++) 
					{
					getCIsId = ci[p].getID().getStringValue();
					getCIsType = ci[p].getType();
					
					//LOGGER.info("getCIsId : " + getCIsId);
					//LOGGER.info("getCIstype : " + getCIsType);
					
					StrProp[] strprop = ci[p].getProps().getStrProps().getStrPropArray();
					LOGGER.info("strprop.length : " + strprop.length);
					deltaloadinserttimestamp = getCurrentTimeStamp();
						for (int r = 0; r < strprop.length; r++)
						{
						getCIsName = strprop[r].getName();
						getCIsValue = strprop[r].getValue();
						//LOGGER.info("getCIsId : " + getCIsId);
						//LOGGER.info("getCIsName : " + getCIsName);
						//LOGGER.info("getCIsValue : " + getCIsValue);
						
							//LOGGER.info("getCIsName before assigning: " + getCIsName);
							//LOGGER.info("getCIsValue before assigning: " + getCIsValue);
							//Assigning values into respective variables
							cmdbId =getCIsId;
							//LOGGER.info("serviceId777:" +serviceId);
							if(getCIsName.equalsIgnoreCase("service_id")){
								serviceId = getCIsValue;
								//LOGGER.info("555" + serviceId);
							}
							if(getCIsName.equalsIgnoreCase("cust_id")){
								custId = getCIsValue;
							}
							if(getCIsName.equalsIgnoreCase("telstra_service_id")){
								telstraId = getCIsValue;
							}
							if(getCIsName.equalsIgnoreCase("product_line")){
								productline = getCIsValue;
							}
							if(getCIsName.equalsIgnoreCase("product_name")){
								productName = getCIsValue;
							}
							//LOGGER.info("getCIsName select5 : " +getCIsName);
							//LOGGER.info("getCIsValue select5 : " +getCIsValue);

							mergeRecordIntoTable();						
				
						}
					}
					getCIsChunkNo = response.getGetCIsByIdResponse().getChunkInfo().getNumberOfChunks();
					getCIsKey1 = response.getGetCIsByIdResponse().getChunkInfo().getChunksKey().getKey1();
					getCIsKey2 = response.getGetCIsByIdResponse().getChunkInfo().getChunksKey().getKey2();
					
					//updateRecordInSyncStatus();
					LOGGER.info("getCIsChunkNo : " + getCIsChunkNo);
					LOGGER.info("getCIsKey1 : " + getCIsKey1);
					LOGGER.info("getCIsKey2 : " + getCIsKey2);
					
				} catch (RemoteException rme) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("RemoteException inside getCIsByIdDemo", rme);
				} catch (UcmdbFault ufe) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("UcmdbFaultException inside getCIsByIdDemo", ufe);
				} catch (Exception e) {
					/*oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("exception inside getCIsByIdDemo", e);
				}

      	}
      	
      
      	 private static void mergeRecordIntoTable() {
      		//LOGGER.info("inside mergeRecordIntoFull method");
				//LOGGER.info("serviceId : " +serviceId);
				tableName = "CDM_CFS_1_STG";
				String timestamp = deltaloadinserttimestamp;
				if(FULL_OR_CHANGED_LOAD.equalsIgnoreCase("full"))
				{
					tableName = "CDM_CFS_1_FULLLOAD_STG";
					timestamp = fullloadinserttimestamp;
				}
				LOGGER.info("tableName : " +tableName);
				//Correct Merge Query
				//String insertSQL = "MERGE INTO CDM_CFS_1_FULLLOAD_STG t USING DUAL";
				LOGGER.info("MERGING");
				String insertSQL = "MERGE INTO "+tableName+" t USING DUAL";
				insertSQL = insertSQL + " ON (t.CMDB_ID = '"+cmdbId+"')";
				insertSQL = insertSQL + "WHEN MATCHED THEN ";
				insertSQL = insertSQL + "UPDATE SET ";
				if(serviceId!=""){
				insertSQL = insertSQL + "A_SERVICE_ID ='"+serviceId+"',";
					}
				if(custId!=""){
				insertSQL = insertSQL + "A_CUST_ID ='"+custId+"',";
					}
				if(telstraId!=""){
				insertSQL = insertSQL + "A_TELSTRA_SERVICE_ID ='"+telstraId+"' ,";
					}
				if(productline!=""){
				insertSQL = insertSQL + "A_PRODUCT_LINE = '"+productline+"',";
					}
				if(asNo!=""){
				insertSQL = insertSQL + "A_AS_NUMBER ='"+asNo+"',";
					}
				if(productName!=""){
				insertSQL = insertSQL + "A_PRODUCT_NAME ='"+productName+"',";
					}
				if(cfsId!=""){
				insertSQL = insertSQL + "A_CFS_ID = '"+cfsId+"',";
					}
				insertSQL = insertSQL + "TIMESTAMP = to_date('"+ timestamp + "', 'yyyy/mm/dd hh24:mi:ss') ";
				insertSQL = insertSQL + "WHEN NOT MATCHED THEN ";
				insertSQL = insertSQL + "INSERT ";
				insertSQL = insertSQL + "(A_SERVICE_ID,A_CUST_ID,A_TELSTRA_SERVICE_ID,A_PRODUCT_LINE,A_AS_NUMBER,A_PRODUCT_NAME,A_CFS_ID,CMDB_ID,TIMESTAMP) ";
				insertSQL = insertSQL + "VALUES ";
				insertSQL = insertSQL + "('"+serviceId+"','"+custId+"','"+telstraId+"','"+productline+"','"+asNo+"','"+productName+"',";
				insertSQL = insertSQL + " '"+cfsId+"','"+cmdbId+"'," + "to_date('"+ timestamp + "', 'yyyy/mm/dd hh24:mi:ss')) ";
				
				
				try {
					
					dbConnection = getDBConnection();
					statement = dbConnection.createStatement();

					statement.executeUpdate(insertSQL);
					serviceId = "";
					custId = "";
					telstraId = "";
					productline = "";
					asNo = "";
					productName = "";
					cfsId = "";					
					
				} catch (SQLException sqle) {
					LOGGER.error("SQLException inside mergeRecordIntoTable", sqle);
				} catch (Exception e) {
				/*	oprtnStatus = "N";
					updateRecordInSyncStatus();*/
					LOGGER.error("exception inside mergeRecordIntoTable", e);
				}
				
			}
    	
		/*	private static void updateRecordInSyncStatus() {
      		LOGGER.info("inside updateRecordInSyncStatus method");
      		syncStatus = "COMPLETED_LG";
      		//not required to call as trigger created on sync_status table
      		//CallableStatement cstmt = null;
      		if(oprtnStatus.equalsIgnoreCase("N"))
      		{
      			syncStatus = "ERROR_LG";
      		}
      		LOGGER.info("syncStatus : " + syncStatus);
      		LOGGER.info("syncIdAfter : " + syncId);
      		LOGGER.info("UPDATING");
      		String insertSQL = "INSERT INTO SYNC_STATUS " ;
      		insertSQL = insertSQL + "(SYNC_ID,SYNC_START,SYNC_END,SYNC_STATUS,SYNC_TYPE) VALUES ";
      		insertSQL = insertSQL +  "(SYNC_ID.nextval," + "to_date('"+ sync_start_date + "', 'yyyy/mm/dd hh24:mi:ss'), " + "to_date('"+ getCurrentTimeStamp() + "', 'yyyy/mm/dd hh24:mi:ss'),'"+syncStatus+"','"+syncType+"'  )" ;

				try {
					
					dbConnection = getDBConnection();
					statement = dbConnection.createStatement();

					LOGGER.info(insertSQL);
					statement.executeUpdate(insertSQL);
					LOGGER.info("Record inserted in sync status!");
					//Additional
					//Getting this syncId
					String selectSQL = "SELECT MAX(SYNC_ID) FROM SYNC_STATUS " ;
					selectSQL = selectSQL + "WHERE ";
					selectSQL = selectSQL + "UPPER(SYNC_STATUS)='COMPLETED_LG' ";
					try {
						dbConnection = getDBConnection();
						statement = dbConnection.createStatement();

						LOGGER.info(selectSQL);
						ResultSet rSet = statement.executeQuery(selectSQL);
						if(rSet.next())
						{
							LOGGER.info("within select loop");
							syncId = rSet.getInt(1);
						}
						LOGGER.info("syncId : " + syncId);
					} catch (Exception e) {
						oprtnStatus = "N";
						updateRecordInSyncStatus();
						e.printStackTrace();
					}
					//Additional
					
				} catch (SQLException sqle) {
					//LOGGER.error("SQLException inside updateRecordInSyncStatus", sqle);
					sqle.printStackTrace();
				} catch (Exception e) {
					oprtnStatus = "N";
					updateRecordInSyncStatus();
					//LOGGER.error("exception inside updateRecordInSyncStatus", e);
					e.printStackTrace();
				}
			}
*/
			
			private static String getCurrentTimeStamp() {
				java.util.Date today = new java.util.Date();
				return dateFormat.format(today.getTime());
			}
			
			public static void loadParams(String configPath) {
				
			    InputStream is = null;
			 
			    try {
			    	LOGGER.info("configPath : " +configPath);
			    	File f = new File (configPath);
			        //File f = new File("D:\\WorkSpace5\\testUcmdb5\\src\\com\\hp\\ucmdb\\generated\\config.txt");
			    	//File f = new File("D:\\WorkSpace5\\testUcmdb5\\src\\config.txt");
			    	//File f = new File("E:\\Ahana\\Workspace5\\Ucmdb5\\src\\config.txt");
			        is = new FileInputStream( f );
			    }
			    catch ( Exception e ) { is = null; }
			 
			    try {
			        props.load( is );
			    }
			    catch ( Exception e ) {
			    	LOGGER.error("exception inside loadParams", e);
			    }

			   
			}

			
			private static Connection getDBConnection() {
				
				//Using config file to remove hardcoding
				String DB_DRIVER = props.getProperty("DB_DRIVER");
				String DB_CONNECTION = props.getProperty("DB_CONNECTION");
				String DB_USER = props.getProperty("DB_USER");
				String DB_PASSWORD = props.getProperty("DB_PASSWORD");
				
				try {						
					Class.forName(DB_DRIVER);					
				} catch (ClassNotFoundException e) {
					LOGGER.info(e.getMessage());
				}
								
				try {
					dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
					return dbConnection;				
				} catch (SQLException sqle) {
					LOGGER.info(sqle.getMessage());
				}
				return dbConnection;
			}

	
}
