package com.telstra.netops.lookingglass.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A node test request.
 * 
 * TODO - consider merging with NodeTestRequest
 * 
 * @author d772392
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientNodeTestRequest {

	private static final String SUBMIT = "submit";
	
	@JsonProperty
	public String customerId;
	
	@JsonProperty("level")
	public ClientNodeTestRequestType type;
	
	@JsonProperty
	public String product;
	
	@JsonProperty
	public Long sourceNode;
	
	@JsonProperty
	public String targetNode;
	
	@JsonProperty
	public String submit;
	
	@JsonProperty
	public Integer numPings;
	

	public static ClientNodeTestRequest createNodeTestRequest(Long snode, Long dnode, Integer numPings) {
		return ClientNodeTestRequest.createRequest(ClientNodeTestRequestType.NODE_TEST, snode, String.valueOf(dnode), numPings);
	}

	public static ClientNodeTestRequest createNodeHostTestRequest(Long snode, String dnode) {
		return ClientNodeTestRequest.createRequest(ClientNodeTestRequestType.NODE_HOST_TEST, snode, dnode);
	}
	
	public static ClientNodeTestRequest createNodeTracerouteTestRequest(Long snode, String dnode) {
		return ClientNodeTestRequest.createRequest(ClientNodeTestRequestType.NODE_TRACEROUTE_TEST, snode, dnode);
	}
	
	private static ClientNodeTestRequest createRequest(ClientNodeTestRequestType type, Long snode, String dnode) {
		return createRequest(type, snode, dnode, null);	
	}
	
	private static ClientNodeTestRequest createRequest(ClientNodeTestRequestType type, Long snode, String dnode, Integer numPings) {
		ClientNodeTestRequest request = new ClientNodeTestRequest();
		request.setType(type);
		request.setSourceNode(snode);
		request.setTargetNode(dnode);
		request.setSubmit(SUBMIT);
		request.setNumPings(numPings);
		return request;		
	}

	public String getSubmit() {
		return submit;
	}
	
	public void setSubmit(String submit) {
		this.submit = submit;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public ClientNodeTestRequestType getType() {
		return type;
	}
	
	public void setType(ClientNodeTestRequestType type) {
		this.type = type;
	}
	
	public String getProduct() {
		return product;
	}
	
	public void setProduct(String product) {
		this.product = product;
	}
	
	public Long getSourceNode() {
		return sourceNode;
	}
	
	public void setSourceNode(Long sourceNode) {
		this.sourceNode = sourceNode;
	}
	
	public String getTargetNode() {
		return targetNode;
	}
	
	public void setTargetNode(String targetNode) {
		this.targetNode = targetNode;
	}

	public void setUserProfile(String userProfile) {
		// not needed
	}

	public Integer getNumPings() {
		return numPings;
	}

	public void setNumPings(Integer numPings) {
		this.numPings = numPings;
	}

}
