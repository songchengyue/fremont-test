/*
 * XML Type:  ClassModelQueryGetClassModelIconPathsRequest
 * Namespace: http://schemas.hp.com/ucmdb/ui/1/params
 * Java type: com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.ui._1.params.impl;
/**
 * An XML ClassModelQueryGetClassModelIconPathsRequest(@http://schemas.hp.com/ucmdb/ui/1/params).
 *
 * This is a complex type.
 */
public class ClassModelQueryGetClassModelIconPathsRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest
{
    
    public ClassModelQueryGetClassModelIconPathsRequestImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/ui/1/params", "cmdbContext");
    private static final javax.xml.namespace.QName CLASSNAME$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/ui/1/params", "className");
    private static final javax.xml.namespace.QName LARGE$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/ui/1/params", "large");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets the "className" element
     */
    public java.lang.String getClassName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLASSNAME$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "className" element
     */
    public org.apache.xmlbeans.XmlString xgetClassName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CLASSNAME$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "className" element
     */
    public boolean isSetClassName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CLASSNAME$2) != 0;
        }
    }
    
    /**
     * Sets the "className" element
     */
    public void setClassName(java.lang.String className)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLASSNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLASSNAME$2);
            }
            target.setStringValue(className);
        }
    }
    
    /**
     * Sets (as xml) the "className" element
     */
    public void xsetClassName(org.apache.xmlbeans.XmlString className)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CLASSNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CLASSNAME$2);
            }
            target.set(className);
        }
    }
    
    /**
     * Unsets the "className" element
     */
    public void unsetClassName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CLASSNAME$2, 0);
        }
    }
    
    /**
     * Gets the "large" element
     */
    public boolean getLarge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LARGE$4, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "large" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetLarge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(LARGE$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "large" element
     */
    public void setLarge(boolean large)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LARGE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LARGE$4);
            }
            target.setBooleanValue(large);
        }
    }
    
    /**
     * Sets (as xml) the "large" element
     */
    public void xsetLarge(org.apache.xmlbeans.XmlBoolean large)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(LARGE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(LARGE$4);
            }
            target.set(large);
        }
    }
}
