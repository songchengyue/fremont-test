/*
 * An XML document type.
 * Localname: getCIsByIdResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getCIsByIdResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetCIsByIdResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponseDocument
{
    
    public GetCIsByIdResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCISBYIDRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getCIsByIdResponse");
    
    
    /**
     * Gets the "getCIsByIdResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponse getGetCIsByIdResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponse)get_store().find_element_user(GETCISBYIDRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCIsByIdResponse" element
     */
    public void setGetCIsByIdResponse(com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponse getCIsByIdResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponse)get_store().find_element_user(GETCISBYIDRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponse)get_store().add_element_user(GETCISBYIDRESPONSE$0);
            }
            target.set(getCIsByIdResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getCIsByIdResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponse addNewGetCIsByIdResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponse)get_store().add_element_user(GETCISBYIDRESPONSE$0);
            return target;
        }
    }
}
