/*
 * XML Type:  impactResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.ImpactResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * An XML impactResponse(@http://schemas.hp.com/ucmdb/1/params/impact).
 *
 * This is a complex type.
 */
public class ImpactResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.ImpactResponse
{
    
    public ImpactResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTTOPOLOGY$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "impactTopology");
    private static final javax.xml.namespace.QName IDENTIFIER$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "identifier");
    
    
    /**
     * Gets the "impactTopology" element
     */
    public com.hp.schemas.ucmdb._1.types.Topology getImpactTopology()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Topology target = null;
            target = (com.hp.schemas.ucmdb._1.types.Topology)get_store().find_element_user(IMPACTTOPOLOGY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "impactTopology" element
     */
    public void setImpactTopology(com.hp.schemas.ucmdb._1.types.Topology impactTopology)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Topology target = null;
            target = (com.hp.schemas.ucmdb._1.types.Topology)get_store().find_element_user(IMPACTTOPOLOGY$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.Topology)get_store().add_element_user(IMPACTTOPOLOGY$0);
            }
            target.set(impactTopology);
        }
    }
    
    /**
     * Appends and returns a new empty "impactTopology" element
     */
    public com.hp.schemas.ucmdb._1.types.Topology addNewImpactTopology()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Topology target = null;
            target = (com.hp.schemas.ucmdb._1.types.Topology)get_store().add_element_user(IMPACTTOPOLOGY$0);
            return target;
        }
    }
    
    /**
     * Gets the "identifier" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.Identifier getIdentifier()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.Identifier target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.Identifier)get_store().find_element_user(IDENTIFIER$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "identifier" element
     */
    public void setIdentifier(com.hp.schemas.ucmdb._1.types.impact.Identifier identifier)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.Identifier target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.Identifier)get_store().find_element_user(IDENTIFIER$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.impact.Identifier)get_store().add_element_user(IDENTIFIER$2);
            }
            target.set(identifier);
        }
    }
    
    /**
     * Appends and returns a new empty "identifier" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.Identifier addNewIdentifier()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.Identifier target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.Identifier)get_store().add_element_user(IDENTIFIER$2);
            return target;
        }
    }
}
