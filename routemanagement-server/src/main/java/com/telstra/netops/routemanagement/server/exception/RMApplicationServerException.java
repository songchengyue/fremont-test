package com.telstra.netops.routemanagement.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception class for Generic Errors
 *
 */
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "There was an internal application error")
public class RMApplicationServerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RMApplicationServerException(String message, Throwable cause) {
		super(message, cause);
	}

	public RMApplicationServerException(String message) {
		super(message);
	}

}
