/*
 * XML Type:  ImpactStep
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactStep
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML ImpactStep(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ImpactStepImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactStep
{
    
    public ImpactStepImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TRIGGERCI$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "triggerCI");
    private static final javax.xml.namespace.QName RULENAME$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "ruleName");
    private static final javax.xml.namespace.QName IMPACTEDCI$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "impactedCI");
    
    
    /**
     * Gets the "triggerCI" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs getTriggerCI()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs)get_store().find_element_user(TRIGGERCI$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "triggerCI" element
     */
    public void setTriggerCI(com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs triggerCI)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs)get_store().find_element_user(TRIGGERCI$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs)get_store().add_element_user(TRIGGERCI$0);
            }
            target.set(triggerCI);
        }
    }
    
    /**
     * Appends and returns a new empty "triggerCI" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs addNewTriggerCI()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs)get_store().add_element_user(TRIGGERCI$0);
            return target;
        }
    }
    
    /**
     * Gets the "ruleName" element
     */
    public java.lang.String getRuleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RULENAME$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ruleName" element
     */
    public org.apache.xmlbeans.XmlString xgetRuleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RULENAME$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ruleName" element
     */
    public void setRuleName(java.lang.String ruleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RULENAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RULENAME$2);
            }
            target.setStringValue(ruleName);
        }
    }
    
    /**
     * Sets (as xml) the "ruleName" element
     */
    public void xsetRuleName(org.apache.xmlbeans.XmlString ruleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RULENAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(RULENAME$2);
            }
            target.set(ruleName);
        }
    }
    
    /**
     * Gets the "impactedCI" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs getImpactedCI()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs)get_store().find_element_user(IMPACTEDCI$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "impactedCI" element
     */
    public void setImpactedCI(com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs impactedCI)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs)get_store().find_element_user(IMPACTEDCI$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs)get_store().add_element_user(IMPACTEDCI$4);
            }
            target.set(impactedCI);
        }
    }
    
    /**
     * Appends and returns a new empty "impactedCI" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs addNewImpactedCI()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactedCIs)get_store().add_element_user(IMPACTEDCI$4);
            return target;
        }
    }
}
