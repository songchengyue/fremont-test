/*
 * An XML document type.
 * Localname: getDomainsNamesRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getDomainsNamesRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetDomainsNamesRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequestDocument
{
    
    public GetDomainsNamesRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETDOMAINSNAMESREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDomainsNamesRequest");
    
    
    /**
     * Gets the "getDomainsNamesRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequest getGetDomainsNamesRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequest)get_store().find_element_user(GETDOMAINSNAMESREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getDomainsNamesRequest" element
     */
    public void setGetDomainsNamesRequest(com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequest getDomainsNamesRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequest)get_store().find_element_user(GETDOMAINSNAMESREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequest)get_store().add_element_user(GETDOMAINSNAMESREQUEST$0);
            }
            target.set(getDomainsNamesRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "getDomainsNamesRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequest addNewGetDomainsNamesRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesRequest)get_store().add_element_user(GETDOMAINSNAMESREQUEST$0);
            return target;
        }
    }
}
