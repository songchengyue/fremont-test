DROP TABLE IF EXISTS service;
 
 CREATE TABLE service
(
  SERVICE_ID          NVARCHAR(50)             DEFAULT '0'    NOT NULL PRIMARY KEY,
  TELSTRA_SERVICE_ID  NVARCHAR(50)             DEFAULT NULL,
  CUSTOMER_ID         NVARCHAR(50)             DEFAULT NULL,
  PRODUCT_LINE        NVARCHAR(50)             DEFAULT NULL,
  ROUTER              NVARCHAR(50)             DEFAULT NULL,
  INTERFACE           NVARCHAR(50)             DEFAULT NULL,
  PARENT_SITE_ID      NVARCHAR(50)             DEFAULT NULL,
  AS_NO               NVARCHAR(50)             DEFAULT NULL,
  BGP_NEIGHBOR_IP	  NVARCHAR(50)             DEFAULT NULL,
  IRRD_OBJECT	  	  NVARCHAR(50)             DEFAULT NULL,
  BHA_BGP_NEIGHBOR_IP	  NVARCHAR(50)         DEFAULT NULL,
  BLACK_HOLE_ROUTING	  NVARCHAR(50)             DEFAULT NULL,
  TIME_ENTERED        NVARCHAR(50)             DEFAULT NULL,
  BGP_NEIGHBOR_IP_V6	  NVARCHAR(50)             DEFAULT NULL,
  BHA_BGP_NEIGHBOR_IP_V6	  NVARCHAR(50)         DEFAULT NULL
);


DROP TABLE IF EXISTS looking_glass_node;

CREATE TABLE looking_glass_node (
  Node_ID INT (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  Router_Name NVARCHAR (50) DEFAULT NULL,
  Router_Software_ID INT (11) DEFAULT NULL,
  IP_Address NVARCHAR (50) DEFAULT NULL,
  IP_Version INT (1) DEFAULT NULL,
  Pop_Country_ID NVARCHAR (50) DEFAULT NULL,
  Pop_City_ID NVARCHAR (50) DEFAULT NULL
  
) ;

--Pop city Table:

DROP TABLE IF EXISTS pop_city;

CREATE TABLE pop_city (
  Pop_City_ID NVARCHAR (50) DEFAULT '0'    NOT NULL PRIMARY KEY,
  Pop_City_Code NVARCHAR (50) DEFAULT NULL,
  Pop_City NVARCHAR (50) DEFAULT NULL,
  Time_Entered NVARCHAR (50) DEFAULT NULL
) ;

--Pop country Table:

DROP TABLE IF EXISTS pop_country;

CREATE TABLE pop_country (
  Pop_Country_ID NVARCHAR (50) DEFAULT '0'    NOT NULL PRIMARY KEY,
  Pop_Country_Code NVARCHAR (50) DEFAULT NULL,
  Pop_Country NVARCHAR (50) DEFAULT NULL,
  Time_Entered NVARCHAR (50) DEFAULT NULL
  
);

--CustomerTable:

DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
  Customer_ID NVARCHAR (50) NOT NULL PRIMARY KEY,
  Customer_Name NVARCHAR (100) DEFAULT NULL,
  Time_Entered NVARCHAR (50) DEFAULT NULL  
) ;

--Netops product lineTable:

DROP TABLE IF EXISTS netops_productline;

CREATE TABLE netops_productline (
  Product_Line NVARCHAR (50) NOT NULL PRIMARY KEY,
  Product_Name NVARCHAR (100) DEFAULT NULL,
  Service_Product_Line NVARCHAR (100) DEFAULT NULL 
);

--Tool AccessTable:

DROP TABLE IF EXISTS tool_access;

CREATE TABLE tool_access (
  Tool_Access_Id NVARCHAR(50)  DEFAULT '0' NOT NULL PRIMARY KEY,
  Product_Line NVARCHAR (50) NOT NULL,
  User_Role NVARCHAR (50) DEFAULT NULL,
  Tool NVARCHAR (50) DEFAULT NULL,
  menu_access NVARCHAR (10) DEFAULT NULL
) ;

--Router Table:

DROP TABLE IF EXISTS router_software;

CREATE TABLE router_software (
  Router_Software_ID INT (11) NOT NULL PRIMARY KEY,
  Router_Software NVARCHAR (50) DEFAULT NULL,
  routername NVARCHAR (80) DEFAULT NULL,
    Time_Entered NVARCHAR (50) DEFAULT NULL
);







