/*
 * XML Type:  strList
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.StrList
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML strList(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class StrListImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.StrList
{
    
    public StrListImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STRVALUE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strValue");
    
    
    /**
     * Gets array of all "strValue" elements
     */
    public java.lang.String[] getStrValueArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(STRVALUE$0, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "strValue" element
     */
    public java.lang.String getStrValueArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STRVALUE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "strValue" elements
     */
    public org.apache.xmlbeans.XmlString[] xgetStrValueArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(STRVALUE$0, targetList);
            org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "strValue" element
     */
    public org.apache.xmlbeans.XmlString xgetStrValueArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(STRVALUE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return (org.apache.xmlbeans.XmlString)target;
        }
    }
    
    /**
     * Returns number of "strValue" element
     */
    public int sizeOfStrValueArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STRVALUE$0);
        }
    }
    
    /**
     * Sets array of all "strValue" element
     */
    public void setStrValueArray(java.lang.String[] strValueArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(strValueArray, STRVALUE$0);
        }
    }
    
    /**
     * Sets ith "strValue" element
     */
    public void setStrValueArray(int i, java.lang.String strValue)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STRVALUE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(strValue);
        }
    }
    
    /**
     * Sets (as xml) array of all "strValue" element
     */
    public void xsetStrValueArray(org.apache.xmlbeans.XmlString[]strValueArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(strValueArray, STRVALUE$0);
        }
    }
    
    /**
     * Sets (as xml) ith "strValue" element
     */
    public void xsetStrValueArray(int i, org.apache.xmlbeans.XmlString strValue)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(STRVALUE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(strValue);
        }
    }
    
    /**
     * Inserts the value as the ith "strValue" element
     */
    public void insertStrValue(int i, java.lang.String strValue)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(STRVALUE$0, i);
            target.setStringValue(strValue);
        }
    }
    
    /**
     * Appends the value as the last "strValue" element
     */
    public void addStrValue(java.lang.String strValue)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STRVALUE$0);
            target.setStringValue(strValue);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "strValue" element
     */
    public org.apache.xmlbeans.XmlString insertNewStrValue(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(STRVALUE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "strValue" element
     */
    public org.apache.xmlbeans.XmlString addNewStrValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(STRVALUE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "strValue" element
     */
    public void removeStrValue(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STRVALUE$0, i);
        }
    }
}
