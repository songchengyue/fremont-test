/*
 * XML Type:  CICountNodes
 * Namespace: http://schemas.hp.com/ucmdb/1/types/query
 * Java type: com.hp.schemas.ucmdb._1.types.query.CICountNodes
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.query.impl;
/**
 * An XML CICountNodes(@http://schemas.hp.com/ucmdb/1/types/query).
 *
 * This is a complex type.
 */
public class CICountNodesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.query.CICountNodes
{
    
    public CICountNodesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CICOUNTNODE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/query", "CICountNode");
    
    
    /**
     * Gets array of all "CICountNode" elements
     */
    public com.hp.schemas.ucmdb._1.types.query.CICountNode[] getCICountNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CICOUNTNODE$0, targetList);
            com.hp.schemas.ucmdb._1.types.query.CICountNode[] result = new com.hp.schemas.ucmdb._1.types.query.CICountNode[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "CICountNode" element
     */
    public com.hp.schemas.ucmdb._1.types.query.CICountNode getCICountNodeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.CICountNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.CICountNode)get_store().find_element_user(CICOUNTNODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "CICountNode" element
     */
    public int sizeOfCICountNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CICOUNTNODE$0);
        }
    }
    
    /**
     * Sets array of all "CICountNode" element
     */
    public void setCICountNodeArray(com.hp.schemas.ucmdb._1.types.query.CICountNode[] ciCountNodeArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(ciCountNodeArray, CICOUNTNODE$0);
        }
    }
    
    /**
     * Sets ith "CICountNode" element
     */
    public void setCICountNodeArray(int i, com.hp.schemas.ucmdb._1.types.query.CICountNode ciCountNode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.CICountNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.CICountNode)get_store().find_element_user(CICOUNTNODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(ciCountNode);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "CICountNode" element
     */
    public com.hp.schemas.ucmdb._1.types.query.CICountNode insertNewCICountNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.CICountNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.CICountNode)get_store().insert_element_user(CICOUNTNODE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "CICountNode" element
     */
    public com.hp.schemas.ucmdb._1.types.query.CICountNode addNewCICountNode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.CICountNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.CICountNode)get_store().add_element_user(CICOUNTNODE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "CICountNode" element
     */
    public void removeCICountNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CICOUNTNODE$0, i);
        }
    }
}
