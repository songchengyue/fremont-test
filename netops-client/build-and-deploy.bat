set JAVA_OPTS=-Dspring.profiles.active=local -Dspring.config.location=classpath:,file:/etc/netops/site-application-local.yml
set CATALINA_HOME=c:\apps\apache-tomcat-8.0.33
-- c:\apps\apache-tomcat-8.0.33\bin\catalina.bat stop
mvn package
del C:\Apps\apache-tomcat-8.0.33\webapps\Netops.war C:\Apps\apache-tomcat-8.0.33\webapps\Netops
move target\netops-client-0.0.1-SNAPSHOT.war C:\Apps\apache-tomcat-8.0.33\webapps\Netops.war
c:\apps\apache-tomcat-8.0.33\bin\catalina.bat start
