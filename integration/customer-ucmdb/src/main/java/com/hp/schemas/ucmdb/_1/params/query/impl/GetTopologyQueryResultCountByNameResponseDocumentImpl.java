/*
 * An XML document type.
 * Localname: getTopologyQueryResultCountByNameResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getTopologyQueryResultCountByNameResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetTopologyQueryResultCountByNameResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponseDocument
{
    
    public GetTopologyQueryResultCountByNameResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETTOPOLOGYQUERYRESULTCOUNTBYNAMERESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getTopologyQueryResultCountByNameResponse");
    
    
    /**
     * Gets the "getTopologyQueryResultCountByNameResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse getGetTopologyQueryResultCountByNameResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse)get_store().find_element_user(GETTOPOLOGYQUERYRESULTCOUNTBYNAMERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getTopologyQueryResultCountByNameResponse" element
     */
    public void setGetTopologyQueryResultCountByNameResponse(com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse getTopologyQueryResultCountByNameResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse)get_store().find_element_user(GETTOPOLOGYQUERYRESULTCOUNTBYNAMERESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse)get_store().add_element_user(GETTOPOLOGYQUERYRESULTCOUNTBYNAMERESPONSE$0);
            }
            target.set(getTopologyQueryResultCountByNameResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getTopologyQueryResultCountByNameResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse addNewGetTopologyQueryResultCountByNameResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponse)get_store().add_element_user(GETTOPOLOGYQUERYRESULTCOUNTBYNAMERESPONSE$0);
            return target;
        }
    }
}
