package com.telstra.oauth2.server.saml1;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.saml1.core.Attribute;
import org.opensaml.saml1.core.AttributeStatement;
import org.opensaml.saml1.core.AttributeValue;
import org.opensaml.saml1.core.Assertion;
import org.opensaml.saml1.core.AuthenticationStatement;
import org.opensaml.saml1.core.ConfirmationMethod;
import org.opensaml.saml1.core.NameIdentifier;
import org.opensaml.saml1.core.Subject;
import org.opensaml.saml1.core.SubjectConfirmation;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObjectBuilder;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.validation.ValidationException;

public class AssertionParserTest {
	
	private SAMLObjectBuilder<SAMLObject> assertionBuilder;
	private XMLObjectBuilderFactory builderFactory;
	private SAMLObjectBuilder<SAMLObject> nameIdBuilder;
	private SAMLObjectBuilder<SAMLObject> subjectConfirmationBuilder;
	private SubjectConfirmation subjectConfirmation;
	private SAMLObjectBuilder<SAMLObject> confirmationMethodBuilder;
	private ConfirmationMethod confirmationMethod;
	private SAMLObjectBuilder<SAMLObject>  subjectBuilder;
	private Subject subject;
	private SAMLObjectBuilder<SAMLObject> authStatementBuilder;
	private AuthenticationStatement authnStatement;
	private SAMLObjectBuilder<SAMLObject> attrBuilder;
	private Attribute attrGroups;
	private XMLObjectBuilder<SAMLObject> stringBuilder;
	private XSString attrNewValue;
	private  SAMLObjectBuilder<SAMLObject> attrStatementBuilder;
	private AttributeStatement attrStatement;
	private NameIdentifier nameId;
	private AssertionParser parser;
	
	private static String strNameID = "testUserID";
	private static String strNameQualifier = "Example:FrontEnd";
	private static String strAuthMethod = "SunAccessManager";
	
	@Before
	public void before() throws ConfigurationException {
		
		//
		// setup builders for assertion , AuthenticationStatements and AttributeStatements 
		//
		DefaultBootstrap.bootstrap();
		builderFactory = Configuration.getBuilderFactory();
		assertionBuilder =  (SAMLObjectBuilder<SAMLObject>) builderFactory.getBuilder(Assertion.DEFAULT_ELEMENT_NAME);
		nameIdBuilder = (SAMLObjectBuilder) builderFactory.getBuilder(NameIdentifier.DEFAULT_ELEMENT_NAME);
        nameId = (NameIdentifier) nameIdBuilder.buildObject();
        confirmationMethodBuilder = (SAMLObjectBuilder)  builderFactory.getBuilder(ConfirmationMethod.DEFAULT_ELEMENT_NAME);
        confirmationMethod = (ConfirmationMethod) confirmationMethodBuilder.buildObject();
        confirmationMethod.setConfirmationMethod("urn:oasis:names:tc:SAML:1.0:cm:sender-vouches");
        subjectConfirmationBuilder = (SAMLObjectBuilder) builderFactory.getBuilder(SubjectConfirmation.DEFAULT_ELEMENT_NAME);
        subjectConfirmation = (SubjectConfirmation) subjectConfirmationBuilder.buildObject();
        subjectConfirmation.getConfirmationMethods().add(confirmationMethod);
        
        //
        // Create the Subject
        //
        subjectBuilder = (SAMLObjectBuilder) builderFactory.getBuilder(Subject.DEFAULT_ELEMENT_NAME);
        subject = (Subject) subjectBuilder.buildObject();
        subject.setNameIdentifier(nameId);
        subject.setSubjectConfirmation(subjectConfirmation);
        authStatementBuilder = (SAMLObjectBuilder) builderFactory.getBuilder(AuthenticationStatement.DEFAULT_ELEMENT_NAME);
        authnStatement = (AuthenticationStatement) authStatementBuilder.buildObject();
        authnStatement.setSubject(subject);
        authnStatement.setAuthenticationMethod(strAuthMethod);
        authnStatement.setAuthenticationInstant(new DateTime());
        
        //
        // Create the attribute statement
        //
        attrBuilder = (SAMLObjectBuilder) builderFactory.getBuilder(Attribute.DEFAULT_ELEMENT_NAME);
        attrGroups = (Attribute) attrBuilder.buildObject();
        attrGroups.setAttributeName("Groups");
        stringBuilder = builderFactory.getBuilder(XSString.TYPE_NAME);
        attrNewValue = (XSString) stringBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
        attrNewValue.setValue("AssetManager");
        attrGroups.getAttributeValues().add(attrNewValue);
        attrStatementBuilder = (SAMLObjectBuilder) builderFactory.getBuilder(AttributeStatement.DEFAULT_ELEMENT_NAME);
        attrStatement = (AttributeStatement) attrStatementBuilder.buildObject();
        attrStatement.getAttributes().add(attrGroups);
		parser = new AssertionParser();

		
	}
	
	@Test
	public void validUser() throws ValidationException, ConfigurationException{
		
		//
		//setting the nameId 
		//
	     nameId.setNameIdentifier(strNameID);
         nameId.setNameQualifier(strNameQualifier);
         nameId.setFormat(NameIdentifier.UNSPECIFIED);
         
		//
		// Create Assertion to test
		//
        Assertion assertion = (Assertion)assertionBuilder.buildObject();
        
        //
        //setting  AuthenticationStatements and AttributeStatements for assertion of user
        //
        assertion.getAuthenticationStatements().add(authnStatement);
        assertion.getAttributeStatements().add(attrStatement);
        parser.parseUser(assertion);
			
	}
	
	@Test(expected = ValidationException.class)
	public void inValidSubject() throws ValidationException, ConfigurationException{
		
		//
		//setting the nameId 
		//
	     nameId.setNameIdentifier(strNameID);
         nameId.setNameQualifier(strNameQualifier);
         nameId.setFormat(NameIdentifier.UNSPECIFIED);
         
		//
		// Create Assertion to test
		//
        Assertion assertion = (Assertion)assertionBuilder.buildObject();
        
        //
        //not setting  AuthenticationStatements and AttributeStatements for assertion
        //
        parser.parseUser(assertion);
			
	}
	
	@Test
	public void inValidAttribute() throws ValidationException,IllegalArgumentException, ConfigurationException{
		
		//
		//setting the nameId 
		//
	     nameId.setNameIdentifier(strNameID);
         nameId.setNameQualifier(strNameQualifier);
         nameId.setFormat(NameIdentifier.UNSPECIFIED);
         
		//
		// Create Assertion to test
		//
        Assertion assertion = (Assertion)assertionBuilder.buildObject();
        
        //
        //setting AuthenticationStatements and not setting  AttributeStatements for assertion
        //
        assertion.getAuthenticationStatements().add(authnStatement);
        parser.parseUser(assertion);
			
	}
	
	

}
