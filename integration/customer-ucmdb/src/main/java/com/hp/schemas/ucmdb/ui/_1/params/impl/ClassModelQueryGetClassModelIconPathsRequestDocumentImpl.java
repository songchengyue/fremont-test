/*
 * An XML document type.
 * Localname: ClassModelQueryGetClassModelIconPathsRequest
 * Namespace: http://schemas.hp.com/ucmdb/ui/1/params
 * Java type: com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.ui._1.params.impl;
/**
 * A document containing one ClassModelQueryGetClassModelIconPathsRequest(@http://schemas.hp.com/ucmdb/ui/1/params) element.
 *
 * This is a complex type.
 */
public class ClassModelQueryGetClassModelIconPathsRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequestDocument
{
    
    public ClassModelQueryGetClassModelIconPathsRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLASSMODELQUERYGETCLASSMODELICONPATHSREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/ui/1/params", "ClassModelQueryGetClassModelIconPathsRequest");
    
    
    /**
     * Gets the "ClassModelQueryGetClassModelIconPathsRequest" element
     */
    public com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest getClassModelQueryGetClassModelIconPathsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest target = null;
            target = (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest)get_store().find_element_user(CLASSMODELQUERYGETCLASSMODELICONPATHSREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ClassModelQueryGetClassModelIconPathsRequest" element
     */
    public void setClassModelQueryGetClassModelIconPathsRequest(com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest classModelQueryGetClassModelIconPathsRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest target = null;
            target = (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest)get_store().find_element_user(CLASSMODELQUERYGETCLASSMODELICONPATHSREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest)get_store().add_element_user(CLASSMODELQUERYGETCLASSMODELICONPATHSREQUEST$0);
            }
            target.set(classModelQueryGetClassModelIconPathsRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "ClassModelQueryGetClassModelIconPathsRequest" element
     */
    public com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest addNewClassModelQueryGetClassModelIconPathsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest target = null;
            target = (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsRequest)get_store().add_element_user(CLASSMODELQUERYGETCLASSMODELICONPATHSREQUEST$0);
            return target;
        }
    }
}
