/*
 * An XML document type.
 * Localname: dispatchAdHocJobResopnse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params;


/**
 * A document containing one dispatchAdHocJobResopnse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public interface DispatchAdHocJobResopnseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(DispatchAdHocJobResopnseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("dispatchadhocjobresopnsef685doctype");
    
    /**
     * Gets the "dispatchAdHocJobResopnse" element
     */
    com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse getDispatchAdHocJobResopnse();
    
    /**
     * Sets the "dispatchAdHocJobResopnse" element
     */
    void setDispatchAdHocJobResopnse(com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse dispatchAdHocJobResopnse);
    
    /**
     * Appends and returns a new empty "dispatchAdHocJobResopnse" element
     */
    com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse addNewDispatchAdHocJobResopnse();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument newInstance() {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
