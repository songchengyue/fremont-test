package com.telstra.oauth2.server.saml1;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.opensaml.saml1.core.Assertion;
import org.opensaml.xml.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

/**
 * Token granter for SAML v1.1 bearer token.
 *
 * TODO - contribute to Spring SAML project.
 * 
 * @author david_donn
 *
 */
public class SAMLTokenGranter extends AbstractTokenGranter {

	public static final String GRANT_TYPE = "urn:ietf:params:oauth:grant-type:saml1-bearer";

	static final String ASSERTION = "assertion";
	
	//
	// Unfortunately not all authority information is available from our Identity Provider :-(
	// We trust a third-party (in our case the C3 Portal) to send us the correct customer id
	// associated with an assertion.
	//
	// We should try to get rid of this by getting this info from our Identity Provider as
	// it is not secure!
	//
	
	static final String CUSTOMER_ID = "customerid";
	
	static final String CUSTOMER_ID_ROLE_NAME_PREFIX = "ROLE_CUSTOMER_ID_";
	
	static final String USER_PROFILE = "userprofile";
	
	static final String USER_PROFILE_ROLE_NAME_PREFIX = "ROLE_USER_PROFILE_";
	
	@Autowired
	private AssertionParser parser;
	
	@Autowired
	private AssertionValidator validator;

	private final XMLAssertionUnmarshaller unmarshaller;

	public SAMLTokenGranter(AuthorizationServerTokenServices tokenServices,
			ClientDetailsService clientDetailsService,
			OAuth2RequestFactory requestFactory,
			XMLAssertionUnmarshaller unmarshaller) {
		this(tokenServices, clientDetailsService, requestFactory,
				GRANT_TYPE, unmarshaller);
	}

	protected SAMLTokenGranter(
			AuthorizationServerTokenServices tokenServices,
			ClientDetailsService clientDetailsService,
			OAuth2RequestFactory requestFactory, String grantType,
			XMLAssertionUnmarshaller unmarshaller) {
		super(tokenServices, clientDetailsService, requestFactory,
				grantType);
		this.unmarshaller = unmarshaller;
	}

	@Override
	protected OAuth2Authentication getOAuth2Authentication(
			ClientDetails client, TokenRequest tokenRequest) {
		Map<String, String> requestParams = tokenRequest
				.getRequestParameters();

		//
		// Assertion processing
		//
		
		// TODO throw exception here? Not sure which one.
		if (requestParams.isEmpty() || requestParams.get(ASSERTION) == null || requestParams.get(CUSTOMER_ID) == null) {
			return null;
		}

		// TODO check for problem with decoding
		byte[] assertionXml = Base64.decodeBase64(requestParams.get(ASSERTION));

		Assertion assertion = unmarshaller
				.unmarshallAssertion(assertionXml);

		validate(assertion);
		
		//
		// CustomerId processing
		//
		// TODO throw exception here? Not sure which one.
		
		OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
		
		// if this is a client acting on behalf of a user, then we should set the user authentication
		return new OAuth2Authentication(storedOAuth2Request, createPreAuthenticatedAuthentication(assertion, requestParams.get(CUSTOMER_ID), requestParams.get(USER_PROFILE)));
	}
	
	private void validate(Assertion assertion) {
		try {
			validator.validate(assertion);
		} catch (ValidationException e) {
			throw new InvalidGrantException(e.getMessage());
		}	
	}
	
	private Authentication createPreAuthenticatedAuthentication(Assertion assertion, String customerId, String userProfile) {
		try {
			Collection<GrantedAuthority> authorities = parser.parseRoles(assertion);
			
			// TODO - is this the most appropriate place to store this?
			//        Note. Any change will also require CustomerIdResolver class to change!
			authorities.add(new SimpleGrantedAuthority(CUSTOMER_ID_ROLE_NAME_PREFIX + customerId));
			authorities.add(new SimpleGrantedAuthority(USER_PROFILE_ROLE_NAME_PREFIX + userProfile));
			
			return new PreAuthenticatedAuthenticationToken(parser.parseUser(assertion), null, authorities);
		} catch (ValidationException e) {
			throw new InvalidGrantException(e.getMessage());
		}
	}

}

