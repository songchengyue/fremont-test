#!/usr/bin/perl

#
# System: OLSS - plotting module
# Function:
#
# $Id: OLSS_PLOT.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $
#

package OLSS_PLOT;
use Exporter;
use Digest::MD5 qw(md5_hex);
use Time::localtime;
use Date::Parse;

# use RRDs;

our @ISA        = qw(Exporter);
our @EXPORT     = qw();


# Plot graph using RRDTool
# Used for 7 plot
# Pre: datafile, giffilename, start_ux, end_ux, interval, samplesize, 
#	$vlabel, convert [0|1], "AREA|LINE", x-axis grid, "datacol:-:label", ...
# Post: giffilename
sub rrd_plot {
open rrd,">/tmp/Chand_rrd";
        my @vals = @_;
        my $datafile = shift @vals;
        my $outfile = shift @vals;
        my $start = shift @vals;
        my $end = shift @vals;
        my $interval = shift @vals;
        my $samplesize = shift @vals;
        my $vlabel = shift @vals;
	my $xlabel = shift @vals;
	my $conv = shift @vals;
	my $gt = shift @vals;
	my $xgrid = shift @vals;

	# get first line type if available
	my (@t, $fgt);
	if ($gt =~ /\:/) {
		my @t = split( /\:/, $gt );
		$gt = $t[1];
		$fgt = $t[0];
	} else {
		$fgt = "AREA"; # default of first line is AREA
	}

	# get vmin and vmax
	my $vminmax = 0;
	my ($vmin, $vmax);
	if ($vlabel =~ /\:/) {
		$vminmax = 1;
		$vmin = (split(/\:/,$vlabel))[1];
		$vmax = (split(/\:/,$vlabel))[2];
		$vlabel = (split(/\:/,$vlabel))[0];
	}

	#open(my $xout, ">/tmp/rrd-test");
	#print $xout "($datafile) ($outfile) ($start) ($end) ($interval) ($samplesize)\n";

        my $db = "/tmp/rrd.$$";
print rrd "db :$db \n";
        #my @colors = ('#00FF00', '#0000FF', '#FF0000', '#000066', '#990000', '#333333', '#555555');
        my @colors = ('#00FF00', '#0000FF', '#FF0000', '#666699', '#990033', '#333333', '#555555', '#FFDAB9', '#40E0D0', '#02B48C', '#EE82EE', '#483d8B', '#696969', '#DAA520');

        my (@cols, @labels, @rrds, @rrgraph, @graphctrl);

        push @cols, 0; # first column is timestamp
  
	#print $xout "TEST\n";
  
        foreach my $i (0..$#vals) {
                my ($datacol, $label) = split(/\:\-\:/, $vals[$i]);
                # need to align datacol with col 1 = 0
                #$datacol = $datacol - 1;
                #$labels[$datacol] = $label;
                push @cols, $datacol;
                push @rrds, "DS:col$datacol:GAUGE:$interval:0:U";
                #push @rrgraph, "DEF:col$datacol=$db:col$datacol:AVERAGE";
                push @rrgraph, "DEF:col$datacol=$db:col$datacol:MAX";
                if($i == 0) {
                        #push @graphctrl, "AREA:col$datacol" . "#00FF00:" . "\"$label\"";
                        #push @graphctrl, "$fgt:col$datacol" . "#00FF00:" . "\"$label\"" . "";
                        push @graphctrl, "$fgt:col$datacol" . "#00FF00:" . "$label" . "";
                } elsif ($i == 1) {
                        #push @graphctrl, "LINE1:col$datacol" . "#0000FF:" . "\"$label\"";
                        #push @graphctrl, "$gt:col$datacol" . "#0000FF:" . "\"$label\"" . "";
                        push @graphctrl, "$gt:col$datacol" . "#0000FF:" . "$label" . "";
                } else {
                        #push @graphctrl, "LINE1:col$datacol" . "$colors[$i]:" . "\"$label\"";
                        #push @graphctrl, "$gt:col$datacol" . "$colors[$i]:" . "\"$label\"" . " ";
                        push @graphctrl, "$gt:col$datacol" . "$colors[$i]:" . "$label" . "";
                }
        }

	#print "Cols (@cols), RRDS (@rrds), RRGRAPH (@rrgraph), GCTRL (@graphctrl)\n";

        # Now create the RRA
        $samplesize += 100;
        #push @rrds, "RRA:AVERAGE:0.5:1:$samplesize";
        push @rrds, "RRA:MAX:0.5:1:$samplesize";

        # Now create the RRD
        my $cstart = $start - $interval; #100;

	# if daily graph - rrd assumes reading from one day to another is whole day
	# therefore, need to phase shift readings by 86400
	my $phase = ($interval == 86400) ? $interval : 0;
	#my $phase = ($interval == 86400) ? 100 : 0;
print rrd "rrdss------->@rrds\n";
	
        RRDs::create("$db", "--start", "$cstart", "--step", "$interval", @rrds);
        my $err = RRDs::error;
 
	#print $xout "($db) ($start) (@rrds)\n";
	#print $xout "(@rrgraph) (@graphctrl)\n";
	#print $xout "1 ERr ($err)\n";

        my $count = 0;
        if(-e $datafile) {
                open(my $in, "$datafile");
                while(my $l = <$in>) {
			next if($l =~ /Timestamp/);
                        chomp($l);
                        my @dataval = (split(/\s+/, $l))[@cols];
                        if( ($interval == 86400) && ($conv == 1) ) {
                                # day - need to calc in/out figures to kbps
                                $dataval[1] = int((($dataval[1] * 80) / 864) + 0.5);
                                $dataval[2] = int((($dataval[2] * 80) / 864) + 0.5);
                        }
  
                        # iterate through - move kbps to bps - for GIA
			if($conv) {
                        	foreach my $i (0..$#dataval) {
                                	next if($i == 0);
                                	$dataval[$i] *= 1000;
                        	}
			}

			#$dataval[0] += $phase;
			if($count == 0) {
				my $v = join ':', @dataval;
				print rrd "vvvvv---->$v\n";
				RRDs::update("$db", "$v");
			}
			$dataval[0] += $phase;

                        my $v = join ':', @dataval;
                        RRDs::update("$db", "$v");
                        $err = RRDs::error;
			#print $xout "Update ($v)\n";
                        if($err) {
                                #print $xout "2 Err ($err)\n";
                        }
                }
        } else {
		my $i = $start;
		while($i <= $end) {
			my $v = "$i:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0";
                        RRDs::update("$db", "$v");
			$i += $interval;
		}
	}


        # Now generate the graph
	$end += $phase;

	#if($vlabel =~ /ddr/i) {
	#my ($tmp);
	#if (length($vmax) != 0) {
	#}
#"--upper-limit", "100", "--lower-limit", "99.9", "--rigid",

	my @ctrl;
	push @ctrl, "$outfile", "--start", "$start", "--end", "$end", "--height", "150";
        if ($xgrid ne "") {
                push @ctrl, "--x-grid", "$xgrid";
        }

	if ($vlabel =~ /^ddr|%$/i) {
		#push @ctrl, "--alt-autoscale";
		push @ctrl, "--alt-autoscale", "--alt-y-grid";
	};
	if ($vminmax) {
		push @ctrl, "--lower-limit", "$vmin", "--upper-limit", "$vmax", "--rigid";
	}
	push @ctrl, "--vertical-label", "$vlabel", "--title", "$xlabel", @rrgraph, @graphctrl;
print rrd " ctrl :@ctrl \n";

	RRDs::graph( @ctrl );

##	if($vlabel =~ /^ddr|%$/i) {
##                RRDs::graph("$outfile", "--start", "$start", "--end", "$end",
##                        "--height", "150", "--alt-autoscale",
##                         "--vertical-label", "$vlabel", "--title", "$xlabel",
##                        @rrgraph, @graphctrl);
##        } else {
##                RRDs::graph("$outfile", "--start", "$start", "--end", "$end",
##                        "--height", "150",
##                         "--vertical-label", "$vlabel", "--title", "$xlabel",
##                        @rrgraph, @graphctrl);
##        }

        $err = RRDs::error; 
        #if($err) {
        #        print $xout "3 Err ($err)\n";
        #}

        unlink($db);
        #unlink($datafile);

        #close($xout);
#       unlink("/tmp/rrd-test");

}


1;
