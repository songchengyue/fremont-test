/*
 * XML Type:  TopologyCountMap
 * Namespace: http://schemas.hp.com/ucmdb/1/types/query
 * Java type: com.hp.schemas.ucmdb._1.types.query.TopologyCountMap
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.query.impl;
/**
 * An XML TopologyCountMap(@http://schemas.hp.com/ucmdb/1/types/query).
 *
 * This is a complex type.
 */
public class TopologyCountMapImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.query.TopologyCountMap
{
    
    public TopologyCountMapImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CICOUNTNODES$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/query", "CICountNodes");
    private static final javax.xml.namespace.QName RELATIONCOUNTNODES$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/query", "relationCountNodes");
    
    
    /**
     * Gets the "CICountNodes" element
     */
    public com.hp.schemas.ucmdb._1.types.query.CICountNodes getCICountNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.CICountNodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.CICountNodes)get_store().find_element_user(CICOUNTNODES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "CICountNodes" element
     */
    public boolean isSetCICountNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CICOUNTNODES$0) != 0;
        }
    }
    
    /**
     * Sets the "CICountNodes" element
     */
    public void setCICountNodes(com.hp.schemas.ucmdb._1.types.query.CICountNodes ciCountNodes)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.CICountNodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.CICountNodes)get_store().find_element_user(CICOUNTNODES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.query.CICountNodes)get_store().add_element_user(CICOUNTNODES$0);
            }
            target.set(ciCountNodes);
        }
    }
    
    /**
     * Appends and returns a new empty "CICountNodes" element
     */
    public com.hp.schemas.ucmdb._1.types.query.CICountNodes addNewCICountNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.CICountNodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.CICountNodes)get_store().add_element_user(CICOUNTNODES$0);
            return target;
        }
    }
    
    /**
     * Unsets the "CICountNodes" element
     */
    public void unsetCICountNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CICOUNTNODES$0, 0);
        }
    }
    
    /**
     * Gets the "relationCountNodes" element
     */
    public com.hp.schemas.ucmdb._1.types.query.RelationCountNodes getRelationCountNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.RelationCountNodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.RelationCountNodes)get_store().find_element_user(RELATIONCOUNTNODES$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "relationCountNodes" element
     */
    public boolean isSetRelationCountNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RELATIONCOUNTNODES$2) != 0;
        }
    }
    
    /**
     * Sets the "relationCountNodes" element
     */
    public void setRelationCountNodes(com.hp.schemas.ucmdb._1.types.query.RelationCountNodes relationCountNodes)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.RelationCountNodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.RelationCountNodes)get_store().find_element_user(RELATIONCOUNTNODES$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.query.RelationCountNodes)get_store().add_element_user(RELATIONCOUNTNODES$2);
            }
            target.set(relationCountNodes);
        }
    }
    
    /**
     * Appends and returns a new empty "relationCountNodes" element
     */
    public com.hp.schemas.ucmdb._1.types.query.RelationCountNodes addNewRelationCountNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.query.RelationCountNodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.query.RelationCountNodes)get_store().add_element_user(RELATIONCOUNTNODES$2);
            return target;
        }
    }
    
    /**
     * Unsets the "relationCountNodes" element
     */
    public void unsetRelationCountNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RELATIONCOUNTNODES$2, 0);
        }
    }
}
