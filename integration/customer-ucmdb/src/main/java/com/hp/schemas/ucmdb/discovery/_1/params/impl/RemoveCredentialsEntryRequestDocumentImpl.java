/*
 * An XML document type.
 * Localname: removeCredentialsEntryRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one removeCredentialsEntryRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class RemoveCredentialsEntryRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequestDocument
{
    
    public RemoveCredentialsEntryRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REMOVECREDENTIALSENTRYREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "removeCredentialsEntryRequest");
    
    
    /**
     * Gets the "removeCredentialsEntryRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequest getRemoveCredentialsEntryRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequest)get_store().find_element_user(REMOVECREDENTIALSENTRYREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "removeCredentialsEntryRequest" element
     */
    public void setRemoveCredentialsEntryRequest(com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequest removeCredentialsEntryRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequest)get_store().find_element_user(REMOVECREDENTIALSENTRYREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequest)get_store().add_element_user(REMOVECREDENTIALSENTRYREQUEST$0);
            }
            target.set(removeCredentialsEntryRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "removeCredentialsEntryRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequest addNewRemoveCredentialsEntryRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RemoveCredentialsEntryRequest)get_store().add_element_user(REMOVECREDENTIALSENTRYREQUEST$0);
            return target;
        }
    }
}
