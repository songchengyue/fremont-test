/*
 * XML Type:  removeCustomerRequest
 * Namespace: http://schemas.hp.com/ucmdb/management/1/params
 * Java type: com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.management._1.params.impl;
/**
 * An XML removeCustomerRequest(@http://schemas.hp.com/ucmdb/management/1/params).
 *
 * This is a complex type.
 */
public class RemoveCustomerRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.management._1.params.RemoveCustomerRequest
{
    
    public RemoveCustomerRequestImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CUSTOMERID$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/management/1/params", "CustomerID");
    
    
    /**
     * Gets the "CustomerID" element
     */
    public int getCustomerID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CUSTOMERID$0, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "CustomerID" element
     */
    public org.apache.xmlbeans.XmlInt xgetCustomerID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(CUSTOMERID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CustomerID" element
     */
    public void setCustomerID(int customerID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CUSTOMERID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CUSTOMERID$0);
            }
            target.setIntValue(customerID);
        }
    }
    
    /**
     * Sets (as xml) the "CustomerID" element
     */
    public void xsetCustomerID(org.apache.xmlbeans.XmlInt customerID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(CUSTOMERID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(CUSTOMERID$0);
            }
            target.set(customerID);
        }
    }
}
