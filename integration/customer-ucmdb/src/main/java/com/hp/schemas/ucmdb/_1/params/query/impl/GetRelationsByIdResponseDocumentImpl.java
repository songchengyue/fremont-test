/*
 * An XML document type.
 * Localname: getRelationsByIdResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getRelationsByIdResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetRelationsByIdResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponseDocument
{
    
    public GetRelationsByIdResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETRELATIONSBYIDRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getRelationsByIdResponse");
    
    
    /**
     * Gets the "getRelationsByIdResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse getGetRelationsByIdResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse)get_store().find_element_user(GETRELATIONSBYIDRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getRelationsByIdResponse" element
     */
    public void setGetRelationsByIdResponse(com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse getRelationsByIdResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse)get_store().find_element_user(GETRELATIONSBYIDRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse)get_store().add_element_user(GETRELATIONSBYIDRESPONSE$0);
            }
            target.set(getRelationsByIdResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getRelationsByIdResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse addNewGetRelationsByIdResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse)get_store().add_element_user(GETRELATIONSBYIDRESPONSE$0);
            return target;
        }
    }
}
