#!/usr/bin/perl

use strict;
use warnings;

use lib "/usr/local/www/wanchai/netops/modules";
use lib "/usr/local/www/wanchai/modules";

use CMS_DB;
use CMS_REVERSEDEL_DB;
use CMS_OLSS;
use bignum;
use Net::IP;


#use constant FILE_PATH_ZONE => "/usr/local/www/wanchai/cms/data/revdns/rev_zones/international/";
use constant FILE_PATH_ZONE => "/usr/local/www/wanchai/netops/dns/revdns/rev_zones/international/";

#use constant FILE_PATH_CONF_TEMPL => "/usr/local/www/wanchai/cms/scripts/make_revdns_conf.template.conf";
use constant FILE_PATH_CONF_TEMPL => "/usr/local/www/wanchai/netops/dns/revdns/template/make_revdns_conf.template.conf";

#use constant FILE_PATH_ZONE_TEMPL => "/usr/local/www/wanchai/cms/scripts/make_revdns_conf.template.zone";
use constant FILE_PATH_ZONE_TEMPL => "/usr/local/www/wanchai/netops/dns/revdns/template/make_revdns_conf.template.zone";

#use constant FILE_PATH_SN => "/usr/local/www/wanchai/cms/scripts/make_revdns_conf.serialno";
use constant FILE_PATH_SN => "/usr/local/www/wanchai/netops/dns/revdns/template/make_revdns_conf.serialno";

open (PRE,">/tmp/make_rev_pl1");
my @now = gmtime;


my ($yr,$mth,$mday,$second, $minute, $hour) = ($now[5], $now[4], $now[3],$now[0],$now[1],$now[2]);
$yr += 1900;
$mth += 1;
if ($mth <= 9) { $mth = "0".$mth; }
if ($mday <= 9) { $mday = "0".$mday; }
my $cur_sec = (($hour*3600)+($minute*60)+$second);
my $cur_time = "$hour:$minute:$second";
my @ar_time = '';
my $hr_format = '';
my @db_hr1 = '';
my $ar_ptr = '';
my $count_ptr = '';
my ($req_time,@req_time1,$request,$req_time_1) = '';
my $ref_time = '';
my @db_sec1 = '';
my @db_hr = '';
my $db_sec = '';
my $sec_format = '';
my @db_date1 = '';
my $db_mon = '';
my $db_day = '';
my $db_year = '';
my @req_time ='';
my @records = '';
my %rec = ();
my $cname='';
my $c_name='';
my $lpct = 0;
my $count_request = 0;
my %names = ();
my %cnm = ();
my $ns = '';
my $cnm_ipp = '';
my $temp_name = '';
my $type_rec = '';
my $zone_hash = '';
my $zone_count = 0;
my $revptr = '';
my $rev_ptr = '';
my $valuesdb = ''; 
my $ptr = '';
my $ptr1='';
my @ptrec='';
my $last_oct = '';
my $domain = '';
my $rec_type = '';
my $status = '';
my $domain_new = '';
my @cnm_pre = '';
my $range = '';
my $cname_var = '';
my $ip_range = '';
my $host = "IN";
my $tempo = '';
my $cnm_tmp = '';
my $reverse_del_hostname = "ns03.telstraglobal.net. ipnoc.reach.com";
my $file_build_date = `date`;
my $base = '';
my @ar_ptr = '';
my $flag_ptr = 0;
my $i_ptr = 0;
my $countptr = '';
my $ct_req = '';
my $sc_req = '';
my $sel_ptr = '';
my $new_sel_ptr = '';
my $sel_ptr1 = '';
my $sel_ptr_new = '';
my $tempname = '';
my $name_s = '';
my $prev_last_oct = '';
my $prev_status = '';
my $prev_domain = '';
my $try_req = '';
my $reqt = '';

#added for delegation changes 
my $str=''; 
my @nib='';
my @temp_name='';




my $serial_number = "$yr$mth$mday";

open(SN, FILE_PATH_SN);
my $last_sn = <SN>;
chomp $last_sn;
my $last_upd_date = substr($last_sn,0,8);
my $last_ver = substr($last_sn,8,2);
print "last update date: $last_upd_date  last version: $last_ver\n";
close(SN);

if ($last_upd_date ne $serial_number) {  # current date is diff from last update date
        $serial_number .= "01";
} else {
        $last_ver = $last_ver +0 +1;
        $last_ver = "0".$last_ver if ($last_ver <= 9);
        $last_ver = "01" if ($last_ver == 100);
        $serial_number .= $last_ver;
}
print "current serial number: $serial_number\n";

open(SN, ">".FILE_PATH_SN);
print SN "$serial_number";
close(SN);

CMS_DB::connect_to_database;
print "connected to db...\n" ;

my @ptrvalue = '';
my $record = '';
my $dnstime =CMS_REVERSEDEL_DB::time_db;
print "time: $dnstime";
my $allvalues =CMS_REVERSEDEL_DB::all_values(0);

my $ar1_ptr = '';
my ($i,$j) = 0;
#get all distinct ptrrec from DBm
my $all_ptr = CMS_REVERSEDEL_DB::all_ptr_location(0,6);


foreach my $allptr (@$all_ptr){
	print "inside all ptr loop \n";
	push (@ar_ptr, $$allptr[0]);
print "values: $$allptr[0]\n";
	$ar1_ptr = ($all_ptr->[0]->[0]);
	#print PRE "all ptr1:$ar1_ptr\n";
	print PRE "all ptr:$ar_ptr[1]\n";
	my $arptr = $ar_ptr[1];
#	$count_ptr = CMS_REVERSEDEL_DB::count_ptr($arptr);
	#$countptr = ($count_ptr->[0]->[0]);
	#print PRE "countptr:$countptr\n";
print "aptr : $arptr\n";
	$sel_ptr1 = CMS_REVERSEDEL_DB::sel_ptr1($arptr);
print "sel_ptr1[0]->[0] : $sel_ptr1->[0]->[0]\n";
print "sel_ptr[0]->[1] : $sel_ptr1->[0]->[1]\n";
print "sel ptr[0] :$sel_ptr1->[0]\n";
print "sel ptr[1] : $sel_ptr1->[1]\n";
	# checking for IPv6 version
print "selptr++++ : $sel_ptr1->[0]->[1]\n";
	$sel_ptr1 = ($sel_ptr1->[0]->[0]);
		$tempo = '';
		$cnm_tmp = '';
  		$temp_name = '';
		$base = '';
		$name_s = '';
		$ns = '';
		%cnm = ();
		%names = ();
		$cname = '';
		my $new_sel_ptr = $sel_ptr1;
	for $i (0 .. $sel_ptr1) {
print "sel ptr I :$i ";
		if ($i == $sel_ptr1){
		print PRE "inside i condn\n";
		$tempo = '';
		$cnm_tmp = '';
  		$temp_name = '';
		$base = '';
		$name_s = '';
		$ns = '';
		}#end of if loop for i
		print PRE "inside countptr loop\n";
		$sel_ptr = CMS_REVERSEDEL_DB::sel_ptr($arptr);
		#$reqt = CMS_REVERSEDEL_DBv6::sel_ptr($arptr,2);
		


		print PRE "VALUE i:$i\n";
		print PRE "SEL:$sel_ptr1\n";
		#get highest request time from DB
		$valuesdb = CMS_REVERSEDEL_DB::values_db($arptr);
		$reqt = ($valuesdb->[0]->[0]);

		print "reqt:$reqt\n";

		$ptr = ($sel_ptr->[$i]->[0]);
		$last_oct = ($sel_ptr->[$i]->[1]);
		$rec_type = ($sel_ptr->[$i]->[2]);
print "record type : $rec_type\n";
		$domain = ($sel_ptr->[$i]->[3]);
print "new domain :$domain\n";
		$req_time = ($sel_ptr->[$i]->[4]);
		$ref_time = ($sel_ptr->[$i]->[5]);
		$status = ($sel_ptr->[$i]->[6]);
		print PRE "ptr:$ptr\tlast_oct:$last_oct\trec_type:$rec_type\tdomain:$domain\treq_time:$req_time\tref_time:$ref_time\n"; 
		print PRE "before request:$ptr\n";
				

		#check for DB connection
	  	$try_req = CMS_REVERSEDEL_DB::request_t($ptr);

		print PRE "try_req:$try_req->[0]->[0]\n";	

		print  "req_time1: $reqt\n";
			
		@db_hr1 = split(/\+/,$reqt);
		@db_hr = split(/ /,$db_hr1[0]);
		@db_sec1 = split(/\./,$db_hr[1]);
		$db_sec = (($db_sec1[0]*3600)+ ($db_sec1[1]*60) + ($db_sec1[2]));
		@db_date1 = split (/-/,$db_hr[0]);
		$sec_format = $db_sec;
		$hr_format = $db_hr1[0];
		@db_date1 = split (/-/,$db_hr[0]);
		$db_year = $db_date1[0];
		$db_mon = $db_date1[1];
		$db_day = $db_date1[2];

		#checkin request time against current time
		$mday =~ s/^\s+|\s+$//g;
		$mth =~ s/^\s+|\s+$//g;
		$mth =~ s/^\s+|\s+$//g;
		$sec_format =~ s/^\s+|\s+$//g;
		$hr_format =~ s/^\s+|\s+$//g;
		$db_year =~ s/^\s+|\s+$//g;
		$db_mon =~ s/^\s+|\s+$//g;
		$db_day =~ s/^\s+|\s+$//g;
		
		

		print  "mday:$mday\tdb_day:$db_day\tmth:$mth\tdb_mon:$db_mon\tcur_sec:$cur_sec\tsec_format:$sec_format\n";
		if (($mday eq $db_day) && ($mth eq $db_mon) && ($yr eq $db_year) && (($cur_sec - $sec_format)<=3600) ){
			print PRE "mday:$mday\tdb_day:$db_day\tmth:$mth\tdb_mon:$db_mon\tcur_sec:$cur_sec\tsec_format:$sec_format\n";
			print PRE "meets 1hr criteria\n";
			$count_request = 1;
		}#end of if req
		#}#end of j for
		
		print PRE "count_request:$count_request\n";
		if ($count_request ==1) {
			print "inside time loopp \n";
			print PRE "before lastoct sub: ptr:$ptr\tlast_oct:$last_oct\n";
			my $lastoct = CMS_REVERSEDEL_DB::lastoct($ptr,$last_oct);
			# lastoct query SELECT distinct ptrrec,last_oct from reverse where ptrrec like $ptr and last_oct like $last_oct and status = 1
			my $last_oct1 = $lastoct->[0]->[1];
			my $value = $lastoct->[0]->[2];
			my $ptr1 = $lastoct->[0]->[0];
			print PRE "last_oct1:$last_oct1\tptr1:$ptr1\n";
			@cnm_pre = split (/\//,$last_oct1);
                	$cnm_ipp = $cnm_pre[0];
			print "cnm_pre[1]: $cnm_pre[1]\n";
			$cname_var =rev_range($cnm_pre[1]);
			print "cname_var : $cname_var\n";
			$ip_range = ($cname_var+ $cnm_pre[0]);
			print "ip range :$ip_range \n";
			print PRE "cname_var:$cname_var\tip_range:$ip_range\n";
			print PRE "before tempname ptr:$ptr\n";
			
			##for each domain 
			$tempname  = CMS_REVERSEDEL_DB::rev_name($ptr,$req_time);
			#rev_name SELECT distinct(fqdn) from reverse where ptrrec like $ptr and status = 1 and request_time like $req_time and rec_type like 'NS' 
			print PRE "tempname:$tempname\n";
			
			print "domain : $prev_domain  new : $domain  status :$status\n ";
			if (($prev_domain ne $domain) && ($status == 1)){
			foreach $name_s(@$tempname){
			print  "inside name_s loop\n";
                        #$temp_name = "$cnm_pre[0]-$ip_range.$ptr. IN NS $$name_s[0]";			
			#$temp_name = "$cnm_pre[0].$ptr. IN NS $$name_s[0]"; - removed by HP

			##################### changes made 
			print "Value before expand: $value";
			$value = Net::IP::ip_expand_address($value, 6);  # expand compressed ipv6  			
  			print "Value after expan: $value";
			my @y = split(/\//,$value);
			my $z1= shift(@y); # ip
			print "z1: $z1";
			my $ip_prefix = shift(@y); #prefix
			print "ip_prefix: $ip_prefix";

			my $rev6=new Net::IP($z1); 				# Net:: ip function for ipv6.arpa 
			print "rev6: $rev6";
			my $ptrval=$rev6->reverse_ip();
			print "ptrval: $ptrval";
			#for substring nibble boundary
			my $ns_str_full = '';
			if ($ip_prefix >= 49 && $ip_prefix <= 52){
                                $str = substr($ptrval,40,-30);
				$ns_str_full = substr($ptrval,38,-30);
                        }elsif ($ip_prefix >= 53 && $ip_prefix <= 56){
                                $str = substr($ptrval,38,-30);
				$ns_str_full = substr($ptrval,36,-30);
                        }elsif ($ip_prefix >= 57 && $ip_prefix <= 60){
                                $str = substr($ptrval,36,-30);
				$ns_str_full = substr($ptrval,34,-30);
                        }elsif ($ip_prefix >= 61 && $ip_prefix <= 64){
                                $str = substr($ptrval,34,-30);
				$ns_str_full = substr($ptrval,32,-30);
                        }elsif ($ip_prefix == 48){
                                $str = substr($ptrval,42,-30);
				$ns_str_full = substr($ptrval,40,-30);
                        }
			print "str after substr: $str";
			#determining the last nibble for the prefix 
			
			my $ns_str = $str;
			$str = substr($ns_str_full,0,1);
                        $str = lc $str;

                        if ($ip_prefix%4 == 1){
                                if ($str >= 0 && $str <= 7){
                                        @nib=(0,1,2,3,4,5,6,7);
                                }else { @nib=(8,9,'a','b','c','d','e','f');
                                }
                        }elsif ($ip_prefix%4 == 2){
                                if ($str >= 0 && $str <= 3){
                                        @nib = (0,1,2,3);
                                }elsif ($str >= 4 && $str <= 7){
                                        @nib = (4,5,6,7);
                                }elsif ($str == 8 || $str == 9 || $str eq 'a' || $str eq 'b'){
                                        @nib = (8,9,'a','b');
                                }else { @nib = ('c','d','e','f');
                                }
                        }elsif($ip_prefix%4 == 3){
                                if ($str == 0 || $str == 1){
                                        @nib = (0,1);
                                }elsif($str == 2 || $str == 3){
                                        @nib = (2,3);
                                }elsif($str == 4 || $str == 5){
                                        @nib = (4,5);
                                }elsif($str == 6 || $str == 7){
                                        @nib = (6,7);
                                }elsif($str == 8 || $str == 9){
                                        @nib = (8,9);
                                }elsif($str eq 'a' || $str eq 'b'){
                                        @nib = ('a','b');
                                }elsif($str eq 'c' || $str eq 'd'){
                                        @nib = ('c','d');
                                }elsif($str eq 'e' || $str eq 'f'){
                                        @nib = ('e','f');
                                }
                        }elsif($ip_prefix%4 == 0){
                                @nib=($str, 0, 1, '0');
                        }

			#appending to the substring
			foreach my $pre(@nib){
			@cnm_pre=join("\.",$pre,$ns_str); 
   			
			#creating nibble record for NS
			foreach my $oct(@cnm_pre){
							$temp_name = "$oct IN NS $$name_s[0]";
							print "the entries r $temp_name \n ";
							push @{$names{$domain}},$temp_name;
						   }
			}													
                        #push @{$names{$domain}},$temp_name;
                        }#end of foreach
			}#end of prev dom
		
		
		
		###### start of removed section 
=begin comment
		if (($rec_type eq 'NS') && ($status ==1)) {
				print "\n inside rec type \n";
				print PRE "\ninside rec type NS condition\n";
				print PRE "\n before if prev:prev_last_oct:$prev_last_oct\tlast_oct:$last_oct\tprev_status:$prev_status\n";
			
			if (($prev_last_oct ne $last_oct) ||(($prev_last_oct eq $last_oct) && ($prev_status != 1))){
				print "prev_last_oct:$prev_last_oct\tprev_status:$prev_status\n";
				print PRE "prev_last_oct:$prev_last_oct\tprev_status:$prev_status\n";
				print "cnm_ipp : $cnm_ipp\n";
				print "ip_range : $ip_range\n";
				my $fi='1';
		
			
			for ($range = $cnm_ipp; $range <= $ip_range; $range++)
				{
				print "inside for loop $fi \n";
				print PRE "range: $range\t value:$cnm_ipp\t cname_var:$cname_var\tip range:$ip_range\n";
				$domain_new = "$range.$cnm_pre[0]-$ip_range.$ptr.";
				$cnm_tmp = "$range $host CNAME $domain_new";
				#print  PRE "cnm_tmp:$cnm_tmp\n";
				push  @{$cnm{$cnm_ipp}},$cnm_tmp;
				$fi++;
				last;
				}#end of for range loop
			
				
				}#end of if prev loop
				#print PRE "\ntemp cname str:$cnm_tmp\n";
			#$temp_name = "$cnm_pre[0]-$ip_range.$ptr. IN NS $domain.\n";
			#print PRE "temo name:$temp_name\n";
		}#end of if loop
=cut
		###################### end of removed section	
		
		
		
		
		if (($rec_type eq 'PTR') && ($status == 1)){
                print PRE "inside PTR loop\n";

		$value = Net::IP::ip_expand_address($value, 6);	
		my $rev6=new Net::IP ($value);
		my $v6=$rev6->reverse_ip();		
		$last_oct=substr($v6,0,43);		

              $tempo .= "$last_oct\tIN PTR\t$domain\n";
		}#end of if PTR
		print PRE "tmp PTR string: $tempo\n";
		

		open(TEMPL, FILE_PATH_ZONE_TEMPL) || die "can't open zone template file: $!";
        	while (my $l = <TEMPL>) { $base .= $l; }
        	close(TEMPL);

		foreach my $ny (sort keys %names) {
                               $ns .= "";
                        foreach my $v_name (@{$names{$ny}}){
                                $ns .= "$v_name\n";
                                print "names: $v_name\n";
				                                }
                        }
						
			##################### to be removed - range calculation
=begin comment
		   foreach my $cy (sort keys %cnm) {
                        $cname .= "";
                        foreach $c_name (@{$cnm{$cy}}){
                	      print  "CNM inside foreach\n";
                                $cname .= "$c_name\n";
                                }
                                #print PRE "\n Sort hash\ncname:$cname\n";
                        }
=cut 
			#################### end of comments 

		$base =~ s/\*SERIALNUMBER\*/$serial_number/;
        	$base =~ s/\*FILEBUILDDATE\*/$file_build_date/;
        	$base =~ s/\*HOSTNAME\*/$reverse_del_hostname/;
		$base =~ s/\*PTRREC\*/$ptr/g;
		$base =~ s/\*DATA\*/$tempo/g;
		$base =~ s/\*CNAME\*/$cname/g;
		$base =~ s/\*NS\*/$ns/g;
		 print "writing zone file...\n";
                print PRE "$ptr:before writing\n";

                open(ZONE, ">".FILE_PATH_ZONE."$ptr");
                print ZONE "$base\n";
                close(ZONE);
		$base = '';
		$ns = '';
		$cnm_tmp = '';
		%cnm = ();
		#$tempo = '';
		if ($i == $sel_ptr1){
		print PRE "inside i condn\n";
		$tempo = '';
		$cnm_tmp = '';
		$c_name = '';
  		$temp_name = '';
         	%cnm = ();
		#$ns = '';
		$base = '';
		}#end of if loop for i
		}#end of count req time condn	
		$prev_last_oct = $last_oct;
		$prev_status = $status;
		$prev_domain = $domain;
		}#end of for	
		$base = '';
		$ns = '';
		$cnm_tmp = '';
		%cnm = ();
		$tempo = '';
		$cname = '';
		$req_time_1 = '';
		$count_request = 0;

shift @ar_ptr;
}#end of distinct ptr


sub rev_range {
my ($rev_val) = @_;
print "inside sub :$rev_val\n";
print PRE "$rev_val: value in sub\n";
my $range = 0;
if ($rev_val ==48) {
                $range = 1208925819614629200000000;
        }elsif ($rev_val == 49) {
                 $range = 604462909807314600000000;
        }elsif ($rev_val == 50) {
                 $range = 302231454903657300000000;
        }elsif ($rev_val == 51 ){
                 $range = 151115727451828650000000;
        }elsif ($rev_val == 52 ){
                 $range = 75557863725914320000000;
        }elsif ($rev_val == 53 ){
                 $range = 37778931862957160000000;
        }elsif ($rev_val == 54 ){
                 $range = 18889465931478580000000;
        }elsif ($rev_val == 55 ){
                 $range = 9444732965739291000000;
        }elsif ($rev_val == 56 ){
                 $range = 4722366482869645500000;
        }
		elsif ($rev_val == 57 ){
                 $range = 2361183241434822700000;
        }
		elsif ($rev_val == 58 ){
                 $range = 1180591620717411300000;
        }
		elsif ($rev_val == 59 ){
                 $range = 590295810358705700000;
        }
		elsif ($rev_val == 60 ){

                $range = 295147905179352830000;
        }
		elsif ($rev_val == 61 ){
                 $range = 147573952589676410000;
        }
		elsif ($rev_val == 62 ){
                 $range = 73786976294838210000;
        }
		elsif ($rev_val == 63 ){
                 $range = 36893488147419104000;
        }
		elsif ($rev_val == 64 ){
                 $range = 18446744073709552000;
        }
#print "range :$range\n";
return $range;
}
