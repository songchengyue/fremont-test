/*
 * XML Type:  StrListConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.StrListConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML StrListConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class StrListConditionsImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.StrListConditions
{
    
    public StrListConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STRLISTCONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strListCondition");
    
    
    /**
     * Gets array of all "strListCondition" elements
     */
    public com.hp.schemas.ucmdb._1.types.StrListCondition[] getStrListConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(STRLISTCONDITION$0, targetList);
            com.hp.schemas.ucmdb._1.types.StrListCondition[] result = new com.hp.schemas.ucmdb._1.types.StrListCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "strListCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListCondition getStrListConditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListCondition)get_store().find_element_user(STRLISTCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "strListCondition" element
     */
    public int sizeOfStrListConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STRLISTCONDITION$0);
        }
    }
    
    /**
     * Sets array of all "strListCondition" element
     */
    public void setStrListConditionArray(com.hp.schemas.ucmdb._1.types.StrListCondition[] strListConditionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(strListConditionArray, STRLISTCONDITION$0);
        }
    }
    
    /**
     * Sets ith "strListCondition" element
     */
    public void setStrListConditionArray(int i, com.hp.schemas.ucmdb._1.types.StrListCondition strListCondition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListCondition)get_store().find_element_user(STRLISTCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(strListCondition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "strListCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListCondition insertNewStrListCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListCondition)get_store().insert_element_user(STRLISTCONDITION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "strListCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListCondition addNewStrListCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListCondition)get_store().add_element_user(STRLISTCONDITION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "strListCondition" element
     */
    public void removeStrListCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STRLISTCONDITION$0, i);
        }
    }
}
