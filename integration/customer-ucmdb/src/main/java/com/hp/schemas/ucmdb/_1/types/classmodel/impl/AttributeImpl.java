/*
 * XML Type:  Attribute
 * Namespace: http://schemas.hp.com/ucmdb/1/types/classmodel
 * Java type: com.hp.schemas.ucmdb._1.types.classmodel.Attribute
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.classmodel.impl;
/**
 * An XML Attribute(@http://schemas.hp.com/ucmdb/1/types/classmodel).
 *
 * This is a complex type.
 */
public class AttributeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.classmodel.Attribute
{
    
    public AttributeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "name");
    private static final javax.xml.namespace.QName ATTRTYPE$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "attrType");
    private static final javax.xml.namespace.QName ATTRTYPENAME$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "attrTypeName");
    private static final javax.xml.namespace.QName DISPLAYLABEL$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "displayLabel");
    private static final javax.xml.namespace.QName DESCRIPTION$8 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "description");
    private static final javax.xml.namespace.QName QUALIFIERS$10 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "qualifiers");
    private static final javax.xml.namespace.QName DEFAULTVALUE$12 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "defaultValue");
    
    
    /**
     * Gets the "name" element
     */
    public java.lang.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "name" element
     */
    public org.apache.xmlbeans.XmlString xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "name" element
     */
    public void setName(java.lang.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NAME$0);
            }
            target.setStringValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "name" element
     */
    public void xsetName(org.apache.xmlbeans.XmlString name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NAME$0);
            }
            target.set(name);
        }
    }
    
    /**
     * Gets the "attrType" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType.Enum getAttrType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ATTRTYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "attrType" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType xgetAttrType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType)get_store().find_element_user(ATTRTYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "attrType" element
     */
    public void setAttrType(com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType.Enum attrType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ATTRTYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ATTRTYPE$2);
            }
            target.setEnumValue(attrType);
        }
    }
    
    /**
     * Sets (as xml) the "attrType" element
     */
    public void xsetAttrType(com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType attrType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType)get_store().find_element_user(ATTRTYPE$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType)get_store().add_element_user(ATTRTYPE$2);
            }
            target.set(attrType);
        }
    }
    
    /**
     * Gets the "attrTypeName" element
     */
    public java.lang.String getAttrTypeName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ATTRTYPENAME$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "attrTypeName" element
     */
    public org.apache.xmlbeans.XmlString xgetAttrTypeName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ATTRTYPENAME$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "attrTypeName" element
     */
    public boolean isSetAttrTypeName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ATTRTYPENAME$4) != 0;
        }
    }
    
    /**
     * Sets the "attrTypeName" element
     */
    public void setAttrTypeName(java.lang.String attrTypeName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ATTRTYPENAME$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ATTRTYPENAME$4);
            }
            target.setStringValue(attrTypeName);
        }
    }
    
    /**
     * Sets (as xml) the "attrTypeName" element
     */
    public void xsetAttrTypeName(org.apache.xmlbeans.XmlString attrTypeName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ATTRTYPENAME$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ATTRTYPENAME$4);
            }
            target.set(attrTypeName);
        }
    }
    
    /**
     * Unsets the "attrTypeName" element
     */
    public void unsetAttrTypeName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ATTRTYPENAME$4, 0);
        }
    }
    
    /**
     * Gets the "displayLabel" element
     */
    public java.lang.String getDisplayLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DISPLAYLABEL$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "displayLabel" element
     */
    public org.apache.xmlbeans.XmlString xgetDisplayLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DISPLAYLABEL$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "displayLabel" element
     */
    public void setDisplayLabel(java.lang.String displayLabel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DISPLAYLABEL$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DISPLAYLABEL$6);
            }
            target.setStringValue(displayLabel);
        }
    }
    
    /**
     * Sets (as xml) the "displayLabel" element
     */
    public void xsetDisplayLabel(org.apache.xmlbeans.XmlString displayLabel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DISPLAYLABEL$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DISPLAYLABEL$6);
            }
            target.set(displayLabel);
        }
    }
    
    /**
     * Gets the "description" element
     */
    public java.lang.String getDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "description" element
     */
    public org.apache.xmlbeans.XmlString xgetDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPTION$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "description" element
     */
    public void setDescription(java.lang.String description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPTION$8);
            }
            target.setStringValue(description);
        }
    }
    
    /**
     * Sets (as xml) the "description" element
     */
    public void xsetDescription(org.apache.xmlbeans.XmlString description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPTION$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DESCRIPTION$8);
            }
            target.set(description);
        }
    }
    
    /**
     * Gets the "qualifiers" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers getQualifiers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers)get_store().find_element_user(QUALIFIERS$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "qualifiers" element
     */
    public void setQualifiers(com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers qualifiers)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers)get_store().find_element_user(QUALIFIERS$10, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers)get_store().add_element_user(QUALIFIERS$10);
            }
            target.set(qualifiers);
        }
    }
    
    /**
     * Appends and returns a new empty "qualifiers" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers addNewQualifiers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers)get_store().add_element_user(QUALIFIERS$10);
            return target;
        }
    }
    
    /**
     * Gets the "defaultValue" element
     */
    public java.lang.String getDefaultValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEFAULTVALUE$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "defaultValue" element
     */
    public org.apache.xmlbeans.XmlString xgetDefaultValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DEFAULTVALUE$12, 0);
            return target;
        }
    }
    
    /**
     * Sets the "defaultValue" element
     */
    public void setDefaultValue(java.lang.String defaultValue)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEFAULTVALUE$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DEFAULTVALUE$12);
            }
            target.setStringValue(defaultValue);
        }
    }
    
    /**
     * Sets (as xml) the "defaultValue" element
     */
    public void xsetDefaultValue(org.apache.xmlbeans.XmlString defaultValue)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DEFAULTVALUE$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DEFAULTVALUE$12);
            }
            target.set(defaultValue);
        }
    }
    /**
     * An XML attrType(@http://schemas.hp.com/ucmdb/1/types/classmodel).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.classmodel.Attribute$AttrType.
     */
    public static class AttrTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType
    {
        
        public AttrTypeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected AttrTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
