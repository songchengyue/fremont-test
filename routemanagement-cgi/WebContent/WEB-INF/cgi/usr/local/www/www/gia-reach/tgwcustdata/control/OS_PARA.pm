#!/usr/bin/perl
#
# Module : OS_PARA.pm
# Desc   : Centralised module for OLSS common parameters
# Last Update : 20030310 by Leo
# Changes:-
#

package OS_PARA;
use Exporter;

use strict;
use warnings;

# Global variables
our @ISA        = qw(Exporter);
#our @EXPORT     = qw(@cms_atm_sids $atm_sid_cnt @atm_sids);
our %values;

# Local variables
my $conffile = "./usr/local/www/www/gia-reach/tgwcustdata/control/modules/OS_PARA.conf";
### ....original
#my $conffile = "D:/OLSS/Perl loggers/RouteRegistryUIcode/WebContent/WEB-INF/cgi/usr/local/www/www/gia-reach/tgwcustdata/control/modules/OS_PARA.conf";

#print "Start!\n";
get_values (\%values);

########################### OUTPUT CODE ##############################

sub get_values {
	my $values = shift @_;
	my ($line, $name, $value);
	open (IN, "$conffile") || die "Parameter file $conffile not found!\n";
		while ($line =<IN>) {
			chomp($line);
			if (($line eq '') || ($line =~ /^#/)) { next; }
			($name, $value) = split (/=/, $line);
			$$values{$name}{value} = $value;
		}
	close (IN);
}

1;
