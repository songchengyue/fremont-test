package com.hp.ucmdb.generated;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator.Enum;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeDocument;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponseDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponseDocument;
import com.hp.schemas.ucmdb._1.types.props.CustomProperties;
import com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties;
import com.hp.schemas.ucmdb._1.types.props.PropertiesList;
import com.hp.schemas.ucmdb._1.types.props.TypedProperties;
import com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection;
import com.hp.schemas.ucmdb._1.types.*;
import com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator;

public class UcmdbWolResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(UcmdbNetopsSynch.class);
	static  UcmdbServiceStub stub =null;
	static int numberOfChunks;
	static int chunkNumber;

	static String key1;
	static String key2;

	static String className4 ="wol_resource";
	static String propertyName3 = "resource_id";
	static String propertyName4 = "resource_type";
	static String propertyName5 = "resource_name";
	static String propertyName6 = "a_site_id";
	static String propertyName7 = "equip_type";
	static String propertyName8 = "parent_resource_id";
	
	static String conditionName4 = "resource_id";

	
	static String callerApplication = "netops";
	static String cndtnLgclOprtrOr = "OR";
	static String strOprtr = "EqualIgnoreCase";


	static List<String> cmdbIds = new ArrayList<String>();
	static List<String> resourceIds = new ArrayList<String>();
	static List<String> resourceIdsTemp ;
	static List<String> resourceIdsNew = new ArrayList<String>();
	static List<String> siteIds = new ArrayList<String>();
	static List<String> resourceNames = new ArrayList<String>();
	static List<String> resourceTypes = new ArrayList<String>();
	static List<String> equipTypes = new ArrayList<String>();
	static List<String> parentResourceIds = new ArrayList<String>();
	static List<Timestamp> fullLastUpdatedDate = new ArrayList<Timestamp>();
	static int resourceIdSize;
	static int resourceIdTempSize;
	static String cmdbId = "";
	static String resourceId = "";
	static String resourceType = "";
	static String resourceName = "";
	static String siteId = "";
	static String equipType = "";
	static String parentResourceId = "";

	static String getName;
	static String getValue;
		
	static Properties props = new Properties();
	static Connection dbConnection;
	static Statement statement;
	static String configPath;
	
	public static void getSiteIds(UcmdbServiceStub ucmdbStub,String mainConfigPath) {
		stub = ucmdbStub;
		configPath = mainConfigPath;
		
		try {
			loadParams(configPath);  
			dbConnection = getDBConnection();
			//Deleting previous entries from CDM_WOL_RSRC_1_STG
			Statement delstmt = dbConnection.createStatement();
			delstmt.executeQuery("delete from CDM_WOL_RSRC_1_STG");
			LOGGER.info("deleted");
			
			statement = dbConnection.createStatement();

			String selectSQL = "SELECT distinct(A_RESOURCE_ID) FROM RFS_TO_RSRC_RLTN_1_STG " ;

			try {
				dbConnection = getDBConnection();
				statement = dbConnection.createStatement();
				//////LOGGER.info(selectSQL);
				ResultSet rSet = statement.executeQuery(selectSQL);
				while(rSet.next())
				{
					//////LOGGER.info("within select loop");
					resourceId = rSet.getString("A_RESOURCE_ID");
					resourceIds.add(resourceId);
					//////LOGGER.info("resourceId : " +resourceId);
				}

				resourceIdSize = resourceIds.size();
				LOGGER.info("resourceIdSize : " + resourceIdSize);
				
		//Dividing into batches of 50
		//for(int i=0; i< resourceIdSize;i++)
		int i=0;
		while(i<resourceIdSize)
		{
			int count = 0;
			resourceIdsTemp = new ArrayList<String>();
					while((count<50) && (i<resourceIdSize))
					{
						//////LOGGER.info("i : " +i);
						resourceIdsTemp.add(resourceIds.get(i));
						i++;
						count++;
						//////LOGGER.info("i : " +i);
						//////LOGGER.info("count : " +count);
					}
					resourceIdTempSize = resourceIdsTemp.size();
					LOGGER.info("resourceIdTempSize : " +resourceIdTempSize);
					getFilteredCIsByTypeDemo();
					insertIntoTable();
					//Initializing the Arrays to avoid duplicates
					cmdbIds.clear();
					resourceIdsNew.clear();
					resourceNames.clear();
					resourceTypes.clear();
					equipTypes.clear();
					parentResourceIds.clear();
					siteIds.clear();
					fullLastUpdatedDate.clear();
					//LOGGER.info("siteIdsSizeAfter : " +siteIds.size());
		}
		
		} catch (Exception e) {
				LOGGER.error("exception inside getResourceIdList Division", e);
			}
			
		} catch (Exception e) {
			LOGGER.error("exception inside getResourceIdList", e);
		}
		
	}
	
	/* getFilteredCIsByType Operation*/
	public static void getFilteredCIsByTypeDemo() {	
		try {	
			LOGGER.info("inside getFilteredCIsByTypeDemo method");
			LOGGER.info("resourceIdTempSize : " + resourceIdTempSize);
			
			Enum conditionsLogicalOperator = Enum.forString(cndtnLgclOprtrOr);

			StrCondition[] strConditionArray = new StrCondition[resourceIdTempSize];
			for(int j=0; j<resourceIdTempSize;j++)
			{
				StrCondition strCondition =  StrCondition.Factory.newInstance();
				StrProp strProp = StrProp.Factory.newInstance();
				strProp.setName(conditionName4);
				strProp.setValue(resourceIdsTemp.get(j));
				strCondition.setCondition(strProp);
				strCondition.setStrOperator(StrOperator.Enum.forString(strOprtr));
				strConditionArray[j]= strCondition;
				
			}
	
			Conditions conditions = Conditions.Factory.newInstance();
			StrConditions strConditions= StrConditions.Factory.newInstance();
			
			
			strConditions.setStrConditionArray(strConditionArray);
			conditions.setStrConditions(strConditions);
			
			CmdbContext cntxt = CmdbContext.Factory.newInstance();
			cntxt.setCallerApplication(callerApplication);
			
			GetFilteredCIsByType getFilteredCIsByType = GetFilteredCIsByType.Factory.newInstance();
			getFilteredCIsByType.setCmdbContext(cntxt);
			getFilteredCIsByType.setType(className4);
			getFilteredCIsByType.setConditions(conditions);
			getFilteredCIsByType.setConditionsLogicalOperator(conditionsLogicalOperator);
			

			CustomProperties properties = CustomProperties.Factory.newInstance();
			PropertiesList propertiesList= PropertiesList.Factory.newInstance();
			propertiesList.addPropertyName(propertyName3);
			propertiesList.addPropertyName(propertyName4);
			propertiesList.addPropertyName(propertyName5);
			propertiesList.addPropertyName(propertyName6);
			propertiesList.addPropertyName(propertyName7);
			propertiesList.addPropertyName(propertyName8);
			properties.setPropertiesList(propertiesList);
			getFilteredCIsByType.setProperties(properties);

			GetFilteredCIsByTypeDocument request = GetFilteredCIsByTypeDocument.Factory.newInstance();
			request.setGetFilteredCIsByType(getFilteredCIsByType);
			
			////LOGGER.info("getFilteredCIsByType request : " + request);					
			//////LOGGER.info("set inputs");
			
			HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
			basicAuthentication.setUsername("admin");
			basicAuthentication.setPassword("admin");
			int so_timeout = 900000;
			int con_timeout = 110000;
			stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
			//////LOGGER.info("authentication passed");

			GetFilteredCIsByTypeResponseDocument response = stub.getFilteredCIsByType(request);
			////LOGGER.info("getFilteredCIsByType response : " + response);
						
			ChunkInfo chunkInfo = response.getGetFilteredCIsByTypeResponse().getChunkInfo();
			numberOfChunks = chunkInfo.getNumberOfChunks();
			ChunkKey chunkKey = chunkInfo.getChunksKey();
			key1 = chunkKey.getKey1();
			key2 = chunkKey.getKey2();
			
			//LOGGER.info("numberOfChunks : " + numberOfChunks);

			if(numberOfChunks==0)
			{
			CI[] CI = response.getGetFilteredCIsByTypeResponse().getCIs().getCIArray();
			//////LOGGER.info("Within zero chunks :CI.length : " +CI.length);
			java.util.Date date= new java.util.Date();
			for(int j=0 ; j < CI.length ; j++)
			{
				cmdbId = CI[j].getID().getStringValue();
				cmdbIds.add(cmdbId);
				fullLastUpdatedDate.add(new Timestamp(date.getTime()));
				StrProp[] strprop = CI[j].getProps().getStrProps().getStrPropArray();
				////LOGGER.info("strprop.length : " + strprop.length);
				//LOGGER.info("cmdbId : " +cmdbId);
				if(strprop.length==6)
				{
					for (int p = 0; p < strprop.length; p++)
					{
					getName = strprop[p].getName();
					getValue = strprop[p].getValue();
						if(strprop[p].getName().equalsIgnoreCase("resource_id")){
							resourceId = strprop[p].getValue();
							resourceIdsNew.add(resourceId);
							//LOGGER.info("resourceId : " + resourceId);
						}
						if(strprop[p].getName().equalsIgnoreCase("resource_type")){
							resourceType = strprop[p].getValue();
							resourceTypes.add(resourceType);
							//LOGGER.info("resourceType : " + resourceType);
						}
						if(strprop[p].getName().equalsIgnoreCase("resource_name")){
							resourceName = strprop[p].getValue();
							resourceNames.add(resourceName);
							//LOGGER.info("resourceName : " + resourceName);
						}
						if(strprop[p].getName().equalsIgnoreCase("a_site_id")){
							siteId = strprop[p].getValue();
							siteIds.add(siteId);
							//LOGGER.info("siteId : " + siteId);
						}
						if(strprop[p].getName().equalsIgnoreCase("equip_type")){
							equipType = strprop[p].getValue();
							equipTypes.add(equipType);
							//LOGGER.info("equipType : " + equipType);
						}
						if(strprop[p].getName().equalsIgnoreCase("parent_resource_id")){
							parentResourceId = strprop[p].getValue();
							parentResourceIds.add(parentResourceId);
							//LOGGER.info("parentResourceId : " + parentResourceId);
						}
					}
				}
				else if(strprop.length==5)
				{
					for (int p = 0; p < strprop.length; p++)
					{
					getName = strprop[p].getName();
					getValue = strprop[p].getValue();
						if(strprop[p].getName().equalsIgnoreCase("resource_id")){
							resourceId = strprop[p].getValue();
							resourceIdsNew.add(resourceId);
							//LOGGER.info("resourceId : " + resourceId);
						}
						if(strprop[p].getName().equalsIgnoreCase("resource_type")){
							resourceType = strprop[p].getValue();
							resourceTypes.add(resourceType);
							//LOGGER.info("resourceType : " + resourceType);
						}
						if(strprop[p].getName().equalsIgnoreCase("resource_name")){
							resourceName = strprop[p].getValue();
							resourceNames.add(resourceName);
							//LOGGER.info("resourceName : " + resourceName);
						}
						if(strprop[p].getName().equalsIgnoreCase("a_site_id")){
							siteId = strprop[p].getValue();
							siteIds.add(siteId);
							//LOGGER.info("siteId : " + siteId);
						}
						if(strprop[p].getName().equalsIgnoreCase("parent_resource_id")){
							parentResourceId = strprop[p].getValue();
							parentResourceIds.add(parentResourceId);
							//LOGGER.info("parentResourceId : " + parentResourceId);
						}
					}
					equipTypes.add("");
				}
					else if(strprop.length==4)
					{
						for (int p = 0; p < strprop.length; p++)
						{
						getName = strprop[p].getName();
						getValue = strprop[p].getValue();
							if(strprop[p].getName().equalsIgnoreCase("resource_id")){
								resourceId = strprop[p].getValue();
								resourceIdsNew.add(resourceId);
								//LOGGER.info("resourceId : " + resourceId);
							}
							if(strprop[p].getName().equalsIgnoreCase("resource_type")){
								resourceType = strprop[p].getValue();
								resourceTypes.add(resourceType);
								//LOGGER.info("resourceType : " + resourceType);
							}
							if(strprop[p].getName().equalsIgnoreCase("resource_name")){
								resourceName = strprop[p].getValue();
								resourceNames.add(resourceName);
								//LOGGER.info("resourceName : " + resourceName);
							}
							if(strprop[p].getName().equalsIgnoreCase("a_site_id")){
								siteId = strprop[p].getValue();
								siteIds.add(siteId);
								parentResourceIds.add("");
								//LOGGER.info("siteId : " + siteId);
							}else if(strprop[p].getName().equalsIgnoreCase("parent_resource_id")){
								parentResourceId = strprop[p].getValue();
								parentResourceIds.add(parentResourceId);
								siteIds.add("");
								//LOGGER.info("parentResourceId : " + parentResourceId);
							}
						}
						equipTypes.add("");
					}
					else if(strprop.length==3)
					{
						for (int p = 0; p < strprop.length; p++)
						{
						getName = strprop[p].getName();
						getValue = strprop[p].getValue();
						//LOGGER.info("inside 3");
						//LOGGER.info(strprop[p].getName());
						if(strprop[p].getName().equalsIgnoreCase("resource_id")){
							resourceId = strprop[p].getValue();
							resourceIdsNew.add(resourceId);
							//LOGGER.info("resourceId : " + resourceId);
						}
						if(strprop[p].getName().equalsIgnoreCase("resource_type")){
							resourceType = strprop[p].getValue();
							resourceTypes.add(resourceType);
							//LOGGER.info("resourceType : " + resourceType);
						}
						if(strprop[p].getName().equalsIgnoreCase("resource_name")){
							resourceName = strprop[p].getValue();
							resourceNames.add(resourceName);
							parentResourceIds.add("");
							//LOGGER.info("resourceName : " + resourceName);
						}else if(strprop[p].getName().equalsIgnoreCase("parent_resource_id")){
							parentResourceId = strprop[p].getValue();
							parentResourceIds.add(parentResourceId);
							resourceNames.add("");
							//LOGGER.info("parentResourceId : " + parentResourceId);
						}
					}
					siteIds.add("");
					equipTypes.add("");
					}
					else if(strprop.length==2)
					{
							for (int p = 0; p < strprop.length; p++)
							{
							getName = strprop[p].getName();
							getValue = strprop[p].getValue();
							//LOGGER.info("inside 2");
							//LOGGER.info(strprop[p].getName());
							if(strprop[p].getName().equalsIgnoreCase("resource_id")){
								resourceId = strprop[p].getValue();
								resourceIdsNew.add(resourceId);
								//LOGGER.info("resourceId : " + resourceId);
							}
							if(strprop[p].getName().equalsIgnoreCase("resource_type")){
								resourceType = strprop[p].getValue();
								resourceTypes.add(resourceType);
								//LOGGER.info("resourceType : " + resourceType);
							}
						}
						resourceNames.add("");
						siteIds.add("");
						equipTypes.add("");
						parentResourceIds.add("");
					}
				
				
			}
			}
			else
			{
				pullTopologyMapChunksFull();
			}
			
		
		}catch (RemoteException rme) {
			LOGGER.error("RemoteException inside getFilteredCIsByType", rme);
		} catch (UcmdbFault ufe) {
			LOGGER.error("UcmdbFaultException inside getFilteredCIsByType", ufe);
		} catch (Exception e) {
			 LOGGER.error("exception inside getFilteredCIsByTypeDemo", e);
			 		}

	}

	 
	// pullTopologyMapChunksFull Operation
			public static void pullTopologyMapChunksFull() {
				LOGGER.info("inside pullTopologyMapChunksFull method");

				try {
					
					////LOGGER.info("numberOfChunks : " + numberOfChunks);
					//////LOGGER.info("key1 : " + key1);
					//////LOGGER.info("key2 : " + key2);
					for(int i=1 ; i <= numberOfChunks ; i++)
					{
						chunkNumber = i;
						//////LOGGER.info("chunkNumber:" +chunkNumber);
						
						CmdbContext cntxt = CmdbContext.Factory.newInstance();
						cntxt.setCallerApplication(callerApplication);
						ChunkRequest chunkRequest = ChunkRequest.Factory.newInstance();
						ChunkInfo chunkInfo = ChunkInfo.Factory.newInstance();
						
						chunkRequest.setChunkNumber(chunkNumber);
						chunkInfo.setNumberOfChunks(numberOfChunks);

						ChunkKey chunksKey = ChunkKey.Factory.newInstance();
						chunksKey.setKey1(key1);
						chunksKey.setKey2(key2);
						
						chunkInfo.setChunksKey(chunksKey);
						chunkRequest.setChunkInfo(chunkInfo );
						
						
						TypedPropertiesCollection queryTypedProperties = TypedPropertiesCollection.Factory.newInstance();
						TypedProperties typedProperties = TypedProperties.Factory.newInstance();
						typedProperties.setType(className4);
						CustomTypedProperties properties = CustomTypedProperties.Factory.newInstance();
						PropertiesList propertiesList = PropertiesList.Factory.newInstance();
						String[] propertyNameArray = {propertyName3,propertyName4,propertyName5,propertyName6,propertyName7,propertyName8};
						propertiesList.setPropertyNameArray(propertyNameArray );

						properties.setPropertiesList(propertiesList);
						typedProperties.setProperties(properties);
						
						TypedProperties[] typedPropertiesArray ={typedProperties};
						queryTypedProperties.setTypedPropertiesArray(typedPropertiesArray );
						
						PullTopologyMapChunks pullTopologyMapChunks = PullTopologyMapChunks.Factory.newInstance();
						
						pullTopologyMapChunks.setCmdbContext(cntxt);
						pullTopologyMapChunks.setChunkRequest(chunkRequest);
						pullTopologyMapChunks.setQueryTypedProperties(queryTypedProperties);
						PullTopologyMapChunksDocument request = PullTopologyMapChunksDocument.Factory.newInstance();
						request.setPullTopologyMapChunks(pullTopologyMapChunks);
						//////LOGGER.info("pullTopologyMapChunks request" +request);					
						
						HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
						basicAuthentication.setUsername("admin");
						basicAuthentication.setPassword("admin");
						int so_timeout = 900000;
						int con_timeout = 110000;
						stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
						stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
						stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
						stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
						//////LOGGER.info("authentication passed");

						PullTopologyMapChunksResponseDocument response = stub.pullTopologyMapChunks(request);
						//////LOGGER.info("pullTopologyMapChunks response : " + response);
						
						
						CINode[] ciNode = response.getPullTopologyMapChunksResponse().getTopologyMap().getCINodes().getCINodeArray();
						CI[] ci = ciNode[0].getCIs().getCIArray();
						//////LOGGER.info("ci : " + ci);
						//////LOGGER.info(ci.length);
						java.util.Date date= new java.util.Date();
						for(int j=0 ; j < ci.length ; j++)
						{						
							cmdbId = ci[j].getID().getStringValue();							
							cmdbIds.add(cmdbId);
							fullLastUpdatedDate.add(new Timestamp(date.getTime()));
								
							StrProp[] strprop = ci[j].getProps().getStrProps().getStrPropArray();
							////LOGGER.info("strprop.length : " + strprop.length);
							//LOGGER.info("cmdbId : " +cmdbId);
							if(strprop.length==6)
							{
								for (int p = 0; p < strprop.length; p++)
								{
								getName = strprop[p].getName();
								getValue = strprop[p].getValue();
									if(strprop[p].getName().equalsIgnoreCase("resource_id")){
										resourceId = strprop[p].getValue();
										resourceIdsNew.add(resourceId);
										//LOGGER.info("resourceId : " + resourceId);
									}
									if(strprop[p].getName().equalsIgnoreCase("resource_type")){
										resourceType = strprop[p].getValue();
										resourceTypes.add(resourceType);
										//LOGGER.info("resourceType : " + resourceType);
									}
									if(strprop[p].getName().equalsIgnoreCase("resource_name")){
										resourceName = strprop[p].getValue();
										resourceNames.add(resourceName);
										//LOGGER.info("resourceName : " + resourceName);
									}
									if(strprop[p].getName().equalsIgnoreCase("a_site_id")){
										siteId = strprop[p].getValue();
										siteIds.add(siteId);
										//LOGGER.info("siteId : " + siteId);
									}
									if(strprop[p].getName().equalsIgnoreCase("equip_type")){
										equipType = strprop[p].getValue();
										equipTypes.add(equipType);
										//LOGGER.info("equipType : " + equipType);
									}
									if(strprop[p].getName().equalsIgnoreCase("parent_resource_id")){
										parentResourceId = strprop[p].getValue();
										parentResourceIds.add(parentResourceId);
										//LOGGER.info("parentResourceId : " + parentResourceId);
									}
								}
							}
							else if(strprop.length==5)
							{
								for (int p = 0; p < strprop.length; p++)
								{
								getName = strprop[p].getName();
								getValue = strprop[p].getValue();
									if(strprop[p].getName().equalsIgnoreCase("resource_id")){
										resourceId = strprop[p].getValue();
										resourceIdsNew.add(resourceId);
										//LOGGER.info("resourceId : " + resourceId);
									}
									if(strprop[p].getName().equalsIgnoreCase("resource_type")){
										resourceType = strprop[p].getValue();
										resourceTypes.add(resourceType);
										//LOGGER.info("resourceType : " + resourceType);
									}
									if(strprop[p].getName().equalsIgnoreCase("resource_name")){
										resourceName = strprop[p].getValue();
										resourceNames.add(resourceName);
										//LOGGER.info("resourceName : " + resourceName);
									}
									if(strprop[p].getName().equalsIgnoreCase("a_site_id")){
										siteId = strprop[p].getValue();
										siteIds.add(siteId);
										//LOGGER.info("siteId : " + siteId);
									}
									if(strprop[p].getName().equalsIgnoreCase("parent_resource_id")){
										parentResourceId = strprop[p].getValue();
										parentResourceIds.add(parentResourceId);
										//LOGGER.info("parentResourceId : " + parentResourceId);
									}
								}
								equipTypes.add("");
							}
								else if(strprop.length==4)
								{
									for (int p = 0; p < strprop.length; p++)
									{
									getName = strprop[p].getName();
									getValue = strprop[p].getValue();
										if(strprop[p].getName().equalsIgnoreCase("resource_id")){
											resourceId = strprop[p].getValue();
											resourceIdsNew.add(resourceId);
											//LOGGER.info("resourceId : " + resourceId);
										}
										if(strprop[p].getName().equalsIgnoreCase("resource_type")){
											resourceType = strprop[p].getValue();
											resourceTypes.add(resourceType);
											//LOGGER.info("resourceType : " + resourceType);
										}
										if(strprop[p].getName().equalsIgnoreCase("resource_name")){
											resourceName = strprop[p].getValue();
											resourceNames.add(resourceName);
											//LOGGER.info("resourceName : " + resourceName);
										}
										if(strprop[p].getName().equalsIgnoreCase("a_site_id")){
											siteId = strprop[p].getValue();
											siteIds.add(siteId);
											parentResourceIds.add("");
											//LOGGER.info("siteId : " + siteId);
										}else if(strprop[p].getName().equalsIgnoreCase("parent_resource_id")){
											parentResourceId = strprop[p].getValue();
											parentResourceIds.add(parentResourceId);
											siteIds.add("");
											//LOGGER.info("parentResourceId : " + parentResourceId);
										}
									}
									equipTypes.add("");
								}
								else if(strprop.length==3)
								{
									for (int p = 0; p < strprop.length; p++)
									{
									getName = strprop[p].getName();
									getValue = strprop[p].getValue();
									//LOGGER.info("inside 3");
									//LOGGER.info(strprop[p].getName());
									if(strprop[p].getName().equalsIgnoreCase("resource_id")){
										resourceId = strprop[p].getValue();
										resourceIdsNew.add(resourceId);
										//LOGGER.info("resourceId : " + resourceId);
									}
									if(strprop[p].getName().equalsIgnoreCase("resource_type")){
										resourceType = strprop[p].getValue();
										resourceTypes.add(resourceType);
										//LOGGER.info("resourceType : " + resourceType);
									}
									if(strprop[p].getName().equalsIgnoreCase("resource_name")){
										resourceName = strprop[p].getValue();
										resourceNames.add(resourceName);
										parentResourceIds.add("");
										//LOGGER.info("resourceName : " + resourceName);
									}else if(strprop[p].getName().equalsIgnoreCase("parent_resource_id")){
										parentResourceId = strprop[p].getValue();
										parentResourceIds.add(parentResourceId);
										resourceNames.add("");
										//LOGGER.info("parentResourceId : " + parentResourceId);
									}
								}
								siteIds.add("");
								equipTypes.add("");
								}
								else if(strprop.length==2)
								{
										for (int p = 0; p < strprop.length; p++)
										{
										getName = strprop[p].getName();
										getValue = strprop[p].getValue();
										//LOGGER.info("inside 2");
										//LOGGER.info(strprop[p].getName());
										if(strprop[p].getName().equalsIgnoreCase("resource_id")){
											resourceId = strprop[p].getValue();
											resourceIdsNew.add(resourceId);
											//LOGGER.info("resourceId : " + resourceId);
										}
										if(strprop[p].getName().equalsIgnoreCase("resource_type")){
											resourceType = strprop[p].getValue();
											resourceTypes.add(resourceType);
											//LOGGER.info("resourceType : " + resourceType);
										}
									}
									resourceNames.add("");
									siteIds.add("");
									equipTypes.add("");
									parentResourceIds.add("");
								}
							
						}


					}
				//////LOGGER.info("OUTSIDE CHUNK LOOP");
				////LOGGER.info(siteIds.size());
				////LOGGER.info(cmdbIds.size());
				////LOGGER.info("SUCCESS FROM PULL CHUNKS");
				//insertIntoTable();
				}
				catch (RemoteException rme) {
					LOGGER.error("RemoteException inside pullTopologyMapChunks", rme);
				} catch (UcmdbFault ufe) {
					LOGGER.error("UcmdbFaultException inside pullTopologyMapChunks", ufe);
				}
				 catch (Exception e) {
					 LOGGER.error("Exception inside pullTopologyMapChunks", e);
				}

			}
			
			private static void insertIntoTable() {
				//Inserting Values into CDM_WOL_RSRC_1_STG TABLE
				try {
					LOGGER.info("Inserting");
					LOGGER.info("siteIdsSize : " +siteIds.size());
					LOGGER.info("resourceIdsNewSize : " +resourceIdsNew.size());
					LOGGER.info("cmdbIdsSize : " +cmdbIds.size());
					LOGGER.info("resourceNamesSize : " +resourceNames.size());
					LOGGER.info("resourceTypesSize : " +resourceTypes.size());
					LOGGER.info("equipTypesSize : " +equipTypes.size());
					LOGGER.info("parentResourceIdsSize : " +parentResourceIds.size());
					LOGGER.info("cmdbIds : "+cmdbIds);
					LOGGER.info("resourceIdsNew : " +resourceIdsNew);
					LOGGER.info("resourceNames : " +resourceNames);
					LOGGER.info("resourceTypes : " +resourceTypes);
					LOGGER.info("siteIds : "+siteIds);
					LOGGER.info("equipTypes : " +equipTypes);
					LOGGER.info("parentResourceIds : " +parentResourceIds);
					
					dbConnection = getDBConnection();
					String stringSQL = "INSERT INTO CDM_WOL_RSRC_1_STG (CMDB_ID,A_RESOURCE_ID,A_RESOURCE_NAME,A_RESOURCE_TYPE,A_EQUIP_TYPE,A_A_SITE_ID,A_PARENT_RESOURCE_ID,TIMESTAMP) VALUES (?,?,?,?,?,?,?,?)" ;
					PreparedStatement ps = dbConnection.prepareStatement(stringSQL);
					
					for (int k = 0; k < siteIds.size(); k++){
						ps.setString(1, cmdbIds.get(k));
						ps.setString(2, resourceIdsNew.get(k));
						ps.setString(3, resourceNames.get(k));
						ps.setString(4, resourceTypes.get(k));
						ps.setString(5, equipTypes.get(k));
						ps.setString(6, siteIds.get(k));
						ps.setString(7, parentResourceIds.get(k));
						ps.setTimestamp(8, fullLastUpdatedDate.get(k));									
						ps.addBatch();
					}		
					LOGGER.info("completed batch execution");
					ps.executeBatch();
					ps.close();
					dbConnection.close();
					
					LOGGER.info("connection closed");
				} 
				catch (SQLException sqle) {
					LOGGER.info(sqle.getMessage());
				}
				catch (Exception e) {
				}	
				
			}
	
	public static void loadParams(String configPath) {
	    InputStream is = null;
	 
	    try {
	    	//LOGGER.info("configPath : " +configPath);
	        File f = new File (configPath);
	    	//File f = new File("D:\\WorkSpace5\\testUcmdb5\\src\\config.txt");
	    	//File f = new File("E:\\Ahana\\Workspace5\\Ucmdb5\\src\\config.txt");
	        //File f = new File("E:\\Ashwini\\workspaceAhana\\ucmdbSrv\\src\\config.txt");
	        is = new FileInputStream( f );
	    }
	    catch ( Exception e ) { is = null; }
	 
	    try {
	        props.load( is );
	    }
	    catch ( Exception e ) {
	    	LOGGER.error("exception inside loadParams", e);
	    }

	   
	}

	private static Connection getDBConnection() {
		//Using config file to remove hardcoding
		String DB_DRIVER = props.getProperty("DB_DRIVER");
		String DB_CONNECTION = props.getProperty("DB_CONNECTION");
		String DB_USER = props.getProperty("DB_USER");
		String DB_PASSWORD = props.getProperty("DB_PASSWORD");
		try {			
			Class.forName(DB_DRIVER);
			
		} catch (ClassNotFoundException e) {
			////LOGGER.info(e.getMessage());
		}		
		
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
			return dbConnection;		
		
		} catch (SQLException e) {
			////LOGGER.info(e.getMessage());
		}
		return dbConnection;
	}
	
}
