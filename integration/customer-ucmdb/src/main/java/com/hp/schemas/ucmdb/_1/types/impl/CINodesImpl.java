/*
 * XML Type:  CINodes
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.CINodes
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML CINodes(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class CINodesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.CINodes
{
    
    public CINodesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CINODE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "CINode");
    
    
    /**
     * Gets array of all "CINode" elements
     */
    public com.hp.schemas.ucmdb._1.types.CINode[] getCINodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CINODE$0, targetList);
            com.hp.schemas.ucmdb._1.types.CINode[] result = new com.hp.schemas.ucmdb._1.types.CINode[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "CINode" element
     */
    public com.hp.schemas.ucmdb._1.types.CINode getCINodeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CINode target = null;
            target = (com.hp.schemas.ucmdb._1.types.CINode)get_store().find_element_user(CINODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "CINode" element
     */
    public int sizeOfCINodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CINODE$0);
        }
    }
    
    /**
     * Sets array of all "CINode" element
     */
    public void setCINodeArray(com.hp.schemas.ucmdb._1.types.CINode[] ciNodeArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(ciNodeArray, CINODE$0);
        }
    }
    
    /**
     * Sets ith "CINode" element
     */
    public void setCINodeArray(int i, com.hp.schemas.ucmdb._1.types.CINode ciNode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CINode target = null;
            target = (com.hp.schemas.ucmdb._1.types.CINode)get_store().find_element_user(CINODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(ciNode);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "CINode" element
     */
    public com.hp.schemas.ucmdb._1.types.CINode insertNewCINode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CINode target = null;
            target = (com.hp.schemas.ucmdb._1.types.CINode)get_store().insert_element_user(CINODE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "CINode" element
     */
    public com.hp.schemas.ucmdb._1.types.CINode addNewCINode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CINode target = null;
            target = (com.hp.schemas.ucmdb._1.types.CINode)get_store().add_element_user(CINODE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "CINode" element
     */
    public void removeCINode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CINODE$0, i);
        }
    }
}
