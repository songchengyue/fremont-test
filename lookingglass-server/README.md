# Telstra NetOps LookingGlass Server

## Synopsis

This project is an NetOps LookingGlass Server.

## Prerequisites

* Git
* Java 8 JDK
* Maven
* curl

## Run

The OAuth2 Server uses the "OAuth 2.0 SAML Bearer Assertion Flow". Only SAML v1.1 is supported as that
is the SAML version used by IDAM. We can use "curl" to simulate the OAuth2 client to test token issuing and
token checking.

* Get the code

```
git clone https://<your_username>@bitbucket.org/telstradevelopment/telstra-netops.git
```

* Build and run

```
cd telstra-netops\lookingglass-server
mvn package
# activate the correct spring profiles for your environment by adding "-Dspring.profiles.active=<your_profiles>"
# to the tomcat java startup - usually you can do this by setting JAVA_OPTS=Dspring.profiles.active=<your_profiles>
# before starting tomcat
cp target\lookingglass-server-0.0.1-SNAPSHOT.war <tomcat_webapps_folder>/lookingglass-server.war
# now restart tomcat
```

* Test

To test you need to first get a token from the OAuth2 server. Then issue a request:

```
curl -H "Authorization: Bearer <TOKEN>" http://localhost:8080/lookingglass-server/nodes?ipv=IPv4
```

