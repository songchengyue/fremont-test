/*
 * XML Type:  StrProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.StrProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML StrProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class StrPropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.StrProps
{
    
    public StrPropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STRPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strProp");
    
    
    /**
     * Gets array of all "strProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.StrProp[] getStrPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(STRPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.StrProp[] result = new com.hp.schemas.ucmdb._1.types.StrProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "strProp" element
     */
    public com.hp.schemas.ucmdb._1.types.StrProp getStrPropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().find_element_user(STRPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "strProp" element
     */
    public int sizeOfStrPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STRPROP$0);
        }
    }
    
    /**
     * Sets array of all "strProp" element
     */
    public void setStrPropArray(com.hp.schemas.ucmdb._1.types.StrProp[] strPropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(strPropArray, STRPROP$0);
        }
    }
    
    /**
     * Sets ith "strProp" element
     */
    public void setStrPropArray(int i, com.hp.schemas.ucmdb._1.types.StrProp strProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().find_element_user(STRPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(strProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "strProp" element
     */
    public com.hp.schemas.ucmdb._1.types.StrProp insertNewStrProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().insert_element_user(STRPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "strProp" element
     */
    public com.hp.schemas.ucmdb._1.types.StrProp addNewStrProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().add_element_user(STRPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "strProp" element
     */
    public void removeStrProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STRPROP$0, i);
        }
    }
}
