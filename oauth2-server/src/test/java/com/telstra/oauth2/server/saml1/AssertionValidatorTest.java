package com.telstra.oauth2.server.saml1;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.saml1.core.Assertion;
import org.opensaml.saml1.core.Conditions;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.validation.ValidationException;

public class AssertionValidatorTest {
	
	private static final String ISSUER = "myIssuer";
	private AssertionValidator validator;
	private SAMLObjectBuilder<SAMLObject> assertionBuilder;
	private XMLObjectBuilderFactory builderFactory;
	private SAMLObjectBuilder<SAMLObject> conditionsBuilder;
	

	@Before
	public void before() throws ConfigurationException {
		//
		// setup builders for assertion and condition
		//
		DefaultBootstrap.bootstrap();
		builderFactory = Configuration.getBuilderFactory();
		assertionBuilder =  (SAMLObjectBuilder<SAMLObject>) builderFactory.getBuilder(Assertion.DEFAULT_ELEMENT_NAME);
		conditionsBuilder = (SAMLObjectBuilder) builderFactory.getBuilder(Conditions.DEFAULT_ELEMENT_NAME);
		//
		//	create object under test
		//
		validator = new AssertionValidator(new AssertionParser());
		validator.setConditionsOnOrAfter(true);
	}

	@Test(expected = ValidationException.class)	
	public void invalidIssuer() throws ValidationException, ConfigurationException{
		
		//  
		// Setup validator
		// 
		validator.setIssuer(ISSUER);
		
		//
		// Create Assertion to test
		//
        Assertion assertion = (Assertion)assertionBuilder.buildObject();
        assertion.setIssuer("NOT" + ISSUER );
        
        //
        // Test!
        //
        validator.validateIssuer(assertion);
	}
	
	@Test
	public void validIssuer() throws ValidationException, ConfigurationException{
		
		//  
		// Setup validator
		// 
		validator.setIssuer(ISSUER);
		
		//
		// Create Assertion to test
		//
        Assertion assertion = (Assertion)assertionBuilder.buildObject();
        assertion.setIssuer(ISSUER);
        
        //
        // Test!
        //
        validator.validateIssuer(assertion);
	}
	
	@Test(expected = ValidationException.class)	
	public void validConditionsNotOnOrAfter() throws ValidationException, ConfigurationException{
		
		//
		// Create Assertion to test
		//
        Assertion assertion = (Assertion)assertionBuilder.buildObject();
        Conditions conditions = (Conditions) conditionsBuilder.buildObject();
        DateTime timeAfterWhichAssertionIsInvalid = DateTime.now();
        conditions.setNotOnOrAfter(timeAfterWhichAssertionIsInvalid);
        assertion.setConditions(conditions);
        //
        // Test!
        //
        DateTime oneDayBeforeAssertionIsInvalid = timeAfterWhichAssertionIsInvalid.minusDays(1);
        validator.validateConditions(assertion, oneDayBeforeAssertionIsInvalid);
	}
	
	@Test(expected = ValidationException.class)	
	public void invalidConditionsNotOnOrAfter() throws ValidationException, ConfigurationException{
		
		//
		// Create Assertion to test
		//
        Assertion assertion = (Assertion)assertionBuilder.buildObject();
        Conditions conditions = (Conditions) conditionsBuilder.buildObject();
        DateTime timeAfterWhichAssertionIsInvalid = DateTime.now();
        conditions.setNotOnOrAfter(timeAfterWhichAssertionIsInvalid);
        assertion.setConditions(conditions);
        
        //
        // Test!
        //
        DateTime oneDayAfterAssertionIsValid = timeAfterWhichAssertionIsInvalid.plusDays(1);
        validator.validateConditions(assertion, oneDayAfterAssertionIsValid);
	}
	
	@Test
	public void equalConditionsNotOnOrAfter() throws ValidationException, ConfigurationException{
		
		//
		// Create Assertion to test
		//
        Assertion assertion = (Assertion)assertionBuilder.buildObject();
        Conditions conditions = (Conditions) conditionsBuilder.buildObject();
        DateTime timeAfterWhichAssertionIsInvalid = DateTime.now();
        conditions.setNotOnOrAfter(timeAfterWhichAssertionIsInvalid);
        assertion.setConditions(conditions);
        
        //
        // Test!
        //
      
        validator.validateConditions(assertion, conditions.getNotOnOrAfter());
	}
	
	

}
