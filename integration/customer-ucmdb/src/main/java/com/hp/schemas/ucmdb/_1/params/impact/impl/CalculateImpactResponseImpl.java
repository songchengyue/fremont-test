/*
 * XML Type:  calculateImpactResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * An XML calculateImpactResponse(@http://schemas.hp.com/ucmdb/1/params/impact).
 *
 * This is a complex type.
 */
public class CalculateImpactResponseImpl extends com.hp.schemas.ucmdb._1.params.impact.impl.ImpactResponseImpl implements com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse
{
    
    public CalculateImpactResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
