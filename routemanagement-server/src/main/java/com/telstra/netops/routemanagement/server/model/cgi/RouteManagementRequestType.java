package com.telstra.netops.routemanagement.server.model.cgi;

/**
 * Common enum class created for different route management levels. Single
 * Prefix Add/Bulk Upload,View , level is 211 Delete, level is 211
 *
 */
public enum RouteManagementRequestType {

	SINGLE(211), DELETE(212), VIEW(211), BULK(211);

	private final Integer value;

	private RouteManagementRequestType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public static RouteManagementRequestType findForValue(Integer value) {
		for (RouteManagementRequestType ipVersion : RouteManagementRequestType.values()) {
			if (value.equals(ipVersion.getValue())) {
				return ipVersion;
			}
		}

		throw new IllegalArgumentException("not such DnsRequestType for value " + value);
	}

}
