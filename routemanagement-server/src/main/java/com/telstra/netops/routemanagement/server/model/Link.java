package com.telstra.netops.routemanagement.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for Link table
 *
 */
@Entity
@Table(name = "Link")
public class Link {

	@Id
	@GeneratedValue
	@Column(name = "SERVICEID")
	private String serviceId;

	@Column(name = "LINKID")
	private String linkId;

	@Column(name = "ROUTER_NAME")
	private String routerName;

	@Column(name = "BHA_ROUTER_NAME")
	private String bhaRoutername;

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) {
		this.linkId = linkId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getRouterName() {
		return routerName;
	}

	public void setRouterName(String routerName) {
		this.routerName = routerName;
	}

	public String getBhaRoutername() {
		return bhaRoutername;
	}

	public void setBhaRoutername(String bhaRoutername) {
		this.bhaRoutername = bhaRoutername;
	}

}
