# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 12 September 2001
# File: CMS_NOTICES.pm
#
# $Id: CMS_WHOLESALER.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $
#

package CMS_WHOLESALER;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_CUSTOMER_DB; # NOTE USES CUSTOMER DB!1

my $uploaddir = "/usr/local/www/www/gia-reach/tgwcustdata/control/reach-reseller/images/rs/upload";

sub start {
  
  
  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} eq "Cancel"))) {
    # default to view_current_outages
    

    CMS::clear_html_values();
    $CMS::html_values{subsystem}{value} = "wholesalers";
    $CMS::html_values{command}{value} = "make_wholesaler";
    
  }

  if ($CMS::html_values{command}{value} eq "make_wholesaler") {

    if ((defined $CMS::html_values{stage}{value}) &&
	($CMS::html_values{stage}{value} eq "2")) {
      
      if (CMS_CUSTOMER_DB::make_customer_into_wholesaler
	  ($CMS::html_values{accno}{value},
	   $CMS::html_values{template}{value},
	   $CMS::html_values{email}{value},
	   $CMS::html_values{contactinfo}{value},
	   $CMS::html_values{logoname}{value})) {
	CMS::add_error("Customer changed to Wholesaler.");
      } else {
	
	CMS::add_error("Failed to change customer into Wholesaler.");
	CMS::add_error($CMS_CUSTOMER_DB::error);
	
      }
    }   
    CMS::output_html("wholesaler_make");
    
    
  } elsif ($CMS::html_values{command}{value} eq "edit_wholesaler") {
    
    if ((defined $CMS::html_values{stage}{value}) &&
	($CMS::html_values{stage}{value} eq "2")) {
      
      if (CMS_CUSTOMER_DB::update_wholesaler_path
	  ($CMS::html_values{accno}{value},
	   $CMS::html_values{template}{value},
	   $CMS::html_values{email}{value},
	   $CMS::html_values{contactinfo}{value},
	   $CMS::html_values{logoname}{value})) {
	CMS::add_error("Updated.");
      } else {
	
	CMS::add_error("Failed to update.");
	CMS::add_error($CMS_CUSTOMER_DB::error);
	
      }
    }   
    CMS::output_html("wholesaler_edit");

  } elsif ($CMS::html_values{command}{value} eq "load_resellerlogo") {

    if(exists $CMS::html_values{file}{value}) {
	&upload_logo($CMS::html_values{filename}{value},
			$CMS::html_values{file}{value});
	delete $CMS::html_values{file}{value};
    }

    CMS::output_html("wholesaler_loadlogo");
    
  } else {
    $CMS::html_values{wholesalers}{table} =
      CMS_CUSTOMER_DB::get_wholesalers_and_paths;
    unshift (@{$CMS::html_values{wholesalers}{table}}, 
		 ["Wholesaler", "Path", "Email", "Logo Filename"]);
    
    $CMS::html_values{wholesalers}{header_rows} = 1;

    CMS::output_html("wholesaler_list");
  }
}

sub upload_logo {
	my $filename = shift @_;
	my $file = shift @_;

	open(my $i, "$file");
	open(my $o, ">$uploaddir/$filename");
	while(my $l = <$i>) {
		print $o $l;
	}
	close($i);
	close($o);

	unlink($file);

}
1;
