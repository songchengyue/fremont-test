#(C) Telstra 2009
#
# Author:Karuna Ballal
# Date: 13 May 2009
# File: CMS_REVERSEDEL_DB.pm
#
# $Id: CMS_REVERSEDEL_DB.pm,v 1.1.1.1 2003/10/19 13:49:52 rxc Exp $


package CMS_REVERSEDEL_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
use CMS_OLSS;

our $error;
our $rdns_value = 0;

# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;

# Select reverse dns record types
# Pre  : contact_email, host, record_parameter
# Prost: $result
sub req_reverse_mapping {
open (DEBUG, ">/tmp/rev_req_map");
		open STDOUT, ">C:/Users/shruti_s03/Desktop/shruti.txt";#added by shruti
	print "hello";
#print $formels{'REV_VIEW'};
	#print "hello1";
close STDOUT;	

#open STDOUT, ">C:/Users/shruti_s03/Desktop/shruti2.txt";
print DEBUG "INSIDE req_reverse_mapping\n\n";
CMS_DB::begin_transaction();
my $uid     = '';
my $accno   = '';
my $contact = '';
my $ptr     = '';
my $in      = '';
my $host    = '';
my $parm    = '';
my $parm_val = '';
my $uid     = $dbh->quote(shift @_);
#print "5 values passed from CMS_REVERSEDEL_DB::req_reverse_mapping (uid, accno,rdns_contact, rdns_ip, rdns_dom);";
#print "1";
#print $uid;
my $accno   = $dbh->quote(shift @_);
#print "2";
#print $accno;
my $contact = $dbh->quote(shift @_);
#print "3";
#print $contact;
#my $parm    = $dbh->quote(shift @_);# original edit
#print "4";
#print $parm;
my $ip_blk = $dbh->quote(shift @_);
#print "5";
#print $ip_blk
my $dom    = (shift @_);
#print "6";
#print $dom;

$dom = $dom.".";
$dom = "\'$dom\'";
my $parm_val = $ip_blk;
#print "7";
#print $parm_val;
my $new_ip_blk = join "/",$ip_blk,"32";
#print "8";
#print $new_ip_blk;
my $temp_sql = '';
my $success = 1;
my $ti = '';
my $match_ip = '';
my $r2 = '';
my $r3 = '';
my $r4 = '';
my $sth = '';
my $r1 = '';
my $r = '';
my $temp_x  = '';
my $div_val = '';
my $host = '';
my $ptr = '';
my $ip_flag = 1;
 my @z1_octets = "";
my $z1_octet1 = "";
my $z1_octet2 = "";
my $z1_octet3 = "";
my $z1_octet4 = "";
my $z1_bin_1 = "";
my $z1_bin_2 = "";
my $z1_bin_3 = "";
my $z1_bin_4 = "";
my @z1_binary = "";
my $z1_binary_stat = "";
my @z1_binary_bit = "";
my @ip_octets = "";
my $ip_octets1 = "";
my $ip_octets2 = "";
my $ip_octets3 = "";
my $ip_octets4 = "";
my $ip_octet1 = "";
my $ip_octet2 = "";
my $ip_octet3 = "";
my $ip_octet4 = "";
my $ip_bin_1 = "";
my $ip_bin_2 = "";
my $ip_bin_3 = "";
my $ip_bin_4 = "";
my @ip_binary = "";
my $ip_binary_stat = "";
my @ip_binary_bit = "";
my $ip_length = "";
my $ip_binary_result = "";
my $z1_binary_result = "";
my $loop_count2 = 0;
my $ipprefix = "";
my $compare_flag=0;
my $last_oct_db = '';

print DEBUG "accno:$accno\n";


$parm_val =~ s/\'//g;
#print "parm_val";
#print $parm_val;
$new_ip_blk = join "/",$parm_val,"32";
#print "new_ip_blk";
#print $new_ip_blk;
my @y = split(/\//,$new_ip_blk);

#close STDOUT;
#open STDOUT, ">C:/Users/shruti_s03/Desktop/shruti.txt";
my $z1= shift(@y);
#print "z1";
#print $z1;


my $z2= shift(@y);
#print $z2;


my @ip_octets = split(/\./,$z1);
#open STDOUT, ">C:/Users/shruti_s03/Desktop/shruti.txt";
#code to split the stored IPs into octets
my $ip_octets1 = shift(@ip_octets);
my $ip_octets2 = shift(@ip_octets);
my $ip_octets3 = shift(@ip_octets);
my $ip_octets4 = shift(@ip_octets);
my $last_oct = join "/",$ip_octets4,$z2;
#print DEBUG "ip_octets4:$ip_octets4\n";
#print DEBUG "last oct:$last_oct\n";
#print DEBUG "parm_val:$parm_val\n";
my $ip_ptr = join ".",$ip_octets3,$ip_octets2,$ip_octets1;
#print "10";
#print $ip_ptr;
my $ptr = join ".",$ip_octets3,$ip_octets2,$ip_octets1,"in-addr.arpa";
#print $ptr;
#my $ptr = join ".", $ip_ptr,"in-addr.arpa";
$ptr = "\'$ptr\'";

$ti = rindex($z1,".");
#print "ti";
#print "$ti";
$match_ip  = substr($z1,0,$ti);
#print "11";
#print $match_ip;
#print DEBUG "match ip:$match_ip\n";


# check for whether the accno exist or not
my $sth = $dbh->prepare ("SELECT count(accno) FROM olss_cust_access_acc WHERE accno like $accno");

$sth->execute();
my $r = $sth->fetchall_arrayref;
#print "12";
#print $r;
#print "13";
#print $r->[0]->[0];
#print "14";
if($r->[0]->[0] >= 1) {
print DEBUG "inside DB mapping select loop\n";
	#check in ip_resource block
        my $sth3 = $dbh->prepare ("SELECT count(ip_block) FROM ip_resource WHERE ip_block like '$match_ip%' AND accno like $accno");
        $sth3->execute();
        my $rt = $sth3->fetchall_arrayref;
		#print "15";
		#print $rt;
		#print "16";
		#print $rt->[0]->[0];
		
		#close STDOUT;
        if ($rt->[0]->[0] >= 1){
	print DEBUG "inside DB mapping user count check\n";
	my $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$parm_val'");
        $sth->execute();
        my $r2 = $sth->fetchrow_hashref;
	#  START_CHECK_3_OCTETS_SAME
        print DEBUG "DB CHECK_3_OCTETS_SAME\n";
        my $db_flag = 0;
        my $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$match_ip%' and accno like $accno");
        $sth->execute();
        my $r5 = $sth->fetchall_arrayref;
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
        print DEBUG "DB 3 temp_x:$temp_x\n";
        for my $lp_count (0 .. $#{$r5}) {
        print DEBUG "lp_count:$lp_count\n";
        my $Ar_chk1 = $r5->[$lp_count]->[0];
        my $ip_block_chk1 = $Ar_chk1;
        print DEBUG "DB ip from db is $ip_block_chk1\n\n";
        my @ip_chk1 = split(/\//,$ip_block_chk1);
        my $ipnew_chk1  = shift(@ip_chk1);#ipnew_chk has ip
        my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
        my @ip_octets = split(/\./,$ipnew_chk1);

        #code to split the stored IPs into octets and changing them to binary
        $ip_octet1 = shift(@ip_octets);
       # print $ip_octet1;
        $ip_octet2 = shift(@ip_octets);
       # print $ip_octet2;
        $ip_octet3 = shift(@ip_octets);
       # print $ip_octet3;
        $ip_octet4 = shift(@ip_octets);
       # print $ip_octet4;
        $last_oct_db = join "/",$ip_octet4,$ipprefixip_chk1;
         #print $last_oct_db;
        print DEBUG "last oct from DB:$last_oct_db\n";
		#close STDOUT;

        $ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
        $ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
        $ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
        $ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

        #combining individual IPs into one variable/array
        my @ip_binary = ();
        push(@ip_binary, $ip_bin_1);
        push(@ip_binary, $ip_bin_2);
        push(@ip_binary, $ip_bin_3);
        push(@ip_binary, $ip_bin_4);
        $ip_binary_stat = join("",@ip_binary);
        @ip_binary_bit = split(//,$ip_binary_stat);

	#code to split the entered IPs into octets and changing octets to binary.
        @z1_octets = split(/\./,$z1);
        $z1_octet1 = shift(@z1_octets);
        $z1_octet2 = shift(@z1_octets);
        $z1_octet3 = shift(@z1_octets);
        $z1_octet4 = shift(@z1_octets);

        $z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
        $z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
        $z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
        $z1_bin_4 = unpack("B8", pack('C',$z1_octet4));

        #combining the individual IPs into one variable/array
        @z1_binary = ();
        push(@z1_binary,$z1_bin_1);
        push(@z1_binary,$z1_bin_2);
        push(@z1_binary,$z1_bin_3);
        push(@z1_binary,$z1_bin_4);
        $z1_binary_stat = join("",@z1_binary);
        @z1_binary_bit = split(//,$z1_binary_stat);
        print DEBUG "input is @z1_binary_bit\n";
        $ipprefix = $ipprefixip_chk1;
	
        # check for whether the IP already exist in reverse
        my $sth1 = $dbh->prepare ("SELECT count(last_oct) FROM reverse WHERE last_oct like '$ip_octets4' and ptrrec like $ptr  and accno like $accno and status = 1");
        $sth1->execute();
        $r3 = $sth1->fetchall_arrayref;

         # CHECK WHEN 3 Octets are SAME and IP PREFIX is diff
        if  ($ipnew_chk1 =~ /$match_ip/)  {
        print DEBUG "DB entered match ip case\n";
        if($r3->[0]->[0] < 1) {
                print DEBUG "DB does not already exist in DB\n";
                print DEBUG "z2:$z2\tipprefix:$ipprefix\n";
                if($z2 > $ipprefix){$ip_length = $ipprefix;}
                else {$ip_length = $z2;}
                print DEBUG "ip length value:$ip_length\n";
                $compare_flag = 1;
		$db_flag = 0;
                for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
                print DEBUG "entered for condition\n";
                        if ($db_flag == 0){
                        print DEBUG "DB entered if compare flag condition\n";
                        $z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
                        $ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
                                if ($z1_binary_result != $ip_binary_result){
                                print DEBUG "DB binary ADD false\n";
                                print DEBUG "DB db flag 1\n";
				$db_flag = 1;
                                }
                                elsif ($z1_binary_result == $ip_binary_result)
                                {
                                print DEBUG "DB equal to binary add\n";
                                print DEBUG "DB db flag 0\n";
                                $db_flag = 0;}
                        }
                }#end of both if loops and for loop
                if ($db_flag == 1 && $compare_flag ==1)
                {
                print DEBUG "DB compare flag 1\n";
                $compare_flag = 1;}
                else
                {
                print DEBUG "DB compare flag 0\n";
                $compare_flag = 0;
                last;
                }
        }#end of r3 if condition
        else {
                #CMS_DB::disconnect_from_transaction();
                #return 1;
        }#end of else r3
        }#end of match ip if condn
        }#end of for loop condition

        if ($compare_flag == 1) 
		{
        print DEBUG "throw error\n";
        #CMS_DB::disconnect_from_transaction();
        return "REVERSE MAPPING cannot be proceeded with the Entered IP block. Please Enter A New IP";
        
		}#end of if
      
	  elsif 
		($compare_flag == 0){
         print DEBUG "DB final check of compare flag 0\n";
        print DEBUG "DB z2:$z2 ipprefix:$ipprefix\n";
        if ($z2 <= $ipprefix) {
        print DEBUG "DB z2:$z2 ipprefix:$ipprefix\n";
        print DEBUG "DB entered IP prefix less thn Db ip prefix\n";
        print DEBUG "parent sub range\n";
        if($$r2{ip_block} =~ /$parm_val/) {
	if($r3->[0]->[0] < 1) {
                my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1, last_oct = '$ip_octets4' WHERE ptrrec like $ptr and accno like $accno";
                print DEBUG "$t_sql\n";
                CMS_DB::begin_transaction();
                my $sth = $dbh->prepare ("$t_sql");
                my $result = $sth->execute;
                print DEBUG "result:$result\n";
                $error = $dbh->errstr;
                print DEBUG "error:$error\n";
                return $result;
                #CMS_DB::disconnect_from_transaction();
                }
        }#nd of r2 if condn
        }#end of z2< condn
        else {
        print DEBUG "success\n";
        print DEBUG "sub range true\n";
        #CMS_DB::disconnect_from_transaction();
        my $sthr = $dbh->prepare ("Select count(last_oct) from reverse where status = 1 and ptrrec like $ptr and last_oct like '$ip_octets4' and accno like $accno ");
	print DEBUG "Select count(last_oct) from reverse where status = 1 and ptrrec like $ptr and last_oct like
 '$ip_octets4' and accno like $accno \n";
        print DEBUG "variables for select query:ptr:$ptr\tlastst_oct_db:$ip_octets4\taccno:$accno";
        $sthr->execute();
        my $r_1 = $sthr->fetchall_arrayref;
        print DEBUG "outside r_1 condn\n";
        print DEBUG "($r_1->[0]->[0])\n";
        if($r_1->[0]->[0] < 1) {
        print DEBUG "inside r_1 condition\n";
        if($r3->[0]->[0] < 1) {
                my $temp_sql = "INSERT INTO reverse (username,accno,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time) VALUES ($uid, $accno, $contact,$ptr,'PTR','$ip_octets4',$dom,1,1,current_timestamp)";
                $sth = $dbh->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                #CMS_DB::disconnect_from_transaction();
		print DEBUG "success:$success\n";
                return $error if ($error);
        #        return $result;
                return $success;
                }
        }else {print DEBUG "IP already exists in DB\n";
                #return "The IP Address has already been requested. Please Cancel it and try again.";
		my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr and accno like $accno and last_oct like '$ip_octets4' and status = 1";
		print DEBUG "$t_sql\n";
                CMS_DB::begin_transaction();
                my $sth = $dbh->prepare ("$t_sql");
                my $result = $sth->execute;
                print DEBUG "result:$result\n";
                $error = $dbh->errstr;
                print DEBUG "error:$error\n";
                return $result;
                }
	#return 1;
        }
        }#end of elsif

        # ENDED_CHECK_3_OCTETS_SAME

        print DEBUG "FINAL ERROR: REQUEST cannot be proceeded with the Entered IP. Please Enter A New IP.\n";
        return "REVERSE MAPPING REQUEST cannot be proceeded with the Entered IP Block. Please Enter A New IP Address";

        }
		else
		{
        print DEBUG "IP Address block does not exist in ip_resource\n";
        return "REVERSE MAPPING REQUEST cannot be proceeded with the Entered IP. Please Enter A New IP Address";
        }
} else {
print DEBUG "Account does not exist.\n";
return "Account does not exist";
}
close (DEBUG);
}


# Select reverse dns record types
# Pre  : contact_email, host, record_parameter
# Prost: $result
sub rev_delegation {
CMS_DB::begin_transaction();
open (DEBUG, ">/tmp/rev_del");
print DEBUG "INSIDE sub rev_delegation\n";

my $new_last_oct = '';
my $uid     = '';
my $accno   = '';
my $contact = '';
my $ptr     = '';
my $in      = '';
my $host    = '';
my $parm    = '';
my $parm_val = '';
my $uid     = $dbh->quote(shift @_);
my $accno   = $dbh->quote(shift @_);
my $contact = $dbh->quote(shift @_);
#my $parm    = $dbh->quote(shift @_);
my $ip_blk = $dbh->quote(shift @_);
my $dom    = (shift @_);
$dom = $dom.".";
$dom = "\'$dom\'";
my $parm_val = $ip_blk;
my $temp_sql = '';
my $success = 1;
my $ti = '';
my $match_ip = '';
my $r2 = '';
my $r3 = '';
my $r4 = '';
my $sth = '';
my $r1 = '';
my $r = '';
my $temp_x  = '';
my $div_val = '';
my $host = '';
my $ptr = '';
my $ip_flag = 1;
my @z1_octets = "";
my $z1_octet1 = "";
my $z1_octet2 = "";
my $z1_octet3 = "";
my $z1_octet4 = "";
my $z1_bin_1 = "";
my $z1_bin_2 = "";
my $z1_bin_3 = "";
my $z1_bin_4 = "";
my @z1_binary = "";
my $z1_binary_stat = "";
my @z1_binary_bit = "";
my @ip_octets = "";
my $ip_octet1 = "";
my $ip_octet2 = "";
my $ip_octet3 = "";
my $ip_octet4 = "";
my $ip_octets4 = "";
my $ip_bin_1 = "";
my $ip_bin_2 = "";
my $ip_bin_3 = "";
my $ip_bin_4 = "";
my @ip_binary = "";
my $ip_binary_stat = "";
my @ip_binary_bit = "";
my $ip_length = "";
my $ip_binary_result = "";
my $z1_binary_result = "";
my $loop_count2 = 0;
my $ipprefix = "";
my $compare_flag=0;
my $last_oct_db = '';

$parm_val =~ s/\'//g;
my @y = split(/\//,$parm_val);
my $z1= shift(@y);
my $z2= shift(@y);
my @ip_octets = split(/\./,$z1);

#code to split the stored IPs into octets
my $ip_octet1 = shift(@ip_octets);
my $ip_octet2 = shift(@ip_octets);
my $ip_octet3 = shift(@ip_octets);
my $ip_octet4 = shift(@ip_octets);
$ip_octets4 = $ip_octet4;
my $last_oct = join "/",$ip_octet4,$z2;
print DEBUG "last oct:$last_oct\n";
print DEBUG "parm_val:$parm_val\n";
my $ip_ptr = join ".",$ip_octet3,$ip_octet2,$ip_octet1;
my $ptr = join ".",$ip_octet3,$ip_octet2,$ip_octet1,"in-addr.arpa";
#my $ptr = join ".", $ip_ptr,"in-addr.arpa";
$ptr = "\'$ptr\'";

$ti = rindex($z1,".");
$match_ip  = substr($z1,0,$ti);
print DEBUG "match ip:$match_ip\n";

# check for whether the accno exist or not
my $sth = $dbh->prepare ("SELECT count(accno) FROM olss_cust_access_acc WHERE accno like $accno");
$sth->execute();
my $r = $sth->fetchall_arrayref;
if($r->[0]->[0] >= 1) {
	#check in ip_resource block
	my $sth3 = $dbh->prepare ("SELECT count(ip_block) FROM ip_resource WHERE ip_block like '$match_ip%' AND accno like $accno");
	$sth3->execute();
        my $rt = $sth3->fetchall_arrayref;
	if ($rt->[0]->[0] >= 1){
	my $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$parm_val'");
        $sth->execute();
        my $r2 = $sth->fetchrow_hashref;
        # check for whether the IP already exist in reverse
        my $sth1 = $dbh->prepare ("SELECT count(last_oct) FROM reverse WHERE last_oct like '$last_oct' and ptrrec like $ptr and status =1 and accno like $accno and status = 1");
        $sth1->execute();
        $r3 = $sth1->fetchall_arrayref;



	#  START_CHECK_4_OCTETS_SAME
	my $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$z1%' and accno like $accno");
	$sth->execute();
        my $r4 = $sth->fetchall_arrayref;
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
	for my $lp_count (0 .. $#{$r4}) {
        my $Ar_chk = $r4->[$lp_count]->[0];
        my $ip_block_chk = $Ar_chk;
        print DEBUG "DB ip from db is $ip_block_chk\n";
	my @ip_chk = split(/\//,$ip_block_chk);
        my $ipnew_chk  = shift(@ip_chk);#ipnew_chk has ip
        my $ipprefixip_chk = shift(@ip_chk);#has prefix
	print DEBUG "DB ipnew_chk: $ipnew_chk\t \t ipprefixip_chk: $ipprefixip_chk\n";
	
	# CHECK WHEN 4 Octets are SAME and IP PREFIX is 24
	
 	if ( ($ipnew_chk =~ /$z1/) && ($ipprefixip_chk == 24) ) {	
		print DEBUG "ipprefixip_chk 24: $ipprefixip_chk\n";
	
		if ($z2 > $ipprefixip_chk) {
			print DEBUG "entered z2> loop\n";
			my $ct_ip = ($z2 - $ipprefixip_chk);
			print DEBUG "ct_ip:$ct_ip\n";
			my $ct_ip1 = 0;
			my $new_z2 = 0;
			print DEBUG "z2:$z2\n";
			my $z3 = $z2;
				for ($ct_ip1 = 0; $ct_ip1 <= $ct_ip; $ct_ip1++){
					print DEBUG "ct_ip1:$ct_ip1\n";
					$new_z2 = ($z3 - 1);
					$z3 = ($z3 - 1);
					my $x2 = `$z2 - 1`;
					print DEBUG "x2 : $x2\n";
					print DEBUG "new_z2:$new_z2\n";
					print DEBUG "z3:$z3\n";
					my $new_last = join "/",$ip_octet4, $new_z2;
					print DEBUG "new_last:$new_last\n";
					my $sth = $dbh -> prepare("SELECT count(last_oct) from reverse where last_oct like '$new_last' and ptrrec like $ptr and accno like $accno and status = 1");
					$sth->execute();
					my $query = $sth->fetchall_arrayref;
					if ($query->[0]->[0]>=1){
						return "The entered IP Block has already been requested. Please Cancel it and try again\n";
						print DEBUG "already exists 24\n";
					}#end of if query loop
				}#end of for loop
			
			}#end of ip z2 loop
		my $sth = $dbh->prepare("SELECT count(*) from reverse where ptrrec like $ptr and accno like $accno");
		$sth -> execute();
		my $count_ptr = $sth->fetchall_arrayref;
		print DEBUG "count ptr:$count_ptr->[0]->[0]\n";
		if ($count_ptr->[0]->[0] > 0) {
			if($r3->[0]->[0] >= 1) {
			print DEBUG "already exists in the DB\n";
			 my $temp_sql = "INSERT INTO reverse (username,accno,contact,ptrrec,rec_type,Last_oct,fqdn,status,zone_status,request_time) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp)";
                        $sth = $dbh->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         my $result = ($success ? $dbh->commit : $dbh->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         #CMS_DB::disconnect_from_transaction();
                         return $error if ($error);
			 #return $result;
			 return $success;
			}
		#	my $sth = $dbh->prepare("UPDATE reverse set request_time = current_timestamp, contact = $contact, last_oct = '$last_oct', fqdn = $dom where status = 1 and ptrrec like $ptr and accno like $accno and rec_type like 'NS' and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'");
			#my $sth = $dbh->prepare("UPDATE reverse set request_time = current_timestamp, contact = $contact, last_oct = '$last_oct', fqdn = $dom where status = 1 and ptrrec like $ptr and accno like $accno and rec_type like 'NS'");
		 	#my $success = $sth->execute;
                        #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                        #my $result = ($success ? $dbh->commit : $dbh->rollback);
                        #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                        #CMS_DB::disconnect_from_transaction();
                        #return $error if ($error);
                        #return $result;
		} else {
			print DEBUG "4 oct before insert1\n";
			 my $temp_sql = "INSERT INTO reverse (username,accno,contact,ptrrec,rec_type,Last_oct,fqdn,status,zone_status,request_time) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp)";
                        $sth = $dbh->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         my $result = ($success ? $dbh->commit : $dbh->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         #CMS_DB::disconnect_from_transaction();
                         return $error if ($error);
                         #return $result;
                         return $success;
		}#end of if else count ptr condn
		if($r3->[0]->[0] < 1) {
			my $sth = $dbh->prepare("SELECT count(last_oct) from reverse where ptrrec like $ptr and accno like $accno and rec_type like 'NS' and status = 1 and last_oct not like '$last_oct'  and fqdn not like 'ns03.telstraglobal.net.' and fqdn not like 'ns04.telstraglobal.net.' and fqdn not like 'unknown.telstraglobal.net.' and fqdn not like 'static.telstraglobal.net.'");
			print DEBUG "sth:$sth\n";
 		 	$sth->execute();
			my $result = $sth->fetchall_arrayref;
			if (($result->[0]->[0]>=1) && ($z2 == 24)){
				#print DEBUG "most recently added update\n";
				print DEBUG "UPDATE1error\n";
				#my $temp_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1,last_oct = '$last_oct' where ptrrec like $ptr and accno like $accno and rec_type like 'NS' and status = 1 and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.'  and fqdn not ilike 'static.telstraglobal.net.'";
				#$sth = $dbh->prepare ("$temp_sql");
                                #my $success = $sth->execute;
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #my $result = ($success ? $dbh->commit : $dbh->rollback);
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                #return $error if ($error);
                                #return $result;
				return "The sub range for the entered IP Block has already been requested.";
                                }else {
			if ($z2 != 24){
print DEBUG "inside z2 ne 24 loop\n";
my %eq_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);

my $eq_initial = 0;
my $eq_count_ip = (32 - $z2);
my $eq_count_ip1 = 0;
my $eq_latestz2 = $z2;
my $check_rs = 0;
for ($eq_count_ip1 = 0; $eq_count_ip1 <= $eq_count_ip; $eq_count_ip1++) {
print DEBUG "eq_count_ip:$eq_count_ip\teq_count_ip1:$eq_count_ip1\n";
my $v_next = $eq_latestz2 + 1;
my $eq_w_range = $eq_range{$v_next};
my $eq_hop = 256/$eq_w_range;
my $eq_i = 0;

	for ($eq_i = 1; $eq_i <= $eq_hop; $eq_i++) {		
		print DEBUG "inside hop loop for z2 ne 24 condition\n";
		my $sth = $dbh-> prepare("SELECT count(last_oct) from reverse where last_oct like '$eq_initial/$v_next' and ptrrec like $ptr and accno like $accno and status = 1");
		$sth->execute();
		my $rs = $sth->fetchall_arrayref;
		print DEBUG "eq_initial/:$eq_initial\t$eq_w_range\tv_next:$v_next\trs:$rs->[0]->[0]\n";
			if ($rs->[0]->[0]>=1){
				print DEBUG "ip_octets4:$ip_octets4 before if check\n";
				if (($eq_initial >= $ip_octets4) && ($ip_octets4 <= ($eq_initial+$eq_w_range))){
				$check_rs = $check_rs + 1;
				#print DEBUG  "prefix not 24, update over sub range\n";
				print DEBUG "UPDATE2error\n";
                                #my $t_sql = "UPDATE reverse SET fqdn = $dom, last_oct = '$last_oct', request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$eq_initial/$v_next'";
                                #print DEBUG "$t_sql\n";
                                #CMS_DB::begin_transaction();
                                #my $sth = $dbh->prepare ("$t_sql");
                                #my $result = $sth->execute;
                                $check_rs = $check_rs+1;
                                #print DEBUG "result:$result\n";
                                #$error = $dbh->errstr;
                                #print DEBUG "error:$error\n";
				#print DEBUG "inside rs condn-check_rs:$check_rs\n";
				}#end of if ipoct check
				}#end of rs >= 1 
	
		$eq_initial = $eq_initial+$eq_w_range;		
		
	}#end for for i

	$eq_initial = 0;
        $eq_w_range = $eq_w_range+$eq_w_range;
        $eq_latestz2 = $eq_latestz2 +1;
        if ($eq_latestz2 == 32) { last;}
	print DEBUG "eq_latestz2:$eq_latestz2\teq_w_range:$eq_w_range\n";


}#end of for count ip
	if ($check_rs >= 1){
                    return "The sub range for the entered IP Block has already been requested";
		}else {
			print DEBUG "4 oct before insert2 prefix not 24\n";
                                 my $temp_sql = "INSERT INTO reverse (username,accno,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp)";
                                $sth = $dbh->prepare ("$temp_sql");
                                $check_rs = $check_rs+1;
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                my $result = ($success ? $dbh->commit : $dbh->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                return $error if ($error);
                                #return $result;
                                return $success;
                                }

		}#end of z2 if
			}#end of result else
                }else {
			#print DEBUG "inside 24 ipp update cond\n";
			print DEBUG "UPDATE3error\n";
			#my $temp_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1,last_oct = '$last_oct' WHERE ptrrec like $ptr and accno like $accno and rec_type like 'NS' and status = 1 and last_oct like '$last_oct'";
                                #$sth = $dbh->prepare ("$temp_sql");
                                #my $success = $sth->execute;
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #my $result = ($success ? $dbh->commit : $dbh->rollback);
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                #return $error if ($error);
                                #return $result;
				return "The sub range for the entered IP Block has already been requested.";
                                }

			
	#CMS_DB::disconnect_from_transaction();
        #return 1;
        }

	# CHECK WHEN 4 Octets are SAME and IP PREFIX is different

	elsif ( ($ipnew_chk =~ /$z1/) && ($ipprefixip_chk != $z2) ) {
	print DEBUG "DB 4 Octets are SAME and IP PREFIX is different\n";
	print DEBUG "DB ipprefixip_chk: $ipprefixip_chk\n";
	print DEBUG "DB before check z2:$z2\n";
		if($z2 >= $ipprefixip_chk) {
		print DEBUG "DB after check z2:$z2\n";
		$new_last_oct = join "/",$ip_octet4,$ipprefixip_chk;
		my $sth5 = $dbh->prepare("SELECT count(last_oct) from reverse where last_oct like '$new_last_oct' and accno like $accno");
	 	$sth5->execute();
       		my $rt1 = $sth5->fetchall_arrayref;
        	if ($rt1->[0]->[0] < 1) {
                	if($r3->[0]->[0] < 1) {
			# check for whether the IP exist or not in ip_resource table
                	my $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$parm_val'");
                	$sth->execute();
                	my $r2 = $sth->fetchrow_hashref;
			if($$r2{ip_block} =~ /$parm_val/) {
				my $temp_sql = "INSERT INTO reverse (username,accno,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp)";
                        	$sth = $dbh->prepare ("$temp_sql");
                        	my $success = $sth->execute;
                         	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         	my $result = ($success ? $dbh->commit : $dbh->rollback);
                         	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         	#CMS_DB::disconnect_from_transaction();
                         	return $error if ($error);
                         	#return $result;
                         	return $success;
				}
				print DEBUG "4 octets ip not matchin ip resource bloack\n";
	#			return "REVERSE DELEGATION REQUEST cannot be proceeded with the Entered IP Block. Please Enter A New IP block.\n";
 	               }
		}
		}# end of z2 if condn
	}#end of elsif condition
	}#end of loop

	# ENDED_CHECK_4_OCTETS_SAME

	#  START_CHECK_3_OCTETS_SAME
	print DEBUG "DB CHECK_3_OCTETS_SAME\n";
	my $db_flag = 0;
	my $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$match_ip%' and accno like $accno");
	$sth->execute();
        my $r5 = $sth->fetchall_arrayref;
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
	print DEBUG "DB 3 temp_x:$temp_x\n";
        for my $lp_count (0 .. $#{$r5}) {
	print DEBUG "lp_count:$lp_count\n";
        my $Ar_chk1 = $r5->[$lp_count]->[0];
        my $ip_block_chk1 = $Ar_chk1;
        print DEBUG "DB ip from db is $ip_block_chk1\n\n";
        my @ip_chk1 = split(/\//,$ip_block_chk1);
        my $ipnew_chk1  = shift(@ip_chk1);#ipnew_chk has ip
	my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
        my @ip_octets = split(/\./,$ipnew_chk1);

	#code to split the stored IPs into octets and changing them to binary
	$ip_octet1 = shift(@ip_octets);
	$ip_octet2 = shift(@ip_octets);
	$ip_octet3 = shift(@ip_octets);
	$ip_octet4 = shift(@ip_octets);
	$last_oct_db = join "/",$ip_octet4,$ipprefixip_chk1;
	print DEBUG "last oct from DB:$last_oct_db\n";

	$ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
	$ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
	$ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
	$ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

	#combining individual IPs into one variable/array
	my @ip_binary = ();
	push(@ip_binary, $ip_bin_1);
	push(@ip_binary, $ip_bin_2);
	push(@ip_binary, $ip_bin_3);
	push(@ip_binary, $ip_bin_4);
	$ip_binary_stat = join("",@ip_binary);
	@ip_binary_bit = split(//,$ip_binary_stat);
	#print"ip binary is @ip_binary_bit\n";

	#code to split the entered IPs into octets and changing octets to binary.
	@z1_octets = split(/\./,$z1);
        $z1_octet1 = shift(@z1_octets);
        $z1_octet2 = shift(@z1_octets);
        $z1_octet3 = shift(@z1_octets);
        $z1_octet4 = shift(@z1_octets);

        $z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
        $z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
        $z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
        $z1_bin_4 = unpack("B8", pack('C',$z1_octet4));
	
	#combining the individual IPs into one variable/array
        @z1_binary = ();
        push(@z1_binary,$z1_bin_1);
        push(@z1_binary,$z1_bin_2);
        push(@z1_binary,$z1_bin_3);
        push(@z1_binary,$z1_bin_4);
        $z1_binary_stat = join("",@z1_binary);
        @z1_binary_bit = split(//,$z1_binary_stat);
        print DEBUG "input is @z1_binary_bit\n";
        $ipprefix = $ipprefixip_chk1;

	 # CHECK WHEN 3 Octets are SAME and IP PREFIX is diff
        if  ($ipnew_chk1 =~ /$match_ip/)  {
        print DEBUG "DB entered match ip case\n";
        if($r3->[0]->[0] < 1) {
                print DEBUG "DB does not already exist in DB\n";
                print DEBUG "z2:$z2\tipprefix:$ipprefix\n";
                if($z2 > $ipprefix){$ip_length = $ipprefix;}
                else {$ip_length = $z2;}
                print DEBUG "ip length value:$ip_length\n";
                $compare_flag = 1;
		$db_flag = 0;
                for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
                print DEBUG "entered for condition\n";
                        if ($db_flag == 0){
                        print DEBUG "DB entered if compare flag condition\n";
                        $z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
                        $ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
                                if ($z1_binary_result != $ip_binary_result){
                                print DEBUG "DB binary ADD false\n";
                                print DEBUG "DB db flag 1\n";
                                $db_flag = 1;
                                }
                                elsif ($z1_binary_result == $ip_binary_result)
                                {
                                print DEBUG "DB equal to binary add\n";
                                print DEBUG "DB db flag 0\n";
                                $db_flag = 0;}
                        }
                }#end of both if loops and for loop
		if ($db_flag == 1 && $compare_flag ==1)
                {
                print DEBUG "DB compare flag 1\n";
                $compare_flag = 1;}
        	else
                {
                print DEBUG "DB compare flag 0\n";
                $compare_flag = 0;
		last;
		}
        }#end of r3 if condition
	else {
		#CMS_DB::disconnect_from_transaction();
        #	return 1;
	}#end of else r3
	}#end of match ip if condn
	}#end of for loop condition

 	if ($compare_flag == 1) {
	print DEBUG "throw error\n";
	#CMS_DB::disconnect_from_transaction();
	return "REVERSE DELEGATION cannot be proceeded with the Entered IP block. Please Enter A New IP";
	}#end of if
	elsif ($compare_flag == 0){
	 print DEBUG "DB final check of compare flag 0\n";
	print DEBUG "DB z2:$z2\tipprefix:$ipprefix\n";
        if ($z2 < $ipprefix) {
	print DEBUG "DB z2:$z2 ipprefix:$ipprefix\n";
        print DEBUG "DB entered IP prefix less thn Db ip prefix\n";
        print DEBUG "parent sub range\n";
        if($$r2{ip_block} =~ /$parm_val/) {
                if($r3->[0]->[0] < 1) {
        	print DEBUG "UPDATE4error\n";
                #my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1, last_oct = '$last_oct' WHERE ptrrec like $ptr accno like $accno and rec_type like 'NS'";
	 	#print DEBUG "$t_sql\n";
                #CMS_DB::begin_transaction();
                #my $sth = $dbh->prepare ("$t_sql");
                #my $result = $sth->execute;
                #print DEBUG "result:$result\n";
                #$error = $dbh->errstr;
                #print DEBUG "error:$error\n";
                #return $result;
                #CMS_DB::disconnect_from_transaction();
		return "The sub range for the entered IP Block has already been requested.";
                }
	}#nd of r2 if condn
	}#end of z2< condn
	elsif ($z2 == $ipprefix) {
		print DEBUG "entered z2 = condn\n";
		my %eq_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2);

my $eq_initial = 0;
my $eq_count_ip = (32 - $z2);
my $eq_count_ip1 = 0;
my $eq_latestz2 = $z2;

for ($eq_count_ip = 0; $eq_count_ip1 <= $eq_count_ip; $eq_count_ip1++) {

my $v_next = $eq_latestz2 + 1;
my $eq_w_range = $eq_range{$v_next};
my $eq_hop = 256/$eq_w_range;
my $eq_i = 0;

	for ($eq_i = 1; $eq_i <= $eq_hop; $eq_i++) {		
		my $sth = $dbh-> prepare("SELECT count(last_oct) from reverse where last_oct like '$eq_initial/$v_next' and ptrrec like $ptr and accno like $accno and status = 1");
		$sth->execute();
		my $rs = $sth->fetchall_arrayref;

			if ($rs->[0]->[0]>=1){
			if (($eq_initial >= $ip_octets4) && ($ip_octets4 <= ($eq_initial+$eq_w_range))){
					#print DEBUG "update when z2 = ipprefix\n";	
						print DEBUG "UPDATE5error\n";	
						#my $t_sql = "UPDATE reverse SET fqdn = $dom, last_oct = '$last_oct', request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$eq_initial/$v_next'";
                				#print DEBUG "$t_sql\n";
                				#CMS_DB::begin_transaction();
                				#my $sth = $dbh->prepare ("$t_sql");
               	 				#my $result = $sth->execute;
                				#3print DEBUG "result:$result\n";
                				#$error = $dbh->errstr;
                				#print DEBUG "error:$error\n";
                				#return $result;
						return "The sub range for the entered IP Block has already been requested.";
				}#end of if ip oct condition	
			}else {
				print DEBUG "insert when z2 = ipprefix\n";
				 my $temp_sql = "INSERT INTO reverse (username,accno,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp)";
                                $sth = $dbh->prepare ("$temp_sql");
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                my $result = ($success ? $dbh->commit : $dbh->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                return $error if ($error);
                                #return $result;
                                return $success;
                                }

				
	
		$eq_initial = $eq_initial+$eq_w_range;		
		
	}#end for for i

	$eq_latestz2 = $eq_latestz2 -1;
	$eq_initial = 0;
        $eq_w_range = $eq_w_range+$eq_w_range;
        $eq_latestz2 = $eq_latestz2 +1;
        if ($eq_latestz2 == 32) { last;}


}#end of for count ip
	}#end of z2 eq condition
	elsif ($z2 > $ipprefix) {
        print DEBUG "success\n";
	print DEBUG "sub range true\n";
        #CMS_DB::disconnect_from_transaction();
	my $sthr = $dbh->prepare ("Select count(last_oct) from reverse where status = 1 and ptrrec like $ptr and last_oct like '$last_oct_db' and accno like $accno ");
	print DEBUG "variables for select query:ptr:$ptr\tlastst_oct_db:$last_oct_db\taccno:$accno";
	$sthr->execute();
	my $r_1 = $sthr->fetchall_arrayref;
	print DEBUG "outside r_1 condn\n";
	print DEBUG "($r_1->[0]->[0])\n";
	if($r_1->[0]->[0] < 1) {
	print DEBUG "inside r_1 condition\n";
 	if($r3->[0]->[0] < 1) {
	print DEBUG "entered r3 loop\n";
#my %v_range = (25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2);
my %v_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2);

my $v_initial = 0;


my $count_ip = ($z2 - $ipprefix);
print DEBUG "ipprefix:$ipprefix\n";
my $count_ip1 = 0;
my $latestz2 = $z2;

for ($count_ip1 = 1; $count_ip1 <= $count_ip; $count_ip1++) {

my $v_previous = $latestz2 -1;
my $w_range = $v_range{$v_previous};
print DEBUG "w_range:$w_range\n";
my $v_hop = (256/($w_range));
print DEBUG "v_previous:$v_previous\tw_range:$w_range\tv_hop:$v_hop\n";
my $i = 0;
print DEBUG "beforei loop\n";
print DEBUG "count_ip:$count_ip\tcount_ip1:$count_ip1\n";
	for ($i = 1; $i <= $v_hop; $i++) {		
		print DEBUG "inside i-v_hop loop\n";
		my $sth = $dbh-> prepare("SELECT count(last_oct) from reverse where last_oct like '$v_initial/$v_previous' and ptrrec like $ptr and accno like $accno and status = 1"); 

		$sth->execute();
		print DEBUG "i inside :$i\n";
		my $rs = $sth->fetchall_arrayref;
			print DEBUG  "SELECT count(last_oct) from reverse where last_oct like '$v_initial/$v_previous' and ptrrec like $ptr and accno like $accno and status = 1\n";
			print DEBUG "rs:$rs->[0]->[0]\n";
			print DEBUG "ip_octets4:$ip_octets4 before if check\n";
			if ($rs->[0]->[0]>=1){
				print DEBUG "v_initial:$v_initial\tw_range:$w_range\n";
				if (($v_initial <= $ip_octets4) && ($ip_octets4 < ($v_initial+$w_range))){
						print DEBUG "just before throwin error..newly removed compare condition\n";
						return "The IP Block that has been requested is already present. Please Cancel it and try again."
				}#end of ipoct if
			}#end of if rs
	
		$v_initial = $v_initial+$w_range;		
		print DEBUG "after rs loop:v_initial:$v_initial\tw_range:$w_range\n";
		print DEBUG "for i loop ends\n";	
	}#end for for i
	$v_initial = 0;
	$latestz2 = $latestz2 -1;
	$w_range = $w_range+$w_range;

}#end of for count

#to check for parent IP
my %new_v_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);
my $check_rs = 0;
my $new_v_initial = 0;
my $new_count_ip = (32 - $z2);
my $new_count_ip1 = 0;
my $new_latestz2 = $z2;
print DEBUG "new_latestz2:$new_latestz2\n";

for ($new_count_ip1 = 0; $new_count_ip1 <= $new_count_ip; $new_count_ip1++) {
print DEBUG "new_count_ip1:$new_count_ip1\tnew_count_ip:$new_count_ip\n";

my $v_next = $new_latestz2 + 1;
my $new_w_range = $new_v_range{$v_next};
my $new_v_hop = 256/$new_w_range;
my $new_i = 0;
print DEBUG "v_next:$v_next\tnew_w_range:$new_w_range\tnew_v_hop:$new_v_hop\n";

	for ($new_i = 1; $new_i <= $new_v_hop; $new_i++) {		
		print DEBUG "new_i:$new_i\tnew_v_hop:$new_v_hop\n";
		print DEBUG "before select count:new_v_initial:$new_v_initial\tv_next:$v_next\n";
		print DEBUG "before select count:ptr:$ptr\taccno:$accno\n";
		my $t_r = "SELECT count(last_oct) from reverse where last_oct like '$new_v_initial/$v_next' and ptrrec like $ptr and accno like $accno and status = 1";
		#my $sth_r = $dbh-> prepare("SELECT count(*) from reverse where last_oct like $new_v_initial\/$v_next and ptrrec like $ptr and accno like $accno and status = 1"); 
		print DEBUG "t_r:$t_r\n";
		my $sth_r = $dbh->prepare("$t_r");
		$sth_r->execute();
		my $rs_r = $sth_r->fetchall_arrayref;
				print DEBUG "rs:$rs_r->[0]->[0]\n";
			print DEBUG "ip_octets4:$ip_octets4 before if check\n";
			if ($rs_r->[0]->[0]>=1){
				print DEBUG "inside rs 1 condition rs:$rs_r->[0]->[0]\tip_octets4:$ip_octets4\n";
				if (($new_v_initial >= $ip_octets4) && ($ip_octets4 <= ($new_v_initial+$new_w_range))){
						print DEUBG "UPDATE6error\n";
						#my $t_sql = "UPDATE reverse SET last_oct = '$last_oct', fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$new_v_initial/$v_next' and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'";
						#my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$new_v_initial/$v_next'";
						$check_rs = $check_rs+1;
						#print DEBUG "check_rs:$check_rs\n";
                				#print DEBUG "t sql:$t_sql\n";
                				#3CMS_DB::begin_transaction();
                				#my $sth = $dbh->prepare ("$t_sql");
               	 				#my $result = $sth->execute;
                				#print DEBUG "result:$result\n";
                				#$error = $dbh->errstr;
                				#print DEBUG "error:$error\n";
                		#		return $result;

				}#end of if oct 
			}#end of if rs
	
		$new_v_initial = $new_v_initial+$new_w_range;		

	}#end for for i

	$new_v_initial = 0;
	$new_w_range = $new_w_range+$new_w_range;
	$new_latestz2 = $new_latestz2 +1;
	if ($new_latestz2 == 32) { last;}

}#end of for count ip
if ($check_rs >= 1){ #return 1;
	return "The sub range for the entered IP Block has already been requested.";
	 }

		print DEBUG "before insert\n";
		my $temp_sql = "INSERT INTO reverse (username,accno,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp)";
		$sth = $dbh->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                #CMS_DB::disconnect_from_transaction();
                return $error if ($error);
                #return $result;
                return $success;
                } else { print DEBUG "IP block already exists in the DB, else of r3\n";
			my $temp_sql = "INSERT INTO reverse (username,accno,contact,ptrrec,rec_type,last_oct,fqdn,status,zone_status,request_time) VALUES ($uid, $accno, $contact,$ptr,'NS','$last_oct',$dom,1,1,current_timestamp)";
                	$sth = $dbh->prepare ("$temp_sql");
                	my $success = $sth->execute;
                	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                	my $result = ($success ? $dbh->commit : $dbh->rollback);
                	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                	#CMS_DB::disconnect_from_transaction();
                	return $error if ($error);
                	#return $result;
                	return $success;
			}#else of r3		

	}else {print DEBUG "range already exists in DB\n";
		if ($last_oct_db eq $last_oct){
		print DEBUG "UPDATE7error\n";
		#my $t_sql = "UPDATE reverse SET fqdn = $dom, request_time = current_timestamp, contact = $contact, status = 1 WHERE ptrrec like $ptr  and accno like $accno and last_oct like '$last_oct'";
                #print DEBUG "$t_sql\n";
                #CMS_DB::begin_transaction();
                #my $sth = $dbh->prepare ("$t_sql");
                #my $result = $sth->execute;
                #print DEBUG "result:$result\n";
                #$error = $dbh->errstr;
                #print DEBUG "error:$error\n";
                #return $result;
                #CMS_DB::disconnect_from_transaction();
		return "The sub range for the entered IP Block has already been requested.";
		} else {
		print DEBUG "sub range case..throw error\n";
		return "The IP Block has already been requested. Please Cancel it and try again.";
		}
	}
        #return 1;
	print DEBUG "r3 neither <1 nor else condition\n";
        }
	}#end of elsif
	
	# ENDED_CHECK_3_OCTETS_SAME
	
	print DEBUG "FINAL ERROR: REQUEST cannot be proceeded with the Entered IP. Please Enter A New IP.\n";
        return "REVERSE DELEGATION REQUEST cannot be proceeded with the Entered IP Block. Please Enter A New IP block";

	}else {
	print DEBUG "IP Address block does not exist in ip_resource\n";
	return "REVERSE DELEGATION REQUEST cannot be proceeded with the Entered IP Block. Please Enter A New IP block";
	}
} else {
print DEBUG "Account does not exist.\n";
return "Account does not exist";
}

close (DEBUG);
}

open(DEBUG,">>/tmp/tmp_reverse_ajax");

sub check_ipRev { 
my ($accno,$ip_reverse) = @_;
CMS_DB::connect_to_database;
$accno = "\'$accno\'";
        print DEBUG "accno:$accno\n";
$ip_reverse =~ s/\'//g;
#my @y = split(/\//,$ip_reverse);
#my $z1= shift(@y);
#my $z2= shift(@y);
my @ip_octets = split(/\./,$ip_reverse);

#code to split the stored IPs into octets
my $ip_octets1 = shift(@ip_octets);
my $ip_octets2 = shift(@ip_octets);
my $ip_octets3 = shift(@ip_octets);
my $ip_octets4 = shift(@ip_octets);
$ip_octets4 = "\'$ip_octets4\'";
print DEBUG "ip_octets4:$ip_octets4\n";
my $ptr = join ".",$ip_octets3,$ip_octets2,$ip_octets1,"in-addr.arpa";
$ptr = "\'$ptr\'";
print DEBUG "ptr: $ptr\n\n";


        # check for whether the IP already exist in reverse
        my $sth1 = $dbh->prepare ("SELECT count(last_oct) FROM reverse WHERE last_oct ilike $ip_octets4 and ptrrec ilike $ptr
and accno ilike $accno and status = 1");
        $sth1->execute();
        my $r3 = $sth1->fetchall_arrayref;

        if ($r3->[0]->[0] < 1) {
        print DEBUG "NO MATCH found\n";
        my $retString = 4;
	#CMS_DB::disconnect_from_database;
        return $retString;
        } else {
        print DEBUG "MATCH found\n";
        my $retString = 3;
	#CMS_DB::disconnect_from_database;
        return $retString;
        }
close (DEBUG);
}

open (DBG, ">/tmp/view_rev");
print DBG "CHECK1\n";

sub view_dns_reverse {

print DBG "CHECK2\n";
open STDOUT, ">C:/Users/shruti_s03/Desktop/out1.txt";# added by shruti
my $accno = $dbh->quote(shift @_);
#print $accno;
print DBG "CHECK3 accno: $accno\n";
my $sth = $dbh->prepare
("SELECT distinct accno, contact, ptrrec,rec_type,fqdn,last_oct FROM reverse WHERE accno = $accno and status = 1 and fqdn != 'ns03.telstraglobal.net.' and fqdn != 'ns04.telstraglobal.net.' and fqdn != 'unknown.telstraglobal.net.' and fqdn != 'ns03.customer.ap.net.reach.com.' and fqdn != 'ns04.customer.ap.net.reach.com.'  and fqdn != 'static.telstraglobal.net.' ORDER BY accno, contact,ptrrec,rec_type,fqdn,last_oct"); 

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
       my $result = $sth->fetchall_arrayref;
		#print $result;
		print $result->[0]->[0] ;
close STDOUT;
	print DBG "result: $result->[0]->[0] \n";
        $error = $dbh->errstr;
	close (DBG);
        return $result;
}
	
sub reverse_cancel {
open (DEBUG, ">/tmp/rev_db_cancel");
print DEBUG "ENTERED\n";
my $uid     = $dbh->quote(shift @_);
my $accno   = $dbh->quote(shift @_);
my $contact = $dbh->quote(shift @_);
my $parm    = $dbh->quote(shift @_);
print DEBUG "before domain\nuid:$uid\taccno:$accno\tcontact:$contact\tparm:$parm\n";
#my $domain    = $dbh->(shift @_);
my $domain    = (shift @_);
$domain = $domain.".";
$domain = "\'$domain\'";
my $parm_val = $parm;
my $result = '';
print DEBUG "uid: $uid\n accno: $accno\n contact: $contact\n domain: $domain\n parm: $parm\n parm_val: $parm_val\n\n";
$parm_val =~ s/\'//g;

my @ip_octets = '';
my $ip_octet1 = '';
my $ip_octet2 = '';
my $ip_octet3 = '';
my $ip_octet4 = '';
my $last_oct = '';
my $ip_ptr = ''; 
my $ptr = '';
my @y = '';
my $z1 = '';
my $z2 = '';

if ($parm_val =~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/){
 @y = split(/\//,$parm_val);
 $z1= shift(@y);
 $z2= shift(@y);
print DEBUG "z1: $z1\n z2: $z2\n";
 @ip_octets = split(/\./,$z1);
 $ip_octet1 = shift(@ip_octets);
 $ip_octet2 = shift(@ip_octets);
 $ip_octet3 = shift(@ip_octets);
 $ip_octet4 = shift(@ip_octets);
 $last_oct = join "/",$ip_octet4,$z2;
 $ip_ptr = join ".",$ip_octet3,$ip_octet2,$ip_octet1;
 $ptr = join ".", $ip_ptr,"in-addr.arpa";
}
elsif ($parm_val =~ /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/){
 @ip_octets = split(/\./,$parm_val);
 $ip_octet1 = shift(@ip_octets);
 $ip_octet2 = shift(@ip_octets);
 $ip_octet3 = shift(@ip_octets);
 $ip_octet4 = shift(@ip_octets);
 $last_oct = $ip_octet4;
 $ip_ptr = join ".",$ip_octet3,$ip_octet2,$ip_octet1;
 $ptr = join ".", $ip_ptr,"in-addr.arpa";
}

$ptr = "\'$ptr\'";
$z1 = "\'$z1\'";
$ip_octet4 = "\'$ip_octet4\'";
$z2 = "\'$z2\'";
$last_oct = "\'$last_oct\'";

print DEBUG "ptr:$ptr\tz1:$z1\tip_octet4:$ip_octet4\tz2:$z2\tlast_oct:$last_oct\n";
# Users owho have requested for DNS Service are only allowed to cancel the Service.

#my $sth = $dbh->prepare ("SELECT count(p.accno) FROM  reverse p, olss_cust_access_acc o where o.accno = p.accno");
my $sth = $dbh->prepare ("SELECT count(p.accno) FROM  reverse p, olss_cust_access_acc o where o.accno = p.accno and p.accno Like $accno");
#my $sth = $dbh->prepare ("SELECT count(p.username) FROM  reverse p, olss_cust_access_acc o where o.accno = p.accno and p.username ilike $uid");
$sth->execute();
my $r1 = $sth->fetchall_arrayref;
open STDOUT, ">C:/Perl/output.txt";                		
#print $res;
print $r1->[0]->[0];
#print $accno;
close STDOUT;
if($r1->[0]->[0] >= 1) {
	my $sth1 = $dbh->prepare ("SELECT count(last_oct) from reverse where last_oct like $last_oct and ptrrec like $ptr and fqdn Like $domain");
	$sth1->execute();
        my $r2 = $sth1->fetchall_arrayref;
	print DEBUG "DB r2:$r2\n";
	print DEBUG "DB inside r1 loop\n";
        if($r2->[0]->[0] >= 1){
		my $success = 1;
		#$sth = $dbh->prepare("UPDATE reverse SET status = 3, request_time = CURRENT_TIMESTAMP  WHERE ptrrec like $ptr and last_oct like $last_oct and accno like $accno and username like $uid"); 
		$sth = $dbh->prepare("UPDATE reverse SET status = 3, request_time = CURRENT_TIMESTAMP  WHERE ptrrec like $ptr and fqdn like $domain and last_oct like $last_oct and accno like $accno "); 
 		$success &&= $sth->execute;
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
		print HKG "error: $error\n";
        	$result = ($success ? $dbh->commit : $dbh->rollback);
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
		print HKG "error: $error\t result: $result\n";
		print HKG "OUTSIDE1 result: $result\n ";
        	return $error if ($error);
        	#return $result;
		return $success;
	}else{
	$result = 0;
	print HKG "OUTSIDE2 result: $result\n ";
	return $result;
	}
} else {
$result = "User $uid does not have the rights to cancel the REVERSE Service for this IP Block\n";;
print DEBUG "OUTSIDE2 result: $result\n ";
return $result;
}
close (DEBUG);
}

sub rev_range {
my $val = @_;
my $range = 0;
if ($val ==24) {
                $range = 255;
        }elsif ($val == 25) {
                 $range = 127;
        }elsif ($val == 26) {
                 $range = 63;
        }elsif ($val == 27 ){
                 $range = 31;
        }elsif ($val == 28 ){
                 $range = 15;
        }elsif ($val == 29 ){
                 $range = 7;
        }elsif ($val == 30 ){
                 $range = 3;
        }elsif ($val == 31 ){
                 $range = 1;
        }elsif ($val == 32 ){
                 $range = 0;
        }
return $range;
}

sub sel_ptr1{
open (HKG, ">/tmp/rev_ptr");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my ($ptr) = $dbh->quote(shift @_); 
my $res = '';
my $temp = "SELECT count(last_oct) from reverse where ptrrec like $ptr";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub sel_ptr{
open (HKG, ">/tmp/rev_sel");
my ($result) = $dbh->quote(shift @_);
print HKG "result:$result";
my $res = '';
my $sth = $dbh->prepare("SELECT ptrrec,last_oct,rec_type,fqdn,request_time,refresh_time,status from reverse where ptrrec like $result and (status =1 or status =3)  and fqdn not like 'ns03.telstraglobal.net.' and fqdn not like 'ns04.telstraglobal.net.' and fqdn not like 'unknown.telstraglobal.net.' and fqdn not like 'static.telstraglobal.net.' order by last_oct");
$sth->execute();
$res = $sth->fetchall_arrayref();
print HKG "result2:$res\n";
return $res;
close (HKG);
}

sub lastoct {
open (HKG, ">/tmp/lastoct");
my ($ptr) = $dbh->quote(shift @_);
my ($last_oct) = $dbh->quote(shift @_);
my $temp = "SELECT distinct ptrrec,last_oct from reverse where ptrrec like $ptr and last_oct like $last_oct and status = 1";
print HKG "temp:$temp\n";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub request_t {
open (HKG, ">/tmp/request");
print HKG "ENTERED\n";
my $ptr1 =  '';
my $ptr1 =  @_[0];
#my ($ptr) = $dbh->quote(shift @_);
print HKG "ptr:$ptr1\n";
#my $temp = "SELECT request_time from reverse where ptrrec like $ptr and status = 1 and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'";
my $temp = "SELECT request_time from reverse where ptrrec like '$ptr1' and status = 1 and fqdn not like 'ns03.telstraglobal.net.' and fqdn not like 'ns04.telstraglobal.net.' and fqdn not like 'unknown.telstraglobal.net.' and fqdn not like 'static.telstraglobal.net.'";
print HKG "temp:$temp\n";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}


sub rev_name{
open (HKG, ">/tmp/rev_name");
print HKG "ENTERED\n";
my ($ptr) = $dbh->quote(shift @_);
my ($req_time) = $dbh->quote(shift @_);
print HKG "req_time:$req_time\n";
my $temp = "SELECT distinct(fqdn) from reverse where ptrrec like $ptr and status = 1 and request_time like $req_time and rec_type like 'NS' and fqdn not like 'ns03.telstraglobal.net.' and fqdn not like 'ns04.telstraglobal.net.' and fqdn not like 'unknown.telstraglobal.net.' and fqdn not like 'static.telstraglobal.net.'";
print HKG "temp:$temp\n";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}



sub rev_req_time {
open (HKG, ">/tmp/rev_req_time");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my ($ptr) = $dbh->quote(shift @_);
print HKG "ptr:$ptr\n";
my $temp = "SELECT request_time from reverse where ptrrec like $ptr and status = 1 and fqdn not ilike 'ns03.telstraglobal.net.' and fqdn not ilike 'ns04.telstraglobal.net.' and fqdn not ilike 'unknown.telstraglobal.net.' and fqdn not ilike 'static.telstraglobal.net.'";
print HKG "temp:$temp\n";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub all_ptr {
open (HKG, ">/tmp/all_ptr");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $temp = "SELECT distinct ptrrec from reverse ";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}


sub time_db {
open (HKG, ">/tmp/revdns_time");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $temp = "SELECT request_time from reverse";
my $sth = $dbh->prepare ("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub all_values {
open (HKG, ">/tmp/revdns_all");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $temp = "SELECT ptrrec,last_oct,rec_type,fqdn from reverse where status =1";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub count_ptr {
open (HKG, ">/tmp/revdns_count");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $ptr = $dbh->quote (shift @_);
print HKG "ptr:$ptr\n";
my $temp = "SELECT count(last_oct) from reverse where ptrrec like $ptr and status = 1";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub values_db {
open (HKG, ">/tmp/revdns_vald");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $ptr = $dbh->quote (shift @_);
print HKG "ptr:$ptr\n";
my $temp = "SELECT request_time from reverse where ptrrec like $ptr and fqdn not like 'ns03.telstraglobal.net.' and fqdn not like 'ns04.telstraglobal.net.' and fqdn not like 'unknown.telstraglobal.net.' and fqdn not like 'static.telstraglobal.net.' and request_time is not null order by request_time desc";
print HKG "temp:$temp\n";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub maildetails {
open (HKG, ">/tmp/mail_details");
print HKG "ENTERED\n";
CMS_DB::begin_transaction();
my $req_time = $dbh->quote (shift @_);
my $temp = "SELECT ptrrec,last_oct,rec_type,fqdn,status,contact,accno, username from reverse where request_time like $req_time";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}
 


sub binary_add {#binary AND of ip blocks
my @z1_octets = "";
my $z1_octet1 = "";
my $z1_octet2 = "";
my $z1_octet3 = "";
my $z1_octet4 = "";
my $z1_bin_1 = "";
my $z1_bin_2 = "";
my $z1_bin_3 = "";
my $z1_bin_4 = "";
my @z1_binary = "";
my $z1_binary_stat = "";
my @z1_binary_bit = "";
my @ip_octets = "";
my $ip_octet1 = "";
my $ip_octet2 = "";
my $ip_octet3 = "";
my $ip_octet4 = "";
my $ip_bin_1 = "";
my $ip_bin_2 = "";
my $ip_bin_3 = "";
my $ip_bin_4 = "";
my @ip_binary = "";
my $ip_binary_stat = "";
my @ip_binary_bit = "";
my $ip_length = "";
my $ip_binary_result = "";
my $z1_binary_result = "";
my $loop_count2 = 0;
my $ipprefix = "";
my $compare_flag=0;
my $db_flag=0;
my ($parm_val,$z1,$z2) = @_;

my @ip_chk1 = split(/\//,$parm_val);
my $ipnew_chk1  = shift(@ip_chk1);#ipnew_chk has ip
my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
my @ip_octets = split(/\./,$ipnew_chk1);
#code to split the stored IPs into octets and changing them to binary
$ip_octet1 = shift(@ip_octets);
$ip_octet2 = shift(@ip_octets);
$ip_octet3 = shift(@ip_octets);
$ip_octet4 = shift(@ip_octets);

$ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
$ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
$ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
$ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

#combining individual IPs into one variable/array
my @ip_binary = ();
push(@ip_binary, $ip_bin_1);
push(@ip_binary, $ip_bin_2);
push(@ip_binary, $ip_bin_3);
push(@ip_binary, $ip_bin_4);
$ip_binary_stat = join("",@ip_binary);
@ip_binary_bit = split(//,$ip_binary_stat);
#print"ip binary is @ip_binary_bit\n";

#code to split the entered IPs into octets and changing octets to binary.
        @z1_octets = split(/\./,$z1);
        $z1_octet1 = shift(@z1_octets);
        $z1_octet2 = shift(@z1_octets);
        $z1_octet3 = shift(@z1_octets);
        $z1_octet4 = shift(@z1_octets);

        $z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
        $z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
        $z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
        $z1_bin_4 = unpack("B8", pack('C',$z1_octet4));

#combining the individual IPs into one variable/array
        @z1_binary = ();
        push(@z1_binary,$z1_bin_1);
        push(@z1_binary,$z1_bin_2);
        push(@z1_binary,$z1_bin_3);
        push(@z1_binary,$z1_bin_4);
        $z1_binary_stat = join("",@z1_binary);
        @z1_binary_bit = split(//,$z1_binary_stat);
        print DEBUG "input is @z1_binary_bit\n";
        $ipprefix = $ipprefixip_chk1;
if($z2 > $ipprefix){$ip_length = $ipprefix;}
 else {$ip_length = $z2;}
                print DEBUG "ip length value:$ip_length\n";
                $compare_flag = 1;
                for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
                print DEBUG "entered for condition\n";
                        if ($db_flag == 0){
                        print DEBUG "DB entered if compare flag condition\n";
                        $z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
                        $ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
                                if ($z1_binary_result != $ip_binary_result){
                                print DEBUG "DB binary ADD false\n";
                                print DEBUG "DB db flag 1\n";
                                $db_flag = 1;
                                }
                                elsif ($z1_binary_result == $ip_binary_result)
                                {
                                print DEBUG "DB equal to binary add\n";
                                print DEBUG "DB db flag 0\n";
                                $db_flag = 0;}
                        }
                }#end of both if loops and for loop
        if ($db_flag == 1 && $compare_flag ==1)
                {
                print DEBUG "DB compare flag 1\n";
                $compare_flag = 1;}
        else
                {
                print DEBUG "DB compare flag 0\n";
                $compare_flag = 0;}
return $compare_flag;
}

# Get reverse request types for View
# Prost: $result
sub get_rev_view {
open (DEBUG,">/tmp/rev_mng_map");
#my $temp_sql = "SELECT rev_type,rev_id FROM reverse_type ORDER BY rev_type,rev_id";
my $sth1 = $dbh->prepare ("SELECT rev_type,rev_id FROM reverse_type ORDER BY rev_type,rev_id");
#my $sth1 = $dbh->prepare ("$temp_sql");
print DEBUG "sth:$sth1\n";
$sth1->execute();
my $result = $sth1->fetchall_arrayref;
print DEBUG "result:$result\n";
$error = $dbh->errstr;
print DEBUG "error:$error\n";
return $result;
close DEBUG;
}

# Get reverse request types
# Pre  : $search_by (0=none, 1=type_name, 2=type_id), $val
# Prost: $result
sub get_reverse_type{
CMS_DB::begin_transaction();
open (DEBUG,">/tmp/rev_mng_map");
my ($search_by, $val) = @_;
print DEBUG "search_by:$search_by\tval:$val\n";

$search_by = 0 if ($search_by eq '');
my $cond = ' WHERE ';
my $temp_sql = '';

#if ($search_by == 1) {
#print DEBUG "search by 1\n";
#       $cond .= " type ilike '$val' ";
#$temp_sql = "SELECT rev_type,rev_id FROM reverse_type $cond ORDER BY rev_type,rev_id";
if ($search_by == 2) {
print DEBUG "search by 2\n";
        my $x = $val + 0;
        $cond .= " rev_id = $x ";
$temp_sql = "SELECT rev_type,rev_id FROM reverse_type $cond ORDER BY rev_type,rev_id";
}

print DEBUG "cond:$cond\n";
print DEBUG "SELECT rev_type,rev_id FROM reverse_type $cond ORDER BY rev_type,rev_id\n";
#my $temp_sql = "SELECT rev_type,rev_id FROM reverse_type $cond ORDER BY rev_type,rev_id";
#my $sth = $dbh->prepare ("SELECT rev_type,rev_id FROM reverse_type $cond ORDER BY rev_type,rev_id");
my $sth1 = $dbh->prepare ("$temp_sql");
print DEBUG "sth:$sth1\n";
#$unless ($sth->execute) {
        #$error = $dbh->errstr;

#print DEBUG "error:$error\n";
        #return 0;
        #}
$sth1->execute();
my $result = $sth1->fetchall_arrayref;
print DEBUG "result:$result\n";
$error = $dbh->errstr;
print DEBUG "error:$error\n";
return $result;
close DEBUG;
#CMS_DB::disconnect_from_transaction();
}
1;

