package com.telstra.netops.lookingglass.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.telstra.netops.lookingglass.server.exception.LookingClassApplicationInternalException;
import com.telstra.netops.lookingglass.server.model.ClientNodeTestRequest;
import com.telstra.netops.lookingglass.server.model.NodeTestResult;
import com.telstra.netops.lookingglass.server.model.NodeTraceTestRequest;
import com.telstra.netops.lookingglass.server.repository.NodeRepository;
import com.telstra.netops.lookingglass.server.util.JsonSerializerDeserializer;
import com.telstra.oauth2.client.OAuth2TokenResolver;

@Service
@Profile("!local")
public class CgiNodeTestService implements NodeTestService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CgiNodeTestService.class);

	@Value("${lookingglass.cgi.url}")
	private String cgiURL;

	@Autowired
	private NodeRepository nodeRepository;

	@Autowired
	private JsonSerializerDeserializer jsonSerializerDeserializer;
	
	@Autowired
    private OAuth2TokenResolver tokenResolver;

	@Override
	public NodeTestResult performNodeTest(ClientNodeTestRequest request) {
		if (request.getSourceNode() == null) {
			LookingClassApplicationInternalException lge = new LookingClassApplicationInternalException(
					"no source node specified!");
			LOGGER.error("can't perform node test", lge);
			throw lge;
		}

		ResponseEntity<NodeTestResult> responseEntity = request(request, true);

		if (!HttpStatus.OK.equals(responseEntity.getStatusCode())) {
			LookingClassApplicationInternalException lge = new LookingClassApplicationInternalException(
					"node test failed with http status code: " + responseEntity.getStatusCode());
			LOGGER.error("can't perform node test", lge);
			throw lge;
		}

		return responseEntity.getBody();
	}

	@Override
	public NodeTestResult performNodeHostTest(ClientNodeTestRequest request) {
		if (request.getSubmit() == null) {
			LookingClassApplicationInternalException lge = new LookingClassApplicationInternalException(
					"Invalid or no Inputs!");
			LOGGER.error("can't perform node test", lge);
			throw lge;
		}

		ResponseEntity<NodeTestResult> responseEntity = request(request, false);

		if (!HttpStatus.OK.equals(responseEntity.getStatusCode())) {
			LookingClassApplicationInternalException lge = new LookingClassApplicationInternalException(
					"node host test failed with http status code: " + responseEntity.getStatusCode());
			LOGGER.error("can't perform node host test", lge);
			throw lge;
		}

		return responseEntity.getBody();
	}

	private ResponseEntity<NodeTestResult> request(ClientNodeTestRequest request, boolean nodeTest) {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.add(getAuthorizationHeaderName(), getAuthorizationHeaderValue());
		
		NodeTraceTestRequest nodeTraceTestRequest = null;
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		if (!nodeTest) {
			nodeTraceTestRequest = new NodeTraceTestRequest(String.valueOf(request.getType().getCode()),
					request.getCustomerId(), request.getProduct(), getIpAddress(request.getSourceNode()),
					request.getTargetNode(), request.getSubmit(), request.getNumPings());
		} else {

			nodeTraceTestRequest = new NodeTraceTestRequest(String.valueOf(request.getType().getCode()),
					request.getCustomerId(), request.getProduct(), getIpAddress(request.getSourceNode()),
					getIpAddress(request.getTargetNode()), request.getSubmit(), request.getNumPings());
		}

		map.add("POSTDATA", jsonSerializerDeserializer.serialize(nodeTraceTestRequest));

		HttpEntity<MultiValueMap<String, Object>> httpRequest = new HttpEntity<MultiValueMap<String, Object>>(map,
				headers);

		ResponseEntity<NodeTestResult> responseEntity = restTemplate.postForEntity(cgiURL, httpRequest,
				NodeTestResult.class);
		return responseEntity;
	}

	private String getIpAddress(Long nodeID) {
		return nodeRepository.findOne(nodeID).getIpAddress();
	}
	
	private String getIpAddress(String nodeID) {
		return nodeRepository.findOne(Long.valueOf(nodeID)).getIpAddress();
	}
	
	private String getAuthorizationHeaderName() {
		return tokenResolver.getAuthorizationHeaderName();
	}
	
	private String getAuthorizationHeaderValue() {
		return tokenResolver.getAuthorizationHeaderValue();
	}

}
