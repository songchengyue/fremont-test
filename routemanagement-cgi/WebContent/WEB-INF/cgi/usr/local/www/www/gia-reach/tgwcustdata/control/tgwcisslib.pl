#!/usr/local/bin/perl -X

#
# TGW Custdata library file
#
# $Id: tgwcisslib.pl,v 1.7 2004/01/15 11:06:57 rchew Exp $
#
# $Log: tgwcisslib.pl,v $
# Revision 1.7  2004/01/15 11:06:57  rchew
# After merging changes from production OLSS
#
# Revision 1.6  2003/12/09 12:19:27  rchew
# Changed as per UAT list 1
#
# Revision 1.5  2003/11/26 13:11:14  rchew
# Flow through in local templates now fixed!
#
# Revision 1.4  2003/11/10 13:34:43  rchew
# Updated if access through eportal, do not allow password change
#
# Revision 1.3  2003/11/10 12:48:55  rchew
# R.1 - OLSS Single-sign on with new branding templates
#
# Revision 1.2  2003/10/28 13:22:39  rchew
# Lots of single sign-on changes and tested
#
# Revision 1.1.1.1  2003/10/15 12:15:19  rchew
# Actual OLSS distribution that needs to be installed in /usr/local/www/www/gia-reach
#
# Revision 1.1.1.1  2003/10/15 12:12:17  rchew
# OLSS
#
###################################################################
# File        : tgwcisslib.pl
# Description : the library file which has common functions for OLSS
# Output of the script
# Input File  : 
# Output File :
# Author      : PRADEEP THANRAJ (Infosys Tech. Ltd., Bangalore )
# Started On  : 07 July 2009
# Modification History :
#  Date               Name             Change/Description
# ------------------------------------------------------------------
# 07Jul2009    	     Pradeep			Baselined for EVPL
# 07Jul2009    	     Pradeep			changes for EVPL
# 02Feb2010          Chandini                   Baselined for EPL
# 14May2010          Radhika                    Changes for VLL
# 01Jun2010          Radhika                    Changes for VPLS 
#                                               Graphs label alignment
# 13Sep2010          Chandini                   Baselined for IP-Transit
####################################################################

#require "ctime.pl";
use OS_PARA;
use lib "$OS_PARA::values{cms_modules}{value}";
use lib "$OS_PARA::values{olss_modules}{value}";
use lib "$OS_PARA::values{cms_common_modules}{value}";

use IO::Socket::INET;
use OLSS_PLOT;
use CMS_OLSS;
use Date::Parse;
use Net::IP;
use CMS_DB;
use CMS_NTP_DB;
#####use CMS;
use CMS_SECDNS;
use CMS_SECDNS_DB;
use CMS_SERVICE_DB;
use CMS_PRIDNS_DB;
#Added by Leo;
use COOKIE;
use CMS_CUSTOMER_DB;
#Added by Matthew;
use Time::Local;
#use Date::Calc qw(Add_Delta_Days);
use OS_REPORTS_DB;
#use LWP::UserAgent;
#use HTTP::Cookies;
#Chandini_EVPL
use POSIX;
#use Date::Manip;
#CR-84 Juniper IPVPN Report
use CGI qw(param);
use CGI ':standard';


#require "OLSS_UM.pl";

# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;

our $value_type = '';
$| = 1;

##### General variables ####
# Root path
#$tgwr = "/usr/local/www/www/gia-reach/tgwcustdata/";
$tgwr = "$OS_PARA::values{tgwrpath}{value}";

# Wholesaler name mappings
%whlrealname = ("extant", "Dynegy Connect");

#
$rhost = 1;
#$realhost = "www.net.reach.com" if($rhost);
#$realhost = "$OS_PARA::values{inthostname}{value}" if($rhost);
if($rhost) {
	$realhost = "$OS_PARA::values{inthostname}{value}";
	if ($ENV{'SERVER_NAME'} =~ /^(la-|hk-)/i) {
		$realhost = $1.$realhost;
	}
}
$devhost = "gomer.dev.in.reach.com";

# Account Wolesalers
%wholesalers = ('MASTER-100418' => 'Telstra', '100418' => 'Telstra', '100200' => 'Telstra', '100302' => 'Telstra', 'W0004' => 'PCCW', '100084' => 'PCCW', '100221' => 'PCCW', '110002' => 'PCCW', 'REACH' => 'Telstra');

#

# Database definitions
$ripenet = "/ripe/nets/";
$dbroot = $tgwr."dbase/";
$opsdb = $dbroot."opsdb";
$fnndb = $dbroot."fnndb";
$shdb = $dbroot."shdb";
$contactdb = $dbroot."contactdb";
$techcdb = $dbroot."techcdb";
$whlattrdb = $dbroot."whlattrdb";
# Ancillary databases
$routedb = $dbroot."routedb";
$secmxdb = $dbroot."secmxdb";
$secdnsdb = $dbroot."secdnsdb";
$curr_capacitydb = $dbroot."curr_capacitydb";
$pending_queuedb = $dbroot."pending_queuedb";
$svcproductdb = $dbroot."svcproduct";
$svclabeldb = $dbroot."svclabel";
$atmlatencyperf = $dbroot."ATM/latencyperf/";
$atmcablepath = $dbroot."ATM/cablepath/"; 
$frcablepath = $dbroot."FR/cablepath/";
$mpls_netperf_db = $dbroot."MPLS/";
################### VPLS TR START ####################
$vpls_netperf_db = $dbroot."VPLS/";
#################### VPLS TR END #####################
# Reseller flag
$resell = 0;
#CR-84 Juniper IPVPN Report
$junos = 0;

# General dependency files
$sl = $tgwr."tmp/allsites.list";
$locksl = $tgwr."tmp/allsites.list.lock";
$siteslist = $tgwr."system-files/sites.list";
$ripealias = "/mnt/snm/aarnet/filters/filtaliases";
$filtalias = "/mnt/snm/aarnet/filters/filtaliases";
$statfile = $tgwr."system-files/ti-ops-contacts";
$ctdir = "/control/";
$ctrlhtml = "control.file";  
$sctrlhtml = "scontrol.file";
$servicectrlhtml = "servicectrl.file";
# Different vals for Extant vs GW
$ctrlpath = $tgwr."control/";
$pgmctrl = "pgm-int.file";
$refctrl = "reference.control";
#RKB#$popfile = "$tgwr/control/POP";
#RKB#$popfile_juniper = "$tgwr/control/Juniper_POP";
$popfile_juniper_VPLS = "$tgwr/control/Juniper_POP_VPLS";
$popfile_GIA = "$tgwr/control/POP_GIA";
$popfile_IPVPN = "$tgwr/control/POP_IPVPN";
$popfile_IPTRANSIT = "$tgwr/control/POP_IPTRANSIT";
$popfile_juniper_EVPL = "$tgwr/control/Juniper_POP_EVPL";
$CLIcfgpath = $ctrlpath."conf/CLI/";
$cityCode_cfg = "/usr/local/www/wanchai/cms/data/poller/locationCode.list";
$lwpcookies = "/tmp/cookies.lwp";
#Chandini_mVPN_CLI
$routerlog_file = "/usr/local/www/www/gia-reach/tgwcustdata/control/conf/CLI/router_mvpn.list";

# tmp dir
$tmpdir = $tgwr."tmp/";

# General dependency dirs
$gifloc = "/friends/linkrptdata/";

# External utilities
$crypt = "/usr/local/bin/crypt";
$mailagent = "/usr/sbin/sendmail -t";

# Other definitions
$root = "root";
$summarygif = "sum-";
$riperoot = $tgwr."nets/";
$lockopscondb = $tgwr."dbase/ops2contact.lock";
$ltgw = "Telstra Global Wholesale";
$errortmpl = "error-lnk.htm";

# Reseller logo
$rlogo_dir = "/tgwcustdata/images/reach-reseller/images/upload/";

$ipt_socket_errorgif = "/tgwcustdata/control/gifs/ipt_socket_errormsg.gif";

# Default GIF fail file
$failgif = $tgwr."control/gifs/nograph.gif";
#Chandini_TE_Tunnel
$mpls_tetunnel_failgif = $tgwr."control/gifs/mpls_tetunnel_errormsg.gif";
#Chandini_IPT
#Error message to be displayed when no data is there or socket connection is not possible
$ipt_graph_errorgif = $tgwr."control/gifs/ipt_graph_errormsg.gif";
#$ipt_graph_errorgif =  "/tgwcustdata/control/gifs/ipt_graph_errormsg.gif";
#$ipt_socket_errorgif =  $tgwr."control/gifs/ipt_socket_errormsg.gif";
# LOG Files
$custlogfile = $tgwr."logs/custinfo-log";
$chaliaslogfile = $tgwr."logs/chalias.log";
$chroutelog = $tgwr."logs/chroute.log";
$chpasslog = $tgwr."logs/chpass.log";
$chdnslog = $tgwr."logs/secdns.log";
$chmxlog = $tgwr."logs/secmx.log";
$chnewslog = $tgwr."logs/usenet.log";
$chbwlog = $tgwr."logs/bwmanager.log";
$logtt = $tgwr."logs/troubleticket.log";
$internalerror = $tgwr."logs/errorlogs.log";
$chsvclabellog = $tgwr."logs/chsvclabel.log";
$CLIlog_dir = $tgwr."logs/CLI/";
$chpdnslog = $tgwr."logs/pridns.log";

# Control Custom Flag Definitions
$s_contacts = "SERVICECONTACTS";
$acc_details = "ACCOUNTDETAILS";
$log_fault = "LOGFAULT";

# Generic script names
$loginscript = "tgwciss-login.cgi"; 
$servicescript = "tgwciss-service.cgi";
$reportscript = "tgwciss-reports.cgi";
$lookglassscript = "tgwciss-lg.cgi";
$mngservscript = "tgwciss-mng.cgi";
$contactscript = "tgwciss-contactus.cgi";
$accscript = "tgwciss-accadmin.cgi";
#Added by Leo;
$termsscript = "tgwciss-termsofuse.cgi";

# Generic template names
$servicectrl = "service/service.htm";
$reportctrl = "reports/reports.htm";
$lgctrl = "looking_glass/looking.htm";
$mngservctrl = "manage_services/manage_services.htm";
$contactctrl = "contactus/contactus.htm";
$accountctrl = "account/account.htm";
#Added by Leo;
$termsctrl = "termsofuse/termsofuse.htm";
$giatermsctrl = "termsofuse/gia_termsofuse.htm";
@giaterms_ctrl = ("../termsofuse/gia_termsofuse1.htm","../termsofuse/gia_termsofuse2.htm","../termsofuse/gia_termsofuse3.htm","../termsofuse/gia_termsofuse4.htm","../termsofuse/gia_termsofuse5.htm");

# Trouble Ticket Email contacts - GHD and GSMC
$ttemail = "cs\@reach.com";

# Routing queue contacts
$routeupdateemail = "cs\@reach.com, provision\@reach.com";

# Service templates
@svc = ( "routing.htm", "secondaryDNS.htm", "secondarymail.htm",
	 "newsfeeds.htm", "bgpPolicy.htm", "bandwidth.htm", "CLI.htm", "gxrv.htm");
#Chandini_EPL
#Added EPL Point to Multipoint template
# Reports templates, sla.htm added
# Sayantan_IPT_Ph_3: 29-Jul-2010 - Start
#Added bursttraffic.htm
#Added China Metering templates - trafficrpts_china.htm
@rpts = ( "reports.htm", "traffic.htm", "network.htm", "outage.htm", "sla.htm", "site2site.htm", "latency.htm",
          "cablepaths.htm", "tunneltraffic.htm","core_qos_report.htm","core_qos_page.htm","traffic_mepl.htm","bursttraffic.htm","traffic_china.htm","trafficrpts_china.htm","trafficrpts_china_wholesaler.htm");
# Sayantan_IPT_Ph_3: 29-Jul-2010 - End
# Looking glass templates
@lgt = ("hosttest.htm", "hosttrace.htm", "nodetest.htm");

#IPT_ph5
# Manage your services
@mst = ("dnsres.htm", "primary.htm", "reverse.htm", "mainDNS.htm", "secondary.htm", "NTP.htm", "routing.htm");

# Contact us templates
@cut = ("general.htm", "raise.htm");

# Account admin templates - local only
# @accadmint = ("chpass.htm", "statsc.htm", "statsc2.htm", "techc.htm");
# Modified by Leo on 2008-11-05
@accadmint = ("chpass.htm", "statsc.htm", "pridns.htm", "statsc2.htm", "techc.htm"); 
#@accadmint = ("pridns.htm"); 

# Server offerings
@server_services = ("Secondary DNS", "Secondary MX", "USENET News",
			"Routing", "Bandwidth Manager");

# Test account - onlineservice
$testacc = "onlineservice";

# Graph generation definitions
$gnuplot = "/usr/local/bin/gnuplot";
$ppmtogif = "/usr/local/bin/ppmtogif";
$ctlfile = "/tmp/ctlfile.sh.$$";
$ppmfile = "/tmp/ppm.$$";
$giffile = "/tmp/gif.$$";

# Timeout period
$maxtime = 50 * 60; # 50 minutes

$tra_rep_host = $OS_PARA::values{traffic_report_host}{value};
$gia_net_rep_host = $OS_PARA::values{gia_network_report_host}{value};
$mpls_net_rep_host = $OS_PARA::values{mpls_network_report_host}{value};
################### VPLS TR START ####################
$vpls_net_rep_host = $OS_PARA::values{vpls_network_report_host}{value};
#################### VPLS TR END #####################
$per_rep_host = $OS_PARA::values{performance_report_host}{value};
$poller_host = $OS_PARA::values{poller_value}{value};
$telstra_saa_host = $OS_PARA::values{telstra_saa_host}{value};

#
# Maettr - 3/11/2006 - 6 COS labels

%telstra_cos_label = (
	class0 => "Low Priority Data",
	class1 => "Standard Data",
	class2 => "Interactive Data",
	class3 => "Critical Data",
	class4 => "Video",
	class5 => "Voice" );

%pccw_cos_label = (
	class0 => "Bronze",
	class1 => "Bronze Plus",
        class2 => "Bronze Enhanced",
        class3 => "Silver",
        class4 => "Silver Plus",
        class5 => "Gold" );
#++mVPN
%telstra_cos_label_mVPN = (
        class0 => "Multicast Low Priority Data",
        class1 => "Multicast Standard Data",
        class2 => "Multicast Interactive Data",
        class3 => "Multicast Critical Data",
        class4 => "Multicast Video",
        class5 => "Multicast Voice" );
#--mVPN

#priyanka debugging 25july2013
 open (MYFILE, '>>/home/t700682/priyanka_log_25july2013.txt');
 print MYFILE "in tgwcisslib.pl\n";
 close (MYFILE);
open(DEBUG,">/data1/tmp_log/log_27072013");
                         print DEBUG " **in tgwcisslib.pl**\n";
                         close (DEBUG);

################### VPLS TR START ####################  
%vpls_cos_label = (
	class0 => "Low Priority Data",
        class1 => "Standard Data",
        class2 => "Interactive Data",
        class3 => "Critical Data",
        class4 => "Video",
        class5 => "Voice"); 
	#vtm => "VLP Trans Mode");        
#################### VPLS TR END #####################

################### EVPL TR START ####################
%evpl_cos_label = (
        class0 => "Standard",
        class3 => "Premium");
#################### EVPL TR END #####################

################### IPTRANSIT START ####################
%iptransit_cos_label = (
        class0 => "Silver",
        class2 => "Platinum");
#################### IPTRANSIT  END #####################


our $master_srv;
our $log_ser;
our $agg_serv;
our $p_t;
our $master_lb;
our $log_lb;
our $agg_lb;
our $maxarrsize;
our @Firstkeys;
our $disp_rept;
#Ch_mVP
#declared golbal variable
our $graph_error;
#Chandini_TE_Tunnel
our $mpls_tetunnel_errormsg =0;
#### End General Variables ####

##Added By Karuna on 16-Nov-2010 for IPT5
my $debug = 1;
#To disable debug logs, uncomment the following line and vice versa
#$debug = 0;
if ($debug){open (IPT_RM, ">/data1/tmp_log/tgwcisslib.log");}

#### Database Ops ####


sub check_contract_status {
	my ($customer) = @_;

	CMS_DB::connect_to_database;
        my $status = CMS_CUSTOMER_DB::get_contract_status($customer);

	if ($$status[0] eq "t" || $$status[0] eq "1") {
		return "t";
	} else {
		return "f";
	}

	CMS_DB::disconnect_from_database;
	
}

sub get_router_os_looking_glass {
	my $router = shift @_;
	CMS_DB::connect_to_database;
	my $result = CMS_CUSTOMER_DB::get_router_os_looking_glass($router);
	CMS_DB::disconnect_from_database;
	return $result;
}

# Log function to log files
# Pre: $logfilename, $action, $msg, $srcip
# Post: 0 (fail), 1 (ok)
sub log_entry {
	my($logfilename, $action, $msg, $srcip) = @_;

	my $date = gmtime(time);
	chomp($date);

	# if srcip not passed - use the our defined $custfrom set at the calling script
	if(not defined $srcip) {
		$srcip = $custfrom;
	}

	chomp($msg);

#priyanka debugging 25july2013
 open (MYFILE, '>/home/t700682/priyanka_log_25july2013.txt');
 print MYFILE "in tgwcisslib.pl\n";
 close (MYFILE);

open(DEBUG,">/data1/tmp_log/log_27072013");
                         print DEBUG " **in tgwcisslib.pl**\n";
                         close (DEBUG);

	# temporary - will move log files into SQL soon
	my $out;
	open($out, ">>$logfilename");
	print $out "$date\t$action\t$msg\tFrom: $srcip\n";
	close($out);

	chmod(0660, $logfilename);

	return 1;

}


# Pre: handle, table, @elements
# Post: 0 (fail), 1 (ok)
sub insert {
	my($handle, $table, @vals) = @_;

	my %cc;
	my $custvals = join '^^', @vals;
	dbmopen(%cc, $table, undef) or return 0;
	$cc{$handle} = $custvals;
	dbmclose(%cc);

	return 1;
}

# Assumes @elements correct
# Pre: handle, table, @elements
# Post: 0 (fail), 1 (OK)
sub update {
	my($handle, $table, @vals) = @_;

	my %cc;
	my $custvals = join '^^', @vals;

	dbmopen(%cc, $table, undef) or return 0;
	if(not exists $cc{$handle}) {
		dbmclose(%cc);
		return 0;
	} else {
		$cc{$handle} = $custvals;
	}
	dbmclose(%cc);

	return 1;
}


# Pre: Handle, table
# Post: 0 (fail), @vals (ok)
sub retrieve {
	my ($handle, $table) = @_;

	my %cc;
	dbmopen(%cc, $table, undef) or return 0;

	if(not exists $cc{$handle}) {
		dbmclose(%cc);
		return 0;
	}
	my @x = split(/\^\^/, $cc{$handle});
	dbmclose(%cc);

	return @x;
}

# Pre: Handle, table
# Post: 0 (fail), 1 (ok)
sub delete {
	my ($handle, $table) = @_;

	my %cc;
	dbmopen(%cc, $table, undef) or return 0;
	if(not exists $cc{$handle}) {
		dbmclose(%cc);
		return 0;
	}
	delete $cc{$handle};
	dbmclose(%cc);

	return 1;
}

# Search for values
# Pre: Handle, table
# Post: 0 (fail), @vals (xx^^xx^^...)
sub search {
	my ($handle, $table) = @_;

	my %cc;
	my (@vals, $count);
	$count = 0;
	dbmopen(%cc, $table, undef) or return 0;
	foreach my $k (keys %cc) {
		if($cc{$k} =~ /(^$handle\^|\^$handle\^)/i) {
			push @vals, $cc{$k};
			$count++;
		}
	}
	dbmclose(%cc);

	return 0 if($count == 0);

	return @vals;
}

######################

##### Screen Ops #####
#open(DEBUG,">/data1/tmp_log/CHK_DNS_25052009");
# Pre: line, code, type (eg service, acc management etc), product_code, template
# Post: line
sub link_format {	# Added by Matthew - Oct, 2007
open(BUG,">/data1/tmp_log/CHK");
	my($line, $code, $type, $prodcode, $template) = @_;
print BUG `/bin/pwd`;
print "\n";
	print BUG "line: $line\n code: $code\n type: $type\n template: $template\n";
	print DEBUG "prodcode: $prodcode\n";

	if(!$prodcode) {
		$prodcode = ($encet) ? $encet : "";
	}

	my $enc = $prodcode;

	$template = (!$template) ? $tmplp : $template;

        if($line =~ /Templates\//) {
                $line =~ s/Templates\///g;
        }

	# if remask URL, remove
	if($rmask) {
		#$line =~ s/reseller-telstra\/images/images/g;
	}

	#$line =~ s/\*PRODUCT\*//g;
	$line =~ s/Reach - Resellers/Reporting/g;

	$line =~ s/http\:\/\//https\:\/\//g;
	my $e = "https://" . $realhost . "/";
	$line =~ s/\#LOGOUT\#/$e/;

	#my $cet = uc(&dcrypt($prodcode));
	my $cet = uc(CMS_OLSS::get_productname_for_productcode(uc(&dcrypt($prodcode))));
	
        if($line =~ /\*PRODUCT\*/) {
                #$line = "";
                $line =~ s/\*PRODUCT\*/$cet/g;
        }

        if($line =~ /\#USERNAME\#/) {
                my $loginid;
                my ($result, $vfg, $upass) = &decrypt($code);
                if ($result =~ /(.+)\^\^(.+)/) {
                        $loginid = $1;
                        $accno = $2;
                } else {
                        $loginid = "OSC user";
                        $accno = $result;
                }

                if($accno) {
                        $line =~ s/\#USERNAME\#/$loginid/g;
                }
        }

	if($line =~ /\#DATE\#/) {
		if($tdate) {
			my ($d, $m, $y) = (split(/\s+/, $tdate))[2,1,4];
			if(length($d) < 2) {
				$d = "0".$d;
			}
			$line =~ s/\#DATE\#/$d-$m-$y/g;
		}
	}

	# get correct script files
	if($line =~ /scripts\/mm_menu.js/) {
		#$line =~ s/\"scripts\/|\"..\/scripts\//\"https:\/\/$realhost\/scripts\//;
		$line =~ s/\"scripts\/|\"..\/scripts\//\"\/olss\/scripts\//;
	}

	#Added by Stavan to include form validations for IPtransit on 09Sept2010
	if($line =~ /scripts\/trafficFormValidator.js/) {
		#$line =~ s/\"scripts\/|\"..\/scripts\//\"https:\/\/$realhost\/scripts\//;
		$line =~ s/\"scripts\/|\"..\/scripts\//\"\/olss\/scripts\//;
	}
	if($line =~ /scripts\/jquery/) {
                #$line =~ s/\"scripts\/|\"..\/scripts\//\"https:\/\/$realhost\/scripts\//;
                $line =~ s/\"scripts\/|\"..\/scripts\//\"\/olss\/scripts\//;
        }

	if($line =~ /MAIN.htm/) {
		$line =~ s/MAIN.htm/$loginscript?code=$code&level=2/g;
	}
	

	if($line =~ /Online Service Centre/i) {
                my $ep = CMS_OLSS::access_through_EP($uid);
                if($ep !~ /true/i) {
                        $line =~ s/Online Service Centre/Online Service Support Centre/g;
                }
        }
        if($line =~ /OSC /) {
                my $ep = CMS_OLSS::access_through_EP($uid);
                if($ep !~ /true/i) {
                        $line =~ s/OSC /OLSS /g;
                }
        }

	#my $h = `/bin/hostname`;
	#my $rhost = ($h =~ /in.reach.com/i) ? 0 : 1;
	#$host = ($rhost) ? $realhost : $devhost;
	$host = $realhost;



	# reseller specific rules
		$line = &resell_link($line, $code, $host, $template, $prodcode);
		if($line =~ /images\//) {
			if($line !~ /document\.images/i && $line !~ /reseller-telstra\/images/) {
				#$line =~ s/images\//$link\/images\//g;
				my $ipsa_imglink = '/olss/tgwcustdata/images/';
                                $line =~ s#\.\./##g;
                                $line =~ s#images/#$ipsa_imglink#g;
                                #$line =~ s/images\//\/tgwcustdata\/images\//g;
                        }
                        if( $line =~ /reseller-telstra\/images/) {
                                my $ipsa_imglink = '/olss/reseller-telstra/images';
                                $line =~ s#\.\./##g;
                                $line =~ s#reseller-telstra/images#$ipsa_imglink#;
			}
		}
	if($line =~ /reseller-telstra\/js/) {
                $line =~ s#\.\./##g;
                my $ipsa_jslink = '/olss/reseller-telstra/js';
                $line =~ s#reseller-telstra/js#$ipsa_jslink#;
        }

	if($line =~ /background\=/) { #\=\"\.\.\/images\/home\/bg\.gif\"/) {
		$line =~ s/background\=\"\/tgwcustdata\/images\/home\/bg.gif\"/HH/;
		$line =~ s/HH/bgcolor=\"#ffffff\"/;
	}



        if($line =~ /\"..\/home.htm\"/) {
        	#$line =~ s/\"..\/home.htm\"/\"$loginscript\?code=$code\&level=2\"/g;
        	$line =~ s/\"..\/home.htm\"/\"$loginscript\?code=$code\&level=6&et=$prodcode\"/g;
        }
	if($line =~ /\"home.htm\"/) {
		$line =~ s/\"home.htm\"/\"$loginscript\?code=$code\&level=6&et=$prodcode\"/g;
		#$line =~ s/\"home.htm\"/\"$loginscript\?code=$code\&level=2\"/g;
        }

	 $line =~ s/\"\.\.\/manage_services\//\"/g;
	$line =~ s/\"\.\.\/service\//\"/g;
	$line =~ s/\"\.\.\/reports\//\"/g;
	$line =~ s/\"\.\.\/looking_glass\//\"/g;
	$line =~ s/\"\.\.\/contactus\//\"/g;
	$line =~ s/\"\.\.\/account\//\"/g;

	# CRQ8582 - Simiplifying path name in templates.Removing dependency of OLSS URL.
        if($line =~ /[href|src]\=\"css\//i) {
		#$line =~ s/\=\"css\//\=\"https:\/\/www.net.reach.com\/css\//;
		#$line =~ s/\=\"css\//\=\"https:\/\/$realhost\/css\//;
		$line =~ s/\=\"css\//\=\"\/olss\/css\//;
	}

        if($line =~ /(\"|\')(service\/service.htm|service.htm)(\'|\")/) {
print BUG "service.htm|service.htm\n";
        	$line =~ s/(\'|\")service\//$1/g;
                $line =~ s/(\'|\")service.htm(\'|\")/$1$servicescript\?code=$code\&level=1&et=$prodcode$2/g;
        }
        if($line =~ /(\'|\")(reports\/reports.htm|reports.htm)(\'|\")/) {
        	$line =~ s/(\'|\")reports\//$1/g;
                $line =~ s/(\'|\")reports.htm(\'|\")/$1$reportscript\?code=$code\&level=1&et=$prodcode$2/g;
        }
        if($line =~ /(\'|\")(looking_glass\/looking.htm|looking.htm)(\'|\")/) {
		print DEBUG "FIRST LEVEL - LOOKING GLASS in .pl\n";
                $line =~ s/(\'|\")looking_glass\//$1/g;
                $line =~ s/(\'|\")looking.htm(\'|\")/$1$lookglassscript\?code=$code\&level=1&et=$prodcode$2/g;
        }
        if($line =~ /(\'|\")(manage_services\/manage_services.htm|manage_services.htm)(\'|\")/) {
print BUG "manage_services.htm|manage_services.htm.htm|service.htm\n";
		print DEBUG "MANAGE - LEVEL1 .pl\n";
               $line =~ s/(\'|\")manage_services\//$1/g;
               $line =~ s/(\'|\")manage_services.htm(\'|\")/$1$mngservscript\?code=$code\&level=1&et=$prodcode$2/g;
		print DEBUG "LINE1\n\n $line\n";
        }
	if($line =~ /(\'|\")(contactus\/contactus.htm|contactus.htm)(\'|\")/) {
                $line =~ s/(\'|\")contactus\//$1/g;
                $line =~ s/(\'|\")contactus.htm(\'|\")/$1$contactscript\?code=$code\&level=1&et=$prodcode$1/g;
        }   
        if($line =~ /(\'|\")(account\/account.htm|account.htm)(\'|\")/) {
                $line =~ s/(\'|\")account\//$1/g;
                $line =~ s/(\'|\")account.htm(\'|\")/$1$accscript\?code=$code\&level=1&et=$prodcode$1/g;
        }
	if($line =~ /(\'|\")(manage_services\/routing.htm|routing.htm)(\'|\")/) {
print BUG "routing\n";
               $line =~ s/(\'|\")manage_services\//$1/g;
               $line =~ s/(\'|\")routing.htm(\'|\")/$1$mngservscript\?code=$code\&level=5002&et=$prodcode$2/g;
#IPT_ph5
	$line = &ms_link($line, $code, $prodcode);
       }

close(BUG);

	# This is for second level headings
	if($type =~ /service/i) {
print BUG "before service_link \n";
		$line = &service_link($line, $code, $prodcode);
	}
	if($type =~ /reports/i) {
		$line = &reports_link($line, $code, $prodcode);
	} 
	if($type =~ /looking_glass/i) {
		print DEBUG "LOOKG GLASS - Secong level heading .pl\n";
		$line = &lg_link($line, $code, $prodcode);
	}
	if($type =~ /manage_services/i) {
print BUG "before ms_link\n";
		print DEBUG "MANAGE - Secong level heading .pl\n LEVEL $level";

	$line = &ms_link($line, $code, $prodcode);
        }
	if($type =~ /contactus/i) {
		$line = &cu_link($line, $code, $prodcode);
	}
        return $line;
}
close (DEBUG);

#sub link_format {
#	my($line, $code, $type, $prodcode, $template) = @_;
#
#	if(!$prodcode) {
#		$prodcode = ($encet) ? $encet : "";
#	}
#
#	my $enc = $prodcode;
#
#	$template = (!$template) ? $tmplp : $template;
#
#       if($line =~ /Templates\//) {
#                $line =~ s/Templates\///g;
#        }
#
#	# if remask URL, remove
#	if($rmask) {
#		$line =~ s/reseller-telstra\/images/images/g;
#	}
#
#	#$line =~ s/\*PRODUCT\*//g;
#	$line =~ s/Reach - Resellers/Reporting/g;
#
#	$line =~ s/http\:\/\//https\:\/\//g;
#	my $e = "https://" . $realhost . "/";
#	$line =~ s/\#LOGOUT\#/$e/;
#
#	my $cet = uc(&dcrypt($prodcode));
# 	my $cet = uc(CMS_OLSS::get_productname_for_productcode(uc(&dcrypt($prodcode))));
#
#	
#       if($line =~ /\*PRODUCT\*/) {
#                #$line = "";
#                $line =~ s/\*PRODUCT\*/$cet/g;
#        }
#
#	if($line =~ /\#USERNAME\#/) {
#		if($accno) {
#			$line =~ s/\#USERNAME\#/$accno/g;
#		}
#	}
#
#	if($line =~ /\#DATE\#/) {
#		if($tdate) {
#			my ($d, $m, $y) = (split(/\s+/, $tdate))[2,1,4];
#			if(length($d) < 2) {
#				$d = "0".$d;
#			}
#			$line =~ s/\#DATE\#/$d-$m-$y/g;
#		}
#	}
#
#	# get correct script files
#	if($line =~ /scripts\/mm_menu.js/) {
#		$line =~ s/\"scripts\/|\"..\/scripts\//\"https:\/\/$realhost\/scripts\//;
#	}
#
#	if($line =~ /MAIN.htm/) {
#		$line =~ s/MAIN.htm/$loginscript?code=$code&level=2/g;
#	}
#	
#
#	# first regenerate code - because staffid have timeouts
#	#my ($result, $vfg, $upass) = &decrypt($code);
#	#if($result) {
#	#	$code = &encrypt($result,$upass,$vfg);
#	#}
#
#	if($line =~ /Online Service Centre/i) {
#               my $ep = CMS_OLSS::access_through_EP($uid);
#                if($ep !~ /true/i) {
#                        $line =~ s/Online Service Centre/Online Service Support Centre/g;
#                }
#        }
#        if($line =~ /OSC /) {
#                my $ep = CMS_OLSS::access_through_EP($uid);
#                if($ep !~ /true/i) {
#                        $line =~ s/OSC /OLSS /g;
#                }
#        }
#
#	#my $h = `/bin/hostname`;
#	#my $rhost = ($h =~ /in.reach.com/i) ? 0 : 1;
#	#$host = ($rhost) ? $realhost : $devhost;
#	$host = $realhost;
#
#	# set reseller flag
#	#$resell = 0;
#
#	#if($line =~ /reseller/i) {
#	#	$resell = 1;
#	#}
#
#	# reseller specific rules
#	#if($resell) {
#		$line = &resell_link($line, $code, $host, $template, $prodcode);
#		if($line =~ /images\//) {
#			if($line !~ /document\.images/i) {
#				#$line =~ s/images\//$link\/images\//g;
#				$line =~ s/images\//\/tgwcustdata\/images\//g;
#			}
#		}
#	#} else {
#	#	if($line =~ /images\//) {
#	#		$line =~ s/images\//\/tgwcustdata\/images\//g;
#	#	}
#	#}
#
#	#if($line =~ /Home/) {
#	#	$line =~ s/Home/Home $prodcode/;
#	#}
#
#	if($line =~ /background\=/) { #\=\"\.\.\/images\/home\/bg\.gif\"/) {
#		$line =~ s/background\=\"\/tgwcustdata\/images\/home\/bg.gif\"/HH/;
#		$line =~ s/HH/bgcolor=\"#ffffff\"/;
#	}
#
#	#if($line =~ /devr\.telstra\.net/) {
#	#	my $host = ($realhost) ? $realhost : "devr.telstra.net";
#	#	$line =~ s/devr\.telstra\.net/$host/g;
#	#}
#
#       #if($line =~ /images\//) {
#	#	if($line !~ /document\.images/i) {
#	#	        $line =~ s/images\//\/tgwcustdata\/images\//g;
#	#	}
#       #}
#
#        if($line =~ /\"..\/home.htm\"/) {
#        	#$line =~ s/\"..\/home.htm\"/\"$loginscript\?code=$code\&level=2\"/g;
#        	$line =~ s/\"..\/home.htm\"/\"$loginscript\?code=$code\&level=6&et=$prodcode\"/g;
#        }
#	if($line =~ /\"home.htm\"/) {
#		$line =~ s/\"home.htm\"/\"$loginscript\?code=$code\&level=6&et=$prodcode\"/g;
#		#$line =~ s/\"home.htm\"/\"$loginscript\?code=$code\&level=2\"/g;
#        }
#
#	$line =~ s/\"\.\.\/service\//\"/g;
#	$line =~ s/\"\.\.\/reports\//\"/g;
#	$line =~ s/\"\.\.\/looking_glass\//\"/g;
#	$line =~ s/\"\.\.\/contactus\//\"/g;
#	$line =~ s/\"\.\.\/account\//\"/g;
#
#	if($line =~ /href\=\"css\//i) {
#		#$line =~ s/\=\"css\//\=\"https:\/\/www.net.reach.com\/css\//;
#		$line =~ s/\=\"css\//\=\"https:\/\/$realhost\/css\//;
#	}
#
#       if($line =~ /(\"|\')(service\/service.htm|service.htm)(\'|\")/) {
#        	$line =~ s/(\'|\")service\//$1/g;
#                $line =~ s/(\'|\")service.htm(\'|\")/$1$servicescript\?code=$code\&level=1&et=$prodcode$2/g;
#        }
#        if($line =~ /(\'|\")(reports\/reports.htm|reports.htm)(\'|\")/) {
#        	$line =~ s/(\'|\")reports\//$1/g;
#                $line =~ s/(\'|\")reports.htm(\'|\")/$1$reportscript\?code=$code\&level=1&et=$prodcode$2/g;
#        }
#        if($line =~ /(\'|\")(looking_glass\/looking.htm|looking.htm)(\'|\")/) {
#                $line =~ s/(\'|\")looking_glass\//$1/g;
#                $line =~ s/(\'|\")looking.htm(\'|\")/$1$lookglassscript\?code=$code\&level=1&et=$prodcode$2/g;
#        }
#        if($line =~ /(\'|\")(contactus\/contactus.htm|contactus.htm)(\'|\")/) {
#                $line =~ s/(\'|\")contactus\//$1/g;
#                $line =~ s/(\'|\")contactus.htm(\'|\")/$1$contactscript\?code=$code\&level=1&et=$prodcode$1/g;
#        }   
#        if($line =~ /(\'|\")(account\/account.htm|account.htm)(\'|\")/) {
#                $line =~ s/(\'|\")account\//$1/g;
#                $line =~ s/(\'|\")account.htm(\'|\")/$1$accscript\?code=$code\&level=1&et=$prodcode$1/g;
#        }
#
#	# This is for second level headings
#	if($type =~ /service/i) {
#		$line = &service_link($line, $code, $prodcode);
#	}
#	if($type =~ /reports/i) {
#		$line = &reports_link($line, $code, $prodcode);
#	} 
#	if($type =~ /looking_glass/i) {
#		$line = &lg_link($line, $code, $prodcode);
#	}
#	if($type =~ /contactus/i) {
#		$line = &cu_link($line, $code, $prodcode);
#	}
#
#       return $line;
#}

sub resell_link {
	my ($line, $code, $host, $template, $prodcode) = @_;

	if(!$prodcode) {
                $prodcode = ($etproduct) ? $etproduct : "";
        }

        my $enc = $prodcode;


	# prepare images
	if($line =~ /images\/rs\//) {
		$line =~ s/\/rs\//\/$template\//g;
	}

        # if javascript load
        if($line =~ /document\.images/i) {
		#$line =~ s/\.\.\///;
                #$line =~ s/images\//http:\/\/$host\/tgwcustdata\/images\//g;
                #$line =~ s/images\//http:\/\/$host\/tgwcustdata\/images\//g;
		$line =~ s/\"images/\"\/tgwcustdata\/images/g;
		#$line =~ s/\.\.\//\/tgwcustdata\//g;
		$line =~ s/\.\.\//\/olss\/tgwcustdata\//g;
        }

	$line =~ s/\.\.\///;

	# If reseller logo
	if($line =~ /\"images\/.+\/logo_webcentral.gif"/i) {
		my $rlogo = CMS_OLSS::get_wholesaler_logo($logo_accno);
		$line =~ s/\/logo_webcentral\.gif/\/upload\/$rlogo/g;
		$line =~ s/webCentral\.com/Logo/;
		#print "<P> LR ($rlogo) ($logo_accno)\n";
	}

        if($line =~ /(\'|\")service\/\w*\.htm(\'|\")/i) {
                $line =~ s/(\'|\")service\//$1/g;
		$line = &service_link($line, $code, $prodcode);
	}
	if($line =~ /(\'|\")reports\/\w*\.htm(\'|\")/i) {
		$line =~ s/(\'|\")reports\//$1/g;
                                open(DEBUG,">/data1/tmp_log/TR");
                                print DEBUG "LINE=($line),CODE=($code),PRODUCTCODE=($prodcode)\n";
                                close (DEBUG);
		$line = &reports_link($line, $code, $prodcode);
	}
open(DEBUG,">/data1/tmp_log/CHK_DNS_25052009");
	if($line =~ /(\'|\")looking_glass\/\w*\.htm(\'|\")/i) {
		print DEBUG "LK GLASS PRODUCTCODE=($prodcode)\n";
		$line =~ s/(\'|\")looking_glass\//$1/g;
		$line = &lg_link($line, $code, $prodcode);
        }
	if($line =~ /(\'|\")manage_services\/\w*\.htm(\'|\")/i) {
                 $line =~ s/(\'|\")manage_services\//$1/g;
                 print DEBUG "Manage: PRODUCTCODE=($prodcode)\n LINE=($line)\n\n CODE=($code)\n";
                 print DEBUG "Manage: PRODUCTCODE=($prodcode)\n\n";
                 $line = &ms_link($line, $code, $prodcode);
         }
	if($line =~ /(\'|\")contactus\/\w*\.htm(\'|\")/i) {
		$line =~ s/(\'|\")contactus\//$1/g;
		$line = &cu_link($line, $code, $prodcode);  
        }

	return $line;

}
close (DEBUG);

sub cu_link {
        my ($line, $code, $pc) = @_;

        # if first level - return
        return $line if ($line =~ /contactus\/contactus.htm/i);

        SWITCH: {
                ($line =~ /\"$cut[0]\"/i) && do {
                        $line =~ s/(\"|\')$cut[0](\"|\')/$1$contactscript\?code=$code\&level=2&et=$pc$1/g;
                        last SWITCH;
                };
                ($line =~ /\"$cut[1]\"/i) && do {
                        $line =~ s/(\"|\')$cut[1](\"|\')/$1$contactscript\?code=$code\&level=3&et=$pc$1/g;
                        last SWITCH;
                };
        };
        
        return $line;
}

sub service_link2 {
        my ($line, $code, $pc) = @_;

        # if first level - return
        return $line if ($line =~ /service\/service.htm/i);

        SWITCH: {
                ($line =~ /\"$svc[0]\"/i) && do {
                        $line =~ s/\"$svc[0]\"/\"$servicescript\?code=$code\&level=2&et=$pc$1\"/g;
                        last SWITCH;
                };
                ($line =~ /\"$svc[1]\"/i) && do {
                        $line =~ s/\"$svc[1]\"/\"$servicescript\?code=$code\&level=3&et=$pc$1\"/g;
                        last SWITCH;
                };
                ($line =~ /\"$svc[2]\"/i) && do {
                        $line =~ s/\"$svc[2]\"/\"$servicescript\?code=$code\&level=4&et=$pc$1\"/g;
                        last SWITCH;
                };
                ($line =~ /\"$svc[3]\"/i) && do {
                        $line =~ s/\"$svc[3]\"/\"$servicescript\?code=$code\&level=5&et=$pc$1\"/g;
                        last SWITCH;
                };
                ($line =~ /\"$svc[4]\"/i) && do {
                        #$line =~ s/\"$svc[4]\"/\"$servicescript\?code=$code\&level=6&et=$pc$1\"/g;
			#IPSA Change
                        $line =~ s/\"$svc[4]\"/\"javascript:gotoUrl('$servicescript\?code=$code\&level=6&et=$pc$1');\"/g;
                        last SWITCH;
                };
                ($line =~ /\"$svc[6]\"/i) && do {
                        $line =~ s/\"$svc[6]\"/\"$servicescript\?code=$code\&level=7&et=$pc$1\"/g;
                        last SWITCH;
                };
        };

        return $line;
}

sub service_link {
	my ($line, $code, $pc) = @_;

	# if first level - return
	return $line if ($line =~ /service\/service.htm/i);

	SWITCH: {
		($line =~ /(\"|\')$svc[0](\"|\')/i) && do {
			$line =~ s/(\"|\')$svc[0](\"|\')/$1$servicescript\?code=$code\&level=2&et=$pc$1/g;
			last SWITCH;
		};
                ($line =~ /(\"|\')$svc[1](\"|\')/i) && do {
                        $line =~ s/(\"|\')$svc[1](\"|\')/$1$servicescript\?code=$code\&level=3&et=$pc$1/g;
                        last SWITCH;
                };
                ($line =~ /(\"|\')$svc[2](\"|\')/i) && do {
                        $line =~ s/(\"|\')$svc[2](\"|\')/$1$servicescript\?code=$code\&level=4&et=$pc$1/g;
                        last SWITCH;
                };
                ($line =~ /(\"|\')$svc[3](\"|\')/i) && do {
                        $line =~ s/(\"|\')$svc[3](\"|\')/$1$servicescript\?code=$code\&level=5&et=$pc$1/g;
                        last SWITCH;
                };
                ($line =~ /(\"|\')$svc[4](\"|\')/i) && do {
			#IPSA_CR
			$line =~ s/(\"|\')$svc[4](\"|\')/$1\/olss\/$servicescript\?code=$code\&level=6&et=$pc$1/g;
                        $line =~ s/(\"|\')$svc[4](\"|\')/$1$servicescript\?code=$code\&level=6&et=$pc$1/g;
                        last SWITCH;
                };
                ($line =~ /(\"|\')$svc[6](\"|\')/i) && do {
                        $line =~ s/(\"|\')$svc[6](\"|\')/$1$servicescript\?code=$code\&level=7&et=$pc$1/g;
                        last SWITCH;
                };
                ($line =~ /(\"|\')$svc[7](\"|\')/i) && do {
                        $line =~ s/(\"|\')$svc[7](\"|\')/$1$servicescript\?code=$code\&level=8&et=$pc$1/g;
                        last SWITCH;
                };
	};

	return $line;
}
open(DEBUG,">/data1/tmp_log/CHK_DNS_25052009");
sub lg_link {
        my ($line, $code, $pc) = @_;
        
        # if first level - return
        return $line if ($line =~ /looking_glass\/looking.htm/i);
                        
        SWITCH: { 
                ($line =~ /(\"|\')$lgt[0](\"|\')/i) && do {
			#print DEBUG "lg_link $lgt[0] level $level\n";
                        $line =~ s/(\"|\')$lgt[0](\"|\')/$1$lookglassscript\?code=$code\&level=2&et=$pc$1/g;
                        last SWITCH;
                };
                ($line =~ /(\"|\')$lgt[1](\"|\')/i) && do {
			#print DEBUG "lg_link $lgt[1] level $level\n";
                        $line =~ s/(\"|\')$lgt[1](\"|\')/$1$lookglassscript\?code=$code\&level=3&et=$pc$1/g;
                        last SWITCH;
                };
                ($line =~ /(\"|\')$lgt[2](\"|\')/i) && do {  
			#print DEBUG "lg_link $lgt[2] level $level\n";
                        $line =~ s/(\"|\')$lgt[2](\"|\')/$1$lookglassscript\?code=$code\&level=4&et=$pc$1/g;
                        last SWITCH;   
                };
#                ($line =~ /(\"|\')$lgt[3](\"|\')/i) && do {
#                        $line =~ s/(\"|\')$lgt[3](\"|\')/$1$lookglassscript\?code=$code\&level=5&et=$pc$1/g;
#                        last SWITCH;
#                };
        };

        return $line;
}       
close (DEBUG);
 sub ms_link {
         my ($line, $code, $pc) = @_;

         # if first level - return
         return $line if ($line =~ /manage_services\/manage_services.htm/i);

         SWITCH: {
                 ($line =~ /(\"|\')$mst[3](\"|\')/i) && do {
                         open(DEBUG,">/data1/tmp_log/CHK_DNS_25052009");
print DEBUG"$$$$$$$$$$$\n";
                         #print DEBUG " Anjan MST L2\t $mst[3]\t mngservscript : $mngservscript level: $level\n";
                         $line =~ s/(\"|\')$mst[3](\"|\')/$1$mngservscript\?code=$code\&level=2&et=$pc$1/g;
print DEBUG "line:$line\n";
                         close (DEBUG);
                         last SWITCH;
                 };
                 ($line =~ /(\"|\')$mst[0](\"|\')/i) && do {
                         open(DEBUG,">/data1/tmp_log/CHK_DNS_25052009");
                         #print DEBUG " Anjan MST L3\t $mst[0]\t mngservscript : $mngservscript level: $level\n";
                         close (DEBUG);
                         $line =~ s/(\"|\')$mst[0](\"|\')/$1$mngservscript\?code=$code\&level=3&et=$pc$1/g;
                         last SWITCH;
                 };
                 ($line =~ /(\"|\')$mst[1](\"|\')/i) && do {
                         open(DEBUG,">/data1/tmp_log/CHK_DNS_30112010");
                         print DEBUG " Anjan MST L4 \t $mst[1]\t mngservscript : $mngservscript level: $level\n\n";
                         close (DEBUG);
                         $line =~ s/(\"|\')$mst[1](\"|\')/$1$mngservscript\?code=$code\&level=4&et=$pc$1/g;
                         last SWITCH;
                 };
                 ($line =~ /(\"|\')$mst[2](\"|\')/i) && do {
                         open(DEBUG,">/data1/tmp_log/CHK_DNS_25052009");
                         #print DEBUG " Anjan MST L5 \t $mst[2]\t mngservscript : $mngservscript level: $level\n";
                         close (DEBUG);
                         $line =~ s/(\"|\')$mst[2](\"|\')/$1$mngservscript\?code=$code\&level=5&et=$pc$1/g;
                         last SWITCH;
                 };
                 ($line =~ /(\"|\')$mst[4](\"|\')/i) && do {
                         open(DEBUG,">/data1/tmp_log/CHK_DNS_25052009");
                         #print DEBUG " Anjan MST L6 \t $mst[4]\t mngservscript : $mngservscript level: $level\n";
                         close (DEBUG);
                         $line =~ s/(\"|\')$mst[4](\"|\')/$1$mngservscript\?code=$code\&level=6&et=$pc$1/g;
                         last SWITCH;
                 };
		($line =~ /(\"|\')$mst[5](\"|\')/i) && do {
                         open(DEBUG,">/data1/tmp_log/CHK_NTP_150609");
                         #print DEBUG " Anjan MST L6 \t $mst[5]\t mngservscript : $mngservscript level: $level\n";
                         close (DEBUG);
                         $line =~ s/(\"|\')$mst[5](\"|\')/$1$mngservscript\?code=$code\&level=7&et=$pc$1/g;
                         last SWITCH;
                 };

       };

         return $line;
 }

sub reports_link {
        my ($line, $code, $pc) = @_;

        # if first level - return
        return $line if ($line =~ /reports\/reports.htm/i);

        SWITCH: {
                ($line =~ /(\"|\')$rpts[0](\"|\')/i) && do {
                        $line =~ s/(\"|\')$rpts[0](\"|\')/$1$reportscript\?code=$code\&level=1&et=$pc$1/g; 
                        last SWITCH;
                };
                ($line =~ /(\"|\')$rpts[1](\"|\')/i) && do {
                        $line =~ s/(\"|\')$rpts[1](\"|\')/$1$reportscript\?code=$code\&level=2&et=$pc$1/g; 
                        last SWITCH;
                };
                ($line =~ /(\"|\')$rpts[2](\"|\')/i) && do {
                        $line =~ s/(\"|\')$rpts[2](\"|\')/$1$reportscript\?code=$code\&level=3&et=$pc$1/g; 
                        last SWITCH;
                };
                ($line =~ /(\"|\')$rpts[3](\"|\')/i) && do {
                        $line =~ s/(\"|\')$rpts[3](\"|\')/$1$reportscript\?code=$code\&level=4&et=$pc$1/g; 
                        last SWITCH;
                };
		# sla reports
		($line =~ /(\"|\')$rpts[4](\"|\')/i) && do {
			$line =~ s/(\"|\')$rpts[4](\"|\')/$1$reportscript\?code=$code\&level=7&et=$pc$1/g;
			last SWITCH;
		};
		# mpls-site2site report
                ($line =~ /(\"|\')$rpts[5](\"|\')/i) && do {
                        $line =~ s/(\"|\')$rpts[5](\"|\')/$1$reportscript\?code=$code\&level=8&et=$pc$1/g;
                };
		# latency performance - ATM/FR
		($line =~ /(\"|\')$rpts[6](\"|\')/i) && do {
			$line =~ s/(\"|\')$rpts[6](\"|\')/$1$reportscript\?code=$code\&level=9&et=$pc$1/g;
			last SWITCH;
		};
		# cable paths - ATM/FR
		($line =~ /(\"|\')$rpts[7](\"|\')/i) && do {
			$line =~ s/(\"|\')$rpts[7](\"|\')/$1$reportscript\?code=$code\&level=A&et=$pc$1/g;
			last SWITCH;
		};
		# mpls te tunnel traffic report
		($line =~ /(\"|\')$rpts[8](\"|\')/i) && do {
			$line =~ s/(\"|\')$rpts[8](\"|\')/$1$reportscript\?code=$code\&level=B&et=$pc$1/g;
			last SWITCH;
		};
		# for Core Qos Report -- Pradeep
		($line =~ /(\"|\')$rpts[9](\"|\')/i) && do {
			$line =~ s/(\"|\')$rpts[9](\"|\')/$1$reportscript\?code=$code\&level=Z&et=$pc$1/g;
			last SWITCH;
		};
#Chandini_EPL
		 # for Point to multipoint -- trafficreport
                ($line =~ /(\"|\')$rpts[11](\"|\')/i) && do {
                        $line =~ s/(\"|\')$rpts[11](\"|\')/$1$reportscript\?code=$code\&level=E&et=$pc$1/g;
                        last SWITCH;
                };
# Sayantan_IPT_Ph_3: 29-Jul-2010 - Start
                 # for Burstable traffic report
                ($line =~ /(\"|\')$rpts[12](\"|\')/i) && do {
                        $line =~ s/(\"|\')$rpts[12](\"|\')/$1$reportscript\?code=$code\&level=I&et=$pc$1/g;
                        last SWITCH;
                };
# Sayantan_IPT_Ph_3: 29-Jul-2010 - End
#Stavan - IP Transit China Metering_20100806
                 # for China traffic report
                ($line =~ /(\"|\')$rpts[13](\"|\')/i) && do {
                        $line =~ s/(\"|\')$rpts[13](\"|\')/$1$reportscript\?code=$code\&level=C&et=$pc$1/g;
                        last SWITCH;
                };
        };

        return $line;
}



######################

# Validate services
# Pre: serviceid, accno, wholesaler
# Post: 0 (fail); 1 (ok)
sub validate_service {
        my ($serviceid, $accno, $is_wholesaler) = @_;

        if($accno =~ /$testacc/i) {
                return 0;
        }
                
        my($svcids, @allservices);

        my $result = 0;
##++ EVPL - SD
        open (DEBUG_SD, ">/data1/tmp_log/tgwcisslib_label.txt");
	my $pcet = ($etproduct) ? $etproduct : "GIA";
	#print DEBUG_SD "Product code = $pcet.\n";
        eval {
                if ($is_wholesaler =~ /^1$/) { # is wholesaler
                        $svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno($accno,$pcet);
                } else {
                        $svcids = CMS_OLSS::get_serviceid_for_accno($accno,$pcet);
                }
        };        

        foreach my $svcref (@$svcids) {
	        #print DEBUG_SD "SVC REF = '$$svcref[0]'.\n";
	        $$svcref[0] =~ s/ //g;
	        #print DEBUG_SD "Without space SVC REF = '$$svcref[0]'.\n";
	        $$svcref[0] =~ s/\[|\]//g;
	        #print DEBUG_SD "Without [] SVC REF = '$$svcref[0]'.\n";
	        #print DEBUG_SD "Service ID = '$serviceid'.\n";
	        $serviceid =~ s/ //g;
	        #print DEBUG_SD "Without space SID = '$serviceid'.\n";
	        $serviceid =~ s/\[|\]//g;
	        #print DEBUG_SD "Without [] SID = '$serviceid'.\n";
                if($$svcref[0] =~ /^$serviceid$/i) {
	                #print DEBUG_SD "Matching Service ID.\n";
                        return 1;
                }
                #push @allservices, $$svcref[0];
        }
        close DEBUG_SD; 
##-- EVPL - SD
        return 0;

}

######################

# Validate services
# Pre: serviceid, accno, wholesaler
# Post: 0 (fail); 1 (ok)
sub validate_service_aggrmast {
        my ($serviceid, $accno, $is_wholesaler) = @_;

        if($accno =~ /$testacc/i) {
                return 0;
        }

        my($svcids, @allservices);

        my $result = 0;
        eval {
                if ($is_wholesaler =~ /^1$/) { # is wholesaler
                        $svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno_aggrmast($accno);
                } else {
                        $svcids = CMS_OLSS::get_serviceid_for_accno_aggrmast($accno);
                }
        };

        foreach my $svcref (@$svcids) {
                if($$svcref[0] =~ /^$serviceid$/i) {
                        return 1;
                }
        }
        return 0;
}

# Validate services
# Pre: serviceid, accno, wholesaler
# Post: 0 (fail); 1 (ok)
sub validate_service1 {
	my ($serviceid, $accno) = @_;

	if($accno =~ /$testacc/i) {
		return 0;
	}

	my($svcids, @allservices);

	my $result = 0;
        eval {
                $svcids = CMS_OLSS::get_serviceid_for_accno($accno);  
        };

        foreach my $svcref (@$svcids) {
		if($$svcref[0] =~ /^$serviceid$/i) {
			return 1;
		}
                #push @allservices, $$svcref[0];
        }

	return 0;

}

##### Get HTML List of services #####
# Pre: uid, code, accno, tmplroot, script, level, server, , [display_as]
# Post: @vals
sub screen_services {
	open (FH_Chand_log,">/data1/tmp_log/Chand_log");
	print FH_Chand_log "in side screen services =====================+++++++++\n";
	my ($uid, $code, $accno, $tmplroot, $script, $lvl, $server, $display_as) = @_;
	print FH_Chand_log "tmplroot:$tmplroot\tlvl:$lvl\tserver:$server\tdisplay_as:$display_as\n";
	my (@allservices, @service_as, @bgp_ip, $svcids, $whlattr);
	#my (@allservices, $svcids, $whlattr);

	##### Modified on 17 Apr 2007
	my (%svclabel_list, %serviceidalt_list);
	#####
	
	# uid is accno
	# get attribute of accno
	my $attribute = CMS_OLSS::is_wholesaler($accno);

	my $pcet = ($etproduct) ? $etproduct : "GIA";
	
	if($attribute =~ /^1$/i) {
		# if wholesaler, want to see all sub-customers
		eval {
			$svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno($accno, $pcet);
		};
 		foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
			push @service_as, $$svcref[2];
                        push @bgp_ip, $$svcref[3];

			##### Modified on 17 Apr 2007
			$svclabel_list{$$svcref[0]} = $$svcref[5];
			$serviceidalt_list{$$svcref[0]} = $$svcref[6];
			#####
                }

	} else {
        	eval {
               		$svcids = CMS_OLSS::get_serviceid_for_accno($accno, $pcet);
        	};

		foreach my $svcref (@$svcids) {
			#$$svcref[0] =~ tr/A-Z/a-z/;
			push @allservices, $$svcref[0];
			push @service_as, $$svcref[1];
                        push @bgp_ip, $$svcref[2]; 

			##### Modified on 17 Apr 2007
			$svclabel_list{$$svcref[0]} = $$svcref[3];
			$serviceidalt_list{$$svcref[0]} = $$svcref[4];
			#####
		}
	}


	#print "<P>SEE (@allservices) ($svcref) (@$svcref) ($@) ($svcids)\n";

	my @screenoutput = ();

	##Karuna on 17-11-2010 for IPT5  #Modified and added GIA aslo 
	if ($debug) {print IPT_RM "allservices1:$#allservices\tallservices2:($#allservices +1)\n";}
	if (($#allservices) > 0){
		push @screenoutput, "<b class=header>&nbsp Select a service:</b><p><ol>\n";
	} else {
		#if ($tmplroot !~ /iptransit/) {
		if (($tmplroot !~ /iptransit/) && ($tmplroot !~ /gia/)){
			push @screenoutput, "<b class=header>No services found.</b><p><ol>\n";
		} else {
			push @screenoutput, "<b class=header>&nbsp Select a service:</b><p><ol>\n";
		}
	}
	##--IPT5
	
######### just for telstra demo 10/9/03 ########
if (($accno eq "mplstest") || ($accno eq "testing1")) {
	@allservices = ('saa_hkg_wvp','saa_ldn_wvp');
}
########################################

	my $encrptp = ($encet) ? $encet : "";

	my $i = 0;
	my $prev_as = "";

        if (!$display_as) {
                @allservices = sort { lc($a) cmp lc($b) } @allservices;
        }
print FH_Chand_log " allservices:@allservices\n";
        foreach my $k (@allservices) {
		my $service_id = $k;

		##### Modified on 17 Apr 2007
		my $svclabel = $svclabel_list{$k};
		my $serviceid_alt = $serviceidalt_list{$k};
		#my $svclabel = &get_svclabel($accno, $k, 0);
		#my $serviceid_alt = &get_serviceid_alt($accno, $k, 0);
		#####

		my $x = ecrypt($k);
                my $href_begin = "<li class=\"text\"><a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&et=$encrptp\">";
		my $href_end = "</a></li>\n";
		my $t = "";

                if ($display_as) {
print FH_Chand_log "prev_as:$prev_as    service_as[1]: $service_as[$i]  bgp_ip[i]:$bgp_ip[$i]\n";
                        if ($prev_as ne $service_as[$i]) {
                                #if ($prev_as eq "") { $t .= $href_end; }
                                $t .= $href_end;
                                if (($service_as[$i] ne "") && ($bgp_ip[$i] ne "")) {
                                        $t .= "$href_begin";
                                } else {
                                        $t .= "<li class=\"text\">";
                                }
                                $t .= " AS$service_as[$i]<br>";
print FH_Chand_log "ttttttttttttttt:$t\n";
                                $prev_as = $service_as[$i];
                        }
                } else {
                        $t .= $href_begin;
                }

		
                if (($svclabel) && ($serviceid_alt)) {
                        $svclabel =~ s/\^//g;
                        $t .= "$svclabel ($serviceid_alt) ($k)\n";
                } elsif ($svclabel) {
                        $svclabel =~ s/\^//g;
                        $t .= "$svclabel ($k)\n";
                } elsif ($serviceid_alt) {
                        $t .= "$serviceid_alt ($k)\n";
                } else {
                        $t .= "$k\n";
                }
#Chandini_Ph5
#if($service_as[$i]){
                if ($display_as) {
                        #$t .= " ($bgp_ip[$i])";
			##15-11-2010 Karuna for IPT5
			print FH_Chand_log "DB IF display_as: bgp_ip:$bgp_ip[$i]\t i:$i\t t:$t\n";
			if ($bgp_ip[$i] eq ""){
				print FH_Chand_log "DB NO BGP\n";
				#if ($prev_as ne ""){
				#	$t .= " ($bgp_ip[$i])";
				#} else {
                			$i++;
			if ($i == $#allservices) { 
				print FH_Chand_log "DB end of array\n";
				$t .= $href_end; 
			}
					next;
				#}
			} else {
				print FH_Chand_log "DB BGP found\n";
				$t .= " ($bgp_ip[$i])";
			}
			if ($t =~ /\<\/a\>\<\/li\>/){
				print FH_Chand_log "DB END of links reached\n";
			}
			print FH_Chand_log "DB IF display_as:before output t:$t\n";
print FH_Chand_log  "t2222:$t    iiiii:$i\n";
			if ($i == $#allservices) { 
				print FH_Chand_log "DB end of array\n";
				$t .= $href_end; 
			}
                        #if (($service_as[$i] ne "") && ($bgp_ip[$i] ne "")) {
			##15-11-2010 Karuna for IPT5
			print FH_Chand_log "DB before pushing to screen\n";
                        push @screenoutput, "$t<br>";
			print FH_Chand_log "DB SCREENOUTPUT $t<br>\n";
                        #} else {
                        #        push @screenoutput, "<li>$t</li>";
                        #}
                } else {
			##15-11-2010 Karuna for IPT5
			print FH_Chand_log "DB ELSE of display_as:$display_as before pushing to screen\n";
                        push @screenoutput, "$t$href_end";
                }
#}###
                $i++;
		##15-11-2010 Karuna for IPT5
		print FH_Chand_log "DB INDEX NUM:$i\n";
	}

	# This gives server services
	if($server) {
		foreach my $ss (@server_services) {
			my $x = ecrypt($ss);
			my $t = "<li class=\"text\"><a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&et=$encrptp\">";
			$t .= "$ss</a></li>\n";
			push @screenoutput, $t;
		}
	}
	print FH_Chand_log  "array size:$#allservices\n";
		
	push @screenoutput, "</ol>\n";

	return @screenoutput;
}

##### Get HTML List of services #####
# Pre: uid, code, accno, tmplroot, script, level, server, , [display_as]
# Post: @vals
sub screen_services_aggrmast {
        my ($uid, $code, $accno, $tmplroot, $script, $lvl, $server, $display_as) = @_;
        my (@allservices, $svcids, $whlattr);

        my %svclabel_list;

        # uid is accno
        # get attribute of accno
        my $attribute = CMS_OLSS::is_wholesaler($accno);

        my $pcet = ($etproduct) ? $etproduct : "GIA";

        if($attribute =~ /^1$/i) {
                # if wholesaler, want to see all sub-customers
                eval {
                        $svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno($accno, $pcet);
                };
                foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
                        $svclabel_list{$$svcref[0]} = $$svcref[5];
                }
        } else {
                eval {
                        $svcids = CMS_OLSS::get_serviceid_for_accno($accno, $pcet);
                };
                foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
			$svclabel_list{$$svcref[0]} = $$svcref[3];
                }
        }

        my $screenoutput = ();

        my $encrptp = ($encet) ? $encet : "";

        my $i = 0;
        my $prev_as = "";

        if (!$display_as) {
                @allservices = sort { lc($a) cmp lc($b) } @allservices;
        }

        foreach my $k (@allservices) {
                my $service_id = $k;

                my $svclabel = $svclabel_list{$k};

                my $x = ecrypt($k);
                my $t = "";

                if ($svclabel) {
                        $svclabel =~ s/\^//g;
                        $t .= "$svclabel ($k)\n";
                } else {
                        $t .= "$k\n";
                }
                my $href_begin = "<option value='$x'>$t";
                my $href_end = "</option>\n";
                push @screenoutput, "$href_begin$href_end";
        }

        # This gives server services
        if($server) {
                foreach my $ss (@server_services) {
                        my $x = ecrypt($ss);
                        push @screenoutput, $ss;
                }
        }

        foreach my $i (@screenoutput) {
                $retstr .= $i;
        }

        return $retstr;
}


############### Chandini ###########
sub screen_master_services {
        my ($uid, $code, $accno, $tmplroot, $script, $lvl) = @_;
        my (@allservices, $svcids, $whlattr, $label);
        my (@svclabel_list);
        my $attribute = CMS_OLSS::is_wholesaler($accno);
        my $pcet = ($etproduct) ? $etproduct : "GIA";
	if($attribute =~ /^1$/i) {
		$svcids = CMS_OLSS::get_master_serv_for_wholesaler_accno($accno, $pcet);
	my $count_lab = 0;
	foreach my $svcref (@$svcids) {
        	push @allservices, $$svcref[0];
        	$svclabel_list[$count_lab] = $$svcref[1];
        	$count_lab++;
	}

		
	}else{
                eval {
                        $svcids = CMS_OLSS::get_master_services_for_accno($accno, $pcet);
                };
                my $count_lab = 0;
                foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
                        $svclabel_list[$count_lab] = $$svcref[1];
                        $count_lab++;
                }
	}
        my @screenoutput = ();
        push @screenoutput, "<b class=header>&nbsp Select a Master VPN Service ID:</b><p><ol>\n";
	######### just for telstra demo 10/9/03 ########
	if (($accno eq "mplstest") || ($accno eq "testing1")) {
        @allservices = ('saa_hkg_wvp','saa_ldn_wvp');
	}
        my $encrptp = ($encet) ? $encet : "";
        my $i = 0;
        my $prev_as = "";
        @allservices = sort { lc($a) cmp lc($b) } @allservices;
        my $count_id = 0;
        foreach my $k (@allservices) {
                my $service_id = $k;
                my $svclabel = $svclabel_list[$count_id];
                $count_id++;
                my $x = ecrypt($k);
                my $href_begin = "<li class=\"text\"><a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&et=$encrptp&master_srv=$service_id&master_lb=$svclabel\">";
                my $href_end = "</a></li>\n";
                my $t = "";
                $t .= $href_begin;
                if ($svclabel) {
                        $svclabel =~ s/\^//g;
                        $t .= "$svclabel ($k)\n";
                } else {
                        $t .= "$k\n";
                }
                push @screenoutput, "$t$href_end";
                $i++;
        }
	push @screenoutput, "</ol>\n";
	push @screenoutput, "<form ACTION=\"/cgi-bin/$script\">";
 	push @screenoutput, "<input type=hidden name=code value=$code>";	
 	push @screenoutput, "<input type=hidden name=level value=21>";	
 	push @screenoutput, "<input type=hidden name=et value=$encrptp>";	
        push @screenoutput, "<b>&nbsp <br><br>Search a service for Traffic Report:</b><br>\n";
        push @screenoutput, "<Select name=svc>";
        push @screenoutput, &screen_services_aggrmast($uid, $code, $accno, $tmplroot, $reportscript, "21");
	push @screenoutput, &screen_agg_services_aggrmast($uid, $code, $accno, $reportscript, "21");
        push @screenoutput, "</select>";
        push @screenoutput, "<input class=\"btnPrimary\" type=submit value=\"Go\">";
        push @screenoutput, "</form>";
        return @screenoutput;
}

sub screen_services_of_master {
        my ($uid, $code, $accno, $tmplroot, $script, $master_serv,$lvl, $master_lb) = @_;
        my (@allservices, @service_as, @bgp_ip, $svcids, $whlattr, @poll_type, @svclabel_list);
        my (%serviceidalt_list);
        my $pcet = ($etproduct) ? $etproduct : "GIA";
                eval {
                        $svcids = CMS_OLSS::get_services_of_master($accno, $pcet, $master_serv);
                };
        my $count_lab = 0;
 	foreach my $svcref (@$svcids) {
                push @allservices, $$svcref[0];
                push @bgp_ip, $$svcref[2];
                push @poll_type, $$svcref[5];
                $svclabel_list[$count_lab] = $$svcref[3];
                $count_lab++;
        }
        my $master_details;
        my @screenoutput = ();
	if ($master_lb) {
                $master_lb =~ s/\^//g;
                $master_details = "$master_lb ($master_serv)\n";
        } else {
                $master_details = "$master_serv\n";
        }
	push @screenoutput, "<b class=header>&nbsp Master VPN Service ID Selected:</b> $master_details<p><ol>\n";
	if (($accno eq "mplstest") || ($accno eq "testing1")) {
        @allservices = ('saa_hkg_wvp','saa_ldn_wvp');
	}
        my $encrptp = ($encet) ? $encet : "";
        my $i = 0;
        my $prev_as = "";
        my $count_id = 0;
	my (@log_rows,@phy_rows);
        foreach my $k (@allservices) {
                my $service_id = $k;
                my $svclabel = $svclabel_list[$count_id];
		my $p_type = $poll_type[$count_id];
                $count_id++;
                my $serviceid_alt = $serviceidalt_list{$k};
		if ($p_type =~ /TRANS/i){
			$lvl=21;
		}else {
			$lvl=231;
		}
		my $x = ecrypt($k);
                my $href_begin = "<li class=\"text\"><a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&et=$encrptp&master_srv=$master_serv&log_ser=$service_id&pt=$p_type&master_lb=$master_lb&log_lb=$svclabel\">";
                my $href_end = "</a></li>\n";
                my $t = "";
                $t .= $href_begin;
        	if ($svclabel) {
                        $svclabel =~ s/\^//g;
                        $t .= "$svclabel ($k)\n";
                } else {
                        $t .= "$k\n";
                }
 		if ($p_type  =~ /TRANS/i){
                        push @phy_rows, "$t$href_end";
		} else {
			push @log_rows, "$t$href_end";
		}
                $i++;
        }
#	if (@log_rows == 0) {
	#	push @screenoutput, "<b><FONT color=red>&nbsp There are no Logical Interface associated with the Master Service.</FONT></b><p>\n";
	#}
	#else {
	if (@log_rows != 0){
		push @screenoutput, "<b>&nbsp Select a Logical Interface (VLAN):</b><p><ol>\n";
        	push @screenoutput,@log_rows;
		push @screenoutput, "</ol>\n";
		push @screenoutput, "<br>\n";
	}
	#}
	#if (@phy_rows == 0) {
      #  	push @screenoutput, "<b><FONT color=red>&nbsp There are no Physical Port service associated with the Master Service.</FONT></b><p>\n";
#	}
#	else {
	if (@phy_rows != 0) {
        	push @screenoutput, "\n<b class=header>&nbsp  Select a Physical Port:</b><p><ol>\n";
        	push @screenoutput,@phy_rows;
        	push @screenoutput, "</ol>\n";
	}
	#}
        return @screenoutput;
}

sub screen_aggregate_services {
        my ($uid, $code, $accno, $tmplroot, $script, $master_serv, $log_serv, $lvl,$pt,$master_lb, $log_lb) = @_;
        my (@allservices, @service_as, @bgp_ip, $svcids, $whlattr,$agg_serv,$agg_serv_label,$agg_serv_full);
        my (%svclabel_list, %serviceidalt_list);
        my $pcet = ($etproduct) ? $etproduct : "GIA";
                eval {
                        $svcids = CMS_OLSS::get_aggregate_services_for_accno($accno, $pcet, $master_serv, $log_serv );
                };
                foreach my $svcref (@$svcids) {
                        $agg_serv=$$svcref[0];
                        $agg_serv_label=$$svcref[1];
			$agg_serv_full=$$svcref[2];
                }
	my $encrptp = ($encet) ? $encet : "";
	my $x = ecrypt($log_serv);
	my $href_end = "</a>\n";
        my $log_details;
        my $master_details;
        my $agg_details;
        my @screenoutput = ();
        if ($master_lb) {
                $master_lb =~ s/\^//g;
                $master_details = "$master_lb ($master_serv)\n";
        } else {
                $master_details = "$master_serv\n";
        }
        if ($log_lb) {
                $log_lb =~ s/\^//g;
                $log_details = "$log_lb ($log_serv)\n";
        } else {
                $log_details = "$log_serv\n";
        }
        push @screenoutput, "<b class=header>&nbsp Master VPN Service ID Selected:</b> $master_details<p>\n";
        push @screenoutput, "<b>&nbsp Logical Interface (VLAN) Selected:</b> $log_details<p>\n";
        push @screenoutput, "<b>&nbsp Generate Traffic Report for this Logical Interface (VLAN): </b><p>\n";
	my $href_begin = "&nbsp <a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&master_srv=$master_serv&log_ser=$log_serv&agg_serv=$agg_serv&pt=$pt&et=$encrptp&master_lb=$master_lb&log_lb=$log_lb&agg_lb=$agg_serv_label\">";
        my $t .= $href_begin;
	if ($log_lb){
		$log_lb =~ s/\^//g;
		$t.= "$log_lb ($log_serv)\n<br>";
	}else{
                $t.="$log_serv \n";
	}
        $t.=$href_end;
        push @screenoutput,$t;
	if ($agg_serv ne ""){
    #    if ($agg_serv eq ""){
	#	push @screenoutput, "<br><br><b><FONT color = red>&nbspThis Logical Interface has no Physical Port Service Associated with it.</FONT></b><p>\n";
        #} else {
		$x = ecrypt($agg_serv_full);
		push @screenoutput, "<br><p><b class=header>&nbsp Generate Traffic Report for the Physical Port:</b><p>\n";
		my $href_begin = "&nbsp <a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&master_srv=$master_serv&log_ser=$log_serv&agg_serv=$agg_serv&pt=$pt&et=$encrptp&master_lb=$master_lb&log_lb=$log_lb&agg_lb=$agg_serv_label\">";
		my $t .= $href_begin;
		if ($agg_serv_label) {
                	$agg_serv_label =~ s/\^//g;
                        $t .= "$agg_serv_label ($agg_serv)\n";
		}else {
                        $t .= "$agg_serv\n";
                }
		$t.=$href_end;
		push @screenoutput,$t;
        #}
	}
	return @screenoutput;
}
################## End Chandini#########################
#++EVPL
sub screen_services_evpl{
	my ($uid, $code_l, $accno, $tmplroot, $script, $lvl) = @_;
	
	my (@allservices, $svcids, $whlattr);

	my (%svclabel_list, %serviceidalt_list,%polltype_list);

	# get attribute of accno
	my $attribute = CMS_OLSS::is_wholesaler($accno);
	open(DEBUG_1, ">/data1/tmp_log/CHK_PRAD_0308");
	#print DEBUG_1 "attribute-------->$attribute\n";

	my $pcet = $etproduct;
	
	if($attribute =~ /^1$/i) {
		# if wholesaler, want to see all sub-customers
		eval {
			$svcids = CMS_OLSS::get_evplservices_wholesaler($accno);
		};
 		foreach my $svcref (@$svcids) {
            push @allservices, $$svcref[0];
            $svclabel_list{$$svcref[0]} = $$svcref[1];
			$serviceidalt_list{$$svcref[0]} = $$svcref[2];
			$polltype_list{$$svcref[0]} = $$svcref[3];
	    }
	} else {
       	eval {
      		$svcids = CMS_OLSS::get_evpl_services($accno);
       	};
	#print DEBUG_1 "svcids----->@$svcids\n";
		foreach my $svcref (@$svcids) {
			push @allservices, $$svcref[0];
			$svclabel_list{$$svcref[0]} = $$svcref[1];
			$serviceidalt_list{$$svcref[0]} = $$svcref[2];
			$polltype_list{$$svcref[0]} = $$svcref[3];
			
		}
	}
	my @screenoutput = ();
  	my @VPMRows = ();
	my @TPMRows = ();	
	my $encrptp = ($encet) ? $encet : "";

	my $firstVPM=0;
	my $firstTPM=0;
	

	foreach my $k (@allservices) {
		my $service_id = $k;
		my $svclabel = $svclabel_list{$k};
		my $serviceid_alt = $serviceidalt_list{$k};
		my $prodcode = $prodtype_list{$k};
		my $polltype = $polltype_list{$k};
		my $x = ecrypt($k);
                my $href_begin = "<li class=\"text\"><a class=\"text\" href=\"/cgi-bin/$script?code=$code_l&svc=$x&level=$lvl&et=$encrptp\">";
		my $href_end = "</a></li>\n";
		my $t = "";

		$t .= $href_begin;
	
        if (($svclabel) && ($serviceid_alt)) {
                $svclabel =~ s/\^//g;
                $t .= "$svclabel ($serviceid_alt) ($k)\n";
        } elsif ($svclabel) {
                $svclabel =~ s/\^//g;
                $t .= "$svclabel ($k)\n";
        } elsif ($serviceid_alt) {
                $t .= "$serviceid_alt ($k)\n";
        } else {
                $t .= "$k\n";
        }
		
		if( $polltype =~ /VPM/){

			if($firstVPM==0){push @VPMRows, "<b class=header>&nbsp Select a EVPL VLAN Service:</b><p><ol>\n";}

			push @VPMRows,"$t$href_end";
        		$firstVPM = 1;
    		}
    	if( $polltype =~ /TPM/){
		if($firstTPM==0){ push @TPMRows, "<br><b class=header>&nbsp Select a EVPL Transparent Service:</b><p><ol>\n";}
		push @TPMRows,"$t$href_end";
	    	$firstTPM = 1;
    	}
	}
        push @screenoutput,@VPMRows;
        push @screenoutput, "</ol>\n";
        push @screenoutput,@TPMRows;
        push @screenoutput, "</ol>\n";

	
	return @screenoutput;
}
#--EVPL

#Chandini_EPL
##### Get List of EPL Point to point services #####
# Pre: uid, code, accno, tmplroot, script, level
# Post: @vals

sub screen_services_epl{
        my ($uid, $code, $accno, $tmplroot, $script, $lvl) = @_;
	#Print eplFH "inside screen services epl: $uid, $code, $accno, $tmplroot, $script, $lvl \n";
        my (@allservices, $svcids, %svclabel_list);
        # get attribute of accno
        my $attribute = CMS_OLSS::is_wholesaler($accno);
       		#if wholesaler, want to see all sub-customers
	if($attribute =~ /^1$/i) {
        	eval {
        	#	$svcids = CMS_OLSS::get_epl_services_wholesaler($accno);
			$svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno($accno,$etproduct);
        	};
       		foreach my $svcref (@$svcids) {
        		push @allservices, $$svcref[0];
        		$svclabel_list{$$svcref[0]} = $$svcref[5];
       		}
	}else {
        	eval {
                	#$svcids = CMS_OLSS::get_epl_services($accno);
			$svcids = CMS_OLSS::get_serviceid_for_accno($accno,$etproduct);
			
        	};
                foreach my $svcref (@$svcids) {
			push @allservices, $$svcref[0];
                        $svclabel_list{$$svcref[0]} = $$svcref[3];
        	}
	}
        my @screenoutput = ();
	push @screenoutput, "<b class=header>&nbsp Select a Service:</b><p><ol>\n";
        my $encrptp = ($encet) ? $encet : "";
        foreach my $k (@allservices) {
                my $service_id = $k;
		$service_id =~ s/\s//g;
		my $sstrng = substr($service_id,13,2);
                #print eplFH "14th element : $sstrng \n";
      		if ($sstrng eq "em") {
			next;
		}
                my $svclabel = $svclabel_list{$k};
                my $x = ecrypt($k);
                my $href_begin = "<li class=\"text\"><a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&et=$encrptp\">";
open (FH1,">/data1/tmp_log/log1");
print FH1 "href_begin: $href_begin\n";
                my $href_end = "</a></li>\n";
                my $t = "";
                $t .= $href_begin;
        if ($svclabel) {
                $svclabel =~ s/\^//g;
                $t .= "$svclabel ($k)\n";
        } else {
                $t .= "$k\n";
        }
	push @screenoutput, "$t$href_end";
	}
        return @screenoutput;
}

#Chandini_EPL
##### Get List of EPL Point to Multipoint Masterservices #####
# Pre: uid, code, accno, tmplroot, script, level
# Post: @vals

sub screen_master_services_mepl{
        my ($uid, $code, $accno, $tmplroot, $script, $lvl) = @_;
        my (@allservices, $svcids, %svclabel_list);
        # get attribute of accno
        my $attribute = CMS_OLSS::is_wholesaler($accno);
                #if wholesaler, want to see all sub-customers
        if($attribute =~ /^1$/i) {
                eval {
                        $svcids = CMS_OLSS::get_master_serv_for_wholesaler_accno($accno,$etproduct);
                };
                foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
                        $svclabel_list{$$svcref[0]} = $$svcref[1];
                }
        }else {
                eval {
                        $svcids = CMS_OLSS::get_master_services_for_accno($accno,$etproduct);
                };
                foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
                        $svclabel_list{$$svcref[0]} = $$svcref[1];
                }
        }
        my @screenoutput = ();
        push @screenoutput, "<b class=header>&nbsp Select a Master Service:</b><p><ol>\n";
        my $encrptp = ($encet) ? $encet : "";
        foreach my $k (@allservices) {
                my $service_id = $k;
                my $svclabel = $svclabel_list{$k};
                my $x = ecrypt($k);
                my $href_begin = "<li class=\"text\"><a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&et=$encrptp&master_id =$k&master_lb=$svclabel\">";
                my $href_end = "</a></li>\n";
                my $t = "";
                $t .= $href_begin;
        	if ($svclabel) {
                	$svclabel =~ s/\^//g;
                	$t .= "$svclabel ($k)\n";
        	} else {
                	$t .= "$k\n";
        	}
        	push @screenoutput, "$t$href_end";
        }
	
	push @screenoutput, "</ol>\n";
        push @screenoutput, &screen_services_mepl($uid, $code, $accno,$tap, $reportscript,"21");
        return @screenoutput;
}
#Chandini_EPL
##### Get List of EPL Point to Multipoint services of Master#####
# Pre: uid, code, accno, tmplroot, script, level, master_id
# Post: @vals

sub screen_meplservices_of_master{
        my ($uid, $code, $accno, $tmplroot, $script, $lvl,$master_id,$master_lb) = @_;
        my (@allservices, $svcids, %svclabel_list);
        # get attribute of accno
        my $attribute = CMS_OLSS::is_wholesaler($accno);
                eval {
                        $svcids = CMS_OLSS::get_services_of_master($accno,$etproduct,$master_id);
                };
                foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
                        $svclabel_list{$$svcref[0]} = $$svcref[3];
                }
	my $master_display;
	if ($master_lb) {
		#$master_id = "$master_lb ($master_id)";	
		$master_display = "$master_lb ($master_id)";
	} else {
		#$master_id = $master_id;
		$master_display = $master_id;
	}
        my @screenoutput = ();
	#push @screenoutput, "<b class=header>&nbsp Master Service ID Selected:</b> $master_id<p><ol>\n";
	push @screenoutput, "<b class=header>&nbsp Master Service ID Selected:</b> $master_display<p><ol>\n";
        push @screenoutput, "<b class=header>&nbsp Select a Service:</b><p><ol>\n";
        my $encrptp = ($encet) ? $encet : "";
        foreach my $k (@allservices) {
                my $service_id = $k;
                my $svclabel = $svclabel_list{$k};
                my $x = ecrypt($k);
	 	my $lvl =21;	
                my $href_begin = "<li class=\"text\"><a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&et=$encrptp&master_srv=$master_id\">";
                my $href_end = "</a></li>\n";
                my $t = "";
                $t .= $href_begin;
        if ($svclabel) {
                $svclabel =~ s/\^//g;
                $t .= "$svclabel ($k)\n";
        } else {
                $t .= "$k\n";
        }
        push @screenoutput, "$t$href_end";
        }
	push @screenoutput, "</ol></ol>\n";
	push @screenoutput, "<br><b>&nbsp Generate Traffic Report for this Master Service:</b><p>";
	my $encry_master = ecrypt($master_id);
	my $href_begin = "&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$encry_master&level=$lvl&et=$encrptp\">";
	my $y.=$href_begin;
	#$y.= "$master_id\n";
	$y.= "$master_display\n";
	$y.= "</a>\n";
	push @screenoutput, "$y";
        return @screenoutput;
}

#Chandini_EPL
##### Get List of EPL Point to Multipoint services #####
# Pre: uid, code, accno, tmplroot, script, level
# Post: @vals

sub screen_services_mepl{
        my ($uid, $code, $accno, $tmplroot, $script, $lvl) = @_;
	open(eplFH,">/data1/tmp_log/Chand_epll2");	
        #print eplFH "inside screen services mepl: $uid, $code, $accno, $tmplroot, $script, $lvl \n";
        my (@allservices, $svcids, %svclabel_list);
        # get attribute of accno
        my $attribute = CMS_OLSS::is_wholesaler($accno);
                #if wholesaler, want to see all sub-customers
        if($attribute =~ /^1$/i) {
                eval {
                        #$svcids = CMS_OLSS::get_epl_services_wholesaler($accno);
			$svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno($accno,$etproduct);
                };
                foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
                        $svclabel_list{$$svcref[0]} = $$svcref[5];
                }
        }else {
                eval {
                        #$svcids = CMS_OLSS::get_epl_services($accno);
			$svcids = CMS_OLSS::get_serviceid_for_accno($accno,$etproduct);
                };
                foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
                        $svclabel_list{$$svcref[0]} = $$svcref[3];
                }
        }
        my @screenoutput = ();
        push @screenoutput, "<br><b class=header>&nbsp Select a Service:</b><p><ol>\n";
        my $encrptp = ($encet) ? $encet : "";
        foreach my $k (@allservices) {
                my $service_id = $k;
		$service_id =~ s/\s//g;
                my $sstrng = substr($service_id,13,2);
                #print eplFH "service_id:$service_id\n";
                if ($sstrng eq "ew") {
                        next;
                }
		#print eplFH "service_id:$service_id\n";
                my $svclabel = $svclabel_list{$k};
		#print eplFH "svclabel :$svclabel \n";
                my $x = ecrypt($k);
		my (@m_id, @all_services);
		print eplFH "accno: $accno\n";
	 	if($attribute =~ /^1$/i) {	
			eval {
                        	#$m_id = CMS_OLSS::get_mepl_masterid($k);
				$m_id = CMS_OLSS::get_masterid($k,$etproduct);
                	};
		} else {
			eval {
                        	$m_id = CMS_OLSS::get_masterid_wholesaler($k,$etproduct);
                	};
		}
		foreach my $m_id_ref (@$m_id) {
                        push @all_services, $$m_id_ref[0];
                        $svclabel_list{$$svcref[0]} = $$svcref[1];
                }
		my $master_id = $all_services[0];
		#print eplFH "master_id: $master_id\n";
                my $href_begin = "<li class=\"text\"><a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&et=$encrptp&master_srv =$master_id\">";
                my $href_end = "</a></li>\n";
                my $t = "";
                $t .= $href_begin;
        	if ($svclabel) {
                	$svclabel =~ s/\^//g;
                	$t .= "$svclabel($k)\n";
        	} else {
                	$t .= "$k\n";
        	}
        	push @screenoutput, "$t$href_end";
        }
        return @screenoutput;
}

sub screen_agg_services {
        my ($uid, $code, $accno, $script, $lvl) = @_;
        my (@agg_services, @agg_service_full, @bgp_ip, $svcids, $whlattr);

        my $pcet = ($etproduct) ? $etproduct : "GIA";
        my $encrptp = ($encet) ? $encet : "";

        my @screenoutput = ();
        my $i = 0;

        eval {
                $svcids = CMS_OLSS::get_aggregate_by_acc($accno, $pcet);
                foreach my $svcref (@$svcids) {
                        push @agg_services, $$svcref[0];
                        push @agg_service_full, $$svcref[1];
                }
        };

        if ($#agg_services >= 0) {
                push @screenoutput, "<b class=header>&nbsp Aggregate service:</b><p><ol>\n";
                foreach my $k (@agg_service_full) {
                        my $service_id = $k;

                        my $x = ecrypt($k);
                        my $href_begin = "<li class=\"text\"><a class=\"text\" href=\"/cgi-bin/$script?code=$code&svc=$x&level=$lvl&et=$encrptp\">";
                        my $href_end = "</a></li>\n";
                        my $t = "$href_begin $agg_services[$i] $href_end";

                        push @screenoutput, $t;
                        $i++;
                }
        }
        return @screenoutput;
}

##### Get HTML List of aggregate iservices #####
# Pre: uid, code, accno
# Post: @vals
sub screen_agg_services_aggrmast {
        my ($uid, $code, $accno, $script, $lvl) = @_;
        my (@agg_services, @agg_service_full, @agg_svclabel,$svcids, $whlattr);

        my $pcet = ($etproduct) ? $etproduct : "GIA";
        my $encrptp = ($encet) ? $encet : "";

        my @screenoutput = ();
        my $i = 0;
	my $serv;

        eval {
                $svcids = CMS_OLSS::get_aggregate_by_acc_aggrmast($accno, $pcet);
		my $iswholesaler = CMS_OLSS::is_wholesaler($accno);
                foreach my $svcref (@$svcids) {
                        push @agg_services, $$svcref[0];
                        push @agg_service_full, $$svcref[1];
			if($iswholesaler =~ /^1$/) {
				 push @agg_svclabel, $$svcref[3];
			} else{	
				push @agg_svclabel, $$svcref[2];
                	}
		}
        };

        if ($#agg_services >= 0) {
		my $count =0;
                foreach my $k (@agg_service_full) {
                        my $service_id = $k;
			if ($agg_svclabel[$count] ne '') {
				$serv ="$agg_svclabel[$count] ($agg_services[$count])";
			}
			else {
			    	$serv ="$agg_services[$count]";
			}
                        my $x = ecrypt($k);
			my $href_begin = "<option value='$x'>$serv";
			my $href_end = "</option>\n";
                        my $t = "$href_begin$href_end";
                        push @screenoutput, $t;
			$count++;
                        $i++;
                }
        }
        return @screenoutput;
}

# Generate list of services
# Pre: accno, server-flag (if 1 then add list of server services), label-flag (if 1 then display customer service label)
# Post: %list
#Chandini_Admin_Search
sub list_services {
print DEBUG "in sub list_services\n";
        my ($accno, $server, $label, $search_label) = @_;
print DEBUG "$accno, $server, $label, search_label:$search_label\n";
print DEBUG1 "$accno, $server, $label, search_label:$search_label\n";
        my ($x, $svcids, @allservices, $whlattr);

	##### Modified on 17 Apr 2007
	my (%svclabel_list, %serviceidalt_list);
	#####
        
        # get attribute of accno
        my $attribute = CMS_OLSS::is_wholesaler($accno);

        #print "<P>DEBUG ($attribute) ($accno)\n";       

        if($attribute =~ /^1$/i) {
                # if wholesaler, want to see all sub-customers
#Chandini_Admin_Search
	if($search_label) {
		eval {
                        $svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno($accno, $etproduct,$search_label);
                };
print DEBUG "in if of search_label ,Wholesaler svcids : $svcids\n";
print DEBUG1 "in if of search_label ,Wholesaler svcids : $svcids\n";
	}else{
print DEBUG "in else of search_label ,Wholesaler svcids : $svcids\n";
print DEBUG1 "in else of search_label ,Wholesaler svcids : $svcids\n";
                eval {
                        $svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno($accno, $etproduct);
                };
	}

		 foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];

			##### Modified on 17 Apr 2007
			if ($label =~ /^1$/) {
				$svclabel_list{$$svcref[0]} = $$svcref[4];
			} else {
				$svclabel_list{$$svcref[0]} = $$svcref[5];
			}
			$serviceidalt_list{$$svcref[0]} = $$svcref[6];
			#####
                }

        } else {
		if($search_label) {
print DEBUG "in if of search_label ,non-wholesaler\n";
print DEBUG1 "in if of search_label ,non-wholesaler\n";
                	eval {
				$svcids = CMS_OLSS::get_serviceid_for_accno($accno, $etproduct,$search_label);
			};
		} else {
print DEBUG "in else of search_label ,non-wholesaler\n";
print DEBUG1 "in else of search_label ,non-wholesaler\n";
                	eval {
                        	$svcids = CMS_OLSS::get_serviceid_for_accno($accno, $etproduct);
                	};
		}

	         foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];

			##### Modified on 17 Apr 2007
			$svclabel_list{$$svcref[0]} = $$svcref[3];
			$serviceidalt_list{$$svcref[0]} = $$svcref[4];
			#####
                }
	}
        
	my %list;
#Chandini_Admin_search
my $count =0; 
        foreach my $k (@allservices) {
	$count = $count +1;
print DEBUG "allservices each value: $k\n";
print DEBUG "count :$count\n"; 
 print DEBUG1 "allservices each value: $k\n";
print DEBUG1 "count :$count\n";
		$x = ecrypt($k);

		##### Modified on 17 Apr 2007
		my $svclabel = $svclabel_list{$k};
		#my $svclabel = &get_svclabel($accno, $k, $label);
		#####

		#print "<p> svc ($k) ($svclabeldb) (@svclabel)\n";
#		if($svclabel) {
#			$svclabel =~ s/\^//g;
#			$list{$x} = "$svclabel ($k)";
#		} else {
#			$list{$x} = "$k";
#		}

		##### Modified on 17 Apr 2007
		my $serviceid_alt = $serviceidalt_list{$k};
		#my $serviceid_alt = &get_serviceid_alt($accno, $k, 0);
		#####

                if (($svclabel) && ($serviceid_alt)) {
                        $svclabel =~ s/\^//g;
                        $list{$x} = "$svclabel ($serviceid_alt) ($k)";
                } elsif ($svclabel) {
                        $svclabel =~ s/\^//g;
                        $list{$x} = "$svclabel ($k)";
                } elsif ($serviceid_alt) {
                        $list{$x} = "$serviceid_alt ($k)";
                } else {
                        $list{$x} = "$k";
                } 
        }

        # This gives server services
        if($server) {
                foreach my $ss (@server_services) {
                        $x = ecrypt($ss);
			$list{$x} = "$ss";
                }
        }

        return \%list;
}


# Generate list of services
# Pre: accno, server-flag (if 1 then add list of server services), label-flag (if 1 then display customer service label)
# Post: %list
#Chandini_IPT-20100729
sub list_services_aggrmast {
        my ($accno, $server, $label, $mastersearch_label) = @_;
        my ($x, $svcids, @allservices, $whlattr);

	my (%svclabel_list);
        
        # get attribute of accno
        my $attribute = CMS_OLSS::is_wholesaler($accno);
open(FH1,">/data1/tmp_log/Chand_FH1");

        if($attribute =~ /^1$/i) {
                # if wholesaler, want to see all sub-customers
#Chandini_IPT-20100729
#For administration: screening the services which has the contain the search label
		if ($mastersearch_label){
                	eval {
                        	$svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno_aggrmast($accno, $etproduct, $mastersearch_label);
                	};
		} else {
print FH1 "inside no label : accno:$accno, etproduct:$etproduct\n";
			eval {
				$svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno_aggrmast($accno, $etproduct);
			};
print FH1 "svcids:@$svcids\n";
		}

		 foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];

			if ($label =~ /^1$/) {
				$svclabel_list{$$svcref[0]} = $$svcref[1];
			} else {
				$svclabel_list{$$svcref[0]} = $$svcref[2];
			}
                }
print FH1 "allservices:@allservices\n";
        } else {
#Chandini_IPT-20100729
#For administration: screening the services which has the contain the search label: for a non-wholesaler account
		if ($mastersearch_label){
                	eval {
                        	$svcids = CMS_OLSS::get_serviceid_for_accno_aggrmast($accno, $etproduct, $mastersearch_label);
                	};
		} else {
			eval {
				$svcids = CMS_OLSS::get_serviceid_for_accno_aggrmast($accno, $etproduct);
			};
		}

	         foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
			$svclabel_list{$$svcref[0]} = $$svcref[1];
                }
	}
        
	my %list;
        foreach my $k (@allservices) {
		$x = ecrypt($k);

		my $svclabel = $svclabel_list{$k};

                if ($svclabel) {
                        $svclabel =~ s/\^//g;
                        $list{$x} = "$svclabel ($k)";
                } else {
                        $list{$x} = "$k";
                } 
        }

        # This gives server services
        if($server) {
                foreach my $ss (@server_services) {
                        $x = ecrypt($ss);
			$list{$x} = "$ss";
                }
        }

        return \%list;
}

# Generate list of primary dns record type
# Pre: 
# Post: %list
sub list_pridns_type {
	my ($x, $svcids, @allservices, $whlattr);
	my ($result);
	my %list;

## 	eval {
## 		$result = CMS_OLSS::get_pridns_type();
## 	};

	# $result = CMS_OLSS::get_pridns_type(0, 'ALL');
	$result = CMS_OLSS::get_pridns_type(@_);
	#print "list_pridns_type result: ($result) ";
	for my $row (@$result) {
		my $id = $$row[0];
		my $type = $$row[1];
		$list{$type} = $id;
	}
        return \%list;

##	foreach my $svcref (@$svcids) {
##		push @allservices, $$svcref[0];
##
##			##### Modified on 17 Apr 2007
##			$svclabel_list{$$svcref[0]} = $$svcref[3];
##			$serviceidalt_list{$$svcref[0]} = $$svcref[4];
##			#####
##                }
##        
##                  
##        foreach my $k (@allservices) {
##		$x = ecrypt($k);
##
##		##### Modified on 17 Apr 2007
##		my $svclabel = $svclabel_list{$k};
##		#my $svclabel = &get_svclabel($accno, $k, $label);
##		#####
##
##		#print "<p> svc ($k) ($svclabeldb) (@svclabel)\n";
###		if($svclabel) {
###			$svclabel =~ s/\^//g;
###			$list{$x} = "$svclabel ($k)";
###		} else {
###			$list{$x} = "$k";
###		}
##
##		##### Modified on 17 Apr 2007
##		my $serviceid_alt = $serviceidalt_list{$k};
##		#my $serviceid_alt = &get_serviceid_alt($accno, $k, 0);
##		#####
##
##                if (($svclabel) && ($serviceid_alt)) {
##                        $svclabel =~ s/\^//g;
##                        $list{$x} = "$svclabel ($serviceid_alt) ($k)";
##                } elsif ($svclabel) {
##                        $svclabel =~ s/\^//g;
##                        $list{$x} = "$svclabel ($k)";
##                } elsif ($serviceid_alt) {
##                        $list{$x} = "$serviceid_alt ($k)";
##                } else {
##                        $list{$x} = "$k";
##                } 
##        }
##
##        # This gives server services
##        if($server) {
##                foreach my $ss (@server_services) {
##                        $x = ecrypt($ss);
##			$list{$x} = "$ss";
##                }
##        }
## 
##         return \%list;
}


# Generate list of primary dns status
# Pre: search_by, value
# Post: %list
sub list_pridns_status {
	my ($x, $svcids, @allservices, $whlattr);
	my ($result);
	my %list;

	$result = CMS_OLSS::get_pridns_status(@_);
	#print "list_pridns_status result: ($result) <br>";
	foreach my $row (@$result) {
		# my $id = $$row[0];
		# my $type = $$row[1];
		# $list{$type} = $id;
		#Modified 13 Sep 2012
		my $type = $$row[1];
		my $id   = $$row[0];
		#print "list_pridns_status result detail: (type:$type id:$id) <br>";
		$list{$id} = $type;
	}
        return \%list;
}

#### Account Admin ####
#Pre: $uid, $code, $accno, $tmplroot, $accscript, level
#Chandini_IPT-20100729
#Chandini_Admin_Search
sub screen_acc {	# Added by Matthew - Oct, 2007
        my ($uid, $code, $accno, $tmplroot, $accscript, $level,$service_label,$customer_label,$masterservice_label, $mastercustomer_label) = @_;
#       my $ctrlfile = $tmplroot."/local/account.htm";
        my @retvals = ();
        my ($techcontact, $outagecontact, $ctrlfile);
	open(DEBUG, ">/data1/tmp_log/CHK_DNS_24052010");
#Chandini_Admin_Search
	my $varlist;
	if ($service_label){
        $varlist = &list_services($accno, 0, 0,$service_label);
	} else {
	$varlist = &list_services($accno, 0, 0);
	}

#Chandini_IPT-20100729
#If search varaible has been given then pass on to the function:list_services_aggrmast
	my $varlist_aggrmast;
	if ($masterservice_label){
		$varlist_aggrmast = &list_services_aggrmast($accno, 0, 0,$masterservice_label);
	} else {
		$varlist_aggrmast = &list_services_aggrmast($accno, 0, 0);
	}
 	
        #my @contacts = &retrieve($accno, $contactdb);
        my $attribute = CMS_OLSS::is_wholesaler($accno);

        # temporary until CMS db
        #my @techcontacts = &retrieve($accno, $techcdb);
        eval {
                $techcontact = CMS_OLSS::get_tech_contact_details($accno);
        };
        my $ref = $$techcontact[0];
        my @tt = @$ref;

 #        print "Content-type: text/html\n\n ($etproduct) (@accadmint)\n";

        $outagecontact = $tt[4];
        my $techemail = $tt[4];
        #my $outagecontact = $techcontacts[6];

        my $msg = "No technical contact email found. ";
	$msg .= "Please go to account administration to update your details.";

	my $pridns_typelist = &list_pridns_type();
	my @pridnstable_with_selection = &view_pridns($uid, $accno, $mrwaccno, 1);  # with rowid selection
	shift (@pridnstable_with_selection);

	# print "pridns_typelist:\n";
	# foreach my $k (keys %$pridns_typelist) {
	# 	print "k($k) v($$pridns_typelist{$k}) \n";
	# }

        # Account admin area is handled differently from others because there is
        # no second level template provided
        # Hence, only local templates are used to break things down

        my @adt;

        if ($tmplroot =~ /telstra/) {
                my $temp = pop(@accadmint);
#Chandini_IPT
	print DEBUG "before if accccccccccc etpr:$etproduct\n";
		if ($etproduct ne "IPTRANSIT") {
	print DEBUG "inside if accccccccccc etpr:$etproduct\n";
			$temp = pop(@accadmint);
		}
        }

        # print "Content-type: text/html\n\n<P>DEBUG etproduct($etproduct) tmplroot($tmplroot) accadmint(@accadmint)\n";
        push @adt, @accadmint;

        my $loginid;
        my ($result, $vfg, $upass) = &decrypt($code);
        if ($result =~ /(.+)\^\^/) {
                $loginid = $1;
        }


        my $adt_pass_remove = 0;
        if((CMS_OLSS::access_through_EP($accno) =~ /true/i) || (!CMS_OLSS::is_olss_cust_acc($loginid))) {
                shift @adt;
                $adt_pass_remove = 1;
        }

        foreach my $i (0..$#adt) {
        	# print "Content-type: text/html\n\n<P>DEBUG i($i) adt($adt[$i])\n";
                # skip change customer label feature if user is not a wholesaler
 		if ((($adt_pass_remove == 0) && ($adt[$i] =~ /^statsc2/i) && ($attribute !~ /^1$/i)) || (($adt_pass_remove ==1) && ($adt[$i] =~ /^statsc2/i) && ($attribute !~ /^1$/i)) || (($attribute =~ /^1$/i) && ($adt[$i] =~ /^pridns/i) ) ) {                       
next;
                }

                $ctrlfile = $tmplroot."/local/".$adt[$i];
		#print DEBUG "CHKING PRIDNSHTMi\n";
#               my $lvl = $i+2;
                if(-e $ctrlfile) {
                        my $xx;
                        open($xx, $ctrlfile);
                                while(my $line = <$xx>) {
                                        $line = &link_format($line, $code, $ctrlname);
                                       # $line =~ s/\*STATSCONTACT\*/$outagecontact/g;
                                        $line =~ s/\*TECHCONTACT\*/$techemail/g;

					# added by Leo on 2008-11-05
                                        # $line =~ s/\*TECHEMAIL\*/$techemail/g;
					if($line =~ /\*TECHEMAIL\*/) {
						if($techemail !~ /\w+\@\w+/) {
							$line =~ s/\*TECHEMAIL\*/$msg/g;  # remind user to enter
						} else {
							$line =~ s/\*TECHEMAIL\*/$techemail/g;
						}
					}
					if($line =~ /\*PRIDNSTYPESELECT\*/) {
						my @x;
                                                foreach my $k (sort(keys %$pridns_typelist)) {
                                                        my $tmp = "<option value='$k'>$$pridns_typelist{$k}</option>\n";
                                                        push @x, $tmp;
                                                }
                                                push @retvals, @x;
					}
                                        $line =~ s/\*ACCNO\*/$accno/g;
					$line =~ s/\*PRIDNSTABLE\*/@pridnstable_with_selection/g;

                                        $line =~ s/\*CODE\*/$code/g;
                                        $line =~ s/\*ETPRODUCT\*/$encet/g;
#                                       $line =~ s/\*LEVEL\*/$lvl/g;
                                        if($line =~ /\*SVCSELECT\*/) {
                                                foreach my $k (sort(keys %$varlist)) {
                                                        my $tmp = "<option value='$k'>$$varlist{$k}</option>\n";
                                                        push @listvals, $tmp;
                                                }
                                                push @retvals, @listvals;
                                        } elsif($line =~ /\*AGGRMASTSELECT\*/) {
                                                foreach my $k (sort(keys %$varlist_aggrmast)) {
                                                        my $tmp = "<option value='$k'>$$varlist_aggrmast{$k}</option>\n";
                                                        push @aggrmast_listvals, $tmp;
                                                }
                                                push @retvals, @aggrmast_listvals;
					} elsif ($line =~ /\*CUSSVCSELECT\*/) {
#Chandini_Admin_Search
open (DEBUG1, ">/data1/tmp_log/cutomerlabel");
						my $cusvarlist;
						if ($customer_label){
                                                $cusvarlist = &list_services($accno, 0, 1,$customer_label);
						}else {
						$cusvarlist = &list_services($accno, 0, 1);
						}
                                                foreach my $k (sort(keys %$cusvarlist)) {
                                                        my $tmp = "<option value='$k'>$$cusvarlist{$k}</option>\n";
                                                        push @cuslistvals, $tmp;
                                                }
                                                push @retvals, @cuslistvals;
                                        }  elsif ($line =~ /\*AGGRMASTCUSSVCSELECT\*/) {
#Chandini_IPT-20100729
#If search varaible has been given then pass on to the function:list_services_aggrmast
						my $cusvarlist_aggrmast;
						if ($mastercustomer_label){
							$cusvarlist_aggrmast = &list_services_aggrmast($accno, 0, 1,$mastercustomer_label);
						} else {
							$cusvarlist_aggrmast = &list_services_aggrmast($accno, 0, 1);
						}

                                                foreach my $k (sort(keys %$cusvarlist_aggrmast)) {
                                                        my $tmp = "<option value='$k'>$$cusvarlist_aggrmast{$k}</option>\n";
                                                        push @cuslistvals_aggrmast, $tmp;
                                                }
                                                push @retvals, @cuslistvals_aggrmast;
                                        } else {
                                                push @retvals, $line;
                                        }
                                }
                        close($xx);
                } else {
                        push @retvals, "<P><!-- NF -->\n";
                }
        }

        return @retvals;

}
close (DEBUG);

#Manage your services screen
#sub screen_acc {
#	my ($uid, $code, $accno, $tmplroot, $accscript, $level) = @_;
##	my $ctrlfile = $tmplroot."/local/account.htm";
#	my @retvals = ();
#	my ($techcontact, $outagecontact, $ctrlfile);
#
#	my $varlist = &list_services($accno, 0, 0);
#
#	#my @contacts = &retrieve($accno, $contactdb);
#	my $attribute = CMS_OLSS::is_wholesaler($accno);
#	
#	# temporary until CMS db
#	#my @techcontacts = &retrieve($accno, $techcdb);
#	eval {
#		$techcontact = CMS_OLSS::get_tech_contact_details($accno);
#	};
#	my $ref = $$techcontact[0];
#       my @tt = @$ref;
#
#	#print "Content-type: text/html\n\n<P>DEBUG ($etproduct) (@accadmint)\n";
#
#	$outagecontact = $tt[4];
#	my $techemail = $tt[4];
#	#my $outagecontact = $techcontacts[6];
#
#	# Account admin area is handled differently from others because there is
#	# no second level template provided 
#	# Hence, only local templates are used to break things down
#
#	my @adt;
#
#	if ($tmplroot =~ /telstra/) {
#		my $temp = pop(@accadmint);
#		$temp = pop(@accadmint);
#	}
#	
#	push @adt, @accadmint;
#	if(CMS_OLSS::access_through_EP($accno) =~ /true/i) {
#		shift @adt;
#	}
#
#	#foreach my $i (0..$#accadmint) {
#	foreach my $i (0..$#adt) {
#		# skip change customer label feature if user is not a wholesaler
#		if (($i == 2) && ($attribute !~ /^1$/i)) {
#			next;
#		}
#
#		$ctrlfile = $tmplroot."/local/".$adt[$i];
#		my $lvl = $i+2;
#	        if(-e $ctrlfile) {
#			my $xx;
#       	        open($xx, $ctrlfile);
#                 		while(my $line = <$xx>) {
#		                        $line = &link_format($line, $code, $ctrlname);
#					$line =~ s/\*STATSCONTACT\*/$outagecontact/g;
#					$line =~ s/\*TECHCONTACT\*/$techemail/g;
#                		        $line =~ s/\*CODE\*/$code/g;  
#					$line =~ s/\*ETPRODUCT\*/$encet/g;
#		                        $line =~ s/\*LEVEL\*/$lvl/g;
#					if($line =~ /\*SVCSELECT\*/) {
#						foreach my $k (sort(keys %$varlist)) {
#							my $tmp = "<option value='$k'>$$varlist{$k}</option>\n";
#							push @listvals, $tmp;
#						}
#						push @retvals, @listvals;
#					} elsif ($line =~ /\*CUSSVCSELECT\*/) {
#						my $cusvarlist = &list_services($accno, 0, 1);
#						foreach my $k (sort(keys %$cusvarlist)) {
#							my $tmp = "<option value='$k'>$$cusvarlist{$k}</option>\n";
#							push @cuslistvals, $tmp;
#						}
#						push @retvals, @cuslistvals;
#					} else {
#						push @retvals, $line;
#					}
#		                }
#               	close($xx);
#	        } else {
#       	        push @retvals, "<P><!-- NF -->\n";
#	        }
#	}
#
#        return @retvals;
# 
#}


#### Contact Us ####

# Online fault report
# Pre: uid, code, svc (encr opsh), accno, tmplroot, svcscript, fnn, opshandle, parentlevel
sub screen_onlinefault {
#        my ($uid, $code, $svc, $accno, $tmplroot, $script, $fnn, $ohandle, $level) = @_;
	my ($uid, $code, $accno, $tmplroot, $script, $level) = @_;
        my $ctrlfile = $tmplroot."/local/onlinefault.htm";
        my @retvals = ();
	my @listvals = ();

	my $coname = &getconame($accno);
                        
	my $varlist = &list_services($accno, 1, 0);

        # This is the second sub-level of contact us - level = 32
        $level = "$level"."2";
                        
        if(-e $ctrlfile) {
		my $xx;
                open($xx, $ctrlfile);
                while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
			$line =~ s/\*CONAME\*/$coname/g;
                        $line =~ s/\*CODE\*/$code/g; 
                        $line =~ s/\*LEVEL\*/$level/g;
			$line =~ s/\*SVC\*/$ohandle/g;
			$line =~ s/\*SVCCODE\*/$svc/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g;
			if($line =~ /\*SVCSELECT\*/) {
				foreach my $k (keys %$varlist) {
					my $tmp = "<option value='$k'>$$varlist{$k}</option>\n";
					push @listvals, $tmp;
				}
				push @retvals, @listvals;
			} else {
	                        push @retvals, $line;
	                }
                }
                close($xx);
        } else {
                push @retvals, "Cannot generate Online Trouble Ticket page";
        }
        
        return @retvals;
 
}      

# Log trouble ticke
# Pre: $code, $name, $ph, $email, $accno, $svc, $fault
# Post: 0 (fail), >1 (ok)
sub logTroubleTicket {
	my($code, $name, $ph, $email, $accno, $svc, $fault, $mrwaccno) = @_;

	my @retvals;
	if($mrwaccno) {
		push @retvals, 0;
		push @retvals, "<p class=header>Error Encountered</p><p class=text>This account has insufficient privileges.</p>";
		return @retvals;

	}

	my ($usercode, $vflg, $pass) = &decrypt($code);

	my %mth = ("jan" => "01", "feb" => "02", "mar" => "03", "apr" => "04", "may" => "05",
			"jun" => "06", "jul" => "07", "aug" => "08", "sep" => "09", "oct" => "10",
			"nov" => "11", "dec" => "12");
	my $res;

	if($email !~ /\w+\@\w+/) {
		push @retvals, 0;
		push @retvals, "<p class=header>Error Encountered</p><p class=text>Email format wrong.</p>";
		
		return @retvals;
	}

	my $d = gmtime(time);
	chomp($d);
	my @dvals = split(/\s+/, $d);
	$dvals[1] =~ tr/A-Z/a-z/;
	my $date = $mth{$dvals[1]}."$dvals[2]$dvals[4]";
	my $time = $dvals[3];
	$time =~ s/\://g;

	my $tsvc = $svc;
	$tsvc =~ s/\s//g;
	$tsvc =~ s/\-//g;
#	my $trackno = "$date$time";

	my $iswholesaler = CMS_OLSS::is_wholesaler($accno);
	if($iswholesaler =~ /^1$/) {
		# reseller - send to reseller
		my $mailout = &mailTroubleTicketWholesaler($name, $ph, $email, $accno, $tsvc, $fault, $mrwaccno);
		return $mailout;
	}

	# test account - do nothing		
	if($accno =~ /$testacc/i) {
		$res = 1;
	} else {

		eval {
			$res = CMS_OLSS::add_trouble_ticket($name, $ph, $email, "", $accno, $tsvc, $fault);
		};

	}

	if( ($res == 0) || ($@) ) {
		&log_entry($internalerror, "LOG TT", "$@");
		push @retvals, 0;
                push @retvals, "<p class=header>Error Encountered</p><p class=text>Internal error.</p>";
                return @retvals;
	}

	# Dont want to send if development
	#if($hostname !~ /\.in.reach.com\./) {
	if($realhost !~ /uat/i) {
		my $mailout = &mailTroubleTicket($name, $ph, $email, $accno, $tsvc, $fault);

		$mailout &&= &log_entry($logtt, "LOG_TROUBLE_TICKET", "$usercode: $name, $email, $accno, $svc");

		if($mailout) {
			return $res;
		} else {
			return 0;
		}
	} 

	return 1;

}

# Mail TT details back to cust and to reseller Helpdesk
# Pre: $name, $ph, $email, $accno, $svc, $fault
# Post: 0 (fail), 1 (ok)
sub mailTroubleTicketWholesaler {
	my ($name, $ph, $email, $accno, $svc, $fault) = @_;
        # if development platform - dont want to email cust care
        #my $host = `/bin/hostname`;
        #return 1 if($host =~ /in.reach.com/i);
        my $host = $realhost;
        return 1 if($host =~ /uat/i);

#	my $ttemail = &getWholesalerHD($accno);
	my $ttemail = CMS_OLSS::get_wholesaler_email($accno);

        # now send to Reach Cust care
        open($MAIL, "| $mailagent");
        print $MAIL "From : \"Reach OLSS System\" <olss\@net.reach.com)\n";
        print $MAIL "To : $ttemail\n";
        print $MAIL "Reply-to : olss\@net.reach.com\n";
        print $MAIL "Subject : Trouble Ticket Received\n";
        print $MAIL "\n";
        $name =~ s/\+/ /g;
        $ph =~ s/\+/ /g;
        $fault =~ s/\+/ /g;
        print $MAIL <<EOG;

The following trouble ticket has been received from OLSS:

Contact name:   $name
Contact phone:  $ph
Email:          $email  
Client Id:      $accno
Service:        $svc
          
Fault Descrition:
$fault

Thank you.

Note: This email has been automatically generated. Please do not reply to this
message as it is unattended.

EOG

        close($MAIL);


        return 1;
	

}
# Mail TT details back to cust and to reseller Helpdesk
# Pre: $uid, $accno, $dns_contact, $dns_dom, $dns_ip
# Post: 0 (fail), 1 (ok)
sub mailDNSRESOLVER_REQ {
        my ($uid, $accno, $dns_contact, $dns_dom, $dns_ip) = @_;

	my @email_ar = split('^', $uid);
	my $em_ar = shift(@email_ar);
        #my $ttemail = CMS_OLSS::get_wholesaler_email($accno);
	my $ttemail = "anjan-babu.etha\@reach.com";

        # now send to Reach Cust care
        open($MAIL, "| $mailagent");
        print $MAIL "From : \"Reach OLSS System\" <olss\@net.reach.com)\n";
        print $MAIL "To : $dns_contact\n";
        print $MAIL "Reply-to : olss\@net.reach.com\n";
        print $MAIL "Subject : $value_type DNS RESOLVER SERVICE\n";
        print $MAIL "\n";
        $name =~ s/\+/ /g;
        $ph =~ s/\+/ /g;
        $fault =~ s/\+/ /g;
        print $MAIL <<EOG;

$value_type DNS RESOLVER Service for following IP:

Username:       $em_ar
IP REQ:  	$dns_ip
Email:          $dns_contact
Account:        $accno
Hostname:       $dns_dom


Thank you.

Note: This email has been automatically generated. Please do not reply to this
message as it is unattended.

EOG

        close($MAIL);


        return 1;


}

# Mail TT details back to cust and to reseller Helpdesk
# Pre: $uid, $accno, $pdns_contact, $pdns_dom, $pdns_aip
# Post: 0 (fail), 1 (ok)
sub mailPRIMARYDNS_REQ {
        my ($uid, $accno, $pdns_contact, $pdns_dom, $pdns_aip) = @_;

        my @email_ar = split('^', $uid);
        my $em_ar = shift(@email_ar);
        #my $ttemail = CMS_OLSS::get_wholesaler_email($accno);
        my $ttemail = "karuna.ballal\@reach.com";

        # now send to Reach Cust care
        open($MAIL, "| $mailagent");
        print $MAIL "From : \"Reach OLSS System\" <olss\@net.reach.com)\n";
        print $MAIL "To : $pdns_contact\n";
        print $MAIL "Reply-to : olss\@net.reach.com\n";
        print $MAIL "Subject : $value_type PRIMARY DNS SERVICE\n";
        print $MAIL "\n";
        $name =~ s/\+/ /g;
        $ph =~ s/\+/ /g;
        $fault =~ s/\+/ /g;
        print $MAIL <<EOG;

$value_type PRIMARY DNS Service for following IP:

Username:       $em_ar
IP REQ:         $pdns_aip
Email:          $pdns_contact
Account:        $accno
Hostname:       $pdns_dom


Thank you.

Note: This email has been automatically generated. Please do not reply to this
message as it is unattended.

EOG

        close($MAIL);


        return 1;


}


# Mail trouble ticket details back to customer
# Pre: $name, $ph, $email, $accno, $svc, $fault
# Post: 0 (fail), 1 (ok) 
sub mailTroubleTicket {
	my ($name, $ph, $email, $accno, $svc, $fault) = @_;

	# First mail back to customer
	my $MAIL;
	open($MAIL, "| $mailagent") or return 0;
	print $MAIL "From : \"Reach OLSS System\" <olss\@net.reach.com)\n";
	print $MAIL "To : $email\n";
	print $MAIL "Reply-to : olss\@net.reach.com\n";
	print $MAIL "Subject : Trouble Ticket Received\n";
	print $MAIL "\n";
	$name =~ s/\+/ /g;
	$ph =~ s/\+/ /g;
	$fault =~ s/\+/ /g;
	print $MAIL <<EOF;
Dear $name,

The following trouble ticket has been sent to Reach Global Helpdesk for attention:

Contact name: 	$name
Contact phone: 	$ph
Email:		$email
Client Id:	$accno
Service:	$svc

Fault Descrition:
$fault

A Customer Service Officer will be in contact with you shortly.

Thank you.

Regards,

Reach Global Helpdesk

Note: This email has been automatically generated.

EOF
	close($MAIL);

	# if development platform - dont want to email cust care
	#my $host = `/bin/hostname`;
	#return 1 if($host =~ /in.reach.com/i);
        my $host = $realhost;
        return 1 if($host =~ /uat/i);

	# now send to Reach Cust care
	open($MAIL, "| $mailagent");
        print $MAIL "From : \"Reach OLSS System\" <olss\@net.reach.com)\n";
        print $MAIL "To : $ttemail\n";
        print $MAIL "Reply-to : olss\@net.reach.com\n";
        print $MAIL "Subject : Trouble Ticket Received\n";
        print $MAIL "\n";
        $name =~ s/\+/ /g;
        $ph =~ s/\+/ /g;
        $fault =~ s/\+/ /g;
        print $MAIL <<EOG;

The following trouble ticket has been received from OLSS:
        
Contact name:   $name
Contact phone:  $ph
Email:          $email
Client Id:      $accno
Service:        $svc

Fault Descrition:
$fault
         
This trouble ticket can be viewed through CMS (http://wanchai.telstraglobal.net/).
        
Thank you.
        
Note: This email has been automatically generated.
        
EOG

	close($MAIL);


	return 1;

}

# Screen Trouble Ticket once Trouble Ticket is logged
# Pre: trackno, accno
# Post: @screenvals
sub screenTroubleTicket {
	my ($trackno, $accno) = @_;

	my @screen;

	push @screen, "<p class=header>Trouble Ticket Received</p>\n";
	push @screen, "<p class=text>The trouble ticket has been received.\n";
	push @screen, "<p><hr><p class=text>\n";

	my $curr;
	eval {
		$curr = CMS_OLSS::get_current_trouble_tickets_for_account($accno);
	};

	#push @screen, "<p> debug ($@) ($#{@$curr})\n";
	if( ($@) || ($#{@$curr} < 0) ) {
		&log_entry($internalerror, "TT SCREEN", "$@");
		return @screen;
	}

	push @screen, "<p class=header>Current Trouble Tickets</p>\n";
	push @screen, "<p class=text><table border=1 cellpadding=3 cellspacing=3>\n";
	push @screen, "<tr><td class=thx>Date</td><td class=thx>Service</td></tr>\n";

	for my $i (0..$#{@$curr}) {
		push @screen, "<tr>\n";
		for my $j (0 .. 1) {
			shift  @{$$curr[$i]};
			push @screen, "<td class=text>$$curr[$i][$j]</td>\n";
		}
		push @screen, "</tr>\n";
	}


	return @screen;
}

# Get service label
# Pre: service_id
sub get_svclabel {
        my $accno = shift;
	my $ohandle = shift;
	my $label = shift;

	my $attribute = CMS_OLSS::is_wholesaler($accno);
	eval {
		if (($attribute =~ /^1$/i) && ($label =~ /^0$/i)) {
			$svc_label = CMS_OLSS::get_reseller_label_for_serviceid($ohandle);
		} else {
			$svc_label = CMS_OLSS::get_label_for_serviceid($ohandle);
		}
       	};
	return $svc_label;

##	my @svclabel = &retrieve($ohandle, $svclabeldb);
#
#	if($svclabel[0]) {
#		return $svclabel[0];
#	}
#	return 0;
}

# Get service id alternative
# Pre: service_id
sub get_serviceid_alt {
        my $accno = shift;
        my $ohandle = shift;
        my $label = shift;

        my $serviceid_alt;
        eval {
                $serviceid_alt = CMS_OLSS::get_serviceid_alt_for_serviceid($ohandle);
        };
        return $serviceid_alt;
}

##### Routing Service #####
# routing - sublevel 21 
# Pre: uid, code, accno, tmplroot, fnn, opshandle
sub screen_route {
  $logger->debug("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Inside screen_route in in tgw lib pl file for VIEW!!!!!!!!!!!!!!!!!!!");
        my ($uid, $code, $accno, $tmplroot, $fnn, $ohandle) = @_;
 $logger->debug("Parameters passed ::::uid>>>$uid,code>> $code,accno>>>> $accno,tmplroot>>> $tmplroot,fnn>>>>> $fnn,ohandle>>>>> $ohandle");

       # my $ctrlfile = $tmplroot."/local/routing.htm";

        #my @retvals = ();
my %json;
       # my $svccode = &ecrypt($ohandle);
       # my $svclabel = &get_svclabel($accno, $ohandle, 0);
        #$service = ($svclabel) ? "$svclabel ($ohandle)" : $ohandle;

	#IPT_ph5
open (DEBUG_dis, ">/data1/tmp_log/diable");
print DEBUG_dis "service:$service and ohandle:$ohandle\n";
$logger->debug("service:$service and ohandle:$ohandle\n");
       
        my $as_no_array = CMS_OLSS::get_as_by_opshandle($ohandle, "all");
        my $as_no = $as_no_array->[0][0];
print DEBUG_dis "as_no:$as_no\n";
$logger->debug("as_no:$as_no\n");
  
print DEBUG_dis "disp_type: $disp_type\n";
        #passing the disp_type parameter to the function
$logger->debug("passing the disp_type parameter to the function >>>>>sub screen_printroutetable \n");
        my @route_table = &screen_printroutetable($uid, $code, $accno, $tmplroot, $fnn, $ohandle);
	   
             

		#push @retvals, @route_table;
	#end IPT_ph5

        #my $build_time = next_build_time();

        #if(-e $ctrlfile) {
                #my $xx;
		#my $prefcount = 0;
            #    open($xx, $ctrlfile);
             #   while((my $line = <$xx>)) {
              #          $line = &link_format($line, $code, $ctrlname);
               #         $line =~ s/\*SERVICE\*/$service/g;
                #        $line =~ s/\*CODE\*/$code/g;
                 #				 $line =~ s/\*SVCCODE\*/$svccode/g;
                  #      $line =~ s/\*ROUTETABLE\*/@route_table/g;
                   #     $line =~ s/\*BUILDTIME\*/$build_time/g;
			#$line =~ s/\*ETPRODUCT\*/$encet/g;
		#IPT_ph5
		#$line =~ s/\*EDITABLE\*/$disp_type/g;
		#02 Feb
               #for disabling/hideing the maintained by and emailid fields in the template
               #Reading values from config file for Account number validation
         #      my $configFile = "./var/data/conf/ExcludeAccnoList.cfg";
          #     my $noValidation = "NO";
           #    my @accno_list;
            #   open(CONFIG, "$configFile") or die "can't open $configFile: $!";
             #  while (<CONFIG>) {
              #     chomp;                  # no newline
               #    s/#.*//;                # no comments
                #   s/^\s+//;               # no leading white
                 #  s/\s+$//;               # no trailing white
                  # next unless length;     # anything left
                   #push(@accno_list,$_);
               #}
               #close CONFIG;
my $logger_line=join(", ", @route_table);			  
	$logger->debug("view details >>>>>$logger_line\n");		
        return @route_table ;
}

# Pre: uid, code, accno, tmplroot, fnn, opshandle
sub screen_dochroute {
	my ($uid, $code, $accno, $tmplroot, $fnn, $ohandle) = @_;
#print "Content=type: text/html\n\n";
#print "<P>($uid, $code, $accno, $tmplroot, $fnn, $ohandle)\n";
	my $ctrlfile = $tmplroot."/local/routing.htm";
	my @retvals = ();

	my $svccode = &ecrypt($ohandle);

	my $svclabel = &get_svclabel($accno, $ohandle, 0);
	$ohandle = ($svclabel) ? "$svclabel ($ohandle)" : $ohandle;

        if(-e $ctrlfile) {
		my $xx;
                open($xx, $ctrlfile);
                while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                       	$line =~ s/\*SERVICE\*/$ohandle/g;
                        $line =~ s/\*CODE\*/$code/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g;
                        $line =~ s/\*LEVEL\*/211/g;
			$line =~ s/\*SVCCODE\*/$svccode/g;
                        push @retvals, $line;
                }
                close($xx);
        } else {
		push @retvals, "Cannot generate routing page";
        }

        #push @retvals, "<p class=text><hr><p class=text>";

        my @vals1 = &screen_printroutetable($uid, $code, $accno, $tmplroot, $fnn, $ohandle);

        push @retvals, @vals1;

        return @retvals;
}

sub next_build_time {
	my $job_start_hr_gmt = 11;  # routing management backend job in server start at 11:00 GMT
        my @current = gmtime(time);
        # if ($current[2] >= 7) {
        if ($current[2] >= $job_start_hr_gmt) {
          @current = gmtime(time+86400);
        } 
	if ($current[6] == 6) {
          $current[6] = 0;
        }
        $current[0] = 0;
        $current[1] = 0;
        # $current[2] = "7";
        $current[2] = "$job_start_hr_gmt";
        return gmtime(&timelocal(@current));
}


 #IPT_ph5 # collected the new arguments(email_id, $maintained_by, $prefix_type, $route)
 # Pre: $uid, $code, $accno, $level, $opshhaandle [,$route]
 # Post: @retvals, 0
#25 Jan
 sub process_route {
	#Added new varilable customer_origin for storing origin-CRQ9161
        #my($uid, $code, $accno, $level, $opshandle, $route) = @_;
        my($uid, $code, $accno, $level, $opshandle, $route, $email_id, $maintained_by, $prefix_type, $customer_origin, $empty_file_flag,$ip_version,$role) = @_;	       
		
        SWITCH: {
                ($level == 211) && do {
		#IPT_ph5
		#Added new varilable customer_origin for storing origin-CRQ9161
                        @vals = &screen_changeroute($uid, $code, $accno, $opshandle, 1, $route, $email_id, $maintained_by, $prefix_type, $customer_origin, $empty_file_flag,$ip_version,$role);
			 $logger->debug("Inside tgwcisslib pl file output of sub screen_changeroute >>>>>>>>>>@vals\n"); 
				last SWITCH;
                };
                ($level == 212) && do {
			###my $opshandle = 'SLD-0012';	     
      
                        @vals = &screen_changeroute($uid, $code, $accno, $opshandle, 0, $route);
                        last SWITCH;
                };
        };
	 
		  
		  
        return @vals;
}

# Add/delete routes
# Pre: $uid, $code, $accno, $opshandle, $add, $route
# Post: failmsg, okmsg
#IPT_ph5 # collected the new arguments(email_id, $maintained_by, $prefix_type, $route)
#25 Jan
sub screen_changeroute {
       
	my ($uid, $code, $accno, $opshandle, $add, $route, $email_id, $maintained_by, $prefix_type, $customer_origin, $empty_file_flag,$ip_version,$role) = @_;
            $logger->debug("Inside tgwcisslib pl file IN  sub screen_changeroute >>>>>>>>>>$uid, $code, $accno, $opshandle, $add, $email_id, $maintained_by, $prefix_type, $customer_origin, $empty_file_flag,$ip_version\n");
        my @retval = ();
	
	
        my $addmsg;
        my $next_build = next_build_time();
	
		my $reset_msg  = "Please remember to reset your BGP session after we update your filter list.";
        my $rs;
	if ($debug) {print IPT_RM "arguments:uid:$uid\taccno:$accno\topshandle:$opshandle\tadd:$add\troute:$route\temail_id:$email_id\tmaintained_by:$maintained_by\tprefix_type:$prefix_type\n";
}

        if ($add == 1) {
                $addmsg = "Add";
		
	        if ($empty_file_flag == 1){
			
 $logger->debug("Error - Uploaded file cannot be empty \n");
				push(@retval, {Error => "Error - Uploaded file cannot be empty."});
				  
			return @retval;
			
			
		}

		
 $logger->debug("arguments passed to dochnageroute: $uid, $code, $accno, $opshandle, $add, $route, $email_id, $maintained_by, $prefix_type, $customer_origin,$ip_version\n");   
		my @result = &dochangeroute ($uid, $code, $accno, $opshandle, $add, $route, $email_id, $maintained_by, $prefix_type, $customer_origin,$ip_version,$role);
 $logger->debug("output from dochnageroute >>>>>>>>>$result[0] \n");      
          		

    
                if($result[0] == 1) {
                        # Route action succeeded
 $logger->debug("route will be added \n"); 
			if ($prefix_type eq 'Single')
			{
				#$rs .= "$addmsg network prefix $route is successful.</p>";
				#Changed on 6Jan2011 by Karuna Ballal for IPT5
$logger->debug("$addmsg network prefix $route is successful.\n"); 
				push(@retval, {success_message => "$addmsg network prefix $route accepted. An email notification on the status of the prefix update request will be sent within the next 2 business days."});
								
			} else
			{
				#$rs .= "$addmsg network prefix list is successful.</p>";
$logger->debug("$addmsg network prefix list is successful.\n"); 
				#Changed on 6Jan2011 by Karuna Ballal for IPT5
					push(@retval, {success_message => "$addmsg network prefix list accepted. An email notification on the status of the prefix update request will be sent within the next 2 business days."});
						#$rs .= "$addmsg network prefix list accepted. An email notification on the status of the prefix update request will be sent within the next 2 business days.";
			}
                        #$rs .= $reset_msg;
                } else {
                        # Route action failed
			if ($prefix_type eq 'Single')
			{
$logger->debug("$result[1] $addmsg network prefix $route has failed.\n"); 
			push(@retval, {Error => "$result[1] $addmsg network prefix $route has failed."});
		
			
			} 
			else
			{
$logger->debug("$result[1] $addmsg network prefix list has failed.\n"); 
			push(@retval, {Error => "$result[1] $addmsg network prefix list has failed."});
			
			}
                }
                #$rs .= "\n<p class=text><hr><p class=text>\n";
        } elsif ($add == 0) { #Delete route
                my $action = 2;
		
					
             
		if ($debug) {print IPT_RM "screen delete_routing_request called from tgwcisslib\n";
$logger->debug("screen delete_routing_request called from tgwcisslib\n"); 
}####if debug ends here
      
				my @delete_route_id = split("\0", $route);
				my $route_id=join(" , ",@delete_route_id);
			$logger->debug("track no to be deleted : $route_id \n"); 
open (DEBUG_dis22, ">/data1/tmp_log/diable22");
print DEBUG_dis22 "delete_route_id:@delete_route_id\n";
print DEBUG_dis22 "111delete_route_id:$delete_route_id[0]\n";
print DEBUG_dis22 "222222delete_route_id:$delete_route_id[1]\n";
$logger->debug("delete_route_id:@delete_route_id\n"); 
$logger->debug("111delete_route_id:$delete_route_id[0]\n"); 
$logger->debug("222222delete_route_id:$delete_route_id[1]\n"); 
		if ($debug) {print IPT_RM "delete_route_id:$delete_route_id\n";
$logger->debug("delete_route_id:$delete_route_id\n"); 
}
###################edited by shruti
#### foreach my $route_id (@delete_route_id) { 
###			if ($debug) {print IPT_RM "DB before calling CMS_OLSS subroutine\n";} 
####			$logger->debug("DB before calling CMS_OLSS subroutine\n"); 
####			$logger->debug("value of route id being passed looopedd..... $route_id.... $action..... \n");
#####			my $opshandle ='SLD-0012';
####                      my $result  = CMS_OLSS::delete_routing_request($route_id, $opshandle, $action);
####$logger->debug("after calling CMS_OLSS subroutine>>>>>>$result\n"); 
						
######                } 
####################
				  my $result  = CMS_OLSS::delete_routing_request($route_id, $opshandle, $action);

				push(@retval, {success_message => "Delete network prefix is successful. The status of the deleted prefix request will be updated within the next 2 business days."});
        }
     	         
		return @retval;
		
}

# Add/delete routes
# Pre: $uid, $code, $accno, $tmplroot, $opshandle, $add, $route, $pref
# Post: failmsg, okmsg
sub screen_changeroute1 {
	my ($uid, $code, $accno, $tmplroot, $fnn,
		$opshandle, $add, $route, $pref) = @_;

	my @result = &dochangeroute ($uid, $code, $accno, 
					$opshandle, $add, $route, $pref);
					
	my @vals = ();
	my $addmsg = ($add) ? "Add" : "Delete";

	my $rs = "<p class=header>$addmsg Route</p><p class=text>";
	
	if($result[0] == 1) {
		# Route action succeeded
		$rs .= "$addmsg route $route is successful.</p>";
	} else {
		# Route action failed
		$rs .= "$addmsg route $route has failed.</p>";
		$rs .= "<p class=text>The error is: $result[1]</p>";
	}

	push @vals, $rs;

	push @vals, "\n<p class=text><hr><p class=text>\n";
	
	my @vals1 = &screen_dochroute($uid, $code, $accno, $tmplroot, $fnn, $opshandle);

	push @vals, @vals1;

	return @vals;
		

}

#IPT_ph5 # collected the new arguments(email_id, $maintained_by, $prefix_type, $route)
# Pre: $uid, $code, $accno, $opshandle, $add, $route
# Post: 0,failmsg; 1,okmsg
sub dochangeroute {
        #my ($uid, $code, $accno, $opshandle, $add, $route) = @_;
	#Added new varilable customer_origin for storing origin-CRQ9161
	my ($uid, $code, $accno, $opshandle, $add, $route, $email_id, $maintained_by, $prefix_type, $customer_origin,$ip_version,$role) = @_;
$logger->debug("arguments came to dochangeroute:>>>>>>>>>>>>>>>>$uid, $code, $accno, $opshandle, $add, $route, $email_id, $maintained_by, $prefix_type, $customer_origin,$ip_version \n"); 
	my ($ref, @contactvals, $email, $as, $as_no);
	#Reading values from config file for Account number validation
  
	my $noValidation = 0;
      if($role eq "INTERNAL"){
  $noValidation = 1;
	 
                }
      
           
	$logger->debug("noValidation value set after checking role: $noValidation \n");	   		   
		  
	#to make it easy to compare with radb output converting into Capital letters
	#02 Feb
	if($maintained_by){
		$maintained_by = uc($maintained_by);
	}

        #my $accno1;
        #eval {
        #        $accno1 = CMS_OLSS::get_accno_for_serviceid($opshandle);
        #};
  		$logger->debug("opshandle value  >>>>>>>>> $opshandle \n");       
	eval {
                $as = CMS_OLSS::get_as_by_opshandle($opshandle, "all");
                $as_no =  $as->[0][0];
	        };

        my $time = time;
        my @retval = ();
	#IPT_ph5  #added if condition
		
$logger->debug("maintained_by value  >>>>>>>>>$maintained_by\n");	
$logger->debug("as_no value  >>>>>>>>> $as_no\n");

	$logger->debug("IP_version value  >>>>>>>>>  $ip_version\n");
	if (($prefix_type eq "Single") && ($ip_version eq "IPv4"))
	{

$logger->debug("!!!!!!!!!!!!!!!***********entering IPv4 Route Validations***********!!!!!!!!!>>>>>$route \n");
	   
		$route =~ s/ //g;

		#if($accno1 !~ /^$accno$/) {
		#        push @retval, 0, "$addmsg route action failed. This account does not have permission to take this action.";
		#        return @retval;
		#}
$logger->debug("!!!!!!!!!!!!!!!entering Error 1!!!!!!!!! \n");
		if($route !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/) {
 $logger->debug("Error in route format. \n");
		
			push @retval, 0, "Error in route format.";
					return @retval;
					
		
		}
$logger->debug("!!!!!!!!!!!!!!!entering Error 2!!!!!!!!! >>>>>$route \n");
		if(($route =~ /\/(.*?)$/) && (($1 > 24) || ($1 < 8)) ){
		
			push @retval, 0, "Error - CIDR mask must be between /8 and /24.";
		 $logger->debug("Error - CIDR mask must be between /8 and /24. \n");
		
	return @retval;
		}
$logger->debug("!!!!!!!!!!!!!!!entering Error 3!!!!!!!!! >>>>>$route \n");
		if(($route =~ /\.(.*?)\.(.*?)\.(.*?)\/(.*?)$/) && (($4 < 24) && ($4 > 16)) ){
			if (($2 % (2 ** (24-$4))) != 0) {
			
 $logger->debug("Error - Invalid CIDR format. \n");

				push @retval, 0, "Error - Invalid CIDR format.";
	
			
					return @retval;
			}
		}
$logger->debug("!!!!!!!!!!!!!!!entering Error 4!!!!!!!!! >>>>>$route \n");
		if(($route =~ /\.(.*?)\.(.*?)\.(.*?)\//) && ($3 != 0)){
 $logger->debug("Error - only class C or shorter IP block is allowed. \n");
	
			push @retval, 0, "Error - only class C or shorter IP block is allowed.";
			
			return @retval;
		}
$logger->debug("!!!!!!!!!!!!!!!entering Error 5!!!!!!!!! >>>>>$route \n");
		if($route =~ /^(139\.130\.|203\.50\.([0-5])\.)/) {
			
 $logger->debug("Error - route range prohibited. \n");
			push @retval, 0, "Error - route range prohibited.";
			
			
			
		return @retval;
		}

		#if($route =~ /^(0\.|10\.|24\.|39\.|127\.|128\.0\.|169.254\.|191\.255\.|192\.0\.0\.|192\.0\.2\.|192\.168\.|255\.255\.255\.)|^(172\.(1[6-9]|2[0-9]|31)\.)|^(2(2[4-9]|3[0-9]|4[0-9]|5[0-5])\.)/) {
		# Removed 128.0.0.0/16 and 191.255.0.0/16 as per request from Bob
		# Removed 39.109.0.0/19 as per request from Matthew Mo
		$logger->debug("!!!!!!!!!!!!!!!entering Error 6!!!!!!!!! >>>>>$route \n");
                if($route =~ /^(0\.|10\.|24\.|127\.|169.254\.|192\.0\.0\.|192\.0\.2\.|192\.168\.|255\.255\.255\.)|^(172\.(1[6-9]|2[0-9]|31)\.)|^(2(2[4-9]|3[0-9]|4[0-9]|5[0-5])\.)/) {
				
			 $logger->debug("Request rejected - private / experimental addresses are not publicly routeable. See RFC 1918. \n");
push @retval, 0, "Request rejected - private / experimental addresses are not publicly routeable. See RFC 1918.";
			
			return @retval;
		}

		# check user is login from CMS or not
		#my $staff_id = COOKIE::get_cookie("cms_login");
		#if (not defined ($staff_id)) {
		$logger->debug("!!!!!!!!!!!!!!!entering Error 7!!!!!!!!!>>>>>$route \n");
			if (CMS_OLSS::check_reach_ip($route) == 1) {
			
 $logger->debug("Error - route fall into REACH Network IP addresss range. \n");
				push @retval, 0, "Error - route fall into REACH Network IP addresss range.";
				
					return @retval;
			}
		#}
	$logger->debug("!!!!!!!!!!!!!!!entering Error 8!!!!!!!!!>>>>>$route \n");
		if (CMS_OLSS::check_bad_host($route) == 1) {
 $logger->debug("Error - route fall into REACH bad host list.\n");
			
			push @retval, 0, "Error - route fall into REACH bad host list.";

			
			
				return @retval;
		}
	$logger->debug("!!!!!!!!!!!!!!!entering Error 9!!!!!!!!!>>>>>$route \n");
		if (CMS_OLSS::check_routing_exist_allstatus($route, 1, $opshandle) > 0) {
 $logger->debug("Error - prefix already exists in our record. \n");
			
			push @retval, 0, "Error - prefix already exists in our record.";
		

			
			return @retval;
		}
		
	} 
	elsif (($prefix_type eq "Single") && ($ip_version eq "IPv6")) 
	{

	$logger->debug("!!!!!!!!!!!!!!!*********entering IPv6 Route Validations******!!!!!!!!!>>>>>$route \n");     
  	$route =~ s/ //g;
$logger->debug("!!!!!!!!!!!!!!!entering Error 1 IPv6!!!!!!!!!>>>>>$route \n");
		if(($route =~ /\/(.*?)$/) && (($1 > 64) || ($1 < 48))  ){
		
$logger->debug("Error - CIDR mask must be between /48 and /64. \n");

			push @retval, 0, "Error - CIDR mask must be between /48 and /64.";
		
	return @retval;
		}
		
		#my $staff_id = COOKIE::get_cookie("cms_login");
		#if (not defined ($staff_id)) {
$logger->debug("!!!!!!!!!!!!!!!entering Error 2 IPv6!!!!!!!!!>>>>>$route \n");
			if (CMS_OLSS::check_reach_ip($route) == 1) {
			$logger->debug("Error - route fall into REACH Network IP addresss range. \n");

				push @retval, 0, "Error - route fall into REACH Network IP addresss range.";
				
					return @retval;
			}
		#}
$logger->debug("!!!!!!!!!!!!!!!entering Error 3 IPv6!!!!!!!!!>>>>>$route \n");
		if (CMS_OLSS::check_bad_host($route) == 1) {
			
$logger->debug("Error - route fall into REACH bad host list. \n");
			push @retval, 0, "Error - route fall into REACH bad host list.";

			
			
				return @retval;
		}
$logger->debug("!!!!!!!!!!!!!!!entering Error 4 IPv6!!!!!!!!!>>>>>$route \n");
		if (CMS_OLSS::check_routing_exist_allstatus($route, 1, $opshandle) > 0) {
			
$logger->debug("Error - prefix already exists in our record. \n");
			push @retval, 0, "Error - prefix already exists in our record.";
		

			
			return @retval;
		}
	
	} 
        # store route in DB - for preprocessing logging only - CMS does the real work
        my $result = 1; #&insert($key, $routedb, $route, $opshandle, $pref, $time, "xx");

        # pass route into the CMS system
        my $action = ($add) ? 1 : 2;
        my $actionmsg = ($add) ? "add" : "delete";
        my $result2;
          
        # if test account (trl-trial) do nothing
        if($opshandle =~ /trl\-trial/i) {
                $result2 = 1;
        } else {
		#IPT_ph5 #added if condition
		my $accno_check = 0; 
		if (($prefix_type eq "Single") && ($noValidation == 1)){
			#the variable is passed to insert fucntion to set the status as 1 so that processing is avoided.
$logger->debug("Priviledged customer single prefix upload entered \n");
			$accno_check = 1;
			eval {
			#Passing new arguement customer_orgin as null -CRQ9161
			if($customer_origin) {
			$logger->debug("!!!!!!!!!!!!!!!!!!!!!Priviledged customer single prefix upload entered customer origin present!!!!!!!!!!!!!!!!!! >>>>>>>>>$customer_origin \n");
$logger->debug("Arguments passed to DB >>>>>>>>>opshandle:$opshandle,action: $action,route: $route,as_no: $as_no,customer_origin: $customer_origin,ip_version :$ip_version \n");
			$result2 = CMS_OLSS::add_routing_request($opshandle, $action, $route, $as_no, $customer_origin,$ip_version);
$logger->debug("result of upload>>>>>>>>$result2 \n");
$logger->debug("exit upload \n");
					}	
			else
				{
$logger->debug("add single prefix if customer origin is not present \n");
$logger->debug("Arguments passed to DB >>>>>>>>>opshandle:$opshandle,action: $action,route: $route,as_no: $as_no,customer_origin: Null,ip_version :$ip_version \n");
				$result2 = CMS_OLSS::add_routing_request($opshandle, $action, $route, $as_no, NULL,$ip_version);
$logger->debug("result of upload>>>>>>>>$result2 \n");
$logger->debug("exit upload \n");
				}
			};
		}
		elsif((($prefix_type eq "Single") && ($noValidation == 0)) || ($prefix_type ne "Single")){

$logger->debug("!!!!!!!!!!!!!!!!!inside upload for single and bulk customer!!!!!!!!!!!NORMAL!!!!!!!!!!!!!!!!! \n");
							eval {
$logger->debug("Arguments passed >>>>>>>>>route:$route, prefix_type:$prefix_type,email_id: $email_id,maintained_by: $maintained_by,as_no:$as_no,opshandle: $opshandle, accno:$accno, accno_check:$accno_check,customer_origin: $customer_origin,ip_version:$ip_version \n");
                                $result2 = CMS_OLSS::insert_route_bulkupload($route, $prefix_type, $email_id, $maintained_by,$as_no, $opshandle, $accno, $accno_check, $customer_origin,$ip_version);
              
$logger->debug("result of upload >>>>>>>>$result2 \n");
$logger->debug("exit upload \n");

          };
			#CRQ000000005326 -- Jan-2012
                        if ($result2 eq "Re-entered"){
						
						$logger->debug("result returns when bulkupload returns reEntered!!!!!!!!!!!Re-entered !!!!!!!!!!!!!!! \n");       	
                                push @retval, 0, "An error was encountered. Network prefix $route could not be added because it has already been added.\n";
							 
                                
								
									return @retval;
                        }
                        #CRQ000000005326 -- Jan-2012	
		}

                if( (!$result) || ($@) ) {
                        &log_entry($internalerror, "CHROUTE", "$@");
                }
        }
        #$result2 &&= &log_entry($chroutelog, $action, "Userid: $uid, $opshandle, $route");
        if( ($result) && ($result2) ) {
$logger->debug("going for updating contact........!!!!!!! \n"); 

my $result=CMS_OLSS::update_tech_contact_details_new($accno,2,$email_id);
$logger->debug("conatct details updated successfully>>>>>.>>>>>>$result!!!!!!! \n"); 
		push(@retval, 1, {success_message => "OK."});
                #push @retval, 1, "OK.";
			$logger->debug("Successfully Added Prefix Hurray!!!!!!! \n"); 
                #&mail_reachcontacts($opshandle, $actionmsg, $route, $accno);
          	return @retval;
        }
	$logger->debug("An error was encountered. Network prefix $route could not be $actionmsg"."ed.\n");   	
        push @retval, 0, "An error was encountered. Network prefix $route could not be $actionmsg"."ed.\n";
       
	
    	return @retval;
}


# Pre: $uid, $code, $accno, $opshandle, $add, $route, $pref
# Post: 0,failmsg; 1,okmsg
sub dochangeroute1 {
	my ($uid, $code, $accno, $opshandle, $add, $route, $pref) = @_;
	
	my $accno1;
	eval {
		$accno1 = CMS_OLSS::get_accno_for_serviceid($opshandle);
	};

	my $time = time;

	my @retval = ();

        if($accno1 !~ /^$accno$/) {
        	push @retval, 0, "$addmsg route action failed. This account does not have permission to take this action.";
        	return @retval;
        }

        if($route !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/) {
        	push @retval, 0, "Error in route format.";
        	return @retval;
        }

        if($route =~ /\/3[1-2]$/) {
        	push @retval, 0, "Error - CIDR mask must be /30 or less.";
        	return @retval;
        }

        if($route =~ /^(139\.130\.|203\.50\.([0-5])\.)/) {
        	push @retval, 0, "Error - route range prohibited.";
        	return @retval;
        }

        if($route =~ /^(10\.|192\.168\.|127\.)|^(172\.(1[6-9]|2[0-9]|31)\.)/) {
        	push @retval, 0, "Error - private addresses are not publicly routeable. See RFC 1918.";
        	return @retval;
        }

	# pref not used anymore
	$pref = "normal";
        if($pref !~ /^(normal|low)$/i) {
        	push @retval, 0, "Error in preference format.";
        	return @retval;
        }

        my $key = "$opshandle-$route-$time";

        #if( ($add) && ($pref =~ /^low$/i) ) {
        #	$route = "L$route";
        #}

        #if(!$add) {
        #	$route = "D$route";
        #}
       
	# store route in DB - for preprocessing logging only - CMS does the real work 
	my $result = 1; #&insert($key, $routedb, $route, $opshandle, $pref, $time, "xx");

	# pass route into the CMS system
	my $action = ($add) ? 1 : 2;
	my $actionmsg = ($add) ? "add" : "delete";
	my $result2;


	# if test account (trl-trial) do nothing
	if($opshandle =~ /trl\-trial/i) {
		$result2 = 1;
	} else {

		eval {
			$result2 = CMS_OLSS::add_routing_request($opshandle, $action, $route);
		};
		if( (!$result) || ($@) ) {
			&log_entry($internalerror, "CHROUTE", "$@");
		}
	}

	$result2 &&= &log_entry($chroutelog, $action, "Userid: $uid, $opshandle, $route");

	if( ($result) && ($result2) ) {
		push @retval, 1, "OK.";
		&mail_reachcontacts($opshandle, $actionmsg, $route, $accno);
		return @retval;
	}

	push @retval, 0, "An error was encountered. <p class=text>Route $route could not be $actionmsg"."ed.\n";
	#."<p class=text>AA ($result) ($result2) ($@) ($opshandle) ($route) ($action) ($CMS_OLSS::error)\n";

	return @retval;

}

# Mail out routing changes request to GHD and GSMC: KC Fong Kwok-Cheong.Fong@reach.com
# Pre: $opshandle, $action, $route, $accountno
# Post: none
sub mail_reachcontacts {
	my($opshandle, $action, $route, $accountno) = @_;

	my $time = gmtime(time);

	#my $host = `/bin/hostname`;
	#return 1 if($host =~ /in.reach.com/i);
        my $host = $realhost;
        return 1 if($host =~ /uat/i);

	my $MAIL;
	open($MAIL, "| $mailagent");
        print $MAIL "From : \"Reach OLSS System\" <olss\@net.reach.com)\n";
        print $MAIL "To : $routeupdateemail\n";
        print $MAIL "Reply-to : olss\@net.reach.com\n";
	#print MAIL "Cc : rchew\@telstra.net\n";
        print $MAIL "Subject : Routing Update Received from $opshandle ($route)\n";
        print $MAIL "\n";
        print $MAIL <<EOF;

GIA Routing Update Request

A routing request has been received through OLSS from:

	Customer account number:	$accountno
	Operations handle:		$opshandle
	Route:				$route
	Action:				$action
	Date (GMT):			$time

This request is pending in CMS (http://wanchai.telstraglobal.net/).

EOF

}
	
# Print out current routing table
# Pre: $uid, $code, $accno, $tmplroot, $fnn, $opshandle
# Post: Routing Table
#IPT_ph5
sub screen_printroutetable {
  $logger->debug("!!!!!!!!!Inside screen_printroutetable !!!!!!!!!!!!"); 
        #my ($uid, $code, $accno, $tmplroot, $fnn, $opshandle) = @_;
	my ($uid, $code, $accno, $tmplroot, $fnn, $opshandle) = @_;
 $logger->debug("Parameters passed to this sub screen_printroutetable: uid:$uid,code: $code,accno: $accno,tmplroot: $tmplroot,fnn: $fnn,opshandle: $opshandle"
); 

        my $tmplfile = $ripenet."$opshandle";
		    $logger->debug("!!!!!!!!!tmplfile >>>>>>>>$tmplfile!!!!!!!!!!!!");

        my $x = 0;
        my @vals = ();
        my $as_no = CMS_OLSS::get_as_by_opshandle($opshandle, "all");

	     $logger->debug("!!!!!!!!!opshandle>>>>>$opshandle , as_no>>>>> $as_no!!!!!!!!!!!");
        #push @vals,%sucess_hash = ('Routing table for AS.'=>"$as_no->[0][0]");
		push(@vals, {AS => $as_no->[0][0]});
		

		
        #my @exist_route = CMS_OLSS::get_routing_request_by_opshandle($opshandle);$result->[0]->[0]
my $exist_route= CMS_OLSS::get_routing_request_by_opshandle($opshandle);
###############################################################################################
  if($exist_route->[0]->[0] > 0){

   $logger->debug("One");
for $i (0 .. $#{$exist_route}){
  $logger->debug("two");
                              	my $networkprefix = $exist_route->[$i][3];
							   my $action = $exist_route->[$i][2];
							   my $trackno = $exist_route->[$i][0];
							   my $timeE = $exist_route->[$i][4];
							   my $timeC = $exist_route->[$i][5];
							   my $status = $exist_route->[$i][6];
					        	$logger->debug(" for row $i >>>>trackno => $trackno,timeCompleted=> $timeC, status=> $status, prefix=> $networkprefix, action=> $action,timeEntered=>$timeE"); 

         
                        if ($action eq "Add") {
				$subnet = cal_subnet($networkprefix);
				$total_subnet +=  $subnet;
				                        } else 
										{
										
											}
                        if ($timeE =~ /^(.*?)\./)
{
$timeE = $timeE. " GMT";
 }
                        if ($timeC  eq "") 
						{
                             $timeC = " ";
                        } else 
						{
                                if ($timeC =~ /^(.*?)\./) 
								{ 
								$timeC = $timeC." GMT"; 
								}
                        }
			if ($status =~ /^(.*?)-/) 
			{ 
			$status = $1; 
			}
			
			push(@vals, {trackno => $trackno,timeCompleted=> $timeC, status=> $status, prefix=> $networkprefix, action=> $action,timeEntered=>$timeE});
$logger->debug("trackno => $trackno,timeCompleted=> $timeC, status=> $status, prefix=> $networkprefix, action=> $action,timeEntered=>$timeE");
				       
                }
				}
             else {
		
			##push(@vals, {Error => "No network prefix found."});
			return @vals;
        }
   
   

###############################################################################################
	
    
my $logger_line=join(" , ", @vals);
$logger->debug("!!!!!!!! view json >>>>>>>>>>$logger_line!!!!!!!!!!!!");
$logger->debug("!!!!!!!! EXIT from VIEW!!!!!!!!!!!!");
	 return @vals;
}

# Print out current routing table
# Pre: $uid, $code, $accno, $tmplroot, $fnn, $opshandle
# Post: Routing Table
sub screen_printroutetable1 {
	my ($uid, $code, $accno, $tmplroot, $fnn, $opshandle) = @_;

	my $tmplfile = $ripenet."$opshandle";

	my $x = 0;
	my $td = "td class=text";
	my @vals = ();

	my $tline;
 
	if(-e $tmplfile) {
		push @vals, "<p class=header>Routing table for $opshandle</p>\n";
		push @vals, "<p class=text><table border=1 cellpadding=3 cellspacing=3>\n";
		my $xx;
		open($xx, $tmplfile);
		while(my $line = <$xx>) {
			next if($line !~ /^\d{1,3}\./);
			if($x == 0) {
				push @vals, $tline;
				$tline = "<tr><$td>$line</td>";
			} else {
				$tline .= "<$td>$line</td>";
			}
			$x++;
			
			if($x == 5) {
				$tline .= "</tr>";
				$x = 0;
			}
		}
		push @vals, "</table>\n";
	} else {
		push @vals, "<p class=header>Routing table for $opshandle</p>\n";
		push @vals, "No routes found for $opshandle.";
	}

	return @vals;
}		
			
	


##### Traffic reports #####
# Pre: uid, code, accno, tmplroot, fnn, opshandle, encryp_svc
# NB: FNN not used
sub screen_trafficrpt {
################### Chandini 2########################
	open (FHR ,">/data1/tmp_log/logr");
	my ($uid, $code, $accno, $tmplroot, $fnn, $ohandle, $svc, $master_serv,$log_serv,$agg_serv,$pt, $master_lb, $log_lb,$agg_lb) = @_;
	#print FHR " Inputs at screen_trafficrpt :$uid, $code, $accno, $tmplroot, $fnn, $ohandle, $svc, $master_serv,$log_serv,$agg_serv,$pt \n ";
	print FHR " Inputs at screen_trafficrpt :$ohandle, $svc \n ";
	#print FHR " labels----> $master_lb, $log_lb \n";
        my $ctrlfile;

#Chandini_EPL
#Declared local Variable
	 my $epl_mastid;
	 my $svclabel;
##
        if ($etproduct =~ /VPLS/i ){
        	if($pt eq "TRANS"){
                	$ctrlfile=$tmplroot."/local/trafficrpts_vpls_phy.htm";
                	#print FHR "ctrlfile:---$ctrlfile \n";
                }
                elsif($pt eq "VLV"){
			if ($ohandle =~ /x-alias-/){
				$ctrlfile=$tmplroot."/local/trafficrpts_vpls_agg.htm";
			}
			elsif ($ohandle =~ /VLL/i || $ohandle =~ /VLI/i) {
				$ctrlfile=$tmplroot."/local/trafficrpts_vll_log.htm";
			}
			else {
				$ctrlfile=$tmplroot."/local/trafficrpts_vpls_log.htm";
                        }
		#if ($agg_serv eq "") {
				#print FHR "entered into else \n";
                        	#$ctrlfile=$tmplroot."/local/trafficrpts_vpls_log.htm";
                        	#print FHR "ctrlfile:----$ctrlfile \n";
                       # } 
			#else {
                        #	$ctrlfile=$tmplroot."/local/trafficrpts_vpls_agg.htm";
                        #}
                 }
		 else {
        	 $ctrlfile = $tmplroot."/local/trafficrpts.htm";
        	 }
        }

#Chandini_EPL
#Selecting the template based on the Service whether 'point to point' or 'point to multipoint' or 'master service' report.
	elsif ($etproduct =~ /EPL/i ){
	my (@m_id,@all_services,$svclabel_list);
	my $attribute = CMS_OLSS::is_wholesaler($accno);
	#print FHR "accno : $accno\n";
        #if wholesaler
        if($attribute =~ /^1$/i) {
		eval {
                	#$m_id = CMS_OLSS::get_mepl_masterid($ohandle);
			$m_id = CMS_OLSS::get_masterid($ohandle,$etproduct);			
                };
	}else {
		eval {
                	$m_id = CMS_OLSS::get_masterid_wholesaler($ohandle,$etproduct);
                };
	}
	foreach my $m_id_ref (@$m_id) {
                        push @all_services, $$m_id_ref[0];
                        $svclabel_list = $$m_id_ref[1];
        }
	if ($svclabel_list) {
                $svclabel_list =~ s/\^//g;
              	$epl_mastid = "$svclabel_list ($all_services[0])";
        } else {
		$epl_mastid = $all_services[0];
        }
	my $sid_sqez = $ohandle; 
	$sid_sqez =~ s/\s//g;
                my $sstrng = substr($sid_sqez,13,2);
                if ($sstrng eq "ew") {
			$ctrlfile=$tmplroot."/local/trafficrpts_epl.htm";
                }
		elsif ($sstrng eq "em") {
			$ctrlfile=$tmplroot."/local/trafficrpts_mepl.htm";
		}
		else{
			$ctrlfile=$tmplroot."/local/trafficrpts_mas_mepl.htm";	
                        #if wholesaler
                        if($attribute =~ /^1$/i) {
				eval {
                                	$svclabel = CMS_OLSS::get_reseller_label_for_masterservice($ohandle);
                        	};
                        }
                        else {
				eval {
					$svclabel = CMS_OLSS::get_reseller_label_for_masterservice_wholesaler($ohandle);
				};
			}
			if ($svclabel){
				$ohandle = "$svclabel ($ohandle)";
			}
		}
        }
        else {
		#print FHR "in 2nd else @@@@@@@22\n";
         	$ctrlfile = $tmplroot."/local/trafficrpts.htm";
		#print FHR "ctrlfile:---$ctrlfile \n";
        }
        my $master_details;
        my $log_details;
        my $agg_details;
        if ($master_lb) {
                        $master_lb =~ s/\^//g;
                       $master_details = "$master_lb ($master_serv)\n";
                } else {
                        $master_details = "$master_serv\n";
                }
        if ($log_lb) {
                        $log_lb =~ s/\^//g;
                       $log_details = "$log_lb ($log_serv)\n";
                } else {
                        $log_details = "$log_serv\n";
                }
        if ($agg_lb) {
                        $agg_lb =~ s/\^//g;
                       $agg_details = "$agg_lb ($agg_serv)\n";
                } else {
                        $agg_details = "$agg_serv\n";
                }
###########################End Chandini 2##############33

        my @retvals = ();

        my $opt = "<option value";
        my $startyear = "2002";
        my $date = &ctime(time);
        my @vals = split(/\s+/, $date);
        my $curyear = $vals[4];
#Chandini_EPL(commented out)
	#my $svclabel;
#Chandini_EVPL
	if ($etproduct =~ /EVPL/i ){
		 $svclabel = &get_svclabel($accno, $ohandle."a",0);
		#print FHR "ohandle in EVPL:$ohandle \n";
	}
	else {
        	 $svclabel = &get_svclabel($accno, $ohandle, 0);
	}

	#print FHR "ohandle----here----->$ohandle\n";
        if ($ohandle =~ /x-alias-/){
        $ohandle =~ s/x-alias-//g;
	$ohandle =~ s/ //g;
	#print FHR "ohandle----->$ohandle---accno----->$accno\n";
                        $svclabel = CMS_OLSS::get_aggr_label_forservice($ohandle,$accno);
        }

	#print FHR "svclabel---->$svclabel\n";
        my $serviceid_alt = &get_serviceid_alt($accno, $ohandle, 0);
	#print FHR "serviceid_alt--->$serviceid_alt\n";
        #$ohandle = ($svclabel) ? "$svclabel ($ohandle)" : $ohandle;
        if (($svclabel) && ($serviceid_alt)) {
                $ohandle = "$svclabel ($serviceid_alt) ($ohandle)";
        } elsif ($svclabel) {
                $ohandle = "$svclabel ($ohandle)";
		#print FHR "in else svclabel :$ohandle \n";
        } elsif ($serviceid_alt) {
                $ohandle = "$serviceid_alt ($ohandle)";
        }
	#print FHR "ohandle----->$ohandle================final\n";
        if(-e $ctrlfile) {
		my $xx;
                open($xx, $ctrlfile);
                 while(my $line = <$xx>) {
		#print FHR "in report file :$ohandle \n";
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*SERVICE\*/$ohandle/g;
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/211/g;
			$line =~ s/\*FNN\*/$fnn/g;
			$line =~ s/\*SVCCODE\*/$svc/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g; #encet set at the calling CGI level
#Chandini_EPL
#Displaying the Master service id in the Template
			$line =~ s/\*MASTER_SERVICE\*/$epl_mastid/g;
#### Chandini 3###########
                        $line =~ s/\*MASTER_SERV\*/$master_details/g;
                        $line =~ s/\*LOG_SERV\*/$log_details/g;
                        $line =~ s/\*AGG_SERV\*/$agg_details/g;
			$line =~ s/\*M_ID\*/$master_serv/g;
			$line =~ s/\*L_ID\*/$log_serv/g;
			$line =~ s/\*A_ID\*/$agg_serv/g;
			$line =~ s/\*M_LB\*/$master_lb/g;
			$line =~ s/\*L_LB\*/$log_lb/g;
			$line =~ s/\*A_LB\*/$agg_lb/g;
			$line =~ s/\*PT\*/$pt/g;
########### Chandini  End 3#################


			if($line =~ /\*MRTGVALS\-(\w+)\*/) {
                                my $tmpx = "<img src=\"/cgi-bin/tgwciss-reports.cgi?level=2112&code=$code&svc=$svc&pic=$1\">\n";
                                push @retvals, $tmpx;
				$line = "";
                        }

			if($line =~ /\*YEAR\*/) {
				#print "DEBUG ($startyear <= $curyear) (@vals) ($date)\n";
				while($curyear >= $startyear) {
					my $optline = "$opt=$curyear>$curyear</option>\n";
					push @retvals, $optline;
					$curyear--;
				}
				$curyear = $vals[4];
			} else {
				#print FHR " in retvals : $line \n";
	                        push @retvals, $line;
			}
                }
                close($xx);   
        } else {
                push @retvals, "Cannot generate traffic reports page";
        }
	#print FHR " in last retvals : @retvals \n";

        return @retvals;

}


##### SLA reports #####
# Pre: uid, code, accno, tmplroot, fnn, opshandle, encryp_svc
# NB: FNN not used
sub screen_slarpt {
        my ($uid, $code, $accno, $tmplroot, $fnn, $ohandle, $svc) = @_;
        my $ctrlfile = $tmplroot."/local/slarpts.htm";
        my @retvals = ();
        #print '($uid, $code, $accno, $tmplroot, $fnn, $ohandle, $svc)=';
        #print "($uid, $code, $accno, $tmplroot, $fnn, $ohandle, $svc)";

        my $opt = "<option value";
        my $startyear = "1999";
        my $date = &ctime(time);
        my @vals = split(/\s+/, $date);
        my $curyear = $vals[4];

        my $svclabel = &get_svclabel($accno, $ohandle, 0);
        my $serviceid_alt = &get_serviceid_alt($accno, $ohandle, 0);
        #$ohandle = ($svclabel) ? "$svclabel ($ohandle)" : $ohandle;
        if (($svclabel) && ($serviceid_alt)) {
                $ohandle = "$svclabel ($serviceid_alt) ($ohandle)";
        } elsif ($svclabel) {
                $ohandle = "$svclabel ($ohandle)";
        } elsif ($serviceid_alt) {
                $ohandle = "$serviceid_alt ($ohandle)";
        }

        if(-e $ctrlfile) {
                my $xx;
                open($xx, $ctrlfile);
                 while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*SERVICE\*/$ohandle/g;
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/711/g;  # 711 for sla report
                        $line =~ s/\*FNN\*/$fnn/g;
                        $line =~ s/\*SVCCODE\*/$svc/g;
                        $line =~ s/\*ETPRODUCT\*/$encet/g;

                        if($line =~ /\*MRTGVALS\-(\w+)\*/) {
                                my $tmpx = "<img src=\"/cgi-bin/tgwciss-reports.cgi?level=2112&code=$code&svc=$svc&pic=$1\">\n";
                                push @retvals, $tmpx;
                                $line = "";
                        }

                        if($line =~ /\*YEAR\*/) {
                                #print "DEBUG ($startyear <= $curyear) (@vals) ($date)\n";
                                while($curyear >= $startyear) {
                                        my $optline = "$opt=$curyear>$curyear</option>\n";
                                        push @retvals, $optline;
                                        $curyear--;
                                }
                                $curyear = $vals[4];
                        } else {
                                push @retvals, $line;
                        }
                }
                close($xx);
        } else {
                push @retvals, "Cannot generate sla reports page";
        }

        return @retvals;

} # end of sub screen_slarpt


##### Latency Performance #####
# Pre: code, tmplroot
# Post: @vals
sub screen_atmfrlatencyperf {
        my ($code, $tmplroot) = @_;
        my $ctrlfile = $tmplroot."/local/latencyperf.htm";
        my @retvals = ();

        my $opt = "<option value";
        my $startyear = "2002";
        my $date = &ctime(time);
        my @vals = split(/\s+/, $date);
        my $curyear = $vals[4];

        if(-e $ctrlfile) {
		my $xx;
                open($xx, $ctrlfile);
                 while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/91/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g; #encet set at the calling CGI level

			if($line =~ /\*YEAR\*/) {
				#print "DEBUG ($startyear <= $curyear) (@vals) ($date)\n";
				while($curyear >= $startyear) {
					my $optline = "$opt=$curyear>$curyear</option>\n";
					push @retvals, $optline;
					$curyear--;
				}
				$curyear = $vals[4];
			} else {
	                        push @retvals, $line;
			}
                }
                close($xx);   
        } else {
                push @retvals, "Cannot generate network page";
        }

        return @retvals;
}


##### Cable Paths #####
# Pre code, tmproot, Node|Primary|Alternative
# Post: @vals
sub screen_cablepaths {
	my ($code, $tmplroot, $rpttype, $datatype) = @_;

	my @retvals = ();
	my $pcet = ($etproduct) ? $etproduct : "ATM";

	if ($rpttype =~ /Node/i) {
		push @retvals, "<div id=\"tabmenu\">\n";
	        push @retvals, "<a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Node\"><span class=\"curposr\">$pcet Routing Report</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Primary\"><span>Primary Cable Paths</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Alternative\"><span>Alternative Cable Paths</span></a>\n";
		push @retvals, "</div>\n";
		push @retvals, "<div id=\"tabmenuborder\"></div>\n";
		push @retvals, &screen_cablepathrpt_by_node($code, $tmplroot);
	} elsif ($rpttype =~ /Primary/i) {
		push @retvals, "<div id=\"tabmenu\">\n";
	        push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Node\"><span>$pcet Routing Report</span></a><a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Primary\"><span class=\"curposr\">Primary Cable Paths</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Alternative\"><span>Alternative Cable Paths</span></a>\n";
		push @retvals, "</div>\n";
		push @retvals, "<div id=\"tabmenuborder\"></div>\n";
		push @retvals, &screen_cablepathrpt($code, $tmplroot, $rpttype, $datatype);
	} elsif ($rpttype =~ /Alternative/i) {
		push @retvals, "<div id=\"tabmenu\">\n";
	        push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Node\"><span>$pcet Routing Report</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Primary\"><span>Primary Cable Paths</span></a><a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Alternative\"><span class=\"curposr\">Alternative Cable Paths</span></a>\n";
		push @retvals, "</div>\n";
		push @retvals, "<div id=\"tabmenuborder\"></div>\n";
		push @retvals, &screen_cablepathrpt($code, $tmplroot, $rpttype, $datatype);
	}

	return @retvals;
}

##### Primary & Alternative Cable Paths/Latency Performance #####
# Pre: code, tmplroot, Primary|Alternative
# Post: @vals
sub screen_cablepathrpt {
	my ($code, $tmplroot, $rpttype, $datatype) = @_;

	my $ctrlfile = $tmplroot."/local/cablepathrpt.htm";
	my @retvals = ();
	my @result = ();

        if(-e $ctrlfile) {
                my $xx;
                open($xx, $ctrlfile);
                while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*ETPRODUCT\*/$encet/g;
                        $line =~ s/\*LEVEL\*/A/g;
                        $line =~ s/\*RPTTYPE\*/$rpttype/g;

			if ($datatype =~ /latency/i) {
				$line =~ s/\*PATH_CHECKED\*/onClick=\'cprpt\.submit\(\)\;\'/g;
				$line =~ s/\*LATENCY_CHECKED\*/checked/g;
				$line =~ s/\*EXPORT_EXCEL\*//g;
			} else {
				$line =~ s/\*PATH_CHECKED\*/checked/g;
				$line =~ s/\*LATENCY_CHECKED\*/onClick=\'cprpt\.submit\(\)\;\'/g;
				$line =~ s/\*EXPORT_EXCEL\*/<img border='0' src='\/tgwcustdata\/images\/excel\.gif' \/>&nbsp;<a class=text href=\"\/cgi-bin\/tgwciss-reports\.cgi\?code=$code\&level=A2\&et=$encet\">Export <span style='font-weight:bold;font-style:italic;'>Primary \& Alternative Paths<\/span> to Microsoft Excel<\/a>/;
			}

                        if($line =~ /\*DATA\*/) {
				if ($datatype =~ /latency/i) {
					@result = &gen_cablepathlatencyperf($code, $tmplroot, $rpttype);
				} else {
					@result = &gen_cablepathrpt($code, $tmplroot, $rpttype);
				}
                                push @retvals, @result;
                        } else {
                                push @retvals, $line;
                        }
                }
                close($xx);
        } else {
                push @retvals, "Cannot generate cable paths report page";
        }

	return @retvals;
}


##### Node to Node Cable Paths #####
# Pre: code, tmplroot
# Post: @vals
sub screen_cablepathrpt_by_node {
        my ($code, $tmplroot) = @_;
	my $cppath;

        my $ctrlfile = $tmplroot."/local/node_cablepathrpt.htm";

	my $pcet = ($etproduct) ? $etproduct : "ATM";
	if ($pcet =~ /ATM/i) {
		$cppath = $atmcablepath;
	} elsif ($pcet =~ /FR/i) {
		$cppath = $frcablepath;
	}

	my $cpfile = $cppath."primary_cablepaths.csv";
        my @retvals = ();
	my %pop = ();

	if ((-e $cpfile) && (-s $cpfile)) {
		my $xx;
		open($xx, $cpfile);
		while(chomp(my $line = <$xx>)) {
			my @field = split /,/, $line;
			$pop{$field[0]} = '';
		}
		close($xx);
	}

        if(-e $ctrlfile) {
		my $xx;
                open($xx, $ctrlfile);
                while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/A1/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g; #encet set at the calling CGI level

			if($line =~ /\*POPS\*/) {
				my @sortedpop = sort keys%pop;
				foreach my $p (@sortedpop) {
					my $optline = "<option value=\"$p\">$p</option>\n";
					push @retvals, $optline;
				}
			} else {
	                        push @retvals, $line;
			}
                }
                close($xx);   
        } else {
                push @retvals, "Cannot generate routing report page";
        }
        return @retvals;
}


##### Networks Command Line Access #####
# Pre: $uid, $code, $accno, $tmplroot
# Post: @vals
sub screen_CLI {
        my ($uid, $code, $accno, $tmplroot, $method, $router, $serviceid, $loginout, $status, $def_interface, $def_vrfname, $def_ipaddress, $def_mapname, $command, $parameter, $execute, $syntax, $screen, $scrollpos, $clear, $search, $sch_serviceid, $sch_interface, $svcinfo, $aendzend) = @_;

	open (OUT4, ">/data1/tmp_log/log_CLI");

	my $ctrlfile = $tmplroot."/local/CLI_access.htm";
        my @retvals = ();
	my @rtrmsg = ();
	my @schmsg = ();
####################################################################################################
# Uncomment when CLI does not support "Router Name Logon"

#	$method = "serviceid";

####################################################################################################

	$method = ($method) ? $method : "router";
       	my $pcet = ($etproduct) ? $etproduct : "GIA";

	$serviceid =~ s/\+/ /g;
	$def_ipaddress =~ s/\+/ /g;
	$def_mapname =~ s/\+/ /g;
	$command =~ s/\+/ /g;
	$parameter =~ s/\+/ /g;
	$parameter =~ s/\*PLUS_SIGN\*/\+/g;
	$syntax =~ s/\+/ /g;
	$screen =~ s/\+/ /g;
	$screen =~ s/\*PLUS_SIGN\*/\+/g;
	$sch_serviceid =~ s/\+/ /g;
	$svcinfo =~ s/\+/ /g;
	$svcinfo =~ s/\*PLUS_SIGN\*/\+/g;


        #print OUT4 "Before get_serviceid_accno_etproduct\n";
	my @svcids = &get_serviceid_accno_etproduct();
        #print OUT4 "After get_serviceid_accno_etproduct\n";

	my (%SERVICEIDS, %ALL_INTERFACE, %ALL_VRF_NAME, %ALL_IP_ADDRESS, %ALL_MAP_NAME, %INTERFACE, %VRF_NAME, %IP_ADDRESS, %MAP_NAME,);

	my $prompt = $router;
	$prompt =~ s/.net.reach.com/\#/;
	my $evpl_ends;
	my $result;
	my $mVPN_enabled;

	push @rtrmsg, $screen;
	push @schmsg, $svcinfo;
	my @list_ends = ("A-END","Z-END");
	my $IsmVPNEnabled = "no";
	if ($loginout =~ /Login/) {
		if ($method =~ /serviceid/) {
		        OS_REPORTS_DB::connect_to_database;
			if ($pcet =~ /EVPL/i) {
				if ($aendzend =~ m/A-END/) {
					$evpl_ends = 'a';
				} elsif ($aendzend =~ m/Z-END/) {
					$evpl_ends = 'z';
				}
				$result = OS_REPORTS_DB::get_link_service($serviceid.$evpl_ends);
			}
			else {
	        	 	$result = OS_REPORTS_DB::get_link_service($serviceid);
			}

		        OS_REPORTS_DB::disconnect_from_database;
               	        if ($$result{routername} !~ /.net.reach.com/) {
       	                        $$result{routername} .= '.net.reach.com';
			}
			$$result{interface} =~ s/-aal5_layer//i;
			$$result{interface} =~ s/-802.1q_vlan_subif//i;
			@def_result = &CLI_interfaceconfig($$result{routername},$$result{interface});

			$router = $$result{routername};
			$prompt = $router;
			$prompt =~ s/.net.reach.com/\#/;
			$intf_evpl = $$result{interface};

			$def_interface = undef;
			$def_vrfname = undef;
			$def_ipaddress = undef;
			$def_mapname = undef;
			my @def_ip = ();
			my @def_map = ();
			foreach my $line (@def_result) {
				chomp ($line);
       	        	        if ($line =~ /^interface (\S*)/i) {
               	        	        $def_interface = $1;
	                       	}
				if ((($pcet =~ /EVPL/i) || ($pcet =~ /VPLS/i)) && ($line =~ /interface (\S*)/i)) {
					$def_interface = $1;
				}
        	                if ($line =~ /ip vrf forwarding (\S*)/i) {
       	        	                $def_vrfname = $1;
               	        	}
	                       	if ($line =~ /ip address (\S*)(.*)/i) {
        	                       	push @def_ip, $1;
                	        }
				if ((($pcet =~ /EVPL/i) || ($pcet =~ /VPLS/i)) && ($line =~ /address (\S*)(.*)/i)) {
					push @def_ip, $1;
				}	
       		                if ($line =~ /service-policy (\w*)\s(\S*)/i) {
               		                push @def_map, $2;
                       		}
			}
			$def_ipaddress = join(" ", @def_ip);
			$def_mapname = join(" ", @def_map);
		}

		if (($method =~ /serviceid/) && (!$def_interface)) {
			my $cr = "\n";
			if ($screen)  { $cr .= "\n\n"; }
			if ($def_result[0] !~ /^problem connecting/) {
				$def_result[0] = 'CMS data incorrect or missing.';
			}
                        $def_result[0] =~ s/ at \/usr\/local\/bin\/CLI_access\.pl(.*)/\./g;
			push @rtrmsg, $cr."Networks CLI Access is not available for Service ID: ".$serviceid."\n".$def_result[0];
			$router = undef;
			$serviceid = undef;
			$status = undef;
			$def_interface = undef;
			$def_vrfname = undef;
			$def_ipaddress = undef;
			$def_mapname = undef;
		}
		else {
			my @rtrcfg;
                        #print OUT4 "Before 3959 CLI_routerconfig function call\n";
			@rtrcfg = &CLI_routerconfig($router);
                        #print OUT4 "After 3959 CLI_routerconfig function call\n";
			my $cr = "\n";
			if ($screen)  { $cr .= "\n\n"; }
		
			if ($rtrcfg[0] =~ /^unknown remote host: (\S*)/) {
				push @rtrmsg, $cr."unknown remote host:: ".$1;
				$router = undef;
				$serviceid = undef;
				$status = undef;
				$def_interface = undef;
				$def_vrfname = undef;
				$def_ipaddress = undef;
				$def_mapname = undef;
			} else {
                        	#print OUT4 "inside line 3975\n";
				open (OUT, ">/tmp/CLI-$router");
				print OUT @rtrcfg;
				close (OUT);
				push @rtrmsg, $cr.$prompt;
				$status = 'OK';
			}
                        #print OUT4 "inside line 3982\n";
		}
                        #print OUT4 "inside line 3984\n";
	}
                        #print OUT4 "inside line 3986\n";

	if ($loginout =~ /Logout/) {
		$rtrmsg[$#rtrmsg] .= "exit";
		push @rtrmsg, "\nConnection closed by foreign host.";
		$router = undef;
		$serviceid = undef;
		$status = undef;
		if (!$sch_serviceid) {
			@schmsg = undef;
		}
	}
                        #print OUT4 "inside line 3998\n";

	my ($def_interface_type, $def_interface_port);
	if ($def_interface =~ /(\D+)([^\.]+)/) {
                        #print OUT4 "inside line 4002\n";
		$def_interface_type = $1;
		$def_interface_port = $2;
	}
	my @cmds = ();
	my %cmd = ();
	my $cmd_syntax_r;
	if ($status) {
                        #print OUT4 "inside line 4010\n";
		open (IN, "$CLIcfgpath"."CLI-$pcet".".cfg");
                                if (($pcet =~ /mpls/i) && ($method =~ /router/))
                                {
#Chandini_mVPN_CLI
					print OUT4 "Before checking if VPNEnabled "; 
					print OUT4 `date`;
				        open ($FH, $routerlog_file);
                			while (chomp(my $line = <$FH>)) {
					        my @name = split(/\s/,$line);
						if ($name[0] eq $router){
        						$IsmVPNEnabled = $name[1];
						}
					}
					close($FH);
					print OUT4 "After checking if VPNEnabled ";
					print OUT4 `date`;
					
					#print OUT4 "Before calling function check_mvpn";
					#print OUT4 `date`;
                                        #$IsmVPNEnabled = check_mvpn($router);
				 	#print OUT4 "After  calling function check_mvpn";
					#print OUT4 `date`;	
                                }
                                if (($pcet =~ /mpls/i) && ($method =~ /serviceid/))
				{
		        		OS_REPORTS_DB::connect_to_database;
					$mVPN_enabled = OS_REPORTS_DB::get_mcast_enabled($serviceid);
				 	OS_REPORTS_DB::disconnect_from_database;
				}	


		my $prev_line = "";
		while (chomp(my $line = <IN>)) {
        		if ((!$line) || ($line =~ /^\#/))  { next; }
		        if ($line =~ s/^-(\s*)/Syntax: /) {
				if ($prev_line ne "")
				{
					my $temp_line = substr($line, 20);
					$prev_line = $prev_line." "."OR"." ".$temp_line;
				}
				else
				{
					$prev_line = $line;
					$prev_line =~ s/multicast\s-\s//g;
				}
				$cmd_syntax_r = "\"".$prev_line."\"";
				#$cmd_syntax_r = "\"".$line."\"";
				
	        	} else {
				$prev_line = "";
				push @cmds, $line;
	        	        @t = split /( \!| \?| \*)/, $line;
				if ($method =~ /router/) {
                                        #print OUT4 "Before Entering the variable check\n";
				

					if (!(($pcet =~ /mpls/i) && ($IsmVPNEnabled =~ /no/i) && ($t[0] =~ m/multicast/i)))
					{
			          		$cmd{$t[0]} = $cmd_syntax_r;
					}

				} else {
					
					if ($line =~ /\?/)  { next; }
					my $cmd_syntax_s = "\"Syntax: ".$line."\"";
					$line =~ s/ \*DISPLAY:(.+)\*//;
					
					if (!(($pcet =~ /mpls/i) && (($$mVPN_enabled{mcast_option} == 2) || (($$mVPN_enabled{mcast_option} == 1) && (!($t[0] =~ m/vrf/i)))) && ($t[0] =~ m/multicast/i)))
					{
                                           #print OUT4 "Inside the checks with date `date`\n";
					if ($line =~ /\Q$t[0]\E\s(.*)/) {
						my $para = $1;
						my @def_ip = split / /, $def_ipaddress;
						my @def_map = split / /, $def_mapname;
						$para =~ s/\!INTERFACE_TYPE/$def_interface_type/;
						$para =~ s/\!INTERFACE_PORT/$def_interface_port/;
						$para =~ s/\!INTERFACE/$def_interface/;
						$para =~ s/\!VRF_NAME/$def_vrfname/;
						if ($#def_ip == 0)  { $para =~ s/\!IP_ADDRESS/$def_ip[0]/; }
						if ($#def_map == 0)  { $para =~ s/\!MAP_NAME/$def_map[0]/; }
						my @pa = ();
						push @pa, $para;
					
						if (($#def_ip > 0) || ($#def_map > 0)) {
							for (my $i = 0; $i <= $#pa; $i++) {
								if ($#def_ip > 0) {
									if ($pa[$i] =~ /\!IP_ADDRESS/) {
										my $tp1 = $pa[$i];
										$pa[$i] =~ s/\!IP_ADDRESS/$def_ip[0]/;
										foreach my $j (1..$#def_ip) {
											my $tp2 = $tp1;
											$tp2 =~ s/\!IP_ADDRESS/$def_ip[$j]/;
											push @pa, $tp2;
										}
									}
								}
								if ($#def_map > 0) {
									if ($pa[$i] =~ /\!MAP_NAME/) {
										my $tp1 = $pa[$i];
										$pa[$i] =~ s/\!MAP_NAME/$def_map[0]/;
										foreach my $j (1..$#def_map) {
											my $tp2 = $tp1;
											$tp2 =~ s/\!MAP_NAME/$def_map[$j]/;
											push @pa, $tp2;
										}
									}
								}
							}
						}

						foreach my $i (0..$#pa) {					
							if ($pa[$i] =~ /\!/)  { next; }
							$cmd{$t[0]." ".$pa[$i]} = $cmd_syntax_s;
						}
					} else {
						$cmd{$t[0]} = $cmd_syntax_s;
					}
				    }
				}
		        }
		}
		close (IN);

 
        	foreach my $id (@svcids) {
                	$SERVICEIDS{$id} = ' ';
	        }
        	open (IN, "/tmp/CLI-$router");
	        OS_REPORTS_DB::connect_to_database;
        	$get_cfgvals = 0;
	        while (chomp(my $line = <IN>)) {
			my $result;
			if (($pcet =~ /VPLS/i ) || ($pcet =~ /EVPL/i)) {
			   if ($line =~ /interface (\S*)/i) {
				$result = OS_REPORTS_DB::get_link_routername_interface($router,$1);
				if ($pcet =~ /EVPL/i) { 
					chop $$result{serviceid};
				}
				if ($SERVICEIDS{$$result{serviceid}}) {
                                        $get_cfgvals = 1;
                                } else {
                                        $get_cfgvals = 2;
                                }
			    }
			}
			else {
        	        if ($line =~ /^interface (\S*)/i)  {
                	        $result = OS_REPORTS_DB::get_link_routername_interface($router, $1);
                        	if ($SERVICEIDS{$$result{serviceid}}) {
                                	$get_cfgvals = 1;
	                        } else {
					$get_cfgvals = 2;
				}
        	        }}

                	if ($line =~ /^(\!|end)$/i)  { $get_cfgvals = 0;}
                        open (OUT2, ">/data1/tmp_log/log_CLI");
                       # print OUT2 " Value of get_cfgvals is $get_cfgvals\n";
			if ($get_cfgvals) {
				if ($line =~ /^interface (\S*)/i) {
	                	        $ALL_INTERFACE{$1} = ' ';
				}
				if ((($pcet =~ /VPLS/i) || ($pcet =~ /EVPL/i)) && ($line =~ /interface (\S*)/i)) {
					$ALL_INTERFACE{$1} = ' ';
				}
				if ($line =~ /ip vrf forwarding (\S*)/i) {
					$ALL_VRF_NAME{$1} = ' ';
				}
				if ($line =~ /ip address (\S*)/i) {
					$ALL_IP_ADDRESS{$1} = ' ';
				}
                                if ((($pcet =~ /VPLS/i) || ($pcet =~ /EVPL/i)) && ($line =~ /address (\S*)/i)) {
					$ALL_IP_ADDRESS{$1} = ' ';
                                }
				if ($line =~ /service-policy in(\w*)\s(\S*)/i) {
					$ALL_MAP_NAME{$2} = ' ';
				}
				if ($line =~ /service-policy out(\w*)\s(\S*)/i) {
					$ALL_MAP_NAME{$2} = ' ';
				}
			}

			if ($get_cfgvals == 1) {
				if ((($pcet =~ /VPLS/i) || ($pcet =~ /EVPL/i)) && ($line =~ /interface (\S*)/i)) {
					$INTERFACE{$1} = ' ';
				}
				else {
					if ($line =~ /^interface (\S*)/i) {
	                	        	$INTERFACE{$1} = ' ';
					}
				}
				if ($line =~ /ip vrf forwarding (\S*)/i) {
					$VRF_NAME{$1} = ' ';
				}
                                if ($line =~ /ip address (\S*)/i) {
                                        $IP_ADDRESS{$1} = ' ';
                                }
				if ((($pcet =~ /VPLS/i) || ($pcet =~ /EVPL/i)) && ($line =~ /address (\S*)/i)) {
					$IP_ADDRESS{$1} = ' ';
				}
				if ($line =~ /service-policy in(\w*)\s(\S*)/i) {
					$MAP_NAME{$2} = ' ';
				}
				if ($line =~ /service-policy out(\w*)\s(\S*)/i) {
					$MAP_NAME{$2} = ' ';
				}
	                }
        	}
	        OS_REPORTS_DB::disconnect_from_database;
	        close (IN);
	}

	if ($execute) {
		my $disp_cfg;
		my @para_cfg;
		my $exec_status;
		my @exemsg;
		my $cerr;
		my $errpos;
		my $errmsg = 'Invalid command.';
		my $timestamp = time();
		$rtrmsg[$#rtrmsg] .= $command." ".$parameter;

		$parameter =~ s/\s+/ /g;
		$parameter =~ s/\s$//;
		my @para_cmd = split / /, $parameter;

		if ($method =~ /router/) {
			$def_interface = '';
			foreach my $line (@cmds) {
				if ($line =~ /^\Q$command\E( \!(.*)| \?(.*)| \*(.*))*$/i) {
					if ($line =~ /\*DISPLAY:(.+)\*/) {
						$disp_cfg = $1;
					} else {
						$disp_cfg = undef;
					}
					$line =~ s/ \*DISPLAY:(.+)\*//;
					if ($line =~ /\Q$command\E\s(.*)/) {
						@para_cfg = split / /, $1;
					} 

					if ($#para_cfg == $#para_cmd) {
						if ($#para_cfg == -1) {
							$exec_status = 'OK';
							last;
						} else {
							my @result = ();
							my $para_cerr;
							my $para_errpos;
							my $para_errmsg;
							foreach my $i (0..$#para_cfg) {
								if ($para_cfg[$i] =~ /^\!INTERFACE$/) {
									$para_cmd[$i] =~ /(\D+)(.+)/;
									my $if = $1;
									my $id = $2;
                                                                       open (OUT1, ">/data1/tmp_log/log_CLI");
                                                                       #print OUT1 " DIF = $if and DID= $id\ and paracfg = $para_cfg[$i]\n";
									if ('ATM' =~ /^$if/i) {
 										$para_cmd[$i] = 'ATM'.$id; 
										$def_interface = 'ATM'.$id;
									}
									if ('FastEthernet' =~ /^$if/i) { 
										$para_cmd[$i] = 'FastEthernet'.$id;
                                                                                $def_interface = 'FastEthernet'.$id;
									}
									if ('GigabitEthernet' =~ /^$if/i) { 
										$para_cmd[$i] = 'GigabitEthernet'.$id;
                                                                                $def_interface = 'GigabitEthernet'.$id;
									}
									if ('Loopback' =~ /^$if/i) { 
										$para_cmd[$i] = 'Loopback'.$id; 
                                                                                $def_interface = 'Loopback'.$id;
									}
									if ('Serial' =~ /^$if/i) { 
										$para_cmd[$i] = 'Serial'.$id; 
                                                                                $def_interface = 'Serial'.$id;
									}
									if ('POS' =~ /^$if/i) { 
										$para_cmd[$i] = 'POS'.$id;
                                                                                $def_interface = 'POS'.$id;
									}
                                                                        if ('ge-' =~ /^$if/i) {
                                                                                   $para_cmd[$i] = 'ge-'.$id;
                                                                                    $def_interface = 'ge-'.$id;
                                                                            }
									if ($ALL_INTERFACE{$para_cmd[$i]}) {
										if ($INTERFACE{$para_cmd[$i]}) {
                                                                                 #print OUT1 " Entering Here\n";
											$result[$i] = 'OK';
										} else {
											#$result[$i] = 'OK';
											#$result[$i] = 'Restricted for this interface.';
										}
									}
                                                                        else {
										$result[$i] = 'Invalid interface detected';
									}
								}
								if ($para_cfg[$i] =~ /^\!INTERFACE_TYPE$/) {
									$result[$i] = 'Invalid interface type detected';
                                                                        if ('ge-' =~ /^$para_cmd[$i]/i) {
                                                                                    $def_interface_type = 'ge-';
                                                                                    $result[$i] = 'OK';
                                                                        }
									if ('ATM' =~ /^$para_cmd[$i]/i) {
								 		$def_interface_type = 'ATM'; 
										$result[$i] = 'OK';
									}
									if ('FastEthernet' =~ /^$para_cmd[$i]/i) { 
										$def_interface_type = 'FastEthernet';
										$result[$i] = 'OK';
									}
									if ('GigabitEthernet' =~ /^$para_cmd[$i]/i) { 
										$def_interface_type = 'GigabitEthernet';
										$result[$i] = 'OK';
									}
									if ('Loopback' =~ /^$para_cmd[$i]/i) { 
										$def_interface_type = 'Loopback'; 
										$result[$i] = 'OK';
									}
									if ('Serial' =~ /^$para_cmd[$i]/i) { 
										$def_interface_type = 'Serial'; 
										$result[$i] = 'OK';
									}
									if ('POS' =~ /^$para_cmd[$i]/i) { 
										$def_interface_type = 'POS';
										$result[$i] = 'OK';
									}
									if (($para_cfg[$i+1] =~ /^\!INTERFACE_PORT$/) && ($result[$i] =~ /^OK$/)) {
										$result[$i+1] = 'Invalid interface slot/port-adapter/port detected';
										if ($para_cmd[$i+1] !~ /\./) {
											foreach my $if (keys%ALL_INTERFACE) {
												if ($if =~ /^($def_interface_type)($para_cmd[$i+1])([\.]*)/i) {
													$result[$i+1] = 'Restricted for this interface slot/port-adapter/port';
													last;
												}
											}
											foreach my $if (keys%INTERFACE) {
												if ($if =~ /^($def_interface_type)($para_cmd[$i+1])([\.]*)/i) {
													$result[$i+1] = 'OK';
													last;
												}
											}
										}
									}
								}
								if ($para_cfg[$i] =~ /^\!VRF_NAME$/) {
									if ($ALL_VRF_NAME{$para_cmd[$i]}) {
										if ($VRF_NAME{$para_cmd[$i]}) {
											$result[$i] = 'OK';
										} else {
											$result[$i] = 'Restricted for this VRF name.';
										}
									} else {
										$result[$i] = 'Invalid VRF name detected';
									}
								}
								if ($para_cfg[$i] =~ /^\!IP_ADDRESS$/) {
									if ($ALL_IP_ADDRESS{$para_cmd[$i]}) {
										if ($IP_ADDRESS{$para_cmd[$i]}) {
											$result[$i] = 'OK';
										} else {
											$result[$i] = 'Restricted for this ip address.';
										}
									} else {
										$result[$i] = 'Invalid ip address detected';
									}
								}
								if ($para_cfg[$i] =~ /^\!MAP_NAME$/) {
									if ($ALL_MAP_NAME{$para_cmd[$i]}) {
										if ($MAP_NAME{$para_cmd[$i]}) {
											$result[$i] = 'OK';
										} else {
											$result[$i] = 'Restricted for this MAP name.';
										}
									} else {
										$result[$i] = 'Invalid MAP name detected';
									}
								}
								if ($para_cfg[$i] =~ /^\?IP_ADDRESS$/) {
									if (($para_cmd[$i] =~ /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/) || ($para_cmd[$i] =~ /^([A-Fa-f0-9]{0,4})\:([A-Fa-f0-9]{0,4})\:([A-Fa-f0-9]{0,4})\:([A-Fa-f0-9]{0,4})\:([A-Fa-f0-9]{0,4})\:([A-Fa-f0-9]{0,4})\:([A-Fa-f0-9]{0,4})\:([A-Fa-f0-9]{0,4})$/)) {
										$result[$i] = 'OK';
									} else {
										$result[$i] = 'Invalid input detected';
									}
								}
								if ($para_cfg[$i] =~ /^(\w|-)+$/) {
									if ($para_cfg[$i] eq $para_cmd[$i]) {
										$result[$i] = 'OK';
									} else {
										$result[$i] = 'Invalid input detected';
									}
								}
							}

							foreach my $j (0..$#result) {
								if ($result[$j] =~ /^Restricted/) {
									$para_cerr++;
									if (!$para_errpos)  { $para_errpos = $j; }
									if (!$para_errmsg)  { $para_errmsg = $result[$j]; }
								} elsif ($result[$j] =~ /^Invalid/){
									$para_cerr = 998;
									$para_errpos = $j;
									$para_errmsg = $result[$j];
									last;
								}
							}
		
							if (!$para_cerr) {
								$exec_status = 'OK';
								last;
							} else {
								if (!$cerr) {
									$cerr = $para_cerr;
									$errpos = $para_errpos;
									$errmsg = $para_errmsg;
								} else {				
									if ($para_cerr < $cerr) {
										$cerr = $para_cerr;
										$errpos = $para_errpos;
										$errmsg = $para_errmsg;
									}
								}
							}
						}
					} elsif ($#para_cfg > $#para_cmd) {
						if (!$cerr) {
							$cerr = 999;
							$errpos = @para_cmd;
							$errmsg = 'Argument missing detected';
						}
					} else {
						if (!$cerr) {
							$cerr = 999;
							$errpos = $#para_cmd;
							$errmsg = 'Invalid input detected';
						}
					}
				}
			}
		} else {
			if ($syntax =~ /\*DISPLAY:(.+)\*/) {
				$disp_cfg = $1;
			} else {
				$disp_cfg = undef;
			}
			$exec_status = 'OK';
		}
		if ($exec_status) {
                                if ($command =~ /snmp/) 
                                {
                                   @router_result = &CLI_access($router."|".$command.'.'.$parameter);
                                }
                                else
                                {
                                    #Added by Deepti on 25-Jun-2009 for Network CLI for mVPN     
                                    if ($command =~ /multicast/)
                                    {
                                        my $temp = substr($command,12);
                                        $command = $temp;
                                    } 
				    @router_result = &CLI_access($router."|".$command. " ".$parameter);
                                }
			my @CLI_result = ();

			foreach my $line (@router_result) {
				if ($line !~ /^(\s*)Description: (.*)/) { 
					if ((($method =~ /serviceid/) && ($command =~ /^show interface (.+) rate-limit/i) && ($line =~ /^($def_interface) [^ ]+/)) || (($method =~ /router/) && ($command =~ /^show interface/i) && ($parameter =~ /rate-limit/i) && ($line =~ /^($def_interface) [^ ]+/))) {
					#if ((($method =~ /serviceid/) && ($command =~ /^show interface (.+) rate-limit/i) && ($line =~ /^($def_interface) [^ ]+/)) || (($method =~ /router/) && ($command =~ /^show interface/i) )) 
						push @CLI_result, $1."\n";
					} else {
						push @CLI_result, $line;
					}
				}
			}

			if ($disp_cfg =~ /^\!INTERFACE$/) {
				my $disp_allow = 1;
				foreach my $line (@CLI_result) {
					$line =~ /^(\s*)(\S*)/;
					if ($ALL_INTERFACE{$2}) {
						$disp_allow = 0;
						if ($INTERFACE{$2}) {
							$disp_allow = 1;
						}
					}			
					if ($disp_allow) {
						push @exemsg, $line;
					}		
				}
			} elsif ($disp_cfg =~ /^\!VRF_NAME$/) {
				my $disp_allow = 1;
				foreach my $line (@CLI_result) {
					$line =~ /^(\s*)(\S*)/;
					if ($ALL_VRF_NAME{$2}) {
						$disp_allow = 0;
						if ($VRF_NAME{$2}) {
							$disp_allow = 1;
						}
					}			
					if ($disp_allow) {
						push @exemsg, $line;
					}		
				}
			} else {
				if ($command =~ /^ping/ && $parameter =~ /(repeat|size|source|tos)/) {
					if ($CLI_result[0] =~ /^Last command and router error/) {
						my $n;
						my $extendedping_errmsg = $CLI_result[$#CLI_result];
						if ($extendedping_errmsg =~ /^% (A decimal number between 1 and 2147483647(.*))/) {
							$extendedping_errmsg = "% Invalid Repeat count. ".$1;
							$n = length($prompt) + length($command) + index($parameter,"repeat") + 8;
						} elsif ($extendedping_errmsg =~ /^% (A decimal number between 36 and 18024(.*))/) {
							$extendedping_errmsg = "% Invalid Datagram size. ".$1;
							$n = length($prompt) + length($command) + index($parameter,"size") + 6;
						} elsif ($extendedping_errmsg =~ /^% Invalid source\. Must use IP address or full interface name without spaces/) {
							$extendedping_errmsg = "% Invalid Source address.";
							$n = length($prompt) + length($command) + index($parameter,"source") + 8;
						}
						@CLI_result = ();
						push @CLI_result, "Last command error:\n";
						push @CLI_result, $prompt.$command." ".$parameter."\n";
						push @CLI_result, " " x $n."^\n";
						push @CLI_result, $extendedping_errmsg."\n";
					}
				}
				foreach my $line (@CLI_result) {
					$line =~ s/\.* at \/usr\/local\/bin\/CLI_access\.pl(.*)/\./g;
					$line =~ s/(command timed-out)\./Command timed-out on Networks CLI Access.\nPlease directly run this command on the router ($router)./i;
					push @exemsg, $line;
				}
			}
		} else {
			push @exemsg, "Last command error:\n";
			push @exemsg, $prompt.$command." ".$parameter."\n";
			if (($errmsg =~ /^Restricted/) || ($errmsg =~ /^Invalid command\.$/)) {
				push @exemsg, "\% ".$errmsg."\n";
			} else {
				my $n = length($prompt) + length($command) + 1;
				foreach my $k (0..$errpos-1) {
					$n += length($para_cmd[$k]) + 1;
				}
				push @exemsg, " " x $n."^\n";
				push @exemsg, "\% ".$errmsg." at '^' marker.\n";
			}
		}
		push @rtrmsg, @exemsg;
		push @rtrmsg, "\n\n".$prompt;

		my ($day, $month, $year) = (localtime)[3..5];
		$year = $year + 1900;
		$month = $month + 1;
		if (length($month) == 1)  { $month = "0".$month; }
		if (length($day) == 1)  { $day = "0".$day; }

		my $CLIlog_file = $CLIlog_dir."CLI_".$year.$month.$day.".log";
		open (OUT, ">$CLIlog_file");
		print OUT "----- CLI START ---------------------------------------------------------------------------------------------------------\n";
		print OUT "*TIMESTAMP*: $timestamp\n";
		print OUT "*DATETIME*: ".gmtime($timestamp)."\n";
		print OUT "*ACCOUNT NO.*: $accno\n";
		print OUT "*PRODUCT TYPE*: $pcet\n";
		print OUT "*SERVICE ID*: $serviceid\n";
		print OUT "*ROUTER*: $router\n";
		print OUT "*COMMAND*: $command $parameter\n\n";
		print OUT "========== RESULT START ==========\n";
		print OUT "$prompt$command $parameter\n";
		print OUT @exemsg;
		print OUT "========== RESULT END ============\n\n";
		print OUT "----- CLI END -----------------------------------------------------------------------------------------------------------\n";
		print OUT "\n\n";
		close (OUT);

		$command = undef;
		$parameter = undef;
		$syntax = undef;
	}

	if ($clear) {
		@rtrmsg = undef;
		if ($status)  {	push @rtrmsg, "\n".$prompt; }
	}
	
	if ((($sch_serviceid) || ($sch_interface)) && (!$svcinfo)) {
		my @sch_result = ();
		my ($sch_sid, $sch_router, $sch_int, $sch_vrf, $sch_ip, $sch_mapin, $sch_mapout);
		my @sch_ipsec = ();

		if ($search =~ /sch_serviceid/) {
		        OS_REPORTS_DB::connect_to_database;
			my $result;
			if ($pcet =~ /EVPL/i) {
	        		$result = OS_REPORTS_DB::get_link_service($sch_serviceid.'a');
			}
			else {
				$result = OS_REPORTS_DB::get_link_service($sch_serviceid);
			}
		        OS_REPORTS_DB::disconnect_from_database;
               	        if ($$result{routername} !~ /.net.reach.com/) {
       	                        $$result{routername} .= '.net.reach.com';
			}
			$$result{interface} =~ s/-aal5_layer//i;
			@sch_result = &CLI_interfaceconfig($$result{routername},$$result{interface});
			$sch_sid = $sch_serviceid;
			$sch_router = $$result{routername};
		} else {
			@sch_result = &CLI_interfaceconfig($router, $sch_interface);
		        OS_REPORTS_DB::connect_to_database;
               	        my $result = OS_REPORTS_DB::get_link_routername_interface($router, $sch_interface);
		        OS_REPORTS_DB::disconnect_from_database;
			$sch_sid = $$result{serviceid};
			$sch_router = $router;
		}

		foreach my $line (@sch_result) {
			chomp ($line);
			if (($pcet =~  /VPLS/i) || ($pcet =~ /EVPL/i)) {
				if ($line =~ /address (\S*)/i) {
					$sch_ip = $1;
					my @tmp = split(/\//,$sch_ip,2); 
					$sch_ip = $tmp[0];
					#$sch_int = 1;
				}
				if ($line =~ /interface (\S*)/i) {
					$sch_int = $1;
				}
			}
			else {
       	                	if ($line =~ /^interface (\S*)/i) {
               	                	$sch_int = $1;
                       		}
			}
                        if ($line =~ /ip vrf forwarding (\S*)/i) {
       	                        $sch_vrf = $1;
               	        }
                       	if ($line =~ /ip address (\S*)/i) {
                               	$sch_ip = $1;
                        }
			if ($line =~ /ip address (\S*)(.*)secondary$/i) {
				push @sch_ipsec, $1;
			}
       	                if ($line =~ /service-policy in(\w*)\s(\S*)/i) {
               	                $sch_mapin = $2;
                       	}
                        if ($line =~ /service-policy out(\w*)\s(\S*)/i) {
       	                        $sch_mapout = $2;
               	        }

		}

		push @schmsg, "Service ID:\n";
		if (($pcet =~ /EVPL/i) && ($sch_interface)) {
			chop $sch_sid;
		}
		push @schmsg, $sch_sid."\n";
		push @schmsg, "\n";
                if ($pcet =~ /EVPL/i) {
                         if ($sch_serviceid) {
                                push @schmsg, "	A-END\n\n";
                         }
                         elsif ($sch_interface) {
                                if ($sch_sid =~ /z/i) {
                                        push @schmsg, "	Z-END\n\n";
                                }
                                else {
                                        push @schmsg, "	A-END\n\n";
                                }
                        }
                }

		if ($sch_int) {
			push @schmsg, "Router:\n";
			push @schmsg, "$sch_router\n";
			push @schmsg, "\n";
		
			push @schmsg, "Interface:\n";
			push @schmsg, "$sch_int\n";
			if (!(($pcet =~ /VPLS/i) || ($pcet =~ /EVPL/i))) {
				if ($sch_vrf) {
					push @schmsg, "\n";
					push @schmsg, "VRF Name:\n";
					push @schmsg, "$sch_vrf\n";
				}
			}
			if ($sch_ip) {
				push @schmsg, "\n";
				push @schmsg, "IP Address:\n";
				push @schmsg, "$sch_ip\n";
				if (($pcet != /VPLS/i) || ($pcet != /EVPLS/i)) {
					
					foreach my $ipsec (@sch_ipsec) {
						push @schmsg, $ipsec." secondary\n";
					}
				}
			}
			if ($sch_mapin || $sch_mapout) {
				push @schmsg, "\n";
				push @schmsg, "Service-policy:\n";
				if ($sch_mapin) {
					push @schmsg, "$sch_mapin\n";
				}
				if ($sch_mapout) {
					push @schmsg, "$sch_mapout\n";
				}
			}
		} else {
			push @schmsg, "*** Not Available ***";
#priyanka debugging 25july2013
 open (MYFILE, '>/home/t700682/priyanka_log_25july2013.txt');
 print MYFILE "in loop1\n";
 close (MYFILE);
open(DEBUG,">/data1/tmp_log/log_27072013");
                         print DEBUG " **in tgwcisslib.pl**\n";
                         close (DEBUG);
		}
	}

	## To display only EVPL specific Z end Search Info #####
	if (($pcet =~ /EVPL/i) && ($sch_serviceid)) {
	  if ((($sch_serviceid) || ($sch_interface)) && (!$svcinfo)) {
		my @sch_result = ();
		my ($sch_sid, $sch_router, $sch_int, $sch_vrf, $sch_ip, $sch_mapin, $sch_mapout);
		my @sch_ipsec = ();

		if ($search =~ /sch_serviceid/) {
		        OS_REPORTS_DB::connect_to_database;
			#$sch_serviceid = $sch_serviceid.'z';
			my $result = OS_REPORTS_DB::get_link_service($sch_serviceid.'z');
		        OS_REPORTS_DB::disconnect_from_database;
               	        if ($$result{routername} !~ /.net.reach.com/) {
       	                        $$result{routername} .= '.net.reach.com';
			}
			$$result{interface} =~ s/-aal5_layer//i;
			@sch_result = &CLI_interfaceconfig($$result{routername},$$result{interface});
			$sch_sid = $sch_serviceid;
			$sch_router = $$result{routername};
		} else {
			@sch_result = &CLI_interfaceconfig($router, $sch_interface);
		        OS_REPORTS_DB::connect_to_database;
               	        my $result = OS_REPORTS_DB::get_link_routername_interface($router, $sch_interface);
		        OS_REPORTS_DB::disconnect_from_database;
			$sch_sid = $$result{serviceid};
			$sch_router = $router;
		}

		foreach my $line (@sch_result) {
			chomp ($line);
			if (($pcet =~  /VPLS/i) || ($pcet =~ /EVPL/i)) {
				if ($line =~ /address (\S*)/i) {
					$sch_ip = $1;
					my @tmp = split(/\//,$sch_ip,2); 
					$sch_ip = $tmp[0];
				}
                                if ($line =~ /interface (\S*)/i) {
                                        $sch_int = $1;
                                }

			}
       	               	if ($line =~ /^interface (\S*)/i) {
               	               	$sch_int = $1;
                       	}
                       	if ($line =~ /ip address (\S*)/i) {
                               	$sch_ip = $1;
                        }
			if ($line =~ /ip address (\S*)(.*)secondary$/i) {
				push @sch_ipsec, $1;
			}
       	                if ($line =~ /service-policy in(\w*)\s(\S*)/i) {
               	                $sch_mapin = $2;
                       	}
                        if ($line =~ /service-policy out(\w*)\s(\S*)/i) {
       	                        $sch_mapout = $2;
               	        }

		}

		push @schmsg, "\n	Z-END\n\n";
		#chop $sch_sid;
		#push @schmsg, "Service ID:\n";
		#push @schmsg, $sch_sid."\n";
		if ($sch_int) {
			push @schmsg, "Router:\n";
			push @schmsg, "$sch_router\n";
			push @schmsg, "\n";
		
			push @schmsg, "Interface:\n";
			push @schmsg, "$sch_int\n";
			if (!(($pcet =~ /VPLS/i) || ($pcet =~ /EVPL/i))) {
				if ($sch_vrf) {
					push @schmsg, "\n";
					push @schmsg, "VRF Name:\n";
					push @schmsg, "$sch_vrf\n";
				}
			}
			if ($sch_ip) {
				push @schmsg, "\n";
				push @schmsg, "IP Address:\n";
				push @schmsg, "$sch_ip\n";
				if (($pcet != /VPLS/i) || ($pcet != /EVPL/i)) {
					
					foreach my $ipsec (@sch_ipsec) {
						push @schmsg, $ipsec." secondary\n";
					}
				}
			}
			if ($sch_mapin || $sch_mapout) {
				push @schmsg, "\n";
				push @schmsg, "Service-policy:\n";
				if ($sch_mapin) {
					push @schmsg, "$sch_mapin\n";
				}
				if ($sch_mapout) {
					push @schmsg, "$sch_mapout\n";
				}
			}
		} else {
			push @schmsg, "*** Not Available ***";
#priyanka debugging 25july2013
 open (MYFILE, '>/home/t700682/priyanka_log_25july2013.txt');
 print MYFILE "in tgwcisslib.pl\n";
 close (MYFILE);
open(DEBUG,">/data1/tmp_log/log_27072013");
                         print DEBUG " **in tgwcisslib.pl**\n";
                         close (DEBUG);
		}
 	  }
	}
	my @CLI_command = ('');
	my @CLI_syntax = ('""');

	foreach my $k (sort keys%cmd) {
		push @CLI_command, $k;
		push @CLI_syntax, $cmd{$k};
	}

	$parameter =~ s/\+/\&#043/g;
	$syntax =~ s/\'/\&#039/g;

	foreach my $i (0..$#rtrmsg) {
		$rtrmsg[$i] =~ s/\"/\&quot/g;
		$rtrmsg[$i] =~ s/\'/\&#039/g;
		$rtrmsg[$i] =~ s/\+/\&#043/g;
		$rtrmsg[$i] =~ s/\</\&lt/g;
		$rtrmsg[$i] =~ s/\>/\&gt/g;
		if ($rtrmsg[$i] =~ s/\n$//g) {
			$rtrmsg[$i] = "\n".$rtrmsg[$i];
		}
	}

	foreach my $i (0..$#schmsg) {
		$schmsg[$i] =~ s/\"/\&quot/g;
		$schmsg[$i] =~ s/\'/\&#039/g;
		$schmsg[$i] =~ s/\+/\&#043/g;
		$schmsg[$i] =~ s/\</\&lt/g;
		$schmsg[$i] =~ s/\>/\&gt/g;
	}

#	if ($pcet !~ /EVPL/i) {
	#   if ($accno !~ /MASTER-100418/i) { 
       	   #  if (($accno !~ /MASTER-100418/i)&& ($accno != 100418)) {
       	     if (($accno !~ /MASTER-100418/i)&& ($accno !~ /100418/)) {
                push @retvals, "Restricted to run Networks Command Line Access on OLSS.<br>Please access this system again via OSC.";
                return @retvals; 
             }
#	   }
#	}
#	else {
	#	if ($accno !~ /MASTER-100418/i) {
		#	push @retvals, "Restricted to run Networks Command Line Access on OLSS.<br>Please access this system again via OSC.";
		#	return @retvals;
#		}
#	}

        if(-e $ctrlfile) {
		my @list_router = ('');
		if ($method =~ /router/) {
			if (!$status) {
				push @list_router, &get_routername_serviceid(@svcids);
			} else {
				@list_router = ($router);
			}
		}

		my @list_service = ('');
		push @list_service, @svcids;

                my $xx;
                open($xx, $ctrlfile);
                while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*ETPRODUCT\*/$encet/g;
                        $line =~ s/\*LEVEL\*/7/g;

			if ($method =~ /router/) {
				$line =~ s/\*ROUTER-CHECKED\*/checked/g;
				$line =~ s/\*SERVICEID-CHECKED\*/onClick=\'go_rs\(\)\;\'/g;
				$line =~ s/\*SELECT-RS_NAME\*/router/g;
				$line =~ s/\*LIST-RS\*/\*LIST-ROUTER\*/g;
				$line =~ s/\*HIDDEN-RS_NAME\*/serviceid/g;
				$line =~ s/\*HIDDEN-RS_VALUE\*/$serviceid/g;
				$line =~ s/\*PARAMETER-TEXT_HIDDEN\*/text/g;
				$line =~ s/\*SYNTAX-TEXT_HIDDEN\*/text/g;
			} else {
				$line =~ s/\*ROUTER-CHECKED\*/onClick=\'go_rs\(\)\;\'/g;
				$line =~ s/\*SERVICEID-CHECKED\*/checked/g;
				$line =~ s/\*SELECT-RS_NAME\*/serviceid/g;
				$line =~ s/\*LIST-RS\*/\*LIST-SERVICEID\*/g;
				#$line =~ s/\*SELECT-EVPL-RS_NAME\*/aendzend/g;
				#$line =~ s/\*LIST-EVPL-RS\*/\*LIST-ENDS\*/g;
				$line =~ s/\*HIDDEN-RS_NAME\*/router/g;
				$line =~ s/\*HIDDEN-RS_VALUE\*/$router/g;
				$line =~ s/\*PARAMETER-TEXT_HIDDEN\*/hidden/g;
				$line =~ s/\*SYNTAX-TEXT_HIDDEN\*/hidden/g;
			}

			$line =~ s/\*METHOD\*/$method/g;
			$line =~ s/\*ROUTER\*/$router/g;
			$line =~ s/\*SERVICEID\*/$serviceid/g;
			$line =~ s/\*STATUS\*/$status/g;
			$line =~ s/\*DEF_INTERFACE\*/$def_interface/g;
			$line =~ s/\*DEF_VRFNAME\*/$def_vrfname/g;
			$line =~ s/\*DEF_IPADDRESS\*/$def_ipaddress/g;
			$line =~ s/\*DEF_MAPNAME\*/$def_mapname/g;
			$line =~ s/\*COMMAND\*/$command/g;
			$line =~ s/\*PARAMETER\*/$parameter/g;
			$line =~ s/\*SYNTAX\*/$syntax/g;
                        $line =~ s/\*SCREEN\*/@rtrmsg/g;
                        $line =~ s/\*SCROLLPOS\*/$scrollpos/g;
			$line =~ s/\*SEARCH\*/$search/g;
			$line =~ s/\*SCH_SERVICEID\*/$sch_serviceid/g;
			$line =~ s/\*SCH_INTERFACE\*/$sch_interface/g;
			$line =~ s/\*SVCINFO\*/@schmsg/g;

			if ($status) {
				$line =~ s/\*LOGINOUT\*/Logout/;
				if ($method !~ /router/) {
					$line =~ s/\*METHOD_ROUTER-DISABLED\*/disabled/;
					$line =~ s/\*METHOD_SERVICEID-DISABLED\*//;
				} else {
					$line =~ s/\*METHOD_ROUTER-DISABLED\*//;
					$line =~ s/\*METHOD_SERVICEID-DISABLED\*/disabled/;
				}
				$line =~ s/\*RS-DISABLED\*/disabled/;
				if ($method =~ /router/) {
					$line =~ s/\*COMMAND-DISABLED\*/style=\'width:220px\'/;
				} else {
					$line =~ s/\*COMMAND-DISABLED\*/style=\'width:495px\'/;
				}
				$line =~ s/\*FASTTRACK-DISABLED\*//;
				$line =~ s/\*PARAMETER-DISABLED\*//;
				$line =~ s/\*EXECUTE-DISABLED\*//;
				$line =~ s/\*SEARCHRESULT-ROWS\*/14/;
			} else {
				$line =~ s/\*LOGINOUT\*/Login/;
				$line =~ s/\*METHOD_ROUTER-DISABLED\*//;
				$line =~ s/\*METHOD_SERVICEID-DISABLED\*//;
				$line =~ s/\*RS-DISABLED\*//;
				if ($method =~ /router/) {
					$line =~ s/\*COMMAND-DISABLED\*/disabled style=\'width:220px\'/;
				} else {
					$line =~ s/\*COMMAND-DISABLED\*/disabled style=\'width:495px\'/;
				}
				$line =~ s/\*FASTTRACK-DISABLED\*/alt='Search' onClick='do_fasttrack();'/;
				$line =~ s/\*PARAMETER-DISABLED\*/disabled/;
				$line =~ s/\*EXECUTE-DISABLED\*/disabled/;
				$line =~ s/\*SEARCHRESULT-ROWS\*/18/;
			}

                        if($line =~ /\*LIST-ROUTER\*/) {
				push @retvals, print_list('0', $router, @list_router);
                        } elsif($line =~ /\*LIST-SERVICEID\*/) {
				if (!$status) {
					push @retvals, print_list('0', $serviceid, @list_service);
				} else {
					push @retvals, print_list('0', $serviceid, $serviceid);
				}
                        } 
			elsif($line =~ /\*LIST-END\*/) {
				if (!$status) {
                                        push @retvals, print_list('0', $aendzend, @list_ends);
                                } else {
                                        push @retvals, print_list('0', $aendzend, $aendzend);
                                }
			}
			elsif($line =~ /\*LIST-COMMAND\*/) {
				push @retvals, print_list('0', $command, @CLI_command);
			} elsif($line =~ /\*JAVASCRIPT-GET_RSVALUE\*/) {
				if ($method =~ /router/) {
					push @retvals, "var rsvalue=new Array(\"".join("\",\"",@list_router)."\");\n";
				} else {
					if (!$status) {
						push @retvals, "var rsvalue=new Array(\"".join("\",\"",@list_service)."\");\n";
					} else {
						push @retvals, "var rsvalue=new Array(\"".$serviceid."\");\n";
					}
				}				
			} elsif($line =~ /\*JAVASCRIPT-GET_SVALUE\*/) {
				if (($method =~ /router/) || ($status)) {
					push @retvals, "var svalue=new Array(\"".join("\",\"",@list_service)."\");\n";
				} else {
					push @retvals, "var svalue=rsvalue;\n";
				}
                        } elsif($line =~ /\*JAVASCRIPT-GET_SYNTAX\*/) {
                                push @retvals, "var syntax=new Array(".join(",",@CLI_syntax).");\n";
                        } elsif($line =~ /\*JAVASCRIPT-CLEAR\*/) {
				if (!$status) {
	                                push @retvals, "\tCLI_screen.search.value=CLI_svcinfo.search.value;\n";
				} else {
					push @retvals, "\tif (CLI_svcinfo.search[0].checked) {\n";
	                                push @retvals, "\t\tCLI_screen.search.value=CLI_svcinfo.search[0].value;\n";
					push @retvals, "\t} else {\n";
	                                push @retvals, "\t\tCLI_screen.search.value=CLI_svcinfo.search[1].value;\n";
					push @retvals, "\t}\n";
	                                push @retvals, "\tCLI_screen.sch_interface.value=CLI_svcinfo.sch_interface.value;\n";
				}

####################################################################################################
# Comment when CLI does not support "Router Name Logon"

			} elsif($line =~ /\*SEARCH-TEMPLATE\*/) {
				push @retvals, "<tr>\n";
				push @retvals, "<td>&nbsp;</td>\n";
				push @retvals, "<td colspan='2' class='text'>Service ID</td>\n";
				push @retvals, "</tr>\n";
				push @retvals, "<tr>\n";
				push @retvals, "<td valign='top'><input type='radio' name='search' value='sch_serviceid'";
				if ($search !~ /sch_interface/) {
					push @retvals, " checked";
				}
				if ($status) {
					push @retvals, " onClick='CLI_svcinfo.sch_interface.options[0].selected=true;clear_svcinfo();'";
				}
				push @retvals, "></td>\n";
				push @retvals, "<td><select class='text' name='sch_serviceid' ";
				if ($status) {
					push @retvals, "onClick='CLI_svcinfo.search[0].checked=true;CLI_svcinfo.sch_interface.options[0].selected=true;clear_svcinfo();' ";
				}
				push @retvals, "onChange='return go_svcinfo_serviceid();' style='width:215px'>\n";
				push @retvals, print_list('0', $sch_serviceid, @list_service);
				push @retvals, "</select><br>\n";
				#push @retvals, "[ <input type='text' name=sch_fasttrack onKeyup='reset_sch_fasttrack();'> <img border='0' src='../images/search.gif' alt='Search' onClick='";
				push @retvals, "[ <input type='text' class='text' name=sch_fasttrack onKeyup='reset_sch_fasttrack();'> <img border='0' src='/olss/tgwcustdata/images/search.gif' alt='Search' onClick='";
				if ($status) {
					push @retvals, "CLI_svcinfo.search[0].checked=true;CLI_svcinfo.sch_interface.options[0].selected=true;clear_svcinfo();";
				}
				push @retvals, "do_sch_fasttrack();'> ]\n";
				push @retvals, "</td>\n";
				push @retvals, "</tr>\n";
				if ($status) {
					my @list_interface = ('');
					push @list_interface, sort keys%INTERFACE;
					push @retvals, "<tr><td colspan='2'>&nbsp;</td></tr>\n";
					push @retvals, "<tr>\n";
					push @retvals, "<td>&nbsp;</td>\n";
					push @retvals, "<td>Router: $router<br>Interface:</td>\n";
					push @retvals, "</tr>\n";
					push @retvals, "<tr>\n";
					push @retvals, "<td><input type='radio' name='search' value='sch_interface'";
					if ($search =~ /sch_interface/) {
						push @retvals, " checked";
					}
					if ($status) {
						push @retvals, " onClick='CLI_svcinfo.sch_serviceid.options[0].selected=true;clear_svcinfo();'";
					}
					push @retvals, "></td>\n";
					push @retvals, "<td><select name='sch_interface' onClick='CLI_svcinfo.search[1].checked=true;CLI_svcinfo.sch_serviceid.options[0].selected=true;clear_svcinfo();' onChange='return go_svcinfo_interface();'>";
					push @retvals, print_list('0', $sch_interface, @list_interface);
					push @retvals, "</select></td>\n";
					push @retvals, "</tr>\n";
				}

####################################################################################################

			} else {
	                        push @retvals, $line;
			}
                }
                close($xx);

        } else {
                push @retvals, "Cannot generate Networks Command Line Access page";
        }

        return @retvals; 
}


##### GX Router Visibility #####
# Pre: $uid, $code, $accno, $tmplroot
# Post: @vals
sub screen_gxrv {
        my ($uid, $code, $accno, $tmplroot, $ucommand_values) = @_;

        my $login_account = '0204717392';
        my $login_userid = 'mleung';
      
	#  my $login_password = 'Telstra12345';  ## login password changed on 20110512 ##
	my $login_password = 'TelstraGX123';

	my $ucommand_host = $ENV{'SERVER_NAME'};

        my $ws = &get_wholesaler($accno);
        if ($ws !~ /Telstra/i) {
                push @retvals, "** TELSTRA ONLY **<br><br>Restricted to run GX Router Visibility.<br> Please contact your Account Manager.";
                return @retvals;
        }
        my $cookie_jar = HTTP::Cookies->new(file => $lwpcookies, autosave => 1, ignore_discard => 1);
        my $ua = LWP::UserAgent->new();
        $ua->cookie_jar($cookie_jar);
        $ua->agent('Mozilla/4.0 (compatible; MSIE 6.0; Windows NT  5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)');
        my ($response, $result);
        $response = $ua->post ('https://'.$ucommand_host.'/uCommand/RouterVisibility/RouterVisibilityPage.aspx', $ucommand_values);
        $result = $response->content;
        if ($result =~ m{Login}i) {
                $response = $ua->post ('https://'.$ucommand_host.'/uCommand/login/login.aspx');
		# Changed for INC000001046256 
		if ($response->content =~ m{<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="(\S*)" />}) {
                        my $state = $1;
                        $response->content =~ m{<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="(\S*)" />};
                        my $event = $1;
                        $response = $ua->post ('https://'.$ucommand_host.'/uCommand/login/login.aspx',
                               [ '__VIEWSTATE' => $state,
				 '__EVENTVALIDATION' => $event,
                                 'LOGIN_ACCOUNT' => $login_account,
                                 'LOGIN_USERID' => $login_userid,
                                 'LOGIN_PASSWORD' => $login_password,
                                 'SUBMIT_Login_ctl0.x' => '0',
                                 'SUBMIT_Login_ctl0.y' => '0' ]
                        );
                } else {
                        push @retvals, "Cannot generate GX Router Visibility page - uCommand Error (Invalid Login)!";
                        return @retvals;
                }

                $response = $ua->post ('https://'.$ucommand_host.'/uCommand/RouterVisibility/RouterVisibilityPage.aspx', $ucommand_values);
                $result = $response->content;
        }
        $result =~ s/\cM//g;
        $result =~ s/\n/\*NEWLINE\*/g;
        $result =~ s/\/images/\/uCommand_images/gi;
        $result =~ s/RouterVisibilityPage\.aspx/tgwciss-service\.cgi\?code=$code\&level=81\&et=$encet/gi;

        my $area;
        if ($result =~ m{</head>(.*)<table id="FrameWorkTable"}) {
                $area .= "<br>";
                $area .= $1;
                $area .= "<input type='hidden' name='code' value='$code'>";
                $area .= "<input type='hidden' name='level' value='81'>";
                $area .= "<input type='hidden' name='et' value='$encet'>";
        }

        if ($result =~ m{<td>(<LINK REL=stylesheet(.*))<table id="ContentTabControl"}) {
                $area .= $1;
        }

        if ($result =~ m{<div style='width:621; ' id='ContentArea'>(.*)</div>(.*)Global Crossing. All Rights Reserved}) {
                $area .= $1;
        }

        if ($result =~ m{</table>(<LINK REL=stylesheet(.*))</body>}) {
                $area .= $1;
        }

        if (defined($area)) {
                $area =~ s/\*NEWLINE\*/\n/g;
                $area =~ s/<table /<table align="center" /gi;
                push @retvals, $area;
        } else {
                push @retvals, "Cannot generate GX Router Visibility page - uCommand Error!";
        }

        return @retvals;
}


##### GX Router Visibility #####
# get LWP Cookies
# Post: @cookies
sub get_gxrv_cookies {
	my @cookies = ();
	if ((-e $lwpcookies) && (-s $lwpcookies)) {
		my $xx;
		open ($xx, $lwpcookies);
		while (chomp(my $line = <$xx>)) {
			if ($line =~ /^\#/) { next; }
			$line =~ s/^(Set-Cookie)3/$1/i;
			$line =~ s/\"//gi;
			push @cookies, $line;
		}
		close ($xx);
	}
	return @cookies;
}


##### MPLS TE Tunnel Traffic Report #####
# Pre: code, tmproot
# Post: @vals
sub screen_mplstetunneltrafficrpt {
        my ($code, $tmplroot) = @_;
open (TE, ">/data1/tmp_log/log_TE");
        my $ctrlfile = $tmplroot."/local/tunneltrafficrpt.htm";
	my $tunnellist_file = $mpls_netperf_db."/mplstetunnel/mplstetunnel.list";
#Chandini_TE_Tunnel
	my $tunneltype_file = $mpls_netperf_db."/mplstetunnel/TYPES.csv";

        my @retvals = ();

        if(-e $ctrlfile && -e $tunnellist_file) {
		my %routers = ();
                OS_REPORTS_DB::connect_to_database;
                my $result = OS_REPORTS_DB::get_routers_location();
                OS_REPORTS_DB::disconnect_from_database;
		foreach $r (@{$result})
		{
			$$r[0] =~ s/\.net\.reach\.com//i;
			$routers{lc($$r[0])} = $$r[2];
		}
		my $xx;
		my %POPS = ();
		open($xx, $tunnellist_file);
		while(my $line = <$xx>) {
			if (!$line)  { next; }
			my @seg = split /,/, $line;
			$POPS{$routers{$seg[0]}} = $seg[1];
		}
		close($xx);
#Chandini_TE_Tunnel
		my $xx;
		my @tunnel_types;
		open($xx, $tunneltype_file);
                while(my $line = <$xx>) {
			chomp($line);              # remove the newline
                	my @name = split(/\,/,$line);
			my $lc_name = ucfirst(lc($name[2]));
                       	push @tunnel_types, "$lc_name\n";
		 }
                close($xx);

	        my $opt = "<option value";
	        my $startyear = "2008";
	        my $date = &ctime(time);
        	my @vals = split(/\s+/, $date);
	        my $curyear = $vals[4];

		my $xx;
                open($xx, $ctrlfile);
                while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/B1/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g; #encet set at the calling CGI level
			if($line =~ /\*POPS\*/) {
				foreach my $k (sort keys%POPS) {
					if (!$k) { next; }
					push @retvals, "<option value=\"$k ($POPS{$k})\">$k ($POPS{$k})<value>\n";
				}
			} elsif($line =~ /\*YEAR\*/) {
				while($curyear >= $startyear) {
					my $optline = "$opt=$curyear>$curyear</option>\n";
					push @retvals, $optline;
					$curyear--;
				}
				$curyear = $vals[4];
#Chandini_TE_Tunnel
			} elsif($line =~ /\*TTYPE\*/) {
				my $default_opt = "$opt=Aggregate>Aggregate</option>\n";
                                push @retvals, $default_opt;
                                foreach my $type (@tunnel_types) {
					if ($type =~ /Name/i){
						next;
					} elsif ($type =~ /\$\$/) { 
						last;
					} else {
						my $optline_2 = "$opt=$type>$type</option>\n";
                                        	push @retvals, $optline_2;
					}
                                }

			} else {
	                        push @retvals, $line;
			}
                }
                close($xx);
        } else {
                push @retvals, "Cannot generate te tunnel page";
        }

        return @retvals;
}


##### MPLS TE Tunnel Traffic Report #####
# Pre: $aend, $zend, $data, $start, $end, $disp, $code
# Post: @vals
#Chandini_TE_Tunnel
sub gen_mplstetunneltrafficrpt {
	my ($aend, $zend, $data, $start, $end, $disp, $code, $id, $tunnel_type) = @_;
open (TET, ">/data1/tmp_log/log_TET");
print TET "in gen_mplstetunneltrafficrpt\n";
#Chandini_TE_Tunnel
        my $tunneltype_file = $mpls_netperf_db."/mplstetunnel/TYPES.csv";
	my %t_type_hash;
	my $tunnel_value = "";
	open($xx, $tunneltype_file);
        while(my $line = <$xx>) {
		$line =~ tr/\cM//d;
        	chomp($line);              # remove the newline
		if (($line =~ /NAME/) || ($line =~ /\$\$/)){
			next;
		} else {
                	my @name = split(/\,/,$line);
			$t_type_hash{ $name[2] } = $name[0];
		}
       	}
while ( my ($key, $value) = each(%t_type_hash) ) {
        print TET "$key => $value\n";
    }

        close($xx);
	my $remote = $tra_rep_host;
	my @retvals = ();
	my ($aend_pop, $zend_pop);
	if ($aend =~ / \((\w{3})\)$/) { $aend_pop = $1 };
	if ($zend =~ / \((\w{3})\)$/) { $zend_pop = $1 };

	my %files = ();
	my $filename_header = "z-mplstetunnel-alias:";
#Chandini_TE_Tunnel
	if ($tunnel_type =~ /Aggregate/){
		$files{'IN'} = $filename_header.lc($zend_pop)."-".lc($aend_pop);
		$files{'OUT'} = $filename_header.lc($aend_pop)."-".lc($zend_pop);
	} else {
		$tunnel_value = ($t_type_hash{uc($tunnel_type)});
		$files{'IN'} = $filename_header.lc($zend_pop)."-".lc($aend_pop)."_".$tunnel_value;
                $files{'OUT'} = $filename_header.lc($aend_pop)."-".lc($zend_pop)."_".$tunnel_value;
	}

	my (@IN, @OUT);
	foreach my $k (keys%files) {
		my $socket = IO::Socket::INET->new(PeerAddr => $remote,
			PeerPort => 4546,
			Proto => "tcp",
			Type => SOCK_STREAM) or $fail = 1;

		if($fail) {
			push @retvals, "FAIL";
			push @retvals, "<p class=header>Error</p><p class=text>";
			push @retvals, "Cannot retrieve TE tunnel traffic report.";
			return @retvals;
		}

		# Only need tabular data from stats poller
		print $socket "$start $end table $data $files{$k} \n";
		@vals = ();
		while(my $l = <$socket>) {
			push @vals, $l;
#print TET "@vals\n";
		}
		close($socket);

		if($k =~ /IN/) {
			@IN = @vals;
		} else {
			@OUT = @vals;
		}
	}
        my $td = "td class=text";
        my $th = "td class=th";
        push @retvals, "<p class=header>Traffic report for TE Tunnel: $aend To $zend</p><p class=text>\n";
        push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=B&et=$encet\">";
	push @retvals, "Click here to generate another report.</a>\n<p class=text>";
        push @retvals, "Report type: $data</p>\n";
	push @retvals, "<p><hr><p class=text>\n";
        push @retvals, "<table cellpadding=4 cellspacing=5 border=1>";
        my $h;
        if($data =~ /daily/i) {
	        $h = "<tr><$th>Date (GMT)</td><$th>Incoming MBytes<br>($zend_pop to $aend_pop)</td><$th>%in</td>";
		$h .= "<$th>Outgoing MBytes<br>($aend_pop to $zend_pop)</td><$th>%out</td>";
		$h .= "<$th>5 min peak<br>Max of In or Out</td><$th>1 hr peak<br>Max of In or Out</td>";
		$h .= "<$th>24 hr peak<br>Max of In or Out</td>\n";
	} else {
		$h = "<tr><$th>Date</td><$th>Kbps In<br>($zend_pop to $aend_pop)</td><$th>Kbps Out<br>($aend_pop to $zend_pop)</td><$th>Bytes In<br>($zend_pop to $aend_pop)</td><$th>Bytes Out<br>($aend_pop to $zend_pop)</td>\n";
	}
	push @retvals, $h;	

	my (%timestamp, %traffic_IN, %traffic_OUT);
        undef(%timestamp);
        if ($IN[0] !~ /FAIL/) {
                foreach my $val (@IN) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $traffic_IN{$seg[0]} = $val;
                }
        }
        if ($OUT[0] !~ /FAIL/) {
                foreach my $val (@OUT) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $traffic_OUT{$seg[0]} = $val;
                }
        }

	if($disp =~ /graph/i) {
#Chandini_TE_Tunnel
		if (($IN[0] =~ /FAIL/) && ($OUT[0] =~ /FAIL/)){
			$mpls_tetunnel_errormsg =1;
		}
		my $max = 0;
		my $datafile = "/tmp/mplstetunnelgraph.data.$$.$id.traffic";
		open (OUT, ">$datafile");
        	foreach my $ts (sort {$a <=> $b} keys%timestamp) {
			if (!$ts)  {next; }
                        my @vin = split /\s+/, $traffic_IN{$ts};

			my @vout = split /\s+/, $traffic_OUT{$ts};
			print OUT "$ts\t";
			if($data =~ /daily/i) {
				if (!defined($vin[6]))  { $vin[6] = 0; }
				if (!defined($vout[6]))  { $vout[6] = 0; }
				$vin[6] = int((($vin[6] * 80) / 864) + 0.5);
				if ($vin[6] > $max) { $max = $vin[6]; }
				$vout[6] = int((($vout[6] * 80) / 864) + 0.5);
				if ($vout[6] > $max) { $max = $vout[6]; }
				print OUT "$vin[6]\t$vout[6]";
			} else {
				if (!defined($vin[2]))  { $vin[2] = 0; }
				if (!defined($vout[2]))  { $vout[2] = 0; }
				if ($vin[2] > $max) { $max = $vin[2]; }
				if ($vout[2] > $max) { $max = $vout[2]; }
				#Chandini_TE_TIR01
				#converted from Kbps to bps
				$vin[2] = $vin[2] * 1000;
				$vout[2] = $vout[2] * 1000;
				print OUT "$vin[2]\t$vout[2]";
			}
			print OUT "\n";
		}
		close (OUT);

		if($data =~ /daily/i) {
			&gen_atmgraph($start, $end, $disp, $data, "", $code, "", $datafile, "mplstetunnel", $max, "", "", "", "", $aend_pop, $zend_pop);
		} else {
			&gen_atmgraph($start, $end, $disp, $data, "", $code, "", $datafile, "mplstetunnel", $max, "", "", 1, "", $aend_pop, $zend_pop);
		}

		return;
	} else {
		my ($totin, $totout);
		if($data =~ /daily/i) {
	        	foreach my $ts (sort {$a <=> $b} keys%timestamp) {
	        	        if (!$ts)  { next; }
				my @vin = split /\s+/, $traffic_IN{$ts};
				my @vout = split /\s+/, $traffic_OUT{$ts};
				my $date = gmtime($ts);
				$date =~ s/00:00:00 //g;
				my $row = "<tr><$td>$date GMT</td>";
				if (!defined($traffic_IN{$ts})) {
					$totout += $vout[6];
					$row .= "<$td>-</td><$td>-</td>";
					$row .= "<$td>$vout[6]</td>";
					if ($vout[3]) {
						$row .= "<$td>$vout[7]</td>";
					} else {
						$row .= "<$td>-</td>";
					}
					$row .= "<$td>$vout[14]</td><$td>$vout[12]</td><$td>$vout[10]</td>";
				} elsif (!defined($traffic_OUT{$ts})) {
					$totin += $vin[6];
					$row .= "<$td>$vin[6]</td>";
					if ($vin[3]) {
						$row .= "<$td>$vin[7]</td>";
					} else {
						$row .= "<$td>-</td>";
					}
					$row .= "<$td>-</td><$td>-</td>";
					$row .= "<$td>$vin[14]</td><$td>$vin[12]</td><$td>$vin[10]</td>";
				} else {
					$totin += $vin[6];
					$totout += $vout[6];
					$row .= "<$td>$vin[6]</td>";
					if ($vin[3]) {
						$row .= "<$td>$vin[7]</td>";
					} else {
						$row .= "<$td>-</td>";
					}
					$row .= "<$td>$vout[6]</td>";
					if ($vout[3]) {
						$row .= "<$td>$vout[7]</td>";
					} else {
						$row .= "<$td>-</td>";
					}
					if ($vout[14] > $vin[14]) {
						$row .= "<$td>$vout[14]</td>";
					} else {
						$row .= "<$td>$vin[14]</td>";
					}
					if ($vout[12] > $vin[12]) {
						$row .= "<$td>$vout[12]</td>";
					} else {
						$row .= "<$td>$vin[12]</td>";
					}
					if ($vout[10] > $vin[10]) {
						$row .= "<$td>$vout[10]</td>";
					} else {
						$row .= "<$td>$vin[10]</td>";
					}
				}
				push @retvals, $row;
	       		}
		} else {
	        	foreach my $ts (sort {$a <=> $b} keys%timestamp) {
        	        	if (!$ts)  { next; }
				my @vin = split /\s+/, $traffic_IN{$ts};
				my @vout = split /\s+/, $traffic_OUT{$ts};
				my $date = gmtime($ts);
				my $row = "<tr><$td>$date GMT</td>";
				if (!defined($traffic_IN{$ts})) {
					$totout += $vout[4];
					$row .= "<$td>-</td><$td>$vout[2]</td>";
					$row .= "<$td>-</td><$td>$vout[4]</td>";
				} elsif (!defined($traffic_OUT{$ts})) {
					$totin += $vin[4];
					$row .= "<$td>$vin[2]</td><$td>-</td>";
					$row .= "<$td>$vin[4]</td><$td>-</td>";
				} else {
					$totin += $vin[4];
					$totout += $vout[4];
					$row .= "<$td>$vin[2]</td><$td>$vout[2]</td>";
					$row .= "<$td>$vin[4]</td><$td>$vout[4]</td>";
				}
				push @retvals, $row;
			}
		}

		my $unit;
		if($data =~ /daily/i) {
			$unit = "MBytes";
		} else {
			$unit = "Bytes";
		}
	
		push @retvals, "</table><p class=th>Total In: $totin $unit, Total Out: $totout $unit</p>\n";

		return @retvals;
	}
}


# get serviceid from accno & etproduct
# Pre: accno, etproduct
# Post: serviceid
sub get_serviceid_accno_etproduct {
open (CLI_OUT, ">/data1/tmp_log/OUT_CLI"); 
        my (@allservices, $svcids);

        my $attribute = CMS_OLSS::is_wholesaler($accno);
        my $pcet = ($etproduct) ? $etproduct : "GIA";
print CLI_OUT "accno pcet :$accno, $pcet\n";
        if($attribute =~ /^1$/i) {
                eval {
                        $svcids = CMS_OLSS::get_serviceid_for_wholesaler_accno($accno, $pcet);
                };
                foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
                }
        } else {
                eval {
                        $svcids = CMS_OLSS::get_serviceid_for_accno($accno, $pcet);
                };
                foreach my $svcref (@$svcids) {
                        push @allservices, $$svcref[0];
                }
        }
print CLI_OUT "@allservices\n";
	return sort @allservices;
}


# get routername from serviceid
# Pre: serviceid
# Post: routername
sub get_routername_serviceid {
	my @allservices = @_;
        my %routername;
	my $result;
	my $pcet = ($etproduct) ? $etproduct : "GIA";
        OS_REPORTS_DB::connect_to_database;
        foreach my $sid (@allservices) {
		if($pcet !~ /EVPL/i){
			$result = OS_REPORTS_DB::get_link_service($sid);
			#print OUT4 "list of routers: $$result{routername}\n";
	                if ($$result{routername}) {
                        	if ($$result{routername} !~ /.net.reach.com/) {
                                	$$result{routername} .= '.net.reach.com';
                        	}
                        	$routername{$$result{routername}} = ' ';
         	        }
		}
		else {
			$result = OS_REPORTS_DB::get_link_service($sid.'a');
                        if ($$result{routername}) {
                        	if ($$result{routername} !~ /.net.reach.com/) {
                                	$$result{routername} .= '.net.reach.com';
                        	}
                        	$routername{$$result{routername}} = ' ';
                        }
			$result = OS_REPORTS_DB::get_link_service($sid.'z');
                        if ($$result{routername}) {
                        	if ($$result{routername} !~ /.net.reach.com/) {
                                	$$result{routername} .= '.net.reach.com';
                        	}
                        	$routername{$$result{routername}} = ' ';
                        }
		}
        }
        OS_REPORTS_DB::disconnect_from_database;

        return sort keys%routername;
}

# get routername from serviceid
# Pre: serviceid
# Post: routername
sub get_routername_serviceid_evpl {
        my @allservices = @_;
        my %routername;
        my $result;

        my $pcet = ($etproduct) ? $etproduct : "GIA";
        OS_REPORTS_DB::connect_to_database;
        foreach my $sid (@allservices) {
          $result = OS_REPORTS_DB::get_link_service_evpl($sid.'z');
          if ($$result{routername}) {
            if ($$result{routername} !~ /.net.reach.com/) {
                $$result{routername} .= '.net.reach.com';
            }
            $routername{$$result{routername}} = ' ';
          }
        }
        OS_REPORTS_DB::disconnect_from_database;

        return sort keys%routername;
}

# get innerface config by serviceid
# Pre : serviceid
# Post : interface config
sub CLI_interfaceconfig {
        my ($routername, $interface) = @_;
	my @vals = ();
	my $pcet = ($etproduct) ? $etproduct : "GIA";

	if (($pcet =~ /VPLS/i) || ($pcet =~ /EVPL/i)) {
		@vals = &CLI_access($routername."|show interfaces ".$interface);
	}
	else {        
		@vals = &CLI_access($routername."|show run interface ".$interface);
	}
	return @vals;

}


# get router config by routername
# Pre : routername
# Post : router config
sub CLI_routerconfig {
        my $rtrname = $_[0];
	my @vals = ();
	my $pcet = ($etproduct) ? $etproduct : "GIA";

	if (($pcet =~ /EVPL/i) || ($pcet =~ /VPLS/i)) {
		@vals = &CLI_access($rtrname."|show interfaces");
	}
	else {
              #print OUT4 "Before 5576 CLI_access function call\n"; 
        	@vals = &CLI_access($rtrname."|show run");
              #print OUT4 "After 5576 CLI_access function call\n"; 
	}
        return @vals;
}

# Networks Command Line Access
# Pre : command (routername|command)
# Post : router information
sub CLI_access {
        my $cmd = $_[0];
        my @vals = ();

	my $pcet = ($etproduct) ? $etproduct : "GIA";
	if ($pcet =~ /EVPL/i) {
		$pcet = VPLS;
	}
        #print OUT4 "Before 5594 remote call\n";
	my $remote = $per_rep_host;
        my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                            PeerPort => 7308,
                            Proto => "tcp",
			    Timeout => 90,
                            Type => SOCK_STREAM) or return 0;
	
        $cmd=$cmd."|$pcet";
        print $socket "$cmd\n";
        while(my $l = <$socket>) {
                push @vals, $l;
        }
	@vals = grep { !/poller@/ } @vals;
        close($socket);
        #print OUT4 "Before 5609 returning values\n";
        return @vals;
}

sub check_mvpn{
        my $cmd = $_[0];
        my @vals = ();
        my $remote = $poller_host;
        my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                            PeerPort => 7408,
                            Proto => "tcp",
                            Timeout => 90,
                            Type => SOCK_STREAM) or return 0;

        print $socket "$cmd\n";

	my $l = <$socket>;
        close($socket);
        return $l;
}




# Get MRTG reports from REACH MRTG Server
# Pre: $opshandle
sub gen_mplsrpt {
	my ($opshandle, $pic) = @_;

	my $remote = $tra_rep_host;
	$fail = 0;
	my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                            PeerPort => 4546,
                            Proto => "tcp",
                            Type => SOCK_STREAM) or $fail = 1;

	print $socket "0 0 0 0 $opshandle $pic \n";
	print "Content-type: image/png\n\n";
	$disp = "gif";

	while(my $l = <$socket>) {
                if($disp =~ /gif|graph/) {
                        print $l;
                } else {
                        push @vals, $l;
                }
        }
        close($socket);

}

# Generate traffic reports
# Pre: $opshandle, $data, $start, $end, $disp, code, svccode, [type], $egress, parent_pid
# Chandini
#Chandini_IPT
#added one more argument :ipt_costype,which contains costype

sub gen_trafficrpt {
	my ($opshandle, $data, $start, $end, $disp, $code, $svc, $type, $egress, $id, $accno, $master_srv_lo,$log_ser_lo,$agg_serv_lo,$p_t_lo, $master_lb_lo, $log_lb_lo, $agg_lb_lo,$displaysvc,$mVPN_flag, $ipt_costype) = @_;
	$master_srv=$master_srv_lo;
	$log_ser=$log_ser_lo;
	$agg_serv=$agg_serv_lo;
	$p_t=$p_t_lo;
	$master_lb=$master_lb_lo;
	$log_lb=$log_lb_lo;
	$agg_lb=$agg_lb_lo;
	$disp_rept=$displaysvc;
	open (FHU, ">/data1/tmp_log/log_chan");
	my @retvals = ();
	if( ($end !~ /^\d{1,2}\/\d{1,2}\/\d{4}$/) || ($start !~ /^\d{1,2}\/\d{1,2}\/\d{4}$/) ) {
#Chandini_IPT
#For aggregate report the entered to and from fiels will not have 'day' option, so to avoid the display of unnecessarey error message
		if (($etproduct =~ /IPTRANSIT/i) && ($data =~ /aggre/i)){
			#hash to display alpa-numberic dates
			my %mth = ( "01" => "Jan", "02" => "Feb", "03" => "Mar", "04" => "Apr","05" => "May", "06" => "Jun", "07" => "Jul", "08" => "Aug","09" => "Sep", "10" => "Oct", "11" => "Nov", "12" => "Dec");
			#splitting the dates to get month field
			my @tmp_s = split(/\//, $start);			
			my @tmp_e = split(/\//, $end);			
			#converting numberic display to month to aplabetic display
			$start_month = $mth{$tmp_s[0]};
			$end_month = $mth{$tmp_e[0]};
			#alpha-numeric display of date
		 	$start = $start_month."-".$tmp_s[2];
		 	$end = $end_month."-".$tmp_e[2];
		} else{	
			#this is valid for all other product types
			push @retvals, "FAIL";
			push @retvals, "<p class=header>Error</p><p class=text>";
			push @retvals, "Date format must be mm/dd/yyyy, eg 2/20/2001 or 02/20/2001.\n";
			return @retvals;
		}
	}

	#my @svcprod = &retrieve($opshandle, $svcproductdb);
	# should only have 1 elem - prod code
	my $tpc = CMS_OLSS::get_productcode_for_serviceid($opshandle);
#Chandini_IPT
	#Since aggregate report is at Master service level, the above query can not fetch the productcode
	if (($etproduct =~ /IPTRANSIT/i) && ($data =~ /aggre/i)){
		$tpc = $etproduct;
	}
	# for CPLS oaggregates CR 
	my $prodcode = $tpc;

	#++EVPL
	if(!$prodcode){
		$prodcode = CMS_OLSS::get_productcode_for_serviceid($opshandle."a");
	}
	#--EVPL
	if(!$prodcode){
		#print FHU "no product code present so checking whether aggregate setvice for VPLS----->$opshandle---\n"; 
		my $temp_servid = $opshandle;
		$temp_servid =~ s/x-alias-//g;
		$temp_servid =~ s/ //g;
		#print FHU "temp_servid----->$temp_servid-----\n";
		my $temprs = CMS_OLSS::get_productcode_agg_service($temp_servid);
	        my $temp123 = $$temprs[0];
		my $tempprodcode = $$temp123[0];	
	        #print FHU "result set----->$tempprodcode--- \n";
		if($tempprodcode =~ /VPLS/){
			$prodcode=$tempprodcode;
		}
		#EPL - prashant
		else
		{
   			my $temp1 = CMS_SERVICE_DB::get_prodcode_master_service($opshandle);
        		my $temp2 = $$temp1[0];
                	if($$temp2[0]=~ /EPL/i){
                        	$prodcode=$$temp2[0];
                	}
		}	
	}



	#print FHU "prodcode ---> $prodcode\n";
	my @tmp = split(/\//, $start);
	my $dateok = 0;
	if($tmp[0] =~ /^(0|1[0-2])/) {
		$dateok = 1;
	}
	@tmp = split(/\//, $end);
	if($tmp[0] =~ /^(0|1[0-2])/) {
		$dateok = 1;
	}
	my $pt;
#Chandini_IPT
	#since the format for Aggregate report is only /month/year, this is used to bypass the error message
	if (($etproduct != /IPTRANSIT/i) && ($data != /aggre/i)){
	if(!$dateok) {
		push @retvals, "FAIL";
		push @retvals, "<p class=header>Error</p><p class=text>";
		push @retvals, "Date format must be month/day/year, eg 2/20/2001.\n";
		return @retvals;
	}
	}

	#Chandini_IPT-20100812
	#Bypassing the error message since iptransit product has monthly report and hourly reports as well
	if ($prodcode != /IPTRANSIT/i){
		if($data !~ /^(daily$|15min|5min)/i) {
			push @retvals, "FAIL";
			push @retvals, "<p class=header>Error</p><p class=text>";
			if($prodcode =~ /ATM|FR/i) {
				push @retvals, "Customers can only extract daily or 1 hour reports.";
			} else {
				push @retvals, "Customers can only extract daily or 5 minute reports.";
			}
			return @retvals;
		}
	}
	my $remote = $tra_rep_host;

	# Product Specific Reports
	# if ATM reports
	#CR-84 Juniper IPVPN Report
        CMS_DB::connect_to_database;
        my $router_os = CMS_ROUTERS_DB::get_routeros($opshandle);
        if ($prodcode =~ /MPLS/i && $router_os =~ /Juniper/i) {
                $junos = 1;
                $prodcode = 'VPLS';
        }
	SWITCH: {
		($prodcode =~ /ATM/i) && do {
			if($disp =~ /gif|graph/) {
				&gen_trafficrptatm($start, $end, $disp, $data, $opshandle, $code, $svc, $accno, $type, $id);
				return;
			} else {
				@retvals = &gen_trafficrptatm($start, $end, $disp, $data, $opshandle, $code, $svc, $accno);
				return @retvals;
			}
			last SWITCH;
		};
		($prodcode =~ /FR/i) && do {
			if($opshandle =~ /\d+\.\d+/) { # FRATM services
				my @v = split(/\-/, $opshandle);
				$v[0] = "00" . $v[0];
				$v[2] = "505" . $v[2];
				$opshandle = join '-', @v;
                        } elsif($opshandle !~ /^505/) { # ord FR service
				my @v = split(/\-/, $opshandle);
				$v[0] = "505" . $v[0];
				$v[2] = "505" . $v[2];
				$opshandle = join '-', @v;
			}
                        if($disp =~ /gif|graph/) {
                                &gen_trafficrptfr($start, $end, $disp, $data, $opshandle, $code, $svc, $accno, $type, $id);
                                return;
                        } else {
                                @retvals = &gen_trafficrptfr($start, $end, $disp, $data, $opshandle, $code, $svc, $accno);
                                return @retvals;
                        }
                        last SWITCH;
                };
		($prodcode =~ /MPLS/i) && do {
			if($disp =~ /gif|graph/) {
				if($mVPN_flag == 1){
					&gen_trafficrptmpls_mVPN($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $egress,$id);
				}else{
				&gen_trafficrptmpls($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $egress, $id);
				}
				# generate graphs
				return;
			} else {
				# generate table
				if($mVPN_flag == 1){
				@retvals = &gen_trafficrptmpls_mVPN($start, $end, $disp, $data, $opshandle, $code, $svc, $remote);
				}else{
				@retvals = &gen_trafficrptmpls($start, $end, $disp, $data, $opshandle, $code, $svc, $remote);
				}
				return @retvals;
			}
			last SWITCH;
		};
		################### VPLS TR START ####################
		($prodcode =~ /VPLS/i) && do {
			CMS_DB::connect_to_database;
			my $rspt_temp = CMS_SERVICE_DB::get_pollingtype_sid($opshandle); 
			my $pgt_temp = $$rspt_temp[0];
			$pt = $$pgt_temp[0];
                        print FHU "rspt ---->$rspt\n";
                        #print FHU "pgt---->$pgt\n";
			#print FHU "pt---->$pt\n";		
			if( $pt =~ /TRANS/){
			last SWITCH;
			}
			
			if($disp =~ /gif|graph/) {
				&gen_trafficrptvpls($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $egress, $id);
				# generate graphs
				return;
			} else {
				# generate table
# Chandini
				@retvals = &gen_trafficrptvpls($start, $end, $disp, $data, $opshandle, $code, $svc, $remote);
				return @retvals;
			}
			last SWITCH;
		};
		#################### VPLS TR END #####################
		#++EVPL
#Chandini_EVPL
		($prodcode =~ /EVPL/i) && do {
			open(Raz,">/data1/tmp_log/Chand");
			if($disp =~ /gif|graph/) {
				# generate graphs
				#print Raz "inputs to gen_trafficrptevpl for graphs:$start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $egress,$id \n\n";
				&gen_trafficrptevpl($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $egress, $id);
				return;
			} else {
				# generate table
				#print Raz "inputs to gen_trafficrptevpl for table:$start, $end, $disp, $data, $opshandle, $code, $svc, $remote \n\n";
				@retvals = &gen_trafficrptevpl($start, $end, $disp, $data, $opshandle, $code, $svc, $remote);
				return @retvals;
			}
			last SWITCH;
		};
		#--EVPL	

#Prashant EPL
		($prodcode =~ /EPL/i) && do {
			if($disp =~ /gif|graph/) 
			{
				# generate graphs
				&gen_trafficrptepl($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $accno, $id);
				return;
			} 
			else 
			{
				# generate table
				@retvals = &gen_trafficrptepl($start, $end, $disp, $data, $opshandle, $code, $svc, $remote,$accno);
				return @retvals;
			}
			last SWITCH;
		};
		#--EPL	

	}
        $fail = 0;
	open (TMP, ">/data1/tmp_log/test");
	print TMP "fail--->$fail\n";
        my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                            PeerPort => 4546,
                            Proto => "tcp",
                            Type => SOCK_STREAM) or $fail = 1;
	
	if(($prodcode =~ /ETHERNET/i) || (($prodcode =~ /VPLS/i) && ( $pt =~ /TRANS/))){
		$opshandle =~ s/x-alias-//g;
		$opshandle = "x-vpls-vlp-".$opshandle;
	}
	#print FHU "opshandle---->$opshandle\n";
        my @vals = ();
	print TMP "fail--->$fail\n";
	print TMP "DEBUG : $start $end $disp $data $opshandle\n";

	# change upper case to lower case
	$opshandle =~ tr/A-Z/a-z/;
	my $service_label = $opshandle;
        $service_label =~ s/x-alias-//g;
	$service_label =~ s/x-vpls-vlp-//g;
	#print FHU "service_label---->$service_label\n";
	close FHU;
        if(!$fail) {
		print TMP " not fail--->$start $end $disp $data $opshandle $id\n";
		#Chandini_IPT: added and if and else condition
		#Reusing the code of GIA; but for gia the graph is generated in mediator this is avoided by passing 'table' instead of $disp
	 	if ($prodcode =~ /IPTRANSIT/i) {
			print $socket "$start $end table $data $opshandle $id \n";
		}else { 		
			print $socket "$start $end $disp $data $opshandle $id \n";
			if($disp =~ /gif|graph/) {
				print "Content-type: image/gif\n\n";
			}
		}
	        while(my $l = <$socket>) {
			#Chandini_IPT-20100816: added and if and else condition
			#geting the values from socket		
			if ($prodcode =~ /IPTRANSIT/i) {
				chomp($l);
                                push @vals, $l;
			} else {
	        		if($disp =~ /gif|graph/) {
					if($prodcode !~ /EVPL/i){
					print $l;
					}
				} else {
	        	        	push @vals, $l;
	        		}
        		}
		}
	        close($socket);
	} else {
		#Chandini_IPT
		#To avoid Socket fail error message being displayed here
		if ($prodcode != /IPTRANSIT/i) {
			push @retvals, "FAIL";
			push @retvals, "<p class=header>Error</p><p class=text>";
			push @retvals, "Cannot retrieve traffic report for $disp_rept.";
			return @retvals;
		}
        }

	#Chandini_IPT-20100809
        if($prodcode =~ /IPTRANSIT/i){
                if($disp =~ /gif|graph/){
                        # generate graphs
			&gen_trafficrptipt($fail,$start, $end, $disp_rept, $disp, $data, $opshandle, $code, $svc, $remote, $accno, $id, \@vals, $master_srv);
                        return ;
                } else {
                         #generate table
                        @retvals = &gen_trafficrptipt($fail, $start, $end, $disp_rept,$disp, $data, $opshandle, $code, $svc, $remote,$accno, $id, \@vals, $master_srv, $ipt_costype);
                        return @retvals;
                }
        }

        if($disp =~ /gif|graph/) {
#Sayantan_VPLS_BugFix
#system("/usr/local/bin/rsync -azlv dv-mediator.net.reach.com::vpls_tpm_datafile /tmp");
#`/usr/local/bin/rsync -azlv 134.159.153.195::vpls_tpm_datafile /tmp`;
`/usr/local/bin/rsync -azlv \`echo $tra_rep_host\`::vpls_tpm_datafile /tmp`;
        	return;
        }
        
        my $td = "td class=text";
        my $th = "td class=th align=center";

        push @retvals, "<p class=header>Traffic Report for $disp_rept</p><p class=text>\n";
	if($prodcode eq "ETHERNET") {
		my %mth = ( "01" => "Jan", "02" => "Feb", "03" => "Mar", "04" => "Apr","05" => "May", "06" => "Jun", "07" => "Jul", "08" => "Aug","09" => "Sep", "10" => "Oct", "11" => "Nov", "12" => "Dec");
		my @tmp_s = split(/\//, $start);			
		my @tmp_e = split(/\//, $end);			
		push @retvals, "<p class=header>Report Period:<b>$tmp_s[1] $mth{$tmp_s[0]} $tmp_s[2]</b> to <b>$tmp_e[1] $mth{$tmp_e[0]} $tmp_e[2]</b></p>";
	}
# Chandini
        push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=21&et=$encet&master_srv=$master_srv&log_ser=$log_ser&agg_serv=$agg_serv&pt=$p_t&master_lb=$master_lb&log_lb=$log_lb&agg_lb=$agg_lb\">";
	push @retvals, "Click here to generate another report.</a>\n<p class=text>";
	if($data !~ /daily/i) {
		$data = "5minute";
	}
        push @retvals, "Report type: $data</p>\n";
        push @retvals, "<p><hr><p class=text>\n";
        
        if($opshandle =~ /mpls/i) {
		push @retvals, @vals;
		return @retvals;
	}
	    ################### VPLS TR START ####################
	    #if($opshandle =~ /vpls/i) {
		#	push @retvals, @vals;
			#return @retvals;
	    #}
		#pradeep commented for testing checjk it
        #################### VPLS TR END #####################
       #++EVPL 
        #push @retvals, "<table cellpadding=4 cellspacing=5 border=1>";
        my $h;
        if($prodcode !~ /EVPL/i){
#ETHERNET_Stavan_20101230 - Start
		if($prodcode eq "ETHERNET" && ($vals[0] =~ /FAIL/ || $vals[0] eq "")) {
			push @retvals, "<p class=header>No report generated.</p>\n";
			push @retvals, "<p class=text>There was no data available. Please try other dates.</p>\n";
			if($disp =~ /table/i) { 
				return @retvals;
			}
#ETHERNET_Stavan_20101230 - End
		} else {
			if($data =~ /daily/i) {
				push @retvals, "<table cellpadding=4 cellspacing=5 border=1 width=100%>";
				$h = "<tr><$th width=10%>Date (GMT)</td><$th>Service Capacity<br>(Kbps)</td><$th>Traffic In <br/>(MBytes)</td>";
				$h .= "<$th>%in</td><$th>Traffic out <br/>(MBytes)</td><$th>%out</td><$th>24hr avg <br/>(Kbps)</td><$th>4hr peak <br/>(Kbps)</td>";
				$h .= "<$th>1hr peak <br/>(Kbps)</td><$th>15min peak <br/>(Kbps)</td>\n";
			} else {
				push @retvals, "<table cellpadding=4 cellspacing=5 border=1 width=70%>";
				$h = "<tr><$th>Date</td><$th>Kbps In</td><$th>Kbps Out</td><$th>Bytes In</td><$th>Bytes Out</td>\n";
			}
		}
    	}else{	
    	
        if($data =~ /daily/i) {
	        push @retvals, "<table cellpadding=4 cellspacing=5 border=1 width=100%>";
	        $h = "<tr><$th width=10% rowspan=2>Date (GMT)</td><$th colspan=2 width=13%>Service Capacity<br>(Kbps)</td><$th colspan=2 width=13%>Traffic (MBytes) </td>";
		$h .= "<$th colspan=2 width=12%>Use of port(%)</td><$th colspan=2 width=12% >24hr avg (kbps)</td><$th colspan=2 width=12% >4hr peak (kbps)</td>";
		$h .= "<$th colspan=2 width=12% >1hr peak (kbps)</td><$th width=13% colspan=2 align=center>15min peak (kbps)</td></tr>\n";
		$h .= "<tr><$th >A-Z End</td><$th >Z-A End</td><$th >A-Z End</td><$th >Z-A End</td><$th >A-Z End</td><$th >Z-A End</td><$th >A-Z End</td><$th >Z-A End</td><$th >A-Z End</td><$th >Z-A End</td><$th >A-Z End</td><$th >Z-A End</td><$th >A-Z End</td><$th >Z-A End</td></tr>\n";
	} else {
		push @retvals, "<table cellpadding=4 cellspacing=5 border=1 width=85%>";
		$h = "<tr><$th width=20%>Date(GMT)</td><$th width=17%>Traffic (kbps) A-Z End</td><$th width=17%>Traffic (kbps) Z-A End</td><$th width=17%>Traffic (bytes) A-Z End</td><$th width=17%>Traffic (bytes) Z-A End</td>\n";
	}
	}
	#--EVPL	
	
	push @retvals, $h;
	        
	my ($totin, $totout);
	#print TMP "DEBUG : @vals\n";
	close TMP;
	foreach my $k (@vals) {
		my @xvals = split(/\s+/, $k);
		next if ($xvals[0] !~ /\d+/);
		my $d = gmtime($xvals[0]);
		if($data =~ /daily/i) {
			my @tmpd = split(/\s+/, $d);
			$d = "$tmpd[1] $tmpd[2] $tmpd[4]";
		}
		
		my $row;
		if($data =~ /daily/i) {
			$totin += $xvals[4];
			$totout += $xvals[6];
			$row = "<tr><$td>$d</td><$td>$xvals[3]</td><$td>$xvals[4]</td><$td>$xvals[5]</td>";
			$row .= "<$td>$xvals[6]</td><$td>$xvals[7]</td>";
			$row .= "<$td>$xvals[8]</td><$td>$xvals[10]</td><$td>$xvals[12]</td>";
			$row .= "<$td>$xvals[14]</td></tr>\n";
		} else {
			$totin += $xvals[3];
			$totout += $xvals[4];
			$row = "<tr><$td>$d GMT</td><$td>$xvals[1]</td><$td>$xvals[2]</td>";
			$row .= "<$td>$xvals[3]</td><$td>$xvals[4]</td></tr>\n";
		}
		push @retvals, $row;
	}

	my $unit;
	if($data =~ /daily/i) {
		$unit = "MBytes";
	} else {
		$unit = "Bytes";
	}

#	push @retvals, "</table><p class=text>Total In: $totin KB, Total Out: $totout KB</p>\n";	
	if($prodcode !~ /EVPL/i){
	push @retvals, "</table><p class=text>Total In: $totin $unit, Total Out: $totout $unit</p>\n";	
	}else{
	push @retvals, "</table><p class=text>Total A-Z End: $totin $unit <br><br> Total Z-A End: $totout $unit</p>\n";
	}
	return @retvals;
}

# Generate FR Reports
# Pre: $start, $end, $disp, $data, $opshandle, $code, $service, $accno, type, parent_pid
sub gen_trafficrptfr {
	my ($start, $end, $disp, $data, $opshandle, $code, $svc, $accno, $type, $id) = @_;

	my @tv = split(/\-/, $opshandle);
	if ($opshandle =~ /\d+\.\d+/) { # FRATM services
		$tv[0] =~ s/^00//;
		$tv[2] =~ s/^505//;
	} elsif ($opshandle =~ /^505/) { # ord FR service
		$tv[0] =~ s/^505//;
		$tv[2] =~ s/^505//;
	}
	my $topshandle = join('-', @tv);

        my $svclabel = &get_svclabel($accno, $topshandle, 0);
        my $serviceid_alt = &get_serviceid_alt($accno, $topshandle, 0);
        #my $displaysvc = ($svclabel) ? "$svclabel ($topshandle)" : $topshandle;
        my $displaysvc = '';
        if (($svclabel) && ($serviceid_alt)) {
                $svclabel =~ s/\^//g;
                $displaysvc = "$svclabel ($serviceid_alt) ($topshandle)";
        } elsif ($svclabel) {
                $svclabel =~ s/\^//g;
                $displaysvc = "$svclabel ($topshandle)";
        } elsif ($serviceid_alt) {
                $displaysvc = "$serviceid_alt ($topshandle)";
        } else {
                $displaysvc = "$topshandle";
        }
        $displaysvc =~ s/x-alias-//g;

	# NB: if type = bday - generate busiest day in the start & end period
        # NB2: if type = dutil - generate daily utilisation
        my ($nodata, @retvals, @vals);
        chdir($dbroot);
        
        my(%mths) = ("jan", "01", "feb", "02", "mar", "03", "apr", "04", "may", "05", "jun", "06", "jul", "07",
                        "aug", "08", "sep", "09", "oct", "10", "nov", "11", "dec", "12");
	my(%mths2) = ("01", "jan", "02", "feb", "03", "mar", "04", "apr", "05", "may", "06", "jun", "07", "jul",
			"08", "aug", "09", "sep", "10", "oct", "11", "nov", "12", "dec");

        #start and end in US date format
        my ($ustart, $usdate) = &get_uxtime($start);
        my ($uend, $uedate) = &get_uxtime($end);

        if($ustart == $uend) {
                $uend += 60 * 60 * 24;
                my $x = localtime($uend);
                my ($d, $m, $y) = (split(/\s+/, $x))[2,1,4];
                $m =~ tr/A-Z/a-z/;
                $d = (length($d) != 2) ? "0$d" : $d;
                my $t = "$mths{$m}/$d/$y";
                ($uend, $uedate) = &get_uxtime($t);
        }
               
	my $graphtype = "LINE2";
 
        my $datafound = 0;
        my $daily = 0;
        chdir("FR/");

	my $header = "";
        #my $dh = "Timestamp,svckey,logdate,cir-eg,B to A Total Bytes,dailyutil-eg,";
        #$dh .= "cir-ig,A to B Total Bytes,dailyutil-ig,a-cirddr,b-cirddr,a-eirddr,b-eirddr,A end CIR\n";
	my $dh = "Timestamp,svckey,logdate,B to A CIR Bytes,B to A Total Bytes,B to A Daily Utilisation,";
        $dh .= "A to B CIR Bytes,A to B Total Bytes,A to B Daily Utilisation,";
        $dh .= "A to B CIR DDR,B to A CIR DDR,A to B EIR DDR,B to A EIR DDR,A end CIR\n";

           
	#my $hh = "Timestamp,svckey,logdate,egcir,egtotal,igcir,igtotal,aegeirb,";
        #$hh .= "aigb,aigeirb,adiscard,begb,begeirb,bigb,bigeirb,bdiscard,";
        #$hh .= "aendcir,Service CIR,eg_util,ig_util,a_cir_ddr,b_cir_ddr,a_eir_ddr,b_eir_ddr\n";   

	my $hh = "Timestamp,svckey,logdate,B to A CIR Bytes,B to A Total Bytes,A to B CIR Bytes,";
        $hh .= "A to B Total Bytes,A End EIR Egress Bytes,";
        $hh .= "A End Ingress Bytes,A End EIR Ingres Bytes,A End Discarded Bytes,";
        $hh .= "B End Egress Bytes,B End Egress EIR Bytes,B End Ingress Bytes,";
        $hh .= "B End Ingress EIR Bytes,B End Discarded Bytes,";
        $hh .= "A End CIR,Service CIR,B to A Utilisation,A to B Utilisation,";
	$hh .= "A to B CIR DDR,B to A CIR DDR,A to B EIR DDR,B to A EIR DDR\n";


	my $interval = 86400;

        if($data =~ /daily/i) {
		$header = $dh;

                chdir("daily");
                $daily = 1;
        } else {  
		$interval = 3600;
		$header = $hh;
                chdir("hourly");
        }
        my $busiestday;
        my $maxtraff = 0;
#open(my $xo, ">/tmp/frdebug");

        if(-e "$opshandle") {
                my ($fh, $out);
                open($fh, "$opshandle");
                open($out, ">/tmp/data");
                                
                while(my $line = <$fh>) {
                        chomp($line);
                        my @tmp = split(/\s+/, $line);

                        if( ($tmp[0] >= $ustart) && ($tmp[0] <= $uend) ) {
				#my @t = split(/\s+/, $line);

				#my @sk = split(/\-/, $t[1]);
			 	#$sk[0] =~ s/^505//;
				#$sk[2] =~ s/^505//;
				#$t[1] = join '-', @sk;	
				#my $l = join '	', @t;
				#print $out "$l\n";

                                print $out "$line\n";
                                $datafound++;
                                push @vals, $line;
                                my @t = split(/\s+/, $line);
				my $tot = $t[4] + $t[7];
                                #if($t[5] >= $maxtraff) {
                                if($tot >= $maxtraff) {
                                        $busiestday = $t[2]; # dd/mm/yyyy format
                                        $maxtraff = $tot;
                                }
                        }

                }
                close($fh); close($out);
        } else {
                $nodata = "<p class=text>Cannot extract traffic report for $opshandle.</p>\n";
        }
                        
        if($disp =~ /table/i) {
                #push @retvals, "<p class=header>Traffic Report for $opshandle</p><p class=text>\n";
                push @retvals, "<p class=header>Traffic Report for $displaysvc</p><p class=text>\n";
                push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=21&et=$encet\">";
                push @retvals, "Click here to generate another report.</a>\n<p class=text>";

                if($nodata) {
                        push @retvals, $nodata;
                        return @retvals;
                } elsif(!$datafound) {
                        push @retvals, "<p class=text>No traffic data found for this period.</p>\n";
                        return @retvals;
                }

                my $th = "<td class=th>";
                my $tp = "<td class=text>";
                my $thx = "<td class=thx>";

		if (1==2) {  # for debug use only
	                push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>Date</td><td class=th>Category</td>";
	                # daily data first
	                if($daily) {
	                        push @retvals, "$th Egress CIR </td>$th Bytes </td>$th Daily Utilisation</td></tr>\n";
	                } else {
	                        push @retvals, "$th Egress CIR</td>$th Bytes </td></tr>\n";
	
	                }
	
	                foreach my $l (@vals) {
	                        my @tv = split(/\s+/, $l);
	                        my $totalbytes = 0;
	
	                        #push @retvals, "<tr>$thx $tv[2]</td>$tp Egress Guaranteed</td>$tp $tv[3]</td>$tp $totalbytes</td></tr>\n";
	                        #$totalbytes = $tv[4] * 48;
	                        #push @retvals, "<tr><td></td>$tp Egress Non-guaranteed</td>$tp $tv[4]</td>$tp $totalbytes</td></tr>\n";
	
	                        #$totalbytes = $tv[5] * 48;
				$totalbytes = $tv[4];
	
	                        if($daily) {
	                                push @retvals, "<tr>$thx $tv[2]</td>$thx Total Egress</td>$thx $tv[3]</td>$thx $totalbytes</td>$thx $tv[5]</td></tr>\n";
	                        } else {
	                                push @retvals, "<tr>$thx $tv[2]</td>$thx Total Egress</td>$thx $tv[5]</td>$thx $totalbytes</td></tr>\n";
	                        }
	                }
	                push @retvals, "</table>\n";
		}

		# new summary table
		#
		my ($fromd, $tod);
		my $cnt=0;
		my ($tot_atob_cir_by, $tot_atob_eir_by, $tot_atob_tot_by);
		my ($tot_btoa_cir_by, $tot_btoa_eir_by, $tot_btoa_tot_by);
		my ($tot_begeirb, $tot_aigb, $tot_aigeirb, $tot_adiscard);
		my ($tot_egeir, $tot_bigb, $tot_bigeirb, $tot_bdiscard);

		foreach my $l (@vals) {
			my @tv = split(/\s+/, $l);

			# start and end date of period
			if ($cnt == 0) { $fromd = $tv[2]; };
			$tod = $tv[2];

			if ($daily) {
				$tot_atob_cir_by += $tv[6];
				$tot_atob_tot_by += $tv[7];
				$tot_btoa_cir_by += $tv[3];
				$tot_btoa_tot_by += $tv[4];
				$tot_igeir       += $tv[14];
				$tot_aigb        += $tv[15];
				$tot_aigeirb     += $tv[16];
				$tot_adiscard    += $tv[17];
				$tot_egeir       += $tv[18];
				$tot_bigb        += $tv[19];
				$tot_bigeirb     += $tv[20];
				$tot_bdiscard    += $tv[21];
			} else {
				$tot_atob_cir_by += $tv[5];
				$tot_atob_tot_by += $tv[6];
				$tot_btoa_cir_by += $tv[3];
				$tot_btoa_tot_by += $tv[4];
				$tot_igeir       += $tv[12];
				$tot_aigb        += $tv[8];
				$tot_aigeirb     += $tv[9];
				$tot_adiscard    += $tv[10];
				$tot_egeir       += $tv[7];
				$tot_bigb        += $tv[13];
				$tot_bigeirb     += $tv[14];
				$tot_bdiscard    += $tv[15];
			}
			$cnt++;
		}
		$tot_atob_eir_by = $tot_atob_tot_by - $tot_atob_cir_by;
		$tot_btoa_eir_by = $tot_btoa_tot_by - $tot_btoa_cir_by;

		# DDR
		my $ddr_100 = ($tot_aigb==$tot_atob_tot_by) ? 1 : 0;
		my $ddr_0   = (($tot_aigb>0)&&($tot_atob_tot_by==0)) ? 1 : 0;
		my ($atob_cir_ddr, $btoa_cir_ddr, $atob_eir_ddr, $btoa_eir_ddr);
		my $t;

		if ($ddr_100) {
			$atob_cir_ddr = 100;
			$btoa_cir_ddr = 100;
			$atob_eir_ddr = 100;
			$btoa_eir_ddr = 100;
		} elsif ($ddr_0) {
			$atob_cir_ddr = 0;
			$btoa_cir_ddr = 0;
			$atob_eir_ddr = 0;
			$btoa_eir_ddr = 0;
		} else {
			$t = ($tot_aigb - $tot_aigeirb - $tot_adiscard);
			$atob_cir_ddr = ($t > 0 ) ? (($tot_atob_tot_by - $tot_igeir)/$t) * 100 : 0;

			$t = ($tot_bigb - $tot_bigeirb - $tot_bdiscard);
			$btoa_cir_ddr = ($t > 0 ) ? (($tot_btoa_tot_by - $tot_egeir)/$t) * 100 : 0;

			$t = ($tot_aigeirb);
			$atob_eir_ddr = ($t > 0 ) ? (($tot_igeir)/$t) * 100 : 0;
			$atob_eir_ddr = ($tot_igeir == $t) ? 100 : $atob_eir_ddr;

			$t = ($tot_bigeirb);
			$btoa_eir_ddr = ($t > 0 ) ? (($tot_egeir)/$t) * 100 : 0;
			$btoa_eir_ddr = ($tot_egeir == $t) ? 100 : $btoa_eir_ddr;
		}
		$atob_cir_ddr = sprintf("%.3f",$atob_cir_ddr);
		$btoa_cir_ddr = sprintf("%.3f",$btoa_cir_ddr);
		$atob_eir_ddr = sprintf("%.3f",$atob_eir_ddr);
		$btoa_eir_ddr = sprintf("%.3f",$btoa_eir_ddr);

		# change format from dd/mm/yyyy to mmm/dd/yyyy
		if ($daily) {
			my @ttt = split(/\//,$fromd);
			#$fromd = "$ttt[1]/$ttt[0]/$ttt[2]";
			$fromd = $mths2{$ttt[1]}."/$ttt[0]/$ttt[2]";
			my @ttt = split(/\//,$tod);
			#$tod = "$ttt[1]/$ttt[0]/$ttt[2]";
			$tod = $mths2{$ttt[1]}."/$ttt[0]/$ttt[2]";
		} else {
			my @ttx = split(/\:/,$fromd);
			my @tty = split(/\//,$ttx[3]);
			$fromd = join(":",@ttx[0,1,2]).":".$mths2{$tty[0]}."/$tty[1]/$tty[2]";
			my @ttx = split(/\:/,$tod);
			my @tty = split(/\//,$ttx[3]);
			$tod = join(":",@ttx[0,1,2]).":".$mths2{$tty[0]}."/$tty[1]/$tty[2]";
		}

		push @retvals, "<p class=header>Summary<p>";
		push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>Period (mmm/dd/yyyy)</td><td class=text>$fromd GMT - $tod GMT</td></tr>\n";
		push @retvals, "<tr></tr>\n";
		push @retvals, "<tr><td class=th colspan=2>Traffic</td></tr>\n";
		push @retvals, "<tr><td class=text>A to B CIR Bytes</td><td class=text>$tot_atob_cir_by</td></tr>\n";
		push @retvals, "<tr><td class=text>A to B EIR Bytes</td><td class=text>$tot_atob_eir_by</td></tr>\n";
		push @retvals, "<tr><td class=text>A to B Total Bytes</td><td class=text>$tot_atob_tot_by</td></tr>\n";
		push @retvals, "<tr><td class=text>B to A CIR Bytes</td><td class=text>$tot_btoa_cir_by</td></tr>\n";
		push @retvals, "<tr><td class=text>B to A EIR Bytes</td><td class=text>$tot_btoa_eir_by</td></tr>\n";
		push @retvals, "<tr><td class=text>B to A Total Bytes</td><td class=text>$tot_btoa_tot_by</td></tr>\n";
		# remove DDR from traffic section, moved to SLA section, requested by AM, 20030808
		#push @retvals, "<tr></tr>\n";
		#push @retvals, "<tr><td class=th colspan=2>DDR</td></tr>\n";
		#push @retvals, "<tr><td class=text>A to B CIR DDR</td><td class=text>$atob_cir_ddr%</td></tr>\n";
		#push @retvals, "<tr><td class=text>A to B EIR DDR</td><td class=text>$atob_eir_ddr%</td></tr>\n";
		#push @retvals, "<tr><td class=text>B to A CIR DDR</td><td class=text>$btoa_cir_ddr%</td></tr>\n";
		#push @retvals, "<tr><td class=text>B to A EIR DDR</td><td class=text>$btoa_eir_ddr%</td></tr>\n";

		push @retvals, "</table>\n";

		if (1==2) {
			push @retvals, "<p>Debug<br><br>";
			push @retvals, "ddr_100 ($ddr_100)<br>\n";
			push @retvals, "ddr_0 ($ddr_0)<br><br>\n";
			push @retvals, "tot_atob_tot_by ($tot_atob_tot_by)<br>\n";
			push @retvals, "tot_igeir       ($tot_igeir)<br>\n";
			push @retvals, "tot_aigb        ($tot_aigb)<br>\n";
			push @retvals, "tot_aigeirb     ($tot_aigeirb)<br>\n";
			push @retvals, "tot_adiscard    ($tot_adiscard)<br>\n";
			push @retvals, "<br>\n";
			push @retvals, "tot_btoa_tot_by ($tot_btoa_tot_by)<br>\n";
			push @retvals, "tot_egeir       ($tot_egeir)<br>\n";
			push @retvals, "tot_bigb        ($tot_bigb)<br>\n";
			push @retvals, "tot_bigeirb     ($tot_bigeirb)<br>\n";
			push @retvals, "tot_bdiscard    ($tot_bdiscard)<br>\n";
		}
		#
		#

                return @retvals;

        } else {
                my $datafile = "/tmp/atmgraph.data.$$.$id";
                my $out;
                my $label;
		#print $xo " PRE ($type) ($ustart) ($uend) ($maxtraff) B Day = ($busiestday) \n";

                if($type =~ /bday/i) { # busiest day
			$datafile .= ".$type";
                        my @dvals = split(/\//, $busiestday);
                        $maxtraff = 0;
                        my $bd = "$dvals[1]/$dvals[0]/$dvals[2]"; # want mm/dd/yyyy
			my $displaybd = $mths2{$dvals[1]}."/".$dvals[0]."/".$dvals[2];

			$header = $hh;

                        my @blist = ();
                        if($daily) {
                                my $fail;
                                chdir($dbroot);
                                chdir("FR/hourly");
                                if(-e "$opshandle") {
                                        my ($fh, $out);
                                        open($fh, "$opshandle");
                                        while(my $line = <$fh>) {
                                                next if($line !~ /\s+\d{2}\:\d{2}\:\d{2}\:$bd\s+/);
                                                my @t = split(/\s+/, $line);
                                                if($t[4] > $maxtraff) {
                                                        $maxtraff = $t[4];
                                                }
                                                if($t[6] > $maxtraff) {
                                                        $maxtraff = $t[6];
                                                }
                                                chomp($line);
                                                push @blist, $line;
                                        }
                                        close($fh);
                                } else {
                                        $fail = "No $opshandle";
                                }

                                open($out, ">$datafile");
				print $out "$header";
                                foreach my $l (@blist) {
                                        print $out "$l\n";
                                }
                                close($out);
                                #$label = "Busiest day $busiestday";
				$label = "Busiest day $displaybd";
                        }

                } elsif ($type =~ /dutil/i) { # daily utilisation
			$datafile .= ".$type";
                        $maxtraff = 0;
                        open($out, ">$datafile");

			print $out "$header";

                        foreach my $l (@vals) {
                                my @t = split(/\s+/, $l);
				$t[5] = $t[5] * 100;
				$t[8] = $t[8] * 100;
                                if($t[5] > $maxtraff) {
                                        $maxtraff = $t[5];
                                }
				if($t[8] > $maxtraff) {
					$maxtraff = $t[8];
				}
				$l = join '	', @t;
                                print $out "$l\n";
                        }
                        close($out);
                        $label = "Daily Utilisation";
		} elsif ($type =~ /hutil/i) { # hourly utilisation
                        $datafile .= ".$type";
                        $maxtraff = 0;
                        open($out, ">$datafile");

                        print $out "$header";

                        foreach my $l (@vals) {
				my @t = split(/\s+/, $l);
                                $t[18] = $t[18] * 100;
                                $t[19] = $t[19] * 100;

                                #if($t[5] > $maxtraff) {
                                #        $maxtraff = $t[5];
                                #}
                                #if($t[8] > $maxtraff) {
                                #        $maxtraff = $t[8];
                                #}
                                $l = join '	', @t;
                                print $out "$l\n";
                        }
                        close($out);
                        $label = "Hourly Utilisation";
		} elsif ($type =~ /ddr/i) { # DDR end-date - 30 days
#my $xx;
#open($xx, ">/tmp/ddr");
#print $xx "Start\n";
			$datafile .= ".$type";
			my $factor = 30 * 24 * 60 * 60;
			#my $lstart = $uend - $factor;
			my $lstart = $ustart;
			my @blist;

			$maxtraff = 0;
			chdir($dbroot);
			chdir("FR/daily");
			if(-e "$opshandle") {
#print $xx "$opshandle -- \n";
				my ($fh, $out);
				open($fh, "$opshandle");   
				while(my $line = <$fh>) {
					chomp($line);
                                       	my @t = split(/\t/, $line);
					if( ($t[0] >= $lstart) && ($t[0] <= $uend) ) {
						my $nl = $t[0];
						my $acirddr = $t[9];
						my $bcirddr = $t[10];
						my $aeirddr = $t[11];
						my $beirddr = $t[12];

						$maxtraff = ($acirddr > $maxtraff) ? $acirddr : $maxtraff;
						$maxtraff = ($bcirddr > $maxtraff) ? $bcirddr : $maxtraff;
						$maxtraff = ($aeirddr > $maxtraff) ? $aeirddr : $maxtraff;
						$maxtraff = ($beirddr > $maxtraff) ? $beirddr : $maxtraff;
						$nl .= "\t$acirddr\t$bcirddr\t$aeirddr\t$beirddr\n";
                                       		push @blist, $nl;
					}
                                }
                                close($fh);
                       	}  else {
                                $fail = "No $opshandle";
                       	}
			#print $xo "DDR Maxtraff ($maxtraff) ($fail) ($lstart) ($uend) ($xlabel) (@blist)\n";

			#$header = "Timestamp,a-cirddr,b-cirddr,a-eirddr,b-eirddr\n";
			$header = "Timestamp,A to B CIR DDR,B to A CIR DDR,A to B EIR DDR,B to A EIR DDR\n";

                       	open($out, ">$datafile");
			print $out "$header";
                       	foreach my $l (@blist) {
                                print $out "$l";
#print $xx "$l";
                       	}
#close($xx);
                       	close($out);
                       	$label = "Data Delivery Ratio";
	
                } else {
			$datafile .= ".traffic";
			$maxtraff = 0;
                        # graph for daily traffic
                        #print "Content-tye: image/gif\n\n";
                        open($out, ">$datafile");
			print $out "$header";
                        foreach my $l (@vals) {
				my @t = split(/\s+/, $l);
				if($t[4] > $maxtraff) {
					$maxtraff = $t[4];
				}
				if($t[7] > $maxtraff) {
                                        $maxtraff = $t[7];
                                }

				#$t[4] = $t[4] / 1000000;
				#$t[7] = $t[7] / 1000000;
				#$l = join '	', @t;

                                print $out "$l\n";
                        }
                        close($out);
                }

                &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "fr", $maxtraff, $label, $graphtype, $interval);
        }
}

# Generate ATM reports
#Pre: $start, $end, $disp, $data, $opshandle, $code, $service, $accno, type, parent_pid
sub gen_trafficrptatm {
	my ($start, $end, $disp, $data, $opshandle, $code, $svc, $accno, $type, $id) = @_;

        my $svclabel = &get_svclabel($accno, $opshandle, 0);
        my $serviceid_alt = &get_serviceid_alt($accno, $opshandle, 0);
        #my $displaysvc = ($svclabel) ? "$svclabel ($opshandle)" : $opshandle;
        my $displaysvc = '';
        if (($svclabel) && ($serviceid_alt)) {
                $svclabel =~ s/\^//g;
                $displaysvc = "$svclabel ($serviceid_alt) ($opshandle)";
        } elsif ($svclabel) {
                $svclabel =~ s/\^//g;
                $displaysvc = "$svclabel ($opshandle)";
        } elsif ($serviceid_alt) {
                $displaysvc = "$serviceid_alt ($opshandle)";
        } else {
                $displaysvc = "$opshandle";
        }
        $displaysvc =~ s/x-alias-//g;

	#return ($start, $end, $disp, $data, $opshandle);
	#print "<p> ($start, $end, $disp, $data, $opshandle)\n";

	# NB: if type = bday - generate busiest day in the start & end period
	# NB2: if type = dutil - generate daily utilisation

	my ($nodata, @retvals, @vals);
	chdir($dbroot);

	my(%mths) = ("jan", "01", "feb", "02", "mar", "03", "apr", "04", "may", "05", "jun", "06", "jul", "07",
			"aug", "08", "sep", "09", "oct", "10", "nov", "11", "dec", "12");
	my(%mths2) = ("01", "jan", "02", "feb", "03", "mar", "04", "apr", "05", "may", "06", "jun", "07", "jul",
			"08", "aug", "09", "sep", "10", "oct", "11", "nov", "12", "dec");

	#start and end in US date format
	my ($ustart, $usdate) = &get_uxtime($start);
	my ($uend, $uedate) = &get_uxtime($end);

        if($ustart == $uend) {
                $uend += 60 * 60 * 24;
                my $x = localtime($uend);
                my ($d, $m, $y) = (split(/\s+/, $x))[2,1,4];
                $m =~ tr/A-Z/a-z/;
                $d = (length($d) != 2) ? "0$d" : $d;
                my $t = "$mths{$m}/$d/$y";
                ($uend, $uedate) = &get_uxtime($t);
        }

	my ($hh, $dh, $header);
	#print "<p> ($ustart, $usdate) ($uend, $uedate)\n";

	my $datafound = 0;
	my $graphtype = "LINE2";

	my $interval = 86400;

	my $daily = 0;
	chdir("ATM/");

	$dh  = "Timestamp,key,logdate,A_dailyegress_guaranteed,A_dailyegress_non-guaranteed,A_dailyegress_total,";
	$dh .= "B_to_A_utilisation,A_dailyingress_guaranteed,A_dailyingress_non-guaranteed,A_dailyingress_total,";
	$dh .= "dailyingressutilisation,B_dailyegress_guaranteed,B_dailyegress_non-guaranteed,B_dailyegress_total,";
	$dh .= "A_to_B_utilisation,B_dailyingress_guaranteed,B_dailyingress_non-guaranteed,B_dailyingress_total,B_dailyingressutilisation";

	$hh  = "Timestamp,svckey,logdate,A_ingress_guaranteed,A_ingress_non-guaranteed,A_ingress_total,A SCR for all cells,";
	$hh .= "A_egress_guaranteed,A_egress_non-guaranteed,A_egress_total,B_ingress_guaranteed,B_ingress_non-guaranteed,B_ingress_total,b_scr,B_egress_guaranteed,";
	$hh .= "B_egress_non-guaranteed,B_egress_total,a_igutilisation,B_to_A_utilisation,b_igutilisation,A_to_B_utilisation";

	if($data =~ /daily/i) {
		$header = $dh;
		#$header = "Timestamp,key,logdate,A_dailyegress_guaranteed,A_dailyegress_non-guaranteed,A_dailyegress_total,";
                #$header .= "B_to_A_utilisation,A_dailyingress_guaranteed,A_dailyingress_non-guaranteed,A_dailyingress_total,";
                #$header .= "dailyingressutilisation,B_dailyegress_guaranteed,B_dailyegress_non-guaranteed,B_dailyegress_total,";
		#$header .= "A_to_B_utilisation,B_dailyingress_guaranteed,B_dailyingress_non-guaranteed,B_dailyingress_total,B_dailyingressutilisation";
		chdir("daily");
		$daily = 1;
	} else {
		$header = $hh;
		#$header = "Timestamp,svckey,logdate,A_ingress_guaranteed,A_ingress_non-guaranteed,A_ingress_total,A SCR for all cells,";
                #$header .= "A_egress_guaranteed,A_egress_non-guaranteed,A_egress_total,B_ingress_guaranteed,B_ingress_non-guaranteed,B_ingress_total,b_scr,B_egress_guaranteed,";
		#$header .= "B_egress_non-guaranteed,B_egress_total,a_igutilisation,B_to_A_utilisation,b_igutilisation,A_to_B_utilisation";

		#$header = "Timestamp,svckey,logdate,inhp,nonguaranteed,intotal,SCR for all cells,";
		#$header .= "eghp,egnonguaranteed,egtotal";
		chdir("hourly");
		$interval = 3600;
	}
        my $busiestday = 0;
        my $maxtraff = 0;
	my $lastday;

	if(-e "$opshandle") {
		my ($fh, $out);
		open($fh, "$opshandle");
                open($out, ">/tmp/data");
		
		while(my $line = <$fh>) {
			chomp($line);
			my @tmp = split(/\s+/, $line);

			if( ($tmp[0] >= $ustart) && ($tmp[0] <= $uend) ) {
				print $out "$line\n";
				$datafound++;
				push @vals, $line;
				my @t = split(/\s+/, $line);
				if($t[5] > $maxtraff) {
					$busiestday = $t[2]; # dd/mm/yyyy format
					$maxtraff = $t[5];
				}
				$lastday = $t[2];
			}
				
		}
		# if busiest day not found - use last day
		$busiestday = (!$busiestday) ? $lastday : $busiestday;
		close($fh); close($out); 
	} else {
		my $c = $ustart;
		while($c <= $uend) {
			push @vals, "$c\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t$ustart\t$uend\n";
			if(type =~ /bday/) { #
				$c += 3600;
			} else {
				$c += 86400;
			}
		}	
		$nodata = "<p class=text>Cannot extract traffic report for $opshandle.</p>\n";
	}

	if($disp =~ /table/i) {
	        #push @retvals, "<p class=header>Traffic Report for $opshandle</p><p class=text>\n";
		push @retvals, "<p class=header>Traffic Report for $displaysvc</p><p class=text>\n";
       		push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=21&et=$encet\">";
       		push @retvals, "Click here to generate another report.</a>\n<p class=text>";

		if($nodata) {
			push @retvals, $nodata;
			return @retvals;
		} elsif(!$datafound) {
			push @retvals, "<p class=text>No traffic data found for this period.</p>\n";
			return @retvals;
		}

		my $th = "<td class=th>";
		my $tp = "<td class=text>";
		my $thx = "<td class=thx>";

		if (1 == 2) { # for debug use only
			push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>Date</td><td class=th>Category</td>";
			# daily data first
			if($daily) {
				push @retvals, "$th Egress Cells</td>$th Bytes (excluding header)</td>$th Daily Utilisation</td></tr>\n";
			} else {
				push @retvals, "$th Egress Cells</td>$th Bytes (excluding header)</td></tr>\n";
	
			}
	
			foreach my $l (@vals) {
				my @tv = split(/\s+/, $l);
				my $totalbytes = 0;
	
				$totalbytes = $tv[3] * 48;
			
				push @retvals, "<tr>$thx $tv[2]</td>$tp Egress Guaranteed</td>$tp $tv[3]</td>$tp $totalbytes</td></tr>\n";
				$totalbytes = $tv[4] * 48;
				push @retvals, "<tr><td></td>$tp Egress Non-guaranteed</td>$tp $tv[4]</td>$tp $totalbytes</td></tr>\n";
	
				$totalbytes = $tv[5] * 48;
				if($daily) {
					push @retvals, "<tr><td></td>$thx Total Egress</td>$thx $tv[5]</td>$thx $totalbytes</td>$thx $tv[6]</td></tr>\n";
				} else {
					push @retvals, "<tr><td></td>$thx Total Egress</td>$thx $tv[5]</td>$thx $totalbytes</td></tr>\n";
				}
			}
			push @retvals, "</table>\n";
		}

		# new summary table
		#
		my ($fromd, $tod);
		my $cnt=0;
		my ($tot_a_eg_guaranteed, $tot_a_eg_nonguaranteed, $tot_a_eg_tot);
		my ($tot_b_eg_guaranteed, $tot_b_eg_nonguaranteed, $tot_b_eg_tot);

		foreach my $l (@vals) {
			my @tv = split(/\s+/, $l);

			# start and end date of period
			if ($cnt == 0) { $fromd = $tv[2]; };
			$tod = $tv[2];

			if ($daily) {
				$tot_a_eg_guaranteed += $tv[3];
				$tot_a_eg_nonguaranteed += $tv[4];
				$tot_a_eg_total += $tv[5];
				$tot_b_eg_guaranteed += $tv[11];
				$tot_b_eg_nonguaranteed += $tv[12];
				$tot_b_eg_total += $tv[13];
			} else {
				$tot_a_eg_guaranteed += $tv[7];
				$tot_a_eg_nonguaranteed += $tv[8];
				$tot_a_eg_total += $tv[9];
				$tot_b_eg_guaranteed += $tv[14];
				$tot_b_eg_nonguaranteed += $tv[15];
				$tot_b_eg_total += $tv[16];
			}
			$cnt++;
		}

                # change format from dd/mm/yyyy to mmm/dd/yyyy
                if ($daily) {
                        my @ttt = split(/\//,$fromd);
                        #$fromd = "$ttt[1]/$ttt[0]/$ttt[2]";
                        $fromd = $mths2{$ttt[1]}."/$ttt[0]/$ttt[2]";
                        my @ttt = split(/\//,$tod);
                        #$tod = "$ttt[1]/$ttt[0]/$ttt[2]";
                        $tod = $mths2{$ttt[1]}."/$ttt[0]/$ttt[2]";
                } else {
                        my @ttx = split(/\:/,$fromd);
                        my @tty = split(/\//,$ttx[3]);
                        $fromd = join(":",@ttx[0,1,2]).":".$mths2{$tty[0]}."/$tty[1]/$tty[2]";
                        my @ttx = split(/\:/,$tod);
                        my @tty = split(/\//,$ttx[3]);
                        $tod = join(":",@ttx[0,1,2]).":".$mths2{$tty[0]}."/$tty[1]/$tty[2]";
                }

		push @retvals, "<p class=header>Summary<p>";
		push @retvals, "<table border=1 cellpadding=5 cellspacing=5><tr><td class=th>Period (mmm/dd/yyyy)</td><td class=text>$fromd GMT - $tod GMT</td></tr>\n";
		push @retvals, "<tr></tr>\n";
		push @retvals, "<tr><td class=th colspan=2>Traffic</td></tr>\n";
		push @retvals, "<tr><td class=text>A to B Guaranteed Cells</td><td class=text>$tot_b_eg_guaranteed</td></tr>\n";
		push @retvals, "<tr><td class=text>A to B Non Guaranteed Cells</td><td class=text>$tot_b_eg_nonguaranteed</td></tr>\n";
		push @retvals, "<tr><td class=text>A to B Total Cells</td><td class=text>$tot_b_eg_total</td></tr>\n";
		push @retvals, "<tr><td class=text>B to A Guaranteed Cells</td><td class=text>$tot_a_eg_guaranteed</td></tr>\n";
		push @retvals, "<tr><td class=text>B to A Non Guaranteed Cells</td><td class=text>$tot_a_eg_nonguaranteed</td></tr>\n";
		push @retvals, "<tr><td class=text>B to A Total Cells</td><td class=text>$tot_a_eg_total</td></tr>\n";
		push @retvals, "</table>\n";
		#
		#

		return @retvals;

	} else {
		
		my $datafile = "/tmp/atmgraph.data.$$.$id";
		my $out;
		my $label;

		if($type =~ /bday/i) { # busiest day
			$header = $hh;
			$datafile .= ".$type";
			my @dvals = split(/\//, $busiestday);
			$maxtraff = 0;
			my $bd = "$dvals[1]/$dvals[0]/$dvals[2]"; # want mm/dd/yyyy
			my $displaybd = $mths2{$dvals[1]}."/".$dvals[0]."/".$dvals[2];

			my @blist = ();
			if($daily) {
				my $fail;
				chdir($dbroot);
				chdir("ATM/hourly");
				if(-e "$opshandle") {
					my ($fh, $out);
 			        	open($fh, "$opshandle");
					while(my $line = <$fh>) {
						next if($line !~ /\s+\d{2}\:\d{2}\:\d{2}\:$bd\s+/);
						my @t = split(/\s+/, $line);
						$t[6] = $t[6] * 3600;
						if($t[5] > $maxtraff) {
							$maxtraff = $t[5];
						}
						if($t[9] > $maxtraff) {
							$maxtraff = $t[9];
						}
						chomp($line);
						$line = join "\t", @t;
						push @blist, $line;
					}
					close($fh);
				} else {
					$fail = "No $opshandle";
					my $c = $ustart;
                			while($c <= $uend) {
                        			push @blist, "$c\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n";
                                		$c += 3600;
					}

				}

				open($out, ">$datafile");
				print $out "$header\n";
				foreach my $l (@blist) {
					print $out "$l\n";
				}
				close($out);
				#$label = "Busiest day $busiestday";
				$label = "Busiest day $displaybd";
			}

		} elsif ($type =~ /dutil/i) { # daily utilisation
			$datafile .= ".$type";
			$maxtraff = 0;
			open($out, ">$datafile");
			print $out "$header\n";
			foreach my $l (@vals) {
				my @t = split(/\s+/, $l);
				$t[10] *= 100;
                                $t[18] *= 100; 
				if($t[6] > $maxtraff) {
					$maxtraff = $t[6];
				}
				#$l = join '     ', @t;
				$l = join "\t", @t;

				print $out "$l\n";
                        }
                        close($out);
			$label = "Daily Utilisation";
		} elsif ($type =~ /hutil/i) { # hourly utilisation
                        $datafile .= ".$type";
                        $maxtraff = 0;
                        open($out, ">$datafile");

                        print $out "$header\n";

                        foreach my $l (@vals) {
                                my @t = split(/\s+/, $l);
                                $t[18] = $t[18] * 100;
                                $t[20] *= 100;
                                  
                                #$l = join '	', @t;
                                $l = join "\t", @t;
                                print $out "$l\n";
                        }
                        close($out);
                        $label = "Hourly Utilisation";
		} else {
			$datafile .= ".traffic";
			#$label = "Daily Usage";
			$label = "Usage";
			# graph for daily traffic
	                #print "Content-tye: image/gif\n\n";
			open($out, ">$datafile");
			print $out "$header\n";
			#foreach my $l (@vals) {
			#	print $out "$l\n";
			#}
			foreach my $l (@vals) {
                                my @v = split(/\s+/, $l);
                                if($interval == 86400) {
                                        for my $i (19..22) {
                                                $v[$i] = $v[$i] * $interval;
                                        }
                                } else { # hourly
                                        $v[6] = $v[6] * $interval;
                                }
                                #$l = join '     ', @v;
                                $l = join "\t", @v;
                                print $out "$l\n";
                        }

			close($out);
			$graphtype = "LINE2";
		}

		&gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "atm", $maxtraff, $label, $graphtype, $interval);
	}
}


# generate graphs (for FR, ATM and MPLS but extensible to other)
# Pre: $start, $end, $disp, $data, $opshandle, $code, $svc, datafile, prod, maxcell, xlabel, graphtype
# Post:
sub gen_atmgraph {
	my ($start, $end, $disp, $data, $opshandle, 
		$code, $svc, $datafile, $prod, $maxcell, 
		$xlabel, $graphtype, $interval, $costype, $aend_pop, $zend_pop,$mVPN_flag) = @_;
	#print see " in gen_atm : graph_error  : $graph_error ";	
	open Raz,">/data1/tmp_log/Chand_graph";
print Raz "aend_pop:$aend_pop\n";
	#print Raz "mVPN_flag--->@_\n";
	print Raz "in sub gen_atmgraph : start:$start, end:$end, disp:$disp, data:$data, opshandle:$opshandle,code:$code, svc:$svc, datafile:$datafile, prod:$prod, maxcell:$maxcell,xlabel:$xlabel, graphtype:$graphtype, interval:$interval, costype:$costype, aend_pop:$aend_pop, zend_pop:$zend_pop mVPN_flag:$mVPN_flag\n";

	$prod = (!$prod) ? "ATM" : $prod;
open(my $oxt, ">/tmp/rchew-test");
#Chandini_IPT
	if (($prod =~ /^iptransit$/i) && ($data =~ /monthly/i)) {
		
		my @sval = split(/\//, $start);
		my @eval = split(/\//, $end);
	
	}
# Sayantan_IPT_Ph_3: 16_Aug_2010 - Start    
	my ($chk_s_digit_month, $s_digit_month, $e_digit_month, $starttime_ipt, $endtime_ipt);
	if ($prod =~ /^iptransit$/i) {
		my(%mth_rev) = ("Jan", "01", "Feb", "02", "Mar", "03", "Apr", "04", "May", "05", "Jun", "06", "Jul", "07", "Aug", "08", "Sep", "09", "Oct", "10", "Nov", "11", "Dec", "12");
		#my @sval = split(/\-/, $start);
		my (@sval, @eval) ;
		my ($syear, $eyear);
#Chandini_IPT
		#Added if conditions for mapping date
		if ($data =~ /monthly/i){
			@sval = split(/\//, $start);
			@eval = split(/\//, $end);
			$s_digit_month = $sval[0];
			$e_digit_month = $eval[0];
			$syear = $sval[2];
			$eyear = $eval[2];
		}else {
			@sval = split(/\-/, $start);
			@eval = split(/\-/, $end);
			$syear = $sval[1];
			$eyear = $eval[1];
			$s_digit_month = $mth_rev{$sval[0]};
			$e_digit_month = $mth_rev{$eval[0]};
		} 
		my $sday = 01;
		#my $syear = $sval[1];
		#$s_digit_month = $mth_rev{$sval[0]};
		$chk_s_digit_month = $s_digit_month + 01;
		if ($chk_s_digit_month == 13) {
			$chk_s_digit_month = 01;
		}
		#my @eval = split(/\-/, $end);
		#$e_digit_month = $mth_rev{$eval[0]};
		my $eday = 31;
		# Finding Yesterday's date in 'MMM-DD-YYYY' format
		$now = time;
		$yest = $now - (1 * 86400);
		@ytv = localtime($yest);
		++$ytv[4] ;
		$ytv[5] += 1900 ;
		$yday = $ytv[3];
		$ymonth = $ytv[4];
		$yyear = $ytv[5];
		if ($ymonth < 10) { $ymonth = "0" . $ymonth; }
		#my $eyear = $eval[1];
		my $is_leap = isleap($eyear);
		if (($e_digit_month == "04") || ($e_digit_month == "06") || ($e_digit_month == "09") || ($e_digit_month == "11")) {
			$eday = 30;
		}
		elsif ($e_digit_month == "02") {
			if ($is_leap == 1){
				$eday = 29;
			} else {
				$eday = 28;
			}
		}
		else {
			$eday = 31;
		}
		if ($ymonth == $e_digit_month){
                	$eday = $yday;
       	 	}

		my ($start_str, $end_str);
#Chandini_IPT
                #Added if conditions for mapping date
		if ($data =~ /monthly/i){
			my(%mth) = ("01", "Jan", "02", "Feb", "03", "Mar", "04", "Apr", "05", "May", "06", "Jun", "07", "Jul", "08", "Aug", "09", "Sep", "10", "Oct", "11", "Nov", "12", "Dec");
			my $s_month = $mth{$sval[0]};
			my $e_month = $mth{$eval[0]};
			$start_str = "$sday $s_month $syear 00:00:00 GMT";
			$end_str = "$eday $e_month $eyear 23:59:59 GMT";
		} else {
			$start_str = "$sday $sval[0] $syear 00:00:00 GMT";
			$end_str = "$eday $eval[0] $eyear 23:59:59 GMT";
		}
		#my $start_str = "$sday $sval[0] $syear 00:00:00 GMT";
		$starttime_ipt = str2time($start_str, "GMT");
		#my $end_str = "$eday $eval[0] $eyear 23:59:59 GMT";
		$endtime_ipt = str2time($end_str, "GMT");
		open (SDLog,">>/tmp/sdLog_001");
		print SDLog "eyear: ($eyear); eday: ($eday);\n";
		print SDLog "start_str: ($start_str); end_str: ($end_str); s_digit_month: ($s_digit_month); e_digit_month: ($e_digit_month); ymonth: ($ymonth);\n";
	}
#Added by Stavan for China Metering Sept17_2010 - Start
my ($starttime_china , $endtime_china);
	if($prod =~ /^china$/) {
		my(%mth_rev) = ("Jan", "01", "Feb", "02", "Mar", "03", "Apr", "04", "May", "05", "Jun", "06", "Jul", "07", "Aug", "08", "Sep", "09", "Oct", "10", "Nov", "11", "Dec", "12");
		my @sval = split(/\s/, $start);
                my $sday = $sval[0];
		my $s_digit_month = $mth_rev{$sval[1]};	
                my $syear = $sval[2];
	        $chk_s_digit_month = $s_digit_month + 01;
                if ($chk_s_digit_month == 13) {
                        $chk_s_digit_month = 01;
                }
				
		my $epochYesterday = time - 24 * 60 * 60;  # subtract secs in day from current epoch time
		# Find date of yesterday, [5,4,3] selects the year,month and day from the list returned by localtime
		my ($yyear, $ymonth, $yday) = (localtime($epochYesterday))[5,4,3];
		$yyear += 1900;
		$ymonth++;

		my @eval = split(/\s/, $end);
		my $eday = $eval[0];
		$e_digit_month = $mth_rev{$eval[1]};	
		if ($ymonth == $e_digit_month){
                        $eday = $yday;
                }
                my $eyear = $eval[2];
			
                $starttime_china = str2time("$sday $sval[1] $syear 00:00:00 GMT", "GMT");
                $endtime_china = str2time("$eday $eval[1] $eyear 00:00:00 GMT", "GMT");
	}
#Added by Stavan for China Metring no Sept17_2010 - End 
# Sayantan_IPT_Ph_3: 16_Aug_2010 - End
	#my ($cls, $costype);
        my ($c0, $c1, $c2, $c3, $c4, $c5, $vtm);
#Added by Stavan for China Metering	
	my $chinaGifError = 0;
	if($prod =~ /^mpls$/i) {
		#$cls = CMS_SERVICE_DB::get_costype_sid($opshandle);
        	#$costype = $$cls[0];

        	# Now set the labels    
        	if($costype =~ /telstra/i) {
	                $c0 = $telstra_cos_label{class0};
       	         	$c1 = $telstra_cos_label{class1};
                	$c2 = $telstra_cos_label{class2};
                	$c3 = $telstra_cos_label{class3};
                	$c4 = $telstra_cos_label{class4};
                	$c5 = $telstra_cos_label{class5};
        	} elsif($costype =~ /pccw/i) {
	                $c0 = $pccw_cos_label{class0};
	                $c1 = $pccw_cos_label{class1};
                	$c2 = $pccw_cos_label{class2};
                	$c3 = $pccw_cos_label{class3};
                	$c4 = $pccw_cos_label{class4};
                	$c5 = $pccw_cos_label{class5};
        	} else {         
	                $c0 = "Default";
	                $c1 = "Class1";
	                $c2 = "Class2";
	                $c3 = "Silver";
	                $c4 = "Class4";
	                $c5 = "Gold";
		}
		#++mVPN	
		if($mVPN_flag == 1){
			$c0 = $telstra_cos_label_mVPN{class0};
			$c1 = $telstra_cos_label_mVPN{class1};
			$c2 = $telstra_cos_label_mVPN{class2};
			$c3 = $telstra_cos_label_mVPN{class3};
			$c4 = $telstra_cos_label_mVPN{class4};
			$c5 = $telstra_cos_label_mVPN{class5};	
		}
		#--mVPN
        }
    ################### VPLS TR START ####################
    if($prod =~ /^vpls$/i) {
	#print LOG "inside vpls if \n";
    	# Now set the labels    
    	if($costype =~ /reachvpls/i) {
                $c0 = $vpls_cos_label{class0};
                $c1 = $vpls_cos_label{class1};
            	$c2 = $vpls_cos_label{class2};
            	$c3 = $vpls_cos_label{class3};
            	$c4 = $vpls_cos_label{class4};
            	$c5 = $vpls_cos_label{class5};
		#$vtm = $vpls_cos_label{vtm};
		}
    }

	#################### VPLS TR END #####################
print $oxt "-- Prod = ($prod)\n";
print $oxt "-- C ($c0) ($c1) ($c2) ($c3) ($c4) ($c5)\n";

	my ($in, @timvals, $tmp, $start, $end, $ix);

	my $daily = ($data =~ /daily/i) ? 1 : 0;
	#print LOG " daily :$daily \n";

	# First prepare xtics
	my @tmpx;
print Raz "datafile: $datafile by chan\n";
	open($in, "$datafile"); 
	my $samplesize = 0;
	$tmp = $start = $ix = 0;
	while(my $line = <$in>) {
#print Raz "line: $line";
		next if($line =~ /\,/);
		$samplesize++;
		push @tmpx, $line;
       		chomp($line);
        	my $tmp = (split(/\s+/, $line))[0];
        	push @timvals, $tmp;
        	if($ix == 0) {
                	$start = $tmp;
                	$ix++;
        	}
        	$end = ($tmp >= $end) ? $tmp : $end;
		print Raz "$line\nEnd :$end".localtime($end).",Tmp: $tmp ,Start : $start".localtime($start)." samplesize($samplesize)\n";
	}
	close($in);
# Sayantan_IPT_Ph_3: 16_Aug_2010 - Start
	if (($prod =~ /^iptransit$/i) && ( $start =~ /FAIL/) && ($end =~ /FAIL/)){
		$starttime_ipt = $endtime_ipt = "FAIL";
	}
# Sayantan_IPT_Ph_3: 16_Aug_2010 - End
	my $startdate = localtime($start);
print Raz "startdate:$startdate\n";
	my $enddate = localtime($end);
print Raz "enddate:$enddate\n";
	chomp($startdate); chomp($enddate);

	my $xl = (!$xlabel) ? "Report from $startdate to $enddate" : $xlabel;

#	print "($startdate) ($start), ($enddate) ($end)\n";
	my ($hrxtic, $xtics);
	my $ix = 0; 
	my $xcount = 1;   

	# set xtics
	my $day = 24 * 60 * 60;
	my $period = $end - $start;
	if($period > 432000) {
		$hrxtic = 120;	
	} else {
		$hrxtic = 24;
	}

#if($xl =~ /day/i) {
#open(my $out1, ">/tmp/rchewxx");
#print $out1 "Start -> ($start), End -> ($end)\n";
#close($out1);
#print $out1 "(@timvals)\n";
#print $out1 "(@tmpx)\n";
#close($out1);
#}

	# divide xtics into 5 blocks
	my $startt = $timvals[0];
	print Raz "startt : $startt \n";
	my $endtt = $timvals[$#timvals];
	print Raz "endtt : $endtt \n";
	my $factor = ($endtt - $startt) / 5;
	for my $i (0..5) {
		my $uxdate = int($startt + ($i * $factor));
		my $d = localtime($uxdate);
		my @dvals = split(/\s+/, $d);
		my $date;
		my @dxx = split(/\:/, $dvals[3]);
                $dvals[3] = "$dxx[0]:$dxx[1]";

		# want 1,2,4
		#if($xl !~ /day/i) {
print Raz "interval:$interval, xl: $xl\n";
		if( (!$interval) && ($xl !~ /day/i) ) {
			$date = "$dvals[2] $dvals[1] $dvals[4]";
		} else {
			#$date = "$dvals[3]";
			$date = "$dvals[2]$dvals[1]:$dvals[3]";
		}
		$xtics .= "\"$date\" $uxdate,";
	}
	chop($xtics); # remove trailing ,

	# now set ytics
	# have $maxcell
	my($ytics);
	my $yticl = $maxcell / 4;
	for my $i (0..4) {
		my $t = ($yticl * $i);
		if($i == 0) {
			$ytics = "\"$i\" 0 "; #first tick
		} else {
			#my $t = ($yticl * $i);
			if($xl =~ /data delivery ratio/i) {
				my $tl;
				$tl = $t;
                                $tl = sprintf("%.2f %s", $tl);
                                $tl =~ s/ //g;
                                $ytics .= ", \"$tl\" $t";
			} elsif( ($xl !~ /data delivery ratio/i) && ($xl !~ /daily utilisation/i) && ($prod !~ /^mpls/i)) {
				my $tl;
				$tl = $t / 1000000;
				$tl = sprintf("%.1f %s", $tl);
				$ytics .= ", \"$tl\" $t";
			} else {
				my $tl;
				$tl = $t;
				$tl = sprintf("%.2f %s", $tl);
				$tl =~ s/ //g;
				$ytics .= ", \"$tl\" $t";
                        }

		}
	}

	my ($rrd, $plot, $out, $ylabel);
print Raz "ctlfile: $ctlfile\n";
	open ($out, ">$ctlfile");
	print $out "#!/bin/sh\n";

	if($prod =~ /ATM/i) {
		if($xl =~ /daily util/i) {
			$plot = "\'$datafile\' using 1:7 title \"Daily Utilisation A to B\" with lines";
			$plot .= ",\'$datafile\' using 1:11 title \"Daily Utilisation B to A\" with lines";
			$ylabel = "% Utilisation";

			OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end, 
			86400, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
			"10:-:Daily Utilisation A to B",
                        "18:-:Daily Utilisation B to A");

			#"6:-:Daily Utilisation A to B", 
			#"10:-:Daily Utilisation B to A");
			$rrd = 1;

		} elsif ($xl =~ /busiest day/i) {
			$ylabel = "Cells";
			$plot = "\'$datafile\' using 1:8 title \"A to B Cell Guaranteed\" with lines, \'$datafile\' using 1:9 title \"A to B Cell Non-guaranteed\" with lines,";
			$plot .= "\'$datafile\' using 1:4 title \"B to A Cell Guaranteed\" with lines, \'$datafile\' using 1:5 title \"B to A Cell Non-guaranteed\" with lines";

			OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        3600, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
			"3:-:A to B Guaranteed Cell",
                        "4:-:A to B Non-guaranteed Cell",
                        "10:-:B to A Guaranteed Cell",
                        #"11:-:B to A Non-guaranteed Cell","6:-:SCR");
                        "11:-:B to A Non-guaranteed Cell","6:-:PVC"); # change from SCR to PVC 

                        #"7:-:A to B Guaranteed Cell",
                        #"8:-:A to B Non-guaranteed Cell",
			#"3:-:B to A Guaranteed Cell",
			#"4:-:B to A Non-guaranteed Cell");
                        $rrd = 1;
		} elsif ($xl =~ /hourly util/i) {
                        $ylabel = "% Utilisation";

                        OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        3600, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
                        "20:-:Hourly Utilisation A to B",
                        "18:-:Hourly Utilisation B to A");
                        $rrd = 1;

		} else {
			$ylabel = "Cells";
			$plot = "\'$datafile\' using 1:4 title \"A to B Cell Guaranteed\" with lines, \'$datafile\' using 1:5 title \"A to B Cell Non-guaranteed\" with lines";
			$plot .= ",\'$datafile\' using 1:8 title \"B to A Cell Guaranteed\" with lines, \'$datafile\' using 1:9 title \"B to A Cell Non-guaranteed\" with lines";
                        #OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        #$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
			#"7:-:Guaranteed Traffic A to B", "8:-:Non-Guaranteed Traffic A to B",
                        #"15:-:Guaranteed Traffic B to A","16:-:Non-Guaranteed Traffic B to A",
			#"19:-:SCR");

			if($interval == 86400) {
                                OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
				"11:-:Guaranteed Traffic A to B", "12:-:Non-Guaranteed Traffic A to B",
                                "3:-:Guaranteed Traffic B to A","4:-:Non-Guaranteed Traffic B to A");

                                #"7:-:Guaranteed Traffic A to B", "8:-:Non-Guaranteed Traffic A to B",
                                #"15:-:Guaranteed Traffic B to A","16:-:Non-Guaranteed Traffic B to A");
                                # "19:-:SCR");
                        } else { #hourly
                                OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
				"14:-:Guaranteed Traffic A to B", "15:-:Non-Guaranteed Traffic A to B",
                                "7:-:Guaranteed Traffic B to A","8:-:Non-Guaranteed Traffic B to A"); #,
				#"6:-:SCR"); #removed at AM's request jul/17/2003

                                #"7:-:Guaranteed Traffic A to B", "8:-:Non-Guaranteed Traffic A to B",
                                #"15:-:Guaranteed Traffic B to A","16:-:Non-Guaranteed Traffic B to A",
                                #"6:-:SCR");
                        }

                        #"3:-:Guaranteed Traffic A to B", "4:-:Non-Guaranteed Traffic A to B",
                        #"7:-:Guaranteed Traffic B to A","8:-:Non-Guaranteed Traffic B to A");
                        $rrd = 1;
		}
	} elsif ($prod =~ /FR/i) {
		# Frame Relay Graphs
		if($xl =~ /daily util/i) {
			$plot = "\'$datafile\' using 1:6 title \"Daily Utilisation A to B\" with lines";
			$plot .= ",\'$datafile\' using 1:9 title \"Daily Utilisation B to A\" with lines";
			$ylabel = "% Utilisation";

			OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        86400, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
                        "8:-:Daily Utilisation A to B",
                        "5:-:Daily Utilisation B to A");
                        $rrd = 1;

		} elsif ($xl =~ /busiest day/i) {
			$ylabel = "Bytes";
			$plot = "\'$datafile\' using 1:4 title \"A to B CIR\" with lines, \'$datafile\' using 1:5 title \"A to B Total MB\" with lines,";
			$plot .= "\'$datafile\' using 1:6 title \"B to A CIR\" with lines, \'$datafile\' using 1:7 title \"B to A Total MB\" with lines";

			OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        3600, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
                        #"4:-:A to B Total MB","3:-:A to B CIR",
                        #"6:-:B to A Total MB","5:-:B to A CIR",
			"6:-:A to B Total MB","5:-:A to B CIR",
			"4:-:B to A Total MB","3:-:B to A CIR",
			"17:-:Service CIR");
                        $rrd = 1;

		} elsif ($xl =~ /hourly util/i) {
                        OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        3600, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
                        "19:-:Hourly Utilisation A to B",
                        "18:-:Hourly Utilisation B to A");
                        $rrd = 1;

		} elsif ($xl =~ /data delivery ratio/i) {
			$ylabel = "DDR";
			$plot = "\'$datafile\' using 1:2 title \"A to B CIR DDR\" with lines, \'$datafile\' using 1:3 title \"B to A CIR DDR\" with lines,";
			$plot .= "\'$datafile\' using 1:4 title \"A to B EIR DDR\" with lines, \'$datafile\' using 1:5 title \"B to A EIR DDR\" with lines";

			OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        86400, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
                        "2:-:B to A CIR DDR", "1:-:A to B CIR DDR",
                        "4:-:B to A EIR DDR", "3:-:A to B EIR DDR");
                        $rrd = 1;

			
		} else {
			$ylabel = "Bytes";
			#$xlabel = "Daily Usage";
			$xlabel = "Usage";
			$plot = "\'$datafile\' using 1:4 title \"A to B CIR\" with lines, \'$datafile\' using 1:5 title \"A to B Total MB\" with lines,";
			$plot .= "\'$datafile\' using 1:7 title \"B to A CIR\" with lines, \'$datafile\' using 1:8 title \"B to A Total MB\" with lines";

			if($interval == 3600) {
                                OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
                                "6:-:A to B Total MB","5:-:A to B CIR",
                                "4:-:B to A Total MB","3:-:B to A CIR",
                                "17:-:Service CIR");
                        } else {
                                OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype, "",
                                "7:-:A to B Total MB","6:-:A to B CIR",
                                "4:-:B to A Total MB","3:-:B to A CIR");
                        }

			#if($interval == 3600) {
                        #        OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        #        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                        #        "4:-:A to B Total MB","3:-:A to B CIR",
                        #        "7:-:B to A Total MB","6:-:B to A CIR", 
                        #        "17:-:Service CIR");
                        #} else {
                        #        OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        #        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                        #        "4:-:A to B Total MB","3:-:A to B CIR",
                        #        "7:-:B to A Total MB","6:-:B to A CIR");
                        #}

			#OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        #$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                        #"4:-:A to B Total MB","3:-:A to B CIR",
                        #"7:-:B to A Total MB","6:-:B to A CIR");

                        $rrd = 1;

		}
	} elsif ($prod =~ /^mpls$/i) {
		my $mVPN_enabled = CMS_OLSS::check_mVPN_service($opshandle);
		#print Raz "mVPN_enabled--->$mVPN_enabled\n";

		# IPVPN
		$graphtype = "LINE2:LINE2";

		# calcuate x-axis grid
		my $num_of_grid = int((($end-$start) / 86400)/10);
                if ($num_of_grid < 1) {
                	$num_of_grid = 1;
                }
                my $precision = $num_of_grid * 86400;

                if($interval == 1) { # 5 min
			$interval = 300;
                        $ylabel = "kbps";

                        if($xlabel !~ /egress/i) {
				$xlabel = "Report";
                                #OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                #        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
				#	"DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                                #        "1:-:Standard Class In","2:-:Standard Class Out",
                                #        "3:-:Silver Class In", "4:-:Silver Class Out",
                                #        "5:-:Gold Class In", "6:-:Gold Class Out");

####################################################################################################
# Comment when PCCW supports CLASS 1 & 2

                        if($costype =~ /pccw/i) {
                                OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                                       "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                                        "1:-:$c0 In","2:-:$c0 Out",
                                        "3:-:$c3 In", "4:-:$c3 Out",
                                        "5:-:$c4 In", "6:-:$c4 Out",
                                        "7:-:$c5 In", "8:-:$c5 Out");
                        } else {

####################################################################################################
				#++mVPN
				print Raz "inside else of PCCW\n";
				print Raz "mVPN_flag:$mVPN_flag\n";
				if($mVPN_flag == 1){
				print Raz "inside mVPN_flag condition\n";
				print Raz "c5 before rrd plot:$c5\n";
                                OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                                       "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                                         "1:-:$c0 In","2:-:$c0 Out",
                                         "3:-:$c1 In    ", "4:-:$c1 Out    ",
                                         "5:-:$c2 In ", "6:-:$c2 Out ",
                                         "7:-:$c3 In    ", "8:-:$c3 Out    ",
                                         "9:-:$c4 In            ", "10:-:$c4 Out            ",
					#Karuna - comment after minor enh testing
                                         #"11:-:$c5 In            ");
					 "11:-:$c5 In                       ", "12:-:$c5 out");
				}else{
#Chandini_mVPN
				print Raz "else of mvpn condition\n";
			 	my $c5out;	
				print Raz "mVPN_enabled: $mVPN_enabled\n";
				if ($mVPN_enabled == 1){
					$c5out ="$c5 Out (Unicast + Multicast)";
				}else{
                                        $c5out="$c5 Out";
                                }
				print Raz "c5out:$c5out\n";

				#--MVPN
				OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                                       "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                                         "1:-:$c0 In        ","2:-:$c0 Out           ",
                                         "3:-:$c1 In        ", "4:-:$c1 Out               ",
                                         "5:-:$c2 In        ", "6:-:$c2 Out            ",
                                         "7:-:$c3 In        ", "8:-:$c3 Out               ",
                                         "9:-:$c4 In                           ", "10:-:$c4 Out                       ",
                                         "11:-:$c5 In                                ", "12:-:$c5out");
				}

####################################################################################################
# Comment when PCCW supports CLASS 1 & 2

                        }

####################################################################################################
				
                                $rrd = 1;

                                #$plot = "\'$datafile\' using 1:2 title \"Standard Class In\" with lines, \'$datafile\' using 1:4 title \"Silver Class In\" with lines, \'$datafile\' using 1:6 title \"Gold Class In\",\'$datafile\' using 1:3 title \"Standard Class Out\" with lines, \'$datafile\' using 1:5 title \"Silver Class Out\" with lines, \'$datafile\' using 1:7 title \"Gold Class Out\" with linespoints 8";
                                #$plot = "\'$datafile\' using 1:2 title \"Standard Class In\" with lines, \'$datafile\' using 1:4 title \"Silver Class In\" with lines, \'$datafile\' using 1:6 title \"Gold Class In\" with lines";                        } else {
				# not used
				print Raz "before plot datafile:$datafile\n";
                                $plot = "\'$datafile\' using 1:3 title \"Standard Class Out\" with lines, \'$datafile\' using 1:5 title \"Silver Class Out\" with lines, \'$datafile\' using 1:7 title \"Gold Class Out\" with linespoints";
				print Raz "plot:$plot";
                        }
			print Raz "\n\n";
                } else { #daily
			$interval = 86400;
                        $ylabel = "kbps";
                        if($xlabel !~ /egress/i) {
				$xlabel = "Report";
                                #OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                #        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
				#	"DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                                #        "1:-:Standard Class In","2:-:Standard Class Out",
                                #        "3:-:Silver Class In", "4:-:Silver Class Out",
                                #        "5:-:Gold Class In", "6:-:Gold Class Out");

####################################################################################################
# Comment when PCCW supports CLASS 1 & 2

		        if($costype =~ /pccw/i) {
                                OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                                       "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                                        "1:-:$c0 In","2:-:$c0 Out",
                                        "3:-:$c3 In", "4:-:$c3 Out",
                                        "5:-:$c4 In", "6:-:$c4 Out",
                                        "7:-:$c5 In", "8:-:$c5 Out");
			} else {

####################################################################################################
				#++mVPN
				if($mVPN_flag == 1){
                                OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                                       "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                                         "1:-:$c0 In","2:-:$c0 Out",
                                         "3:-:$c1 In    ", "4:-:$c1 Out    ",
                                         "5:-:$c2 In ", "6:-:$c2 Out ",
                                         "7:-:$c3 In    ", "8:-:$c3 Out    ",
                                         "9:-:$c4 In            ", "10:-:$c4 Out            ",
					#Karuna - minor enh testing
                                         #"11:-:$c5 In            ");
                                         "11:-:$c5 In                      ","12:-:$c5 Out");
				}else{
#Chandini_mVPN
                                if ($mVPN_enabled == 1){
                                        $c5out="$c5 Out (Unicast + Multicast)";

                                }else{
					$c5out="$c5 Out";
				}

				#--mVPN
                                OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                                        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                                       "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                                         "1:-:$c0 In        ","2:-:$c0 Out           ",
                                         "3:-:$c1 In        ", "4:-:$c1 Out               ",
                                         "5:-:$c2 In        ", "6:-:$c2 Out            ",
                                         "7:-:$c3 In        ", "8:-:$c3 Out               ",
                                         "9:-:$c4 In                           ", "10:-:$c4 Out                       ",
                                         "11:-:$c5 In                                ", "12:-:$c5out");
                                }
####################################################################################################
# Comment when PCCW supports CLASS 1 & 2

        		}

####################################################################################################

                                $rrd = 1;

                                #$plot = "\'$datafile\' using 1:2 title \"Standard Class In\" with lines, \'$datafile\' using 1:4 title \"Silver Class In\" with lines, \'$datafile\' using 1:6 title \"Gold Class In\",\'$datafile\' using 1:3 title \"Standard Class Out\" with lines, \'$datafile\' using 1:5 title \"Silver Class Out\" with lines, \'$datafile\' using 1:7 title \"Gold Class Out\" with linespoints 8";
                                #$plot = "\'$datafile\' using 1:2 title \"Standard Class In\" with lines, \'$datafile\' using 1:4 title \"Silver Class In\" with lines, \'$datafile\' using 1:6 title \"Gold Class In\" with lines";
                        } else {
				# not used
                                $plot = "\'$datafile\' using 1:3 title \"Standard Class Out\" with lines, \'$datafile\' using 1:5 title \"Silver Class Out\" with lines, \'$datafile\' using 1:7 title \"Gold Class Out\" with linespoints";
                        }
				chomp($l);
                }
	} elsif ($prod =~ /^mplstetunnel$/i) {
		# MPLS TE Tunnel
		$graphtype = "LINE2:LINE2";

		# calcuate x-axis grid
		my $num_of_grid = int((($end-$start) / 86400)/10);
                if ($num_of_grid < 1) {
                	$num_of_grid = 1;
                }
                my $precision = $num_of_grid * 86400;

                if($interval == 1) { # 5 min
			$interval = 300;
		} else {
			$interval = 86400;
		}
		#Chandini_TE_TIR01
		#Added if condition to display the lable change 
                #$ylabel = "kbps";
                $ylabel = "bps";
		$xlabel = "Report";

		OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                         $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                         "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                         "1:-:Incoming Traffic ($zend_pop to $aend_pop)","2:-:Outgoing Traffic ($aend_pop to $zend_pop)");
                $rrd = 1;
	}
	################### VPLS TR START ####################
	elsif ($prod =~ /^vpls$/i) {
		# VPLS
		$graphtype = "LINE2:LINE2";

		# calcuate x-axis grid
		my $num_of_grid = int((($end-$start) / 86400)/10);
        if ($num_of_grid < 1) {
        	$num_of_grid = 1;
        }
        my $precision = $num_of_grid * 86400;

        if($interval == 1) { # 5 min
			$interval = 300;
            $ylabel = "kbps";

            if($xlabel !~ /egress/i){
				$xlabel = "Report";
                #OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                #        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
				#	     "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                #        "1:-:Standard Class In","2:-:Standard Class Out",
                #        "3:-:Silver Class In", "4:-:Silver Class Out",
                #        "5:-:Gold Class In", "6:-:Gold Class Out");

####################################################################################################
# CLASS 1 & 2

                if($costype =~ /reachvpls/i) {
					OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                    $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                    "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                    "1:-:$c0 In         ","2:-:$c0 Out           ",
                    "3:-:$c1 In           ", "4:-:$c1 Out               ",
                    "5:-:$c2 In                ", "6:-:$c2 Out            ",
                    "7:-:$c3 In                ", "8:-:$c3 Out               ",
                    "9:-:$c4 In         ", "10:-:$c4 Out                       ",
                    "11:-:$c5 In                                ", "12:-:$c5 Out");
	            #"13:-:$vtm In", "14:-:$vtm Out");
 				}
				
                $rrd = 1;

          		# not used
                $plot = "\'$datafile\' using 1:3 title \"standard class out\" with lines, \'$datafile\' using 1:5 title \"class 3 out\" with lines, \'$datafile\' using 1:7 title \"class 5 out\" with linespoints";
            }
        } else { #daily
			$interval = 86400;
            $ylabel = "kbps";
            if($xlabel !~ /egress/i) {
				$xlabel = "Report";
                #		 OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                #        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
				#		 "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                #        "1:-:Standard Class In","2:-:Standard Class Out",
                #        "3:-:Silver Class In", "4:-:Silver Class Out",
                #        "5:-:Gold Class In", "6:-:Gold Class Out");

####################################################################################################
# CLASS 1 & 2

		        if($costype =~ /reachvpls/i) {
					OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                    $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                    "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                    "1:-:$c0 In          ","2:-:$c0 Out            ",
                    "3:-:$c1 In            ", "4:-:$c1 Out                ",
                    "5:-:$c2 In                 ", "6:-:$c2 Out             ",
                    "7:-:$c3 In                 ", "8:-:$c3 Out                ",
                    "9:-:$c4 In          ", "10:-:$c4 Out                        ",
                    "11:-:$c5 In                               ", "12:-:$c5 Out");
		    #"13:-:$vtm In", "14:-:$vtm Out");
        		}
                $rrd = 1;
          } 
          else {
				# not used
                $plot = "\'$datafile\' using 1:3 title \"Standard Class Out\" with lines, \'$datafile\' using 1:5 title \"Critical Class Out\" with lines, \'$datafile\' using 1:7 title \"Voice Class Out\" with linespoints";
          }
       }
	}
        #################### VPLS TR END #####################


#prashant_EPL
        elsif ($prod =~ /^epl$/i) 
	{
                $graphtype = "LINE2:LINE2";

                # calcuate x-axis grid
                my $num_of_grid = int((($end-$start) / 86400)/10);
        	if ($num_of_grid < 1) {
                	$num_of_grid = 1;
        	}
        	my $precision = $num_of_grid * 86400;

                $ylabel = "Mbps";
                $xlabel = "Report";

		if($interval == 1) { # 15 min
                	$interval = 900;
		        OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                    			$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                    			"DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                    			"1:-:Traffic(A->Z)","2:-:Traffic(Z->A)");
                        $rrd = 1;
        	}
		else 
		{ 
                	$interval = 86400;
	                OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                    			$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                    			"DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                    			"1:-:Traffic(A->Z)","2:-:Traffic(Z->A)","3:-:1 hour peak utilization(A->Z)","4:-:1 hour peak utilization(Z->A)");
                        $rrd = 1;
       		}
	}
	
#Chandini_EVPL
        elsif ($prod =~ /^evpl$/i) {
		print Raz " inside gen_atm elsif \n";
                $graphtype = "LINE2:LINE2";

                # calcuate x-axis grid
                my $num_of_grid = int((($end-$start) / 86400)/10);
        if ($num_of_grid < 1) {
                $num_of_grid = 1;
        }
        my $precision = $num_of_grid * 86400;
	#print Raz "precision-------->$precision     interval----->$interval\n";
	#print Raz " costype:$costype \n";
#Chandinichange
	if ($costype =~ /Standard/i || $costype =~ /Premium/i) {

        if($interval == 1) { # 5 min
        	$interval = 300;
            	$ylabel = "Mbps";
            	if($xlabel !~ /egress/i){
                	$xlabel = "Report";
		        OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                    $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                    "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                    "1:-:$costype(A->Z)" ,"2:-:$costype(Z->A)");
                        $rrd = 1;
            	}
        } else { #daily
                $interval = 86400;
            	$ylabel = "Mbps";
            	if($xlabel !~ /egress/i) {
                     	$xlabel = "Report";
                                        OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                    $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                    "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                    "1:-:$costype(A->Z)","2:-:$costype(Z->A)" ,"3:-:1 hour peak utilization(A->Z)","4:-:1 hour peak utilization(Z->A)");
                	$rrd = 1;
         	 }
       }
	}
 	else {	
	if($interval == 1) { # 5 min
                $interval = 300;
                $ylabel = "Mbps";
                if($xlabel !~ /egress/i){
                        $xlabel = "Report";
                                   OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                    $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                    "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                    "1:-:Traffic(A->Z)","2:-:Traffic(Z->A)");
                        $rrd = 1;
                }
        } else { #daily
                $interval = 86400;
                $ylabel = "Mbps";
                if($xlabel !~ /egress/i) {
                        $xlabel = "Report";
print Raz "before rrdplot:$datafile, $giffile, $start, $end,$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype\n";
                                        OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                    $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                    "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                    "1:-:Traffic(A->Z)","2:-:Traffic(Z->A)","3:-:1 hour peak utilization(A->Z)","4:-:1 hour peak utilization(Z->A)");
                        $rrd = 1;
                 }
       }
	$plot = "\'$datafile\' using 1:3 title \"standard class out\" with lines, \'$datafile\' using 1:5 title \"class 3 out\" with lines, \'$datafile\' using 1:7 title \"class 5 out\" with linespoints";
     }
    }
#Chandini_EVPL
	#Chandini_IPT-20100816
	#graph generation for iptransit
	elsif ($prod =~ /^iptransit$/i) {
                $graphtype = "LINE2:LINE2";
                # calcuate x-axis grid
                my $num_of_grid = int((($end-$start) / 86400)/10);
		if ($num_of_grid < 1) {
			$num_of_grid = 1;
		}
		my $precision = $num_of_grid * 86400;
		my $ylabel = "Mbps";
		#for creating monthly graph , the rrd_plot is invoked here
		#CRQ000000004815 :Chandini 19-Nov-2011 # label change to add 'Average' for all except 5 min rpt.
		if($data =~ /monthly/i){
			$interval = 86400;
			$xlabel = "Report";
			$num_of_grid = int((($endtime_ipt - $starttime_ipt) / 86400)/30);
			if ($num_of_grid < 1) {
                                         $num_of_grid = 1;
                        }
                        if ($chk_s_digit_month == $e_digit_month) {
                                        $num_of_grid = 2;
                        }
                        $precision = $num_of_grid * 86400;
			my $str = "DAY:1:MONTH:1:WEEK:$num_of_grid:$precision:%d";
			if ($num_of_grid > 1) {
				$precision = 1;
				$str = "DAY:1:MONTH:1:MONTH:1:$precision:%b";
			}
			OLSS_PLOT::rrd_plot($datafile, $giffile, $starttime_ipt, $endtime_ipt,
			$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
			$str,
			"1:-:Average Throughput In        ","2:-:Average Throughput Out                      ","3:-:Throughput (95 percentile)**");
			$rrd = 1;
		} elsif ($data =~ /aggre/i){
			#for generating the aggregate graph
			$interval = 86400;
			$xlabel = "Report";
			$num_of_grid = int((($endtime_ipt - $starttime_ipt) / 86400)/30);
			if ($num_of_grid < 1) {
                                         $num_of_grid = 1;
			}
			if ($chk_s_digit_month == $e_digit_month) {
                                        $num_of_grid = 2;
			}
			$precision = $num_of_grid * 86400;
print SDLog "start: ($start); end: ($end); starttime_ipt: ($starttime_ipt); endtime_ipt: ($endtime_ipt); precision: ($precision); num_of_grid: ($num_of_grid)\n";
			my $str = "DAY:1:MONTH:1:WEEK:$num_of_grid:$precision:%d";
			if ($num_of_grid > 1) {
                                $precision = 1;
                                $str = "DAY:1:MONTH:1:MONTH:1:$precision:%b";
                        }
			OLSS_PLOT::rrd_plot($datafile, $giffile, $starttime_ipt, $endtime_ipt,
			$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
			$str,
			"1:-:Average Throughput In        ","2:-:Average Throughput Out                     ","3:-:Throughput (95 percentile)**         ","4:-:ACDR     ");
			$rrd = 1;
# Sayantan_IPT_Ph_3: 20-Aug-2010 - Start
		} elsif ($data =~ /burst/i) {
				$num_of_grid = int((($endtime_ipt - $starttime_ipt) / 86400)/30);
		                if ($num_of_grid < 1) {
               			         $num_of_grid = 1;
               			}
				if ($chk_s_digit_month == $e_digit_month) {
					$num_of_grid = 2;
				}
				$precision = $num_of_grid * 86400;
				$interval = 300;
				$xlabel = "Report";
#open (SDLog,">>/tmp/sdLog_001");
print SDLog "start: ($start); end: ($end); starttime_ipt: ($starttime_ipt); endtime_ipt: ($endtime_ipt); precision: ($precision); num_of_grid: ($num_of_grid)\n";
close (SDLog);
				my $str = "DAY:1:MONTH:1:WEEK:$num_of_grid:$precision:%d";
				#my $str = "";
				#my $str = "DAY:1:MONTH:1:DAY:$num_of_grid:$precision:%d";
				if ($num_of_grid > 1) {
					$precision = 1;
					$str = "DAY:1:MONTH:1:MONTH:1:$precision:%b";
				}
				OLSS_PLOT::rrd_plot($datafile, $giffile, $starttime_ipt, $endtime_ipt,
				$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
				$str,
				"1:-: Throughput (95 percentile)**  ","2:-: Combined Peak (Max of In or Out)   ",
				"3:-: ACDR");
				$rrd = 1;
# Sayantan_IPT_Ph_3: 20-Aug-2010 - End	
		#CRQ000000004815 : Chandini 19-Nov-2011 # label change to add 'Average' for all except 5 min rpt
		}elsif(($data =~ /daily/i) || ($data =~ /hourly/i)){
                        #for daily, hourly only the intervals is different the input parameters are same
                        if($data =~ /daily/i){
                                $interval = 86400;
                        } else{
                                $interval = 3600;
                        }
                        $xlabel = "Report";
                        OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
                        $interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
                        "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
                        "1:-:Average Throughput In                " ,"2:-:Average Throughput Out                  ");
                        $rrd = 1;
		} else {
			#for daily, hourly,5 min only the intervals is different the input parameters are same
			$interval = 300;
			$xlabel = "Report";
			OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,
			$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype,
			"DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d",
			"1:-:Throughput In                " ,"2:-:Throughput Out                  ");
			$rrd = 1;
		}
		$plot = "\'$datafile\' using 1:3 title \"standard class out\" with lines, \'$datafile\' using 1:5 title \"class 3 out\" with lines, \'$datafile\' using 1:7 title \"class 5 out\" with linespoints";
print Raz "plot:$plot\n";
	}
#End IPT
#Added by Stavan for China Traffic Metering on 26Aug2010
        elsif ($prod =~ /china/i) {
		my @splitVal = split(/:/,$disp);	
		open (xx,'>/tmp/china.txt');
		print xx "Array : @splitVal\n$disp : $disp"; 
		close xx;
		my $trafficdir = $splitVal[1];
                $graphtype = "LINE2:LINE2";
                # calcuate x-axis grid
                my $num_of_grid = int((($end-$start) / 86400)/10);
                if ($num_of_grid < 1) {
                        $num_of_grid = 1;
                }
                my $precision = $num_of_grid * 86400;
                my $ylabel = "Mbps";
		my $xlabel = "Report";
		my $filesize = -s "$datafile";
		open (xx,">>/tmp/ch1");
		print xx "FILESIZE : $filesize\n";
		close xx;	
		if ($filesize == 0) {
			$chinaGifError = 1;
		}
		if($data =~ /monthly/i){
			$num_of_grid = int((($endtime_china - $starttime_china) / 86400)/30);
			if ($num_of_grid < 1) {
			      $num_of_grid = 1;
			 }
			 if ($chk_s_digit_month == $e_digit_month) {
				$num_of_grid = 2;
			 }
			 $precision = $num_of_grid * 86400;
			 $interval = 86400;
			 $xlabel = "Report";
			 my $str = "DAY:1:MONTH:1:WEEK:$num_of_grid:$precision:%d";
			 if ($num_of_grid > 1) {
				$precision = 1;
				$str = "DAY:1:MONTH:1:MONTH:1:$precision:%b";
			 }
                        OLSS_PLOT::rrd_plot($datafile, $giffile, $starttime_china, $endtime_china,$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype, $str,"3:-:$trafficdir CT           ","4:-:$trafficdir CU\\n","5:-:$trafficdir CT+CU        ","6:-:$trafficdir CT+CU (95 percentile)*\\n","7:-:Committed IN\/OUT");
			 $rrd = 1;
                }
		else {
                        if($data =~ /daily/i){
                                $interval = 86400;
                        } elsif ($data =~ /hourly/i){
                                $interval = 3600;
                        } else {
                                $interval = 300;
                        }
                        #OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype, "DAY:1:WEEK:1:DAY:$num_of_grid:$precision:%d","1:-:$trafficdir CT    ","2:-:$trafficdir CU\\n","3:-:$trafficdir CT+CU");
                        OLSS_PLOT::rrd_plot($datafile, $giffile, $start, $end,$interval, $samplesize, $ylabel, $xlabel, 0, $graphtype, "","1:-:$trafficdir CT    ","2:-:$trafficdir CU\\n","3:-:$trafficdir CT+CU");
                        $rrd = 1;
                }
	
                $plot = "\'$datafile\' using 1:3 title \"standard class out\" with lines, \'$datafile\' using 1:5 title \"class 3 out\" with lines, \'$datafile\' using 1:7 title \"class 5 out\" with linespoints";

}


	
	print $out <<END;

$gnuplot <<ENDGNU
set terminal pbm color small
set output \'$ppmfile\'
set grid
set key   
#set no label
set xtics ($xtics)
set ytics ($ytics)
set xrange [$start:$end]  
set yrange [0:]
set ylabel "$ylabel"
set noarrow
set xlabel "$xl"
set size 0.9,0.7
plot $plot
ENDGNU
        
$ppmtogif < $ppmfile > $giffile
rm -f $ppmfile 
rm -f $datafile

END

	close($out);
	if(!$rrd) {
		system("/bin/sh < $ctlfile > /dev/null 2>&1");
	}
	unlink($ctlfile);

	print "Content-tye: image/gif\n\n";
#Ch_mVPN
	#print see "before if in gen_atm \n";
	open (see1, ">/tmp/data1.txt");
print see1 "aend_pop:$aend_pop and fail:$zend_pop\n";
	if (($prod =~ /^mpls$/i) && ($graph_error == 1)) {
#		open($in, "/usr/local/www/www/gia-reach/tgwcustdata/control/reach-gia-template/gia/images/arrow_down.gif");
		open($in, "$failgif");
	}

#Chandini_TE_Tunnel
	elsif (($prod =~ /^mplstetunnel$/i) && ($mpls_tetunnel_errormsg == 1)) {
		open($in,"$mpls_tetunnel_failgif");
	}
#Chandini_IPT
	#if the socket connection is not possible then the error figure is displayed
	elsif (($prod =~ /^iptransit$/i) && ($zend_pop == 1)){ 
		open($in,"$ipt_socket_errorgif");
		#to avoid display of no data error figure
		$aend_pop = 0;
	} elsif (($prod =~ /^iptransit$/i) && ($aend_pop == 1)){
		#if the data is not there for a given period then the error figure is displayed
		open($in,"$ipt_graph_errorgif");
	}
#Stavan_China Metering_20100914
	#if the data is not there for a given period then the error figure is displayed
	elsif (($prod =~ /^china$/i) && ($chinaGifError == 1)) {
		open($in,"$ipt_graph_errorgif");
	} else {
		open($in, "$giffile");
	}
	while(my $line = <$in>) {
		print $line;
	}
	close($in);
	unlink($giffile);
}

# Function to find Leap Year
# Pre: Year (YYYY)
# Post: 1 (if leap year) else 0
sub isleap  {
          my ($year) = @_;
          return 1 if ( ($year % 4 == 0) && ( ($year % 100) || ($year % 400 == 0) ) );
          return 0;
}


# Get unix time given a date
# Pre: mm/dd/yyyy
# Post: uxtime, date
sub get_uxtime {
	my $date = shift;

	$date =~ s/\///g;

        my (@v) = split(//, $date);
        my(@mons)= ('fill', 'jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec');

        my $day = ($v[2] == 0) ? $v[3] : "$v[2]$v[3]";
        my $month = ($v[0] == 0) ? $v[1] : "$v[0]$v[1]";
        my $month = $mons[$month];
        my $year = "$v[4]$v[5]$v[6]$v[7]";

        my $date = "$day $month $year";

        my $time = str2time($date, "GMT");

        return ($time, $date);
}


# Generate ATM/FR latency performance
# Pre: code, tmplroot, daily|weekly|monthlty, reportdate, table
# Post: @vals
sub gen_atmfrlatencyperf {
        my ($code, $tmplroot, $data, $reportdate, $disp) = @_;
        my @retvals = ();

        if ($disp =~ /table/i) {
                if ($data =~ /daily/i) {
                        $perffile = $atmlatencyperf."dailyperf/".$reportdate."_atmlatencyperf.csv";
                } elsif ($data =~ /weekly/i) {
                        $wday = (localtime(timelocal(0,0,0,substr($reportdate,6,2),substr($reportdate,4,2)-1,substr($reportdate,0,4)-1900)))[6];
                        @weekstart = &Add_Delta_Days(substr($reportdate,0,4), substr($reportdate,4,2), substr($reportdate,6,2), -$wday);
                        @weekend = &Add_Delta_Days(substr($reportdate,0,4), substr($reportdate,4,2), substr($reportdate,6,2), -$wday+6);
                        if (length($weekstart[1]) == 1) { $weekstart[1] = "0".$weekstart[1]; }
                        if (length($weekstart[2]) == 1) { $weekstart[2] = "0".$weekstart[2]; }
                        if (length($weekend[1]) == 1) { $weekend[1] = "0".$weekend[1]; }
                        if (length($weekend[2]) == 1) { $weekend[2] = "0".$weekend[2]; }
                        $perffile = $atmlatencyperf."weeklyperf/".$weekstart[0].$weekstart[1].$weekstart[2]."-".$weekend[0].$weekend[1].$weekend[2]."_atmlatencyperf.csv";
                } elsif ($data =~ /monthly/i) {
                        $perffile = $atmlatencyperf."monthlyperf/".substr($reportdate,0,6)."_atmlatencyperf.csv";
                }

                if((-e $perffile) && (-s $perffile)) {
                        my $xx;
                        my $colspan;
                        my %pop = ();
                        my %average_ctd = ();
                        my %mth = ("01", "Jan", "02", "Feb", "03", "Mar", "04", "Apr", "05", "May", "06", "Jun", "07", "Jul", "08", "Aug", "09", "Sep", "10", "Oct", "11", "Nov", "12", "Dec");
                        open($xx, $perffile);
                                push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=9&et=$encet\">";
                                push @retvals, "Click here to generate another report.</a>\n<p class=text><br>\n";
                                while(chomp(my $line = <$xx>)) {
                                        my @field = split /,/, $line;
                                        $pop{$field[0]} = '';
                                        $pop{$field[1]} = '';
                                        $average_ctd{$field[0]."->".$field[1]} = $field[2];
                                }
                                my @sortedpop = sort keys%pop;
                                push @retvals, "<table class=text border=1 cellpadding=2 cellspacing=0>";
                                $colspan = $#sortedpop + 2;
                                if ($data =~ /daily/i) {
                                        push @retvals, "<tr><th colspan=\"".$colspan."\" align=center>Daily Latency Performance Report (msec) - ".substr($reportdate,6,2)."-".$mth{substr($reportdate,4,2)}."-".substr($reportdate,0,4)."</th></tr>\n";
                                } elsif ($data =~ /weekly/i) {
                                        push @retvals, "<tr><th colspan=\"".$colspan."\" align=center>Weekly Latency Performance Report (msec) - ".$weekstart[2]."-".$mth{$weekstart[1]}."-".$weekstart[0]." TO ".$weekend[2]."-".$mth{$weekend[1]}."-".$weekend[0]."</th></tr>\n";
                                } elsif ($data =~ /monthly/i) {
                                        push @retvals, "<tr><th colspan=\"".$colspan."\" align=center>Monthly Latency Performance Report (msec) - ".$mth{substr($reportdate,4,2)}."-".substr($reportdate,0,4)."</th></tr>\n";
                                }
                                $colspan = $#sortedpop + 1;
                                push @retvals, "<tr><td rowspan=2>&nbsp;</td><td colspan=\"".$colspan."\" align=center>Destination</td></tr>\n";
                                push @retvals, "<tr>";
                                foreach my $p (@sortedpop) {
				push @retvals, "<th style=background-color:navy;color:white;font-size:10px;>$p</th>";
                                }
                                push @retvals, "</tr>\n";
                                foreach my $ps (@sortedpop) {
                                        push @retvals, "<tr><th style=background-color:navy;color:white;font-size:10px;>$ps</th>";
                                        foreach my $pd (@sortedpop) {
                                                if ($ps lt $pd) {
                                                        if ($average_ctd{$ps."->".$pd}) {
                                                                push @retvals, "<td align=center>".$average_ctd{$ps."->".$pd}."</td>";
                                                        } else {
                                                                push @retvals, "<td align=center>0</td>";
                                                        }
                                                } else {                        
                                                        push @retvals, "<td bgcolor=\"#005EAD\">&nbsp;</td>";
                                                }
                                        }
                                        push @retvals, "</tr>\n";
                                }                      
                                push @retvals, "</table>\n";
                        close($xx);
                } else {
                        push @retvals, "<p class=text>No data found for this report date.</p><hr>\n";
                        push @retvals, &screen_latencyperf($code, $tmplroot);
                }
        }
        return @retvals;
}


# Generate Cable Paths Routing Paths
# Pre: code, tmplroot, Primary|Alternative
# Post: @vals
sub gen_cablepathrpt {
	my ($code, $tmplroot, $route) = @_;
	my $cppath;
	my $cpfile;
	my $cecolor;

	my $pcet = ($etproduct) ? $etproduct : "ATM";
	if ($pcet =~ /ATM/i) {
		$cppath = $atmcablepath;
		if ($route =~ /Primary/i) {
			$cecolor = '#005EAD';
		} elsif ($route =~ /Alternative/i) {
			$cecolor = '#CC6699';
		}
	} elsif ($pcet =~ /FR/i) {
		$cppath = $frcablepath;
		if ($route =~ /Primary/i) {
			$cecolor = '#009999';
		} elsif ($route =~ /Alternative/i) {
			$cecolor = '#996699';
		}
	}

	if ($route =~ /Primary/i) {
		$cpfile = $cppath."primary_cablepaths.csv";
	} elsif ($route =~ /Alternative/i) {
		$cpfile = $cppath."alternative_cablepaths.csv";
	}

	my $cpdesc = $cppath."cable_description.csv";

	my @retvals = ();
        my %pop = ();
	my %cablepath = ();
	my %cabledesc = ();

        if((-e $cpfile) && (-s $cpfile)) {
		my $xx;
		open($xx, $cpfile);
		while(chomp(my $line = <$xx>)) {
			my @field = split /,/, $line;
			$pop{$field[0]} = '';
                        $cablepath{$field[0]."->".$field[1]} = $field[4];
		}
		close($xx);
		my @sortedpop = sort keys%pop;
		push @retvals, "<table border=0>\n";
		push @retvals, "<tr valign=top><td width=690>\n";
		if ($route =~ /Primary/i) {
			push @retvals, "<div id=tbl-container_pri>\n";
			push @retvals, "<table id=tbl_pri>\n";
		} elsif ($route =~ /Alternative/i) {
			push @retvals, "<div id=tbl-container_alt>\n";
			push @retvals, "<table id=tbl_alt>\n";
		}		
		push @retvals, "<thead>\n";
		push @retvals, "<tr><th align=center>CBR</th>";
		foreach my $p (@sortedpop) {
			push @retvals, "<th align=center>$p</th>";
		}
		push @retvals, "</tr>\n";
		push @retvals, "</thead>\n";
		push @retvals, "<tbody>\n";
		foreach my $ps (@sortedpop) {
			push @retvals, "<tr><td>$ps</td>";
			foreach my $pd (@sortedpop) {
				if (($ps ne $pd) && ($cablepath{$ps."->".$pd}) && ($cablepath{$ps."->".$pd} !~ /^N(\/|.)?A(.)?$/i)) {
					push @retvals, "<td>".$cablepath{$ps."->".$pd}."</td>";
				} else {                        
					if ($route =~ /Primary/i) {
						push @retvals, "<td bgcolor=\"$cecolor\">&nbsp;</td>";
					} elsif ($route =~ /Alternative/i) {
						push @retvals, "<td bgcolor=\"$cecolor\">&nbsp;</td>";
					}
				}
			}
			push @retvals, "</tr>\n";
		}                      
		push @retvals, "</tbody>\n";
		push @retvals, "</table>\n";
		push @retvals, "</div>\n";
		push @retvals, "</td>\n";
		push @retvals, "<td>\n";
		if((-e $cpdesc) && (-s $cpdesc)) {
			my $xx;
			open($xx, $cpdesc);
			while(chomp(my $line = <$xx>)) {
				my @field = split /,/, $line;
				$cabledesc{$field[2]} = $field[3];
			}
			close($xx);
			my @sortedcd = sort {lc($a) cmp lc($b)} keys%cabledesc;
			push @retvals, "<div id=tbl-container_pa>\n";
			push @retvals, "<table id=tbl_pa>\n";
			push @retvals, "<thead>\n";
			push @retvals, "<tr><th width=100>Cable</th><th>Description</th></tr>\n";
			push @retvals, "</thead>\n";
			push @retvals, "<tbody>\n";
			foreach my $cd (@sortedcd) {
				push @retvals, "<tr><td>".$cd."</td><td>".$cabledesc{$cd}."</td></tr>\n";
			}
			push @retvals, "</tbody>\n";
			push @retvals, "</table>\n";
			push @retvals, "</div>\n";
		} else {
			push @retvals, "<p class=text>No cable description find.</p>\n";
		}
		push @retvals, "</td>\n";
		push @retvals, "</table>\n";
	} else {
		push @retvals, "<p class=text>No cable paths data find.</p>\n";
        }
        return @retvals;
}


# Generate Cable Paths Latency Performance
# Pre: code, tmplroot, Primary|Alternative
# Post: @vals
sub gen_cablepathlatencyperf {
	my ($code, $tmplroot, $route) = @_;
	my @retvals = ();
	my $perffile;
	my $bgcolor;
	my $cecolor;

	my $pcet = ($etproduct) ? $etproduct : "ATM";

	if ($route =~ /Primary/i) {
		my ($day, $month, $year) = (localtime)[3..5];
		$year = $year + 1900;
		$month = $month + 1;
		if (length($month) == 1)  { $month = "0".$month; }
		if (length($day) == 1)  { $day = "0".$day; }

		$perffile = $atmlatencyperf."dailyperf/".$year.$month.$day."_atmlatencyperf.csv";
		if (!((-e $perffile) && (-s $perffile))) {
			my @lastdate = &Add_Delta_Days($year, $month, $day, -1);
			if (length($lastdate[1]) == 1) { $lastdate[1] = "0".$lastdate[1]; }
			if (length($lastdate[2]) == 1) { $lastdate[2] = "0".$lastdate[2]; }
			$perffile = $atmlatencyperf."dailyperf/".$lastdate[0].$lastdate[1].$lastdate[2]."_atmlatencyperf.csv";
		}
		if ($pcet =~ /ATM/i) {
			$bgcolor = 'navy';
			$cecolor = '#005EAD';
		} elsif ($pcet =~ /FR/i) {
			$bgcolor = '#006666';
			$cecolor = '#009999';
		}
	} elsif ($route =~ /Alternative/i) {
		if ($pcet =~ /ATM/i) {
			$perffile = $atmcablepath."alternative_cablepaths.csv";
			$bgcolor = '#990066';
			$cecolor = '#CC6699';
		} elsif ($pcet =~ /FR/i) {
			$perffile = $frcablepath."alternative_cablepaths.csv";
			$bgcolor = '#660066';
			$cecolor = '#996699';
		}
	}

	if((-e $perffile) && (-s $perffile)) {
		my $xx;
                my $colspan;
                my %pop = ();
		my %average_ctd = ();
		open($xx, $perffile);
		while(chomp(my $line = <$xx>)) {
	                my @field = split /,/, $line;
                        $pop{$field[0]} = '';
                        $pop{$field[1]} = '';
			if ($route =~ /Primary/i) {
	                        $average_ctd{$field[0]."->".$field[1]} = $field[2];
			} else {
	                        $average_ctd{$field[0]."->".$field[1]} = $field[3];
			}
        	}
		close($xx);
                my @sortedpop = sort keys%pop;
                push @retvals, "<table class=text border=1 cellpadding=2 cellspacing=0>";
                $colspan = $#sortedpop + 1;
                push @retvals, "<tr><td rowspan=2>&nbsp;</td><td style=\"color:black\" colspan=\"".$colspan."\" align=center>Destination</td></tr>\n";
                push @retvals, "<tr>";
                foreach my $p (@sortedpop) {
	                push @retvals, "<th style=background-color:$bgcolor;color:white;font-size:10px;>$p</th>";
                }
                push @retvals, "</tr>\n";
                foreach my $ps (@sortedpop) {
                	push @retvals, "<tr><th style=background-color:$bgcolor;color:white;font-size:10px;>$ps</th>";
                        foreach my $pd (@sortedpop) {
	                        if ($ps lt $pd) {
        	                        if ($average_ctd{$ps."->".$pd} ne '') {
                	                        push @retvals, "<td style=\"color:black\" align=center>".$average_ctd{$ps."->".$pd}."</td>";
                                        } else {
                                                push @retvals, "<td bgcolor=\"$cecolor\">&nbsp;</td>";
 	                                }
                                } else {                        
                                        push @retvals, "<td bgcolor=\"$cecolor\">&nbsp;</td>";
                                }
                        }
                        push @retvals, "</tr>\n";
                }                      
                push @retvals, "</table>\n";
	}	

	return @retvals;
}


# Generate Cable Paths Routing Reports
# Pre: code, tmplroot, aend, zend, indicate
# Post: @vals
sub gen_cablepathrpt_by_node {
        my ($code, $tmplroot, $aend, $zend, $indicate) = @_;
	my $cppath;
	my $bgcolor_pri;
	my $bgcolor_alt;

	my $pcet = ($etproduct) ? $etproduct : "ATM";
	if ($pcet =~ /ATM/i) {
		$cppath = $atmcablepath;
		$bgcolor_pri = 'navy';
		$bgcolor_alt = '#990066';
	} elsif ($pcet =~ /FR/i) {
		$cppath = $frcablepath;
		$bgcolor_pri = '#006666';
		$bgcolor_alt = '#660066';
	}

	my $cpfile_pri = $cppath."primary_cablepaths.csv";
	my $cpfile_alt = $cppath."alternative_cablepaths.csv";
	my $cpdesc = $cppath."cable_description.csv";
	my $Na_message = "--- Not Applicable ---";

	my ($day, $month, $year) = (localtime)[3..5];
	$year = $year + 1900;
	$month = $month + 1;
	if (length($month) == 1)  { $month = "0".$month; }
	if (length($day) == 1)  { $day = "0".$day; }

	my $latencyfile = $atmlatencyperf."dailyperf/".$year.$month.$day."_atmlatencyperf.csv";
	if (!((-e $latencyfile) && (-s $latencyfile))) {
		@lastdate = &Add_Delta_Days($year, $month, $day, -1);
		if (length($lastdate[1]) == 1) { $lastdate[1] = "0".$lastdate[1]; }
		if (length($lastdate[2]) == 1) { $lastdate[2] = "0".$lastdate[2]; }
		$latencyfile = $atmlatencyperf."dailyperf/".$lastdate[0].$lastdate[1].$lastdate[2]."_atmlatencyperf.csv";
	}

	my $pri_cablepath = $Na_message;
	my $pri_connectedtype;
	my $pri_nodepath;	
	my $pri_latencytime = $Na_message;
	my $alt_cablepath = $Na_message;
	my $alt_connectedtype;
	my $alt_nodepath;
	my $alt_latencytime = $Na_message;
        my @retvals = ();
	my %cabledesc = ();
	my @sortedcd = ();

	push @retvals, "<div id=\"tabmenu\">\n";
        push @retvals, "<a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Node\"><span class=\"curposr\">$pcet Routing Report</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Primary\"><span>Primary Cable Paths</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Alternative\"><span>Alternative Cable Paths</span></a>\n";
	push @retvals, "</div>\n";
	push @retvals, "<div id=\"tabmenuborder\"></div>\n";
	push @retvals, "<p><a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=A&et=$encet&rpttype=Node\">";
	push @retvals, "Click here to generate another report.</a>\n<p class=text><br>\n";

	$aend =~ s/\+/ /g;
	$zend =~ s/\+/ /g;

	if ((-e $cpfile_pri) && (-e $cpfile_alt) && (-s $cpfile_pri) && (-s $cpfile_alt) && ($aend ne $zend)) {
		my $xx;
		open($xx, $cpfile_pri);
		while(chomp(my $line = <$xx>)) {
			my @field = split /,/, $line;
			if (($field[0] =~ /^$aend$/i) && ($field[1] =~ /^$zend$/i)) {
				$pri_cablepath = $field[4];
				$pri_cablepath =~ s/^N(\/|.)?A(.)?$/$Na_message/i;
				if ($field[5] == 0) {
					$pri_connectedtype = "Direct Connection: ";
				} else {
					$pri_connectedtype = "Intermediate Nodes: ";
				}
				$pri_nodepath = $field[6];
				last;
			}
		}
		close($xx);

		open($xx, $cpfile_alt);
		while(chomp(my $line = <$xx>)) {
			my @field = split /,/, $line;
			if (($field[0] =~ /^$aend$/i) && ($field[1] =~ /^$zend$/i)) {
				if ($field[3])  { $alt_latencytime = $field[3]." ms"; }
				$alt_cablepath = $field[4];
				$alt_cablepath =~ s/^N(\/|.)?A(.)?$/$Na_message/i;
				if ($field[5] == 0) {
					$alt_connectedtype = "Direct Connection: ";
				} else {
					$alt_connectedtype = "Intermediate Nodes: ";
				}
				$alt_nodepath = $field[6];
				last;
			}
		}
		close($xx);

		if (!(($pri_cablepath eq $Na_message) && ($alt_cablepath eq $Na_message))) {
			open($xx, $cpdesc);
			while(chomp(my $line = <$xx>)) {
				my @field = split /,/, $line;
				if (($pri_cablepath =~ /\Q$field[2]\E/i) || ($alt_cablepath =~ /\Q$field[2]\E/i)) {
					$cabledesc{$field[2]} = $field[3];
				}
				@sortedcd = sort {lc($a) cmp lc($b)} keys%cabledesc;
			}
			close($xx);
		}

		if ($pri_cablepath ne $Na_message) {
			open($xx, $latencyfile);
			while(chomp(my $line = <$xx>)) {
				my @field = split /,/, $line;
				if (($field[0] =~ /^$aend$/i) && ($field[1] =~ /^$zend$/i)) {
					if ($field[2])  { $pri_latencytime = $field[2]." ms"; }
					last;
				}
			}
			close($xx);
		}

		if (!(($pri_cablepath eq $Na_message) && ($alt_cablepath eq $Na_message))) {
			push @retvals, "<p class=text>The Primary and Alternative paths between $aend and $zend are shown below.</p>\n";
			push @retvals, "<table border=0>\n";
			push @retvals, "<tr valign=top><td width=690>\n";
			push @retvals, "<table class=text border=1 cellpadding=5 cellspacing=0 width=660>\n";
			push @retvals, "<tr><th colspan=2 style=background-color:#FFCC99;color:black;>$aend <---> $zend</th></tr>\n";
			push @retvals, "<tr>";
			if ($pri_cablepath ne $Na_message) {	
				if ($indicate != 1) {
					push @retvals, "<th rowspan=2 ";
				} else {
					push @retvals, "<th rowspan=3 ";
				}
			} else {
				push @retvals, "<th ";
			}
			push @retvals, "style=background-color:$bgcolor_pri;color:white; width=130>Primary Path</th><td>$pri_cablepath</td></tr>\n";
			if ($pri_cablepath ne $Na_message) {	
				if ($indicate == 1) {
					push @retvals, "<tr><td>$pri_connectedtype$pri_nodepath</td></tr>\n";
				}
				push @retvals, "<tr><td>Latency Time: $pri_latencytime</td></tr>\n";
			}
			push @retvals, "<tr>";
                        if ($alt_cablepath ne $Na_message) {
                                if ($indicate != 1) {
                                        push @retvals, "<th rowspan=2 ";
                                } else {
                                        push @retvals, "<th rowspan=3 ";
                                }
                        } else {
                                push @retvals, "<th ";
                        }
			push @retvals, "style=background-color:$bgcolor_alt;color:white;>Alternative Path</th><td>$alt_cablepath</td></tr>\n";
                        if ($alt_cablepath ne $Na_message) {
                                if ($indicate == 1) {
                                        push @retvals, "<tr><td>$alt_connectedtype$alt_nodepath</td></tr>\n";
                                }
                                push @retvals, "<tr><td>Latency Time: $alt_latencytime</td></tr>\n";
                        }
			push @retvals, "</table>\n";
			push @retvals, "</td>\n";
			push @retvals, "<td>\n";
			if(@sortedcd > 0) {
	                        push @retvals, "<div id=tbl-container_node>\n";
        	                push @retvals, "<table id=tbl_node>\n";
                        	push @retvals, "<tr><th width=100>Cable</th><th>Description</th></tr>\n";
				foreach my $cd (@sortedcd) {
					push @retvals, "<tr><td>".$cd."</td><td>".$cabledesc{$cd}."</td></tr>\n";
				}
        	                push @retvals, "</table>\n";
	                        push @retvals, "</div>\n";
			} else {
				push @retvals, "<p class=text>No cable description find.</p>\n";
			}
			push @retvals, "</td>\n";
			push @retvals, "</table>\n";
		} else {
			push @retvals, "<p class=text>No cable paths data find.</p>\n";
		}
	} else {
		push @retvals, "<p class=text>No cable paths data find.</p>\n";
	}
	return @retvals;
}


# Generate Cable Paths Report (MS-EXCEL Format)
# Post: @vals
sub gen_cablepathrpt_EXCEL {
	my $cpfile;
	my $ticolor_pri;
	my $cecolor_pri;
	my $ticolor_alt;
	my $cecolor_alt;
	my $cdcolor;

	my $pcet = ($etproduct) ? $etproduct : "ATM";
	if ($pcet =~ /ATM/i) {
		$cppath = $atmcablepath;
		$ticolor_pri = 'navy';
		$cecolor_pri = '#99CCFF';
		$ticolor_alt = '#CC0000';
		$cecolor_alt = '#FF99CC';
		$cdcolor = '#009999';
	} elsif ($pcet =~ /FR/i) {
		$cppath = $frcablepath;
		$ticolor_pri = '#006666';
		$cecolor_pri = '#CCFFCC';
		$ticolor_alt = '#660066';
		$cecolor_alt = '#CCCCFF';
		$cdcolor = 'navy';
	}

        my $cpfile_pri = $cppath."primary_cablepaths.csv";
        my $cpfile_alt = $cppath."alternative_cablepaths.csv";
        my $cpdesc = $cppath."cable_description.csv";

        my @retvals = ();
        my %pop = ();
        my %cablepath_pri = ();
        my %cablepath_alt = ();
        my %cabledesc = ();

        if ((-e $cpfile_pri) && (-s $cpfile_pri) && (-e $cpfile_alt) && (-s $cpfile_alt) && (-e $cpdesc) && (-s $cpdesc)) {
                my $xx;
                open($xx, $cpfile_pri);
                while(chomp(my $line = <$xx>)) {
                        my @field = split /,/, $line;
                        $pop{$field[0]} = '';
                        $cablepath_pri{$field[0]."->".$field[1]} = $field[4];
                }
                close($xx);
                open($xx, $cpfile_alt);
                while(chomp(my $line = <$xx>)) {
                        my @field = split /,/, $line;
                        $cablepath_alt{$field[0]."->".$field[1]} = $field[4];
                }
                close($xx);
                open($xx, $cpdesc);
                while(chomp(my $line = <$xx>)) {
                        my @field = split /,/, $line;
                        $cabledesc{$field[2]} = $field[3];
                }
                close($xx);

                my @sortedpop = sort keys%pop;
                my @sortedcd = sort {lc($a) cmp lc($b)} keys%cabledesc;
                my $colspan = $#sortedpop + 2;

                push @retvals, "<json xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-json40\">\n";
                push @retvals, "<head>\n";
                push @retvals, "<xml>\n";
                push @retvals, " <o:DocumentProperties>\n";
                push @retvals, "  <o:Subject>".$pcet." Cable Paths</o:Subject>\n";
                push @retvals, "  <o:Author>Online Service Support Centre (OLSS)</o:Author>\n";
                push @retvals, "  <o:Company>Reach</o:Company>\n";
                push @retvals, " </o:DocumentProperties>\n";
                push @retvals, " <x:ExcelWorkbook>\n";
                push @retvals, "  <x:ExcelWorksheets>\n";
                push @retvals, "   <x:ExcelWorksheet>\n";
                push @retvals, "    <x:Name>".$pcet." Cable Paths</x:Name>\n";
                push @retvals, "    <x:WorksheetOptions>\n";
                push @retvals, "     <x:ProtectContents>False</x:ProtectContents>\n";
                push @retvals, "     <x:ProtectObjects>False</x:ProtectObjects>\n";
                push @retvals, "     <x:ProtectScenarios>False</x:ProtectScenarios>\n";
                push @retvals, "    </x:WorksheetOptions>\n";
                push @retvals, "   </x:ExcelWorksheet>\n";
                push @retvals, "  </x:ExcelWorksheets>\n";
                push @retvals, " </x:ExcelWorkbook>\n";
                push @retvals, "</xml>\n";
                push @retvals, "<style>\n";
                push @retvals, "table { font-size:8pt; }\n";
                push @retvals, "th, td { border:.5pt solid black; }\n";
                push @retvals, "</style>\n";
                push @retvals, "</head>\n";
                push @retvals, "<body>\n";
                push @retvals, "<table>\n";
                push @retvals, "<col width=85 span=$colspan style='mso-width-source:userset;mso-width-alt:3108; width:64pt'>";
                push @retvals, "<tr><th style='background-color:$ticolor_pri;color:white;text-align:left;' colspan=$colspan>Primary Cable Paths</th></tr>\n";
                push @retvals, "<tr><th style='background-color:$cecolor_pri;'>CBR</th>";
                foreach my $p (@sortedpop) {
                        push @retvals, "<th style='background-color:$cecolor_pri;'>$p</th>";
                }
                push @retvals, "</tr>\n";
                foreach my $ps (@sortedpop) {
                        push @retvals, "<tr style='vertical-align:middle;'><th style='background-color:$cecolor_pri;'>$ps</th>";
                        foreach my $pd (@sortedpop) {
                                if (($ps ne $pd) && ($cablepath_pri{$ps."->".$pd}) && ($cablepath_pri{$ps."->".$pd} !~ /^N(\/|.)?A(.)?$/i)) {
                                        push @retvals, "<td>".$cablepath_pri{$ps."->".$pd}."</td>";
                                } else {
                                        push @retvals, "<td style='background-color:$cecolor_pri;'></td>";
                                }
                        }
                        push @retvals, "</tr>\n";
                }
                push @retvals, "<tr><th style='background-color:$ticolor_alt;color:white;text-align:left;' colspan=$colspan>Alternative Cable Paths</th></tr>\n";
                push @retvals, "<tr><th style='background-color:$cecolor_alt;'>CBR</th>";
                foreach my $p (@sortedpop) {
                        push @retvals, "<th style='background-color:$cecolor_alt;'>$p</th>";
                }
                push @retvals, "</tr>\n";
                foreach my $ps (@sortedpop) {
                        push @retvals, "<tr style='vertical-align:middle;'><th style='background-color:$cecolor_alt;'>$ps</th>";
                        foreach my $pd (@sortedpop) {
                                if (($ps ne $pd) && ($cablepath_alt{$ps."->".$pd}) && ($cablepath_alt{$ps."->".$pd} !~ /^N(\/|.)?A(.)?$/i)) {
                                        push @retvals, "<td>".$cablepath_alt{$ps."->".$pd}."</td>";
                                } else {
                                        push @retvals, "<td  style='background-color:$cecolor_alt;'></td>";
                                }
                        }
                        push @retvals, "</tr>\n";
                }
                push @retvals, "<tr></tr>" x 2;
                push @retvals, "<tr><th style='background-color:$cdcolor;color:white;'>Cable</th><th style='background-color:$cdcolor;color:white;' colspan='3'>Description</th></tr>\n";
                foreach my $cd (@sortedcd) {
                        push @retvals, "<tr><td>$cd</td><td colspan='3'>$cabledesc{$cd}</td></tr>\n";
                }
                push @retvals, "</table>\n";
                push @retvals, "</body>\n";
                push @retvals, "</json>\n";
        }
        return @retvals;
}


# MPLS Reports
# Generate MPLS reports
# Pre: $start, $end, $disp, $data, $opshandle, $code, $svc, $remote
# Post: values
sub gen_trafficrptmpls {
	open DEBUG1 ,">/data1/tmp_log/prad_mvpn";
	#++mVPN
	my $mVPNflag = 0;	
        my($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $egress, $id) = @_;
	#print DEBUG1 "DD1 ($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, mVPNflag--->$mVPNflag) \n";

	# COS = Class0 = default/standard/Bronze; 1 = Standard Data/Bronze Plus; 2 = Interactive Data/Bronze Enhanced
	# 3 = (old silver) Critical Data/Silver; 4 = Video/Silver+; 5 = (old gold) Voice/Gold

        my (@vals, @defaultout, @default, @silverout, @silverin, @goldout, @goldin, @retvals, @no_class);
        my (@class4in, @class4out, @class2in, $class2out, , @class1in, @class1out);

	my @files = ("$opshandle");
	my $filename_head = "x-qos-";
	my $filename_class = "class:";
	my $filename_rateacl = "rateacl:";
	my $filename_tail = "-".$opshandle;

	CMS_DB::connect_to_database;

	# first get pollingtype
	# 25/10/2006
   	my $pt = CMS_SERVICE_DB::get_pollingtype_sid($opshandle);
	my $pgt = $$pt[0];
	my $pollingtype = $$pgt[0];

	#print "<P> Polling type = ($pollingtype) ($pt) ($opshandle)\n";

	# Next get cos labels - Telstra or PCCW
	my $cls = CMS_SERVICE_DB::get_costype_sid($opshandle);
	my $cyp = $$cls[0];
	my $costype = $$cyp[0];

	# 25/10/2006
	# if pollingtype == RATEACL, then use old way of in/out; if pollingtype = CLASSMAP, then use new in/out

	# Now get all COS types
	# need to get class 0 (default), class 1, class 2, class 3 (silver), class 4 and class 5 (gold)

	my $retclass = CMS_SERVICE_DB::get_cos_all($opshandle,$mVPNflag);
	# $retclass{classmap}{classname} = value / $ret{rateacl}{classname} = value

	# need index
	my $cosix = 1;
	my %cosval;
	foreach my $rckey (keys %$retclass) {

	#print DEBUG1 "<P> COS VAL ($rckey) ($$retclass{$rckey})\n";

		my $coskey = $$retclass{$rckey};
		# classmap or rateacle
		foreach my $rc2key (keys %$coskey) {
			#print DEBUG1 "<P> COS VAL 2 ($rc2key) \n";
			# class 0, class 1 etc
			$cosval{$cosix} = "$rckey^^$rc2key";
			#print DEBUG1 "<P> COSVAL ($cosix) ($rckey) ($rc2key) fname ($$coskey{$rc2key})\n";
			$cosix++;
##			if($rckey =~ /classmap/i) {
##				push @files, $filename_head.$filename_class.lc($$coskey{$rc2key}).$filename_tail;
##			} else {
##				push @files, $filename_head.$filename_rateacl.lc($$coskey{$rc2key}).$filename_tail;
##			}
			if($rckey =~ /classmap/i) {
				push @files, $filename_head.$filename_class.lc($$coskey{$rc2key}).$filename_tail;
			} else {
				if($pollingtype =~ /classmap/i) {
					push @files, $filename_head.$filename_class.lc($$coskey{$rc2key}).$filename_tail;
				}
				else{
					push @files, $filename_head.$filename_rateacl.lc($$coskey{$rc2key}).$filename_tail;
				}
			}
		}
	}
		

#        my $default_class = CMS_SERVICE_DB::get_default($opshandle);
#        foreach my $default(@$default_class) {
#                push @files, $filename_head.$filename_class.lc($$default[0]).$filename_tail;
#                push @files, $filename_head.$filename_rateacl.lc($$default[1]).$filename_tail;
##        }
#
#	my $silver_class = CMS_SERVICE_DB::get_silver($opshandle);
#	foreach my $silver(@$silver_class) {
#		push @files, $filename_head.$filename_class.lc($$silver[0]).$filename_tail;
#		push @files, $filename_head.$filename_rateacl.lc($$silver[1]).$filename_tail;
#	}
#	my $gold_class = CMS_SERVICE_DB::get_gold($opshandle);
#	foreach my $gold(@$gold_class) {
#		push @files, $filename_head.$filename_class.lc($$gold[0]).$filename_tail;
#		push @files, $filename_head.$filename_rateacl.lc($$gold[1]).$filename_tail;
#	}

	CMS_DB::disconnect_from_database;
	#print DEBUG1 " file=@files";
	#return @files;

        my $fail = 0;
        #for my $i (0..6) 
	open (TMP, ">/data1/tmp_log/anjan-test_uni");
	print TMP "fail---->$fail remote--->$remote\n";
	print TMP "mVPN Flag ----> $mVPN_flag\n";
        for my $i (0..$#files) {
	print TMP "Processing : $files[$i]\n";
	#print "<P>($i) ($files[$i]) ($remote)\n";
                my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                        PeerPort => 4546,
                        Proto => "tcp",
                        Type => SOCK_STREAM) or $fail = 1;

	#print TMP "fail---->$fail\n";
                if($fail) {
                        push @retvals, "FAIL";
                        push @retvals, "<p class=header>Error</p><p class=text>";
                        push @retvals, "Cannot retrieve traffic report for $opshandle.";
                        return @retvals;
                }

		# Only need tabular data from stats poller
                print $socket "$start $end table $data $files[$i] \n";
		#print TMP "sendin over socket $i $start $end table $data $files[$i] <br>\n";
                %vals = ();
                while(my $l = <$socket>) {
                        #if($disp =~ /gif|graph/) {
                        #        print $l;
                        #} else {
                                push @vals, $l;
                        #}
                }
                close($socket);
		#print TMP "vals --- >@vals\n";

##		# now use $cosval{index} to work out whether classmap/rateacle and what the class is
##		if ($cosval{$i} =~ /^classmap/) {
##			# classmap
##			# now work out what the class is
##			my $classtype = (split(/\^\^/, $cosval{$i}))[1];
### 			print "<P> XDEG 1-> ($files[$i]) ($classtype) (@vals)\n";
##			if($classtype =~ /class0/i) {
##				@defaultout = @vals;
##				if($pollingtype =~ /classmap/i) {
##					@default = @vals;
##				}
##			} elsif ($classtype =~ /class1/i) {
##				@class1out = @vals;
##				if($pollingtype =~ /classmap/i) {
##					@class1in = @vals;
##				}
##			} elsif ($classtype =~ /class2/i) {
##                                @class2out = @vals;
##                                if($pollingtype =~ /classmap/i) {
##                                        @class2in = @vals;
##                                }
##                        } elsif ($classtype =~ /class3/i) {
###print "<P> 1-> ($files[$i]) ($classtype) (@vals)\n";
##                                @silverout = @vals;
##                                if($pollingtype =~ /classmap/i) {
##                                        @silverin = @vals;
##                                }
##                        } elsif ($classtype =~ /class4/i) {
##                                @class4out = @vals;
##                                if($pollingtype =~ /classmap/i) {
##                                        @class4in = @vals;
##                                }
##                        } elsif ($classtype =~ /class5/i) {
##                                @goldout = @vals;
##                                if($pollingtype =~ /classmap/i) {
##                                        @goldin = @vals;
##                                }
##                        } 
##		} elsif ($cosval{$i} =~ /^rateacl/i) {
##			# rateacl
###print "<P> DD Rateacl = ($classtype) Polling type = ($pollingtype) Vals = (@vals)\n";
##
##			# now work out what the class is but only if polling type is rateacl
##			if($pollingtype =~ /rateacl/i) {
##				my $classtype = (split(/\^\^/, $cosval{$i}))[1];
###print "<P> 2-> ($files[$i]) ($classtype)\n";
##				if($classtype =~ /class0/i) {
##	       	                        @default = @vals;
##	                        } elsif ($classtype =~ /class1/i) {
##       	                         	@class1in = @vals;
##                        	} elsif ($classtype =~ /class2/i) {
##                                	@class2in = @vals;
##                        	} elsif ($classtype =~ /class3/i) {
###print "<P> 2-> ($files[$i]) ($classtype) (@vals)\n";
##                                	@silverin = @vals;
##                        	} elsif ($classtype =~ /class4/i) {
##                                	@class4in = @vals;
##                        	} elsif ($classtype =~ /class5/i) {
##                                	@goldin = @vals;
##                        	} 
###print "<P> DDDDD (@default) (@class1in) (@class2in) (@silverin) (@class4in) (@goldin)\n";
##			}
##		} else {
##			@no_class = @vals;
##			$defaultout[0] = 'FAIL';
##			$default[0] = 'FAIL';
##			$class1out[0] = 'FAIL';
##			$class1in[0] = 'FAIL';
##			$class2out[0] = 'FAIL';
##			$class2in[0] = 'FAIL';
##			$silverout[0] = 'FAIL';
##			$silverin[0] = 'FAIL';
##			$class4out[0] = 'FAIL';
##			$class4in[0] = 'FAIL';
##			$goldout[0] = 'FAIL';
##			$goldin[0] = 'FAIL';
##		}
		open (TMP1, ">/data1/tmp_log/anjan-test1");
		#print TMP1 "cosval = $cosval{$i}\n";
		print TMP "cosval = $cosval{$i}\n";

		# now use $cosval{index} to work out whether classmap/rateacl and what the class is
		if ($cosval{$i} =~ /^classmap/) {
			# classmap OUT
			# now work out what the class is
			my $classtype = (split(/\^\^/, $cosval{$i}))[1];
		#print TMP1 "<P> XDEG 1-> ($files[$i]) ($classtype) (@vals)\n";
			if($classtype =~ /class0/i) {
				@defaultout = @vals;
			} elsif ($classtype =~ /class1/i) {
				@class1out = @vals;
			} elsif ($classtype =~ /class2/i) {
                                @class2out = @vals;
                        } elsif ($classtype =~ /class3/i) {
#print "<P> 1-> ($files[$i]) ($classtype) (@vals)\n";
                                @silverout = @vals;
                        } elsif ($classtype =~ /class4/i) {
                                @class4out = @vals;
                        } elsif ($classtype =~ /class5/i) {
                                @goldout = @vals;
                        } 
		} elsif ($cosval{$i} =~ /^rateacl/i) {
			# rateacl / classmap IN
			#print TMP1 "<P> DD Rateacl = ($classtype) Polling type = ($pollingtype) Vals = (@vals)\n";
close (TMP1);
			# now work out what the class is
			my $classtype = (split(/\^\^/, $cosval{$i}))[1];
#print "<P> 2-> ($files[$i]) ($classtype)\n";
			if($classtype =~ /class0/i) {
       	                        @default = @vals;
                        } elsif ($classtype =~ /class1/i) {
	                   	@class1in = @vals;
                       	} elsif ($classtype =~ /class2/i) {
                               	@class2in = @vals;
                       	} elsif ($classtype =~ /class3/i) {
#print "<P> 2-> ($files[$i]) ($classtype) (@vals)\n";
                               	@silverin = @vals;
                       	} elsif ($classtype =~ /class4/i) {
                               	@class4in = @vals;
                       	} elsif ($classtype =~ /class5/i) {
                               	@goldin = @vals;
                       	} 
#print "<P> DDDDD (@default) (@class1in) (@class2in) (@silverin) (@class4in) (@goldin)\n";
		} else {
			@no_class = @vals;
			$defaultout[0] = 'FAIL';
			$default[0] = 'FAIL';
			$class1out[0] = 'FAIL';
			$class1in[0] = 'FAIL';
			$class2out[0] = 'FAIL';
			$class2in[0] = 'FAIL';
			$silverout[0] = 'FAIL';
			$silverin[0] = 'FAIL';
			$class4out[0] = 'FAIL';
			$class4in[0] = 'FAIL';
			$goldout[0] = 'FAIL';
			$goldin[0] = 'FAIL';
		}

#                if($i == 1) {
#                       @defaultout = @vals;
#print "default=@default<br><br>";
#                } elsif ($i == 2) {
#                       @default = @vals;
#                } elsif ($i == 3) {
#                       @silverout = @vals;
#print "silverout=@silverout<br><br>";
#		} elsif ($i == 4) {
#			@silverin = @vals;
#print "silverin=@silverin<br><Br>";
#		} elsif ($i == 5) {
#			@goldout = @vals;
#print "goldout=@goldout<br><br>";
#                } elsif ($i == 6) {
#                        @goldin = @vals;
#print "goldin=@goldin<br><br>";
#                } else {
#			@no_class = @vals;
#print "no_class=@no_class<br><br>";
#		}
                @vals = ();
		#print "<P>Default (@default) <P>Silver (@silver) <P>Gold (@gold)\n";
        }

	# Re-group
        my (%timestamp, %tmp_default, %tmp_defaultout, %tmp_class1in, %tmp_class1out, %tmp_class2in, %tmp_class2out, %tmp_silverin, %tmp_silverout, %tmp_class4in, %tmp_class4out, %tmp_goldin, %tmp_goldout);
        my (@new_default, @new_defaultout, @new_class1in, @new_class1out, @new_class2in, @new_class2out, @new_silverin, @new_silverout, @new_class4in, @new_class4out, @new_goldin, @new_goldout);

        undef(%timestamp);
        if ($default[0] !~ /FAIL/) {
                foreach my $val (@default) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_default{$seg[0]} = $val;
                }
        }
        if ($defaultout[0] !~ /FAIL/) {
                foreach my $val (@defaultout) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_defaultout{$seg[0]} = $val;
                }
        }

        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
                if (!$ts)  { next; }
                push @new_default, $tmp_default{$ts};
                push @new_defaultout, $tmp_defaultout{$ts};
        }

        if ($default[0] !~ /FAIL/) {
                @default = @new_default;
        }
        if ($defaultout[0] !~ /FAIL/) {
                @defaultout = @new_defaultout;
        }

        undef(%timestamp);
        if ($class1in[0] !~ /FAIL/) {
                foreach my $val (@class1in) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_class1in{$seg[0]} = $val;
                }
        }
        if ($class1out[0] !~ /FAIL/) {
                foreach my $val (@class1out) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_class1out{$seg[0]} = $val;
                }
        }

        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
                if (!$ts)  { next; }
                push @new_class1in, $tmp_class1in{$ts};
                push @new_class1out, $tmp_class1out{$ts};
        }

        if ($class1in[0] !~ /FAIL/) {
                @class1in = @new_class1in;
        }
        if ($class1out[0] !~ /FAIL/) {
                @class1out = @new_class1out;
        }

        undef(%timestamp);
        if ($class2in[0] !~ /FAIL/) {
                foreach my $val (@class2in) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_class2in{$seg[0]} = $val;
                }
        }
        if ($class2out[0] !~ /FAIL/) {
                foreach my $val (@class2out) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_class2out{$seg[0]} = $val;
                }
        }

        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
                if (!$ts)  { next; }
                push @new_class2in, $tmp_class2in{$ts};
                push @new_class2out, $tmp_class2out{$ts};
        }

        if ($class2in[0] !~ /FAIL/) {
                @class2in = @new_class2in;
        }
        if ($class2out[0] !~ /FAIL/) {
                @class2out = @new_class2out;
        }

        undef(%timestamp);
        if ($silverin[0] !~ /FAIL/) {
                foreach my $val (@silverin) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_silverin{$seg[0]} = $val;
                }
        }
        if ($silverout[0] !~ /FAIL/) {
                foreach my $val (@silverout) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_silverout{$seg[0]} = $val;
                }
        }

        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
                if (!$ts)  { next; }
                push @new_silverin, $tmp_silverin{$ts};
                push @new_silverout, $tmp_silverout{$ts};
        }

        if ($silverin[0] !~ /FAIL/) {
                @silverin = @new_silverin;
        }
        if ($silverout[0] !~ /FAIL/) {
                @silverout = @new_silverout;
        }

        undef(%timestamp);
        if ($class4in[0] !~ /FAIL/) {
                foreach my $val (@class4in) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_class4in{$seg[0]} = $val;
                }
        }
        if ($class4out[0] !~ /FAIL/) {
                foreach my $val (@class4out) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_class4out{$seg[0]} = $val;
                }
        }

        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
                if (!$ts)  { next; }
                push @new_class4in, $tmp_class4in{$ts};
                push @new_class4out, $tmp_class4out{$ts};
        }

        if ($class4in[0] !~ /FAIL/) {
                @class4in = @new_class4in;
        }
        if ($class4out[0] !~ /FAIL/) {
                @class4out = @new_class4out;
        }

        undef(%timestamp);
        if ($goldin[0] !~ /FAIL/) {
                foreach my $val (@goldin) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_goldin{$seg[0]} = $val;
                }
        }
        if ($goldout[0] !~ /FAIL/) {
                foreach my $val (@goldout) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_goldout{$seg[0]} = $val;
                }
        }

        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
                if (!$ts)  { next; }
                push @new_goldin, $tmp_goldin{$ts};
                push @new_goldout, $tmp_goldout{$ts};
        }

        if ($goldin[0] !~ /FAIL/) {
                @goldin = @new_goldin;
        }
        if ($goldout[0] !~ /FAIL/) {
                @goldout = @new_goldout;
        }

	print TMP "<P> DEBUG <P> 0 -> (@default) <P> 0 Out -> (@defaultout) <P> 1 -> (@class1in) <P> 1 Out -> (@class1out) <P> 2 -> (@class2in) <P> 2 Out -> (@class2out)\n";

	## NOW MOVE THE NEW 6 COS to do something 
	if($disp =~ /table/i) {
		#if (@default eq "FAIL" && @defaultout eq "FAIL" && @silverout eq "FAIL" && @silverin eq "FAIL" && @goldout eq "FAIL" && @goldin eq "FAIL") 
		#if ($default[0] =~ /FAIL/ && $defaultout[0] =~ /FAIL/ && $silverout[0] =~ /FAIL/ && $silverin[0] =~ /FAIL/ && $goldout[0] =~ /FAIL/ && $goldin[0] =~ /FAIL/) 
		if ($default[0] =~ /FAIL/ && $defaultout[0] =~ /FAIL/ && $class1out[0] =~ /FAIL/ && $class1in[0] =~ /FAIL/ && $class2out[0] =~ /FAIL/ && $class2in[0] =~ /FAIL/ && $silverout[0] =~ /FAIL/ && $silverin[0] =~ /FAIL/ && $class4out[0] =~ /FAIL/ && $class4in[0] =~ /FAIL/ && $goldout[0] =~ /FAIL/ && $goldin[0] =~ /FAIL/) {
			@retvals = &dogen_trafficrptmpls($disp, $opshandle, $code, $svc, $data, 1, \@no_class, "", "", $costype);
		} else {
			@retvals = &dogen_trafficrptmpls($disp, $opshandle, $code, $svc, $data, 2, \@default,  \@defaultout, \@silverout, \@silverin, \@goldout, \@goldin, 
							\@class1out, \@class1in, \@class2out, \@class2in, \@class4out, \@class4in, "", "", $costype);
			print TMP "**** 1:@default, 2:@defaultout, 3:@silverout, 4:@silverin, 5:@goldout, 6:@goldin, 7:@class1out, 8: @class1in, 9:@class2out,10:@class2in,11: @class4out,12: @class4in \n";
		}
	} else {
###### Chandini
		#if (@default eq "FAIL" && @defaultout eq "FAIL" && @silverout eq "FAIL" && @silverin eq "FAIL" && @goldout eq "FAIL" && @goldin eq "FAIL") 
		#if ($default[0] =~ /FAIL/ && $defaultout[0] =~ /FAIL/ && $silverout[0] =~ /FAIL/ && $silverin[0] =~ /FAIL/ && $goldout[0] =~ /FAIL/ && $goldin[0] =~ /FAIL/) 
		if ($default[0] =~ /FAIL/ && $defaultout[0] =~ /FAIL/ && $class1out[0] =~ /FAIL/ && $class1in[0] =~ /FAIL/ && $class2out[0] =~ /FAIL/ && $class2in[0] =~ /FAIL/ && $silverout[0] =~ /FAIL/ && $silverin[0] =~ /FAIL/ && $class4out[0] =~ /FAIL/ && $class4in[0] =~ /FAIL/ && $goldout[0] =~ /FAIL/ && $goldin[0] =~ /FAIL/) {
			&dogen_trafficrptmpls($disp, $opshandle, $code, $svc, $data, 1, \@no_class, $egress, $id, $costype);
			print TMP "-----> $disp, $opshandle, $code, $svc, $data, 1, @no_class, $egress, $id, $costype\n";
		} else {
			&dogen_trafficrptmpls($disp, $opshandle, $code, $svc, $data, 2, \@default, \@defaultout, \@silverout, \@silverin, \@goldout, \@goldin, 
						\@class1out, \@class1in, \@class2out, \@class2in, \@class4out, \@class4in, $egress, $id, $costype);
			
		 }
		return;
	}

	return @retvals;

}

# Actually prepare the data and generate MPLS traffic reports
# Pre: $disp, $opshandle, $code, $svc, $data, $tyep, $default, $silver, $gold
# Post: @retvals or gif
sub dogen_trafficrptmpls {
	my ($disp, $opshandle, $code, $svc, $data, $default, $defaultout, $silverout, $silverin, $goldout, $goldin, $egress, $id, $minute, $rptgen, @retvals, $costype);
	open (LOG12_p,">/data1/tmp_log/log_pr_mvpn");
	my ($class1out, $class1in, $class2out, $class2in, $class4out, $class4in);
	my $mVPN_flag = 0;
#Ch_mVP
$graph_error = 0;
	#print LOG12_p "mVPN_flag---->$mVPN_flag\n";
	#++mVPN
	#if($mVPN_flag == 1){
	#	return &dogen_trafficrptmpls_mVPN(@_);
	#}
	#--mVPN
	my $type = @_[5];
	#print LOG12_p "type----->$type\n";

	if ($type == 1) {
		($disp, $opshandle, $code, $svc, $data, $type, $default, $egress, $id, $costype) = @_;
	} elsif ($type == 2) {
		($disp, $opshandle, $code, $svc, $data, $type, $default, $defaultout, $silverout, $silverin, $goldout, $goldin, 
			$class1out, $class1in, $class2out, $class2in, $class4out, $class4in, $egress, $id, $costype) = @_;	 
	}
	#print LOG12_p "($mVPN_flag---$disp----$opshandle---$code----$svc----$data----$type)\n";

	# Note:  default = class0, silver = class3, gold = class5

	#my $cls = CMS_SERVICE_DB::get_costype_sid($opshandle);
        #my $costype = $$cls[0];
                        
        my ($c0, $c1, $c2, $c3, $c4, $c5);

        # Now set the labels
        if($costype =~ /telstra/i) {
                $c0 = $telstra_cos_label{class0};
                $c1 = $telstra_cos_label{class1};
                $c2 = $telstra_cos_label{class2};
                $c3 = $telstra_cos_label{class3};
                $c4 = $telstra_cos_label{class4};
                $c5 = $telstra_cos_label{class5};
        } elsif($costype =~ /pccw/i) {
                $c0 = $pccw_cos_label{class0};
                $c1 = $pccw_cos_label{class1};
                $c2 = $pccw_cos_label{class2};
                $c3 = $pccw_cos_label{class3};
                $c4 = $pccw_cos_label{class4};
                $c5 = $pccw_cos_label{class5};
        } else {
                $c0 = "Default";
                $c1 = "Class1";
                $c2 = "Class2";
                $c3 = "Silver";
                $c4 = "Class4";
                $c5 = "Gold";
        }

#print "<P> Label ($telstra_cos_label{class0}) ($costype) ($c0) ($c1) ($c2) ($c3) ($c4) ($c5)\n";

	#print "<P>GTD ($data, $default, $silver, $gold)\n";

        my $td = "td class=text";
        my $th = "td class=th";

        push @retvals, "<p class=header>Traffic Report for $opshandle</p><p class=text>\n";
        push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=21&et=$encet\">";
        push @retvals, "Click here to generate another report.</a>\n<p class=text>";
        if($data !~ /daily/i) {
		$minute = 1;
                $data = "5minute";
        }
        push @retvals, "Report type: $data</p>\n";
        push @retvals, "<p><hr><p class=text>\n";
	#++mVPN
	@vals=();
	
	push @retvals, "<div id=\"tabmenu\">\n";
	push @retvals, "<a id=\"unilink\" class=\"curposl\" onclick=\"clickunicast()\">\n";
#	push @retvals,"<span id=\"unispan\"class=\"curposr\">Unicast</span></a>\n";
	#push @retvals,"<a id=\"multilink\" onclick=\"clickmulticast()\"><span id=\"multispan\">Multicast </span></a></div>\n";
#Chandini_mVPNlink(added style link for the cursor for both unicat and multicast)
        push @retvals,"<span id=\"unispan\"class=\"curposr\" style=\"cursor: hand\">Unicast</span></a>\n";
        push @retvals,"<a id=\"multilink\" onclick=\"clickmulticast()\"><span id=\"multispan\" style=\"cursor: hand\">Multicast </span></a></div>\n";

	push @retvals,"<div id=\"tabmenuborder\"></div><br>\n";
	
	#--mVPN

	my ($h, @rpt, $out, @values);
	#print "($disp, $opshandle, $code, $svc, $data, $type, $default, $defaultout, $silver, $gold)\n";

	my $datafile = "/tmp/mplsgraph.data.$$.$id.traffic";
	#print LOG12_p "datasfile ---->$datafile\n";
	#mVPN
        push @rpt, "<div id=\"unicast\"><table cellpadding=4 cellspacing=5 border=1>";

	if($minute) {
		$h = "<tr><$th>Date</td><$th>Class of Service</td><$th>kbps received from customer</td><$th>kbps sent to customer</td>\n";
	} else {
        	$h = "<tr><$th>Date (GMT)</td><$th>Class of Service</td><$th>Incoming MBytes<br>Customer to REACH</td><$th>Use of port (%)</td>";
		$h .= "<$th>Outgoing MBytes<br>REACH to Customer</td>";
	        $h .= "<$th>5 min peak<br>Max of In or Out</td><$th>1 hr peak<br>Max of In or Out</td><$th>24 hr peak<br>Max of In or Out</td></tr>\n";
	}

	push @rpt, $h;

        my $rpt = &mpls_reconcile($data, $type, $default, $defaultout, $silverout, $silverin, $goldout, $goldin, $disp, $egress, 
					$class1out, $class1in, $class2out, $class2in, $class4out, $class4in);
	#print "<P>YY at begin ($rpt)\n";

	# daily = need col 0; 5min need col 1
	my $numcol = ($minute) ? 1 : 0;

print LOG12_p "numcol\n";
        foreach my $k (sort keys %$rpt) {
print LOG12_p "k--->$k\n";
		if (!$k)  { next; }
		$rptgen = 1;
                #my $date = localtime($k);
		my $date;
		if($k =~ /^\d+/) {
			$date = localtime($k);
		}
		next if($k =~ /fail/i);

                my @vals = split(/\|/, $$rpt{$k});
#print "<P> Rpt 0 ($vals[0]) ($#vals)\n";
#print "<P> Rpt 1 ($vals[1]) ($#vals)\n";
#print "<P> Rpt 2 ($vals[2]) ($#vals)\n";
#print "<P> Rpt 3 ($vals[3]) ($#vals)\n";
#print "<P> Rpt 4 ($vals[4]) ($#vals)\n";
#print LOG12_p "Rpt 5 ($vals[5]) ($#vals)\n";
		# print "<P>VV ($$rpt{$k}) ($#vals)(@vals)\n";
                #my $row = "<tr><$td>$date GMT</td><$td>Default</td>";
                my $row = "<tr><$td>$date GMT</td><$td>$c0</td>";

		#my ($def, $gld, $slv);
		my ($def, $def_out, $gld, $gld_out, $slv, $slv_out);
		my ($cl2, $cl2_out, $cl1, $cl1_out, $cl4, $cl4_out);

                if($vals[0]) {
                        # only default
                        my @v = split(/\s+/, $vals[0]);
			if($minute) {
                                $def = ($v[0]) ? $v[0] : 0;
                                $def_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $def = ($v[0]) ? $v[0] : 0;
                                $def_out = ($v[2]) ? $v[2] : 0;
                        }

			#$def = ($v[$numcol]) ? $v[$numcol] : 0;
                        foreach my $x (@v) {
				if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
			$def = $def_out = 0;
			if($minute) {
				$row .= "<$td>-</td><$td>-</td>";
			} else {
	                        $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
			}
                }


####################################################################################################
# Comment when PCCW supports CLASS 1

	if($costype =~ /pccw/i) {
		#$row .= "</tr>\n<tr><$td></td><$td>$c1</td>";
                if($vals[1]) {
                        # class1
                        my @v = split(/\s+/, $vals[1]);
                        if($minute) {
                                $cl1 = ($v[0]) ? $v[0] : 0;
                                $cl1_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $cl1 = ($v[0]) ? $v[0] : 0;
                                $cl1_out = ($v[2]) ? $v[2] : 0;
                        }

                        #$slv = ($v[$numcol]) ? $v[$numcol] : 0;
                        #foreach my $x (@v) {
			#	if($x !~ /^\d+/) { $x = "-"; }
                        #        $row .= "<$td>$x</td>";
                        #}
                } else {
                        $cl1 = $cl1_out = 0;
                        #if($minute) {
                        #        $row .= "<$td>-</td><$td>-</td>";
                        #} else {
                        #        $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        #}
                }
	} else {

####################################################################################################

		$row .= "</tr>\n<tr><$td></td><$td>$c1</td>";
                if($vals[1]) {
                        # class1
                        my @v = split(/\s+/, $vals[1]);
                        if($minute) {
                                $cl1 = ($v[0]) ? $v[0] : 0;
                                $cl1_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $cl1 = ($v[0]) ? $v[0] : 0;
                                $cl1_out = ($v[2]) ? $v[2] : 0;
                        }

                        #$slv = ($v[$numcol]) ? $v[$numcol] : 0;
                        foreach my $x (@v) {
				if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
                        $cl1 = $cl1_out = 0;
                        if($minute) {
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        }
                }

####################################################################################################
# Comment when PCCW supports CLASS 1

	}

####################################################################################################


####################################################################################################
# Comment when PCCW supports CLASS 2

        if($costype =~ /pccw/i) {
		#$row .= "</tr>\n<tr><$td></td><$td>$c2</td>";
                if($vals[2]) {
                        # class2
                        my @v = split(/\s+/, $vals[2]);
                        if($minute) {
                                $cl2 = ($v[0]) ? $v[0] : 0;
                                $cl2_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $cl2 = ($v[0]) ? $v[0] : 0;
                                $cl2_out = ($v[2]) ? $v[2] : 0;
                        }
         
                        #$slv = ($v[$numcol]) ? $v[$numcol] : 0;
                        #foreach my $x (@v) {
			#	if($x !~ /^\d+/) { $x = "-"; }
                        #        $row .= "<$td>$x</td>";
                        #}
                } else {
                        $cl2 = $cl2_out = 0;
                        #if($minute) {
                        #        $row .= "<$td>-</td><$td>-</td>";
                        #} else {
                        #        $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        #}
                }
        } else {

####################################################################################################

		$row .= "</tr>\n<tr><$td></td><$td>$c2</td>";
                if($vals[2]) {
                        # class2
                        my @v = split(/\s+/, $vals[2]);
                        if($minute) {
                                $cl2 = ($v[0]) ? $v[0] : 0;
                                $cl2_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $cl2 = ($v[0]) ? $v[0] : 0;
                                $cl2_out = ($v[2]) ? $v[2] : 0;
                        }
         
                        #$slv = ($v[$numcol]) ? $v[$numcol] : 0;
                        foreach my $x (@v) {
				if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
                        $cl2 = $cl2_out = 0;
                        if($minute) {
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        }
                }

####################################################################################################
# Comment when PCCW supports CLASS 2

	}

####################################################################################################


                $row .= "</tr>\n<tr><$td></td><$td>$c3</td>";
                if($vals[3]) {
                        # class 3 = old silver
                        my @v = split(/\s+/, $vals[3]);
			if($minute) {
                                $slv = ($v[0]) ? $v[0] : 0;
                                $slv_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $slv = ($v[0]) ? $v[0] : 0;
                                $slv_out = ($v[2]) ? $v[2] : 0;
                        }

			#$slv = ($v[$numcol]) ? $v[$numcol] : 0;
                        foreach my $x (@v) {
				if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
			$slv = $slv_out = 0;
                        if($minute) {
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>"; 
                        }
                }

		$row .= "</tr>\n<tr><$td></td><$td>$c4</td>";
                if($vals[4]) {
                        # class4
                        my @v = split(/\s+/, $vals[4]);
                        if($minute) {
                                $cl4 = ($v[0]) ? $v[0] : 0;
                                $cl4_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $cl4 = ($v[0]) ? $v[0] : 0;
                                $cl4_out = ($v[2]) ? $v[2] : 0;
                        }
         
                        #$slv = ($v[$numcol]) ? $v[$numcol] : 0;
                        foreach my $x (@v) {
				if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
                        $cl4 = $cl4_out = 0;
                        if($minute) {
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        }
                }


                #$row .= "</tr>\n<tr><$td></td><$td>Gold</td>";
                $row .= "</tr>\n<tr><$td></td><$td>$c5</td>";
		#print LOG12_p "vals5:$vals[5] before loop\n";
                if($vals[5]) {                   
			# Gold
                        my @v = split(/\s+/, $vals[5]);
			#$gld = ($v[$numcol]) ? $v[$numcol] : 0;
			if($minute) {
                                $gld = ($v[0]) ? $v[0] : 0;
                                $gld_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $gld = ($v[0]) ? $v[0] : 0;
                                $gld_out = ($v[2]) ? $v[2] : 0;
                        }
			#print LOG12_p "gld_out: $gld_out\n";

                        foreach my $x (@v) {
				if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
			$gld = $gld_out = 0;
                        if($minute) {
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>"; 
                        }
                }
		push @rpt, $row;

#		push @values, "$k\t$def\t$def_out\t$slv\t$slv_out\t$gld\t$gld_out";
		print LOG12_p " before pushing to values:gld_out: $gld_out\n";
		push @values, "$k\t$def\t$def_out\t$cl1\t$cl1_out\t$cl2\t$cl2_out\t$slv\t$slv_out\t$cl4\t$cl4_out\t$gld\t$gld_out";

		#  -> now if not table - give need to split out these values

		#print "<P>YY ($k $def $slv $gld)\n";
        }
        #mVPN	
        push @rpt, "</table></div>\n";

	if($rptgen) {
		push @retvals, @rpt;
	} else {
		#mVPN
                push @retvals, "<div id=\"unicast\"><p class=$th>No report generated.</p>\n";
                push @retvals, "<p class=text>There was no data available. Please try other dates.</p></div>\n";
	}

	if($disp !~ /table/i) {
		open (LOG_GR,">/data1/tmp_log/log_gr_prad");
		#print LOG_GR "values ---> @values\n";
		#graph
                my $max = 0;
                my $prev_timestamp =0;
                my $curr_timestamp=0;
                my $time_diff = 0;
                my @temp=();
##daily
        my $max_daily =0;
        my $prev_timestamp_daily =0;
        my $curr_timestamp_daily =0;
        my $time_diff_daily =0;
        my @temp_daily=();
	my $l_temp;
##daily


		open($out, ">$datafile");
####################################################################################################
# Comment when PCCW supports CLASS 1 & 2

#Ch_mVPN
	open (see,">/data1/tmp_log/Ch_seem");
	#print see "values:@values \n";
        if (@values == " "){
		$graph_error = 1;
	#print see "graph_error = $graph_error \n";
        }

        if($costype =~ /pccw/i) {
                foreach my $l (@values) {
                        my @v = split(/\s+/, $l);
                        for my $id (1..12) {
                                if(!$minute) {
                                        $v[$id] = int((($v[$id] * 80) / 864) + 0.5);
                                }

                                if($v[$id] > $max) {
                                        $max = $v[$id];
                                }
                        }
                        if(!$minute) {
                                $l = join "\t", @v;
                        }

			my @v1 = split(/\s+/, $l);
			$l = "";
			for my $i (0..$#v1) {
				if (($i == 3) || ($i == 4) || ($i == 5) || ($i == 6))  { next; }
				if ($i > 0) {
					$l .= "\t";
				}
				$l .= $v1[$i];
			}
                        print $out "$l\n";
                }
        } else {

####################################################################################################
		foreach my $l (@values) {
			my @v = split(/\s+/, $l);
			#print LOG_GR "current line --- $l\n";
       if($minute){
                $curr_timestamp = $v[0];
                $time_diff = $curr_timestamp - $prev_timestamp;
		#print LOG_GR "curr_timestamp---->$curr_timestamp     prev_timestamp--->$prev_timestamp    time_diff--->$time_diff\n";

                if($prev_timestamp !=0 && $time_diff > 300){
		#print LOG_GR "there is a time diff so goin in \n";
                        for(my $lp=1;$lp <$time_diff/300;$lp++){
                           $temp[0]=$prev_timestamp+300;
                           for my $id (1..12) {
                           $temp[$id]=0;
                           }
                           $l_temp = join "\t", @temp;
			#print LOG_GR " inserting for timestamp $l\n";
			#print LOG_GR "l_temp: $l_temp\n";
                           print $out "$l_temp\n";
                           $prev_timestamp=$temp[0];
                        }
	 	#print LOG_GR "inserted zerio rows for the diff \n";	
                }
                $prev_timestamp = $curr_timestamp;
                }
	#daily
        else{
                $curr_timestamp_daily = $v[0];
                $time_diff_daily = $curr_timestamp_daily - $prev_timestamp_daily;
                if($prev_timestamp_daily !=0 && $time_diff_daily > 86400){
                        for(my $lp=1;$lp <$time_diff_daily/86400;$lp++){
                           $temp_daily[0]=$prev_timestamp_daily+86400;
                           for my $id (1..12) {
                           $temp_daily[$id]=0;
                           }
                           
                           $l_temp = join "\t", @temp_daily;
                           print $out "$l_temp\n";
                           $prev_timestamp_daily=$temp_daily[0];
                        }

                }
                $prev_timestamp_daily = $curr_timestamp_daily;
        }

                        for my $id (1..12) {
				if(!$minute) {
                                        $v[$id] = int((($v[$id] * 80) / 864) + 0.5);
                                }       

                                if($v[$id] > $max) {
                                        $max = $v[$id];
                                }
                        }
			if(!$minute) {
                                $l = join "\t", @v;
                        }
			print LOG_GR "goin to print current line $l\n"; 
			print $out "$l\n";
		}

####################################################################################################
# Comment when PCCW supports CLASS 1 & 2

        }

####################################################################################################
		
		close($out);

		#$max += 50;
                #print "Content-tye: image/gif\n\n";

		if($egress) {
			&gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "mpls", $max, "IP VPN Egress", "",1, $costype);
		} else {
                        if ($data =~ /5minute/i) {
                                &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "mpls", $max, "", "", 1, $costype);
                        } else {
				#print see " befroe gen_atma error_graph : $graph_error \n";
                                &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "mpls", $max, "", "", "", $costype);
                        }
		}

		return;
	}

	#print "<P> (@retvals)\n";

        return @retvals;
}
#++mVPN
sub gen_trafficrptmpls_mVPN {
        open DEBUG1 ,">/data1/tmp_log/prad_rptmpls_mvpn";
        my $mVPN_flag = 1;
        my($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $egress, $id) = @_;
        #print DEBUG1 "DD1 ($start, $end, $disp, $data, $opshandle, $code, $svc, $remote) \n";


        my (@vals, @defaultout_mVPN, @default_mVPN, @silverout_mVPN, @silverin_mVPN, @goldout_mVPN, @goldin_mVPN, @retvals, @no_class_mVPN);
        my (@class4in_mVPN, @class4out_mVPN, @class2in_mVPN, $class2out_mVPN, , @class1in_mVPN, @class1out_mVPN);

        #my @files = ("$opshandle");
	my @files = ("");
        my $filename_head = "x-qos-";
        my $filename_class = "class:";
        my $filename_rateacl = "rateacl:";
        my $filename_tail = "-".$opshandle;

        CMS_DB::connect_to_database;

        my $pt = CMS_SERVICE_DB::get_pollingtype_sid($opshandle);
        my $pgt = $$pt[0];
        my $pollingtype = $$pgt[0];

        # Next get cos labels - Telstra or PCCW
        my $cls = CMS_SERVICE_DB::get_costype_sid($opshandle);
        my $cyp = $$cls[0];
        my $costype = $$cyp[0];

        # 25/10/2006
        # if pollingtype == RATEACL, then use old way of in/out; if pollingtype = CLASSMAP, then use new in/out

        # Now get all COS types
        # need to get class 0 (default_mVPN), class 1, class 2, class 3 (silver), class 4 and class 5 (gold)

        my $retclass = CMS_SERVICE_DB::get_cos_all($opshandle,$mVPN_flag);
	#print DEBUG1 "9:$$retclass[9]\t10:$$retclass[10]\t7:$$retclass[7]\t8:$$retclass[8]\n";

        # need index
        my $cosix = 1;
        my %cosval;
        foreach my $rckey (keys %$retclass) {
        	print DEBUG1 "<P> COS VAL ($rckey) ($$retclass{$rckey})\n";

                my $coskey = $$retclass{$rckey};
		print DEBUG1 "coskey:$coskey[9]\n";
                # classmap or rateacle
                foreach my $rc2key (keys %$coskey) {
                        print DEBUG1 "<P> COS VAL 2 ($rc2key) \n";
                        # class 0, class 1 etc
                        $cosval{$cosix} = "$rckey^^$rc2key";
                        $cosix++;
                        if($rckey =~ /classmap/i) {
                                push @files, $filename_head.$filename_class.lc($$coskey{$rc2key}).$filename_tail;
                        } else {
                                if($pollingtype =~ /classmap/i) {
                                        push @files, $filename_head.$filename_class.lc($$coskey{$rc2key}).$filename_tail;
                                }
                                else{
                                        push @files, $filename_head.$filename_rateacl.lc($$coskey{$rc2key}).$filename_tail;
                                }
                        }
                }
        }


        CMS_DB::disconnect_from_database;
        print DEBUG1 " file=@files";

        my $fail = 0;
        #for my $i (0..6)
	open (TMP, ">/data1/tmp_log/anjan-test_mVPN");
	#print TMP "mVPN Flag ----> $mVPN_flag\n";
        for my $i (0..$#files) {
                my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                        PeerPort => 4546,
                        Proto => "tcp",
                        Type => SOCK_STREAM) or $fail = 1;

                if($fail) {
			print TMP "socket failed for $files[$i]\n";
                        push @retvals, "FAIL";
                        push @retvals, "<p class=header>Error</p><p class=text>";
                        push @retvals, "Cannot retrieve traffic report for $opshandle.";
                        return @retvals;
                }

                # Only need tabular data from stats poller
                print $socket "$start $end table $data $files[$i] \n";
                print TMP "sendin over socket $i $start $end table $data $files[$i] <br>\n";
                %vals = ();
                while(my $l = <$socket>) {
		#print TMP "from socket ----> $l\n";
                        #if($disp =~ /gif|graph/) {
                        #        print $l;
                        #} else {
                                push @vals, $l;
                        #}

                }
                close($socket);

		#print TMP "vals --- >@vals\n";

		open (TMP1, ">/data1/tmp_log/anjan-test1");
		#print TMP "cosval = $cosval{$i}\n";

                # now use $cosval{index} to work out whether classmap/rateacl and what the class is
		#print TMP "vals before if else loop:\n @vals";
                if ($cosval{$i} =~ /^classmap/) {
			#print TMP "inside classmap\n";
                        # classmap OUT
                        # now work out what the class is
                        my $classtype = (split(/\^\^/, $cosval{$i}))[1];
			#print TMP "classtype---->$classtype\n";
			print TMP "<P> XDEG 1-> ($files[$i]) ($classtype) (@vals)\n";
                        if($classtype =~ /class0/i) {
                                @defaultout_mVPN = @vals;
                        } elsif ($classtype =~ /class1/i) {
                                @class1out_mVPN = @vals;
                        } elsif ($classtype =~ /class2/i) {
                                @class2out_mVPN = @vals;
                        } elsif ($classtype =~ /class3/i) {
#print "<P> 1-> ($files[$i]) ($classtype) (@vals)\n";
                                @silverout_mVPN = @vals;
				print TMP "OUT ($files[$i]) ($classtype) (@vals)\nOUT cosval i:$cosval{$i}\n";
                        } elsif ($classtype =~ /class4/i) {
                                @class4out_mVPN = @vals;
				print TMP "OUT ($files[$i]) ($classtype) (@vals)\nOUT cosval i:$cosval{$i}\n";
                        } elsif ($classtype =~ /class5/i) {
                                @goldout_mVPN = @vals;
				print TMP "OUT ($files[$i]) ($classtype) (@vals)\nOUT cosval i:$cosval{$i}\n";
                        }
                } elsif ($cosval{$i} =~ /^rateacl/i) {
			#print TMP "inside rateacl\n";
                        # rateacl / classmap IN
			print TMP "<P> DD Rateacl = ($classtype) Polling type = ($pollingtype) Vals = (@vals)\n";
                        # now work out what the class is
                        my $classtype = (split(/\^\^/, $cosval{$i}))[1];
			#print TMP "classtype---->$classtype\n";
                        if($classtype =~ /class0/i) {
                                @default_mVPN = @vals;
                        } elsif ($classtype =~ /class1/i) {
                                @class1in_mVPN = @vals;
                        } elsif ($classtype =~ /class2/i) {
                                @class2in_mVPN = @vals;
                        } elsif ($classtype =~ /class3/i) {
                                @silverin_mVPN = @vals;
                        } elsif ($classtype =~ /class4/i) {
                                @class4in_mVPN = @vals;
                        } elsif ($classtype =~ /class5/i) {
                                @goldin_mVPN = @vals;
				print TMP "IN ($files[$i]) ($classtype) (@vals)\nIN cosval i:$cosval{$i}\n";
                        }
                } else {
                        @no_class_mVPN = @vals;
                        $defaultout_mVPN[0] = 'FAIL';
                        $default_mVPN[0] = 'FAIL';
                        $class1out_mVPN[0] = 'FAIL';
                        $class1in_mVPN[0] = 'FAIL';
                        $class2out_mVPN[0] = 'FAIL';
                        $class2in_mVPN[0] = 'FAIL';
                        $silverout_mVPN[0] = 'FAIL';
                        $silverin_mVPN[0] = 'FAIL';
                        $class4out_mVPN[0] = 'FAIL';
                        $class4in_mVPN[0] = 'FAIL';
                        $goldout_mVPN[0] = 'FAIL';
                        $goldin_mVPN[0] = 'FAIL';
                }
                @vals = ();
                #print "<P>default_mVPN (@default_mVPN) <P>Silver (@silver) <P>Gold (@gold)\n";
        }
	print TMP "0-@default_mVPN   ------  0-out -->@defaultout_mVPN  -------   1-@class1in_mVPN ------- 1-out---->@class1out_mVPN    -------- 2-@class2in_mVPN      ----- 2-out--->@class2out_mVPN    ----- 3-@silverin_mVPN   ------ 3-out     ----> @silverout_mVPN  ------ 4-@class4in_mVPN ----- 4-out -> @class4out_mVPN   ------- 5-@goldin_mVPN ------ 5-out-> @goldout_mVPN    ---- \n";

        # Re-group
        my (%timestamp, %tmp_default, %tmp_defaultout, %tmp_class1in, %tmp_class1out, %tmp_class2in, %tmp_class2out, %tmp_silverin, %tmp_silverout, %tmp_class4in, %tmp_class4out, %tmp_goldin, %tmp_goldout);
        my (@new_default, @new_defaultout, @new_class1in, @new_class1out, @new_class2in, @new_class2out, @new_silverin, @new_silverout, @new_class4in, @new_class4out, @new_goldin, @new_goldout);

        undef(%timestamp);
        if ($default_mVPN[0] !~ /FAIL/) {
                foreach my $val (@default_mVPN) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_default{$seg[0]} = $val;
                }
        }
        if ($defaultout_mVPN[0] !~ /FAIL/) {
                foreach my $val (@defaultout_mVPN) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_defaultout{$seg[0]} = $val;
                }
        }


        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
                if (!$ts)  { next; }
                push @new_default, $tmp_default{$ts};
                push @new_defaultout, $tmp_defaultout{$ts};
        }

        if ($default_mVPN[0] !~ /FAIL/) {
                @default_mVPN = @new_default;
        }
        if ($defaultout_mVPN[0] !~ /FAIL/) {
                @defaultout_mVPN = @new_defaultout;
        }

        undef(%timestamp);
        if ($class1in_mVPN[0] !~ /FAIL/) {
                foreach my $val (@class1in_mVPN) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_class1in{$seg[0]} = $val;
                }
        }
        if ($class1out_mVPN[0] !~ /FAIL/) {
                foreach my $val (@class1out_mVPN) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_class1out{$seg[0]} = $val;
                }
        }

        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
                if (!$ts)  { next; }
                push @new_class1in, $tmp_class1in{$ts};
                push @new_class1out, $tmp_class1out{$ts};
        }

        if ($class1in_mVPN[0] !~ /FAIL/) {
                @class1in_mVPN = @new_class1in;
        }
        if ($class1out_mVPN[0] !~ /FAIL/) {
                @class1out_mVPN = @new_class1out;
        }

        undef(%timestamp);
        if ($class2in_mVPN[0] !~ /FAIL/) {
                foreach my $val (@class2in_mVPN) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_class2in{$seg[0]} = $val;
                }
        }
        if ($class2out_mVPN[0] !~ /FAIL/) {
                foreach my $val (@class2out_mVPN) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_class2out{$seg[0]} = $val;
                }
        }


        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
                if (!$ts)  { next; }
                push @new_class2in, $tmp_class2in{$ts};
                push @new_class2out, $tmp_class2out{$ts};
        }

        if ($class2in_mVPN[0] !~ /FAIL/) {
                @class2in_mVPN = @new_class2in;
        }
        if ($class2out_mVPN[0] !~ /FAIL/) {
                @class2out_mVPN = @new_class2out;
        }

        undef(%timestamp);
        if ($silverin_mVPN[0] !~ /FAIL/) {
                foreach my $val (@silverin_mVPN) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_silverin{$seg[0]} = $val;
                }
        }
        if ($silverout_mVPN[0] !~ /FAIL/) {
                foreach my $val (@silverout_mVPN) {
                        my @seg = split /\s+/, $val;
                        $timestamp{$seg[0]} = "";
                        $tmp_silverout{$seg[0]} = $val;
                }
        }

        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
                if (!$ts)  { next; }
                push @new_silverin, $tmp_silverin{$ts};
                push @new_silverout, $tmp_silverout{$ts};
        }

        if ($silverin_mVPN[0] !~ /FAIL/) {
                @silverin_mVPN = @new_silverin;
        }
        if ($silverout_mVPN[0] !~ /FAIL/) {
                @silverout_mVPN = @new_silverout;
        }

        undef(%timestamp);
                if ($class4in_mVPN[0] !~ /FAIL/) {
	                foreach my $val (@class4in_mVPN) {
	                        my @seg = split /\s+/, $val;
	                        $timestamp{$seg[0]} = "";
	                        $tmp_class4in{$seg[0]} = $val;
	                }
	        }
	        if ($class4out_mVPN[0] !~ /FAIL/) {
	                foreach my $val (@class4out_mVPN) {
	                        my @seg = split /\s+/, $val;
	                        $timestamp{$seg[0]} = "";
	                        $tmp_class4out{$seg[0]} = $val;
	                }
	        }
	
	        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
	                if (!$ts)  { next; }
	                push @new_class4in, $tmp_class4in{$ts};
	                push @new_class4out, $tmp_class4out{$ts};
	        }
	
	        if ($class4in_mVPN[0] !~ /FAIL/) {
	                @class4in_mVPN = @new_class4in;
	        }
	        if ($class4out_mVPN[0] !~ /FAIL/) {
	                @class4out_mVPN = @new_class4out;
	        }
	
	        undef(%timestamp);
	        if ($goldin_mVPN[0] !~ /FAIL/) {
	                foreach my $val (@goldin_mVPN) {
	                        my @seg = split /\s+/, $val;
	                        $timestamp{$seg[0]} = "";
	                        $tmp_goldin{$seg[0]} = $val;
				print TMP "tmp_goldin - $tmp_goldin{$seg[0]}\n";
	                }
	        }
	        if ($goldout_mVPN[0] !~ /FAIL/) {
	                foreach my $val (@goldout_mVPN) {
	                        my @seg = split /\s+/, $val;
				print TMP "val - $val\n";
	                        $timestamp{$seg[0]} = "";
	                        $tmp_goldout{$seg[0]} = $val;
				print TMP "tmp_goldout - $tmp_goldout{$seg[0]}\n";
	                }
	        }
	
	        foreach my $ts (sort {$a <=> $b} keys%timestamp) {
	                if (!$ts)  { next; }
                push @new_goldin, $tmp_goldin{$ts};
                push @new_goldout, $tmp_goldout{$ts};
		#print TMP "new_goldin - @new_goldout\n";
        }

        if ($goldin_mVPN[0] !~ /FAIL/) {
                @goldin_mVPN = @new_goldin;
        }
        if ($goldout_mVPN[0] !~ /FAIL/) {
                @goldout_mVPN = @new_goldout;
        }

 	#print TMP "<P> DEBUG <P> 0 -> (@default_mVPN) <P> 0 Out -> (@defaultout_mVPN) <P> 1 -> (@class1in_mVPN) <P> 1 Out -> (@class1out_mVPN) <P> 2 -> (@class2in_mVPN) <P> 2
 #Out -> (@class2out_mVPN)\n";
	print TMP "DEBUG gold in:\n @goldin_mVPN\n";

        ## NOW MOVE THE NEW 6 COS to do something
        if($disp =~ /table/i) {
                if ($default_mVPN[0] =~ /FAIL/ && $defaultout_mVPN[0] =~ /FAIL/ && $class1out_mVPN[0] =~ /FAIL/ && $class1in_mVPN[0] =~ /FAIL/ && $class2out_mVPN[0] =~ /FAIL/ && $class2in_mVPN[0] =~ /FAIL/ && $silverout_mVPN[0] =~ /FAIL/ && $silverin_mVPN[0] =~ /FAIL/ && $class4out_mVPN[0] =~ /FAIL/ && $class4in_mVPN[0] =~ /FAIL/ && $goldout_mVPN[0] =~ /FAIL/ && $goldin_mVPN[0] =~ /FAIL/) {
			#print DEBUG1 "going inside fail condition\n";
                        @retvals = &dogen_trafficrptmpls_mVPN($disp, $opshandle, $code, $svc, $data, 1, \@no_class_mVPN, "", "", $costype);
                } else {
                        @retvals = &dogen_trafficrptmpls_mVPN($disp, $opshandle, $code, $svc, $data, 2, \@default_mVPN,  \@defaultout_mVPN, \@silverout_mVPN, \@silverin_mVPN, \@goldout_mVPN, \@goldin_mVPN,\@class1out_mVPN, \@class1in_mVPN, \@class2out_mVPN, \@class2in_mVPN, \@class4out_mVPN, \@class4in_mVPN, "", "", $costype);
                }
        } else {
                if ($default_mVPN[0] =~ /FAIL/ && $defaultout_mVPN[0] =~ /FAIL/ && $class1out_mVPN[0] =~ /FAIL/ && $class1in_mVPN[0] =~ /FAIL/ && $class2out_mVPN[0] =~ /FAIL/ && $class2in_mVPN[0] =~ /FAIL/ && $silverout_mVPN[0] =~ /FAIL/ && $silverin_mVPN[0] =~ /FAIL/ && $class4out_mVPN[0] =~ /FAIL/ && $class4in_mVPN[0] =~ /FAIL/ && $goldout_mVPN[0] =~ /FAIL/ && $goldin_mVPN[0] =~ /FAIL/) {
			#print DEBUG1 "going inside fail condition\n";
                        &dogen_trafficrptmpls_mVPN($disp, $opshandle, $code, $svc, $data, 1, \@no_class_mVPN, $egress, $id, $costype);
                } else {
                        &dogen_trafficrptmpls_mVPN($disp, $opshandle, $code, $svc, $data, 2, \@default_mVPN, \@defaultout_mVPN, \@silverout_mVPN, \@silverin_mVPN, \@goldout_mVPN, \@goldin_mVPN,\@class1out_mVPN, \@class1in_mVPN, \@class2out_mVPN, \@class2in_mVPN, \@class4out_mVPN, \@class4in_mVPN, $egress, $id, $costype);
                 }
                return;
        }

        return @retvals;

}


sub dogen_trafficrptmpls_mVPN {
        my ($disp, $opshandle, $code, $svc, $data, $default, $defaultout, $silverout, $silverin, $goldout, $goldin, $egress, $id, $minute, $rptgen_mVPN, @retvals, $costype);
        my ($class1out, $class1in, $class2out, $class2in, $class4out, $class4in);
	open (LOG12,">/data1/tmp_log/prad_mvpn_dpgen");
        my $type = @_[5];
	#print LOG12 "type--->$type\n";
	my $mVPN_flag=1;	
#Ch_mVP
$graph_error =0;
        if ($type == 1) {
                ($disp, $opshandle, $code, $svc, $data, $type, $default, $egress, $id, $costype) = @_;
        } elsif ($type == 2) {
                ($disp, $opshandle, $code, $svc, $data, $type, $default, $defaultout, $silverout, $silverin, $goldout, $goldin,$class1out, $class1in, $class2out, $class2in, $class4out, $class4in, $egress, $id, $costype) = @_;
        }
	
	print LOG12 "$mVPN_flag,$disp, $opshandle, $code, $svc, $data, $type, $default, $defaultout, $silverout, $silverin, $goldout, $goldin,$class1out, $class1in, $class2out, $class2in, $class4out, $class4in, $egress, $id, $costype\n";
        # Note:  default = class0, silver = class3, gold = class5


        my ($c0, $c1, $c2, $c3, $c4, $c5);

        # Now set the labels
        if($costype =~ /telstra/i) {
                $c0 = $telstra_cos_label_mVPN{class0};
                $c1 = $telstra_cos_label_mVPN{class1};
                $c2 = $telstra_cos_label_mVPN{class2};
                $c3 = $telstra_cos_label_mVPN{class3};
                $c4 = $telstra_cos_label_mVPN{class4};
                $c5 = $telstra_cos_label_mVPN{class5};
        } elsif($costype =~ /pccw/i) {
                $c0 = $pccw_cos_label{class0};
                $c1 = $pccw_cos_label{class1};
                $c2 = $pccw_cos_label{class2};
                $c3 = $pccw_cos_label{class3};
                $c4 = $pccw_cos_label{class4};
                $c5 = $pccw_cos_label{class5};
        } else {
                $c0 = "Default";
                $c1 = "Class1";
                $c2 = "Class2";

                $c3 = "Silver";
                $c4 = "Class4";
                $c5 = "Gold";
        }

        my $td = "td class=text";
        my $th = "td class=th";

        if($data !~ /daily/i) {
                $minute = 1;
                $data = "5minute";
        }
        @vals=();

        my ($h, @rpt, $out, @values);

        my $datafile = "/tmp/mplsgraph.data.$$.$id.traffic";


	print LOG12 "datafile----$datafile\n";
        push @rpt, "<div id=\"multicast\" style=\"display:none;\"><table cellpadding=4 cellspacing=5 border=1>";

        if($minute) {
                $h = "<tr><$th>Date</td><$th>Class of Service</td><$th>kbps received from customer</td><$th>kbps sent to customer</td>\n";
        } else {
                $h = "<tr><$th>Date (GMT)</td><$th>Class of Service</td><$th>Incoming MBytes<br>Customer to REACH</td><$th>Use of port (%)</td>";
                $h .= "<$th>Outgoing MBytes<br>REACH to Customer</td>";
                $h .= "<$th>5 min peak<br>Max of In or Out</td><$th>1 hr peak<br>Max of In or Out</td><$th>24 hr peak<br>Max of In or Out</td></tr>\n";
        }

        push @rpt, $h;

        my $rpt_mVPN = &mpls_reconcile_mVPN($data, $type, $default, $defaultout, $silverout, $silverin, $goldout, $goldin, $disp, $egress,
                                        $class1out, $class1in, $class2out, $class2in, $class4out, $class4in);

        # daily = need col 0; 5min need col 1
	print LOG12 "rpt_mVPN--->$rpt_mVPN\n";
        my $numcol = ($minute) ? 1 : 0;
	print LOG12 "numcol:$numcol\n";

        foreach my $k (sort keys %$rpt_mVPN) {
		print LOG12 "k--->$k\n";
                if (!$k )  { next; }
		if($k && $k !~ /FAIL/ ){ $rptgen_mVPN = 1;}
                #my $date = localtime($k);
                my $date;
                if($k =~ /^\d+/) {
                        $date = localtime($k);
                }
                next if($k =~ /fail/i);

		print LOG12 "before splitting rpt_mVPN: $$rpt_mVPN{$k}\n";
                my @vals = split(/\|/, $$rpt_mVPN{$k});
                my $row = "<tr><$td>$date GMT</td><$td>$c0</td>";

                my ($def, $def_out, $gld, $gld_out, $slv, $slv_out);
                my ($cl2, $cl2_out, $cl1, $cl1_out, $cl4, $cl4_out);

                if($vals[0]) {
                        # only default
                        my @v = split(/\s+/, $vals[0]);
                        if($minute) {
                                $def = ($v[0]) ? $v[0] : 0;
                                $def_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                               $def = ($v[0]) ? $v[0] : 0;
                                $def_out = ($v[2]) ? $v[2] : 0;
                        }
                        foreach my $x (@v) {
                                if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
                        $def = $def_out = 0;
                        if($minute) {
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        }
                }
		# Comment when PCCW supports CLASS 1
        if($costype =~ /pccw/i) {
                if($vals[1]) {
                        # class1
                        my @v = split(/\s+/, $vals[1]);
                        if($minute) {
                                $cl1 = ($v[0]) ? $v[0] : 0;
                                $cl1_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $cl1 = ($v[0]) ? $v[0] : 0;
                                $cl1_out = ($v[2]) ? $v[2] : 0;
                        }
                } else {
                        $cl1 = $cl1_out = 0;
                }
        } else {
                $row .= "</tr>\n<tr><$td></td><$td>$c1</td>";
                if($vals[1]) {
                        # class1
                        my @v = split(/\s+/, $vals[1]);
                        if($minute) {
                                $cl1 = ($v[0]) ? $v[0] : 0;
                                $cl1_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $cl1 = ($v[0]) ? $v[0] : 0;
                                $cl1_out = ($v[2]) ? $v[2] : 0;
                        }
                        foreach my $x (@v) {
                                if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
                        $cl1 = $cl1_out = 0;
                        if($minute) {
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        }
                }
		# Comment when PCCW supports CLASS 1
        }
		# Comment when PCCW supports CLASS 2
        if($costype =~ /pccw/i) {
                if($vals[2]) {
                        # class2
                        my @v = split(/\s+/, $vals[2]);
                        if($minute) {
                                $cl2 = ($v[0]) ? $v[0] : 0;
                                $cl2_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $cl2 = ($v[0]) ? $v[0] : 0;
                                $cl2_out = ($v[2]) ? $v[2] : 0;
                        }
                } else {
                        $cl2 = $cl2_out = 0;
                }
        } else {
                $row .= "</tr>\n<tr><$td></td><$td>$c2</td>";
                if($vals[2]) {
                        # class2
                        my @v = split(/\s+/, $vals[2]);
                        if($minute) {
                                $cl2 = ($v[0]) ? $v[0] : 0;
                                $cl2_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $cl2 = ($v[0]) ? $v[0] : 0;
                                $cl2_out = ($v[2]) ? $v[2] : 0;
                        }
                        foreach my $x (@v) {
                                if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
                        $cl2 = $cl2_out = 0;
                        if($minute) {
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        }
                }
# Comment when PCCW supports CLASS 2
        }
                $row .= "</tr>\n<tr><$td></td><$td>$c3</td>";
                if($vals[3]) {
                        # class 3 = old silver
                        my @v = split(/\s+/, $vals[3]);
                        if($minute) {
                                $slv = ($v[0]) ? $v[0] : 0;
                                $slv_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $slv = ($v[0]) ? $v[0] : 0;
                                $slv_out = ($v[2]) ? $v[2] : 0;
                        }
                        foreach my $x (@v) {
                                if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
                        $slv = $slv_out = 0;
                        if($minute) {
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        }

                }

                $row .= "</tr>\n<tr><$td></td><$td>$c4</td>";
                if($vals[4]) {
                        # class4
                        my @v = split(/\s+/, $vals[4]);
                        if($minute) {
                                $cl4 = ($v[0]) ? $v[0] : 0;
                                $cl4_out = ($v[1]) ? $v[1] : 0;
                        } else { #daily
                                $cl4 = ($v[0]) ? $v[0] : 0;
                                $cl4_out = ($v[2]) ? $v[2] : 0;
                        }

                        #$slv = ($v[$numcol]) ? $v[$numcol] : 0;
                        foreach my $x (@v) {
                                if($x !~ /^\d+/) { $x = "-"; }
                                $row .= "<$td>$x</td>";
                        }
                } else {
                        $cl4 = $cl4_out = 0;
                        if($minute) {
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        }
                }


                $row .= "</tr>\n<tr><$td></td><$td>$c5</td>";
		print LOG12 "vals[5]:$vals[5]\n";
                if($vals[5]) {
                        # Gold
                        my @v = split(/\s+/, $vals[5]);
                        if($minute) {
                                $gld = ($v[0]) ? $v[0] : 0;
                                $gld_out = ($v[1]) ? $v[1] : 0;
				#print LOG12 "minute gld_out:$gld_out\n";
                        } else { #daily
                                $gld = ($v[0]) ? $v[0] : 0;
                                $gld_out = ($v[2]) ? $v[2] : 0;
				#print LOG12 "daily gld_out:$gld_out\n";
				print LOG12 "daily gld_in:$gld\n";
                        }
                        foreach my $x (@v) {
                                if($x !~ /^\d+/) {
				print LOG12 "hyphen when not in digits\n";
				 $x = "-"; }
                                $row .= "<$td>$x</td>";
				#print LOG12 "row: $row\n";
                        }
                } else {
			#print LOG12 "else condition gld_out:$gld_out\n";
                        $gld = $gld_out = 0;
                        if($minute) {
				print LOG12 "printing hyphen\n";
                                $row .= "<$td>-</td><$td>-</td>";
                        } else {
				print LOG12 "daily printing hyphen\n";
                                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
                        }
                }
                push @rpt, $row;
		#print LOG12 "rpt:@rpt\n";
		#print LOG12 "before pushing to values gold out: $gld_out\n";
		print LOG12 "before pushing to values gold in: $gld\n";
                push @values, "$k\t$def\t$def_out\t$cl1\t$cl1_out\t$cl2\t$cl2_out\t$slv\t$slv_out\t$cl4\t$cl4_out\t$gld\t$gld_out";
        }
        push @rpt, "</table></div>\n";

        if($rptgen_mVPN) {
                push @retvals, @rpt;
        } else {
                push @retvals, "<div id=\"multicast\" style=\"display:none;\"><p class=$th>No report generated.</p>\n";
                push @retvals, "<p class=text>There was no data available. Please try other dates.</p></div>\n";
        } 
	#print LOG12 "retvals---->@retvals\n";

        if($disp !~ /table/i) {
                #graph

                my $max = 0;
                my $prev_timestamp =0;
                my $curr_timestamp=0;
                my $time_diff = 0;
                my @temp;
##daily
        my $max_daily =0;
        my $prev_timestamp_daily =0;
        my $curr_timestamp_daily =0;
        my $time_diff_daily =0;
        my @temp_daily;
	my $l_temp;


                open($out, ">$datafile");
# Comment when PCCW supports CLASS 1 & 2

#Ch_mVPN
	open (see,">/data1/tmp_log/Ch_see");
	#print see "values:@values \n";
	if (@values == " "){
        	$graph_error = 1;
	#print see "graph_error = $graph_error \n";
	}

        if($costype =~ /pccw/i) {
                foreach my $l (@values) {
		print LOG12 "values ----->@values\n";
                       my @v = split(/\s+/, $l);
			pop(@v);
                        for my $id (1..12) {
                                if(!$minute) {
                                        $v[$id] = int((($v[$id] * 80) / 864) + 0.5);
                                }

                                if($v[$id] > $max) {
                                        $max = $v[$id];
                                }
                        }
                        if(!$minute) {
                                $l = join "\t", @v;
                        }

                        my @v1 = split(/\s+/, $l);
                        $l = "";
                        for my $i (0..$#v1) {
                                if (($i == 3) || ($i == 4) || ($i == 5) || ($i == 6))  { next; }
                                if ($i > 0) {
                                        $l .= "\t";
                                }
				print LOG12 "skipped v1s:$v1[3]\t$v1[4]\t$v1[5]\t$v1[6]\n";
                                $l .= $v1[$i];
                        }
			print LOG12 "before sending to $datafile\n l:$l\n";
                        print $out "$l\n";
                }
        } else {
		print LOG12 "NOT PCCW\n";
                foreach my $l (@values) {
		 #print LOG12 "values ----->@values\n";
                        my @v = split(/\s+/, $l);
			#Karuna - changed for minor enh
			#pop(@v);
       if($minute){
                $curr_timestamp = $v[0];
                $time_diff = $curr_timestamp - $prev_timestamp;

                if($prev_timestamp !=0 && $time_diff > 300){
                        for(my $lp=1;$lp <$time_diff/300;$lp++){
                           $temp[0]=$prev_timestamp+300;
			   #Karuna - changed for minor enh
                           #for my $id (1..11) {
                           for my $id (1..12) {
                           $temp[$id]=0;
                           }
                           $l_temp = join "\t", @temp;
			   #print LOG12 "l_temp:$l_temp\n";
                           print $out "$l_temp\n";
                           $prev_timestamp=$temp[0];
                        }

                }
                $prev_timestamp = $curr_timestamp;
                }
	#daily
        else{
                $curr_timestamp_daily = $v[0];
                $time_diff_daily = $curr_timestamp_daily - $prev_timestamp_daily;
                if($prev_timestamp_daily !=0 && $time_diff_daily > 86400){
                        for(my $lp=1;$lp <$time_diff_daily/86400;$lp++){
                           $temp_daily[0]=$prev_timestamp_daily+86400;
			   #Karuna - changed for minor enh
                           #for my $id (1..11) {
                           for my $id (1..12) {
                           $temp_daily[$id]=0;
                           }
                           
                           $l_temp = join "\t", @temp_daily;
                           print $out "$l_temp\n";
                           $prev_timestamp_daily=$temp_daily[0];
                        }

                }
                $prev_timestamp_daily = $curr_timestamp_daily;
        }

		#print LOG12 "v---->@v\n";
			#Karuna - changed for minor enh
                        #for my $id (1..11) {
                        for my $id (1..12) {
                                if(!$minute) {
                                        $v[$id] = int((($v[$id] * 80) / 864) + 0.5);
                                }

                                if($v[$id] > $max) {
                                        $max = $v[$id];
                                }
                        }
                                $l = join "\t", @v;
                        print $out "$l\n";
                }
# Comment when PCCW supports CLASS 1 & 2
        }
                close($out);

		print LOG12 "b4 graph mVPN_flag--->$mVPN_flag\tdata:$data\n";
                if($egress) {
                        &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "mpls", $max, "IP VPN Egress", "",1, $costype,'','',$mVPN_flag);
                } else {
                        if ($data =~ /5minute/i) {
				print LOG12 "b4 graph values:\nstart:$start\tend:$end\tdisp:$disp\tdata:$data\tcostype:$costype\tmVPN_flag:$mVPN_flag\n";
                                &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "mpls", $max, "", "", 1, $costype,'','',$mVPN_flag);
                        } else {
#Ch_mVPN
				#print see " before gen_atm graph_error : $graph_error \n";
                                &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "mpls", $max, "", "", "", $costype,'','',$mVPN_flag);
                        }
                }

                return;
        }
	#print LOG12 "retvals at end ----> @retvals\n";
        return @retvals;
}
#--mVPN
         
# Reconcile gold, silver and default into one list
# Pre: $dtype, @default, @silver, @gold, display, $egress, additional cos...
# Post: %report
sub mpls_reconcile {
        my($dtype, $type, $default, $defaultout, $silverout, $silverin, $goldout, $goldin, $disp, $egress,
		$class1out, $class1in, $class2out, $class2in, $class4out, $class4in) = @_;

	open (LOG23,">/data1/tmp_log/tmp_prad_recon_mpls");
	#print LOG23 "dtype-->$dtype,,,type--->$type,,,,default--->$default,,,,defaultout--->$defaultout,,,silverout--->$silverout,,,,silverin---->$silverin,,,goldout--->$goldout,,,goldin---->$goldin,,,,disp---$disp,,,egress---->$egress,,,class1out--->$class1out,,,class1in---->$class1in\n";

        $dtype = ($dtype =~ /daily/i) ? 1 : 0;

        my $values;
	my %report = () ;

	# First, do class0 - old default

#print "<P> PRE TYPE ($type)\n";
#print "<P> DEFAULT\n";
	##print "<P>DEFAULT ($#$default) (@$default)\n";
	if ($type == 1) {
        	$report = &mpls_exline($dtype, 1, $type, $default, \%report, $disp, $egress);
	} elsif ($type == 2) {
        	$report = &mpls_exline($dtype, 1, $type, $defaultout, \%report, $disp, $egress, $default);
	}
	#print "<P>SILVER ($#$silver) (@$silver)\n";

#print "<P> CLASS1\n";
	# Second, do class1
        $report = &mpls_exline($dtype, 2, $type, $class1out, \$report, $disp, $egress, $class1in);

#print "<P> CLASS2\n";
	# Third, do class2
        $report = &mpls_exline($dtype, 3, $type, $class2out, \$report, $disp, $egress, $class2in);

#print "<P> SILVER\n";
	# Fourth, do class3 - old silver
        $report = &mpls_exline($dtype, 4, $type, $silverout, \$report, $disp, $egress, $silverin);

	# Fifth, do class4
	$report = &mpls_exline($dtype, 5, $type, $class4out, \$report, $disp, $egress, $class4in);

	#print "<P>GOLD ($#$gold) (@$gold)\n";
	
	# Sixt, do class5 - old gold
        $report = &mpls_exline($dtype, 6, $type, $goldout, \$report, $disp, $egress, $goldin);

        return $report;
}
sub mpls_reconcile_mVPN {
        my($dtype, $type, $default, $defaultout, $silverout, $silverin, $goldout, $goldin, $disp, $egress,$class1out, $class1in, $class2out, $class2in, $class4out, $class4in) = @_;

	open (LOG23,">/data1/tmp_log/tmp_prad_recon_mpls");
	print LOG23 "dtype-->$dtype,,,type--->$type,,,,default--->$default,,,,defaultout--->$defaultout,,,silverout--->$silverout,,,,silverin---->$silverin,,,goldout--->$goldout,,,goldin---->$goldin,,,,disp---$disp,,,egress---->$egress,,,class1out--->$class1out,,,class1in---->$class1in\n";

        $dtype = ($dtype =~ /daily/i) ? 1 : 0;

        my $values;
        my %report_mVPN = () ;

        if ($type == 1) {
                $report_mVPN = &mpls_exline_mVPN($dtype, 1, $type, $default, \%report_mVPN, $disp, $egress);
        } elsif ($type == 2) {
                $report_mVPN = &mpls_exline_mVPN($dtype, 1, $type, $defaultout, \%report_mVPN, $disp, $egress, $default);
        }
	print LOG23 "report_mVPN--->$report_mVPN\n";
	foreach my $k (sort keys %$report_mVPN){

		#print LOG23 "k---->$k\n";
	}	

        # Second, do class1
        $report_mVPN = &mpls_exline_mVPN($dtype, 2, $type, $class1out, \$report_mVPN, $disp, $egress, $class1in);

        # Third, do class2
        $report_mVPN = &mpls_exline_mVPN($dtype, 3, $type, $class2out, \$report_mVPN, $disp, $egress, $class2in);

        # Fourth, do class3 - old silver
        $report_mVPN = &mpls_exline_mVPN($dtype, 4, $type, $silverout, \$report_mVPN, $disp, $egress, $silverin);

        # Fifth, do class4
        $report_mVPN = &mpls_exline_mVPN($dtype, 5, $type, $class4out, \$report_mVPN, $disp, $egress, $class4in);


        # Sixt, do class5 - old gold
        $report_mVPN = &mpls_exline_mVPN($dtype, 6, $type, $goldout, \$report_mVPN, $disp, $egress, $goldin);
	        foreach my $k ( sort keys %$report_mVPN){

                #print LOG23 "k---->$k\n";
        }
	#print LOG23 "report_mVPN_ref--->$report_mVPN_ref\n";
	print LOG23 "report_mVPN---->\%report_mVPN\n";	

	#%report_mVPN=();
        return $report_mVPN;
}


# Reconcile gold, silver and default into one list
# Pre: dtype, start, default, %report, disp, egress
# Post: %report
sub mpls_exline {
        my ($dtype, $start, $type, $default, $report, $disp, $egress, $defaultin) = @_;
	#my %report = ();
        #print "($dtype, $start, $default, $report)\n";
        #my ($rpt) = $report;
        my $tmp;
	open (LOG34,">/data1/tmp_log/tmp_prad_exline_mpls");
	#print LOG34 "start--->$start,,,type--->$type,,,default---->$default,,,,defaultin--->$defaultin\n";

#print "<br>start=$start<br>";
#print "<P>default=@$default<br><br>";
#print "<P>defaultin=@$defaultin<br><br>";
#print LOG34 "default--->$$default,,,,defaultin---->$$defaultin\n";
	my $tmp_default = $default;
	#if ($$default[$l] =~ /FAIL/i || $$default[$l] eq "") {
	if ($$default[0] =~ /FAIL/i || $$default[0] eq "") {
		$tmp_default = $defaultin;
	}
	#if ($$default[$l] =~ /FAIL/i && $$defaultin[$l] =~ /FAIL/i) {
	if ($$default[0] eq "" && $$defaultin[0] eq "") {
		return \%report;
	}
	#if ($$default[$l] eq "" && $$defaultin[$l] eq "") {
	if ($$default[0] eq "" && $$defaultin[0] eq "") {
		return \%report;
	}
	#if ($default eq " " && $defaultin eq " "){
	#	return \%report;
	#}	
	#print LOG34 "gloing to calaculate=\n";
        for (my $l=0; $l<=$#$tmp_default; $l++) {
                #print "Rec: $l\n";
# print "<P> PRE 1 ($start) ($l) ($#$tmp_default) ($$default[$l]) ($$defaultin[$l])\n";
		if ($start == 1) {
			if ($type == 1) {
				next if ($$default[$l] !~ /^\d+/);
			} elsif ($type == 2) {
				#next if ($$default[$l] !~ /^\d+|^FAIL/ && $$defaultin[$l] !~ /^\d+|^FAIL/);
				if ($$default[$l] !~ /^\d+|^FAIL/ && $$defaultin[$l] !~ /^\d+|^FAIL/) {
#print "<P> NEXTING PRE ($l) ($#$tmp_default) ($$default[$l]) ($$defaultin[$l])\n";
					#print "<P> default=@$default <P> defaultin=@$defaultin <P> --> nexting\n";
					next;
				}
			}
		} else {
#			next if ($$default[$l] !~ /^\d+/ && $$defaultin[$l] !~ /^\d+/);
			next if ($$default[$l] !~ /^\d+|^FAIL/ && $$defaultin[$l] !~ /^\d+|^FAIL/);
		}
		my (@v, @vin, @tmp_v);

		if ($$default[$l] =~ /^\d+/) {
	                chomp($$default[$l]);
	                @v = split(/\s+/, $$default[$l]);
		}
		#if ($start != 1 && $$defaultin[$l] =~ /^\d+/) {
		if ((($start != 1) || (($start == 1) && ($type == 2))) && $$defaultin[$l] =~ /^\d+/) {
	                chomp($$defaultin[$l]);
	                @vin = split(/\s+/, $$defaultin[$l]);
		}
		@tmp_v = split(/\s+/, $$tmp_default[$l]);

                if($dtype) {
			#if($disp !~ /table/) { # graph for mpls - out and in
				#if($egress) {
				#	$tmp = "$v[6]\t$v[7]\t$v[8]\t$v[12]\t$v[14]";
				#} else {
				#	$tmp = "$v[4]\t$v[5]\t$v[8]\t$v[12]\t$v[14]";
				#}				
			#} else {
				#if ($start == 1) {
				if (($start == 1) && ($type == 1)) {
		        	                $tmp = "$v[4]\t$v[5]\t$v[6]\t$v[8]\t$v[12]\t$v[14]";
				} else {
					if ($$defaultin[$l] !~ /^\d+/) {
		        	                #$tmp = "$v[4]\t$v[5]\t$v[6]\t$v[8]\t$v[12]\t$v[14]";
						$tmp = "$v[4]\t$v[5]\t$v[6]\t$v[14]\t$v[12]\t$v[10]";
					}
					if ($$default[$l] !~ /^\d+/) {
		        	                #$tmp = "$vin[4]\t$vin[5]\t$vin[6]\t$vin[8]\t$vin[12]\t$vin[14]";
						$tmp = "$vin[4]\t$vin[5]\t$vin[6]\t$vin[14]\t$vin[12]\t$vin[10]";
					}
					if ($$defaultin[$l] =~ /^\d+/ && $$default[$l] =~ /^\d+/) {
			                        $tmp = "$vin[4]\t$vin[5]\t$v[6]\t";
						#if ($v[8] >= $vin[8]) {
						#	$tmp .= "$v[8]\t";
						#} else {
						#	$tmp .= "$vin[8]\t";
						#}
						if ($v[14] >= $vin[14]) {
                                                        $tmp .= "$v[14]\t";
                                                } else {
                                                        $tmp .= "$vin[14]\t";
                                                }

						if ($v[12] >= $vin[12]) {
							$tmp .= "$v[12]\t";
						} else {
							$tmp .= "$vin[12]\t";
						}
						if ($v[10] >= $vin[10]) {
							$tmp .= $v[10];
						} else {
							$tmp .= $vin[10];
						}
					}
				}
#print "tmp=$tmp<br>";
	                #}
                } else {
			#if($egres) {
				# get egress traffic
			#	$tmp = "$v[2]\t$v[4]";
			#} else {
	                        #$tmp = "$v[3]\t$v[4]"; # this is bytes in/out
				#if ($start == 1) {
				if (($start == 1) && ($type == 1)) {
					$tmp = "$v[1]\t$v[2]"; # kbps in/out
				} else {
					$tmp = "$vin[1]\t$v[2]";
				}
	                #}
                }

                #assume report does not exist
#print "($start) Time: $v[0].....tmp=$tmp\n";
                if($start == 1) {
#                        $report{$v[0]} = $tmp;
                        $report{$tmp_v[0]} = $tmp;
                } else {
                        if(not exists $report{$tmp_v[0]}) {
                                if($start == 2) {
                                        $report{$tmp_v[0]} = "|$tmp";
				} elsif($start == 3) {
					$report{$tmp_v[0]} = "||$tmp";
				} elsif($start == 4) {
					 $report{$tmp_v[0]} = "|||$tmp";
				} elsif($start == 5) {
					 $report{$tmp_v[0]} = "||||$tmp";
                                } else {
                                        $report{$tmp_v[0]} = "|||||$tmp";
                                }
                        } else {
                                $report{$tmp_v[0]} .= "|$tmp";
                        }
                }
#print "<P> tmpv ($tmp_v[0])\n";
		if($tmp_v[0] =~ /fail/i) {
			# insert empty fields for all existing keys
			foreach my $k (sort keys %report) {
				$report{$k} .= "|";
			}
		}

                #print "<P>IN ($start) ($tmp_v[0]) Rpt: ($report{$tmp_v[0]})\n";
        }

#foreach my $k (sort keys %report) {
#	print "<P>XXX ($k) ($report{$k})\n";
#}

        return \%report;
}
sub mpls_exline_mVPN {
        my ($dtype, $start, $type, $default_mVPN, $report_mVPN, $disp, $egress, $defaultin_mVPN) = @_;
        my $tmp;
        open (LOG34,">/data1/tmp_log/tmp_prad_exline_mpls");
        print LOG34 "start--->$start,,,type--->$type,,,default_mVPN---->$default_mVPN[0],,,,defaultin_mVPN--->$defaultin_mVPN[0]\n";

        my $tmp_default = $default_mVPN;
        if ($$default_mVPN[0] =~ /FAIL/i || $$default_mVPN[0] eq "") {
		print LOG34 "if case1\n";
                $tmp_default = $defaultin_mVPN;
        }
        if ($$default_mVPN[0] eq "" && $$defaultin_mVPN[0] eq "") {
		print LOG34 "if case2\n";
                return \%report_mVPN;
        }
        if ($$default_mVPN[0] eq "" && $$defaultin_mVPN[0] eq "") {
		print LOG34 "if case3\n";
                return \%report_mVPN;
        }
	#print LOG34 "gloing to calaculate=\n";
        for (my $l=0; $l<=$#$tmp_default; $l++) {
		#print LOG34 "for case1\n";
                if ($start == 1) {
                        if ($type == 1) {
                                next if ($$default_mVPN[$l] !~ /^\d+/);
                        } elsif ($type == 2) {
                                #next if ($$default_mVPN[$l] !~ /^\d+|^FAIL/ && $$defaultin_mVPN[$l] !~ /^\d+|^FAIL/);
                                if ($$default_mVPN[$l] !~ /^\d+|^FAIL/ && $$defaultin_mVPN[$l] !~ /^\d+|^FAIL/) {
                                        #print "<P> default_mVPN=@$default_mVPN <P> defaultin_mVPN=@$defaultin_mVPN <P> --> nexting\n";
                                        next;
                                }
                        }
                } else {
                        next if ($$default_mVPN[$l] !~ /^\d+|^FAIL/ && $$defaultin_mVPN[$l] !~ /^\d+|^FAIL/);
                }
		#print LOG34 "after for loop\n";
                my (@v, @vin, @tmp_v);

                if ($$default_mVPN[$l] =~ /^\d+/) {
			print LOG34 "digit out:$$default_mVPN[$l]\n";
                        chomp($$default_mVPN[$l]);
                        @v = split(/\s+/, $$default_mVPN[$l]);
                }
                if ((($start != 1) || (($start == 1) && ($type == 2))) && $$defaultin_mVPN[$l] =~ /^\d+/) {
			print LOG34 "if conditon for default in\n";
			print LOG34 "defaultin_mVPN:$$defaultin_mVPN[$l]\n";
                        chomp($$defaultin_mVPN[$l]);
                        @vin = split(/\s+/, $$defaultin_mVPN[$l]);
                }
		print LOG34 "tmp_default:$$tmp_default[$l]\n";
                @tmp_v = split(/\s+/, $$tmp_default[$l]);

		print LOG34 "dtype:$dtype\n";
                if($dtype) {
				print LOG34 "start:$start\ttype:$type\tdefaultin_mVPN:$$defaultin_mVPN[$l]\tdefault_mVPN:$$default_mVPN[$l]\n";
                                if (($start == 1) && ($type == 1)) {
                                                $tmp = "$v[4]\t$v[5]\t$v[6]\t$v[8]\t$v[12]\t$v[14]";
                                } else {
					print LOG34 "A defaultin_mVPN:$$defaultin_mVPN[$l]\ndefault_mVPN:$$default_mVPN[$l]\n";
                                        if ($$defaultin_mVPN[$l] !~ /^\d+/) {
						print LOG34 "defaultin_mVPN not in digits\n";
                                                #$tmp = "$v[4]\t$v[5]\t$v[6]\t$v[8]\t$v[12]\t$v[14]";
						#Karuna - test for mVPN
                                                #$tmp = "$v[4]\t$v[5]\t$v[6]\t$v[14]\t$v[12]\t$v[10]";
                                                $tmp = "\t-\t$v[6]\t$v[14]\t$v[12]\t$v[10]";
                                        }
                                        if ($$default_mVPN[$l] !~ /^\d+/) {
                                                #$tmp = "$vin[4]\t$vin[5]\t$vin[6]\t$vin[8]\t$vin[12]\t$vin[14]";
                                                $tmp = "$vin[4]\t$vin[5]\t$vin[6]\t$vin[14]\t$vin[12]\t$vin[10]";
                                        }
                                        if ($$defaultin_mVPN[$l] =~ /^\d+/ && $$default_mVPN[$l] =~ /^\d+/) {
						
                                                $tmp = "$vin[4]\t$vin[5]\t$v[6]\t";
                                                if ($v[14] >= $vin[14]) {
                                                        $tmp .= "$v[14]\t";
                                                } else {
                                                       $tmp .= "$vin[14]\t";
                                                }

                                                if ($v[12] >= $vin[12]) {
                                                        $tmp .= "$v[12]\t";
                                                } else {
                                                        $tmp .= "$vin[12]\t";
                                                }
                                                if ($v[10] >= $vin[10]) {
                                                        $tmp .= $v[10];
                                                } else {
                                                        $tmp .= $vin[10];
                                                }
                                        }
                                }
		print LOG34 "tmp:$tmp\n";
                } else {
                                if (($start == 1) && ($type == 1)) {
                                        $tmp = "$v[1]\t$v[2]"; # kbps in/out
                                } else {
					print LOG34 "else condition of dtype\n";
					print LOG34 "vin:$vin[1]\t$vin[0]\n";
                                        $tmp = "$vin[1]\t$v[2]";
					print LOG34 "tmp:$tmp\n";
                                }
                }

                #assume report_mVPN does not exist
                if($start == 1) {
                        $report_mVPN{$tmp_v[0]} = $tmp;
                } else {
			print LOG34 "else of start 1 condition\n";
                        if(not exists $report_mVPN{$tmp_v[0]}) {
                                if($start == 2) {
                                        $report_mVPN{$tmp_v[0]} = "|$tmp";
                                } elsif($start == 3) {
                                        $report_mVPN{$tmp_v[0]} = "||$tmp";
                                } elsif($start == 4) {
                                         $report_mVPN{$tmp_v[0]} = "|||$tmp";
                                } elsif($start == 5) {
                                         $report_mVPN{$tmp_v[0]} = "||||$tmp";
                                } else {
                                        $report_mVPN{$tmp_v[0]} = "|||||$tmp";
                                }
                        } else {
                                $report_mVPN{$tmp_v[0]} .= "|$tmp";
                        }
                }
                if($tmp_v[0] =~ /fail/i) {
                        # insert empty fields for all existing keys
                        foreach my $k (sort keys %report_mVPN) {
                                $report_mVPN{$k} .= "|";
                        }
                }
        }

        return \%report_mVPN;
}


################### VPLS TR START ####################
# VPLS Reports
# Generate VPLS reports
# Pre: $start, $end, $disp, $data, $opshandle, $code, $svc, $remote
# Post: values
# Chandini
sub gen_trafficrptvpls {
	open (FHK,">/data1/tmp_log/logk");
    my($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $egress, $id) = @_;
	print FHK "inpurts to gen_trafficrptvpls in the gen_trafficrptvpls ---> $master_srv,$log_ser,$agg_serv,$p_t, $master_lb,$log_lb, $agg_lb \n";
print FHK " inputs===> start-$start end-$end disp-$disp data-$data opshandle-$opshandle code -$code svc-$svc remote-$remote egress-$egress id-$id \n";
 
	print FHK "<P>DD1 ($start, $end, $disp, $data, $opshandle, $code, $svc, $remote) \n";

	# COS = Class_0 = default/Low priority data; Class_1 = Standard Data; Class_2 = Interactive Data;
	# Class_3 = Critical Data; Class_4 = Video; Class_5 = Voice; VTM = VLP Transparent Mode.
######  VLP - VPLS Start  ######
    my (@vals, @class0out, @default, @class3out, @class3in, @class5out, @class5in, @retvals, @no_class, @vtmin, @vtmout);
######  VLP - VPLS Stop  ######
    my (@class4in, @class4out, @class2in, $class2out, , @class1in, @class1out);

	my @files = ("$opshandle");
	my $filename_head = "x-vpls-";

	#CR-84 Juniper IPVPN Report
        if($junos) {
                $filename_head = "x-ipvpn-";
        }
	my $filename_class = "";
#	my $filename_rateacl = "rateacl:";
	my $filename_tail = "-".$opshandle;

	CMS_DB::connect_to_database;

	# first get pollingtype
	# 25/10/2006
   	my $pt = CMS_SERVICE_DB::get_pollingtype_sid($opshandle);
	my $pgt = $$pt[0];
	open(DEBUG,">/data1/tmp_log/anjan-test");
#	my $pollingtype = $$pgt[0];
         my $pollingtype = "";  
# print "<P> Polling type = ($pollingtype) ($pt) ($opshandle)\n";
	#print DEBUG "<P> Polling type = ($pollingtype) ($pt) ($opshandle)\n";

	# Next get cos labels - reachvpls
	my $cls = CMS_SERVICE_DB::get_costype_sid($opshandle);
	my $cyp = $$cls[0];
	my $costype = $$cyp[0];
	#print DEBUG "Costype here for all services ---->$costype\n";

	# 25/10/2006
	# if pollingtype == Transparent, then no CoS; if pollingtype = VLAN or Blank, then use poll Cos

	# Now get all COS types
	# need to get class 0 , class 1, class 2, class 3 , class 4 and class 5

	my $retclass = CMS_SERVICE_DB::get_cos_all($opshandle);
	# $retclass{classmap}{classname} = value / $ret{rateacl}{classname} = value

	# need index
	my $cosix = 1;
	my %cosval;
	foreach my $rckey (keys %$retclass) {

# print "<P> COS VAL ($rckey), ($$retclass{$rckey})\n";
	#print DEBUG "<P> COS VAL ($rckey), ($$retclass{$rckey})\n";

		my $coskey = $$retclass{$rckey};
		# classmap or rateacle
		foreach my $rc2key (keys %$coskey) {
			#print "<P> COS VAL 2 ($rc2key) \n";
			# class 0, class 1 etc
			$cosval{$cosix} = "$rckey^^$rc2key";
#print "<P> COSVAL ($cosix) ($rckey) ($rc2key) fname ($$coskey{$rc2key})\n";
			#print DEBUG "<P> COSVAL ($cosix), ($rckey), ($rc2key), fname, ($$coskey{$rc2key}).\n";
			$cosix++;
		#	if($rckey =~ /classmap/i) {
        	if($$coskey{$rc2key}) {
				push @files, $filename_head.$filename_class.lc($$coskey{$rc2key}).$filename_tail;
		    } #else {
			#    $$coskey{$rc2key}="vlp";
				#push @files, $filename_head.$filename_class."vlp".$filename_tail;
			#}
		}
	}
		
	CMS_DB::disconnect_from_database;
    my $fail = 0;
	#print DEBUG "here negore $cosval{1} \n";
if($opshandle =~ /x-alias-/){
$costype="reachvpls";
$opshandle =~ s/x-alias-//g;
$opshandle =~ s/ //g;
$filename_tail = "-".$opshandle;
%cosval = (0=>"",1=>"rateacl^^class5",2=>"classmap^^class5",3=>"rateacl^^class0",4=>"classmap^^class0",5=>"rateacl^^class1",6=>"classmap^^class1",7=>"rateacl^^class2",8=>"classmap^^class2",9=>"rateacl^^class4",10=>"classmap^^class4",11=>"rateacl^^class3",12=>"classmap^^class3");
my @files_temp=("$opshandle");
foreach my $k (5,0,1,2,4,3){
push @files_temp,$filename_head.$filename_class."class_".$k.$filename_tail;
push @files_temp,$filename_head.$filename_class."class_".$k.$filename_tail;

}
@files=@files_temp;

}
print FHK "files array --->@files\n";
for my $i (0..$#files) {
#print FHK "<P>($i) ($files[$i]) ($remote)\n";
        my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                PeerPort => 4546,
                Proto => "tcp",
                Type => SOCK_STREAM) or $fail = 1;

        if($fail) {
                push @retvals, "FAIL";
                push @retvals, "<p class=header>Error</p><p class=text>";
                push @retvals, "Cannot retrieve traffic report for $disp_rept.";
                return @retvals;
        }

		# Only need tabular data from stats poller
	#print DEBUG "\nAnjanv0 $socket $start\n $end\n $data\n $files[$i] \n";
print FHK "$socket $start\n $end\n $files[$i] \n";
print $socket "$start $end table $data $files[$i] \n";
        %vals = ();
        while(my $l = <$socket>) {
        	push @vals, $l;
        }
        close($socket);
	#print DEBUG "AnjanV vals= @vals\n";
print FHK "valsxxxxxxxxxxxxxx= @vals\n";
# print "<BR>DDD ($i) ($cosval{$i})\n";
	#print DEBUG "cosval----------------->    $cosval{$i}\n";

		# now use $cosval{index} to work out whether classmap/rateacl and what the class is
		if ($cosval{$i} =~ /^classmap/) {
			#print DEBUG " inside cosval classmap\n";
			# classmap OUT
			# now work out what the class is
			my $classtype = (split(/\^\^/, $cosval{$i}))[1];
			#print DEBUG "classtype----->$classtype\n";
######  VLP - VPLS Start  ######
#if($pollingtype =~ /TRANS/i) {
	#@vtmout = @vals;
#}
#else {
			if($classtype =~ /class0/i) {
				@class0out = @vals;
			} 
			elsif ($classtype =~ /class1/i) {
				@class1out = @vals;
			} 
			elsif ($classtype =~ /class2/i) {
            			@class2out = @vals;
            		} 
            		elsif ($classtype =~ /class3/i) {
            			@class3out = @vals;
            		} 
            		elsif ($classtype =~ /class4/i) {
            			@class4out = @vals;
            		} 
            		elsif ($classtype =~ /class5/i) {
            			@class5out = @vals;
            		} 
		#} 
######  VLP - VPLS Stop  ######
}
		elsif ($cosval{$i} =~ /^rateacl/i) {
			#print DEBUG " inside cosval rateacl\n";
				# rateacl / classmap IN

			# now work out what the class is
			my $classtype = (split(/\^\^/, $cosval{$i}))[1];
			#print DEBUG "classtypei in  rateacl----->$classtype\n";
######  VLP - VPLS Start ######
#if($pollingtype =~ /TRANS/i) {
	#	@vtmin = @vals;
#}
#else {
			if($classtype =~ /class0/i) {
       	    	@default = @vals;
            } 
            elsif ($classtype =~ /class1/i) {
           		@class1in = @vals;
           	} 
           	elsif ($classtype =~ /class2/i) {
            	@class2in = @vals;
           	} 
           	elsif ($classtype =~ /class3/i) {
#print "<P> 2-> ($files[$i]) ($classtype) (@vals)\n";
            	@class3in = @vals;
           	} 
           	elsif ($classtype =~ /class4/i) {
            	@class4in = @vals;
           	} 
           	elsif ($classtype =~ /class5/i) {
            	@class5in = @vals;
           	} 
#print "<P> DDDDD (@default) (@class1in) (@class2in) (@class3in) (@class4in) (@class5in) (@vtmin)\n";
		#} 
######  VLP - VPLS Stop  ######
}
		else {
			@no_class = @vals;
			$class0out[0] = 'FAIL';
			$default[0] = 'FAIL';
			$class1out[0] = 'FAIL';
			$class1in[0] = 'FAIL';
			$class2out[0] = 'FAIL';
			$class2in[0] = 'FAIL';
			$class3out[0] = 'FAIL';
			$class3in[0] = 'FAIL';
			$class4out[0] = 'FAIL';
			$class4in[0] = 'FAIL';
			$class5out[0] = 'FAIL';
			$class5in[0] = 'FAIL';
######  VLP - VPLS Start  ######
			#$vtmin[0] = 'FAIL';
			#$vtmout[0] = 'FAIL';	
######  VLP - VPLS Stop  ######		
		}

        @vals = ();
		#print "<P>Default (@default) <P>Silver (@silver) <P>Gold (@gold)\n";
	}
close DEDUG;
	# Re-group
######  VLP - VPLS Start  ######
    my (%timestamp, %tmp_default, %tmp_class0out, %tmp_class1in, %tmp_class1out, %tmp_class2in, %tmp_class2out, %tmp_class3in, %tmp_class3out, %tmp_class4in, %tmp_class4out, %tmp_class5in, %tmp_class5out, %tmp_vtmin, %tmp_vtmout);
    my (@new_default, @new_class0out, @new_class1in, @new_class1out, @new_class2in, @new_class2out, @new_class3in, @new_class3out, @new_class4in, @new_class4out, @new_class5in, @new_class5out, @new_vtmin, @new_vtmout);
######  VLP - VPLS Stop  ######

    undef(%timestamp);
    if ($default[0] !~ /FAIL/) {
        foreach my $val (@default) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_default{$seg[0]} = $val;
        }
    }
    if ($class0out[0] !~ /FAIL/) {
        foreach my $val (@class0out) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class0out{$seg[0]} = $val;
        }
    }

    foreach my $ts (sort {$a <=> $b} keys%timestamp) {
        if (!$ts)  { next; }
        push @new_default, $tmp_default{$ts};
        push @new_class0out, $tmp_class0out{$ts};
    }

    if ($default[0] !~ /FAIL/) {
    	@default = @new_default;
    }
    if ($class0out[0] !~ /FAIL/) {
    	@class0out = @new_class0out;
    }

    undef(%timestamp);
    if ($class1in[0] !~ /FAIL/) {
        foreach my $val (@class1in) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class1in{$seg[0]} = $val;
        }
    }
    if ($class1out[0] !~ /FAIL/) {
        foreach my $val (@class1out) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class1out{$seg[0]} = $val;
        }
    }

    foreach my $ts (sort {$a <=> $b} keys%timestamp) {
        if (!$ts)  { next; }
        push @new_class1in, $tmp_class1in{$ts};
        push @new_class1out, $tmp_class1out{$ts};
    }

    if ($class1in[0] !~ /FAIL/) {
    	@class1in = @new_class1in;
    }
    if ($class1out[0] !~ /FAIL/) {
    	@class1out = @new_class1out;
    }

    undef(%timestamp);
    if ($class2in[0] !~ /FAIL/) {
        foreach my $val (@class2in) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class2in{$seg[0]} = $val;
        }
    }
    if ($class2out[0] !~ /FAIL/) {
        foreach my $val (@class2out) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class2out{$seg[0]} = $val;
        }
    }

    foreach my $ts (sort {$a <=> $b} keys%timestamp) {
        if (!$ts)  { next; }
        push @new_class2in, $tmp_class2in{$ts};
        push @new_class2out, $tmp_class2out{$ts};
    }

    if ($class2in[0] !~ /FAIL/) {
    	@class2in = @new_class2in;
    }
    if ($class2out[0] !~ /FAIL/) {
    	@class2out = @new_class2out;
    }

    undef(%timestamp);
    if ($class3in[0] !~ /FAIL/) {
        foreach my $val (@class3in) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class3in{$seg[0]} = $val;
        }
    }
    if ($class3out[0] !~ /FAIL/) {
        foreach my $val (@class3out) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class3out{$seg[0]} = $val;
        }
    }

    foreach my $ts (sort {$a <=> $b} keys%timestamp) {
        if (!$ts)  { next; }
        push @new_class3in, $tmp_class3in{$ts};
        push @new_class3out, $tmp_class3out{$ts};
    }

    if ($class3in[0] !~ /FAIL/) {
    	@class3in = @new_class3in;
    }
    if ($class3out[0] !~ /FAIL/) {
    	@class3out = @new_class3out;
    }

    undef(%timestamp);
    if ($class4in[0] !~ /FAIL/) {
        foreach my $val (@class4in) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class4in{$seg[0]} = $val;
        }
    }
    if ($class4out[0] !~ /FAIL/) {
        foreach my $val (@class4out) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class4out{$seg[0]} = $val;
        }
    }

    foreach my $ts (sort {$a <=> $b} keys%timestamp) {
        if (!$ts)  { next; }
        push @new_class4in, $tmp_class4in{$ts};
        push @new_class4out, $tmp_class4out{$ts};
    }

    if ($class4in[0] !~ /FAIL/) {
    	@class4in = @new_class4in;
    }
    if ($class4out[0] !~ /FAIL/) {
    	@class4out = @new_class4out;
    }

    undef(%timestamp);
    if ($class5in[0] !~ /FAIL/) {
        foreach my $val (@class5in) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class5in{$seg[0]} = $val;
        }
    }
    if ($class5out[0] !~ /FAIL/) {
        foreach my $val (@class5out) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_class5out{$seg[0]} = $val;
        }
    }

    foreach my $ts (sort {$a <=> $b} keys%timestamp) {
        if (!$ts)  { next; }
        push @new_class5in, $tmp_class5in{$ts};
        push @new_class5out, $tmp_class5out{$ts};
    }

    if ($class5in[0] !~ /FAIL/) {
    	@class5in = @new_class5in;
    }
    if ($class5out[0] !~ /FAIL/) {
    	@class5out = @new_class5out;
    }
    
######  VLP - VPLS Start  ######
    undef(%timestamp);
    if ($vtmin[0] !~ /FAIL/) {
        foreach my $val (@vtmin) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_vtmin{$seg[0]} = $val;
        }
    }
    if ($vtmout[0] !~ /FAIL/) {
        foreach my $val (@vtmout) {
            my @seg = split /\s+/, $val;
            $timestamp{$seg[0]} = "";
            $tmp_vtmout{$seg[0]} = $val;
        }
    }

    foreach my $ts (sort {$a <=> $b} keys%timestamp) {
        if (!$ts)  { next; }
        push @new_vtmin, $tmp_vtmin{$ts};
        push @new_vtmout, $tmp_vtmout{$ts};
    }

    if ($vtmin[0] !~ /FAIL/) {
    	@vtmin = @new_vtmin;
    }
    if ($vtmout[0] !~ /FAIL/) {
    	@vtmout = @new_vtmout;
    }
######  VLP - VPLS Stop  ######
	open (FHC,">/data1/tmp_log/logc");     
	#print FHC "<P> DEBUG <P> 0 -> (@default) <P> 0 Out -> (@class0out)\n <P> 1 -> (@class1in) <P> 1 Out -> (@class1out)\n <P> 2 -> (@class2in) <P> 2 Out -> (@class2out)\n <P> 3-> (@class3in) <P> 3 Out -> (@class3out)\n <P> 4 -> (@class4in) <P> 4 out -> (@class4out) \n <P> 5 -> (@class5in) <P> 5 out -> (@class5out)  \n";


	## NOW MOVE THE NEW 6 COS to do something 
######  VLP - VPLS Start  ######
	if($disp =~ /table/i) {
# Chandini
		if ($default[0] =~ /FAIL/ && $class0out[0] =~ /FAIL/ && $class1out[0] =~ /FAIL/ && $class1in[0] =~ /FAIL/ && $class2out[0] =~ /FAIL/ && $class2in[0] =~ /FAIL/ && $class3out[0] =~ /FAIL/ && $class3in[0] =~ /FAIL/ && $class4out[0] =~ /FAIL/ && $class4in[0] =~ /FAIL/ && $class5out[0] =~ /FAIL/ && $class5in[0] =~ /FAIL/ && $vtmout[0] =~ /FAIL/ && $vtmin[0] =~ /FAIL/) {
			#print FHC " Inside indide table if and second if\n";
			@retvals = &dogen_trafficrptvpls($disp, $opshandle, $code, $svc, $data, 1, \@no_class, "", "", $costype);
		} 
		else {
			#print FHC " Insidetable if and else \n";
			@retvals = &dogen_trafficrptvpls($disp, $opshandle, $code, $svc, $data, 2, \@default,  \@class0out, \@class3out, \@class3in, \@class5out, \@class5in, \@vtmout, \@vtmin, \@class1out, \@class1in, \@class2out, \@class2in, \@class4out, \@class4in, "", "", $costype);
		}
	} 
	else {
		if ($default[0] =~ /FAIL/ && $class0out[0] =~ /FAIL/ && $class1out[0] =~ /FAIL/ && $class1in[0] =~ /FAIL/ && $class2out[0] =~ /FAIL/ && $class2in[0] =~ /FAIL/ && $class3out[0] =~ /FAIL/ && $class3in[0] =~ /FAIL/ && $class4out[0] =~ /FAIL/ && $class4in[0] =~ /FAIL/ && $class5out[0] =~ /FAIL/ && $class5in[0] =~ /FAIL/ && $vtmout[0] =~ /FAIL/ && $vtmin[0] =~ /FAIL/) {
			&dogen_trafficrptvpls($disp, $opshandle, $code, $svc, $data, 1, \@no_class, $egress, $id, $costype);
		} 
		else {
			&dogen_trafficrptvpls($disp, $opshandle, $code, $svc, $data, 2, \@default, \@class0out, \@class3out, \@class3in, \@class5out, \@class5in, \@vtmout, \@vtmin, \@class1out, \@class1in, \@class2out, \@class2in, \@class4out, \@class4in, $egress, $id, $costype); 
		}
######  VLP - VPLS Stop  ######
		return;
	}

	return @retvals;

}

# Actually prepare the data and generate VPLS traffic reports
# Pre: $disp, $opshandle, $code, $svc, $data, $tyep, $default, $silver, $gold
# Post: @retvals or gif
# Chandini
sub dogen_trafficrptvpls {
	
	my ($disp, $opshandle, $code, $svc, $data, $default, $class0out, $class3out, $class3in, $class5out, $class5in, $egress, $id, $minute, $rptgen, @retvals, $costype);
	open (FHU,">> /data1/tmp_log/logu");
	print FHU "for vpls\n";
	#print FHU " inputs to dogen_trafficrptvpls -->  $master_srv,$log_ser,$agg_serv,$p_t,$master_lb, $log_lb,$agg_lb \n";
	#print FHU "in dogen_trafficrptvpls ==== inputs are :===> $disp, $opshandle, $code, $svc, $data, $default, $class0out, $class3out, $class3in, $class5out, $class5in, $egress, $id, $minute, $rptgen, @retvals, $costype \n"; 

######  VLP - VPLS Start  ######
	my ($class1out, $class1in, $class2out, $class2in, $class4out, $class4in, $vtmout, $vtmin);
######  VLP - VPLS Stop  ######
	my $type = @_[5];
	#print FHU "type = $type \n";

	if ($type == 1) {
		($disp, $opshandle, $code, $svc, $data, $type, $default, $egress, $id, $costype) = @_;
	} elsif ($type == 2) {
######  VLP - VPLS Start  ######
		($disp, $opshandle, $code, $svc, $data, $type, $default, $class0out, $class3out, $class3in, $class5out, $class5in, $vtmout, $vtmin, $class1out, $class1in, $class2out, $class2in, $class4out, $class4in, $egress, $id, $costype) = @_; 
		#print FHU " inputs are=====> $disp, $opshandle, $code, $svc, $data, $type, $default, $class0out, $class3out, $class3in, $class5out,$class5in, $vtmout, $vtmin, $class1out, $class1in, $class2out, $class2in, $class4out, $class4in, $egress, $id, $costype \n";

######  VLP - VPLS Stop  ######
	}
 
######  VLP - VPLS Start  ######              
        my ($c0, $c1, $c2, $c3, $c4, $c5, $vtm);
######  VLP - VPLS Stop  ######
  
        # Now set the labels
        if($costype =~ /reachvpls/i) {
                $c0 = $vpls_cos_label{class0};
                $c1 = $vpls_cos_label{class1};
                $c2 = $vpls_cos_label{class2};
                $c3 = $vpls_cos_label{class3};
                $c4 = $vpls_cos_label{class4};
                $c5 = $vpls_cos_label{class5};
######  VLP - VPLS Start  ######
                #$vtm = $vpls_cos_label{vtm};
######  VLP - VPLS Stop  ######
         } else {
                $c0 = "class_0";
                $c1 = "class_1";
                $c2 = "class_2";
                $c3 = "class_3";
                $c4 = "class_4";
                $c5 = "class_5";
        }

#print "<P> Label ($telstra_cos_label{class0}) ($costype) ($c0) ($c1) ($c2) ($c3) ($c4) ($c5) ($vtm).\n";

	#print "<P>GTD ($data, $default, $silver, $gold)\n";

        my $td = "td class=text";
        my $th = "td class=th";

        push @retvals, "<p class=header>Traffic Report for $disp_rept</p><p class=text>\n";
# Chandini
        push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=21&et=$encet&master_srv=$master_srv&log_ser=$log_ser&agg_serv=$agg_serv&pt=$p_t&master_lb=$master_lb&log_lb=$log_lb&agg_lb=$agg_lb\">";
        push @retvals, "Click here to generate another report.</a>\n<p class=text>";
        if($data !~ /daily/i) {
		$minute = 1;
                $data = "5minute";
        }
        push @retvals, "Report type: $data</p>\n";
        push @retvals, "<p><hr><p class=text>\n";

	my ($h, @rpt, $out, @values);
	#print "($disp, $opshandle, $code, $svc, $data, $type, $default, $class0out, $silver, $gold)\n";

	my $datafile = "/tmp/vplsgraph.data.$$.$id.traffic";
        push @rpt, "<table cellpadding=4 cellspacing=5 border=1>";

	open(LOG231,">/data1/tmp_log/anjan-test12345");
	#print LOG231 "datafile------>$datafile\n";
close LOG231;
	if($minute) {
		$h = "<tr><$th>Date</td><$th>Class of Service</td><$th>kbps received from customer</td><$th>kbps sent to customer</td>\n";
	} else {
		#print FHU "\n in some else \n";
        	$h = "<tr><$th>Date (GMT)</td><$th>Class of Service</td><$th>Incoming MBytes<br>Customer to Telstra</td><$th>Use of port (%)</td>";
		$h .= "<$th>Outgoing MBytes<br>Telstra to Customer</td>";
	        $h .= "<$th>5 min peak<br>Max of In or Out</td><$th>1 hr peak<br>Max of In or Out</td><$th>24 hr peak<br>Max of In or Out</td></tr>\n";
	}

	push @rpt, $h;
	open (DE, ">/data1/tmp_log/Chand");
	#print FHU "rpt valuies ------->@rpt\n";
	#print FHU "h values-------> $h\n";

close DE; 
######  VLP - VPLS Start  ######
	#print FHU "befored vpls_reconcile\n"; 
    my $rpt = &vpls_reconcile($data, $type, $default, $class0out, $class3out, $class3in, $class5out, $class5in, $vtmout, $vtmin, $disp, $egress, $class1out, $class1in, $class2out, $class2in, $class4out, $class4in);
	#print FHU " After reconcile\n";
######  VLP - VPLS Stop  ######
	#print "<P>YY at begin ($rpt)\n";

	# daily = need col 0; 5min need col 1
	my $numcol = ($minute) ? 1 : 0;
	open (DEBUG1, ">/data1/tmp_log/anjan-test1");
#$rptgen=0;
    foreach my $k (sort keys %$rpt) {
		if (!$k)  { next; }
			#$rptgen = 1;
			my $date;
		if($k =~ /^\d+/) {
			$date = localtime($k);
		}
		next if($k =~ /fail/i);
$rptgen = 1;
	    my @vals = split(/\|/, $$rpt{$k});
		#print DEBUG1 "<P> Rpt 0 - ($vals[0]) - ($#vals).\n";
		#print DEBUG1 "<P> Rpt 1 - ($vals[1]) - ($#vals).\n";
		#print DEBUG1 "<P> Rpt 2 - ($vals[2]) - ($#vals).\n";
		#print DEBUG1 "<P> Rpt 3 - ($vals[3]) - ($#vals).\n";
		#print DEBUG1 "<P> Rpt 4 - ($vals[4]) - ($#vals).\n";
		#print DEBUG1 "<P> Rpt 5 - ($vals[5]) - ($#vals).\n";
######  VLP - VPLS Start  ######
		#print DEBUG1 "<P> Rpt 6 - ($vals[6]) - ($#vals).\n";
######  VLP - VPLS Stop  ######
		#print DEBUG1 "<P>VV ($$rpt{$k}) - ($#vals) - (@vals)\n";
		#print DEBUG1 "costype value ------/>$costype\n";
	
	    my $row = "<tr><$td>$date GMT</td><$td>$c0</td>";
######  VLP - VPLS Start  ######	
		my ($def, $def_out, $cl5, $cl5_out, $clvtm, $clvtm_out, $cl3, $cl3_out);
######  VLP - VPLS Stop  ######
		my ($cl2, $cl2_out, $cl1, $cl1_out, $cl4, $cl4_out);
		
####################################################################################################
# CLASS 0	
	    if($vals[0]) {
	        # only default
	        my @v = split(/\s+/, $vals[0]);
			if($minute) {
	            $def = ($v[0]) ? $v[0] : 0;
	            $def_out = ($v[1]) ? $v[1] : 0;
	        } 
	        else { #daily
	            $def = ($v[0]) ? $v[0] : 0;
	            $def_out = ($v[2]) ? $v[2] : 0;
	        }
	
	        foreach my $x (@v) {
				if($x !~ /^\d+/) { $x = "-"; }
	            $row .= "<$td>$x</td>";
	        }
	    } 
	    else {
			$def = $def_out = 0;
			if($minute) {
				$row .= "<$td>-</td><$td>-</td>";
			} 
			else {
	            $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
			}
	    }


####################################################################################################
# CLASS 1
		if($costype =~ /reachvpls/i) {
			$row .= "</tr>\n<tr><$td></td><$td>$c1</td>";
	        if($vals[1]) {
	            # class1
	            my @v = split(/\s+/, $vals[1]);
	            if($minute) {
	                $cl1 = ($v[0]) ? $v[0] : 0;
	                $cl1_out = ($v[1]) ? $v[1] : 0;
	            } else { #daily
	                $cl1 = ($v[0]) ? $v[0] : 0;
	                $cl1_out = ($v[2]) ? $v[2] : 0;
	            }
	
	            foreach my $x (@v) {
					if($x !~ /^\d+/) { $x = "-"; }
	                $row .= "<$td>$x</td>";
	            }
	        } 
	        else {
	            $cl1 = $cl1_out = 0;
	            if($minute) {
	                $row .= "<$td>-</td><$td>-</td>";
	            } 
	            else {
	                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
	            }
	        }
		}

####################################################################################################
# CLASS 2
		if($costype =~ /reachvpls/i) {
			$row .= "</tr>\n<tr><$td></td><$td>$c2</td>";
	        if($vals[2]) {
	            # class2
	            my @v = split(/\s+/, $vals[2]);
	            if($minute) {
	                    $cl2 = ($v[0]) ? $v[0] : 0;
	                    $cl2_out = ($v[1]) ? $v[1] : 0;
	            } else { #daily
	                    $cl2 = ($v[0]) ? $v[0] : 0;
	                    $cl2_out = ($v[2]) ? $v[2] : 0;
	            }
	
	            #$cl3 = ($v[$numcol]) ? $v[$numcol] : 0;
	            foreach my $x (@v) {
					if($x !~ /^\d+/) { $x = "-"; }
	                    $row .= "<$td>$x</td>";
	            }
		    } 
		    else {
		        $cl2 = $cl2_out = 0;
		        if($minute) {
		                $row .= "<$td>-</td><$td>-</td>";
		        } else {
		                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
		        }
	        }
		}

####################################################################################################
# CLASS 3
		if($costype =~ /reachvpls/i) {
		    $row .= "</tr>\n<tr><$td></td><$td>$c3</td>";
		    if($vals[3]) {
		        # class 3 = old silver
		        my @v = split(/\s+/, $vals[3]);
				if($minute) {
		            $cl3 = ($v[0]) ? $v[0] : 0;
		            $cl3_out = ($v[1]) ? $v[1] : 0;
		        } 
		        else { #daily
		            $cl3 = ($v[0]) ? $v[0] : 0;
		            $cl3_out = ($v[2]) ? $v[2] : 0;
		        }
		        foreach my $x (@v) {
					if($x !~ /^\d+/) { $x = "-"; }
		            $row .= "<$td>$x</td>";
		        }
		     } 
		     else {
				$cl3 = $cl3_out = 0;
		        if($minute) {
		                $row .= "<$td>-</td><$td>-</td>";
		        } else {
		                $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>"; 
		        }
		     }
		}
		
####################################################################################################
# CLASS 4
		if($costype =~ /reachvpls/i) {	
			$row .= "</tr>\n<tr><$td></td><$td>$c4</td>";
	        if($vals[4]) {
	            # class4
	            my @v = split(/\s+/, $vals[4]);
	            if($minute) {
	                    $cl4 = ($v[0]) ? $v[0] : 0;
	                    $cl4_out = ($v[1]) ? $v[1] : 0;
	            } 
	            else { #daily
	                    $cl4 = ($v[0]) ? $v[0] : 0;
	                    $cl4_out = ($v[2]) ? $v[2] : 0;
	            }
	 
	            #$cl3 = ($v[$numcol]) ? $v[$numcol] : 0;
	            foreach my $x (@v) {
					if($x !~ /^\d+/) { $x = "-"; }
	                $row .= "<$td>$x</td>";
	            }
	        } 
	        else {
	            $cl4 = $cl4_out = 0;
	            if($minute) {
	                    $row .= "<$td>-</td><$td>-</td>";
	            } else {
	                    $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>";
	            }
	        }
		}
		
####################################################################################################
# CLASS 5
		if($costype =~ /reachvpls/i) {	
	        $row .= "</tr>\n<tr><$td></td><$td>$c5</td>";
	        if($vals[5]) {                   
				# Gold
	            my @v = split(/\s+/, $vals[5]);
				if($minute) {
	                $cl5 = ($v[0]) ? $v[0] : 0;
	                $cl5_out = ($v[1]) ? $v[1] : 0;
	            } else { #daily
	                $cl5 = ($v[0]) ? $v[0] : 0;
	                $cl5_out = ($v[2]) ? $v[2] : 0;
	            }
	
	            foreach my $x (@v) {
					if($x !~ /^\d+/) { $x = "-"; }
	                $row .= "<$td>$x</td>";
	            }
	        } 
	        else {
				$cl5 = $cl5_out = 0;
	            if($minute) {
	                    $row .= "<$td>-</td><$td>-</td>";
	            } else {
	                    $row .= "<$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td><$td>-</td>"; 
	            }
	        }
	    }
	    
######  VLP - VPLS Start  ######

######  VLP - VPLS Stop  ######
  
		push @rpt, $row;
######  VLP - VPLS Start  ######		
		push @values, "$k\t$def\t$def_out\t$cl1\t$cl1_out\t$cl2\t$cl2_out\t$cl3\t$cl3_out\t$cl4\t$cl4_out\t$cl5\t$cl5_out\t";
######  VLP - VPLS Stop  ######
		#  -> now if not table - give need to split out these values

		#print "<P>YY ($k $def $cl3 $cl5)\n";
    }

    push @rpt, "</table>\n";
	open LOG_pr, ">/data1/tmp_log/tmplog_12_prad";
	#print LOG_pr "rptgen---->$rptgen\n";
	close LOG_pr;

	if($rptgen) {
		push @retvals, @rpt;
	} else {
        push @retvals, "<p class=$th>No report generated.</p>\n";
        push @retvals, "<p class=text>There was no data available. Please try other dates.</p>\n";
	}

	if($disp !~ /table/i) {
		#print FHU "\n in graph\n";
		#graph

		my $max = 0;
		open($out, ">$datafile");
####################################################################################################
# CLASS 1 & 2

	open LOG,">/data1/tmp_log/tmplog";
	#print LOG "values in values :@values \n\n ";
	#print LOG "costype :$costype\n\n";
        if($costype =~ /reachvpls/i) {
			foreach my $l (@values) {
				my @v = split(/\s+/, $l);
	#print LOG "v : @v \n";
                for my $id (1..12) {
					if(!$minute) {
                    	$v[$id] = int((($v[$id] * 80) / 864) + 0.5);
	#print LOG " in for and if minute loop : $v[$id] \n";
                    }       
                    if($v[$id] > $max) {
                    	$max = $v[$id];
	#print LOG " max : $max \n";
                    }
                }
				if(!$minute) {
                	$l = join "\t", @v;
                }
	#print LOG " l printed in fil: $l /n";
				print $out "$l\n";
			}
        }

####################################################################################################
		
		close($out);
        #print "Content-tye: image/gif\n\n";

		if($egress) {
			#print LOG " in egress if : code :$code svc:$svc datafile:$datafile, max :$max costype :$datafile \n";
			&gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "vpls", $max, "VPLS Egress", "",1, $costype);
		} 
		else {
            if ($data =~ /5minute/i) {
                    &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "vpls", $max, "", "", 1, $costype);
            } else {
                    &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile, "vpls", $max, "", "", "", $costype);
            }
		}
		return;
	}

	#print DEBUG1 "<P>Return values - (@retvals).\n";
close (DEBUG1);
    return @retvals;
}
         
# Reconcile gold, silver and default into one list
# Pre: $dtype, @default, @class3, @class5, display, $egress, additional cos...
# Post: %report
sub vpls_reconcile {
	#print FHU "inside vpls_reconcile \n";
######  VLP - VPLS Start  ######
	my($dtype, $type, $default, $class0out, $class3out, $class3in, $class5out, $class5in, $vtmout, $vtmin, $disp, $egress,
	$class1out, $class1in, $class2out, $class2in, $class4out, $class4in) = @_;
	#print FHU "inputs to vpls_reconcile : $dtype, $type, $default, $class0out, $class3out, $class3in, $class5out, $class5in, $vtmout, $vtmin, $disp, $egress,$class1out, $class1in, $class2out, $class2in, $class4out, $class4in \n";


######  VLP - VPLS Stop  ######
	#finding the max size of the arrays
	$maxarrsize = $#$default ;
	open (LOG,">/data1/tmp_log/logchand");
	#print LOG "maarrsize--i am here -->$maxarrsize\n";
	
	for($k=0;$k<=$#$default;$k++){
		$elem = (split(/\s+/, $$default[$k]))[0];
		$Firstkeys[$k] = $elem;
	}
	
		
	if($#$class0out > $maxarrsize){
		$maxarrsize = $#$class0out;
		for($k=0;$k<=$#$class0out;$k++){
			$elem = (split(/\s+/, $$class0out[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
	}
	if($#$class3out > $maxarrsize){
		$maxarrsize = $#$class3out;
		for($k=0;$k<=$#$class3out;$k++){
			$elem = (split(/\s+/, $$class3out[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
		
	}
	if($#$class3in > $maxarrsize){
		$maxarrsize = $#$class3in;
		for($k=0;$k<=$#$class3in;$k++){
			$elem = (split(/\s+/, $$class3in[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
		
	}
	if($#$class5out > $maxarrsize){
		$maxarrsize = $#$class5out;
		for($k=0;$k<=$#$class5out;$k++){
			$elem = (split(/\s+/, $$class5out[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
		
	}
	if($#$class5in > $maxarrsize){
		$maxarrsize = $#$class5in;
		for($k=0;$k<=$#$class5in;$k++){
			$elem = (split(/\s+/, $$class5in[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
		
	}
	if($#$class1out > $maxarrsize){
		$maxarrsize = $#$class1out;
		for($k=0;$k<=$#$class1out;$k++){
			$elem = (split(/\s+/, $$class1out[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
		
	}
	if($#$class1in > $maxarrsize){
		$maxarrsize = $#$class1in;
		for($k=0;$k<=$#$class1in;$k++){
			$elem = (split(/\s+/, $$class1in[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
		
	}
	if($#$class2out > $maxarrsize){
		$maxarrsize = $#$class2out;
		for($k=0;$k<=$#$class2out;$k++){
			$elem = (split(/\s+/, $$class2out[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
		
	}
	if($#$class2in > $maxarrsize){
		$maxarrsize = $#$class2in;
		for($k=0;$k<=$#$class2in;$k++){
			$elem = (split(/\s+/, $$class2in[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
		
	}
	if($#$class4out > $maxarrsize){
		$maxarrsize = $#$class4out;
		for($k=0;$k<=$#$class4out;$k++){
			$elem = (split(/\s+/, $$class4out[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
		
	}
	if($#$class4in > $maxarrsize){
		$maxarrsize = $#$class4in;
		for($k=0;$k<=$#$class4in;$k++){
			$elem = (split(/\s+/, $$class4in[$k]))[0];
			$Firstkeys[$k] = $elem;
		}
		
	}


	#print LOG123 "maxarrsize---->$maxarrsize\n";
	#print LOG123 "Firstkeys------>@Firstkeys\n";

my $count = 0;
my ($elem0,$elem0out,$elem3,$elem3in,$elem5,$elem5in,$elem1,$elem1in,$elem2,$elem2in,$elem4,$elem4in);
	#print LOG123 "fortesting array class2in---->@$class2in--------counts---->$#$class2in\n";
while($maxarrsize != -1){
	#print LOG123 "array element ----->$$class2in[$count]\n";
	
	$elem0 = (split(/\s+/, $$default[$count]))[0];
	$elem0out = (split(/\s+/, $$class0out[$count]))[0];
	$elem3 = (split(/\s+/, $$class3out[$count]))[0];
	$elem3in = (split(/\s+/, $$class3in[$count]))[0];
	$elem5 = (split(/\s+/, $$class5out[$count]))[0];
	$elem5in = (split(/\s+/, $$class5in[$count]))[0];
	$elem1 = (split(/\s+/, $$class1out[$count]))[0];
	$elem1in = (split(/\s+/, $$class1in[$count]))[0];
	$elem2 = (split(/\s+/, $$class2out[$count]))[0];
	$elem2in = (split(/\s+/, $$class2in[$count]))[0];
	$elem4 =(split(/\s+/, $$class4out[$count]))[0];
	$elem4in = (split(/\s+/, $$class4in[$count]))[0];

	#print LOG123 "elem inside the loop ---> elem0($elem0),elem0out($elem0out),elem3($elem3),elem3in($elem3in),elem5($elem5),elem5in($elem5in),elem1($elem1),elem1in($elem1in),elem2($elem2),elem2in($elem2in),elem4($elem4),elem4in($elem4in)\n";
	
	#print LOG123 "Firstkeys---->$Firstkeys[$count]\n";
	
	if($Firstkeys[$count] != $elem0){
		splice @$default,$count,0," ";
	}
	if($Firstkeys[$count] != $elem0out){
		splice @$class0out,$count,0," ";
	}
	if($Firstkeys[$count] != $elem3){
		splice @$class3out,$count,0," ";
	}
	if($Firstkeys[$count] != $elem3in){
		splice @$class3in,$count,0," ";
	}
	if($Firstkeys[$count] != $elem5){
		splice @$class5out,$count,0," ";
	}
	if($Firstkeys[$count] != $elem5in){
		splice @$class5in,$count,0," ";
	}
	if($Firstkeys[$count] != $elem1){
		splice @$class1out,$count,0," ";
	}
	if($Firstkeys[$count] != $elem1in){
		splice @$class1in,$count,0," ";
	}
	if($Firstkeys[$count] != $elem2){
		splice @$class2out,$count,0," ";
	}
	if($Firstkeys[$count] != $elem2in){
		splice @$class2in,$count,0," ";
	}
	if($Firstkeys[$count] != $elem4){
		splice @$class4out,$count,0," ";
	}
	if($Firstkeys[$count] != $elem4in){
		splice @$class4in,$count,0," ";
	}
	
	#print LOG123 "count-----$count\n";
	if($count == $maxarrsize ){
		last;
	}
	$count++;
}
#print LOG123 "fortesting array class2in after----->@$class2in--------countis--->$#$class2in\n";
#for( my $cnt=0; $cnt <= $maxarrsize;$cnt++){
#print LOG123 "array elements final default--->$$default[$cnt]\n";
#print LOG123 "array elements final elem0out--->$$class0out[$cnt]\n";
#print LOG123 "array elements final elem3--->$$class3out[$cnt]\n";
#print LOG123 "array elements final elem3in--->$$class3in[$cnt]\n";
#print LOG123 "array elements final elem5--->$$class5out[$cnt]\n";
#print LOG123 "array elements final elem5in--->$$class5in[$cnt]\n";
#print LOG123 "array elements final elem1--->$$class1out[$cnt]\n";
#print LOG123 "array elements final elem1in--->$$class1in[$cnt]\n";
#print LOG123 "array elements final elem2--->$$class2out[$cnt]\n";
#print LOG123 "array elements final elem2in--->$$class2in[$cnt]\n";
#print LOG123 "array elements final elem4--->$$class4out[$cnt]\n";
#print LOG123 "array elements final elem4in--->$$class4in[$cnt]\n";
#}



close LOG123;

	$dtype = ($dtype =~ /daily/i) ? 1 : 0;

	my ($values, %report);

	# First, do class0 - old default

#print "<P> PRE TYPE ($type)\n";
#print "<P> DEFAULT\n";
	##print "<P>DEFAULT ($#$default) (@$default)\n";
	if ($type == 1) {
    	$report = &vpls_exline($dtype, 1, $type, $default, \%report, $disp, $egress);
	} elsif ($type == 2) {
    	$report = &vpls_exline($dtype, 1, $type, $class0out, \%report, $disp, $egress, $default);
	}
	#print "<P>SILVER ($#$silver) (@$silver)\n";

#print "<P> CLASS1\n";
	# Second, do class1
    $report = &vpls_exline($dtype, 2, $type, $class1out, \$report, $disp, $egress, $class1in);

#print "<P> CLASS2\n";
	# Third, do class2
    $report = &vpls_exline($dtype, 3, $type, $class2out, \$report, $disp, $egress, $class2in);

#print "<P> Class 3\n";
	# Fourth, do class3
    $report = &vpls_exline($dtype, 4, $type, $class3out, \$report, $disp, $egress, $class3in);

	# Fifth, do class4
	$report = &vpls_exline($dtype, 5, $type, $class4out, \$report, $disp, $egress, $class4in);

	#print "<P>Class 5 ($#$class5) (@$class5)\n";
	
	# Sixth, do class5 
    $report = &vpls_exline($dtype, 6, $type, $class5out, \$report, $disp, $egress, $class5in);
######  VLP - VPLS Start  ######
    #print "<P>VLP Trans Mode ($#$vtm) (@$vtm)\n";
	
	# Seventh, do vtm 
    #$report = &vpls_exline($dtype, 7, $type, $vtmout, \$report, $disp, $egress, $vtmin);
######  VLP - VPLS Stop  ######
    return $report;
}

# Reconcile class5, class3 and default into one list
# Pre: dtype, start, default, %report, disp, egress
# Post: %report
sub vpls_exline {
	my ($dtype, $start, $type, $default, $report, $disp, $egress, $defaultin) = @_;
	open (LOG12,">/data1/tmp_log/log_pr_123");
	#print LOG12 "vpls_exline======>($dtype, $start, $default, $report)\n";
    my $tmp;

#print "<br>start=$start<br>";
#print "<P>default=@$default<br><br>";
#print "<P>defaultin=@$defaultin<br><br>";
	my $tmp_default = $default;
			
	if ($$default[0] =~ /FAIL/i || $$default[0] eq "") {
		$tmp_default = $defaultin;
	}
	
if ($$default[0] eq "" && $$defaultin[0] eq "") {
	#print LOG12 "returning since default is empty\n";
$tmp="";
for (my $l=0; $l<=$maxarrsize; $l++) {

        if($start == 1) {
            $report{$Firstkeys[$l]} = $tmp;
        }
        else {
            if(not exists $report{$Firstkeys[$l]}) {
                if($start == 2) {
                        $report{$Firstkeys[$l]} = "|$tmp";
                                }
                                elsif($start == 3) {
                                        $report{$Firstkeys[$l]} = "||$tmp";
                                }
                                elsif($start == 4) {
                                        $report{$Firstkeys[$l]} = "|||$tmp";
                                }
                                elsif($start == 5) {
                                        $report{$Firstkeys[$l]} = "||||$tmp";
                }
                elsif($start == 7) {
                        $report{$Firstkeys[$l]} = "||||||$tmp";
                }
                else {
                        $report{$Firstkeys[$l]} = "|||||$tmp";
                }
             }
             else {
                 $report{$Firstkeys[$l]} .= "|$tmp";
             }
        }
}
		return \%report;
	}
	
	if ($$default[0] eq "" && $$defaultin[0] eq "") {
		return \%report;
	}

	open (LOG12,">/data1/tmp_log/log_pr_123");

	for (my $l=0; $l<=$maxarrsize; $l++) {
		#print LOG12 "<P> PRE 1 ($start) ($l) ($#$tmp_default) ($$default[$l]) ($$defaultin[$l])\n";
		if ($start == 1) {
			if ($type == 1) {
			#	next if ($$default[$l] !~ /^\d+/);
			} 
			elsif ($type == 2) {
				if ($$default[$l] !~ /^\d+|^FAIL/ && $$defaultin[$l] !~ /^\d+|^FAIL/) {
				#print LOG12 "<P> NEXTING PRE ($l) ($#$tmp_default) ($$default[$l]) ($$defaultin[$l])\n";
				#print LOG12 "<P> default=@$default <P> defaultin=@$defaultin <P> --> nexting\n";
				#	next;
				}
			}
		} 
		else {
			#next if ($$default[$l] !~ /^\d+|^FAIL/ && $$defaultin[$l] !~ /^\d+|^FAIL/);
		}
		my (@v, @vin, @tmp_v);

		if ($$default[$l] =~ /^\d+/) {
            chomp($$default[$l]);
            @v = split(/\s+/, $$default[$l]);
		}
		if ((($start != 1) || (($start == 1) && ($type == 2))) && $$defaultin[$l] =~ /^\d+/) {
            chomp($$defaultin[$l]);
            @vin = split(/\s+/, $$defaultin[$l]);
		}
		@tmp_v = split(/\s+/, $$tmp_default[$l]);
		#print LOG12 "tmp_v  value here ----> @tmp_v\n";
        if($dtype) {
			if (($start == 1) && ($type == 1)) {
		    	$tmp = "$v[4]\t$v[5]\t$v[6]\t$v[8]\t$v[12]\t$v[14]";
			} 
			else {
				if ($$defaultin[$l] !~ /^\d+/) {
					$tmp = "$v[4]\t$v[5]\t$v[6]\t$v[14]\t$v[12]\t$v[10]";
				}
				if ($$default[$l] !~ /^\d+/) {
					$tmp = "$vin[4]\t$vin[5]\t$vin[6]\t$vin[14]\t$vin[12]\t$vin[10]";
				}
				if ($$defaultin[$l] =~ /^\d+/ && $$default[$l] =~ /^\d+/) {
		        	$tmp = "$vin[4]\t$vin[5]\t$v[6]\t";
					if ($v[14] >= $vin[14]) {
                    	$tmp .= "$v[14]\t";
                    } else {
                    	$tmp .= "$vin[14]\t";
                    }
 			#print LOG12 "tempv alue here ----> $tmp\n";

						if ($v[12] >= $vin[12]) {
						$tmp .= "$v[12]\t";
					} 
					else {
						$tmp .= "$vin[12]\t";
					}
					if ($v[10] >= $vin[10]) {
						$tmp .= $v[10];
					} 
					else {
						$tmp .= $vin[10];
					}
				}
			}
        } 
        else {
			if (($start == 1) && ($type == 1)) {
				$tmp = "$v[1]\t$v[2]"; # kbps in/out
			} else {
				$tmp = "$vin[1]\t$v[2]";
			}
        }

                #assume report does not exist
		#print LOG12 "($start) Time: $v[0].....tmp=$tmp\n";
		#print LOG12 "tmp valu---->$tmp----\n";
if($tmp !~ /\d+/){
		#print LOG12 "inside not digit\n";
$tmp="";
} 

my $hashkey;
if($v[0]){
	#print LOG12 "value is preesent so takin ghatr as hashkey\n";
$hashkey = $v[0];
}else{
	#print LOG12 "value is not present so taking from firstkeyes array\n";
$hashkey = $Firstkeys[$l];
}

	#print LOG12 "hashkey------->$hashkey\n";
 
	#print LOG12 "tmp valui after ---->$tmp----\n";
        if($start == 1) {
            $report{$hashkey} = $tmp;
        } 
        else {
            if(not exists $report{$hashkey}) {
		#print LOG12 "inside not exists::::$start:::::$Firstkeys[$l]\n";
            	if($start == 2) {
                	$report{$hashkey} = "|$tmp";
				} 
				elsif($start == 3) {
					$report{$hashkey} = "||$tmp";
				} 
				elsif($start == 4) {
					$report{$hashkey} = "|||$tmp";
				} 
				elsif($start == 5) {
					$report{$hashkey} = "||||$tmp";
                } 
		elsif($start == 7) {
			$report{$hashkey} = "||||||$tmp";
		}
                else {
                	$report{$hashkey} = "|||||$tmp";
                }
             } 
             else {
		#print LOG12 "inside else not exists\n";

             	 $report{$hashkey} .= "|$tmp";
             }
        }
		if($hashkey =~ /fail/i) {
			#print LOG12 " insert empty fields for all existing keys\n";
			foreach my $k (sort keys %report) {
				$report{$k} .= "|";
			}
		}
	}
while (($key, $value) = each(%report)){
     #print LOG12 "key------>".$key."</td>\n";
     #print LOG12  "value----->".$value."</td></tr>\n";
}

close LOG12;
    return \%report;
}
#################### VPLS TR END #####################

#++EVPL
#Chandini_EVPL
sub gen_trafficrptevpl {
        my($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $egress, $id) = @_;
	#print Raz "in gen_trafficrptevpl function:$start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $egress, $id \n\n";
        my (@vals, @classout, @default, @retvals, @no_class, @vtmin, @vtmout);
        my $files = ("$opshandle");
	#print Raz "in files opshandle : $files";
	$files =~ s/ /_/g;
	$files =~ s/\[/_/g;
	$files =~ s/\]/_/g;
	my $filename_classtype;

	CMS_DB::connect_to_database;

        # Next get cos labels - reachevpl
        my $cls = CMS_SERVICE_DB::get_costype_sid($opshandle."a");
	#print Raz "in cls form get_costype_sid :$cls \n\n";
        my $cyp = $$cls[0];
        my $costype = $$cyp[0];
	
	my $ser_cap = CMS_SERVICE_DB::get_ser_capacity($opshandle."a");
	#print Raz "From db :*********************** $ser_cap \n";
	my $ser_1 = $$ser_cap[0];
	#print Raz "ser_1   :$ser_1 \n";
	my $ser_cap_disp = $$ser_1[0]/1000;
        if($ser_cap_disp != '0'){
                $ser_cap_disp = $ser_cap_disp / 1000 ;
                $ser_cap_disp = sprintf("%.3f",$ser_cap_disp);
        }

	#print Raz "final display of service capacity :$ser_cap_disp\n";

	#print Raz "in cyp :$cyp in costype:$costype \n\n";
	if ($costype =~ "Standard"){
		$filename_classtype = "class_0";
	}
	elsif ($costype =~ "Premium") {
#		$filename_classtype = "class_5";
		$filename_classtype = "class_3";
	}
	else {
                $filename_classtype = "tpm";
        }
	 my $filename_head = "x-evpl-$filename_classtype";
	#print Raz " filename_head : $filename_head \n";
         my $filename_tail = "-".$files;
	 $files = $filename_head.$filename_tail;
	#print Raz "Final file name ################ :$files \n";

         CMS_DB::disconnect_from_database;
         my $fail = 0;
        my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                PeerPort => 4546,
                Proto => "tcp",
                Type => SOCK_STREAM) or $fail = 1;

               if($fail) {
                       push @retvals, "FAIL";
                       push @retvals, "<p class=header>Error</p><p class=text>";
                       push @retvals, "Cannot retrieve traffic report for $disp_rept.";
                       return @retvals;
               }
		#print Raz " start and end dates: $start $end=====data and files: $data $files \n";
	print $socket "$start $end table $data $files \n";	

        while(my $l = <$socket>) {
               push @vals, $l;
        }
        close($socket);

	

#my $ctrlfile=$tmplroot."/local/x-evpl-class_5-hkth50-hkgphkgrtokec123_sum";
#if(-e $ctrlfile) {
#                my $xx;
#                open($xx, $ctrlfile);
#                while(my $line = <$xx>) {
#                 push @vals,$line;
#                }
	#print Raz "data from files in vals :@vals \n";

#} else {
 #        push @retvals, "The file is not there #########";
#}
        # Re-group
        my (%timestamp, %tmp_default, %tmp_classout, %tmp_vtmin, %tmp_vtmout);
        my (@new_default, @new_classout, @new_vtmin, @new_vtmout);
	#print Raz "size---->$#vals\n";
                foreach my $val (@vals) {
			#print Raz "val--->$val\n";
			if($val == ""){next;}
                        my @seg = split /\s+/, $val;
			#print Raz " seg :@seg \n";
                        $tmp_default{$seg[0]} = $val;
			#print Raz " timestamp :tmp_default :$tmp_default{$seg[0]} \n";
                }

        foreach my $ts (sort {$a <=> $b} keys%tmp_default) {
		#print Raz "in froeach keys ts :$ts \n";
                if (!$ts)  { next; }
                push @new_default, $tmp_default{$ts};
        }
	#print Raz "new_default : @new_default\n";

        ## NOW MOVE THE NEW  COS to do something
        if($disp =~ /table/i) {
	#print Raz " In table if : \@new_default \n\n";
	#print Raz " disp : $disp, opshandle :$opshandle, code :$code, svc: $svc, data:$data,  costype :$costype  \n\n";
                        @retvals = &dogen_trafficrptevpl($disp, $opshandle, $code, $svc, $data, \@new_default, "", "", $costype,$ser_cap_disp,$start,$end);
        }
        else {
		#print Raz " In else not a table \n \n";
                        &dogen_trafficrptevpl($disp, $opshandle, $code, $svc, $data, \@new_default,$egress, $id, $costype,0,$start, $end);
                return;
        }
        return @retvals;
}
#Chandini_EVPL
sub dogen_trafficrptevpl {
	#print Raz " In dogen_trafficrptevpl \n\n";
        my ($disp, $opshandle, $code, $svc, $data, $default, $classout, $egress, $id, $minute, $rptgen, @retvals, $costype, $vtmout, $vtmin, $c, $vtm, $ser_cap_disp);
        ($disp, $opshandle, $code, $svc, $data, $default, $egress, $id, $costype, $ser_cap_disp,$start,$end) = @_;
        my $td = "td class=text";
        my $th = "td class=th align=center";
        push @retvals, "<p class=header>Traffic Report for $disp_rept</p><p class=text>\n";
	#print Raz " in retvals :@retvals \n\n";
        push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=21&et=$encet\">";
        push @retvals, "Click here to generate another report.</a>\n<p class=text>";
	#print Raz " in data : $data \n\n";
	my $data_disp = "Daily Summary";
        if($data !~ /daily/i) {
                $minute = 1;
                $data = "5 Minute Polling Data";
		$data_disp = "5 Minute Polling Data";
        }
	push @retvals, "<table cellpadding=4 cellspacing=5>";	
        push @retvals, "<tr><td class=text> Data Type: </td> <td class=text><b>$data_disp</b></td></tr>";

	if ($costype =~ "Standard" || $costype =~ "Premium") {
        	push @retvals, "<tr><td class=text> Service Grade: </td> <td class=text><b>$costype</b></td></tr>";
	}
#Chandinichange
	push @retvals, "<tr><td class=text>Service Capacity (Mbps): </td><td class=text><b>$ser_cap_disp</b></td></tr>";
	push @retvals, "</table>";
        push @retvals, "<p><hr><p class=text>\n";
	
	#print Raz "fail :@vals[0] \n";
	
	my $time_start = localtime ($start);
	#print Raz "startdate :$start \n";
	#print Raz " timestamp = $time_start \n"; 

	my ($d,$m,$y) = split /\//, $start;
	#my $dv1 = $d+31*$m+365*$y;
	my $dv1 = $y.$m.$d;
	my ($d1,$m1,$y1) = split /\//, $end;
	my $dv2 = $y1.$m1.$d1;
	#my $dv2 = $d1+31*$m1+365*$y1;
	#print Raz " dv1 : $dv1\n";
	#print Raz " dv2 : $dv2\n";



        if(@vals[0] =~ "FAIL" || ($dv1 > $dv2)) {
		#print Raz " inside rptgen\n\n\n\n\n";
                push @retvals, "<p class=header>No report generated.</p>\n";
                push @retvals, "<p class=text>There was no data available. Please try other dates.</p>\n";
                return @retvals;
        }
        my ($h, @rpt, $out_e, @values);
        my $datafile_e = "/tmp/evplgraph.data.$$.$id.traffic";
	#print Raz " file datafile :$datafile_e \n";
        if($minute) {


                push @rpt, "<table cellpadding=4 cellspacing=5 border=1 width=78%>";
                $h = "<tr><td class=th rowspan=2 align=center width=26%>Date (GMT)</td><td class=th colspan =2 align=center width=26%>Traffic Rate (Mbps) </td><td class=th colspan =2 align=center width=26%>Traffic Volume (MBytes)</td></tr>\n";
		$h .= "<tr><td class=th align=center width=13%>A-Z End</td><td class=th align=center width=13% >Z-A End</td><td class=th align=center width=13% >A-Z End</td><td class=th align=center width=13% >Z-A End</td></tr>\n";
        } else {
                push @rpt, "<table cellpadding=4 cellspacing=5 border=1 width=100%>";
                $h = "<tr><$th width=15% rowspan=3 align=center >Date (GMT)</td><$th colspan=2 width=16% rowspan=2 align=center >Traffic Volume (MBytes)</td><$th colspan=2 width=16% align=center rowspan=2>Use of port (%)</td><$th colspan=6 align=center >Traffic Rate (Mbps)</td></tr>\n";
                $h .= "<tr><$th colspan=2 width=16% align=center >5 min Peak </td><$th colspan=2 width=16% align=center  >1 hr Peak </td><$th colspan=2 width=16% >24 hr Peak </td></tr>\n";
                $h .= "<tr><$th align=center >A-Z End</td><$th align=center >Z-A End</td><$th align=center >A-Z End</td><$th align=center >Z-A End</td><$th  align=center >A-Z End</td><$th align=center >Z-A End</td><$th align=center >A-Z End</td><$th align=center >Z-A End</td><$th align=center >A-Z End</td><$th align=center >Z-A End</td></tr>\n";
        }
        push @rpt, $h;
	#print Raz " %%%%%%%%%%%%%%%%%%% in rpt: @rpt\n\n";
	#print Raz " #################before foreach : @$default \n\n";
	
	my $row;
        my $tot_az = 0;
        my $tot_za = 0;

	if($minute) {
	foreach my $val (@$default) {
		my $row = "";
		my ($row1,$row2,$row3,$row4) = 0 ;
                my @seg = split /\s+/, $val;
		#print Raz "******************** seg in minute :@seg \n";
                        my $first_time = strftime "%a %b %e %Y %H:%M:%S", localtime(@seg[0]);
$rptgen =1;
                        $row .="<tr>";
                        $row .="<$td>$first_time</td>";
		$row1 = @seg[1];
		if($row1 != '-' || $row1 != '0'){
		$row1 = $row1 / 1000 ;
		$row1 = sprintf("%.3f",$row1);	
		}
                        $row .="<$td>$row1</td>";
		$row2 = @seg[2];
		if($row2 != '-' || $row2 != '0'){
		$row2 = $row2 /1000;
		$row2 = sprintf("%.3f",$row2); 	
		}
                        $row .="<$td>$row2</td>";
		$row3 = @seg[3];
		if($row3 != '-' || $row3 != '0'){
		$row3 = $row3 / 1000000;
		$row3 = sprintf("%.3f",$row3); 
		}
                        $row .="<$td>$row3</td>";
		$row4 = @seg[4];
		if($row4 != '-' || $row4 != '0'){
		$row4 = $row4 / 1000000;
		$row4 =sprintf("%.3f",$row4);
		}
                        $row .="<$td>$row4</td>";
		 	$row .="</tr>";
                        $tot_az = $tot_az + $row3;
			$tot_az = sprintf("%.3f",$tot_az);
                        $tot_za = $tot_za + $row4;
			$tot_za = sprintf("%.3f",$tot_za);
                        push @rpt, $row;
                }
	}
	else {
		#print Raz "size---->$#$default\n";
	foreach my $val (@$default) {
		my $row = "";
		my ($row8 , $row9 , $row10 , $row11 , $row12 , $row13)= 0;
                my @seg = split /\s+/, $val;
		#print Raz "******************** seg :@seg \n";
			my $first_time = strftime "%a %b %e %Y", localtime(@seg[0]);
$rptgen =1;
			$row .="<tr>";
                        $row .="<$td>$first_time</td>";
                        $row .="<$td>@seg[4]</td>";
                        $row .="<$td>@seg[5]</td>";
                        $row .="<$td>@seg[6]</td>";
                        $row .="<$td>@seg[7]</td>";
                $row8 = @seg[8];
		if($row8 != '-' || $row8 != '0'){
                $row8 = $row8 / 1000;
                $row8 = sprintf("%.3f",$row8);
		}
                        $row .="<$td>$row8</td>";
		$row9 = @seg[9];
		if($row9 != '-' || $row9 != '0'){
		$row9 = $row9 / 1000;
		$row9 = sprintf("%.3f",$row9);
		}
                        $row .="<$td>$row9</td>";
		$row10 = @seg[10];
		if($row10 != '-' || $row10 != '0'){
		$row10 = $row10 / 1000;
		$row10 = sprintf("%.3f",$row10);
		}
                        $row .="<$td>$row10</td>";
		$row11 = @seg[11];
		if($row11 != '-' || $row11 != '0'){
		$row11 = $row11 / 1000;
		$row11 = sprintf("%.3f",$row11);
		}
                        $row .="<$td>$row11</td>";
		$row12 = @seg[12];
		if($row12 != '-' || $row12 != '0'){
		$row12 = $row12 / 1000;
		$row12 = sprintf("%.3f",$row12);
		}
                        $row .="<$td>$row12</td>";
		$row13 = @seg[13];
		if($row13 != '-' || $row13 != '0'){
		$row13 = $row13 / 1000;
		$row13 = sprintf("%.3f",$row13);	
		}
                        $row .="<$td>$row13</td>";
                        $row .="</tr>";
			$tot_az = $tot_az + @seg[4];
			$tot_za = $tot_za + @seg[5];
			push @rpt, $row;
		#print Raz "******************** row :$row \n";
		}
	}


        push @rpt, "</table>\n";
#	push @retvals, @rpt;
		push @rpt, "<p class=text>Total Traffic Volume A-Z End: $tot_az MBytes</p> \n";
        	push @rpt, "<p class=text>Total Traffic Volume Z-A End: $tot_za MBytes</p> \n";

	        if($rptgen) {
                push @retvals, @rpt;
        } else {
                push @retvals, "<p >No report generated.</p>\n";
                push @retvals, "<p class=text>There was no data available. Please try other dates.</p>\n";
        }
	#print Raz "******************** before graph if $datafile_e\n";
	
        if($disp !~ /table/i) {
		#print Raz "******************  INSide graph if @$default\n\n\n";
                my $max = 0;
		my $prev_timestamp =0;	
		my $curr_timestamp=0;
		my $time_diff = 0;
		my @temp;
##daily
	my $max_daily =0;
	my $prev_timestamp_daily =0;
	my $curr_timestamp_daily =0;
	my $time_diff_daily =0;
	my @temp_daily;
##daily
                open($out_e, ">$datafile_e");
                  foreach my $l (@$default) {
                     my @v = split(/\s+/, $l);
		 #print Raz  "vv-------->@v====>$v[4]\n";
		     my @v_temp =(); 	
			if(!$minute){
			   push(@v_temp,$v[0]);
			   push(@v_temp,$v[4]);
			   push(@v_temp,$v[5]);
				push(@v_temp,$v[10]);
				push(@v_temp,$v[11]);
			}else{
                           $v_temp[0]=$v[0];
                           $v_temp[1]=$v[1];
                           $v_temp[2]=$v[2];
			}	
		#print Raz  "temp--->@v_temp\n";
#Chandinichange
                for my $id (1..2) {
                     if(!$minute ) {
			if($v_temp[$id] != '0'){
                        $v_temp[$id] = ($v_temp[$id] * 8) / 86400 ;
			$v_temp[$id] = sprintf("%.3f",$v_temp[$id]);
		        }
               	     }     
		     else{
			if($v_temp[$id] != '0'){
			$v_temp[$id] = $v_temp[$id] / 1000 ;
			$v_temp[$id] = sprintf("%.3f",$v_temp[$id]);
			}
		     }
                    if($v_temp[$id] > $max) {
		
                        $max = $v_temp[$id];
                    }
		
                }
		
		for my $id (3..4) {
                     if(!$minute ) {
			if($v_temp[$id] != '0'){
                        $v_temp[$id] = $v_temp[$id] / 1000;
			$v_temp[$id] = sprintf("%.3f",$v_temp[$id]);
			}
		     }
		}

		if($minute){
                #print Raz "curr_timestamp---->$curr_timestamp---->$v_temp[0]\n";
                $curr_timestamp = $v_temp[0];
                $time_diff = $curr_timestamp - $prev_timestamp;

                if($prev_timestamp !=0 && $time_diff > 300){
                        #print Raz "inside missing row --->$curr_timestamp----->$time_diff\n";
                        for(my $lp=1;$lp <$time_diff/300;$lp++){
                           $temp[0]=$prev_timestamp+300;
                           $temp[1]=0;
                           $temp[2]=0;
                           $l = join "\t", @temp;
                        #print Raz "entering into file ----> $l\n";
                           print $out_e "$l\n";
                           $prev_timestamp=$temp[0];
                        }

                }
                $prev_timestamp = $curr_timestamp;
		}
#daily
	else{
	 	#print Raz "curr_timestamp_dialy---->$curr_timestamp_daily---->$v_temp[0]\n ----->prev_timestamp_daily =$prev_timestamp_daily";
                $curr_timestamp_daily = $v_temp[0];
                $time_diff_daily = $curr_timestamp_daily - $prev_timestamp_daily;
                if($prev_timestamp_daily !=0 && $time_diff_daily > 86400){
                        #print Raz "inside missing row --->$curr_timestamp_daily----->$prev_timestamp_daily----->$time_diff_daily\n";
                        for(my $lp=1;$lp <$time_diff_daily/86400;$lp++){
                           $temp_daily[0]=$prev_timestamp_daily+86400;
                           $temp_daily[1]=0;
                           $temp_daily[2]=0;
			   $temp_daily[3]=0;
			   $temp_daily[4]=0;
                           $l = join "\t", @temp_daily;
                        #print Raz "entering into file ----> $l\n";
                           print $out_e "$l\n";
                           $prev_timestamp_daily=$temp_daily[0];
                        }

                }
                $prev_timestamp_daily = $curr_timestamp_daily;
	}
 #daily

                        $l = join "\t", @v_temp;
				$l =~ s/-/0/g;
                        print $out_e "$l\n";
			
                }
                close($out_e);
		#print Raz " in egress :$egress------> $max\n";
                if($egress) {
			#print Raz " inside if egress \n";
                        &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile_e, "evpl", $max, "EVPLEgress", "",1, $costype);
                }
                else {
			#print Raz " in else fo egress\n";
			#print Raz " data :$data\n";
                        if ($data =~ /5 Minute Polling Data/i) {
				#print Raz "in if to call gen_atmgraph :start:$start, end:$end disp :$disp data :$data opshandle :$opshandle code :$code svc :$svc datafile:$datafile_e max:$max \n\n";
                                &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile_e, "evpl", $max, "", "", 1, $costype);
                        } else {
				#print Raz "in if to call gen_atmgraph :start:$start, end:$end disp :$disp data :$data opshandle :$opshandle code :$code svc :$svc datafile:$datafile max:$max \n\n";
                                &gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile_e, "evpl", $max, "", "", "", $costype);
                        }
                }
                return;
        }
        return @retvals;
}

#Chandini_EVPL


#EPL
#prashant_EPL
sub gen_trafficrptepl {
        my($start, $end, $disp, $data, $opshandle, $code, $svc, $remote, $accno, $id) = @_;

        my (@vals, @classout, @default, @retvals, @no_class, @vtmin, @vtmout);
        my $files = ("$opshandle");
        $files =~ s/ /_/g;
        $files =~ s/\[/_/g;
        $files =~ s/\]/_/g;

	my $temp_svc = $opshandle;
	$temp_svc =~ s/\s+//g;
	$temp_svc = substr $temp_svc,5;

	CMS_DB::connect_to_database;

        my $ser_cap_disp=0;
	
	#If it is master service, get the sum of bandwidths of all services under it
	#Else get the bandwidth of the service
	if ($temp_svc =~ /^emm/i)
	{
        	my @test_svc = CMS_SERVICE_DB::get_master_service_bw($opshandle);
        	$ser_cap_disp = $test_svc[0]/1000;
	}
	else
	{
        	my $ser_cap = CMS_SERVICE_DB::get_service_bw($opshandle);
        	my $ser_1 = $$ser_cap[0];
        	$ser_cap_disp = $$ser_1[0]/1000;
	}
        CMS_DB::disconnect_from_database;
        
	if($ser_cap_disp != '0'){
                $ser_cap_disp = $ser_cap_disp / 1000 ;
        }

	
	#Convert Local time to Unix time
	my @start_time = split(/\//,$start);
	my @end_time = split(/\//,$end);

	my $start_unix = timelocal(0,0,0,$start_time[1],$start_time[0]-1,$start_time[2]);
	my $end_unix = timelocal(59,59,23,$end_time[1],$end_time[0]-1,$end_time[2]);

	my(%mths) = ("01", "Jan", "02", "Feb", "03", "Mar", "04", "Apr", "05", "May", "06", "Jun", "07", "Jul", "08",
                        "Aug", "09", "Sep", "10", "Oct", "11", "Nov", "12", "Dec");
	my $start_time_print = $start_time[1]." ".$mths{$start_time[0]}." ".$start_time[2];
	my $end_time_print = $end_time[1]." ".$mths{$end_time[0]}." ".$end_time[2];

        $filename_head = "x-epl-";
        my $filename_tail = $files;
        $files = $filename_head.$filename_tail;
	chdir($dbroot);
        chdir("EPL");

	my ($h, @rpt, $out_e, @values);

        if ($data =~ /daily/i) {
                chdir("daily");
        }
	else {
                chdir("15min");
	}

	open (READFILE, $files);
	my @tempArray;

	#Read only those data values 
	#from file which are between those start & end dates
        while(my $l = <READFILE>)
        {
                @tempArray= split /\s+/, $l;
		if(($tempArray[0] >= $start_unix) && ($tempArray[0] <= $end_unix))	
		{
                        push @vals, $l;
                }
	}
        close(READFILE);

        # Re-group
        my (%timestamp, %tmp_default, %tmp_classout, %tmp_vtmin, %tmp_vtmout);
        my (@new_default, @new_classout, @new_vtmin, @new_vtmout);
        
	foreach my $val (@vals) {
        	if($val == ""){next;}
                my @seg = split /\s+/, $val;
                $tmp_default{$seg[0]} = $val;
        }

        foreach my $ts (sort {$a <=> $b} keys%tmp_default) {
                if (!$ts)  { next; }
                push @new_default, $tmp_default{$ts};
        }

        if($disp =~ /table/i) {
                        @retvals = &dogen_trafficrptepl($disp, $opshandle, $code, $svc, $data, \@new_default, "", $costype,$ser_cap_disp,$start,$end,$accno,$start_time_print, $end_time_print);
        }
        else {
                        &dogen_trafficrptepl($disp, $opshandle, $code, $svc, $data, \@new_default, $id, $costype,0,$start, $end, $accno, $start_time_print, $end_time_print);
                return;
        }
        return @retvals;

}


sub dogen_trafficrptepl {
        
	my ($disp, $opshandle, $code, $svc, $data, $default, $id, $minute, $rptgen, @retvals, $costype, $vtmout, $vtmin, $c, $vtm, $ser_cap_disp, $accno, $start_time_print, $end_time_print);
	($disp, $opshandle, $code, $svc, $data, $default, $id, $costype, $ser_cap_disp,$start,$end, $accno, $start_time_print, $end_time_print) = @_;


	CMS_DB::connect_to_database;
	my @job_note_ref = CMS_SERVICE_DB::get_job_notes($opshandle);
	my @templates = CMS_SERVICE_DB::get_template_for_account($accno);
	CMS_DB::disconnect_from_database;

	my $aend_city = "";
	my $zend_city = "";
	my @aend_jobnote;
	my @zend_jobnote;
	my $temp = "";

        my $temp_svc = $opshandle;
        $temp_svc =~ s/\s+//g;
        $temp_svc = substr $temp_svc,5;

	if($job_note_ref[0])
	{
        	@aend_jobnote = split(/_/,$job_note_ref[0]);
        	$aend_city = $aend_jobnote[1];
		
		#PoP code shouldnt be displayed for telstra template
        	if ($templates[0] =~ /telstra/i) {
               		$aend_jobnote[2]= "";
        	}
        	else {
                	$temp = "[".uc($aend_jobnote[2])."]";
                	$aend_jobnote[2] = $temp;
        	}
	}

	#For master service Z-end location details will be blank
	if($job_note_ref[1]  && (!($temp_svc =~ /^emm/i)))
	{
		@zend_jobnote = split(/_/,$job_note_ref[1]);
		$zend_city = $zend_jobnote[1]; 
        
		#PoP code shouldnt be displayed for telstra template
        	if ($templates[0] =~ /telstra/i) {
                	$zend_jobnote[2]= "";
        	}
        	else {
			$temp = "[".uc($zend_jobnote[2])."]";
			$zend_jobnote[2] = $temp;
		}
	}

	open(CONFIG, $cityCode_cfg);	
	
	#Derive the country name  from country name by referring config file
	while (chomp(my $line = <CONFIG>)) 
	{
		if ((!$line) || ($line =~ /^\#/))  { next; }

		my @tempArray = split(/\t/,$line);	
		
		if (($aend_city ne "") && (lc($aend_jobnote[1]) eq lc($tempArray[1])))
		{
			#Get the corresponding countryname
			$aend_city = $tempArray[0];
		}		
		if (($zend_city ne "") && (lc($zend_jobnote[1]) eq lc($tempArray[1])))
                {
                        #Get the corresponding countryname
                        $zend_city = $tempArray[0];
                }

	}
close(CONFIG);

        my $td = "td class=text";
        my $th = "td class=th align=center";
        push @retvals, "<p class=header>Traffic Report for $disp_rept</p><p class=text>\n";
        push @retvals, "<a class=text href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&svc=$svc&level=21&et=$encet\">";
        push @retvals, "Click here to generate another report.</a>\n<p class=text>";
	my $data_disp = "Daily Summary";
        
	if($data !~ /daily/i) {
                $minute = 1;
                $data = "15 Minute Polling Data";
		$data_disp = "15 Minute Polling Data";
        }

	push @retvals, "<table cellpadding=4 cellspacing=5>";	
	push @retvals, "<tr><td class=text> Report Period: </td><td class=text><b>$start_time_print</b> to <b>$end_time_print</b></td></tr>";
        push @retvals, "<tr><td class=text> Data Type: </td> <td class=text><b>$data_disp</b></td></tr>";

	push @retvals, "<tr><td class=text>A-end location: </td><td class=text><b>$aend_city $aend_jobnote[2]</b></td></tr>";
	push @retvals, "<tr><td class=text>Z-end location: </td><td class=text><b>$zend_city $zend_jobnote[2]</b></td></tr>";
	push @retvals, "<tr><td class=text>Service Capacity (Mbps): </td><td class=text><b>$ser_cap_disp</b></td></tr>";
	push @retvals, "</table>";
        push @retvals, "<p><hr><p class=text>\n";
	
	
	my ($m,$d,$y) = split /\//, $start;
	my $dv1 = $y.$m.$d;
	my ($m1,$d1,$y1) = split /\//, $end;
	my $dv2 = $y1.$m1.$d1;

        if(@vals[0] =~ "FAIL" || ($dv1 > $dv2)) {
                push @retvals, "<p class=header>No report generated.</p>\n";
                push @retvals, "<p class=text>There was no data available. Please try other dates.</p>\n";
                return @retvals;
        }
        my ($h, @rpt, $out_e, @values);
        my $datafile_e = "/tmp/eplgraph.data.$$.$id.traffic";
	if($minute) 
	{

                push @rpt, "<table cellpadding=4 cellspacing=5 border=1 width=78%>";
                $h = "<tr><td class=th rowspan=2 align=center width=26%>Date (GMT)</td><td class=th colspan =2 align=center width=26%>Traffic Volume (MBytes)</td><td class=th colspan =2 align=center width=26%>Use of Service (%)</td><td class=th colspan =2 align=center width=16%>Traffic Rate (Mbps) </td></tr>\n";
		$h .= "<tr><td class=th align=center width=13%>A-Z End</td><td class=th align=center width=13% >Z-A End</td><td class=th align=center width=13% >A-Z End</td><td class=th align=center width=13% >Z-A End</td><td class=th align=center width=13% >A-Z End</td><td class=th align=center width=13% >Z-A End</td></tr>\n";
        } 
	else 
	{
                push @rpt, "<table cellpadding=4 cellspacing=5 border=1 width=100%>";
                $h = "<tr><$th width=20% rowspan=3 align=center >Date (GMT)</td><$th colspan=2 width=20% rowspan=2 align=center >Traffic Volume (MBytes)</td><$th colspan=2 width=20% align=center rowspan=2>Use of Service (%)</td><$th colspan=4 align=center >Traffic Rate (Mbps)</td></tr>\n";
                $h .= "<tr><$th colspan=2 width=20% align=center  >1 hr Peak </td><$th colspan=2 width=20% >24 hr Peak </td></tr>\n";
                $h .= "<tr><$th align=center >A-Z End</td><$th align=center >Z-A End</td><$th align=center >A-Z End</td><$th align=center >Z-A End</td><$th  align=center >A-Z End</td><$th align=center >Z-A End</td><$th align=center >A-Z End</td><$th align=center >Z-A End</td></tr>\n";
        }

        push @rpt, $h;
	
	my $row;
        my $tot_az = 0;
        my $tot_za = 0;

			$rptgen =0;
	if($minute) 
	{
		foreach my $val (@$default) 
		{
			my $row = "";
			my ($row1,$row2,$row3,$row4) = 0 ;
                	my @seg = split /\s+/, $val;

                	my $first_time = strftime "%a %b %e %Y %H:%M:%S", localtime(@seg[0]);
			$rptgen =1;
               
	      		$row .="<tr>";
                	$row .="<$td>$first_time</td>";
		
                	$row .="<$td>@seg[1]</td>";
                	$row .="<$td>@seg[2]</td>";

			#Putting % utilisation details
                	$row .="<$td>@seg[3]</td>";
                	$row .="<$td>@seg[4]</td>";
                	
			$row .="<$td>@seg[5]</td>";
               		$row .="<$td>@seg[6]</td>";

			$row .="</tr>";

                	if(@seg[1] != '-' || @seg[1] != '0'){
                		$tot_az = $tot_az + @seg[1];
				$tot_az = sprintf("%.3f",$tot_az);
			}
                	if(@seg[2] != '-' || @seg[2] != '0'){
        	        	$tot_za = $tot_za + @seg[2];
				$tot_za = sprintf("%.3f",$tot_za);
			}
                	push @rpt, $row;
            	}
	}
	
	else 
	{
	 	foreach my $val (@$default) 
		{
			my $row = "";
			my ($row8 , $row9 , $row10 , $row11 , $row12 , $row13)= 0;
			$rptgen =1;
		
			my @seg = split /\s+/, $val;
			
			my $first_time = strftime "%a %b %e %Y", localtime(@seg[0]);
			$row .="<tr>";
                	$row .="<$td>$first_time</td>";
               		$row .="<$td>@seg[3]</td>";
                	$row .="<$td>@seg[4]</td>";
                	$row .="<$td>@seg[5]</td>";
                	$row .="<$td>@seg[6]</td>";
			$row .="<$td>@seg[7]</td>";
                	$row .="<$td>@seg[8]</td>";
                	$row .="<$td>@seg[9]</td>";
                	$row .="<$td>@seg[10]</td>";
                	$row .="</tr>";
		
			if(@seg[3] != '-' || @seg[3] != '0'){	
				$tot_az = $tot_az + @seg[3];
			}
			if(@seg[4] != '-' || @seg[4] != '0'){	
				$tot_za = $tot_za + @seg[4];
			}
			push @rpt, $row;
	    	}
	}


        push @rpt, "</table>\n";
	push @rpt, "<p class=text>Total Traffic Volume A-Z End: $tot_az MBytes</p> \n";
       	push @rpt, "<p class=text>Total Traffic Volume Z-A End: $tot_za MBytes</p> \n";

        if($rptgen) {
                push @retvals, @rpt;
        } 
	else {
                push @retvals, "<p >No report generated.</p>\n";
                push @retvals, "<p class=text>There was no data available. Please try other dates.</p>\n";

		return @retvals;
        }
        if($disp !~ /table/i) 
	{
                my $max = 0;
		my $prev_timestamp =0;	
		my $curr_timestamp=0;
		my $time_diff = 0;
		my @temp;
		

		##daily
                open($out_e, ">$datafile_e");
                
		foreach my $l (@$default) 
		{
                	my @v = split(/\s+/, $l);
		     	my @v_temp =(); 	
	
			if($minute)
			{
                           	$v_temp[0]=$v[0];
                           	$v_temp[1]=$v[5];
                           	$v_temp[2]=$v[6];
                		
			}
			else  #daily
			{
		        	#Convert Local time to Unix time
        			my @split_time = split(/\//,$v[1]);
        			my $unix_time  = timelocal(0,0,0,$split_time[0],$split_time[1]-1,$split_time[2]);
	
				push(@v_temp,$unix_time);
			   	push(@v_temp,$v[11]);
			   	push(@v_temp,$v[12]);
			   	push(@v_temp,$v[7]);
			   	push(@v_temp,$v[8]);
                
			}

                        $l = join "\t", @v_temp;
			$l =~ s/-/0/g;
                        print $out_e "$l\n";
			
                }
                close($out_e);
                        
		if ($data =~ /15 Minute Polling Data/i) 
		{
                	&gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile_e, "epl", $max, "", "", 1, $costype);
                }
		else 
		{
                	&gen_atmgraph($start, $end, $disp, $data, $opshandle, $code, $svc, $datafile_e, "epl", $max, "", "", "", $costype);
                }
                
		return;
	}
        return @retvals;
}




# Outage reports
# Pre: uid, code, accno, tmplroot, fnn, opshandle, encryp_svc
sub screen_outage {
        my ($uid, $code, $accno, $tmplroot, $fnn, $ohandle, $svc) = @_;

        my ($outagefound, $outagerow, @alloutages, $outages);
        push @alloutages, "<tr><td class=thx width=10%>Ticket Number</td><td class=thx width=10%>Start (GMT)</td><td class
=thx width=10%>End (GMT)</td>";
        push @alloutages, "<td class=thx width=20%>Item</td><td class=thx>Details</td></tr>\n";

        eval {
                $outages = CMS_OLSS::get_current_outages();
        };

        #print "Content-type: text/json\n\n";
        #print "<P>DEBUG: ($@) ($outages) ($$outages[0][0]) ($#{@$outages}) ($#{$$outages[0]})\n";

        if($#{@$outages} >= 0) {
                $outagefound = ($$outages[0][0] =~ /^\d*$/) ? 1 : 0;

                for my $i (0..$#{@$outages}) {
                        next if($$outages[$i][0] !~ /^\d*$/); # Expect first elem to be ticket no

                        $outagerow = "<tr>";

                        for my $j (0..$#{$$outages[$i]}) {
                                if($$outages[$i][$j] =~ /\^\^/) {
                                        chomp($$outages[$i][$j]);
                                        $$outages[$i][$j] =~ s/\^\^$//;
                                        my $tmp = "<ul><li>";
                                        my $tmpx = $$outages[$i][$j];
                                        $tmpx =~ s/\^\^/\<li\>/;
                                        $tmp .= $tmpx."</ul>\n";
                                        $$outages[$i][$j] = $tmp;
                                }
                                $outagerow .= "\t\t<td class=text>$$outages[$i][$j]</td>\n";
                        }

                        $outagerow .= "</tr>\n";
                        push @alloutages, $outagerow;
                }
        }


        my $ctrlfile = $tmplroot."/local/outage.htm";
        my @retvals = ();

        my $svclabel = &get_svclabel($accno, $ohandle, 0);
        $ohandle = ($svclabel) ? "$svclabel ($ohandle)" : $ohandle;

        if(-e $ctrlfile) {
                my $xx;
                open($xx, $ctrlfile);
                while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*SERVICE\*/$ohandle/g;
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/211/g;
                        $line =~ s/\*FNN\*/$fnn/g;
                        $line =~ s/\*SVCCODE\*/$svc/g;
                        $line =~ s/\*ETPRODUCT\*/$encet/g;

                        if($line =~ /\*TABLEOUTAGE\*/) {
                                if($outagefound) {
                                        push @retvals, @alloutages;
                                } else {
                                        push @retvals, "No current outage notices.";
                                }
                        } else {
                                push @retvals, $line;
                        }
               }
                close($xx);
        } else {
                push @retvals, "Cannot generate outage reports page";
        } 
        return @retvals;
                         
}


# Generate network performance reports
# Pre: uid, code, accno, tmplroot, fnn, opshandle, encryp_svc
sub screen_netperf {
        my ($uid, $code, $accno, $tmplroot, $rpttype, $measurement, $request_time, $fnn, $ohandle, $svc) = @_;
        my $ctrlfile = $tmplroot."/local/netperf.htm";
        my @retvals = ();
	my @netperf = ();
	my $tabmenu = 0;

        my $date = gmtime(time);
        $date .= " GMT";
	my $d = $date;
       
        my $svclabel = &get_svclabel($accno, $ohandle, 0);
        $ohandle = ($svclabel) ? "$svclabel ($ohandle)" : $ohandle;
 
        $date = "<p class=text>Report generated on $date</p><p class=text>";

        if(-e $ctrlfile) {
		my $xx;
                open($xx, $ctrlfile);
                 while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*SERVICE\*/$ohandle/g;
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/211/g;
                        $line =~ s/\*FNN\*/$fnn/g;
                        $line =~ s/\*SVCCODE\*/$svc/g;
                        $line =~ s/\*ETPRODUCT\*/$encet/g;

			if($line =~ s/\*TABMENU\*//g) {
				$tabmenu = 1;
			}

			if($line =~ /\*DATA\*/) {
				if ($tabmenu =~ /^1$/) {
					my $ws = &get_wholesaler($accno);
					if ($ws =~ /Telstra/i ) {
						if ($rpttype =~ /^0$/) {	# Telstra (GIA, GIAE, GIAE+)
							push @retvals, "<div id=\"tabmenu\">\n";
						        push @retvals, "<a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=0\"><span class=\"curposr\">GIA</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=1\"><span>GIA Economy</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=2\"><span>GIA Economy Plus</span></a>\n";
							push @retvals, "</div>\n";
							push @retvals, "<div id=\"tabmenuborder\"></div>\n";
							push @retvals, &screen_netperf_matrix($code, $tmplroot, $rpttype, $measurement, $request_time, "GIA");
						} elsif ($rpttype =~ /^1$/) {
							push @retvals, "<div id=\"tabmenu\">\n";
						        push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=0\"><span>GIA</span></a><a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=1\"><span class=\"curposr\">GIA Economy</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=2\"><span>GIA Economy Plus</span></a>\n";
							push @retvals, "</div>\n";
							push @retvals, "<div id=\"tabmenuborder\"></div>\n";
							push @retvals, &screen_netperf_matrix($code, $tmplroot, $rpttype, $measurement, $request_time, "GIAE");
						} elsif ($rpttype =~ /^2$/) {
							push @retvals, "<div id=\"tabmenu\">\n";
						        push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=0\"><span>GIA</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=1\"><span>GIA Economy</span></a><a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=2\"><span class=\"curposr\">GIA Economy Plus</span></a>\n";
							push @retvals, "</div>\n";
							push @retvals, "<div id=\"tabmenuborder\"></div>\n";
							push @retvals, &screen_netperf_matrix($code, $tmplroot, $rpttype, $measurement, $request_time, "GIAEP");
						}
					} elsif ($ws =~ /PCCW/i) {
						if ($rpttype =~ /^0$/) {	# PCCW (GIA, GIAE, GIAE+)
							push @retvals, "<div id=\"tabmenu\">\n";
						        push @retvals, "<a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=0\"><span class=\"curposr\">GIA</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=1\"><span>GIA Economy</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=2\"><span>GIA Economy Plus</span></a>\n";
							push @retvals, "</div>\n";
							push @retvals, "<div id=\"tabmenuborder\"></div>\n";
							push @retvals, $date;
							push @retvals, &get_netperfdata_slarpt("GIA");
						} elsif ($rpttype =~ /^1$/) {
							push @retvals, "<div id=\"tabmenu\">\n";
						        push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=0\"><span>GIA</span></a><a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=1\"><span class=\"curposr\">GIA Economy</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=2\"><span>GIA Economy Plus</span></a>\n";
							push @retvals, "</div>\n";
							push @retvals, "<div id=\"tabmenuborder\"></div>\n";
							push @retvals, $date;
							push @retvals, &get_netperfdata_slarpt("GIAE");
						} elsif ($rpttype =~ /^2$/) {
							push @retvals, "<div id=\"tabmenu\">\n";
						        push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=0\"><span>GIA</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=1\"><span>GIA Economy</span></a><a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=2\"><span class=\"curposr\">GIA Economy Plus</span></a>\n";
							push @retvals, "</div>\n";
							push @retvals, "<div id=\"tabmenuborder\"></div>\n";
							push @retvals, $date;
							push @retvals, &get_netperfdata_slarpt("GIAEP");
						}
					} else {
						push @retvals, $date;
						@netperf = &get_netperfdata($uid, $d);
						push @retvals, @netperf;
					}
				} else {
					push @retvals, $date;
					@netperf = &get_netperfdata($uid, $d);
					push @retvals, @netperf;
				}
			} else {
	                        push @retvals, $line;
	                }
                }
                close($xx);
        } else { 
                push @retvals, "Cannot generate network performance page";
        } 
                
        return @retvals;
                
}


# Get netperf data 
#Pre: none
#Post: formatted HTML output
sub get_netperfdata {
	my ($uid, $date) = @_;

	#print "Content-type: text/json\n\n";
	# old netperf

	my @vals = ();

	# first check the cache - stats are kept for up to one hour
	my $duration = 60 * 60;
	my $file = "/tmp/cache-netperfstats";
	my $mtime = (stat($file))[8];
	my $currtime = time;
	my @v = split(/\s+/, $date);
	my $flag = "$v[1]-$v[4]";

#print "currtime=$currtime,mtime=$mtime,duration=$duration<br>";
#$currtime = $mtime+3600;
#	if( ($currtime - $mtime) < $duration) {
#		push @vals, "<p>\n";
#		open(my $in, $file);
#		while(my $l = <$in>) {
#			push @vals, $l;
#		}
#		return @vals;
#	}

	my $remote = $gia_net_rep_host;
	my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                            PeerPort => 7300,
                            Proto => "tcp",
                            Type => SOCK_STREAM) or return 0;

	print $socket "$uid\n";

	while(my $l = <$socket>) {
        	push @vals, $l;
	}

	close($socket); 

	#print "DEBUG: (@vals)\n";

	open(my $out, ">$file.$$");
	print $out @vals;
	close($out);

	unlink($file);
	rename("$file.$$", $file); #rename is atomic

	return @vals;

}


# Get netperf data
#Pre: none
#Post: formatted HTML output
sub get_netperfdata_slarpt {
	my ($giasvc) = @_;

	my @vals = ();

	my $file = "/tmp/cache-netperfstats-".$giasvc;

	my $remote = $gia_net_rep_host;
	my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                            PeerPort => 7309,
                            Proto => "tcp",
                            Type => SOCK_STREAM) or return 0;

	print $socket "$giasvc\n";

	while(my $l = <$socket>) {
        	push @vals, $l;
	}

	close($socket); 

	open(my $out, ">$file.$$");
	print $out @vals;
	close($out);

	unlink($file);
	rename("$file.$$", $file); #rename is atomic

	return @vals;
}


# Generate network performance reports
# Pre: code, tmplroot, report type, measurement, request_time, GIA|GIAE|GIAEP
sub screen_netperf_matrix {
        my ($code, $tmplroot, $rpttype, $measurement, $request_time, $giasvc) = @_;
        my $ctrlfile = $tmplroot."/local/netperf_matrix.htm";
        my @retvals = ();
	my @netperf = ();

	if (!$measurement)  { $measurement = 'RTD'; }
	if (!$request_time)  { $request_time = 'current'; }

        my $date = gmtime(time);
        $date .= " GMT";
	my $d = $date;
       
        $date = "<p class=text>Report generated on $date</p><p class=text>";
        if(-e $ctrlfile) {
		my $xx;
                open($xx, $ctrlfile);
                 while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/3/g;
                        $line =~ s/\*ETPRODUCT\*/$encet/g;
                        $line =~ s/\*RPTTYPE\*/$rpttype/g;

			if($line =~ /\*RTD_SELECTED\*/) {
				if ($measurement eq 'RTD')  {
					$line =~ s/\*RTD_SELECTED\*/selected/;
				} else {
					$line =~ s/\*RTD_SELECTED\*//;
				}
			}
			if($line =~ /\*PL_SELECTED\*/) {
				if ($measurement eq 'PL')  {
					$line =~ s/\*PL_SELECTED\*/selected/;
				} else {
					$line =~ s/\*PL_SELECTED\*//;
				}
			}
			if($line =~ /\*CURRENT_CHECKED\*/) {
				if ($request_time eq 'current')  {
					$line =~ s/\*CURRENT_CHECKED\*/checked/;
				} else {
					$line =~ s/\*CURRENT_CHECKED\*//;
				}
			}
			if($line =~ /\*LASTMONTH_CHECKED\*/) {
				if ($request_time eq 'lastmonth')  {
					$line =~ s/\*LASTMONTH_CHECKED\*/checked/;
				} else {
					$line =~ s/\*LASTMONTH_CHECKED\*//;
				}
			}
			if($line =~ /\*DATA\*/) {
				push @retvals, $date;
				@netperf = &get_netperfdata_matrix($measurement, $request_time, $giasvc);
				push @retvals, @netperf;
			} else {
	                        push @retvals, $line;
	                }
	#		print "in while loop: $measurement\n";
                }
                close($xx);
        } else { 
                push @retvals, "2 Cannot generate network performance page";
        } 
                
        return @retvals;
                
}


#Get netperf_matrix data
#Pre: measurement, request_time, GIA|GIAE|GIAEP
#Post: formatted HTML output
sub get_netperfdata_matrix {
        my ($measurement, $request_time, $giasvc) = @_;
                 
        my @vals = ();

	my $file = "/tmp/cache-netperfstatsmatrix-".$giasvc;

	my $remote = $gia_net_rep_host;
        my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                            PeerPort => 7310,
                            Proto => "tcp",
                            Type => SOCK_STREAM) or return 0;

	print $socket "$measurement|$request_time|$giasvc\n";  
        
        while(my $l = <$socket>) {
                push @vals, $l;
        }
        
        close($socket);
        
        open(my $out, ">$file.$$");  
        print $out @vals;
        close($out);
                            
        unlink($file);
        rename("$file.$$", $file); #rename is atomic

	return @vals;
}


# Generate MPLS IPVPN Site to Site reports
# Pre: uid, code, accno, tmplroot, $sitea, $siteb
sub screen_site2site {
	my ($uid, $code, $accno, $tmplroot, $sitea, $siteb) = @_;
	my $ctrlfile = $tmplroot."/local/site2site.htm";

	my $pcet = ($etproduct) ? $etproduct : "GIA";

	my $ws = &get_wholesaler($accno);
	my $defaultfile ;
	my $advfile ;
	my $conffile;
	if (($pcet =~ /EVPL/i)||($pcet =~ /VPLS/i)) {
		$conffile = $mpls_netperf_db."/".lc($ws)."/saa_router_juniper.cfg";
		$defaultfile = $mpls_netperf_db."/".lc($ws)."/OLSS-netperf_juniper.default";
 	        $advfile = $mpls_netperf_db."/".lc($ws)."/OLSS-netperf_juniper.advanced";
	}
	elsif(($pcet =~ /iptransit/i)) {

		$conffile = $mpls_netperf_db."/".lc($ws)."/saa_router_ipt.cfg";
                $defaultfile = $mpls_netperf_db."/".lc($ws)."/OLSS-netperf_ipt.default";
                $advfile = $mpls_netperf_db."/".lc($ws)."/OLSS-netperf_ipt.advanced";
	
	}
	else {
		$conffile = $mpls_netperf_db."/".lc($ws)."/saa_router.cfg";
		$defaultfile = $mpls_netperf_db."/".lc($ws)."/OLSS-netperf.default";
                $advfile = $mpls_netperf_db."/".lc($ws)."/OLSS-netperf.advanced";
	}
	my @retvals = ();
	my @netperf = ();

	my ($sec,$min,$hour,$day,$mon,$year) = (gmtime(time))[0..5];
	if ($hour < 1) {
		($sec,$min,$hour,$day,$mon,$year) = (gmtime(time-24*60*60))[0..5];
	}

	$year += 1900;
	$mon += 1;
	if (length($day) == 1)  { $day = '0'.$day; }
	if (length($mon) == 1)  { $mon = '0'.$mon; }

	my %cityid_vpnip = ();
	my %cityid_cityname = ();
	my %customized_pop = ();


	if((-e $ctrlfile) && (-e $conffile)) {
		my $xx;

		my (%olss_region, %olss_display);
                if ((-e $defaultfile) && ($ws =~ /Telstra/i)) {
                        open ($xx, $defaultfile);
                        while (chomp(my $line = <$xx>)) {
                                if ((!$line) || ($line =~ /^\#/))  { next };

                                my @seg = split /\t/, $line;
                                $olss_region{$seg[0]} = $seg[1];
                                $olss_display{$seg[0]} = $seg[2];
                        }
                        close($xx);
                }

		if ((-e $advfile) && ($ws =~ /Telstra/i)) {
			open($xx, $advfile);
			while(chomp(my $line = <$xx>)) {
				if ((!$line) || ($line =~ /^\#/))  { next };
				my @seg = split /\t/, $line;
				if ($seg[0] =~ /^$accno$/) {
					foreach my $i (2..$#seg) {
						$customized_pop{$seg[$i]} = $seg[1];
					}
				}
			}
			close($xx);	
		}

		open($xx, $conffile);
		while (chomp(my $line = <$xx>)) {
			if ((!$line) || ($line =~ /^\#/))  { next };

			my @seg = split /\t/, $line;

			if ($ws =~ /Telstra/i) {
				if ($customized_pop{$seg[0]} =~ /PERMIT/i) {
					$olss_display{$seg[0]} = "Y";
				} elsif ($customized_pop{$seg[0]} =~ /DENY/i) {
					$olss_display{$seg[0]} = "N";
				}
				if ($olss_display{$seg[0]} eq "Y") {
					$cityid_vpnip{$seg[0]} = $seg[3];
					$cityid_cityname{$seg[0]} = $seg[1];
				}
			} else {
				$cityid_vpnip{$seg[0]} = $seg[3];
				$cityid_cityname{$seg[0]} = $seg[1];
			}
		}		
		close($xx);

		open($xx, $ctrlfile);
		while(my $line = <$xx>) {
			$line = &link_format($line, $code, $ctrlname);
			$line =~ s/\*CODE\*/$code/g;
			$line =~ s/\*LEVEL\*/8/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g;
			if ($line =~ s/\*LIST-SITEA\*//) {
				foreach my $k (sort {$cityid_cityname{$a} cmp $cityid_cityname{$b}} keys%cityid_cityname) {
					my $tmp = "<option value='$k' ";
					if ($sitea eq $k) {
						$tmp .= "selected";
					}
					$tmp .= ">$cityid_cityname{$k}</option>\n";
					push @retvals, $tmp;
				}
			}
			if ($line =~ s/\*LIST-SITEB\*//) {
				foreach my $k (sort {$cityid_cityname{$a} cmp $cityid_cityname{$b}} keys%cityid_cityname) {
					my $tmp = "<option value='$k' ";
					if ($siteb eq $k) {
						$tmp .= "selected";
					}
					$tmp .= ">$cityid_cityname{$k}</option>\n";
					push @retvals, $tmp;
				}
			}

			if($line =~ /\*DATA\*/) {
				if ($sitea ne $siteb) {
					@netperf = &get_mpls_site2sitedata($sitea, $siteb, $day, $mon, $year, $ws );
					if ($netperf[0] !~ /FAIL/) {
	       	                	        push @retvals, @netperf;
					}
				}
			} else {
				push @retvals, $line;
			}
		}
		close($xx);
	} else {
		push @retvals, "Cannot generate network performance page";
	}
                                        
	return @retvals;
}


# Get MPLS IPVPN netperf data
#Pre: site A, site B, day, month, year, wholesaler
#Post: formatted HTML output
sub get_mpls_site2sitedata {
	my ($sitea, $siteb, $day, $month, $year, $ws) = @_;

	my @vals = ();
        my $remote;
        my $ws = &get_wholesaler($accno);
	my $pcet = ($etproduct) ? $etproduct : "GIA";

        if ($ws =~ /Telstra/i ) {
                $remote = $telstra_saa_host;
        } else {
                $remote = $mpls_net_rep_host;
        }
	my $socket = IO::Socket::INET->new(PeerAddr => $remote,
				PeerPort => 7301,
				Proto => "tcp",
				Type => SOCK_STREAM) or return 0;
	print $socket "$sitea|$siteb|$day|$month|$year|$pcet\n";
	
	while(my $l = <$socket>) {
		push @vals, $l;
	}

	close($socket);

	my @result = ();
	my $ignore = 0;
	foreach my $line (@vals) {

####################################################################################################
# Comment when PCCW supports CLASS 1 & 2
		if ($ws =~ /PCCW/i) {
			if ($line =~ /CoS0/) { $ignore = 0; }
			if ($line =~ /CoS1/) { $ignore = 1; }
			if ($line =~ /CoS2/) { $ignore = 1; }
			if ($line =~ /CoS3/) { $ignore = 0; }
			if ($line =~ /CoS4/) { $ignore = 0; }
			if ($line =~ /CoS5/) { $ignore = 0; }
		}	
#
####################################################################################################

		if ($ignore) { next; }
#Chandini_class
                if ($ws =~ /Telstra/i ) {
                   if ($pcet =~ /EVPL/i) {
                        $line =~ s/CoS0/$evpl_cos_label{class0}/g;
                        $line =~ s/CoS3/$evpl_cos_label{class3}/g;
                   }
                   else {
                        $line =~ s/CoS0/$telstra_cos_label{class0}/g;
                        $line =~ s/CoS1/$telstra_cos_label{class1}/g;
                        $line =~ s/CoS2/$telstra_cos_label{class2}/g;
                        $line =~ s/CoS3/$telstra_cos_label{class3}/g;
                        $line =~ s/CoS4/$telstra_cos_label{class4}/g;
                        $line =~ s/CoS5/$telstra_cos_label{class5}/g;
                   }
        	} elsif ($ws =~ /PCCW/i) {
                	$line =~ s/CoS0/$pccw_cos_label{class0}/g;
                        $line =~ s/CoS3/$pccw_cos_label{class3}/g;
                        $line =~ s/CoS4/$pccw_cos_label{class4}/g;
                        $line =~ s/CoS5/$pccw_cos_label{class5}/g;
	        } else {
			$line =~ s/CoS0/CoS Group 0/g;
			$line =~ s/CoS1/CoS Group 1/g;
			$line =~ s/CoS2/CoS Group 2/g;
			$line =~ s/CoS3/CoS Group 3/g;
			$line =~ s/CoS4/CoS Group 4/g;
			$line =~ s/CoS5/CoS Group 5/g;
        	}

		push @result, $line;
	}

        return @result;
}


sub screen_site2site1 {		### NOT USE, Modified by Matthew 20071015
        my ($uid, $code, $accno, $tmplroot, $sitea, $siteb) = @_;
        my $ctrlfile = $tmplroot."/local/site2site.htm";
        my $saarouter_cfg = "/data1/mplsslarpt/saarouter0117.cfg";
        my @retvals = ();
        my @netperf = ();
        my (@saarouter);
        
	#### determine today ####
        my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) = gmtime(time);
        if ($hour < 1) {
                my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) = gmtime(time-24*60*60);
        }
        $year += 1900;
        $mon += 1;
        if ($mon < 10) {
                $mon = "0".$mon; 
        }
        if ($mday < 10) {
                $mday = "0".$mday;
        }
        my $todate = $year.$mon.$mday;

        if (-e $saarouter_cfg) {
                open (FH, $saarouter_cfg);
                while (my $line = <FH>) {
                        push @saarouter, (split('\,', $line))[0];
                }
                close(FH);
        }
 
        @saarouter = sort(@saarouter);

        if(-e $ctrlfile) {
                my $xx;
                open($xx, $ctrlfile);
                 while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/8/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g;
                        if ($line =~ /^\*LIST/) {
                                if ($line =~ /SITEA/) {
					$sitea =~ s/\+/ /g;
                                        push @retvals, print_list('0', $sitea, @saarouter);
                                }
                                if ($line =~ /SITEB/) {
					$siteb =~ s/\+/ /g;
                                        push @retvals, print_list('0', $siteb, @saarouter);
                                }
				$line = "";
                        }
                        if($line =~ /\*DATA\*/) {
                                @netperf = &get_mpls_site2sitedata($uid, $sitea, $siteb, $todate);
                                push @retvals, @netperf;
                        } else {
                                push @retvals, $line;
                        }
                }
                close($xx);
        } else {
                push @retvals, "Cannot generate network performance page";
        }
                                        
        return @retvals;

}

sub get_mpls_site2sitedata1 {	### NOT USE, Modified by Matthew 20071015
        my ($uid, $sitea, $siteb, $todate) = @_;

        # print "Content-type: text/json\n\n";
        # old netperf
                                
        my @vals = ();   
                 
	my $remote = $per_rep_host;
        my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                            PeerPort => 7303,
                            Proto => "tcp",
                            Type => SOCK_STREAM) or return 0;

        print $socket "$sitea|$siteb|$todate\n";

        while(my $l = <$socket>) {
                push @vals, $l;
        }                       
        
        close($socket);
        
        #print "DEBUG: (@vals)\n"; 
        
        open(my $out, ">$file.$$");  
        print $out @vals;               
        close($out);

	unlink($file);
        rename("$file.$$", $file); #rename is atomic

        my $ws = &get_wholesaler($accno);

	my @result = ();
	my $ignore = 0;
	foreach my $line (@vals) {

####################################################################################################
# Comment when PCCW supports CLASS 1 & 2
		if ($ws =~ /PCCW/i) {
			if ($line =~ /CoS0/) { $ignore = 0; }
			if ($line =~ /CoS1/) { $ignore = 1; }
			if ($line =~ /CoS2/) { $ignore = 1; }
			if ($line =~ /CoS3/) { $ignore = 0; }
			if ($line =~ /CoS4/) { $ignore = 0; }
			if ($line =~ /CoS5/) { $ignore = 0; }
		}	
#
####################################################################################################

		if ($ignore) { next; }

	        if ($ws =~ /Telstra/i ) {
                	$line =~ s/CoS0/$telstra_cos_label{class0}/g;
                        $line =~ s/CoS1/$telstra_cos_label{class1}/g;
                        $line =~ s/CoS2/$telstra_cos_label{class2}/g;
                        $line =~ s/CoS3/$telstra_cos_label{class3}/g;
                        $line =~ s/CoS4/$telstra_cos_label{class4}/g;
                        $line =~ s/CoS5/$telstra_cos_label{class5}/g;
        	} elsif ($ws =~ /PCCW/i) {
                	$line =~ s/CoS0/$pccw_cos_label{class0}/g;
                        $line =~ s/CoS3/$pccw_cos_label{class3}/g;
                        $line =~ s/CoS4/$pccw_cos_label{class4}/g;
                        $line =~ s/CoS5/$pccw_cos_label{class5}/g;
	        } else {
			$line =~ s/CoS0/CoS Group 0/g;
			$line =~ s/CoS1/CoS Group 1/g;
			$line =~ s/CoS2/CoS Group 2/g;
			$line =~ s/CoS3/CoS Group 3/g;
			$line =~ s/CoS4/CoS Group 4/g;
			$line =~ s/CoS5/CoS Group 5/g;
        	}

		push @result, $line;
	}

        return @result;
}


# Generate MPLS IPVPN network performance reports
# Pre: uid, code, accno, tmplroot, rpttype, measurement, cos, datetype, requestday, requestmonth, requestyear, region_pairs, city_aend, city_zend
sub screen_mpls_netperf {
        my ($uid, $code, $accno, $tmplroot, $rpttype, $measurement, $cos, $datetype, $requestday, $requestmonth, $requestyear, $region_pairs, $city_aend, $city_zend) = @_;
        my $ctrlfile = $tmplroot."/local/netperf.htm";
	my $ws = &get_wholesaler($accno);
        my @retvals = ();
	my $tabmenu = 0;
        if(-e $ctrlfile) {
                my $xx;
                open($xx, $ctrlfile);
                while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);

			if($line =~ s/\*TABMENU\*//g) {
				if ($ws =~ /Telstra/i) {
					$tabmenu = 1;
				}
			}

			if($line =~ /\*DATA\*/) {
				if ($tabmenu =~ /^1$/) {
					if ($rpttype =~ /^0$/) {
						push @retvals, "<div id=\"tabmenu\">\n";
					        push @retvals, "<a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=0\"><span class=\"curposr\">Pan-Regional, Inter-Regional and Region n X n NPSL Matrices</span></a><a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=1\"><span>Customised City Pairs View</span></a>\n";
						push @retvals, "</div>\n";
						push @retvals, "<div id=\"tabmenuborder\"></div>\n";
						push @retvals, &generate_mpls_netperf($code, $accno, $tmplroot, $measurement, $cos, $datetype, $requestday, $requestmonth, $requestyear, 'region', $region_pairs);
					} elsif ($rpttype =~ /^1$/) {
						push @retvals, "<div id=\"tabmenu\">\n";
					        push @retvals, "<a href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=0\"><span>Pan-Regional, Inter-Regional and Region n X n NPSL Matrices</span></a><a class=\"curposl\" href=\"/cgi-bin/tgwciss-reports.cgi?code=$code&level=3&et=$encet&rpttype=1\"><span class=\"curposr\">Customised City Pairs View</span></a>\n";
						push @retvals, "</div>\n";
						push @retvals, "<div id=\"tabmenuborder\"></div>\n";
						push @retvals, &generate_mpls_netperf($code, $accno, $tmplroot, $measurement, $cos, $datetype, $requestday, $requestmonth, $requestyear, 'city', '', $city_aend, $city_zend);
					}
				} else {
					if ($ws =~ /Telstra/i) {
						push @retvals, &generate_mpls_netperf($code, $accno, $tmplroot, $measurement, $cos, $datetype, $requestday, $requestmonth, $requestyear, 'region', $region_pairs);
					} else {
						push @retvals, &generate_mpls_netperf($code, $accno, $tmplroot, $measurement, $cos, $datetype, $requestday, $requestmonth, $requestyear, 'nonregion');
					}
				}
			} else {
				push @retvals, $line;
                        }
                }
                close($xx);
        } else {
                push @retvals, "Cannot generate network performance page";
        }
        return @retvals;
}


# Generate MPLS IPVPN & VPLS network performance reports
# Pre: code, accno, tmplroot, measurement, cos, datetype, requestday, requestmonth, requestyear, viewtype, region_pairs, cities_aend, cities_zend
sub generate_mpls_netperf {
        my ($code, $accno, $tmplroot, $measurement, $cos, $datetype, $requestday, $requestmonth, $requestyear, $viewtype, $region_pairs, $cities_aend, $cities_zend) = @_;

        my $ctrlfile;

	if ($viewtype =~ /^region$/i) {
		$ctrlfile = $tmplroot."/local/netperf_region.htm";
	} elsif ($viewtype =~ /^nonregion$/i) {
		$ctrlfile = $tmplroot."/local/netperf_nonregion.htm";
	} elsif ($viewtype =~ /^city$/i) {
		$ctrlfile = $tmplroot."/local/netperf_city.htm";
	}

	open (ME77, ">>/data1/tmp_log/ME77.log");
	my $ws = &get_wholesaler($accno);
	my $pcet = ($etproduct) ? $etproduct : "GIA";
	my $conffile;
	my $linkfile;
	my $defaultfile;
	my $advfile;
	if (($pcet =~/EVPL/i)|| ($pcet =~/VPLS/i)) {
		$conffile = $mpls_netperf_db.lc($ws)."/saa_router_juniper.cfg";
		$linkfile = $mpls_netperf_db.lc($ws)."/OLSS-netperf_juniper.regionlink";
 	       	$defaultfile = $mpls_netperf_db.lc($ws)."/OLSS-netperf_juniper.default";
        	$advfile = $mpls_netperf_db.lc($ws)."/OLSS-netperf_juniper.advanced";

	}else {
        	$conffile = $mpls_netperf_db."/".lc($ws)."/saa_router.cfg";
		$linkfile = $mpls_netperf_db."/".lc($ws)."/OLSS-netperf.regionlink";
	        $defaultfile = $mpls_netperf_db."/".lc($ws)."/OLSS-netperf.default";
        	$advfile = $mpls_netperf_db."/".lc($ws)."/OLSS-netperf.advanced";

	}
# Begin Changes - IPTransit - 29-jul-2010 - Namratha

 if (($pcet =~/iptransit/i)) 
{
                $conffile = $mpls_netperf_db.lc($ws)."/saa_router_ipt.cfg";
                $linkfile = $mpls_netperf_db.lc($ws)."/OLSS-netperf_ipt.regionlink";
                $defaultfile = $mpls_netperf_db.lc($ws)."/OLSS-netperf_ipt.default";
                $advfile = $mpls_netperf_db.lc($ws)."/OLSS-netperf_ipt.advanced"
}
# End Changes - IPTransit - 29-jul-2010 - Namratha

       	my @retvals = ();
        my @netperf = ();
	my @measures = ();

# Begin Changes - IPTransit - 29-jul-2010 - Namratha
	if ($pcet =~/iptransit/i)
	{
		@measures = ('RTD','Packet Loss');
	}
	else
	{
		@measures = ('RTD','Jitter','Throughput');
	}
	$measurement = ($measurement) ? $measurement : "RTD";

# End Changes - IPTransit - 29-jul-2010 - Namratha
     	 my %coss = ();
	 my @month = ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');


	if ($ws =~ /Telstra/i ) {
           if ($pcet =~ /EVPL/i) {
             %coss = ('CoS0' => $evpl_cos_label{class0},
                      'CoS3' => $evpl_cos_label{class3});
           }
# Begin Changes - IPTransit - 29-jul-2010 - Namratha

	   elsif ($pcet =~ /iptransit/i) {
             %coss = ('CoS0' => $iptransit_cos_label{class0},
                      'CoS2' => $iptransit_cos_label{class2});
           }

# End Changes - IPTransit - 29-jul-2010 - Namratha
           else {
             %coss = ('CoS0' => $telstra_cos_label{class0},
                         'CoS1' => $telstra_cos_label{class1},
                         'CoS2' => $telstra_cos_label{class2},
                         'CoS3' => $telstra_cos_label{class3},
                         'CoS4' => $telstra_cos_label{class4},
                         'CoS5' => $telstra_cos_label{class5});
           }
	} elsif ($ws =~ /PCCW/i) {
	        %coss = ('CoS0' => $pccw_cos_label{class0},
			 'CoS3' => $pccw_cos_label{class3}, 
			 'CoS4' => $pccw_cos_label{class4},
			 'CoS5' => $pccw_cos_label{class5});
	} else {
		%coss = ('CoS0' => 'CoS Group 0',
			 'CoS1' => 'CoS Group 1',
			 'CoS2' => 'CoS Group 2',
			 'CoS3' => 'CoS Group 3',
			 'CoS4' => 'CoS Group 4', 
			 'CoS5' => 'CoS Group 5');
	}

	$cos = ($cos) ? $cos : "CoS0";

        my ($sec,$min,$hour,$day,$mon,$year) = (gmtime(time))[0..5];
        if ($hour < 2) {
                ($sec,$min,$hour,$day,$mon,$year) = (gmtime(time-24*60*60))[0..5];
        }

        $year += 1900;
	$mon += 1;
	if (length($day) == 1)  { $day = '0'.$day; }
	if (length($mon) == 1)  { $mon = '0'.$mon; }

	$datetype = ($datetype) ? $datetype : "daily";

        if ($requestday eq "" && $requestmonth eq "" && $requestyear eq "") {
                $requestday = $day;
                $requestmonth = $mon;
                $requestyear = $year;
        }

	my @regionlink = ();
	my %cityid_vpnip = ();
	my %cityid_cityname = ();
	my %vpnip_aend = ();
	my %vpnip_zend = ();
	my %customized_pop = ();


	if ((-e $ctrlfile) && (-e $conffile) && ((($viewtype =~ /^region$/i) && (-e $linkfile) && (-e $defaultfile)) || ($viewtype =~ /^nonregion$/i) || (($viewtype =~ /^city$/i) && (-e $defaultfile)))) {
		my $xx;
		my (%olss_region, %olss_display);
		if ((-e $defaultfile) && (($viewtype =~ /^region$/i) || ($viewtype =~ /^city$/i))) {
			open ($xx, $defaultfile);
			while (chomp(my $line = <$xx>)) {
				if ((!$line) || ($line =~ /^\#/))  { next };
				my @seg = split /\t/, $line;
				$olss_region{$seg[0]} = $seg[1];
				$olss_display{$seg[0]} = $seg[2];
			}
			close($xx);
		}

		if ((-e $advfile) && (($viewtype =~ /^region$/i) || ($viewtype =~ /^city$/i))) {
			open($xx, $advfile);
			while(chomp(my $line = <$xx>)) {
				if ((!$line) || ($line =~ /^\#/))  { next };
				my @seg = split /\t/, $line;
				if ($seg[0] =~ /^$accno$/) {
					foreach my $i (2..$#seg) {
						$customized_pop{$seg[$i]} = $seg[1];
					}
				}
# ME_77_Stavan - Start
			   	if ($tmplroot =~ /reach-reseller-telstra/ and $seg[0] =~ /^ALL-TELSTRA-TEMPLATES$/) {
                                        foreach my $i (2..$#seg) {
                                                $customized_pop{$seg[$i]} = $seg[1];
                                        }
                                }
# ME_77_Stavan - End 
			}
			close($xx);
		}

		if ($viewtype =~ /^region$/i) {
			my %region = ();
			open($xx, $conffile);
			while(chomp(my $line = <$xx>)) {
				if ((!$line) || ($line =~ /^\#/))  { next };
				my @seg = split /\t/, $line;
				if ($customized_pop{$seg[0]} =~ /PERMIT/i) {
					$olss_display{$seg[0]} = "Y";
				} elsif ($customized_pop{$seg[0]} =~ /DENY/i) {
					$olss_display{$seg[0]} = "N";
				}
				if ($olss_display{$seg[0]} eq "Y") {
					foreach my $r (split(",", $olss_region{$seg[0]})) {
						$region{$r} = '';
					}
				}
			}
			close($xx);
			open($xx, $linkfile);
			while(chomp(my $line = <$xx>)) {
				if ((!$line) || ($line =~ /^\#/))  { next };
					my @seg = split /-/, (split /\t/, $line)[0];
					if (defined($region{$seg[0]}) && defined($region{$seg[1]})) {
						push @regionlink, $line;
					}
			}
			close($xx);
			$region_pairs = ($region_pairs) ? $region_pairs : (split /\t/, $regionlink[0])[0];
		} elsif ($viewtype =~ /^city$/i) {
			my $owner;
			open($xx, $conffile);
			while(chomp(my $line = <$xx>)) {
				if ((!$line) || ($line =~ /^\#/))  { next };
				my @seg = split /\t/, $line;
				if ($customized_pop{$seg[0]} =~ /PERMIT/i) {
					$olss_display{$seg[0]} = "Y";
				} elsif ($customized_pop{$seg[0]} =~ /DENY/i) {
					$olss_display{$seg[0]} = "N";
				}
				if ($olss_display{$seg[0]} eq "Y") {
					$cityid_vpnip{$seg[0]} = $seg[3];
					$cityid_cityname{$seg[0]} = $seg[1];
				}
			}
			close($xx);

			foreach my $i ((split("\0", $cities_aend))[0..7]) { 
				if (!defined($i))  { last; }
				$vpnip_aend{$i} = $cityid_vpnip{$i};
			}
			foreach my $i ((split("\0", $cities_zend))[0..7]) {
				if (!defined($i))  { last; }
				$vpnip_zend{$i} = $cityid_vpnip{$i};
			}
		}

                open($xx, $ctrlfile);
                while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*ETPRODUCT\*/$encet/g;
                        $line =~ s/\*LEVEL\*/3/g;
                        if ($line =~ s/\*LIST-REGION_PAIRS\*//) {
				foreach my $k (@regionlink) {
					my @seg = split /\t/, $k;
					my $tmp = "<option value='$seg[0]' ";
					if ($seg[0] eq $region_pairs) {
						$tmp .= "selected";
					}
					$tmp .= ">$seg[1]</option>\n";
					push @retvals, $tmp;
				}
                        }
                        if ($line =~ s/\*LIST-CITY-AEND\*//) {
				foreach my $k (sort {$cityid_cityname{$a} cmp $cityid_cityname{$b}} keys%cityid_cityname) {
					my $tmp = "<option value='$k' ";
					if (defined($vpnip_aend{$k})) {
						$tmp .= "selected";
					}
					$tmp .= ">$cityid_cityname{$k}</option>\n";
					push @retvals, $tmp;
				}
                        }
                        if ($line =~ s/\*LIST-CITY-ZEND\*//) {
				foreach my $k (sort {$cityid_cityname{$a} cmp $cityid_cityname{$b}} keys%cityid_cityname) {
					my $tmp = "<option value='$k' ";
					if (defined($vpnip_zend{$k})) {
						$tmp .= "selected";
					}
					$tmp .= ">$cityid_cityname{$k}</option>\n";
					push @retvals, $tmp;
				}
                        }
                        if ($line =~ s/\*LIST-MEASUREMENT\*//) {
                                push @retvals, print_list('measure', $measurement, @measures);
                        }
                        if ($line =~ s/\*LIST-COS\*//) {
                                foreach my $k (sort keys%coss) {
                                        my $tmp = "<option value='$k' ";
                                        if ($k eq $cos) {
                                                $tmp .= "selected";
                                        }
                                        $tmp .= ">$coss{$k}</option>\n";
                                        push @retvals, $tmp;
                                }
                        }
			if($line =~ /\*DAILY_CHECKED\*/) {
				if ($datetype eq 'daily')  {
					$line =~ s/\*DAILY_CHECKED\*/checked/;
				} else {
					$line =~ s/\*DAILY_CHECKED\*//;
				}
			}
			if($line =~ /\*WEEKLY_CHECKED\*/) {
				if ($datetype eq 'weekly')  {
					$line =~ s/\*WEEKLY_CHECKED\*/checked/;
				} else {
					$line =~ s/\*WEEKLY_CHECKED\*//;
				}
			}
			if($line =~ /\*MONTHLY_CHECKED\*/) {
				if ($datetype eq 'monthly')  {
					$line =~ s/\*MONTHLY_CHECKED\*/checked/;
				} else {
					$line =~ s/\*MONTHLY_CHECKED\*//;
				}
			}
                        if ($line =~ s/\*LIST-DAY\*//) {
                                for (my $i=1; $i<=31; $i++) {
					if (length($i) == 1) {
						$d = "0".$i;
					} else {
						$d = $i;
					}
                                        my $tmp = "<option value='$d' ";
                                        if ($d == $requestday) {
                                                $tmp .= "selected";
                                        }
                                        $tmp .= ">$i</option>\n";
                                        push @retvals, $tmp;
                                }
                        }
                        if ($line =~ s/\*LIST-MONTH\*//) {
                                for (my $i=1; $i<=12; $i++) {
					if (length($i) == 1) {
						$m = "0".$i;
					} else {
						$m = $i;
					}
                                        my $tmp = "<option value='$m' ";
                                        if ($m == $requestmonth) {
                                                $tmp .= "selected";
                                        }
                                        $tmp .= ">$month[$i-1]</option>\n";
                                        push @retvals, $tmp;
                                }
                        }
                        if ($line =~ s/\*LIST-YEAR\*//) {
                                for (my $i=$year; $i>=$year-10; $i--) {
                                        my $tmp = "<option value='$i' ";
                                        if ($i == $requestyear) {
                                                $tmp .= "selected";
                                        }
                                        $tmp .= ">$i</option>\n";
                                        push @retvals, $tmp;
                                }
                        }

                        if($line =~ /\*DATA\*/) {
				if ($viewtype =~ /^region$/i) {
					my @customized_permit = ();
					my @customized_deny = ();
					foreach my $p (keys%customized_pop) {
						if ($customized_pop{$p} =~ /PERMIT/i) {
							push @customized_permit, $p;
						} elsif ($customized_pop{$p} =~ /DENY/i) {
							push @customized_deny, $p;
						}
					}
	                                @netperf = &get_mpls_netperfdata($measurement, $coss{$cos}, $cos, $datetype, $requestday, $requestmonth, $requestyear, $viewtype, join("\0", @customized_permit), join("\0", @customized_deny), $region_pairs);
				} elsif ($viewtype =~ /^nonregion$/i) {
	                                @netperf = &get_mpls_netperfdata($measurement, $coss{$cos}, $cos, $datetype, $requestday, $requestmonth, $requestyear, $viewtype);
				} elsif ($viewtype =~ /^city$/i) {
	                                @netperf = &get_mpls_netperfdata($measurement, $coss{$cos}, $cos, $datetype, $requestday, $requestmonth, $requestyear, $viewtype, '', '', join("\0", values%vpnip_aend)."|".join("\0", values%vpnip_zend));
				}
				if ($netperf[0] !~ /FAIL/) {
	       	                        push @retvals, @netperf;
				}
                        } else {
                                push @retvals, $line;
                        }
                }
                close($xx);
        } else {
                push @retvals, "Cannot generate network performance page";
        }
	close (DEBUG);
        return @retvals;
}


# Get MPLS IPVPN netperf data
#Pre: measurement, cos_name, cos, datetype, day, month, year, viewtype, customized_permit, customized_deny, data
#Post: formatted HTML output
sub get_mpls_netperfdata {
	my ($measurement, $cos_name, $cos, $datetype, $day, $month, $year, $viewtype, $customized_permit, $customized_deny, $data) = @_;

	my %tos = ('CoS0' => 0, 'CoS1' => 32, 'CoS2' => 64, 'CoS3' => 96, 'CoS4' => 128, 'CoS5' => 160);
        my @vals = ();
	my $remote;
	my $ws = &get_wholesaler($accno);
	my $pcet = ($etproduct) ? $etproduct : "GIA";
	if ($ws =~ /Telstra/i ) {
	        $remote = $telstra_saa_host;
	} else {
		$remote = $mpls_net_rep_host;
	}
	my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                            PeerPort => 7300,
                            Proto => "tcp",
                            Type => SOCK_STREAM) or return 0;

	print $socket "$measurement|$cos_name|$tos{$cos}|$datetype|$day|$month|$year|$viewtype|$customized_permit|$customized_deny|$pcet|$data\n";
        while(my $l = <$socket>) {
 	#		if($ws =~ /Telstra/i and $l=~/Bangkok/) {next;}
                push @vals, $l;
        }

        close($socket);
        return @vals;
}


# Generate MPLS IPVPN network performance reports
# Pre: uid, code, accno, tmplroot, fnn, opshandle, encryp_svc
sub screen_mpls_netperf1 {	### NOT USE, Modified by Matthew 20071015
        my ($uid, $code, $accno, $tmplroot, $requestday, $requestmonth, $requestyear, $measurement, $cos) = @_;

        my $ctrlfile = $tmplroot."/local/netperf.htm";
        my @retvals = ();
        my @netperf = ();
	my %coss = ();
        my @measures = ('Latency','Jitter','Throughput');
        my @days = ('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31');
        my @month = ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');


	my $ws = &get_wholesaler($accno);
	
	if ($ws =~ /Telstra/i ) {
	        %coss = ('CoS0' => $telstra_cos_label{class0},
			 'CoS1' => $telstra_cos_label{class1},
			 'CoS2' => $telstra_cos_label{class2},
			 'CoS3' => $telstra_cos_label{class3},
			 'CoS4' => $telstra_cos_label{class4}, 
			 'CoS5' => $telstra_cos_label{class5});
	} elsif ($ws =~ /PCCW/i) {
	        %coss = ('CoS0' => $pccw_cos_label{class0},
			 'CoS3' => $pccw_cos_label{class3}, 
			 'CoS4' => $pccw_cos_label{class4},
			 'CoS5' => $pccw_cos_label{class5});
	} else {
		%coss = ('CoS0' => 'CoS Group 0',
			 'CoS1' => 'CoS Group 1',
			 'CoS2' => 'CoS Group 2',
			 'CoS3' => 'CoS Group 3',
			 'CoS4' => 'CoS Group 4', 
			 'CoS5' => 'CoS Group 5');
	}

        if ($cos eq "") {
                $cos = "CoS0";
        }

        if ($measurement eq "") {
                $measurement = "Latency";
        }

        my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) = gmtime(time-24*60*60);
        if ($hour < 4) {
                ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) = gmtime(time-24*60*60*2);
        }

        $year += 1900;
        if ($mday < 10) {
                $mday = "0".$mday;
        }

        if ($requestday eq "" && $requestmonth eq "" && $requestyear eq "") {
                $requestday = $mday;
                $requestmonth = $month[$mon];
                $requestyear = $year;
        }

        if(-e $ctrlfile) {
                my $xx;
                open($xx, $ctrlfile);
                while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*ETPRODUCT\*/$encet/g;
                        $line =~ s/\*LEVEL\*/3/g;
                        if ($line =~ /^\*LIST/) {
                                if ($line =~ /MEASUREMENT\*/) {
                                        push @retvals, print_list('measure', $measurement, @measures);
                                }
                                if ($line =~ /COS\*/) {
                                        foreach my $k (sort keys%coss) {
                                                my $tmp = "<option value='$k' ";
                                                if ($k eq $cos) {
                                                        $tmp .= "selected";
                                                }
                                                $tmp .= ">$coss{$k}</option>\n";
                                                push @retvals, $tmp;
                                        }
                                        push @retvals, print_list('0', $cos, @coss);
                                }
                                if ($line =~ /DAY\*/) {
                                        push @retvals, print_list('0', $requestday, @days);
                                }
                                if ($line =~ /MONTH\*/) {
                                        push @retvals, print_list('0', $requestmonth, @month);
                                }
                                if ($line =~ /YEAR\*/) {
                                        for (my $i=$year; $i>=$year-10; $i--) {
                                                my $tmp = "<option value='$i' ";
                                                if ($i == $requestyear) {
                                                        $tmp .= "selected";
                                                }
                                                $tmp .= ">$i</option>\n";
                                                push @retvals, $tmp;
                                        }
                                }
                        }
                        if($line =~ /\*DATA\*/) {
                                @netperf = &get_mpls_netperfdata($uid, $requestday, $requestmonth, $requestyear, $measurement, $cos, $coss{$cos});
                                push @retvals, @netperf;
                        } else {
                                push @retvals, $line;
                        }
                }
                close($xx);
        } else {
                push @retvals, "Cannot generate network performance page";
        }

        return @retvals;
}


# Get MPLS IPVPN netperf data
#Pre: none
#Post: formatted HTML output
sub get_mpls_netperfdata1 {	### NOT USE, Modified by Matthew 20071015
        my ($uid, $requestday, $requestmonth, $requestyear, $measurement, $cos, $cosname) = @_;

        my @vals = ();

        my $remote = $per_rep_host;
        my $socket = IO::Socket::INET->new(PeerAddr => $remote,
                                        PeerPort => 7302,
                                        Proto => "tcp",
                                        Type => SOCK_STREAM) or return 0;

        print $socket "$requestday|$requestmonth|$requestyear|$measurement|$cos\n";

        while(my $l = <$socket>) {
                $l =~ s/$cos/$cosname/g;
                push @vals, $l;
        }

        close($socket);

        #print "DEBUG: (@vals)\n";

        open(my $out, ">$file.$$");
        print $out @vals;
        close($out);

        unlink($file);
        rename("$file.$$", $file); #rename is atomic

        return @vals;
}

                                 
sub print_list {
        my ($flag, $curr_value, @list) = @_;
        my @result;

        foreach my $i (@list) { 
                my $tmp;
                my $i_value = $i;
                 
                if ($flag eq "measure") {
                        $i_value = lc($i_value);
			$i_value =~ s/ //g;
                }
                $tmp = "<option value='$i_value' ";
                if (lc($i_value) eq lc($curr_value)) {
                        $tmp .= "selected";     
                }
                $tmp .= ">$i</option>\n";
                push @result, $tmp;
        }

        return @result;
}


sub get_wholesaler {
        my $accno = $_[0];

        if ($wholesalers{$accno})  { return $wholesalers{$accno}; }
        CMS_DB::connect_to_database;
        my $result = CMS_CUSTOMER_DB::get_customer($accno);
        CMS_DB::disconnect_from_database;
        if ((!$$result[4]) || ($$result[4] eq $accno))  { return; }
        if ($wholesalers{$$result[4]})  { return $wholesalers{$$result[4]}; }
        return get_wholesaler($$result[4]);
}


######################

sub numerically {$a <=> $b}

# Load pop values from file
# Post: %$popvalues
sub load_pop {
        my %v;
        open DDD, ">/data1/tmp_log/deep.txt";
######################################################################################
#Change Request Number:- CRQ000000004261
#Reason :-              Looking Glass based on loopback address
#                       GIA,IPTRANSIT--- Loopback0 and IPVPN,VPLS,EVPL ----- Loopback11
#Date:-                 22-Sep-2011
########################################################################################
        if($tmp_loop_back = shift)
        {
                %loop_back = %$tmp_loop_back;
        }
        if (($etproduct =~ /VPLS/i )){
                $popfile = $popfile_juniper_VPLS;
        }
        elsif (($etproduct =~ /EVPL/i )){
                $popfile = $popfile_juniper_EVPL;
        }
        elsif (($etproduct =~ /GIA/i )){
                $popfile = $popfile_GIA;
        }
       elsif (($etproduct =~ /IPVPN/i || $etproduct =~ /MPLS/i)){
                $popfile = $popfile_IPVPN;
        }
        elsif (($etproduct =~ /IPTRANSIT/i )){
                $popfile = $popfile_IPTRANSIT;
        }

        my $lp;
        open($lp, $popfile);
        while(my $line = <$lp>) {
                chomp($line);
                my($t, $a, $b, $c, $d) = split(/\t/, $line);
                $v{$t} = "$a $b^$c";
                $loop_back{$t} = "$d";
        }
        close($lp);
        close(DDD);
        return \%v;
}

# Generate selects for POP
# Post: @selects
sub gen_poplist {
	my @selects;
	
	my $v = &load_pop();
	
	foreach my $key (sort numerically keys %$v) {
		my $popdescr = (split(/\^/, $$v{$key}))[0];
		my $tmp = "<option value=\"$key\">$popdescr</option>";
		push @selects, $tmp;
	}

	return @selects;
}

# Node to host Test - level 21
# Pre: uid, code, accno, tmplroot, svcscript, parentlevel
sub screen_nodetohost {
        my ($uid, $code, $accno, $tmplroot, $script, $level) = @_;
        my $ctrlfile = $tmplroot."/local/nodetohostp.htm";
        my @retvals = ();

        # This is the first sub-level of Looking Glass - level = 21
	$level = "$level"."1";
        
        if(-e $ctrlfile) {
		my $xx;
                open($xx, $ctrlfile);
                 while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g;
			$line =~ s/\*LEVEL\*/$level/g;
			if($line =~ /\*POPS\*/) {
				my @poplist = &gen_poplist();
				push @retvals, @poplist;
			} else {
                        	push @retvals, $line;
			}
                }
                close($xx);
        } else {
                push @retvals, "Cannot generate Node to Host Test page";
        }                
        
        return @retvals;
 
}               

# Node to host Trace - level 31
# Pre: uid, code, accno, tmplroot, svcscript, parentlevel
sub screen_nodetohostt {
        my ($uid, $code, $accno, $tmplroot, $script, $level) = @_;
        my $ctrlfile = $tmplroot."/local/nodetohostt.htm";
        my @retvals = ();

	# This is the first sub-level of Looking Glass - level = 31
	$level = "$level"."1";

        if(-e $ctrlfile) {
		my $xx;
                open($xx, $ctrlfile);
                 while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/$level/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g;
			if($line =~ /\*POPS\*/) {
                                my @poplist = &gen_poplist();
                                push @retvals, @poplist;
                        } else {
                        	push @retvals, $line;
			}
                 }
                close($xx);
        } else {
                push @retvals, "Cannot generate Node to Host Trace page";
        }

        return @retvals;

}
open(DEBUG, ">/data1/tmp_log/CHK_DNS_25052009");
# Node to Node Reach - level 41
# Pre: uid, code, accno, tmplroot, svcscript, parentlevel
sub screen_nodetonode {
        my ($uid, $code, $accno, $tmplroot, $script, $level) = @_;
        my $ctrlfile = $tmplroot."/local/nodetonode.htm";
        my @retvals = ();
	#print DEBUG "INSIDE sub screen_nodetonode in .pl\n";

        # This is the first sub-level of Looking Glass - level = 41
	$level = "$level"."1";
	#print DEBUG "level : $level \t ctrlfile: $ctrlfile \n";
        if(-e $ctrlfile) {
		my $xx;
                open($xx, $ctrlfile);
                 while(my $line = <$xx>) {
                        $line = &link_format($line, $code, $ctrlname);
                        $line =~ s/\*CODE\*/$code/g;
                        $line =~ s/\*LEVEL\*/$level/g;
			$line =~ s/\*ETPRODUCT\*/$encet/g;
			if($line =~ /\*POPS\*/) {
                                my @poplist = &gen_poplist();
                                push @retvals, @poplist;
                        } else {
                        	push @retvals, $line;
			}
                 }
                close($xx);
        } else {
                push @retvals, "Cannot generate Node to Node page";
        }
         
        return @retvals;
        
}
close (DEBUG);	


# View Primary DNS
# Pre: $uid, $mrwaccno
# Post: @vals
sub view_pridns {
	my ($uid, $accno, $mrwaccno, $view_type) = @_;

	# view type: 0=ALL, 1=WITH_OID
	if ($view_type eq '') {
		$view_type = 0;
	} else {
		$view_type += 0;
	}
	# $view_type = 1;  # for debug use only

	my($accno, $staffid) = &ex_staffid($uid);

	#my @vals = &search($uid, $secdnsdb);
	my $result;
	eval {
		my $a = ($mrwaccno) ? $mrwaccno : $accno;
		$result = CMS_OLSS::get_pridns_entries_for_account($uid);
	};

	my @retvals;

	if ($view_type == 0) {
		push @retvals, "<p class=header><b>List of domain records on the Reach Primary DNS server</b></p>";
	} else {
		push @retvals, 0;
	}
		

	if($#{$result} < 0) {
		push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>No record found on the Primary DNS server.</p></span>";
	} else {
		push @retvals, "<p class=text><table border=1 cellpadding=4 cellspacing=4>\n";
		if ($view_type == 1) {
			push @retvals, "<td class=thx>&nbsp;</td>";  # for selection column
		}
		push @retvals, "<td class=thx>Account</td><td class=thx>Domain</td>";
		push @retvals, "<td class=thx>Host</td><td class=thx>Record Type</td>";
		push @retvals, "<td class=thx>Parameter</td>";
		if ($view_type == 0) {
			push @retvals, "<td class=thx>Request Date</td>";
			push @retvals, "<td class=thx>Email Contact</td>";
		}
		# if ($view_type == 1) {
		# 	push @retvals, "<td class=thx>&nbsp;</td>";  # for selection column
		# }
		push @retvals, "</tr>\n";

		# $$row[9] is status
		# 1 = pending add, 2 = added, 3 = pending del, 4 = deleted
		# $$row[10] is object id (i.e. row id)
		for my $row (@$result) {
			#next if($$row[3] =~ /^(3|4)$/);
			
			my $accno     = $$row[0];
			my $domain    = $$row[1];
			my $host      = $$row[2];
			my $type_id   = $$row[3];
			my $type      = '';
			my $parm      = $$row[4]; 
			my $parm_web  = '';
			my $request_time = $$row[5];
			my $contact   = $$row[8];
			my $dbrowid   = $$row[10];

			my $status_id = $$row[9];
			my $status    = '';

			my $selected_status_hash = &list_pridns_status(2, $status_id);
			$status = $$selected_status_hash{$status_id};
			next if ($status =~ /delet/i);

			my $selected_type_hash = &list_pridns_type(2, $type_id);
			foreach my $k (keys %$selected_type_hash) {
				$type = $$selected_type_hash{$k};
			}

			if ($parm eq '') {
				$parm_web = '&nbsp;';
			} else {
				$parm_web = $parm;
			}

			my $tr = "<tr>";
			if ($view_type == 1) {
				# $tr .= "<td class=text>$dbrowid</td>\n";
				$tr .= "<td class=text><input type=checkbox name=PDNS_DBROWID value=$dbrowid></td>\n";
			}
			$tr .= "<td class=text>$accno</td><td class=text>$domain</td><td class=text>$host</td>\n";
			$tr .= "<td class=text>$type</td><td class=text>$parm_web</td>\n";
			if ($view_type == 0) {
				$tr .= "<td class=text NOWRAP>$request_time</td>\n";
				$tr .= "<td class=text>$contact</td>\n";
			}
			# if ($view_type == 1) {
			# 	$tr .= "<td class=text><input type=checkbox name=PDNS_DBROWID value=$dbrowid></td>\n";
			# }
			$tr .= "</tr>\n";
			push @retvals, $tr;
		}

		push @retvals, "</table><p class=text>\n";
	}

	return @retvals;
}

# Reload PRIMARY DNS
# Pre: uid, dom
# Post: 0 failmsg, 1 okmsg
sub reload_pridns {
        my ($uid, $dom, $mrwaccno) = @_;

        my @retvals;

        push @retvals, 1, "<p class=header>Reload domain</p>";
        push @retvals, "<p class=text>This function is coming soon.";

        return @retvals;

}



# Add Primary DNS
# Pre: $uid, $code, $accno, $dom, $pip, mrwaccno
# Post: 1 okmsg, 0 failmsg
sub add_pridns {
	open(DEBUG,">/data1/tmp_log/pridns_insideadd1");
	#print DEBUG "ENTERED\n";
	# my ($uid, $code, $accno, $dom, $pip, $mrwaccno) = @_;
	my ($uid, $code, $accno, $pdns_contact, $pdns_dom, $pdns_type_id, $pdns_host, $pdns_aip, $pdns_cnhost, $pdns_mxpv, $pdns_mxex, $pdns_ns, $pdns_tttext, $mrwaccno) = @_;

	my ($ref, @contactvals, $email);

        my $p = "p class=header";
        my $pt = "p class=text";

	#print DEBUG " uid: $uid\t code: $code\t accno: $accno\t pdns_contact: $pdns_contact\t pdns_dom: $pdns_dom\t pdns_type_id: $pdns_type_id\t pdns_host: $pdns_host\n pdns_aip: $pdns_aip\t pdns_cnhost: $pdns_cnhost\t pdns_mxpv: $pdns_mxpv\t pdns_mxex:$pdns_mxex\t pdns_tttext: $pdns_tttext\t mrwaccno: $mrwaccno\n";

	if($mrwaccno) {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</p></span><$pt><span style='font-weight:bold;font-style:bold;color:red;'>This account has not been given the right to take this action.</p></span>";
		return @vals;
	}

	$email = $pdns_contact;

	my @vals = ();

	#print DEBUG "Initial vals: @vals\n";
	# email contact
	if( ($email !~ /[\w\-]+\@[\w\-]+\.[\w\-]+/) || ($email eq "") ) {
	 push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</p></span><$pt><span style='font-weight:bold;font-style:bold;color:red;'><B>ERROR in Email Contact format. </B>($email)</p></span>";
		return @vals;
	}

	# domain name
	if ( ($pdns_dom =~ /\~|\\|\|\;|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\/|\<|\|\s>/) || ($pdns_dom eq "") ) {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</span></p><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Error in Domain. ($pdns_dom)</p></span>";
		return @vals;
	}

	# host name
	#if ( ($pdns_host =~ /\~|\\|\|\;|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\/|\<|\|\s>/) || ($pdns_host eq "") ) {
	if  ($pdns_host =~ /\~|\\|\|\;|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\/|\<|\|\s>/) {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</span></p><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Error in Host. ($pdns_host)</p></span>";
		return @vals;
	}


	my $selected_type_hash = &list_pridns_type(2, $pdns_type_id);
	my $selected_type = '';
	# assume one record only
	# push @vals, 0;
	foreach my $k (keys %$selected_type_hash) {
		$selected_type = $$selected_type_hash{$k};
		# push @vals, " selected type is $selected_type (passed in: $pdns_type_id)  "; 
	}
	# return @vals;

	my $pip       = $pdns_aip;  # A record ip address
	my $pcnhost   = $pdns_cnhost;  #  CNAME record new host
	my $pmxpv     = $pdns_mxpv;  #  MX record preference value
	my $pmxex     = $pdns_mxex;  #  MX record mail exchanger
        my $pns       = $pdns_ns;  #  NS value
	my $ptttext   = $pdns_tttext;  #  TXT record text message

	#print DEBUG "Assigning new values\t pip: $pip\n pcnhost: $pcnhost\t pmxpv: $pmxpv\t pmxex: $pmxex\t ptttext: $ptttext\n";
	my @pipv = split(/\./, $pip);

	# my $fields_ok = 1;
	my $err = '';

	# A record checking
	if ($selected_type =~ /^A$/) {
	if ($pdns_host eq "") {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</span></p><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Error in Host. ($pdns_host)</p></span>";
		return @vals;
        	}	
		my $ok = 0;
     		#print DEBUG "selected type: $selected_type\n";
		foreach my $i (0 .. $#pipv) {
			my $test = $pipv[$i];
			#print DEBUG "test: $test\n";
			if( ($test >= 0) && ($test <= 255) ) {
				if( ($ok == 0) && ($test == 0) ) {
					$ok = 0;
				} else {
					$ok += 1;
				}
			}
		}
		#print DEBUG "ok: $ok\n";
	
		if ( ($ok != 4) || ($pip !~ /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/)
			) {
			$err .= "Error in A record's IP Address ($pdns_aip)<br> ";
			# push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</span></p><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Error in A record's IP Address ($pdns_aip)</p></span>";
			# return @vals;
		}


	} elsif ($selected_type =~ /^CNAME$/) {
		#print DEBUG "selected type: $selected_type\n";
        if ($pdns_host eq "") {
                push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</span></p><$pt><s
pan style='font-weight:bold;font-style:bold;color:red;'>Error in Host. ($pdns_host)</p></span>";
                return @vals;
                }

	if ( ($pcnhost =~ /\~|\\|\|\;|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\/|\<|\|\s>/) || ($pcnhost eq "") ) {
			#print DEBUG "CName: $selected_type\n";
			$err .= "Error in CNAME record's New Host ($pcnhost)<br> ";
			# push @vals, "<$p>Error Encountered</p><$pt>Error in CNAME record's New Host ($pcnhost)</p>";
			# return @vals;
		}
	
	} elsif ($selected_type =~ /^MX$/) {
		 #print DEBUG "selected type: $selected_type\n";
		if ( ($pmxpv =~ /\D/) || ($pmxpv eq "") ) {
			 #print DEBUG "MX: $selected_type\n";
			$err .= "Error in MX record's Preference Value ($pmxpv)<br> ";
			# push @vals, "<$p>Error Encountered</p><$pt>Error in MX record's Preference Value ($pmxpv)</p>";
			# return @vals;
		}
		if ( ($pmxex =~ /\~|\\|\|\;|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\/|\<|\|\s>/) || ($pmxex eq "") ) {
			$err .= "Error in MX record's Mail Exchanger ($pmxex)<br> ";
			# push @vals, "<$p>Error Encountered</p><$pt>Error in MX record's Mail Exchanger ($pmxex)</p>";
			# return @vals;
		}
	} elsif ($selected_type =~ /^NS$/) {
                 #print DEBUG "selected type: $selected_type\n";
                if ( ($pdns_ns =~ /\~|\\|\|\;|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\/|\<|\|\s>/) || ($pdns_ns eq "") ) {
                        #print DEBUG "NS:  $selected_type\n";
                        $err .= "Error in NS record ($pdns_ns)<br> ";
                        # push @vals, "<$p>Error Encountered</p><$pt>Error in NS record($pdns_ns)</p>";
                        # return @vals;
                }
	} elsif ($selected_type =~ /^TXT$/) {
        if ($pdns_host eq "") {
                push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</span></p><$pt><s
pan style='font-weight:bold;font-style:bold;color:red;'>Error in Host. ($pdns_host)</p></span>";
                return @vals;
                }

		#print DEBUG "selected type: $selected_type\n";
		if ($ptttext eq "") {
			$err .= "Error in TXT record's Text ($ptttext)<br> ";
		}
	} else {
	}

	if ($err ne '') {
		my $errmsg = "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</p></span><$pt><span style='font-weight:bold;font-style:bold;color:red;'>$err</p></span>";
		push @vals, "$errmsg";
		return @vals;
	}

	#print DEBUG "err: $err\n";

	my $res = 0;
	my $time = time;
	my $parm = '';

	my $dbaccno = CMS_OLSS::get_accno_for_pridns_domain($pdns_aip);
	#my $dbaccno;

	#print DEBUG "dbaccno: $dbaccno\n";
	# assume @currvals has the correct info
	if(not defined $dbaccno) {
		#  test account - do nothing
		if($accno =~ /$testacc/i) {
			$res = 1;
		#print DEBUG "res: $res\n";
		} else {
			eval {
				#print DEBUG "CHECK if ADD OCCURS\n";
				#print DEBUG " uid: $uid\t code: $code\t accno: $accno\t \n";
				#print DEBUG "selected_type: $selected_type\n ";
				if ($selected_type =~ /^A$/) {
					$parm = $pdns_aip;
					#print DEBUG "parm: $parm\n";
				} elsif ($selected_type =~ /^CNAME$/) {
					$parm = $pdns_cnhost;
					#print DEBUG "parm: $parm\n";
				} elsif ($selected_type =~ /^MX$/) {
					$parm = "$pdns_mxpv\|$pdns_mxex";
					#print DEBUG "parm: $parm\n";	
                                } elsif ($selected_type =~ /^NS$/) {
                                        $parm = $pdns_ns;
                                        #print DEBUG "parm: $parm\n";
				} elsif ($selected_type =~ /^TXT$/) {
					$parm = "$pdns_tttext";
					#print DEBUG "parm: $parm\n";
				}
		#print DEBUG "parm: $parm\n";
		#print DEBUG "res before: $res\n";
				$res = CMS_OLSS::add_pridns_entry($uid, $accno, $pdns_contact, $pdns_dom, $pdns_host, $pdns_type_id, $parm);

		#print DEBUG "res after: $res\n";
		#print DEBUG "add_pridns_entry of res: $res\n";
			};
		}
	} else {
		#print DEBUG "CHECK if UPDATE occurs\n";
		# if exists, need to update
		if($accno !~ /^$dbaccno$/) {
			push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</p></span><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Cannot verify account.</p></span>";
			return @vals;
		}
		eval {
			$res = 0;
			#print DEBUG "inside update eval condition\n";		
			#print DEBUG "accno inside eval: $accno\t dom inside eval:$pdns_dom\t pip inside eval: $pip\n";
			 if ($selected_type =~ /^A$/) {
                                        $parm = $pdns_aip;
                                        #print DEBUG "parm: $parm\n";
                                } elsif ($selected_type =~ /^CNAME$/) {
                                        $parm = $pdns_cnhost;
                                        #print DEBUG "parm: $parm\n";
                                } elsif ($selected_type =~ /^MX$/) {
                                        $parm = "$pdns_mxpv\|$pdns_mxex";
                                        #print DEBUG "parm: $parm\n";
                                } elsif ($selected_type =~ /^TXT$/) {
                                        $parm = "$pdns_tttext";
                                        #print DEBUG "parm: $parm\n";
                                }
			$res = CMS_OLSS::update_pridns_entry($uid, $accno, $pdns_contact,$pdns_dom, $pdns_host, $pdns_type_id, $parm);
			#print DEBUG "update_pridns_entryn of  res: $res\n";
		};
	}

	#print DEBUG "\n\nres values: $res\n\n";
        if(($res != 2) && ($res != 3)) {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:blue;'>Add Primary DNS</p><$pt></span>";
		push @vals, "<span style='font-weight:bold;font-style:bold;color:blue;'>The domain record of $pdns_dom ($selected_type record) has been added.</span>";
		push @vals, "<p class=text>\n";
		push @vals, "<span style='font-weight:bold;font-style:bold;color:blue;'>The Primary DNS server reloads once every 2 hours.</span>";
		push @vals, "</p>";
		push @vals, "<p class=text>\n";
		push @vals, "<span style='font-weight:bold;font-style:bold;color:blue;'>The following information is required for you to set up your server:<$pt></span>";
                push @vals, "<span style='font-weight:bold;font-style:bold;color:blue;'>Dns servers:<br>ns4.telstraglobal.net(134.159.4.145)</br></span>";

	} else {
                if ($res == 2) {
                $res = 'User Exceeded Allowed Limit for Primary DNS Service - max. limit is 10';
                push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered: ($res).</p></span><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Domain record was not added to the Primary DNS server.</span>";
                } elsif ($res != 3) {
                $res = 'Duplicated IP';
                push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered: ($res).</p></span><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Domain record was not added to the Primary DNS server.</span>";
                } else {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered: ($res).</p></span><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Domain record was not added to the Primary DNS server.</span>";
		#CMS::add_error("$res");
                }
	}

        #$res &&= &log_entry($chpdnslog, "ADD_PRIDNS", "Userid: $uid, $accno, $pdns_dom, $pdns_host, $selected_type($pdns_type_id), $parm");
#print DEBUG "log_entry: chpdnslog - $chpdnslog\n res: $res\n";

	#print DEBUG "RETURN VAL: @vals\n";
close (DEBUG);
	return @vals;
		
}

open(DEBUG,">/data1/tmp_log/CHK_DNS_25052009");
# Remove Primary DNS
# Pre: $uid, $code, $accno, $dbrowid
# Post: 0 failmsg, 1 okmsg
sub del_pridns {
	my($uid, $code, $accno, $dbrowid) = @_;

	my @retvals;
	my @dbrowids = split("\0", $dbrowid);

	my $errmsg = '';

	if ($dbrowid eq '') {
		push @retvals, "<p class=header><span style='font-weight:bold;font-style:bold;color:red;'>Primary DNS Removal</p>i</span>";
 		push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>No record selected.</p></span>";
		return @retvals;
	}
	#print DEBUG "uid: $uid \t accno: $accno \t dbrowid: $dbrowid\n";
	my $rowsok = 1;
	foreach my $rid (@dbrowids) {
		# my $dbaccno = CMS_OLSS::get_accno_for_pridns_dbrowid($rid);
		my $rec = CMS_OLSS::get_pridns_entries_for_dbrowid($rid);
		my $dbaccno  = $$rec[0][0];  # account number - first element of first array record row
		my $dbdomain = $$rec[0][1];
		my $dbhost   = $$rec[0][2];
		my $dbtype   = $$rec[0][3];
		my $dbvalue  = $$rec[0][4];
		if ($accno !~ /^$dbaccno$/) {
			$rowsok = 0;
			#print DEBUG " ACCNO not eq to DDBBACCNO\n";
			$errmsg .= "The selected record cannot be removed.<br>";
			$errmsg .= "(ref: $accno $rid $dbaccno $dbdomain $dbhost $dbtype $dbvalue).<br>";
			#last;
		}
	}
	#print DEBUG "rid: $rid\n";
	if (! $rowsok) {
		push @retvals, "<p class=header><span style='font-weight:bold;font-style:bold;color:red;'>Primary DNS Removal</p></span>";
 		push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>$errmsg</p></span>";
		return @retvals;
	}

	$rowsok = 1;
	foreach my $rid (@dbrowids) {
		my $rec = CMS_OLSS::get_pridns_entries_for_dbrowid($rid);
		my $dbaccno  = $$rec[0][0];  # account number - first element of first array record row
		my $dbdomain = $$rec[0][1];
		my $dbhost   = $$rec[0][2];
		my $dbtype   = $$rec[0][3];
		my $dbvalue  = $$rec[0][4];
		#print DEBUG "$dbdomain:dbdomain dbhost:$dbhost dbvalue $dbvalue:dbvalue\n";
		my $selected_type_hash = &list_pridns_type(2, $dbtype);
		my $selected_type = '';
		foreach my $k (keys %$selected_type_hash) {
			$selected_type = $$selected_type_hash{$k};
		}
		
		my $res = 0;
		eval {
        $res = 2;
	#print DEBUG " in .pl delete_pridns_entry \n uid: $uid \t accno:$accno \t rid: $rid\n";

        CMS_DB::connect_to_database;
	$res =  CMS_PRIDNS_DB::del_pridns_entry($uid, $accno, $rid);
        CMS_DB::disconnect_from_database;
		#$res =  CMS_PRIDNS_DB::delete_pridns_entry($uid, $accno, $rid);
		#print DEBUG " INSIDE eval:  \t res: $res\n";
		};
		#$res = 1; # debug use only
	 	#$res &&= &log_entry($chpdnslog, "DELETE_PRIDNS", "Userid: $uid, $accno, $rid, $dbdomain, $dbhost, $selected_type($dbtype), $dbvalue");
		#print DEBUG " res: $res\n";
		if ($res == 0) {
			$rowsok = 0;
			#print DEBUG " RES is NOT TRUE\n";
			$errmsg .= "The record (ref: $accno $rid $dbaccno $dbdomain $dbhost $selected_type($dbtype) $dbvalue) cannot be removed.<br>"; 
		}

	}

	if ($rowsok) {
		push @retvals, "<p class=header><span style='font-weight:bold;font-style:bold;color:red;'>REMOVED PRIMARY NAME SERVER SERVICE</span></p>";
		push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>ACL file is updated.</span></p>";
		#push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>The domain record of $dbdomain  has been verified for $dbvalue.</span></p>";
		push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>The Primary DNS server reloads once every 2 hours.</span></p>";
 #	 	push @retvals, "<p class=header><span style='font-weight:bold;font-style:bold;color:red;'>Primary DNS Removal</p></span>";
 	#	push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>Selected domain record(s) has been removed.</p></span>";
	} else {
 	 	push @retvals, "<p class=header><span style='font-weight:bold;font-style:bold;color:red;'>Primary DNS Removal</p></span>";
 		push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>Some of selected domain record(s) cannot be removed.</p></span>";
 		push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>$errmsg</p></span>";
	}



	return @retvals;
	

}
close (DEBUG);

######################
# Secondary DNS

# Pre: $uid, $code, $accno, $tmplroot, mrwaccno (for ro accounts)
# Post: @retvals, 0
sub screen_secdns {
	my ($uid, $code, $accno, $tmplroot, $mrwaccno) = @_;

	my $ctrlfile = $tmplroot."/local/secdns.htm";
	my @screenoutput = ();
	my $msg = "No technical contact email found. ";
	$msg .= "Please go to account administration to update your details.";

	# Temp - in future will look at CMS DB
	#my @vals = &retrieve($accno, $techcdb);

	# Future - look at CMS
	eval {
		if($mrwaccno) {
			$techcontact = CMS_OLSS::get_tech_contact_details($mrwaccno);
		} else {
                	$techcontact = CMS_OLSS::get_tech_contact_details($accno);
		}
        };

        my $ref = $$techcontact[0];
	my @tt = @$ref;

        #print "Content-type: text/json\n\n<P>DEBUG ($#{@$techcontact}) (@tt) ($@)\n";
	my $xx;
	open($xx, $ctrlfile) or return 0;
	while(my $line = <$xx>) {
		$line =~ s/\*CODE\*/$code/g;
		$line =~ s/\*ETPRODUCT\*/$encet/g;
		$line =~ s/\*ACCNO\*/$accno/g;
		if($line =~ /\*TECHEMAIL\*/) {
			if($tt[4] !~ /\w+\@\w+/) {
				$line =~ s/\*TECHEMAIL\*/$msg/g;
			} else {
				$line =~ s/\*TECHEMAIL\*/$tt[4]/g;
			}
		} 
			
		$line = &link_format($line, $code, $ctrlname);
		push @screenoutput, $line;
	}
	close($xx);

	return @screenoutput;
}

# sec dns - sublevel 3
# Pre: $uid, $code, $accno, $email, $dom, $pip
# Post: @retvals, 0
sub screen_ssecdns {
	my($uid, $code, $accno, $level, $sec_contact, $dom, $pip, $mrwaccno) = @_;

	my @vals = ();
	
	SWITCH: {
		($level == 31) && do {
			open (DEBUG,">/data1/tmp_log/sec_dns_2506");
			#print DEBUG "entered sec dns pl sub level\n";
			@vals = &add_secdns($uid, $code, $accno, $sec_contact, $dom, $pip, $mrwaccno);
			last SWITCH;
		};
		($level == 32) && do {
			@vals = &view_secdns($accno, $mrwaccno);
			last SWITCH;
		};
		($level == 33) && do {
			@vals = &del_secdns($uid, $code, $accno, $dom, $mrwaccno);
			last SWITCH;
		};
		($level == 34) && do {
			@vals = reload_secdns($uid, $dom, $mrwaccno);
			last SWITCH;
		};
close DEBUG;

			
	};

	shift(@vals);

	return @vals;

}

# Reload DNS
# Pre: uid, dom
# Post: 0 failmsg, 1 okmsg
sub reload_secdns {
	my ($uid, $dom, $mrwaccno) = @_;

	my @retvals;

	push @retvals, 1, "<p class=header>Reload domain</p>";
	push @retvals, "<p class=text>This function is coming soon.";

	return @retvals;

}

# View Sec DNS
# Pre: $uid
# Post: @vals
sub view_secdns {
	my ($uid, $mrwaccno) = @_;

	my($accno, $staffid) = &ex_staffid($uid);

	#my @vals = &search($uid, $secdnsdb);
	my $result;
	eval {
		my $a = ($mrwaccno) ? $mrwaccno : $accno;
		$result = CMS_OLSS::get_secdns_entries_for_account($a);
	};

	my @retvals;
	
	push @retvals, "<p class=header><b>List of domains on the Reach secondary DNS server</b></p>";

	if($#{$result} < 0) {
		push @retvals, "<p class=text><b>No domains found on the secondary DNS server.</b></p>";
	} else {
		push @retvals, "<p class=text><table border=1 cellpadding=4 cellspacing=4>\n";
		push @retvals, "<td class=thx>Domain</td><td class=thx>Primary DNS Server IP Address</td>";
		push @retvals, "<td class=thx>Request date</td></tr>\n";

		# $$row[3] is status
		# 1 = pending add, 2 = added, 3 = pending del, 4 = deleted
		for my $row (@$result) {
			next if($$row[3] =~ /^(3|4)$/);
			my $tr = "<tr><td class=text>";
			my @reqtime = split(/\./,$$row[2]);
			$tr .= "$$row[0]</td><td class=text>$$row[1]</td><td class=text>$reqtime[0]</td></tr>\n";
			push @retvals, $tr;
			shift @reqtime;
		}

		push @retvals, "</table><p class=text>\n";
	}

	return @retvals;
}

# Remove Sec DNS
# Pre: $uid, $code, $accno, $dom
# Post: 0 failmsg, 1 okmsg
sub del_secdns {
	open (ANJAN, ">/data1/tmp_log/SEC_TEST");
	my($uid, $code, $accno, $dom) = @_;
	#print ANJAN "inside delete: accno: $accno\t dom:$dom\n";
	my @retvals;
	my $res;

	#my @currvals = &retrieve($dom, $secdnsdb);
	my $dbaccno = CMS_OLSS::get_accno_for_dns_domain($dom);
	#print ANJAN "del sec dbaccno:$dbaccno dom:$dom\n";

	if($accno !~ /^$dbaccno$/) {
		push @retvals, "<p class=header><span style='font-weight:bold;font-style:bold;color:red;'>Secondary DNS Domain Removal</p></span>";
		push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>The domain $dom cannot be removed.</p></span>";
		if($mrwaccno) {
			push @retvals, "<p class=text>This account has insufficient privileges.</p>\n";
		}
		return @retvals;
	}

	#my $res = &delete($dom, $secdnsdb);
	eval {
		$res = CMS_OLSS::delete_secdns_entry($accno, $dom);
	};

	$res &&= &log_entry($chdnslog, "DELETE_SECDNS", "Userid: $uid, $accno, $dom");

	if($res) {
		push @retvals, "<p class=header><span style='font-weight:bold;font-style:bold;color:blue;'>Secondary DNS Domain Removal</p></span>";
		push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:blue;'>The domain $dom has been removed.</p></span>";
	} else {
		push @retvals, "<p class=header><span style='font-weight:bold;font-style:bold;color:red;'>Secondary DNS Domain Removal</p></span>";
	push @retvals, "<p class=text><span style='font-weight:bold;font-style:bold;color:red;'>The domain $dom cannot be removed.</p></span>";
}

return @retvals;

close (ANJAN);
}

# Add Sec DNS
# Pre: $uid, $code, $accno, $dom, $pip, mrwaccno
# Post: 1 okmsg, 0 failmsg
sub add_secdns {
	open(ANJAN,">/data1/tmp_log/SEC_TEST");
	#print ANJAN "Entered add_sec in pl file\n";
my ($uid, $code, $accno, $sec_contact, $dom, $pip, $mrwaccno) = @_;

#my @contactvals = &retrieve($accno, $techcdb);
#my $techemail = $contactvals[5];
my ($ref, @contactvals, $email);

my $p = "p class=header";
my $pt = "p class=text";
my $email = $sec_contact;
my @vals = ();

#if( ($email !~ /^\w*(\.)*.*\@\w*/) || ($email eq "") ) {
	if( ($email !~ /[\w\-]+\@[\w\-]+\.[\w\-]+/) || ($email eq "") ) {
	push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</p></span><$pt><span style='font-weight:bold;font-style:bold;color:red;'><B>ERROR in Email Contact format. </B>($email)</p></span>";
                return @vals;
        }



	if($mrwaccno) {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</p></span><$pt><span style='font-weight:bold;font-style:bold;color:red;'>This account has not been given the right to take this action.</span></p>";
		return @vals;
	}

	eval {
		$ref = CMS_OLSS::get_tech_contact_details($accno);
	};

	if(!$ref) {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</p><$pt>Cannot retrieve technical contact email address.</span></p>";
		return @vals;
	}

	my $resref = $$ref[0];
	my @tt = @$resref;
#	$email = $tt[4];

	my @vals = ();
	
#	if($email !~ /^\w*(\.)*.*\@\w*/) {
#		push @vals, 0, "<$p>Error Encountered</p><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Error in email address format. Please update in account administration. ($email)</span></p>";
#		return @vals;
#	}
	if($dom =~ /\~|\\|\|\;|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\/|\<|\>/ || ($dom eq "")) {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</span></p><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Error in domain name</span></p>";
		return @vals;
	}

	#if($pip !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/) {
	my @pipv = split(/\./, $pip);
	my $ok = 0;
	foreach my $i (0 .. $#pipv) {
		my $test = $pipv[$i];
		if( ($test >= 0) && ($test <= 255) ) {
			if( ($ok == 0) && ($test == 0) ) {
				$ok = 0;
			} else {
				$ok += 1;
			}
		}
	}

	if($ok != 4) {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</span></p><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Error in primary DNS server's IP address</span></p>";
		return @vals;
	}

	if($pip !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/) {
		push @vals,  "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</span></p><$pt><span style='font-weight:bold;font-style:bold;color:red;'>Error in primary DNS server's IP address</span></p>";
		return @vals;
	}

	#my @currvals = &retrieve($dom, $secdnsdb);
	my $res = 0;
	my $time = time;
	#print ANJAN "dom:$dom\n";
	my $dbaccno = CMS_OLSS::get_accno_for_dns_domain($dom);
	#print ANJAN "dbaccno:$dbaccno\n";

	# assume @currvals has the correct info
	if(not defined $dbaccno) {
		#  test account - do nothing
		if($accno =~ /$testacc/i) {
		#print ANJAN "accno:$accno\ttestacc:$testacc\n"; 
			$res = 1;
		} else {
			eval {
				#print ANJAN "inside add_sec calling fn\n";
				$res = CMS_OLSS::add_secdns_entry($accno, $dom, $pip,$email);
			};
			#$res = &insert($dom, $secdnsdb, $dom, $pip, $accno, $time, $time);
		}
	} else {
		# if exists, need to update
		if($accno !~ /^$dbaccno$/) {
			#print ANJAN "before error:accno:$accno\tdbaccno:$dbaccno\n";
			push @vals, "<$p>Error Encountered</p><$pt>Cannot verify account.</p>";
			return @vals;
		}
		eval {
			#$res = &update($dom, $secdnsdb, $dom, $pip, $accno, $currvals[3], $time);
			#print ANJAN "inside add_Sec before update\n";
			$res = CMS_OLSS::update_secdns_entry($accno, $dom, $pip, $email);
		};
	}

	$res &&= &log_entry($chdnslog, "ADD_SECDNS", "Userid: $uid, $accno, $dom, $pip");

	if($res) {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:blue;'>Add Secondary DNS</span></p><$pt>";
		push @vals, "<span style='font-weight:bold;font-style:bold;color:blue;'>The domain $dom with primary DNS server of $pip has been added.</span>";
		push @vals, "<p class=text>\n";
		push @vals, "<span style='font-weight:bold;font-style:bold;color:blue;'>The secondary DNS server reloads once every 2 hours. Please wait</span> ";
		push @vals, "<span style='font-weight:bold;font-style:bold;color:blue;'>for 2 hours before redelegating your domain.</span></p>";
		push @vals, "<p class=text>\n";
 		push @vals, "<span style='font-weight:bold;font-style:bold;color:blue;'>The following information is required for you to set up your server:</span><$pt>";
		push @vals, "<span style='font-weight:bold;font-style:bold;color:blue;'>Our dns server is: ns1.telstraglobal.net</span>";

	} else {
		push @vals, "<$p><span style='font-weight:bold;font-style:bold;color:red;'>Error Encountered</p><$pt>Domain was not added to the secondary DNS server.</span>";
	}

	return @vals;
		
close (ANJAN);
}




# Secondary MX

# Pre: $uid, $code, $accno, $tmplroot  
# Post: @retvals, 0
sub screen_secmx {
        my ($uid, $code, $accno, $tmplroot, $mrwaccno) = @_;

        my $ctrlfile = $tmplroot."/local/secmx.htm";
        my @screenoutput = ();
        my $xx;
 
        open($xx, $ctrlfile) or return 0;
        while(my $line = <$xx>) {
                $line =~ s/\*CODE\*/$code/g;
		$line =~ s/\*ETPRODUCT\*/$encet/g;
                $line =~ s/\*ACCNO\*/$accno/g;
                $line = &link_format($line, $code, $ctrlname);
                push @screenoutput, $line;
        }
        close($xx);
 
        return @screenoutput;
}


# Pre: $userid, $oenc, $accno, $tmplroot, $level, $dom, $add, $view
# Pst: @retvals, 0 (fail)
sub screen_dosecmx {
	my($userid, $oenc, $accno, $tmplroot, $level, $dom, $add, $view, $mrwaccno) = @_;

	my @vals = ();

	$add = 0 if($view);

	# 3 actions - add, del, view
	SWITCH: {
		$add && do {
			@vals = &add_secmx($accno, $dom, $userid, $mrwaccno);
			last SWITCH;
		};
		!$add && !$view && do {
			@vals = &del_secmx($accno, $dom, $userid, $mrwaccno);
			last SWITCH;
		};
		$view && do {
			@vals = &list_secmx($accno, $dom, $mrwaccno);
			last SWITCH;
		};
	};

	shift(@vals);

	return @vals;

}

# list sec mx
sub list_secmx {
	my($accno, $dom, $mrwaccno) = @_;

	my @vals = ();
	my @doms = ();
	my $p = "p class=header";
	my $pt = "p class=text";
	my $td = "td class=text";

        my $secmxentries;
	eval {
		my $a = ($mrwaccno) ? $mrwaccno : $accno;
		$secmxentries = CMS_OLSS::get_secmx_entries_for_account($a);
	};

	if($@) {
		&log_entry($internalerror, "SECMX LIST", "$@");
		push @vals, 1, "<$p>List of domains in the Secondary MX</p>";
		push @vals, "No entries extracted.\n";
		return @vals;
	} else {
		push @vals, 1, "<$p>List of domains in the Secondary MX</p>";
	}


	push @vals, "<table cellpadding=5 cellspacing=5 border=1 width=100%>\n";
	push @vals, "<tr><td class=thx width=40%>Domain name</td><td class=thx width=60%>Request date</td>\n";

	# NB: $$row[2] is status
	# 1 = Pending Add, 2 = Added, 3 = pending del, 4 = deleted
        for my $row (@$secmxentries) {
		next if($$row[2] =~ /^(3|4)$/); #ignore pending del or deleted entries
		my $rw = "<tr><td class=text>";
		$rw .= "$$row[0]</td><td class=text>$$row[1]</td></tr>\n";
		push @vals, $rw;
			
                #print "DEBUG1: (@$row)\n";
        }

	push @vals, "</table>\n";

	return @vals;
}

# Add sec mx
# Pre: Accno, domain, userid
sub add_secmx {
	my ($accno, $dom, $userid, $mrwaccno) = @_;

	my @vals = ();
	my $p = "p class=header";
	my $pt = "p class=text";

	my $res;
	my$time = time;

	if($mrwaccno) {
		push @vals, 0, "<$p>Add to Secondary MX Error</p>";
		push @vals, "<$pt>This account has insufficient privilege to add.</p>\n";
		return @vals;
	}

	if($dom !~ /^\w*\./) {
		push @vals, 0, "<$p>Add to Secondary MX Error</p>";
		push @vals, "<$pt>The domain $dom could not be added to the secondary MX service (1).";
		return @vals;
	}
	
	#my @currvals = &retrieve($dom, $secmxdb);

	my $dbaccno = CMS_OLSS::get_accno_for_mx_domain($dom);
	#print "<P>DEBUG: $dbaccno\n";

	if( (!$dbaccno) || ($dbaccno =~ /^$accno$/) ) {
		my $addres;
		# test account - do nothing
		if($accno =~ /$testacc/i) {
			$addres = 1;
		} else {

			eval {
				$addres = CMS_OLSS::add_secmx_entry($accno, $dom);
			};
			if($@) {
				&log_entry($internalerror, "SECMX ADD", "$@");
				$res = 0;
			} else {
				$res = 1;
			}
			#$res = &insert($dom, $secmxdb, $dom, $accno, $time);

		}
	} else {
			
		if($accno !~ /^$dbaccno$/) {
			push @vals, 0, "<$p>Add to Secondary MX Error</p>";
			push @vals, "<$pt>The domain $dom could not be added to the secondary MX service. ";
			return @vals;
		}
		#$res = &update($dom, $secmxdb, $dom, $accno, $time);
		# adding a sec MX domain is a non-event, hence just return 1
		$res = 1;
	}

	$res &&= &log_entry($chmxlog, "ADD_SECMX", "Userid: $userid, $accno, $dom");

	if($res) {
		push @vals, 1, "<$p>Add to Secondary MX</p>";
		push @vals, "<$pt>The domain $dom has been added to the secondary MX service.";
	} else {
		push @vals, 0, "<$p>Add to Secondary MX Error</p>";
		push @vals, "<$pt>The domain $dom could not be added to the secondary MX service. ($CMS_OLSS::error)";
	}

	return @vals;

}

# Delete sec mx
# Pre: accno, domain, userid
sub del_secmx {
        my ($accno, $dom, $userid, $mrwaccno) = @_;
        
        my @vals = ();
        my $p = "p class=header";
        my $pt = "p class=text";
 
        my $res = 0;
        my$time = time;

	if($mrwaccno) {
		push @vals, 0, "<$p>Delete from Secondary MX Error</p>";
		push @vals, "<$pt>This account has insufficient privilege.</p>\n";
		return @vals;
	}

        if($dom !~ /^\w*\./) {
                push @vals, 0, "<$p>Delete from Secondary MX Error</p>";
                push @vals, "<$pt>The domain $dom could not be removed from the secondary MX service.";
                return @vals;
        }
        
        
        #my @currvals = &retrieve($dom, $secmxdb);
	my $dbaccno = CMS_OLSS::get_accno_for_mx_domain($dom);

	#print "<P> DEBUG ($dbaccno) ($dom)\n";
                        
        #if($dbaccno =~ /^0$/) {
	if(not defined $dbaccno) {
		push @vals, 0, "<$p>Delete from Secondary MX</p";
		push @vals, "<$pt>The domain $dom was not found in the secondary MX service.";
        } else {
                if($accno !~ /^$dbaccno$/) {
                        push @vals, 0, "<$p>Delete from Secondary MX Error</p>";
                        push @vals, "<$pt>The domain $dom could not be removed from the secondary MX service.";
                        
                } else {
	                #$res = &delete($dom, $secmxdb);
			eval {
				$res = CMS_OLSS::delete_secmx_entry($accno, $dom);
			};

			$res &&= &log_entry($chmxlog, "DELETE_SECMX", "Userid: $userid, $accno, $dom");

		        if( ($res) && (!$@) ) {
                		push @vals, 1, "<$p>Delete from Secondary MX</p";
		                push @vals, "<$pt>The domain $dom has been removed from the secondary MX service.";
        		} else {
				&log_entry($internalerror, "SECMX DEL", "$@");
				push @vals, 0, "<$p>Delete from Secondary MX Error</p>";
				push @vals, "<$pt>The domain $dom could not be removed from the secondary MX service.";
			}
	        }
        }
          
        return @vals;

}


# Usenet news
# Pre: $uid, $code, $accno, $tmplroot
# Post: @retvals, 0
sub screen_news {
	my ($uid, $code, $accno, $tmplroot, $mrwaccno) = @_;
	my $ctrlfile = $tmplroot."/local/news.htm";
        my @screenoutput = ();
	my $xx;

        open($xx, $ctrlfile) or return 0;
        while(my $line = <$xx>) {
                $line =~ s/\*CODE\*/$code/g;
		$line =~ s/\*ETPRODUCT\*/$encet/g;
                $line =~ s/\*ACCNO\*/$accno/g;
                $line = &link_format($line, $code, $ctrlname);
                push @screenoutput, $line;
        }
        close($xx); 
        
        return @screenoutput;
}

# Do usenet news
# Pre: $userid, $oenc, $accno, $tmplroot, $level, $incominghost, $outgoinghost, $pathalias, $type, $action
# Post: @retvals, 0 (fail)
sub screen_donews {
        my($userid, $oenc, $accno, $tmplroot, $level, $incominghost, 
		$outgoinghost, $pathalias, $type, $action, $mrwaccno) = @_;

        my @vals = ();
	my ($view, $add);

	if($action =~ /view/i) {
		$view = 1;
	}
	if($action =~ /add/i) {
		$add = 1;
	}

        # 3 actions - add, del, view
        SWITCH: {
                $add && do {
                        @vals = &add_news($userid, $accno, $incominghost, $outgoinghost, $pathalias, $type, $mrwaccno);
                        last SWITCH;
                };
                !$add && !$view && do {
                        @vals = &del_news($userid, $accno, $incominghost, $outgoinghost, $pathalias, $type, $mrwaccno);
                        last SWITCH;
                };
                $view && do {
                        @vals = &list_news($accno, $mrwaccno);
                        last SWITCH;
                };
        };

        shift(@vals);

        return @vals;   

}

# Add news service
# NB: incominghost is the Customer's incoming hostname, ie when Reach pushes to Customer
# Pre: userid, $accno, $incominghost, $outgoinghost, $pathalias, $type
# Post: 1 okmsg, 0 failmsg
sub add_news {
	my($uid, $accno, $incominghost, $outgoinghost, $pathalias, $type, $mrwaccno) = @_;

        my ($ref, $dbaccno, @contactvals, $techemail);
	my $p = "p class=header";
        my $pt = "p class=text";

	if($mrwaccno) {
		push @vals, 0, "<$p>Error Encountered</p><$pt>This account has insufficient privileges.</p>";
                return @vals;
        }
        
	# get the tech email        
        eval {
                $ref = CMS_OLSS::get_tech_contact_details($accno);
	        $dbaccno = CMS_OLSS::get_accno_for_news($incominghost);

        };

        my $resref = $$ref[0];
        my @tt = @$resref;
        $techemail = $tt[4];


	my @vals;
	my $delay = ($type =~ /delay/i) ? 10 : 0;

	# validate fields
	if($incominghost !~ /^\w*-?\w*\.\w*/) {
		push @vals, 0, "<$p>Error Encountered</p><$pt>Error in incoming host name ($incominghost).</p>";
		return @vals;
	}
	if($outgoinghost !~ /^\w*-?\w*\.\w*/) {
                push @vals, 0, "<$p>Error Encountered</p><$pt>Error in outgoing host name.</p>";
                return @vals;
        }
	if($pathalias =~ /(;|:|\||\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\{|\}|\/|\+)/) {
		push @vals, 0, "<$p>Error Encountered</p><$pt>Error in pathalias.</p>";
		return @vals;
	}

	if(not defined $dbaccno) {
		#  test account - do nothing
		if($accno =~ /$testacc/i) {
			$res = 1;
		} else {

			eval {
       	                	$res = CMS_OLSS::add_news_entry($accno, $incominghost, $outgoinghost, $pathalias, $delay);
       	         	};

		}
	} else {
		# if exists, need to update
		if($accno !~ /^$dbaccno$/) {
			push @vals, 0, "<$p>Error Encountered</p><$pt>Cannot verify account.</p>";
                        return @vals;
                }

		# test account - do nothing
		if($accno =~ /$testacc/i) {
			# $testacc test account - do nothing
			$res = 1;
		} else {

			eval {
       	                 	#$res = &update($dom, $secdnsdb, $dom, $pip, $accno, $currvals[3], $time);
       		                $res = CMS_OLSS::delete_news_entry($accno, $incominghost);
				#print "<P>DEBUG ($res)\n";
                        	$res &&= CMS_OLSS::add_news_entry($accno, $incominghost, $outgoinghost, $pathalias, $delay);
				#print "<P>DEBUG1 ($res) ($accno, $incominghost, $outgoinghost, $pathalias, $delay)\n";
                	};

		}
        }

	my($reachin, $reachout, $reachpath);

	if($delay) {
		$reachin = "news.telstraglobal.net";
		$reachout = "news.telstraglobal.net";
		$reachpath = "news.telstraglobal.net";
	} else {
		$reachin = "news.telstraglobal.net";
		$reachout = "news.telstraglobal.net";
		$reachpath = "news.telstraglobal.net";
	}

	$res &&= &log_entry($chnewslog, "ADD_NEWS", "Userid: $uid, $accno, $incominghost, $outgoing, $delay");
		
        if($res) {
                push @vals, 1, "<$p>Add Usenet news feed</p><$pt>";
                push @vals, "The Usenet news feed has been added.";
                push @vals, "<p class=text>\n";
                push @vals, "The following information is required for you to set up your server:<$pt><ol>";
		push @vals, "<li class=text>You send news to: $reachin";
		push @vals, "<li class=text>You receive news from: $reachout";
		push @vals, "<li class=text>Our path alias is: $reachpath";
		if($delay) {
			push @vals, "<li class=text>This is a secondary feed. We delay the feed for $delay minutes.";
		}
		push @vals, "</ol>\n";
		push @vals, "<p class=text>This feed will be loaded into our news servers every 30 minutes.</p>\n";

        } else {
                push @vals, 0, "<$p>Error Encountered</p><$pt>Usenet newsfeed not added. Please try again.";
        }

        return @vals;
}

# Del news service
# Pre: userid, $accno, $incominghost, $outgoinghost, $pathalias, $type
# Post: 1 okmsg, 0 failmsg
sub del_news {
	my($userid, $accno, $incominghost, $outgoinghost, $pathalias, $type, $mrwaccno) = @_;
        my @retvals;
	my $res;
	
	if($mrwaccno) {
                push @vals, 0, "<p class=header>Error Encountered</p><p class=text>This account has insufficient privileges.</p>";
                return @vals;
        }


	my $dbaccno = CMS_OLSS::get_accno_for_news($incominghost);

	if($accno !~ /^$dbaccno$/) {
		push @retvals, 0, "<p class=header>Usenet news service cancellation</p>";
		push @retvals, "<p class=text>The news service for $incominghost cannot be removed ($dbaccno) ($accno).</p>";
		return @retvals;
        }
	eval {
		$res = CMS_OLSS::delete_news_entry($accno, $incominghost);
        };

	$res &&= &log_entry($chnewslog, "DELETE_NEWS", "Userid: $userid, $accno, $incominghost");

	if($res) {
                push @retvals, 1, "<p class=header>Usenet news service cancellation</p>";
                push @retvals, "<p class=text>The news service for $incominghost has been removed.</p>";
        } else {
                push @retvals, 0, "<p class=header>Usenet news service cancellation</p>";
                push @retvals, "<p class=text>The news service for $incominghost cannot be removed.</p>";
        }

        return @retvals;

}

# List news service
# Pre: $accno, $incominghost, $outgoinghost, $pathalias, $type
# Post: 1 okmsg, 0 failmsg
sub list_news {
	my($accno, $mrwaccno) = @_;
        my @vals;

        my $p = "p class=header";
        my $pt = "p class=text";
        my $td = "td class=text";

	my $newsentries;
	eval {
		my $a = ($mrwaccno) ? $mrwaccno : $accno;
		$newsentries = CMS_OLSS::get_news_entries_for_account($a);
	};

	if( ($#{$newsentries} < 0) || ($@) ) {  
		&log_entry($internalerror, "NEWS LIST", "$@");
                push @vals, 1, "<$p>List of news feeds</p>";
                push @vals, "No entries extracted.\n";
                return @vals;
        } else {
                push @vals, 1, "<$p>List of Usenet news feeds</p>";
        }

	#push @vals, "<table cellpadding=5 cellspacing=5 border=1 width=100%>\n";

	# NB: $$row[5] = status
	# Status: 1 = Pending Add, 2 = Added, 3 = pending del, 4 = deleted
	for my $row (@$newsentries) {
		next if($$row[5] =~ /^(3|4)$/);
		push @vals, "<p class=thx>Feed name: $$row[0]</p>\n";
	        push @vals, "<table cellpadding=5 cellspacing=5 border=1 width=100%>\n";
		my $rw = "<tr><td class=text width=40%>";
		$rw .= "Customer incoming (where we send news to): </td><td class=text>$$row[0]</td></tr>\n";
		push @vals, $rw;

		$rw = "<tr><td class=text>";
		$rw .= "Customer outgoing (where you send news from):</td><td class=text>$$row[1]</td></tr>\n";
		push @vals, $rw;

		$rw = "<tr><td class=text>";
		$rw .= "Your path alias:</td><td class=text>$$row[2]</td></tr>\n";
		push @vals, $rw;

		$rw = "<tr><td class=text>";
		my $feedtype = ($$row[3] == 0) ? "Primary Feed" : "Secondary Feed";
		$rw .= "Feed type:</td><td class=text>$feedtype</td></tr></table>\n";
		push @vals, $rw;
	}
		
        return @vals;

}


# Bandwidth Management 
# Pre: $uid, $code, $accno, $tmplroot, $opshandle, fnn, svccode
# Post: @retvals, 0
sub screen_bwmanage {
        my ($uid, $code, $accno, $tmplroot, $opshandle, $fnn, $svccode) = @_;   
        my $ctrlfile = $tmplroot."/local/bwmanage.htm";
        my @screenoutput = ();
        my ($maxcapacity, $capacity, $linecapacity, $maxc);

        # TEMPORARY: look at sites.list to work this out
        #my $sl = "/mnt/snm/aarnet/sites.list";
        #open(xx, $sl);
        #while(my $l = <xx>) {
        #	if($l =~ /\t$opshandle$/i) {
        #		my @vals = split(/\t/, $l);
        #		if($vals[5] =~ /\{(\w*)\}/i) {
        #			$linecapacity = $1;
        #		}
	#		if($vals[5] =~ /\[(\w*)\]/i) {
        #			$capacity = $1;
        #		}
        #		last;
        #	}
        #}
        #close($sl);

	# retrieve from CMS
	eval {
		$linecapacity = CMS_OLSS::get_currbw_for_service($opshandle);
		$maxcapacity = CMS_OLSS::get_maxbw_for_service($opshandle);
	};

	#print "<P>LC ($opshandle) ($linecapacity) ($maxcapacity)\n";

	$linecapacity = (!$linecapacity) ? $maxcapacity : $linecapacity;

	if($maxcapacity =~ /^1000000000$/) {
		$maxcapacity = "gigabitethernet";
	} elsif ($maxcapacity =~ /^100000000$/) {
		$maxcapacity = "fastethernet";
	}

	#print "<P>LC ($maxcapacity) ($linecapacity)\n";

        my @capacity_opts = ();
        my ($lcdescr, $capacitydescr, $x);
        if($maxcapacity =~ /gigabitethernet/i) {
        	$x = 100;
        	$maxc = 1000;
        	$lcdescr = "GigabitEthernet";
        } elsif ($maxcapacity =~ /fastethernet/i) {
        	$x = 10;
        	$maxc = 100;
        	$lcdescr = "FastEthernet";
        } else {
        	push @screenoutput, "<p class=text>The Bandwidth Manager ";
		push @screenoutput, "is only available for FastEthernet or ";
		push @screenoutput, "GigabitEthernet services.";
		return @screenoutput;
	}

	my $displaycapacity;
        if($linecapacity) {

		$displaycapacity = $linecapacity / 1000000;
	       	my ($a, $tmp);

	       	$tmp = $x;
	       	while($tmp <= $maxc) {
			#print "<P>DD ($tmp) ($maxc) ($displaycapacity)\n";
                        if($tmp =~ /^$displaycapacity$/i) {
				$a = "<option value=$tmp SELECTED>$tmp Mbps</option>\n";
			} else {
				$a = "<option value=$tmp>$tmp Mbps</option>\n";
			}
		       	push @capacity_opts, $a;
		       	$tmp += $x;
		}
	}

	#print "<P>LC CTRL ($ctrlfile)\n";
        my $xx; 
        open($xx, $ctrlfile) or return 0;

        while(my $line = <$xx>) {
                $line =~ s/\*CODE\*/$code/g;  
                $line =~ s/\*ACCNO\*/$accno/g;
		$line =~ s/\*HANDLE\*/$opshandle/g;
		$line =~ s/\*FNN\*/$fnn/g;
                $line =~ s/\*ETPRODUCT\*/$encet/g;

		$line =~ s/\*SVC\*/$svccode/g;
		$line =~ s/\*LEVEL\*/611/g;
                $line =~ s/\*LINECAPACITY\*/$lcdescr/g;
                $line =~ s/\*CAPACITY\*/$displaycapacity Mbps/g;                                
		$line = &link_format($line, $code, $ctrlname);
                if($line =~ /\*BANDWIDTHOPTION\*/) {
                        push @screenoutput, @capacity_opts;
                } else {
                        push @screenoutput, $line;
                }

        }
        close($xx);

        return @screenoutput;
}       

# Do bandwidth management
# Bandwidth Management
# Pre: $uid, $code, $accno, $tmplroot, $opshandle, fnn, svccode, option
# Post: @retvals, 0
sub do_bwmanage {
        my ($uid, $code, $accno, $tmplroot, $opshandle, $fnn, $svccode, $option) = @_;
        my $ctrlfile = $tmplroot."/local/bwmanagedone.htm";
        my @screenoutput = ();
        my ($capacity, $linecapacity);

        # retrieve from CMS
        eval {
                $maxcapacity = CMS_OLSS::get_maxbw_for_service($opshandle);
		$linecapacity = CMS_OLSS::get_currbw_for_service($opshandle);
        };

        if($maxcapacity =~ /^1000000000$/) {
                $maxcapacity = "gigabitethernet";
        } elsif ($maxcapacity =~ /^100000000$/) {
                $maxcapacity = "fastethernet";
        }

	$option *= 1000000;

	my $increase = 0;
	if($linecapacity < $option) {
		$increase = 1;
	}

	push @screenoutput, "<p class=text>This feature is not operational yet - to come ($option) ($svccode)\n";
	return @screenoutput;

	my @vals = ();
	my $currtime = time;
	my $result;
	my @currvals = &retrieve($opshandle, $curr_capacitydb);
	if($currvals[0] =~ /^0$/) {
		# means can increase or decrease bandwidth
		if($increase) {
			$result = &insert($opshandle, $curr_capacitydb, $opshandle, $option, $currtime, 0);
		} else {
			$result = &insert($opshandle, $curr_capacitydb, $opshandle, $option, 0, $currtime);
		}
	} else {
		# need to check if increase or decrease
		if(!$increase) {
			# customer has requested decrease, check if ok to decrease
			if($currvals[3] != 0) {
				push @screenoutput, "<p class=text>Customers are only allowed to decrease bandwidth once.";
				push @screenoutput, "According to our records, you last decreased your bandwidth on ";
				my $t = &gmtime($currvals[3]);
				push @screenoutput, $t;

				return @screenoutput;
			}
			$result = &insert($opshandle, $curr_capacitydb, $opshandle, $option, $currvals[2], $currtime);
		} else {
			# customer has requested increase
			$result = &insert($opshandle, $curr_capacitydb, $opshandle, $option, $currtime, $currvals[3]);
		}
	}

	# NB: not atomic transaction - may cause problems here - when moved to SQL change way updated
	#$result &&= &insert("$opshandle-$currtime-$$", $pending_queuedb, $opshandle, $currtime, "pending", 0, $option);
	if($result) {
		$result = 0;
		eval {
			$result = CMS_OLSS::set_currbw_for_service($opshandle, $option);
		};
	}

	if(!$result) {
		push @screenoutput, "<p class=header>Error</p>";
		push @screenoutput, "<p class=text>There was an error updating your bandwidth requirements.";
		push @screenoutput, " Please try again later or log a trouble ticket.";
		return @screenoutput;
	}

	my $xx;	
        open($xx, $ctrlfile) or return 0;
        while(my $line = <$xx>) {
                $line =~ s/\*CODE\*/$code/g;
                $line =~ s/\*ACCNO\*/$accno/g;
                $line =~ s/\*HANDLE\*/$opshandle/g;
                $line =~ s/\*FNN\*/$fnn/g;
                $line =~ s/\*ETPRODUCT\*/$encet/g;

                $line =~ s/\*SVC\*/$svccode/g;
                $line =~ s/\*LEVEL\*/611/g;
                $line =~ s/\*LINECAPACITY\*/$lcdescr/g;
                $line =~ s/\*OLDCAPACITY\*/$cap Mbps/g;
		$line =~ s/\*NEWCAPACITY\*/$option Mbps/g;
                $line = &link_format($line, $code, $ctrlname);

		if($line =~ /\*INCREASE\*/) {
			if($increase) {
				$line =~ s/\*INCREASE\*/increase/g;
			} else {
				$line =~ s/\*INCREASE\*/decrease/g;
			}
		}

                if($line =~ /\*BANDWIDTHOPTION\*/) {
                        push @screenoutput, @capacity_opts;
                } else {
                        push @screenoutput, $line;
                }
        }
        close($xx);


                
        return @screenoutput;
}        
	                                

######################
# Account Admin - functional functions

# Change password - tgwciss-accadmin.cgi
# Pre: accno, oldpass, newpass1, newpass2, uid
# Post: 0 fail ; 1 ok 
sub dochpass {		# Added by Matthew - Oct, 2007
	#Chandini_IPT(comment out the wrong arguments received and added the correct ones)
        #my($userid, $oldp, $pass1, $pass2) = @_;
        my($accno, $oldp, $pass1, $pass2,$userid) = @_;

print DEBUG1 "inside .pl oldp:$oldp, pass1:$pass1, pass2:$pass2\n";
        my $loginid;

        if ($oldp eq $pass1)  { return -1; }
        if ($pass1 ne $pass2)  { return -2; }
        if (length($pass1) < 8)  { return -3; }
        if (length($pass1) > 15)  { return -4; }
        if (!(($pass1 =~ /[^A-Za-z_]/) && ($pass1 =~ /[A-Za-z_](.*)[A-Za-z_]/)))  { return -5; }
        if ($pass1 =~ /\+/)  { return -6; }
        if ($pass1 =~ /password/i)  { return -7; }

        if ($userid =~ /(.+)\^\^/) {
                $loginid = $1;
        } else {
                $loginid = $userid;
        }

        if(!CMS_OLSS::verify_newpassword($loginid, $pass1)) { return -8; }

        my $res = &updatePass($loginid, $oldp, $pass1);

        return $res;
}


#sub dochpass {
#       my($accno, $oldp, $p1, $p2, $userid) = @_;
#
#       my ($res, $result);
#
#       eval {
#               $result = CMS_OLSS::verify_plaintext_password($accno, $oldp);
#       };
#
#       #my $coldp = crypt($oldp, $oldp);
#       #$coldp = crypt($coldp, "0203l");
#
#       #my $res = &checkpass($accno, $coldp);
#
#       if($result =~ /^0$/) {
#               return 0;
#       }
#       if($p1 !~ /^$p2$/) {
#               return 0;
#       }
#
#       if($p1 =~ /^customer$/i) {
#               return 0;
#       }
#
#       $res = &updatePass($userid, $accno, $oldp, $p1);
#
#       $res &&= &log_entry($chpasslog, "PASS_CHANGE", "Userid $userid, $accno");
#
#       return $res;
#
#}


# Change stats contact
# Pre: accno, email, action (0 = remove; 1 = add), userid
# Post: 0 (fail), 1 (ok)
sub ch_statsc {
	my($accno, $email, $action, $userid) = @_;

	my @vals = &retrieve($accno, $contactdb);
	# stats contact is $vals[3]

	if($action) {
		$vals[3] = $email;
	} else {
		$vals[3] = "";
	}

	my $res = &insert($accno, $contactdb, @vals);

	$res &&= &log_entry($chaliaslogfile, "CHANGE_STATSC", "Userid: $userid, $accno, $email");

	return $res;

}

# update tech contact - temporary
# Pre: $accno, $firstname,$lastname,$tel,$fax,$email,$mob
# Post: 0 (fail), 1 (ok)
sub update_tech_contact_details {
	my($accno, $firstname,$lastname,$tel,$fax,$email,$mob) = @_;

	my $res = &insert($accno, $techcdb, $accno, $firstname,$lastname,$tel,$fax,$email,$mob);
	return $res;
}

#        accno             varchar(20) NOT NULL,
#        typeid            integer NOT NULL,
#        firstname         varchar(50) NOT NULL,
#        surname           varchar(50) NOT NULL,
#        telephone         varchar(20),
#        fax               varchar(20),
#        email             varchar(60),
#        mobile            varchar(20),
#        subscriptionid    integer,

# Change tech contact
# Pre: uid, accno, email, action (0 = remove; 1 = add), $firstname,$lastname,$tel,$fax,$mob,$outages
# Post: 0 (fail), 1 (ok)
sub ch_techc {
        my($userid, $accno, $email, $action,
		$firstname,$lastname,$tel,$fax,$mob,$outages) = @_;
    
	my $res = 0; 
        #my @vals = &retrieve($accno, $contactdb);
        # tech contact is $vals[1]
	#print "Content-type: text.json\n\n";
	# if add
        if($action) {
                #$vals[1] = $email;
	        eval {
        	        $res = CMS_OLSS::update_tech_contact_details($accno, $firstname,$lastname,$tel,$fax,$email,$mob);
        	        #$res = update_tech_contact_details($accno, $firstname,$lastname,$tel,$fax,$email,$mob);
			#print "<P>DEBUG XX1 ($res)\n";
                	$res &&= &log_entry($chaliaslogfile, "CHANGE_TECHC ADD", "Userid: $userid, $accno, $email");
        	};
		#print "<P>ch_techc DEBUG XX ($@) result ($res)\n";

        } else {
		# else delete
                #$vals[1] = "";
	        eval {
       		        $res = CMS_OLSS::update_tech_contact_details($accno,"","","","","","");
               		$res &&= &log_entry($chaliaslogfile, "CHANGE_TECHC DEL", "Userid: $userid, $accno, $email");
        	};
		#print "<P>ch_techc DEBUG ($@) ($res)\n";
        }
                         
        #my $res = &insert($accno, $contactdb, @vals);
	#eval {
	#	$res = CMS_OLSS::update_tech_contact_details($accno, $firstname,$lastname,$tel,$fax,$email,$mob);
	#	$res &&= &log_entry($chaliaslogfile, "CHANGE_TECHC", "$accno, $email");
	#};
        
        return $res;
                
}


######################


######################


# Parse out staffid
# Pre: uid
# Post: accno, staffid
sub ex_staffid {
	my $uid = shift;

	my ($accno, $staffid);
	if($uid =~ /\^\^/) {
		($staffid, $accno) = split(/\^\^/, $uid);
	} else {
		$accno = $uid;
	}

	if($staffid) {
		return ($accno, $staffid);
	}
	
	return $accno;
}


# Change password
# Pre: [accno], old cleartext pass, cleartext pass
# Post: 0 (fail), 1 (ok)
sub updatePass {	# Added by Matthew - Oct, 2007
#       my ($userid, $handle, $oldp, $pass) = @_;
        my ($userid, $oldp, $pass) = @_;

#       my $accno = $handle; #&getOps($handle);

        # first update opsdb
        my $ret;
        eval {
#               $ret = CMS_OLSS::change_password($accno, $oldp, $pass);
                $ret = CMS_OLSS::change_olss_cust_password($userid, $oldp, $pass);
        };

        if(!$ret) {
                return 0;
        }

#       $ret &&= &log_entry($chpasslog, "PASS_CHANGE", "Userid: $userid, $accno");
        $ret &&= &log_entry($chpasslog, "PASS_CHANGE", "Userid: $userid");

        return $ret;
}


#sub updatePass {
#	my ($userid, $handle, $oldp, $pass) = @_;
#
#	my $accno = $handle; #&getOps($handle);
#
#	# first update opsdb
#	my $ret;
#	eval {
#		$ret = CMS_OLSS::change_password($accno, $oldp, $pass);
#	};
#
#	if(!$ret) {
#		return 0;
#	}
#
#	$ret &&= &log_entry($chpasslog, "PASS_CHANGE", "Userid: $userid, $accno");
#
#	return $ret;
#}

# Get template path
# Pre: account number, prod code
# Post: path
sub get_template {
	my ($accno, $prodcode) = @_;

	$prodcode = (!$prodcode) ? "GIA" : $prodcode;

	my $result;
	eval {
		$result = CMS_OLSS::get_template_path($accno, $prodcode);
	};

	return $result;
}

# Get product code
# Pre: accno
# Post: @prodcodes
sub get_prodcode {
	my $accno = shift;
	my (@prods, $x);

	eval {
		$x = CMS_OLSS::get_products($accno);
	};
	# have 2d array - row col
	foreach my $row (@$x) {
		push @prods, $$row[0];
	}
	
	return @prods;
}	

# Pre: accno
# Post: company name, 0 fail
sub getconame {
	my $handle = shift;

	my $x;

	eval {
		$x = CMS_OLSS::get_customer($handle);
	};

	my @tt = @$x;

	if($tt[0] =~ /^0$/) {
		return 0;
	}

	return $tt[2];

}

# Pre: service handle
# Post: accountnumber (ok), 0 fail
sub getOps {
	my $handle = shift;

	my ($result, $accno);

	eval {
		$result = CMS_OLSS::get_accno_for_serviceid($handle);
	};

	if($result) {
		return $result;
	}
		
	return 0;
}


# Get attribute - deprecated
# Pre: accno
# Post: attribute (WHOLESALER|POP)
sub getAttr {
	my $accno = shift;
	my @vals = &retrieve($accno, $opsdb);
	#print "<P>DEBUG 1 (@vals) ($accno)\n";
	return $vals[5];
}

# Get wholesaler attribute (ie wholesaler group) - deprecated
# Pre: accno
# Post: whlattr
sub getWhlAttr {
	my $accno = shift;
	my @vals = &retrieve($accno, $opsdb);
	return $vals[4];
}

# Pre: handle = sitehandle - deprecated
sub getCustElem {
	my $handle = shift;

	my @vals = &retrieve($handle, $shdb);

	print "<P>GCE (@vals) ($handle) ($shdb)\n";
	
	return @vals;
}
			
			

# Generate GIF
# Pre: Filename, template_master (eg telstra)
# Post: gif
sub gen_gif {
	my($fname, $template) = @_;
	if($template =~ /^t$/i) {
		$template = "telstra";
	} else {
		$template = "extant";
	}
	my ($path) = $tgwr."/control/".$template."-template/gif/";
	my $gfile = $path.$fname.".gif";
	my($content_length);
	if(-e $gfile) {
		$content_length = (stat($gfile))[7];
		print "Content-type: image/gif\n";
		print "Content-length: $content_length\n\n" ;
		my $GIF;
		if (open($GIF,"$gfile")) {
			while (<$GIF>) {
				print;
			}
			close($GIF);
		}
	}
}

# Pre: *formels
# Post: *formels, from address
sub exvalues {
	local(*formels) = @_;
	local($from);
	
	$from = $ENV{'REMOTE_ADDR'};
	$from =~ s/\./\./;
 
	# turn on for debugging purposes
	#print "Content-type: text/json\n\n";
 
	# First check for type of method
	if($ENV{'REQUEST_METHOD'} eq "GET") {
        	#print "Location: http://www.net.reach.com\n\n";
        	print "Location: http://$realhost\n\n";
	        exit(1);
	}
 
	read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
 
	#split pairs by the & which divides the vars
	local(@nuid) = split(/&/, $buffer);
	local($nuid, $name, $value);
	foreach $nuid (@nuid)
	{
        	($name, $value) = split(/=/, $nuid);
	        $value =~ tr/+/ /;
        	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	        $formels{$name} = $value;
	}

	return ($from);
}

# Pre: handle, password, uxtime|attr, prod_code
sub encrypt {
        my($code, $ps, $vattr, $prodcode) = @_ ;
        my(@offsets) = (3,6,1,4,5) ;
        my(@zero) = ("s","x","e","f","g") ;
	my ($result, @chars, $i, $val, $o);

	my $ts = time;
	$ts = &ecrypt($ts);
	$prodcode = (!$prodcode) ? "GIA" : $prodcode;

        $result = "" ;
        my(@chars) = split(//,$code) ;
        for $i (0..$#chars) {
                $o = $i % 5 ;
                $val = ord($chars[$i]) ;
                $val += $offsets[$o] ;
                $val =~ s/0/$zero[$o]/g;
                $result .= $val ;
                $result .= "0" ;
        }
        $result .= "a";

	#print "Content-type: text/json\n\n";
	#print "<P>EN ($code, $ps)\n";
	#exit;

	#my $x = crypt($ps, "0203l");

	# ecrypt the password for inclusion in code
	my $x = &ecrypt($ps);

        $result .= $x;
	
	# include the timestamp
	#$result .= "rcid" . $ts;

	my($vat);
	my $ct = time;
	$vattr = (!$vattr) ? $ct : $vattr;
	if($vattr) {
		$vat = &ecrypt($vattr);
		$result .= "rxc".$vat;
		$vat = &ecrypt($prodcode);
		$result .= "rcp".$vat;
	}

#	result .= "rcid".$ts;
		
        return $result ;
}

# Encrypt just a handle
sub ecrypt {
        local($code, $ps) = @_ ;
        local(@offsets) = (3,6,1,4,5) ;
        local(@zero) = ("s","x","e","f","g") ;
	local($i, $o, $val, $result, @chars);
        $result = "" ;
        local(@chars) = split(//,$code) ;
        for $i (0..$#chars) {
                $o = $i % 5 ;
                $val = ord($chars[$i]) ;
                $val += $offsets[$o] ;
                $val =~ s/0/$zero[$o]/g;
                $result .= $val ;
                $result .= "0" ;
        }
        return $result ;
}

# Decrypt just a handle
sub dcrypt {
        local($code) = @_ ;
        local(@offsets) = (3,6,1,4,5) ;
        local(@zero) = ("s","x","e","f","g") ;

        (@letts) = split(/0/,$code) ;

        $result = "" ;
        for ($i = 0; $i <= $#letts ; ++$i) {
                $o = $i % 5 ;
                $letts[$i] =~ s/$zero[$o]/0/g;
                $letts[$i] -= $offsets[$o] ;
                $char = pack("C",$letts[$i]) ;
                $result .= $char ;
        }
        return($result) ;
}

# Pre: accno|staffid, clearpass|staff_md5_cmspass, root, exit
sub check_pass {
        my ($ohandle, $userpass, $setroot, $exit) = @_;

	my ($password);

        if($userpass eq "customer") {
                $userpass =~ tr/a-z/A-Z/;
        }

	my $accno = $ohandle; #&getOps($ohandle);
	#print "<P> check_pass DEBUG ($accno) ($userpass)\n";
        #my @custvals = &retrieve($accno, $opsdb);
	#my $x = crypt($userpass, $userpass);

	#print "Content-type: text/json\n\n";

	# verify password
	my ($encrpass, $result);
	eval {
		# first check if staff
		$result = CMS_OLSS::authenticate_staff($ohandle, $userpass);

        open(LEOZ,">/data1/tmp_log/leo_debug~tgwcisslib.cgi~sub_check_pass");
        #print LEOZ "ohandle($ohandle) userpass($userpass) result($result)\n";

		$encrpass = "staffidauth^^$result";
		if(!$result) {
			$result = CMS_OLSS::verify_plaintext_password($accno, $userpass);
			$encrpass = $result;

	#print LEOZ "accno($accno)userpass($userpass)result($result)\n";

		}
	};

        #print "<P>DEBUG ($accno) ($ohandle) ($userpass) ($result) ($@) ($x)\n";
        #exit;

        #print LEOZ "eval passed\n";
        #print LEOZ "ohandle($ohandle) userpass($userpass) result($result)\n";
        close(LEOZ);

	if($result =~ /^0$/) {
		if(!$exit) {
			return 0;
		}
		&print_error("The password entered is incorrect .. please try again");
		return 0;
	}

	return($encrpass);
}

# Check if a given requested service (eg FNN/opshandle) matches with the uid/pass pair
# Pre: staffid^^accno|accno, code, svc, MRW_accno
# Post: 0 (fail), 1 (ok) 
sub check_svcpass {
        my ($accno, $code, $service, $mrwaccno) = @_;
        my ($password);

        my($result, $vfg, $crypass) = &decrypt($code);

	#print "<P>CHECK SVC PASS ($result, $vfg, $crypass) ($code) accno ($accno) codea \n";

        if($result eq "FAIL") {
        	return 0;
        }
	my ($codeaccno, $staffid) = &ex_staffid($result); 

	#print "<P>CHECK SVC PASS ($result, $vfg, $crypass) accno ($accno) codea ($codeaccno)\n";

	# $codeaccno should be eq to $accno
	if($accno !~ /^$codeaccno$/) {
		return 0;
	}

	my $accno1 = &getOps($service);

	# If mrwaccno exists - then RO account
	if($mrwaccno) {
		if($accno1 =~ /^$mrwaccno$/) {
			return 1;
		}
	} else {
		if($accno1 =~ /^$accno$/) {
			return 1;
		}
	}

        return 0;
}


sub liblog_user {
        local($userid, $clients, $file, $auditrail)  = @_;
        local($dates) = &ctime(time);
	chop($dates);
	my $FF;
        open($FF, ">>$file");
        print $FF "User = $userid ::: From = $clients ::: Date = $dates";
	if($auditrail) {
		print $FF " ::: ($auditrail)\n";
	} else {
		print $FF "\n";
	}
        close($FF);
        chmod(0660, $logfile);
}

sub print_error {
        local($errmsg, $code, $wholesaler) = @_;

#print "Content-type: text/json\n\n";
print "tmp=$tmplroot, $code";

	$tmplroot = (!$tmplroot) ? $tgwr."control/reach-template/" : $tmplroot;

	chdir($tmplroot);

	if(-e "$errortmpl") {
		my $xx;
		open($xx, $errortmpl);
		print "Content-type: text/json\n\n";
		while(my $line = <$xx>) {
			$line = &link_format($line, $code, $ctrlname);
			if($line =~ /\*DATA\*/) {
				print "$errmsg\n";
				if ($wholesaler =~ /telstra/) {
#					print "<p class=text><a href=\"/reseller-telstra/\">Back to Login</a>";
					print "<p class=text><a href=\"/index.json\">Back to Login</a>";
				} elsif ($wholesaler =~ /pccw/) {
					print "<p class=text><a href=\"/reseller-pccw/\">Back to Login</a>";
				} else {
					print "<p class=text><a href=\"/index.json\">Back to Login</a>";
				}
			} else {
				print $line;
			}
		}
		close($xx);
	} else {
	
	        #print "Content-type: text/json\n\n";
		#print "<HTML>\n";
	        #print "<HEAD><TITLE>Error !</TITLE></HEAD>\n";
        	#print "<BODY BGCOLOR=#FFFFFF TEXT=#000000 LINK=#FF0000>\n";
		#print '<!-- Added by Gary (AGENDA): Start -->';
                #print '<script language="JavaScript"> function handleTimeout() { if (window!=window.top) { parent.handleTimeout(); } } handleTimeout(); </script>';
                #print '<!-- Added by Gary (AGENDA): End -->';
	        #print "<h3><I>Error !</I></h3><P><HR><P>\n";
        	print "An error was encountered. <P>";
	        print "The error is: $errmsg. ";
		if ($wholesaler =~ /telstra/) {
#			print "<p><HR><P><a href=\"/reseller-telstra/\">Back to Login</a>";
			print "<p><HR><P><a href=\"/index.json\">Back to Login</a>";
		} elsif ($wholesaler =~ /pccw/) {
			print "<p><HR><P><a href=\"/reseller-pccw/\">Back to Login</a>";
		} else {
        		print "<P><HR><P><A HREF=\"/index.json\">Back to Login</A>";
		}
	        #print "<P><HR><P><I>olss</I></BODY>";
		#print '</BODY></HTML>';
	}
 
        exit(0);
}


sub oldprint_error {
        local($errmsg) = @_;
        print "Content-type: text/json\n\n";
	print "<HTML>\n";
        print "<HEAD><TITLE>Error !</TITLE></HEAD>\n";
        print "<BODY BGCOLOR=#FFFFFF TEXT=#000000 LINK=#FF0000>\n";
	print '<!-- Added by Gary (AGENDA): Start -->';
        print '<script language="JavaScript"> function handleTimeout() { if (window!=window.top) { parent.handleTimeout(); } } handleTimeout(); </script>';
        print '<!-- Added by Gary (AGENDA): End -->';
        print "<h3><I>Error !</I></h3><P><HR><P>\n";
        print "An error was encountered. <P>";
        print "The error is : $errmsg. ";
        print "<P><HR><P><A HREF=\"/tgwcustdata/index.json\">Back to Login</A>";
        print "<P><HR><P><I>oms\@reach.com</I></BODY>\n";
	print "</BODY></HTML>\n";
 
        exit(0);
}

sub print_inerror {
        local($errmsg) = @_;
	print '<!-- Added by Gary (AGENDA): Start -->';
        print '<script language="JavaScript"> function handleTimeout() { if (window!=window.top) { parent.handleTimeout(); } } handleTimeout(); </script>';
        print '<!-- Added by Gary (AGENDA): End -->';
        print "<h3><I>Error !</I></h3></CENTER><P><HR><P>\n";
        print "An error was encountered. <P>";
        print "The error is : $errmsg. ";
	print "</TABLE></TABLE>\n";
        exit(0);
}

# Pre: accno, encr_pass
# Post: pass or 0
sub getdecpass {
  	local($account, $pass) = @_ ;

  	my $accno = $handle; &getOps($account);
	my $result;
	eval {
		$result = CMS_OLSS::verify_encrypted_password($accno, $pass);
	};

	if($result) {
		return $result;
	}

	return 0;

}
	        
sub decrypt {
  	my($cd, $flag) = @_;
  	my(@offsets) = (3,6,1,4,5) ;
  	my(@zero) = ("s","x","e","f","g") ;
	my($code, $gpassw, @letts, $result, $i);

	my $ts = time;
 
	my($ccd, $vattrflag) = split(/rxc/,$cd,2); # break off rcid first
	my ($cts);

#	($cts, $vattrflag) = split(/rxc/, $vattrflag, 2); # get code timestamp
#	$cts = &vflagdecrypt($cts);
#print "<P>A ($cts) ($vattrflag)\n";
#	if( ($ts - $cts) > $maxtime) {
#		return "FAIL-Session Timeout";
#	}

#	my($ccd, $vattrflag) = split(/rxc/,$cd,2);


	my $prodcode;
	($vattrflag, $prodcode) = split(/rcp/, $vattrflag, 2);
        open(LEOZ,">/data1/tmp_log/leo_debug~tgwcisslib.pl~decrypt");
        #print LEOZ "cd($cd) flag($flag) ccd($ccd) vattrflag($vattrflag) prodcode($prodcode)\n";
	if($vattrflag) {
		$vattrflag = &vflagdecrypt($vattrflag);
        	#print LEOZ "ts($ts) vattrflag($vattrflag)\n";
		# this is the timestamp?
		if( ($ts - $vattrflag) > $maxtime)  {
			return "FAIL-Session Timeout";
	        }

	}
	if($prodcode) {
		$prodcode = &vflagdecrypt($prodcode);
	} else {
		$prodcode = "GIA";
	}
        #print LEOZ "prodcode($prodcode)\n";

  	($code, $gpassw) = split(/a/,$ccd,2);

	$flag && print "Content-type: text/json\n\n";

	# strip ecrypt off gpassw
	$gpassw = &dcrypt($gpassw);
	
  	(@letts) = split(/0/,$code) ; 
 
  	$result = "" ;
  	for $i (0..$#letts) {
    		$o = $i % 5 ;
    		$letts[$i] =~ s/$zero[$o]/0/g;
    		$letts[$i] -= $offsets[$o] ;
    		$char = pack("C",$letts[$i]) ;
    		$result .= $char ;
    	}
  	$code = $result ;

	$flag && print "<P>code ($code) pass ($gpassw)\n";
        #print LEOZ "code($code) gpassw($gpassw)\n";

	my $verify;

	# NB: overloaded operators
	#	code is either "account_number" or "staffid^^accountnumber"
	#	gpassw is either "password" or "staffidauth^^staff-md5-from-CMS
	if($gpassw =~ /^staffidauth\^\^/i) {
		my $staffauthcode = (split(/\^\^/, $gpassw))[1];
		my $staffid = (split(/\^\^/, $code))[0];
		#print "<P>DD ($staffid) ($staffauthcode)\n";
		eval {
			# CMS returns the new MD5 (for timeout purposes)
			$verify = CMS_OLSS::authenticate_staff($staffid, $staffauthcode);
		};
		$gpassw = "staffidauth^^$verify";
		#print "<P>DD VERIFY ($verify)\n";
	} else {
		$verify = &checkpass($code, $gpassw);
	}
        close(LEOZ);
  	if ($verify) {
		# Remarked by Leo
    		#return($code, $vattrflag, $gpassw) ;
		# Added by Leo
    		return($code, $ts, $gpassw) ;
    	}

  	return("FAIL");
}

sub vflagdecrypt {
	my($cd) = shift;
	my(@offsets) = (3,6,1,4,5) ;
	my(@zero) = ("s","x","e","f","g") ;
	my($code, $gpassw, @letts, $result, $i);
	@letts = split(/0/,$cd) ; 
	$result = "" ;
	for $i (0..$#letts) {
                $o = $i % 5 ;
                $letts[$i] =~ s/$zero[$o]/0/g;
                $letts[$i] -= $offsets[$o] ;
                $char = pack("C",$letts[$i]) ;
                $result .= $char ;
				
        }
        $code = $result ;

	return $code if($code);
}


# Pre: accno, encrypted password
# Post: 0 (fail), 1 (ok)
sub checkpass {  
  	my($handle, $pass) = @_ ;

  	my $accno = $handle; #&getOps($handle);
	my $result;
	eval {
		$result = CMS_OLSS::verify_encrypted_password($accno, $pass);
	};

	if($result) {
		return 1;
	}
  	
  	return(0);
}

sub parse_form_data {
open(LEOZ,">/data1/tmp_log/leo_debug");
	local(*formels) = @_;  
	my($from) = $ENV{'REMOTE_ADDR'};    
        $from =~ s/\./\./;

  	my(@key_value_pairs, $query_string, $key, $value);
  	$request_method = $ENV{'REQUEST_METHOD'} ;
  	if ($request_method eq "GET") {
    		$query_string = $ENV{'QUERY_STRING'} ;
    	} elsif ($request_method eq "POST") {
		read(STDIN, $query_string, $ENV{'CONTENT_LENGTH'}) ;
    	} elsif ($query_string = $ENV{'PATH_INFO'}) {
		$query_string =~ s/\//&/g ;
    	} else {
    		&print_error("Server uses unsupported method", 500);
    	}
  	@key_value_pairs = split(/&/,$query_string) ;
  	foreach $key_value (@key_value_pairs) {
    		($key, $value) = split(/=/,$key_value);
                $key =~ s/%([\dA-Fa-f][\dA-Fa-f])/pack("C",hex($1))/eg;
                $key =~ s/\s+$//g;
		$value =~ tr/+/ /;
	    	$value =~ s/%([\dA-Fa-f][\dA-Fa-f])/pack("C",hex($1))/eg;
		$value =~ s/\s+$//g;
    		if (defined($formels{$key})) { 
      			$formels{$key} = join("\0", $formels{$key}, $value) ;
	      	} else {
      			$formels{$key} = $value ;  
      		} 
print LEOZ "key:$key\n";
print LEOZ "key value:$formels{$key}\n";
	}
	return($from);
}

sub print_file {
	my($filename, $code) = @_;
	if(-e $filename) {
		my $IIs;
		open($IIs, $filename);
		while(<$IIs>) {
			if(/telstra/i) {
				$_ =~ s/telstra/Reach/gi;
			}
			if(/\*code\*/) {
				s/\*code\*/$code/g;
			}
			print;
		}
		close($IIs);
	}
}

sub checkhandle {
	my($ecode) = @_;
	my(%tmp);
	my $II;

	open($II, $ripealias);
	while(<$II>) {
		chop;
		($key, $value) = split(/\s+/);
		$tmp{$key} = $value;
	}
	close($II);
	return($tmp{$ecode} ||  $ecode);
}

#calculate no of /24
sub cal_subnet {
        my ($route) = @_;
		
           
        if (($route =~ /\/(.*?)$/) && ($1 <=24)){
                return (2 ** (24-$1));
        } else {
                return 0;
        }
}

#Pradeep for OLSS MRTG link
sub getURLForCore {
        my ($uid, $code, $tmplroot, $script, $lvl) = @_;
	open (FH1,">/data1/tmp_log/log_prad");
	#print FH1 "userid :$userid\n  oenc :$oenc\n accno= $accno \n tmplroot=$tmplroot \n script:$script \n lvl:$lvl \n";
        
        my $encrptp = ($encet) ? $encet : "";
	my $urlString = "/cgi-bin/$script?code=$code&level=$lvl&et=$encrptp";  
	#print FH1 "urlString---> $urlString";
	close FH1;
	return $urlString;
}


1;
