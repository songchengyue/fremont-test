/*
 * XML Type:  addCIsAndRelationsResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * An XML addCIsAndRelationsResponse(@http://schemas.hp.com/ucmdb/1/params/update).
 *
 * This is a complex type.
 */
public class AddCIsAndRelationsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponse
{
    
    public AddCIsAndRelationsResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CREATEDIDSMAP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "CreatedIDsMap");
    private static final javax.xml.namespace.QName COMMENTS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "comments");
    
    
    /**
     * Gets array of all "CreatedIDsMap" elements
     */
    public com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID[] getCreatedIDsMapArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CREATEDIDSMAP$0, targetList);
            com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID[] result = new com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "CreatedIDsMap" element
     */
    public com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID getCreatedIDsMapArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID)get_store().find_element_user(CREATEDIDSMAP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "CreatedIDsMap" element
     */
    public int sizeOfCreatedIDsMapArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CREATEDIDSMAP$0);
        }
    }
    
    /**
     * Sets array of all "CreatedIDsMap" element
     */
    public void setCreatedIDsMapArray(com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID[] createdIDsMapArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(createdIDsMapArray, CREATEDIDSMAP$0);
        }
    }
    
    /**
     * Sets ith "CreatedIDsMap" element
     */
    public void setCreatedIDsMapArray(int i, com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID createdIDsMap)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID)get_store().find_element_user(CREATEDIDSMAP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(createdIDsMap);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "CreatedIDsMap" element
     */
    public com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID insertNewCreatedIDsMap(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID)get_store().insert_element_user(CREATEDIDSMAP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "CreatedIDsMap" element
     */
    public com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID addNewCreatedIDsMap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID)get_store().add_element_user(CREATEDIDSMAP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "CreatedIDsMap" element
     */
    public void removeCreatedIDsMap(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CREATEDIDSMAP$0, i);
        }
    }
    
    /**
     * Gets the "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments getComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "comments" element
     */
    public void setComments(com.hp.schemas.ucmdb._1.types.ResComments comments)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            }
            target.set(comments);
        }
    }
    
    /**
     * Appends and returns a new empty "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments addNewComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            return target;
        }
    }
}
