/*
 * XML Type:  SimplePredefinedPropertyCollection
 * Namespace: http://schemas.hp.com/ucmdb/1/types/props
 * Java type: com.hp.schemas.ucmdb._1.types.props.SimplePredefinedPropertyCollection
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.props.impl;
/**
 * An XML SimplePredefinedPropertyCollection(@http://schemas.hp.com/ucmdb/1/types/props).
 *
 * This is a complex type.
 */
public class SimplePredefinedPropertyCollectionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.props.SimplePredefinedPropertyCollection
{
    
    public SimplePredefinedPropertyCollectionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SIMPLEPREDEFINEDPROPERTY$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "simplePredefinedProperty");
    
    
    /**
     * Gets array of all "simplePredefinedProperty" elements
     */
    public com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty[] getSimplePredefinedPropertyArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SIMPLEPREDEFINEDPROPERTY$0, targetList);
            com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty[] result = new com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "simplePredefinedProperty" element
     */
    public com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty getSimplePredefinedPropertyArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty)get_store().find_element_user(SIMPLEPREDEFINEDPROPERTY$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "simplePredefinedProperty" element
     */
    public int sizeOfSimplePredefinedPropertyArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SIMPLEPREDEFINEDPROPERTY$0);
        }
    }
    
    /**
     * Sets array of all "simplePredefinedProperty" element
     */
    public void setSimplePredefinedPropertyArray(com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty[] simplePredefinedPropertyArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(simplePredefinedPropertyArray, SIMPLEPREDEFINEDPROPERTY$0);
        }
    }
    
    /**
     * Sets ith "simplePredefinedProperty" element
     */
    public void setSimplePredefinedPropertyArray(int i, com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty simplePredefinedProperty)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty)get_store().find_element_user(SIMPLEPREDEFINEDPROPERTY$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(simplePredefinedProperty);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "simplePredefinedProperty" element
     */
    public com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty insertNewSimplePredefinedProperty(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty)get_store().insert_element_user(SIMPLEPREDEFINEDPROPERTY$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "simplePredefinedProperty" element
     */
    public com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty addNewSimplePredefinedProperty()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimplePredefinedProperty)get_store().add_element_user(SIMPLEPREDEFINEDPROPERTY$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "simplePredefinedProperty" element
     */
    public void removeSimplePredefinedProperty(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SIMPLEPREDEFINEDPROPERTY$0, i);
        }
    }
}
