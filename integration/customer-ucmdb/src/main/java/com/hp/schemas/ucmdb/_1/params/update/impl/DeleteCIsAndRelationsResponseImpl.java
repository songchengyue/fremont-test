/*
 * XML Type:  deleteCIsAndRelationsResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * An XML deleteCIsAndRelationsResponse(@http://schemas.hp.com/ucmdb/1/params/update).
 *
 * This is a complex type.
 */
public class DeleteCIsAndRelationsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponse
{
    
    public DeleteCIsAndRelationsResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
