# (C) Telstra 2009
#
# Author: Meena Prakash
# Date: 1 April 2009
# File: CMS_DNSRESOLVER_DB.pm
#
# $Id: CMS_DNSRESOLVER_DB.pm,v 1.1.1.1 2003/10/19 13:49:52 rxc Exp $

use lib "/usr/local/www/wanchai/netops/modules";
package CMS_DNSRESOLVER_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
use CMS_OLSS;
use Mail::Sendmail;
# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;

our $error;

# Select dns record types
# Pre  : contact_email, host, record_parameter
# Prost: $result
sub get_dns_resolver_entry {
open (DEBUG, ">/tmp/DB_DNS");
CMS_DB::begin_transaction();
print DEBUG "INSIDE ACL\n";
        my $uid     = $dbh->quote(shift @_);
        my $customer_id   = $dbh->quote(shift @_);
        my $contact = $dbh->quote(shift @_);
        my $host    = $dbh->quote(shift @_);
        my $parm    = $dbh->quote(shift @_);
        my $parm_val = $parm;
        my $temp_sql = '';
        my $success = 1;
        my $ti = '';
        my $match_ip = '';
        my $r2 = '';
        my $r3 = '';
        my $sth = '';
        my $r1 = '';
        my $r = '';
        my $temp_x  = '';
	my ($div_val,$ip_block_chk1) = '';
	 my @z1_octets = "";
        my $z1_octet1 = "";
        my $z1_octet2 = "";
        my $z1_octet3 = "";
        my $z1_octet4 = "";
        my $z1_bin_1 = "";
        my $z1_bin_2 = "";
        my $z1_bin_3 = "";
        my $z1_bin_4 = "";
        my @z1_binary = "";
        my $z1_binary_stat = "";
        my @z1_binary_bit = "";
        my @ip_octets = "";
        my $ip_octet1 = "";
        my $ip_octet2 = "";
        my $ip_octet3 = "";
        my $ip_octet4 = "";
        my $ip_bin_1 = "";
        my $ip_bin_2 = "";
        my $ip_bin_3 = "";
        my $ip_bin_4 = "";
        my @ip_binary = "";
        my $ip_binary_stat = "";
        my @ip_binary_bit = "";
        my $ip_length = "";
        my $ip_binary_result = "";
        my $z1_binary_result = "";
        my $loop_count2 = 0;
	my $ipprefix = "";
	my $compare_flag=0;
 	my $ip_flag = 1;	


        $parm_val =~ s/\'//g;
        my @y = split(/\//,$parm_val);
        my $z1= shift(@y);
        my $z2= shift(@y);
	$ti = rindex($z1,".");
        $match_ip  = substr($z1,0,$ti);
	my @ip_octet =  split(/\./,$z1);
	my $ip_octets1 = shift(@ip_octet);
	my $ip_octets2 = shift(@ip_octet);
	my $ip_octets3 = shift(@ip_octet);
	my $ip_octets4 = shift(@ip_octet);

print DEBUG "DB match_ip before entering loop:$match_ip ti:$ti\n";
print DEBUG "DB parm_value: $parm_val z1:$z1 z2:$z2\n";

# check for whether the accno exist or not
        $sth = $dbh->prepare ("SELECT count(customer_id) FROM olss_cust_access_acc WHERE customer_id like $customer_id");
        $sth->execute();
        $r = $sth->fetchall_arrayref;
if($r->[0]->[0] >= 1) {
# username checked in ip_resource block
	my $sth3 = $dbh->prepare ("SELECT count(ip_block) FROM ip_resource WHERE ip_block like '$match_ip%' AND customer_id like $customer_id AND LOWER(dns_service) like 'yes'");
	$sth3->execute();
	my $rt = $sth3->fetchall_arrayref;
if ($rt->[0]->[0] >= 1){

# Same User not allowed for DNS Resolver Service for more than 10 requests.
        #my $sth = $dbh->prepare ("SELECT count(o.username) FROM  dns_resolver p, olss_cust_access_acc o where o.accno = p.accno and o.username ilike $uid");
        $sth = $dbh->prepare ("SELECT count(username) FROM  dns_resolver where username like $uid");
        $sth->execute();
        $r1 = $sth->fetchall_arrayref;

  if($r1->[0]->[0] < 10) {

        $ti = rindex($z1,".");
        $match_ip  = substr($z1,0,$ti);

        # check for whether the IP already exist in dns_resolver
        $sth = $dbh->prepare ("SELECT count(value) FROM dns_resolver WHERE value like $parm and username like $uid and status = 1");
        $sth->execute();
        $r3 = $sth->fetchall_arrayref;
print DEBUG "username check r3:$r3\n";


	# CHECK WHEN IP IS EXACTLY SAME

        # check for whether the IP exist or not
        $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$parm_val' and customer_id like $customer_id AND LOWER(dns_service) like 'yes'");
        $sth->execute();
        my $r2 = $sth->fetchrow_hashref;
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
print DEBUG "DB temp_x:$temp_x\t \n";
	  if($$r2{ip_block} =~ /$parm_val/) {	
	print DEBUG "DB inside IP same r2 loop\n";
	   if($r3->[0]->[0] < 1) {
	print DEBUG "inside r3 loop\n";
	my $sth3 = $dbh->prepare ("SELECT count(value) FROM dns_resolver WHERE value like $parm and username like $uid and status = 3");
	$sth3->execute();
	my $r4 = $sth3->fetchall_arrayref;
	my $sth6 = $dbh->prepare ("SELECT value from dns_resolver WHERE value like '$match_ip%' and username like $uid and status = 1");
	$sth6->execute();
	my $r6 = $sth6->fetchall_arrayref;
	if($r4->[0]->[0] < 1) {
	for my $q_count (0 .. $#{$r6}){
	print DEBUG "inside for r6 loop\n";
	$ip_flag = binary_add($z1,$parm_val,$match_ip);
	print DEBUG "ip_flag:$ip_flag\n";
	}
        if ($ip_flag == 0){
		print DEBUG "inside ip_flag 0 condition\n";
		my $temp_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$match_ip%' and username like $uid"; 
	        $sth = $dbh->prepare ("$temp_sql");
		my $success = $sth->execute;
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
        	my $result = ($success ? $dbh->commit : $dbh->rollback);
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
        	CMS_DB::disconnect_from_transaction();
        	return $error if ($error);
        	#return $result;
		return $success;
		}else{
print DEBUG "CHECK when IP is SAME\n";
		#$uid = 'h999748';
		#$uid = "\'$uid\'";
print DEBUG "uid: $uid\n";
        	my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update) VALUES ($uid, $customer_id, $contact, $host , $parm, 1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP)";

	        $sth = $dbh->prepare ("$temp_sql");
print DEBUG "prepare: $temp_sql\n";
       		#my $success &&= $sth->execute;
		my $success = $sth->execute;
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "success-initial: $success\n";
        	my $result = ($success ? $dbh->commit : $dbh->rollback);
print DEBUG "result before: $result\n";
print DEBUG "success: $success\n";
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "error: $error\n result: $result\n";
        	CMS_DB::disconnect_from_transaction();
        	return $error if ($error);
#        	return $result;
		return $success;
	}} else {
	my $temp_sql = "UPDATE dns_resolver SET status=1, request_time=CURRENT_TIMESTAMP,date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value = $parm and username like $uid";
	$sth = $dbh->prepare ("$temp_sql");
        print DEBUG "prepare: $temp_sql\n";
                #my $success &&= $sth->execute;
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "success-initial: $success\n";
                my $result = ($success ? $dbh->commit : $dbh->rollback);
print DEBUG "success: $success\n";
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                CMS_DB::disconnect_from_transaction();
print DEBUG "error: $error\n result: $result\n";
                return $error if ($error);
                #return $result;
                return $success;
	
        	}
	}
	 print DEBUG "parm else of r3: $parm\n";

	CMS_DB::disconnect_from_transaction();
           return 1;
	  } 


        #  START_CHECK_4_OCTETS_SAME
        $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$z1%' and customer_id like $customer_id AND LOWER(dns_service) like 'yes'");
        $sth->execute();
        my $r4 = $sth->fetchall_arrayref;
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
print DEBUG "DB 4octets same temp_x:$temp_x\t \n";
	for my $lp_count (0 .. $#{$r4}) {
	my $Ar_chk = $r4->[$lp_count]->[0];
	my $ip_block_chk = $Ar_chk;
	print DEBUG "DB ip from db is $ip_block_chk\n";

	my @ip_chk = split(/\//,$ip_block_chk);
	my $ipnew_chk  = shift(@ip_chk);#ipnew_chk has ip
	my $ipprefixip_chk = shift(@ip_chk);#has prefix

print DEBUG "DB ipnew_chk: $ipnew_chk\t \t ipprefixip_chk: $ipprefixip_chk\n"; 
	# CHECK WHEN 4 Octets are SAME and IP PREFIX is 24
        if ( ($ipnew_chk =~ /$z1/) && ($ipprefixip_chk == 24) ) {
print DEBUG "ipprefixip_chk: $ipprefixip_chk\n";
                if ($z2 > $ipprefixip_chk) {
                        print DEBUG "entered z2> loop\n";
                        my $ct_ip = ($z2 - $ipprefixip_chk);
                        print DEBUG "ct_ip:$ct_ip\n";
                        my $ct_ip1 = 0;
                        my $new_z2 = 0;
                        print DEBUG "z2:$z2\n";
                        my $z3 = $z2;
                                for ($ct_ip1 = 0; $ct_ip1 <= $ct_ip; $ct_ip1++){
                                        print DEBUG "ct_ip1:$ct_ip1\n";
                                        $new_z2 = ($z3 - 1);
                                        $z3 = ($z3 - 1);
                                        my $x2 = `$z2 - 1`;
                                        print DEBUG "x2 : $x2\n";
                                        print DEBUG "new_z2:$new_z2\n";
                                        print DEBUG "z3:$z3\n";
                                        my $new_last = join "/",$z1, $new_z2;
                                        print DEBUG "new_last:$new_last\n";
                                        my $sth = $dbh -> prepare("SELECT count(value) from dns_resolver where value like '$new_last'  and customer_id like $customer_id and username like $uid and status = 1");
                                        $sth->execute();
                                        my $query = $sth->fetchall_arrayref;
                                        if ($query->[0]->[0]>=1){
                                                print DEBUG "already exists 24\n";
				return 1;
                                                }#end of if query loop
                                }#end of for loop

                        }#end of ip z2 loop
                my $sth = $dbh->prepare("SELECT count(*) from dns_resolver where customer_id like $customer_id and username like $uid");
                $sth -> execute();
                my $count_ptr = $sth->fetchall_arrayref;
                print DEBUG "count ptr:$count_ptr->[0]->[0]\n";
                if ($count_ptr->[0]->[0] > 0) {
                        if($r3->[0]->[0] >= 1) {
                        print DEBUG "already exists in the DB\n";
                         my $temp_sql = "INSERT into dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update) VALUES ($uid, $customer_id, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP)";
                        $sth = $dbh->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         my $result = ($success ? $dbh->commit : $dbh->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         #CMS_DB::disconnect_from_transaction();
                         return $error if ($error);
                         #return $result;
		 return $success;
                        }
                        } else {
                        print DEBUG "4 oct before insert1\n";
                         my $temp_sql = "INSERT into dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update) VALUES ($uid, $customer_id, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP)";
                        $sth = $dbh->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         my $result = ($success ? $dbh->commit : $dbh->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         #CMS_DB::disconnect_from_transaction();
                         return $error if ($error);
                         #return $result;
		 return $success;
                }#end of if else 
                if($r3->[0]->[0] < 1) {
                        my $sth = $dbh->prepare("SELECT count(value) from dns_resolver where value like '$match_ip%' and customer_id like $customer_id and username like $uid'");
                        print DEBUG "sth:$sth\n";
                        $sth->execute();
                        my $result = $sth->fetchall_arrayref;
                        if (($result->[0]->[0]>=1) && ($z2 == 24)){
                                print DEBUG "most recently added update\n";
                                print DEBUG "UPDATE1error\n";
                                my $temp_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$match_ip%' and username like $uid";
                                $sth = $dbh->prepare ("$temp_sql");
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                my $result = ($success ? $dbh->commit : $dbh->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                CMS_DB::disconnect_from_transaction();
                                return $error if ($error);
                                #return $result;
                                return $success; 
		        
                                }else {
                        if ($z2 != 24){
print DEBUG "inside z2 ne 24 loop\n";
my %eq_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);

my $eq_initial = 0;
my $eq_count_ip = (32 - $z2);
my $eq_count_ip1 = 0;
my $eq_latestz2 = $z2;
my $check_rs = 0;
for ($eq_count_ip1 = 0; $eq_count_ip1 <= $eq_count_ip; $eq_count_ip1++) {
print DEBUG "eq_count_ip:$eq_count_ip\teq_count_ip1:$eq_count_ip1\n";
my $v_next = $eq_latestz2 + 1;
my $eq_w_range = $eq_range{$v_next};
my $eq_hop = 256/$eq_w_range;
my $eq_i = 0;

        for ($eq_i = 1; $eq_i <= $eq_hop; $eq_i++) {
                print DEBUG "inside hop loop for z2 ne 24 condition\n";
                my $sth = $dbh-> prepare("SELECT count(value) from dns_resolver where value like '$match_ip.$eq_initial/$v_next'  and username like $uid and customer_id like $customer_id and status = 1");
                $sth->execute();
                my $rs = $sth->fetchall_arrayref;
                print DEBUG "eq_initial/:$eq_initial\t$eq_w_range\tv_next:$v_next\trs:$rs->[0]->[0]\n";
                        if ($rs->[0]->[0]>=1){
                                print DEBUG "ip_octet4:$ip_octet4 before if check\n";
                                if (($eq_initial >= $ip_octet4) && ($ip_octet4 <= ($eq_initial+$eq_w_range))){
                                $check_rs = $check_rs + 1;
                                
                                print DEBUG "UPDATE2error\n";
                                my  $temp_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$match_ip.$eq_initial/$v_next' and username like $uid";
                $sth = $dbh->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                CMS_DB::disconnect_from_transaction();
                return $error if ($error);
                #return $result;
                return $success;
		 }#end of if ipoct check
                                }#end of rs >= 1

                $eq_initial = $eq_initial+$eq_w_range;

        }#end for for i

        $eq_initial = 0;
        $eq_w_range = $eq_w_range+$eq_w_range;
        $eq_latestz2 = $eq_latestz2 +1;
        if ($eq_latestz2 == 32) { last;}
        print DEBUG "eq_latestz2:$eq_latestz2\teq_w_range:$eq_w_range\n";


}#end of for count ip
        if ($check_rs >= 1){
                    return 1;
                }else {
                        print DEBUG "4 oct before insert2 prefix not 24\n";
                                 my $temp_sql = "INSERT into dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update) VALUES ($uid, $customer_id, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP)";
                                $sth = $dbh->prepare ("$temp_sql");
                                $check_rs = $check_rs+1;
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                my $result = ($success ? $dbh->commit : $dbh->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                return $error if ($error);
                                return $result;
                                }

                }#end of z2 if
                        }#end of result else
                }else {
                        #print DEBUG "inside 24 ipp update cond\n";
                        print DEBUG "UPDATE3error\n";
                        #my $temp_sql = "UPDATE dns_resolver SET fqdn = $dom, request_time = current_timestamp, contact = $contact,status = 1,value = '$value' WHERE ptrrec like $ptr and accno like $accno and rec_type like 'NS' and status = 1 and last_oct like '$value'";
                                #$sth = $dbh->prepare ("$temp_sql");
                                #my $success = $sth->execute;
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #my $result = ($success ? $dbh->commit : $dbh->rollback);
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                #return $error if ($error);
                                #return $result;
                                return 1;
                                }


        #CMS_DB::disconnect_from_transaction();
        #return 1;
        }


	  # CHECK WHEN 4 Octets are SAME and IP PREFIX is different

	elsif ( ($ipnew_chk =~ /$z1/) && ($ipprefixip_chk != $z2) ) {
print DEBUG "DB 4 Octets are SAME and IP PREFIX is different\n";
print DEBUG "DB ipprefixip_chk: $ipprefixip_chk\n";
print DEBUG "DB before check z2:$z2\n";
            if($z2 >= $ipprefixip_chk) {
print DEBUG "DB after check z2:$z2\n";
	my $sth5 = $dbh->prepare("SELECT count(value) FROM dns_resolver WHERE value like '$ip_block_chk' and username like $uid");
	$sth5->execute();
	my $rt1 = $sth5->fetchall_arrayref; 
	if ($rt1->[0]->[0] < 1) {
		if($r3->[0]->[0] < 1) {

                my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update) VALUES ($uid, $customer_id, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP)";

                $sth = $dbh->prepare ("$temp_sql");
print DEBUG "DB inside loop CHECK WHEN 4 Octets are SAME and IP PREFIX is different\n";
print DEBUG "prepare2: $temp_sql\n";
                #my $success &&= $sth->execute;
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "success-initial: $success\n";
                my $result = ($success ? $dbh->commit : $dbh->rollback);
print DEBUG "success: $success\n";
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                CMS_DB::disconnect_from_transaction();
print DEBUG "error: $error\n result: $result\n";
                return $error if ($error);
                #return $result;
                return $success;
	        }
	   }
	      CMS_DB::disconnect_from_transaction();		
              return 1;
	   }	
         } 

	} # ENDED_CHECK_4_OCTETS_SAME



        #  START_CHECK_3_OCTETS_SAME
print DEBUG "DB CHECK_3_OCTETS_SAME\n";
	my $db_flag = 0;
	print DEBUG "initial db_flag:$db_flag\n";
        my $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$match_ip%' and customer_id like $customer_id AND LOWER(dns_service) like 'yes'");
        $sth->execute();
        my $r5 = $sth->fetchall_arrayref;
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
print DEBUG "DB 3 temp_x:$temp_x\n";
	for my $lp_count (0 .. $#{$r5}) {
	my $Ar_chk1 = $r5->[$lp_count]->[0];
	$ip_block_chk1 = $Ar_chk1;
	print DEBUG "DB ip from db is $ip_block_chk1\n\n";

	my @ip_chk1 = split(/\//,$ip_block_chk1);
	my $ipnew_chk1  = shift(@ip_chk1);#ipnew_chk has ip
	my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
	@ip_octets = split(/\./,$ipnew_chk1);
#code to split the stored IPs into octets and changing them to binary
$ip_octet1 = shift(@ip_octets);
$ip_octet2 = shift(@ip_octets);
$ip_octet3 = shift(@ip_octets);
$ip_octet4 = shift(@ip_octets);

$ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
$ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
$ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
$ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

#combining individual IPs into one variable/array
my @ip_binary = ();
push(@ip_binary, $ip_bin_1);
push(@ip_binary, $ip_bin_2);
push(@ip_binary, $ip_bin_3);
push(@ip_binary, $ip_bin_4);
$ip_binary_stat = join("",@ip_binary);
@ip_binary_bit = split(//,$ip_binary_stat);
#print"ip binary is @ip_binary_bit\n";

#code to split the entered IPs into octets and changing octets to binary.
        @z1_octets = split(/\./,$z1);
        $z1_octet1 = shift(@z1_octets);
        $z1_octet2 = shift(@z1_octets);
        $z1_octet3 = shift(@z1_octets);
        $z1_octet4 = shift(@z1_octets);

	$z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
        $z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
        $z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
        $z1_bin_4 = unpack("B8", pack('C',$z1_octet4));

	#combining the individual IPs into one variable/array
	@z1_binary = ();
	push(@z1_binary,$z1_bin_1);
	push(@z1_binary,$z1_bin_2);
	push(@z1_binary,$z1_bin_3);
	push(@z1_binary,$z1_bin_4);
	$z1_binary_stat = join("",@z1_binary);
	@z1_binary_bit = split(//,$z1_binary_stat);
	print DEBUG "input is @z1_binary_bit\n";
	$ipprefix = $ipprefixip_chk1;

	# CHECK WHEN 3 Octets are SAME and IP PREFIX is diff 
        if  ($ipnew_chk1 =~ /$match_ip/)  {
	print DEBUG "DB entered match ip case\n";
        if($r3->[0]->[0] < 1) {
		print DEBUG "DB does not already exist in DB\n";
		print DEBUG "z2:$z2\tipprefix:$ipprefix\n";
		if($z2 > $ipprefix){$ip_length = $ipprefix;}
		else {$ip_length = $z2;}
		print DEBUG "ip length value:$ip_length\n";
		$compare_flag = 1;
	 	$db_flag = 0;	
		for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
		print DEBUG "entered for condition\n";
        		if ($db_flag == 0){
			print DEBUG "DB entered if compare flag condition\n";
       		 	$z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
        		$ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
        			if ($z1_binary_result != $ip_binary_result){
				print DEBUG "DB binary ADD false\n";
				print DEBUG "DB db flag 1\n";
                		$db_flag = 1;
                		}
				elsif ($z1_binary_result == $ip_binary_result)
				{
				print DEBUG "DB equal to binary add\n";	
				print DEBUG "DB db flag 0\n";
				$db_flag = 0;}
			} 
		}#end of both if loops and for loop
	if ($db_flag == 1 && $compare_flag ==1)
		{
		print DEBUG "DB compare flag 1\n";
		$compare_flag = 1;}
	else 
		{
		print DEBUG "DB compare flag 0\n";
		$compare_flag = 0;
		last;
		}
        }#end of r3 if condition
else {	CMS_DB::disconnect_from_transaction();
        return 1;
}#end of else r3
        }#end of ip if condition
}#end of for condition
if ($compare_flag == 1) {
print DEBUG "COMPARE flag found to be 1\n";
print DEBUG "throw error\n";
CMS_DB::disconnect_from_transaction();
return "DNS RESOLVE REQUEST cannot be proceeded with the Entered IP. Please Enter A New IP";
}#end of if
elsif ($compare_flag == 0){
	print DEBUG "DB final check of compare flag 0\n";
	if ($z2 < $ipprefix) {
	print DEBUG "DB entered IP prefix less thn Db ip prefix\n";
	print DEBUG "parent sub range\n";
	if($$r2{ip_block} =~ /$parm_val/) {
		if($r3->[0]->[0] < 1) {
		my $t_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$z1%' and username like $uid";
		print DEBUG "$t_sql\n";
		CMS_DB::begin_transaction();
		my $sth = $dbh->prepare ("$t_sql");
		my $result = $sth->execute;
		print DEBUG "result:$result\n";
		$error = $dbh->errstr;
		print DEBUG "error:$error\n";
		#return $result;
                return $success;
		CMS_DB::disconnect_from_transaction();
		}	
	}}
	else {
	print DEBUG "success\n";
	print DEBUG "entered else of $z2 > $ipprefix\n";
	 print DEBUG "sub range\n";
	print DEBUG "parm_val:$parm_val\n";
	print DEBUG "entered subrange parm block\n";
 my $sthr = $dbh->prepare ("SELECT count(value) FROM dns_resolver WHERE value like $parm and username like $uid and status = 1");
         $sthr->execute();
        my $r_1 = $sthr->fetchall_arrayref;
        print DEBUG "outside r_1 condn\n";
        print DEBUG "($r_1->[0]->[0])\n";
        if($r_1->[0]->[0] < 1) {
        print DEBUG "inside r_1 condition\n";
        if($r3->[0]->[0] < 1) {
        print DEBUG "entered r3 loop\n";
my %v_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2);
my $v_initial = 0;
my $count_ip = ($z2 - $ipprefix);
print DEBUG "ipprefix:$ipprefix\n";
my $count_ip1 = 0;
my $latestz2 = $z2;

for ($count_ip1 = 1; $count_ip1 <= $count_ip; $count_ip1++) {

my $v_previous = $latestz2 -1;
my $w_range = $v_range{$v_previous};
print DEBUG "w_range:$w_range\n";
my $v_hop = (256/($w_range));
print DEBUG "v_previous:$v_previous\tw_range:$w_range\tv_hop:$v_hop\n";
my $i = 0;
print DEBUG "beforei loop\n";
print DEBUG "count_ip:$count_ip\tcount_ip1:$count_ip1\n";
        for ($i = 1; $i <= $v_hop; $i++) {
                print DEBUG "inside i-v_hop loop\n";
                my $sth = $dbh-> prepare("SELECT count(value) from dns_resolver where value like '$match_ip.$v_initial/$v_previous' and customer_id like $customer_id and status = 1");
                $sth->execute();
                print DEBUG "i inside :$i\n";
                my $rs = $sth->fetchall_arrayref;
                        print DEBUG  "SELECT count(value) from dns_resolver where value like '$match_ip.$v_initial/$v_previous'  and customer_id like $customer_id and status = 1\n";
                        print DEBUG "rs:$rs->[0]->[0]\n";
                        print DEBUG "ip_octets4:$ip_octets4 before if check\n";
                        if ($rs->[0]->[0]>=1){
                                print DEBUG "v_initial:$v_initial\tw_range:$w_range\n";
                                if (($v_initial <= $ip_octets4) && ($ip_octets4 <= ($v_initial+$w_range))){
                                                print DEBUG "just before throwin error..newly removed compare condition\n";
                                                return 1;
                                }#end of ipoct if
                        }#end of if rs

                $v_initial = $v_initial+$w_range;
                print DEBUG "after rs loop:v_initial:$v_initial\tw_range:$w_range\n";
                print DEBUG "for i loop ends\n";
        }#end for for i
        $v_initial = 0;
        $latestz2 = $latestz2 -1;
        $w_range = $w_range+$w_range;

}#end of for count

#to check for parent IP
my %new_v_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);
my $check_rs = 0;
my $new_v_initial = 0;
my $new_count_ip = (32 - $z2);
my $new_count_ip1 = 0;
my $new_latestz2 = $z2;
print DEBUG "new_latestz2:$new_latestz2\n";

for ($new_count_ip1 = 0; $new_count_ip1 <= $new_count_ip; $new_count_ip1++) {
print DEBUG "new_count_ip1:$new_count_ip1\tnew_count_ip:$new_count_ip\n";

my $v_next = $new_latestz2 + 1;
my $new_w_range = $new_v_range{$v_next};
my $new_v_hop = 256/$new_w_range;
my $new_i = 0;
print DEBUG "v_next:$v_next\tnew_w_range:$new_w_range\tnew_v_hop:$new_v_hop\n";

        for ($new_i = 1; $new_i <= $new_v_hop; $new_i++) {
                print DEBUG "new_i:$new_i\tnew_v_hop:$new_v_hop\n";
                print DEBUG "before select count:new_v_initial:$new_v_initial\tv_next:$v_next\n";
                my $t_r = "SELECT count(value) from dns_resolver where value like '$match_ip.$new_v_initial/$v_next'  and customer_id like $customer_id and status = 1";
                print DEBUG "t_r:$t_r\n";
                my $sth_r = $dbh->prepare("$t_r");
                $sth_r->execute();
                my $rs_r = $sth_r->fetchall_arrayref;
                                print DEBUG "rs:$rs_r->[0]->[0]\n";
                        print DEBUG "ip_octets4:$ip_octets4 before if check\n";
                        if ($rs_r->[0]->[0]>=1){
                                print DEBUG "inside rs 1 condition rs:$rs_r->[0]->[0]\tip_octets4:$ip_octets4\n";
                                if (($new_v_initial >= $ip_octets4) && ($ip_octets4 <= ($new_v_initial+$new_w_range)))					{
                                                print DEBUG "UPDATE6error\n";
						$check_rs = $check_rs+1;
		my $temp_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$match_ip.$new_v_initial/$v_next' and username like $uid";
		print DEBUG "temp_sql:$temp_sql\n";
                $sth = $dbh->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                CMS_DB::disconnect_from_transaction();
                return $error if ($error);
                #return $result;
                return $success;

    }#end of if oct
                        }#end of if rs

                $new_v_initial = $new_v_initial+$new_w_range;

        }#end for for i

        $new_v_initial = 0;
        $new_w_range = $new_w_range+$new_w_range;
        $new_latestz2 = $new_latestz2 +1;
        if ($new_latestz2 == 32) { last;}

}#end of for count ip
if ($check_rs >= 1){ 
return 1;
         }

 print DEBUG "before insert\n";
                my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update) VALUES ($uid, $customer_id, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP)";
                $sth = $dbh->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                #CMS_DB::disconnect_from_transaction();
                return $error if ($error);
#                return $result;
	   return $success;
                } else { print DEBUG "IP block already exists in the DB, else of r3\n";
                        my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update) VALUES ($customer_id, $customer_id, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP)";
                        $sth = $dbh->prepare ("$temp_sql");
                        my $success = $sth->execute;
                        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                        my $result = ($success ? $dbh->commit : $dbh->rollback);
                        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                        #CMS_DB::disconnect_from_transaction();
                        return $error if ($error);
                        return $success;
                        }#else of r3

        }else {print DEBUG "range already exists in DB\n";
                if ($ip_block_chk1 eq $parm){
                print DEBUG "UPDATE7error\n";
 return 1;
                } else {
                print DEBUG "sub range case..throw error\n";
                return 1;
                }
        }
 print DEBUG "r3 neither <1 nor else condition\n";
        }
}#end of elsif


  # ENDED_CHECK_3_OCTETS_SAME


        print DEBUG "FINAL ERROR: DNS RESOLVE REQUEST cannot be proceeded with the Entered IP. Please Enter A New IP.\n";
        return "DNS RESOLVE REQUEST cannot be proceeded with the Entered IP. Please Enter A New IP";

	} else {
	print DEBUG "User Exceeded Allowed Limit for DNS Resolve Service (max limit 10).\n";
	return "User Exceeded Allowed Limit for DNS Resolve Service - max limit 10";
	}
}else {
 print DEBUG "IP Address block does not exist in ip_resource\n";
return "DNS RESOLVE REQUEST cannot be proceeded with the Entered IP. Please Enter A New IP"; 
}
} else {
     print DEBUG "Account does not exist.\n";
     return "Account does not exist";
}

close (DEBUG);
}

open (DBG , ">/tmp/view_dnsreslv");
print DBG "CHECK1\n";
sub view_dns_resolver {
print DBG "CHECK2\n";
        my $username = $dbh->quote(shift @_);

print DBG "CHECK3 username: $username\n";
        my $sth = $dbh->prepare
                ("SELECT customer_id, contact, host, value
                        FROM dns_resolver
                        WHERE username = $username and status = 1
                        ORDER BY customer_id, contact, host, value
                ");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
print DBG "result: $result->[0]->[0] \n";
        $error = $dbh->errstr;
close (DBG);
        return $result;
}

open (HKG, ">/tmp/get_acl");
sub get_acl_parm_location {
my $value=shift @_;
print "id:$value\n";
my $sth;
print HKG "ENTERED\n";
		
	 if($value == '44889'){
        $sth = $dbh->prepare("SELECT value from dns_resolver join service on dns_resolver.service_id=service.service_id where status=1 and parent_site_id='44889'");
	 }
	 elsif($value == '44892'){
        $sth = $dbh->prepare("SELECT value from dns_resolver join service on dns_resolver.service_id=service.service_id where status=1 and parent_site_id='44892'");
	 }
	 else{
        $sth = $dbh->prepare("SELECT value from dns_resolver join service on dns_resolver.service_id=service.service_id where status=1 and parent_site_id not in ('44889','44892') or parent_site_id is null");
	 }

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        $error = $dbh->errstr;
        return $error if ($error);
        return $result;
}
close (HKG);
open (HKG, ">/tmp/cancel_resolver");
sub cancel_dns_resolver {
print HKG "ENTERED\n";
        my $uid     = $dbh->quote(shift @_);
        my $customer_id   = $dbh->quote(shift @_);
        my $contact = $dbh->quote(shift @_);
        my $host    = $dbh->quote(shift @_);
        my $parm    = $dbh->quote(shift @_);
        my $parm_val = $parm;
        my $result = '';

        $parm_val =~ s/\'//g;
        my @y = split(/\//,$parm_val);
        my $z1= shift(@y);
        my $z2= shift(@y);
print HKG "z1: $z1\n z2: $z2\n";

# Users who have requested for DNS Service are only allowed to cancel the Service.
        my $sth = $dbh->prepare ("SELECT count(p.username) FROM  dns_resolver p, olss_cust_access_acc o where o.customer_id = p.customer_id and p.username like $uid");
        $sth->execute();
        my $r1 = $sth->fetchall_arrayref;
if($r1->[0]->[0] >= 1) {

# username checked in ip_resource block
      #  my $sth3 = $dbh->prepare ("SELECT count(ip_block) FROM ip_resource WHERE ip_block like '$parm_val', accno like $accno");
#print HKG "DB ip resource check parm_val:$parm_val accno:$accno\n";
      #  $sth3->execute();
#print HKG "DB sth3:$sth\n";
      #  my $rt = $sth3->fetchall_arrayref;
##print HKG "DB rt:$rt\n";
#print HKG "entered ip resource check\n";

my $sth1 = $dbh->prepare ("SELECT count(value) FROM dns_resolver where value = $parm and username = $uid");
        $sth1->execute();
        my $r2 = $sth1->fetchall_arrayref;
print HKG "DB r2:$r2\n";
print HKG "DB inside r1 loop\n";
        if($r2->[0]->[0] >= 1){

        my $success = 1;

       # $sth = $dbh->prepare("DELETE FROM dns_resolver WHERE value = $parm");
	$sth = $dbh->prepare("UPDATE dns_resolver SET status = 3, request_time = CURRENT_TIMESTAMP  WHERE value = $parm and username = $uid");
        $success &&= $sth->execute;
        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;

print HKG "error: $error\n";
        $result = ($success ? $dbh->commit : $dbh->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;

print HKG "error: $error\t result: $result\n";
print HKG "OUTSIDE1 result: $result\n ";
        return $error if ($error);
        #return $result;
	return $success;
}else{
$result = 0;
print HKG "OUTSIDE2 result: $result\n ";
return $result;
}
} else {
$result = "User $uid does not have the rights to cancel DNS Service for this IP Block\n";;
print HKG "OUTSIDE2 result: $result\n ";
return $result;
}
close (HKG);
}


sub count_rows {
open (HKG, ">/tmp/count_r");
print HKG "ENTERED\n";
        my $sth = $dbh->prepare("SELECT count(*) from dns_resolver");
print HKG "sth: $sth\n";
       unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
	my $count = ($result->[0]->[0]);
print HKG "result: ($result->[0]->[0])\n";
print HKG "count: $count\n";
        $error = $dbh->errstr;
        return $error if ($error);
       return $count;

close (HKG);
}

sub all_values {
open (HKG, ">/tmp/all_val");
print HKG "ENTERED\n";
#my ($new_ip) = $dbh->quote(shift @_);
my ($reqtime) = $dbh->quote(shift @_);
my $value=shift @_;
my $temp;
print HKG "$reqtime\n";
#my $temp = "SELECT username, accno, contact, host, value, status FROM dns_resolver where value like $new_ip";
if($value == '44889'){
	$temp = "SELECT username, dns_resolver.customer_id, contact, host, value, status FROM dns_resolver join service on dns_resolver.service_id=service.service_id where  TO_CHAR(request_time,'YYYY-MM-DD HH24.MI.SSXFF TZR') like $reqtime
	 and parent_site_id= '44889'";

}
elsif($value=='44892'){
	$temp = "SELECT dns_resolver.customer_id, username, contact, host, value, status FROM dns_resolver join service on dns_resolver.service_id=service.service_id where  TO_CHAR(request_time,'YYYY-MM-DD HH24.MI.SSXFF TZR') like $reqtime
	 and parent_site_id = '44892'";
}
else{
	$temp = "SELECT dns_resolver.customer_id, username, contact, host, value, status FROM dns_resolver join service on dns_resolver.service_id=service.service_id where  TO_CHAR(request_time,'YYYY-MM-DD HH24.MI.SSXFF TZR') like $reqtime
	 and parent_site_id not in ('44889','44892') or parent_site_id is null";
}
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
  return $result;

close (HKG);
}

sub DNSRESOLVER_mail_REQ {
  #my $firststr = shift(@_);
 my ($uid, $customer_id, $dns_contact, $dns_dom, $dns_aip, $value_type) = @_;
my @email_ar = split('^^', $uid);
  my $em_ar = shift(@email_ar);
  my $note = "Note: This email has been automatically generated. Please do not reply to this message as it is unattended.";
  my $message = ("$value_type DNS RESOLVER Service for following IP:\n\nUsername:\t$em_ar\nIP REQ:\t\t$dns_aip\nEmail:\t\t$dns_contact\nAccount:\t$customer_id\nHostname:\t$dns_dom\n\nThank you.\n\n$note");
  my $subj = "$value_type DNS RESOLVER SERVICE";

  my $to_mail = $dns_contact;
  my $bcc_mail = "TI.DL-GL-TI_EM\@team.telstra.com; TI.infosyscms\@team.telstra.com";
  my $from_mail = "\"Telstra Global Online Services\" (tgos\@telstrainternational.com)";
  my %mail = (
           To => $to_mail,
	   Bcc => $bcc_mail,
           From => $from_mail,
           Subject => $subj
  );
  my $hostname = `hostname`;
  print "$hostname";
  $mail{Smtp} = ' HHTRED161.in.sa.telstrainternational.com';
  $mail{body} = $message;

  if (sendmail (%mail)) {
    print "\nMail sent success to $to_mail.\n" ;
  } else {
    print "Error sending mail to $to_mail: $Mail::Sendmail::error \n";
  }

}

sub status_ip {
open (HKG, ">/tmp/status_ip");
print HKG "ENTERED\n";
my $temp = "SELECT value from dns_resolver where status=1";
        my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
	$sth->execute();
	my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
  	return $result;

close (HKG);
}

sub time_db {
open (HKG, ">/tmp/dns_time_rec");
my $value=shift @_;
my $temp;
print HKG "ENTERED\n";
if($value=='44889'){
$temp = "SELECT TO_CHAR(request_time,'YYYY-MM-DD HH24.MI.SSXFF TZR') from dns_resolver join service on dns_resolver.service_id=service.service_id where parent_site_id='44889'";
}
elsif($value=='44892'){
$temp = "SELECT TO_CHAR(request_time,'YYYY-MM-DD HH24.MI.SSXFF TZR') from dns_resolver join service on dns_resolver.service_id=service.service_id where parent_site_id='44892'";
}
else{
$temp = "SELECT TO_CHAR(request_time,'YYYY-MM-DD HH24.MI.SSXFF TZR') from dns_resolver join service on dns_resolver.service_id=service.service_id where parent_site_id not in ('44889','44892')";
}
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
return $result;
close (HKG);
}

sub binary_add {#binary and of ip blocks
my @z1_octets = "";
        my $z1_octet1 = "";
        my $z1_octet2 = "";
        my $z1_octet3 = "";
        my $z1_octet4 = "";
        my $z1_bin_1 = "";
        my $z1_bin_2 = "";
        my $z1_bin_3 = "";
        my $z1_bin_4 = "";
        my @z1_binary = "";
        my $z1_binary_stat = "";
        my @z1_binary_bit = "";
        my @ip_octets = "";
        my $ip_octet1 = "";
        my $ip_octet2 = "";
        my $ip_octet3 = "";
        my $ip_octet4 = "";
        my $ip_bin_1 = "";
        my $ip_bin_2 = "";
        my $ip_bin_3 = "";
        my $ip_bin_4 = "";
        my @ip_binary = "";
        my $ip_binary_stat = "";
        my @ip_binary_bit = "";
        my $ip_length = "";
        my $ip_binary_result = "";
        my $z1_binary_result = "";
        my $loop_count2 = 0;
        my $ipprefix = "";
        my $compare_flag=0;
my $db_flag=0;
my ($parm_val,$z1,$z2) = @_;

my @ip_chk1 = split(/\//,$parm_val);
my $ipnew_chk1  = shift(@ip_chk1);#ipnew_chk has ip
my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
@ip_octets = split(/\./,$ipnew_chk1);
#code to split the stored IPs into octets and changing them to binary
$ip_octet1 = shift(@ip_octets);
$ip_octet2 = shift(@ip_octets);
$ip_octet3 = shift(@ip_octets);
$ip_octet4 = shift(@ip_octets);

$ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
$ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
$ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
$ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

#combining individual IPs into one variable/array
@ip_binary = ();
push(@ip_binary, $ip_bin_1);
push(@ip_binary, $ip_bin_2);
push(@ip_binary, $ip_bin_3);
push(@ip_binary, $ip_bin_4);
$ip_binary_stat = join("",@ip_binary);
@ip_binary_bit = split(//,$ip_binary_stat);
#print"ip binary is @ip_binary_bit\n";

#code to split the entered IPs into octets and changing octets to binary.
        @z1_octets = split(/\./,$z1);
        $z1_octet1 = shift(@z1_octets);
        $z1_octet2 = shift(@z1_octets);
        $z1_octet3 = shift(@z1_octets);
        $z1_octet4 = shift(@z1_octets);

        $z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
        $z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
        $z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
        $z1_bin_4 = unpack("B8", pack('C',$z1_octet4));

 #combining the individual IPs into one variable/array
        @z1_binary = ();
        push(@z1_binary,$z1_bin_1);
        push(@z1_binary,$z1_bin_2);
        push(@z1_binary,$z1_bin_3);
        push(@z1_binary,$z1_bin_4);
        $z1_binary_stat = join("",@z1_binary);
        @z1_binary_bit = split(//,$z1_binary_stat);
        print DEBUG "input is @z1_binary_bit\n";
        $ipprefix = $ipprefixip_chk1;
if($z2 > $ipprefix){$ip_length = $ipprefix;}
                else {$ip_length = $z2;}
                print DEBUG "ip length value:$ip_length\n";
                $compare_flag = 1;
                for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
                print DEBUG "entered for condition\n";
                        if ($db_flag == 0){
                        print DEBUG "DB entered if compare flag condition\n";
                        $z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
                        $ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
                                if ($z1_binary_result != $ip_binary_result){
                                print DEBUG "DB binary ADD false\n";
                                print DEBUG "DB db flag 1\n";
                                $db_flag = 1;
                                }
                                elsif ($z1_binary_result == $ip_binary_result)
                                {
                                print DEBUG "DB equal to binary add\n";
                                print DEBUG "DB db flag 0\n";
                                $db_flag = 0;}
                        }
                }#end of both if loops and for loop
        if ($db_flag == 1 && $compare_flag ==1)
                {
                print DEBUG "DB compare flag 1\n";
                $compare_flag = 1;}
        else
                {
                print DEBUG "DB compare flag 0\n";
                $compare_flag = 0;}

return $compare_flag;

}
1;
