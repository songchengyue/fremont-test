/*
 * An XML document type.
 * Localname: updateProbeScopeRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one updateProbeScopeRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class UpdateProbeScopeRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequestDocument
{
    
    public UpdateProbeScopeRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName UPDATEPROBESCOPEREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "updateProbeScopeRequest");
    
    
    /**
     * Gets the "updateProbeScopeRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequest getUpdateProbeScopeRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequest)get_store().find_element_user(UPDATEPROBESCOPEREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "updateProbeScopeRequest" element
     */
    public void setUpdateProbeScopeRequest(com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequest updateProbeScopeRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequest)get_store().find_element_user(UPDATEPROBESCOPEREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequest)get_store().add_element_user(UPDATEPROBESCOPEREQUEST$0);
            }
            target.set(updateProbeScopeRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "updateProbeScopeRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequest addNewUpdateProbeScopeRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.UpdateProbeScopeRequest)get_store().add_element_user(UPDATEPROBESCOPEREQUEST$0);
            return target;
        }
    }
}
