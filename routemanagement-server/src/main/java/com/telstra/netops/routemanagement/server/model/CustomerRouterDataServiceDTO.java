package com.telstra.netops.routemanagement.server.model;

import java.util.List;

public class CustomerRouterDataServiceDTO {
	private String asNumber;
	private List<String> telstraServiceId;
	private List<String> serviceId;
	private String routerName;
	private String customerID;

	public String getAsNumber() {
		return asNumber;
	}

	public void setAsNumber(String asNumber) {
		this.asNumber = asNumber;
	}

	public List<String> getTelstraServiceId() {
		return telstraServiceId;
	}

	public void setTelstraServiceId(List<String> telstraServiceId) {
		this.telstraServiceId = telstraServiceId;
	}

	public List<String> getserviceId() {
		return serviceId;
	}

	public void setServiceId(List<String> serviceId) {
		this.serviceId = serviceId;
	}

	public String getRouterName() {
		return routerName;
	}

	public void setRouterName(String routerName) {
		this.routerName = routerName;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
}
