/*
 * XML Type:  getProbeIPsResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML getProbeIPsResponse(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class GetProbeIPsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse
{
    
    public GetProbeIPsResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PROBEIPS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "probeIPs");
    
    
    /**
     * Gets the "probeIPs" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.IPList getProbeIPs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IPList target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IPList)get_store().find_element_user(PROBEIPS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "probeIPs" element
     */
    public void setProbeIPs(com.hp.schemas.ucmdb.discovery._1.types.IPList probeIPs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IPList target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IPList)get_store().find_element_user(PROBEIPS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.types.IPList)get_store().add_element_user(PROBEIPS$0);
            }
            target.set(probeIPs);
        }
    }
    
    /**
     * Appends and returns a new empty "probeIPs" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.IPList addNewProbeIPs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.IPList target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.IPList)get_store().add_element_user(PROBEIPS$0);
            return target;
        }
    }
}
