/*
 * An XML document type.
 * Localname: getFilteredCIsByTypeResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getFilteredCIsByTypeResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetFilteredCIsByTypeResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponseDocument
{
    
    public GetFilteredCIsByTypeResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETFILTEREDCISBYTYPERESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getFilteredCIsByTypeResponse");
    
    
    /**
     * Gets the "getFilteredCIsByTypeResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse getGetFilteredCIsByTypeResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse)get_store().find_element_user(GETFILTEREDCISBYTYPERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getFilteredCIsByTypeResponse" element
     */
    public void setGetFilteredCIsByTypeResponse(com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse getFilteredCIsByTypeResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse)get_store().find_element_user(GETFILTEREDCISBYTYPERESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse)get_store().add_element_user(GETFILTEREDCISBYTYPERESPONSE$0);
            }
            target.set(getFilteredCIsByTypeResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getFilteredCIsByTypeResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse addNewGetFilteredCIsByTypeResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse)get_store().add_element_user(GETFILTEREDCISBYTYPERESPONSE$0);
            return target;
        }
    }
}
