/*
 * An XML document type.
 * Localname: calculateImpactResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * A document containing one calculateImpactResponse(@http://schemas.hp.com/ucmdb/1/params/impact) element.
 *
 * This is a complex type.
 */
public class CalculateImpactResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponseDocument
{
    
    public CalculateImpactResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CALCULATEIMPACTRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "calculateImpactResponse");
    
    
    /**
     * Gets the "calculateImpactResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse getCalculateImpactResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse)get_store().find_element_user(CALCULATEIMPACTRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "calculateImpactResponse" element
     */
    public void setCalculateImpactResponse(com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse calculateImpactResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse)get_store().find_element_user(CALCULATEIMPACTRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse)get_store().add_element_user(CALCULATEIMPACTRESPONSE$0);
            }
            target.set(calculateImpactResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "calculateImpactResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse addNewCalculateImpactResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponse)get_store().add_element_user(CALCULATEIMPACTRESPONSE$0);
            return target;
        }
    }
}
