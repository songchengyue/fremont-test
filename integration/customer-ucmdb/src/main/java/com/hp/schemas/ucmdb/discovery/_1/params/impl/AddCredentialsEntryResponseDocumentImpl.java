/*
 * An XML document type.
 * Localname: addCredentialsEntryResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one addCredentialsEntryResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class AddCredentialsEntryResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponseDocument
{
    
    public AddCredentialsEntryResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDCREDENTIALSENTRYRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "addCredentialsEntryResponse");
    
    
    /**
     * Gets the "addCredentialsEntryResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse getAddCredentialsEntryResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse)get_store().find_element_user(ADDCREDENTIALSENTRYRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "addCredentialsEntryResponse" element
     */
    public void setAddCredentialsEntryResponse(com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse addCredentialsEntryResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse)get_store().find_element_user(ADDCREDENTIALSENTRYRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse)get_store().add_element_user(ADDCREDENTIALSENTRYRESPONSE$0);
            }
            target.set(addCredentialsEntryResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "addCredentialsEntryResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse addNewAddCredentialsEntryResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse)get_store().add_element_user(ADDCREDENTIALSENTRYRESPONSE$0);
            return target;
        }
    }
}
