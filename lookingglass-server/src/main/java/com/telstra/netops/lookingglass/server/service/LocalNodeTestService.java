package com.telstra.netops.lookingglass.server.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.lookingglass.server.model.ClientNodeTestRequest;
import com.telstra.netops.lookingglass.server.model.NodeTestResult;

@Service
@Profile("local")
public class LocalNodeTestService implements NodeTestService {

	@Autowired
	private LocalErrorThrowingService errorThrowingService;

	public NodeTestResult performNodeTest(ClientNodeTestRequest request) {
		errorThrowingService.throwError();

		// fake a result
		if (request.getNumPings() == 5) {
			return new NodeTestResult(new Date(), 1.271f, 1.82f, 1.422f, 0.146f, 0f,
					"1", "1", "1", "1", "1", null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null);
		} else if (request.getNumPings() == 10) {
			return new NodeTestResult(new Date(), 1.271f, 1.82f, 1.422f, 0.146f, 0f,
					"1", "1", "1", "1", "1", "1", "1", "1", "1", "1",
					null, null, null, null, null, null, null, null, null, null);
		}  else if (request.getNumPings() == 15) {
			return new NodeTestResult(new Date(), 1.271f, 1.82f, 1.422f, 0.146f, 0f,
					"1", "1", "1", "1", "1", "1", "1", "1", "1", "1",
					"1", "1", "1", "1", "1", null, null, null, null, null);
		} else if (request.getNumPings() == 20) {
			return new NodeTestResult(new Date(), 1.271f, 1.82f, 1.422f, 0.146f, 0f,
					"1", "1", "1", "1", "1", "1", "1", "1", "1", "1",
					"1", "1", "1", "1", "1", "1", "1", "1", "1", "1");
		}
		
		return null;
	}

	@Override
	public NodeTestResult performNodeHostTest(ClientNodeTestRequest request) {
		errorThrowingService.throwError();

		// fake a result
		return new NodeTestResult(new Date(), 1.271f, 1.82f, 1.422f, 0.146f, 0f,
				"1", "1", "1", "1", "1", "1", "1", "1", "1", "1",
				null, null, null, null, null, null, null, null, null, null);
	}

}
