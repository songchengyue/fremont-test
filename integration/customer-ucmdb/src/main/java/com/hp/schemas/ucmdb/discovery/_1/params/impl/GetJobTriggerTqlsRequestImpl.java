/*
 * XML Type:  getJobTriggerTqlsRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML getJobTriggerTqlsRequest(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class GetJobTriggerTqlsRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest
{
    
    public GetJobTriggerTqlsRequestImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName JOBNAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "jobName");
    private static final javax.xml.namespace.QName CMDBCONTEXT$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "CmdbContext");
    
    
    /**
     * Gets the "jobName" element
     */
    public java.lang.String getJobName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(JOBNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "jobName" element
     */
    public org.apache.xmlbeans.XmlString xgetJobName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(JOBNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "jobName" element
     */
    public void setJobName(java.lang.String jobName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(JOBNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(JOBNAME$0);
            }
            target.setStringValue(jobName);
        }
    }
    
    /**
     * Sets (as xml) the "jobName" element
     */
    public void xsetJobName(org.apache.xmlbeans.XmlString jobName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(JOBNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(JOBNAME$0);
            }
            target.set(jobName);
        }
    }
    
    /**
     * Gets the "CmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$2);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "CmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$2);
            return target;
        }
    }
}
