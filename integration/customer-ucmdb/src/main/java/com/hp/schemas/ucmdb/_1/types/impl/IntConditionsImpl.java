/*
 * XML Type:  IntConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML IntConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IntConditionsImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.IntConditions
{
    
    public IntConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INTCONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intCondition");
    
    
    /**
     * Gets array of all "intCondition" elements
     */
    public com.hp.schemas.ucmdb._1.types.IntCondition[] getIntConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INTCONDITION$0, targetList);
            com.hp.schemas.ucmdb._1.types.IntCondition[] result = new com.hp.schemas.ucmdb._1.types.IntCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "intCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.IntCondition getIntConditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntCondition)get_store().find_element_user(INTCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "intCondition" element
     */
    public int sizeOfIntConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTCONDITION$0);
        }
    }
    
    /**
     * Sets array of all "intCondition" element
     */
    public void setIntConditionArray(com.hp.schemas.ucmdb._1.types.IntCondition[] intConditionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(intConditionArray, INTCONDITION$0);
        }
    }
    
    /**
     * Sets ith "intCondition" element
     */
    public void setIntConditionArray(int i, com.hp.schemas.ucmdb._1.types.IntCondition intCondition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntCondition)get_store().find_element_user(INTCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(intCondition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "intCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.IntCondition insertNewIntCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntCondition)get_store().insert_element_user(INTCONDITION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "intCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.IntCondition addNewIntCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntCondition)get_store().add_element_user(INTCONDITION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "intCondition" element
     */
    public void removeIntCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTCONDITION$0, i);
        }
    }
}
