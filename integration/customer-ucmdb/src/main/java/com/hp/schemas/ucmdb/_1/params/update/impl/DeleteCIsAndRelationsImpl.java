/*
 * XML Type:  deleteCIsAndRelations
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * An XML deleteCIsAndRelations(@http://schemas.hp.com/ucmdb/1/params/update).
 *
 * This is a complex type.
 */
public class DeleteCIsAndRelationsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations
{
    
    public DeleteCIsAndRelationsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "cmdbContext");
    private static final javax.xml.namespace.QName DATASTORE$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "dataStore");
    private static final javax.xml.namespace.QName CISANDRELATIONSUPDATES$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "CIsAndRelationsUpdates");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets the "dataStore" element
     */
    public java.lang.String getDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATASTORE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "dataStore" element
     */
    public org.apache.xmlbeans.XmlString xgetDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DATASTORE$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "dataStore" element
     */
    public boolean isSetDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DATASTORE$2) != 0;
        }
    }
    
    /**
     * Sets the "dataStore" element
     */
    public void setDataStore(java.lang.String dataStore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATASTORE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DATASTORE$2);
            }
            target.setStringValue(dataStore);
        }
    }
    
    /**
     * Sets (as xml) the "dataStore" element
     */
    public void xsetDataStore(org.apache.xmlbeans.XmlString dataStore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DATASTORE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DATASTORE$2);
            }
            target.set(dataStore);
        }
    }
    
    /**
     * Unsets the "dataStore" element
     */
    public void unsetDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DATASTORE$2, 0);
        }
    }
    
    /**
     * Gets the "CIsAndRelationsUpdates" element
     */
    public com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates getCIsAndRelationsUpdates()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().find_element_user(CISANDRELATIONSUPDATES$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CIsAndRelationsUpdates" element
     */
    public void setCIsAndRelationsUpdates(com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates cIsAndRelationsUpdates)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().find_element_user(CISANDRELATIONSUPDATES$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().add_element_user(CISANDRELATIONSUPDATES$4);
            }
            target.set(cIsAndRelationsUpdates);
        }
    }
    
    /**
     * Appends and returns a new empty "CIsAndRelationsUpdates" element
     */
    public com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates addNewCIsAndRelationsUpdates()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().add_element_user(CISANDRELATIONSUPDATES$4);
            return target;
        }
    }
}
