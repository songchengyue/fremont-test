###################################################################
# File        : CMS_CUST_SERVICE.pm
# Author:       Karuna Ballal
# Date:         28th Sep 2010
# Description  :Perl module to process the html parameters to add,
#		edit,delete and search customer based services
###################################################################
# Modification History :
#  Date               Name                 Change/Description
# ------------------------------------------------------------------
# 28SEP2010     KARUNA BALLAL           Created for IP Transit ph4a
#######################################################################
##! /usr/local/bin/perl
# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;
package CMS_CUST_SERVICE;
use Exporter;
use DBI;
use strict;
use warnings;
use CMS_CUST_SERVICE_DB;
use CMS_SERVICE_DB;
use CMS_DB;
our $error;
my $debug = 1;
#to disable debug statements, uncomment the following line
#$debug = 0;

sub start {
	if ($debug){open (IPC1, ">>/data1/tmp_log/addcustserv.log");}
        if ($debug){print IPC1 "entered CMS_CUST_SERVICE pm file\n";}
	if ((not defined($CMS::html_values{command}{value})) || ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} =~ /cancel/i))) {
                # default to add_cust_serv
                CMS::clear_html_values();
                $CMS::html_values{subsystem}{value} = "cust_service";
		$CMS::html_values{command}{value} = "add_cust_serv";
		if ($debug){print IPC1 "PM entered first IF\n";}
	}
	
        if (defined $CMS::html_values{stage}{value}) {
               unless (CMS::check_screen($CMS::html_values{current_screen}{value})){
                        $CMS::html_values{stage}{value}--;
                }
        }
	if ($debug){print IPC1 "PM before entering conditions:submit value - $CMS::html_values{submit}{value}\n";}
	if ($debug){print IPC1 "PM before entering conditions:command value - $CMS::html_values{command}{value}\n";}

	if($CMS::html_values{command}{value} eq "add_cust_serv") {
		$CMS::html_values{productcode}{options} = CMS_CUST_SERVICE_DB::get_productcode();
		if (not defined ($CMS::html_values{submit}{value})) {
			CMS::add_error("Select a Product.");
                        CMS::output_html("cust_service");

        	} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{stage}{value} == 2 )) {
			if ($debug){print IPC1 "PM stage value 2 condition\n";}
			CMS::output_html("cust_service_add");

		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Add")){
			#Add a new customer based service
	        	if ($debug){print IPC1 "ADD module\n";}
			$CMS::html_values{service}{value} =~ s/\s+//g;
                	$CMS::html_values{service}{value} =~ tr/A-Z/a-z/;
			my $res="";
			my $ipc_sid = $CMS::html_values{service}{value};
			if ($debug){print IPC1 "PM service value:$CMS::html_values{service}{value}\n";}
#Commented because of CR-TGC0032462 
			#$ipc_sid = substr($ipc_sid,5,3);
			if ($debug){print IPC1 "PM ipc_sid:$ipc_sid\n";}
#Changed the serviceid condition -TGC0032462
			if ($ipc_sid =~ /ipc/) {
				#IPC Services
				if ($debug){print IPC1 "ipc found\n";}
				$res = CMS_CUST_SERVICE_DB::add_cust_service(
				$CMS::html_values{service}{value},
				$CMS::html_values{accno}{value},
				$CMS::html_values{description}{value},
				"IPTRANSIT",
				$CMS::html_values{committed_rate}{value},
				$CMS::html_values{dir_traffic}{value},
				$CMS::html_values{as_no}{value}
				);
				if ($res == "1") {
					if ($debug){print IPC1 "PM res = 1 ONCE\n";}
					CMS::add_error("IPC Service added successfully.");
                		} else {
					if ($debug){print IPC1 "PM res = error $res\n";}
					CMS::add_error("Failed to add IPC Service.");
					CMS::add_error("$res");
                        	}
			} else {
				#can be changed in the future in case new Customer Based Services are introduced
				if ($debug){print IPC1 "not ipc\n";}
				CMS::add_error("Failed to add IPC Service.");
				CMS::add_error("Service ID should be of the format pausripc12345 or phgipc9123974");
			}
                CMS::output_html("cust_service_add");
		}

	} elsif($CMS::html_values{command}{value} eq "edit_cust_serv") {
		if (not defined ($CMS::html_values{stage}{value})) {
			#Edit Service first screen
			CMS::output_html("cust_service_edit");
		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Edit")) {
			my $edit_vals = CMS_CUST_SERVICE_DB::get_service_details($CMS::html_values{service}{value});
			if ($edit_vals =~ /exist/){
				# Invalid Service ID entered
				CMS::add_error("Failed to Edit IPC Service.");
				CMS::add_error("IPC Service does not exist.");
				CMS::output_html("cust_service_edit");
			} else {
				#fetch all existing parameters
				$CMS::html_values{description}{value}=@$edit_vals[2];
				$CMS::html_values{committed_rate}{value}=@$edit_vals[3];
				$CMS::html_values{dir_traffic}{value}=@$edit_vals[4];
				$CMS::html_values{as_no}{value}=@$edit_vals[5];
				$CMS::html_values{eff_dir_date}{value}=@$edit_vals[6];
				#my $mo_names = &get_mo_names_for_as(@$edit_vals[5]);
				## IPT Addendum 2##
				my $mo_names = &get_mo_names_for_as(@$edit_vals[5], @$edit_vals[1]);
				if ($debug){print IPC1 "PM EDIT mo_names:$mo_names\n";}
				my @mo_names = split(/\n/,$mo_names);
				my $mo_names_new = join '<br>',@mo_names;
				#$CMS::html_values{mo_names}{value}=&get_mo_names_for_as(@$edit_vals[5]);
				$CMS::html_values{mo_names}{value}=$mo_names_new;
				CMS::output_html("cust_service_edit2");
			}

		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Update")) {
			# Edit Second Screen
			if ($debug){print IPC1 "PM UPDATE:\nservice:$CMS::html_values{service}{value}\taccno:$CMS::html_values{accno}{value}\tdescription:$CMS::html_values{description}{value}\tcommitted_rate:$CMS::html_values{committed_rate}{value}\tdir_traffic:$CMS::html_values{dir_traffic}{value}\tas_no:$CMS::html_values{as_no}{value}\texisting.eff_dir_date:$CMS::html_values{'existing.eff_dir_date'}{value}\n";}
			my $edit_res = CMS_CUST_SERVICE_DB::update_service(
			$CMS::html_values{service}{value},
			$CMS::html_values{accno}{value},
			$CMS::html_values{description}{value},
			$CMS::html_values{committed_rate}{value},
			$CMS::html_values{dir_traffic}{value},
			$CMS::html_values{as_no}{value},
			$CMS::html_values{'existing.eff_dir_date'}{value}
			);
			if ($edit_res == 1) {
				CMS::add_error("IPC Service Updated successfully.");
			} else {
				CMS::add_error("Failed to Update IPC Service.");
				CMS::add_error("$edit_res");
			}
			CMS::output_html("cust_service_edit2");
		}

	} elsif($CMS::html_values{command}{value} eq "delete_cust_serv") {
		#Delete Service
		if (not defined ($CMS::html_values{stage}{value})) {
			CMS::output_html("cust_service_delete");
		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Delete")) {
			if ($debug){print IPC1 "PM DELETE hidden_param:$CMS::html_values{hidden_param}{value}\n";}
			#On delete 
			my $del_result = 1; 
			if ($debug){print IPC1 "PM before calling DB FUNCTION\n";}
			$del_result = CMS_CUST_SERVICE_DB::delete_cust_service(
			$CMS::html_values{service}{value},
                        $CMS::html_values{accno}{value},
			$CMS::html_values{hidden_param}{value},
			);
			if ($debug){print IPC1 "PM after returning from DB FUNCTION\n";}
			if ($debug){print IPC1 "PM DELETE del_result:$del_result\n";}
			if ($del_result == 1) {
				CMS::add_error("IPC Service deleted successfully.");
			} else {
				CMS::add_error("Failed to Delete IPC Service.");
				CMS::add_error("$del_result");
			}
			CMS::output_html("cust_service_delete");
		}

	} elsif($CMS::html_values{command}{value} eq "search_cust_serv") {
		if ($debug){print IPC1 "PM search entered\n";}
		if (not defined ($CMS::html_values{stage}{value})) {
			if ($debug){print IPC1 "PM search stage not defined:$CMS::html_values{stage}{value}\n";}
			#Search Service functionality
			$CMS::html_values{search_productcode}{options} = CMS_CUST_SERVICE_DB::get_productcode();
			unshift (@{$CMS::html_values{search_productcode}{options}},[""]);
			CMS::output_html("cust_service_search");
		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Search")) {
			if ($debug){print IPC1 "PM search submit value:$CMS::html_values{submit}{value}\n";}
			my $result = CMS_CUST_SERVICE_DB::search_service
			($CMS::html_values{search_productcode}{value},
			$CMS::html_values{search_serviceid}{value},
			$CMS::html_values{search_accno}{value},
			$CMS::html_values{search_description}{value},
			$CMS::html_values{search_committed}{value},
			$CMS::html_values{search_as}{value},
			$CMS::html_values{search_dir}{value}
			);
			$CMS::html_values{cust_services}{table} = $result;
			if ($debug){print IPC1 "$$result[0]\t$$result[1]\n";}
			unshift (@{$CMS::html_values{cust_services}{table}},["IPC Service ID","Master Service ID","Service ID","Customer","Description","Product Type","Committed In/Out","AS No.","Direct/Indirect"]);	
			$CMS::html_values{cust_services}{header_rows} = 1;
			$CMS::html_values{search_productcode}{options} = CMS_CUST_SERVICE_DB::get_productcode();
			unshift (@{$CMS::html_values{search_productcode}{options}},[""]);
			CMS::output_html("cust_service_search");
		}
	} elsif ($CMS::html_values{command}{value} eq "fetchMO") {
		#fetch MO names, given an AS Number
		my $as_num = $CMS::html_values{as_num}{value};
		my $customer = $CMS::html_values{customer}{value};
		my $mo_names = &get_mo_names_for_as($as_num, $customer);
		if ($debug){print IPC1 "PM elsif as_num:$as_num\tmo_names:$mo_names\n";}
		print "Content-type: html/text\n\n";
		print $mo_names;
	} elsif ($CMS::html_values{command}{value} eq "fetchall") {
		#fetch services for auto complete feature
		my $retval = CMS_CUST_SERVICE_DB::get_service();
		print "Content-type: html/text\n\n";
                print $retval;
	} elsif ($CMS::html_values{command}{value} eq "fetchcust") {
		#fetch customer, given Service ID
		my $serv = $CMS::html_values{servid}{value};
		my $accno = CMS_CUST_SERVICE_DB::get_accno_for_service($serv);
		print "Content-type: html/text\n\n";
		print $accno;
	} elsif ($CMS::html_values{command}{value} eq "get_del_details") {
		my $serv = $CMS::html_values{servid}{value};
		my $retStr = CMS_CUST_SERVICE_DB::get_del_details($serv);
		if ($debug){print IPC1 "PM AFTER DB function\n";}
		if ($debug){print IPC1 "PM get_del_details retStr:$retStr\n";}
		print "Content-type: html/text\n\n";
		print $retStr;
	}
	
}

################################################################
# Name		: get_mo_names_for_as
# Description	: generates the MO names based on the AS Number
# Author	: Karuna Ballal
# Input 	: AS Number
# Outupt	: string of generated MO Names
################################################################
sub get_mo_names_for_as {
	my $debug = 1;
	#to disable debug statements, uncomment following line
	#$debug = 0;
	if ($debug){open (IPC1, ">>/data1/tmp_log/addcustserv.log");}
        if ($debug){print IPC1 "entered get_mo sub\n";}
	my $as_num = shift @_;
	##IPT addendum 2##
	my $customer = shift @_;
	if ($debug){print IPC1 "PM get sub as_num:$as_num\tcustomer:$customer\n";}
	#my $mo_str1 = "CU_AS".$as_num."_FromCT";
	#my $mo_str2 = "CU_AS".$as_num."_ToCT";
	#my $mo_str3 = "CU_AS".$as_num."_FromCU";
	#my $mo_str4 = "CU_AS".$as_num."_ToCU";
	my $mo_name = "CU_AS".$as_num."_".$customer; 
	if ($as_num =~ /^\d+$/){
	#$mo_name = join "\n", $mo_str1, $mo_str2, $mo_str3, $mo_str4;
	$mo_name = $mo_name; 
	} elsif ($as_num eq ""){
	if ($debug){print IPC1 "as num null\n";}
	$mo_name = "";
	} else {
	$mo_name = "Invalid AS Number format."; 
	}
	return $mo_name;
}

	

1;
