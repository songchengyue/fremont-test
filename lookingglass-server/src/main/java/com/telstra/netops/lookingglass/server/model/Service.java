package com.telstra.netops.lookingglass.server.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Service")
public class Service implements Serializable {
	
	@Id
	@GeneratedValue
	@Column(name = "Service_ID")
	private Long id;
	
	@Column(name = "Telstra_Service_ID")
	private String telstraServiceId;
	
	@Column(name = "Customer_ID")
	private String customerId;
	
	@ManyToOne(fetch=FetchType.EAGER) 	
	// TODO either change the name of the field or use the same product_line in ALL tables!
	@JoinColumn(name="Product_Line", referencedColumnName = "Service_Product_Line")
	private ProductLine productLine;
	
	@Column(name = "Router")
	private String router;
	
	@Column(name = "Interface")
	private String routerInterface;
	
	@Column(name = "Parent_Site_ID")
	private String parentSiteId;
	
	@Column(name = "As_Number")
	private String asNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTelstraServiceId() {
		return telstraServiceId;
	}

	public void setTelstraServiceId(String telstraServiceId) {
		this.telstraServiceId = telstraServiceId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public ProductLine getProductLine() {
		return productLine;
	}

	public void setProductLine(ProductLine productLine) {
		this.productLine = productLine;
	}

	public String getRouter() {
		return router;
	}

	public void setRouter(String router) {
		this.router = router;
	}

	public String getRouterInterface() {
		return routerInterface;
	}

	public void setRouterInterface(String routerInterface) {
		this.routerInterface = routerInterface;
	}

	public String getParentSiteId() {
		return parentSiteId;
	}

	public void setParentSiteId(String parentSiteId) {
		this.parentSiteId = parentSiteId;
	}

	public String getAsNumber() {
		return asNumber;
	}

	public void setAsNumber(String asNumber) {
		this.asNumber = asNumber;
	}
	

}
