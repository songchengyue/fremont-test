/*
 * XML Type:  getCINeighbours
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetCINeighbours
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML getCINeighbours(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class GetCINeighboursImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetCINeighbours
{
    
    public GetCINeighboursImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "cmdbContext");
    private static final javax.xml.namespace.QName ID$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "ID");
    private static final javax.xml.namespace.QName NEIGHBOURTYPE$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "neighbourType");
    private static final javax.xml.namespace.QName CIPROPERTIES$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "CIProperties");
    private static final javax.xml.namespace.QName RELATIONPROPERTIES$8 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "relationProperties");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets the "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID getID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ID$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ID" element
     */
    public void setID(com.hp.schemas.ucmdb._1.types.ID id)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ID$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ID$2);
            }
            target.set(id);
        }
    }
    
    /**
     * Appends and returns a new empty "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID addNewID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ID$2);
            return target;
        }
    }
    
    /**
     * Gets the "neighbourType" element
     */
    public java.lang.String getNeighbourType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NEIGHBOURTYPE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "neighbourType" element
     */
    public org.apache.xmlbeans.XmlString xgetNeighbourType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NEIGHBOURTYPE$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "neighbourType" element
     */
    public void setNeighbourType(java.lang.String neighbourType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NEIGHBOURTYPE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NEIGHBOURTYPE$4);
            }
            target.setStringValue(neighbourType);
        }
    }
    
    /**
     * Sets (as xml) the "neighbourType" element
     */
    public void xsetNeighbourType(org.apache.xmlbeans.XmlString neighbourType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NEIGHBOURTYPE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NEIGHBOURTYPE$4);
            }
            target.set(neighbourType);
        }
    }
    
    /**
     * Gets the "CIProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection getCIProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().find_element_user(CIPROPERTIES$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "CIProperties" element
     */
    public boolean isSetCIProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CIPROPERTIES$6) != 0;
        }
    }
    
    /**
     * Sets the "CIProperties" element
     */
    public void setCIProperties(com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection ciProperties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().find_element_user(CIPROPERTIES$6, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().add_element_user(CIPROPERTIES$6);
            }
            target.set(ciProperties);
        }
    }
    
    /**
     * Appends and returns a new empty "CIProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection addNewCIProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().add_element_user(CIPROPERTIES$6);
            return target;
        }
    }
    
    /**
     * Unsets the "CIProperties" element
     */
    public void unsetCIProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CIPROPERTIES$6, 0);
        }
    }
    
    /**
     * Gets the "relationProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection getRelationProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().find_element_user(RELATIONPROPERTIES$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "relationProperties" element
     */
    public boolean isSetRelationProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RELATIONPROPERTIES$8) != 0;
        }
    }
    
    /**
     * Sets the "relationProperties" element
     */
    public void setRelationProperties(com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection relationProperties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().find_element_user(RELATIONPROPERTIES$8, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().add_element_user(RELATIONPROPERTIES$8);
            }
            target.set(relationProperties);
        }
    }
    
    /**
     * Appends and returns a new empty "relationProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection addNewRelationProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().add_element_user(RELATIONPROPERTIES$8);
            return target;
        }
    }
    
    /**
     * Unsets the "relationProperties" element
     */
    public void unsetRelationProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RELATIONPROPERTIES$8, 0);
        }
    }
}
