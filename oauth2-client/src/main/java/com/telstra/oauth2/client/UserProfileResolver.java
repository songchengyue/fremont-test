package com.telstra.oauth2.client;

import org.springframework.stereotype.Component;

@Component
public class UserProfileResolver extends AuthorityResolver {
	
	// must match same value in SAMLTokenGranter class
	private static final String USER_PROFILE_ROLE_NAME_PREFIX = "ROLE_USER_PROFILE_";
	
	public UserProfileResolver() {
		super(USER_PROFILE_ROLE_NAME_PREFIX, "User Profile");
	}

}
