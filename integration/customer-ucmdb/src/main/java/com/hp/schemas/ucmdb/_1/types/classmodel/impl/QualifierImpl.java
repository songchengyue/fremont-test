/*
 * XML Type:  Qualifier
 * Namespace: http://schemas.hp.com/ucmdb/1/types/classmodel
 * Java type: com.hp.schemas.ucmdb._1.types.classmodel.Qualifier
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.classmodel.impl;
/**
 * An XML Qualifier(@http://schemas.hp.com/ucmdb/1/types/classmodel).
 *
 * This is a complex type.
 */
public class QualifierImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.classmodel.Qualifier
{
    
    public QualifierImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "name");
    private static final javax.xml.namespace.QName QUALIFIERPROPS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "qualifierProps");
    
    
    /**
     * Gets the "name" element
     */
    public java.lang.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "name" element
     */
    public org.apache.xmlbeans.XmlString xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "name" element
     */
    public void setName(java.lang.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NAME$0);
            }
            target.setStringValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "name" element
     */
    public void xsetName(org.apache.xmlbeans.XmlString name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NAME$0);
            }
            target.set(name);
        }
    }
    
    /**
     * Gets the "qualifierProps" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.QualifiersProperties getQualifierProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.QualifiersProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.QualifiersProperties)get_store().find_element_user(QUALIFIERPROPS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "qualifierProps" element
     */
    public boolean isSetQualifierProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(QUALIFIERPROPS$2) != 0;
        }
    }
    
    /**
     * Sets the "qualifierProps" element
     */
    public void setQualifierProps(com.hp.schemas.ucmdb._1.types.classmodel.QualifiersProperties qualifierProps)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.QualifiersProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.QualifiersProperties)get_store().find_element_user(QUALIFIERPROPS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.QualifiersProperties)get_store().add_element_user(QUALIFIERPROPS$2);
            }
            target.set(qualifierProps);
        }
    }
    
    /**
     * Appends and returns a new empty "qualifierProps" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.QualifiersProperties addNewQualifierProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.QualifiersProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.QualifiersProperties)get_store().add_element_user(QUALIFIERPROPS$2);
            return target;
        }
    }
    
    /**
     * Unsets the "qualifierProps" element
     */
    public void unsetQualifierProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(QUALIFIERPROPS$2, 0);
        }
    }
}
