/*
 * An XML document type.
 * Localname: ChunkRequest
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ChunkRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * A document containing one ChunkRequest(@http://schemas.hp.com/ucmdb/1/types) element.
 *
 * This is a complex type.
 */
public class ChunkRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ChunkRequestDocument
{
    
    public ChunkRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHUNKREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "ChunkRequest");
    
    
    /**
     * Gets the "ChunkRequest" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkRequest getChunkRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkRequest target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkRequest)get_store().find_element_user(CHUNKREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ChunkRequest" element
     */
    public void setChunkRequest(com.hp.schemas.ucmdb._1.types.ChunkRequest chunkRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkRequest target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkRequest)get_store().find_element_user(CHUNKREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ChunkRequest)get_store().add_element_user(CHUNKREQUEST$0);
            }
            target.set(chunkRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "ChunkRequest" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkRequest addNewChunkRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkRequest target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkRequest)get_store().add_element_user(CHUNKREQUEST$0);
            return target;
        }
    }
}
