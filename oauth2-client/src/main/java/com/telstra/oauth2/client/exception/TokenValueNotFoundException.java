package com.telstra.oauth2.client.exception;

public class TokenValueNotFoundException extends RuntimeException {
	
	public TokenValueNotFoundException(String message) {
		super(message);
	}

}
