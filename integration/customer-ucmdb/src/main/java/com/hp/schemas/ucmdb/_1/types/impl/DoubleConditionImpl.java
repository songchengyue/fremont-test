/*
 * XML Type:  DoubleCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.DoubleCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML DoubleCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class DoubleConditionImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.DoubleCondition
{
    
    public DoubleConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "condition");
    private static final javax.xml.namespace.QName DOUBLEOPERATOR$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "doubleOperator");
    
    
    /**
     * Gets the "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleProp getCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "condition" element
     */
    public void setCondition(com.hp.schemas.ucmdb._1.types.DoubleProp condition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().add_element_user(CONDITION$0);
            }
            target.set(condition);
        }
    }
    
    /**
     * Appends and returns a new empty "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleProp addNewCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().add_element_user(CONDITION$0);
            return target;
        }
    }
    
    /**
     * Gets the "doubleOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator.Enum getDoubleOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOUBLEOPERATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "doubleOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator xgetDoubleOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator)get_store().find_element_user(DOUBLEOPERATOR$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "doubleOperator" element
     */
    public void setDoubleOperator(com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator.Enum doubleOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOUBLEOPERATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DOUBLEOPERATOR$2);
            }
            target.setEnumValue(doubleOperator);
        }
    }
    
    /**
     * Sets (as xml) the "doubleOperator" element
     */
    public void xsetDoubleOperator(com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator doubleOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator)get_store().find_element_user(DOUBLEOPERATOR$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator)get_store().add_element_user(DOUBLEOPERATOR$2);
            }
            target.set(doubleOperator);
        }
    }
    /**
     * An XML doubleOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.DoubleCondition$DoubleOperator.
     */
    public static class DoubleOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.DoubleCondition.DoubleOperator
    {
        
        public DoubleOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected DoubleOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
