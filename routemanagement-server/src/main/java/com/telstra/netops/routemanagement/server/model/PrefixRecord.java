package com.telstra.netops.routemanagement.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Modal class for View prefix record
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrefixRecord {

	@JsonProperty("prefix")
	private String prefix;

	@JsonProperty("action")
	private String action;

	@JsonProperty("timeEntered")
	private String timeEntered;

	@JsonProperty("timeCompleted")
	private String timeCompleted;

	@JsonProperty("status")
	private String status;

	@JsonProperty("AS")
	private String as;

	@JsonProperty("customerId")
	private String customerId;

	@JsonProperty("trackno")
	private String trackno;

	@JsonProperty("header")
	private String header;

	@JsonProperty("Error")
	private String error;

	public PrefixRecord() {
	}

	public PrefixRecord(String prefix, String action, String timeEntered, String timeCompleted, String status,
			String as, String customerId, String trackno, String header, String error) {
		super();
		this.prefix = prefix;
		this.action = action;
		this.timeEntered = timeEntered;
		this.timeCompleted = timeCompleted;
		this.status = status;
		as = as;
		this.customerId = customerId;
		this.trackno = trackno;
		this.header = header;
		this.error = error;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getTimeEntered() {
		return timeEntered;
	}

	public void setTimeEntered(String timeEntered) {
		this.timeEntered = timeEntered;
	}

	public String getTimeCompleted() {
		return timeCompleted;
	}

	public void setTimeCompleted(String timeCompleted) {
		this.timeCompleted = timeCompleted;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAS() {
		return as;
	}

	public void setAS(String as) {
		this.as = as;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getTrackno() {
		return trackno;
	}

	public void setTrackno(String trackno) {
		this.trackno = trackno;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean notARecord() {
		return error != null;
	}

}
