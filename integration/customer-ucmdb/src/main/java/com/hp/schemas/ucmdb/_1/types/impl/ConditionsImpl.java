/*
 * XML Type:  Conditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.Conditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML Conditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ConditionsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.Conditions
{
    
    public ConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName BOOLEANCONDITIONS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "booleanConditions");
    private static final javax.xml.namespace.QName DATECONDITIONS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "dateConditions");
    private static final javax.xml.namespace.QName DOUBLECONDITIONS$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "doubleConditions");
    private static final javax.xml.namespace.QName FLOATCONDITIONS$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "floatConditions");
    private static final javax.xml.namespace.QName INTCONDITIONS$8 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intConditions");
    private static final javax.xml.namespace.QName INTLISTCONDITIONS$10 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intListConditions");
    private static final javax.xml.namespace.QName STRCONDITIONS$12 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strConditions");
    private static final javax.xml.namespace.QName STRLISTCONDITIONS$14 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strListConditions");
    private static final javax.xml.namespace.QName XMLCONDITIONS$16 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "xmlConditions");
    private static final javax.xml.namespace.QName LONGCONDITIONS$18 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "longConditions");
    
    
    /**
     * Gets the "booleanConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanConditions getBooleanConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanConditions)get_store().find_element_user(BOOLEANCONDITIONS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "booleanConditions" element
     */
    public boolean isSetBooleanConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BOOLEANCONDITIONS$0) != 0;
        }
    }
    
    /**
     * Sets the "booleanConditions" element
     */
    public void setBooleanConditions(com.hp.schemas.ucmdb._1.types.BooleanConditions booleanConditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanConditions)get_store().find_element_user(BOOLEANCONDITIONS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.BooleanConditions)get_store().add_element_user(BOOLEANCONDITIONS$0);
            }
            target.set(booleanConditions);
        }
    }
    
    /**
     * Appends and returns a new empty "booleanConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanConditions addNewBooleanConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanConditions)get_store().add_element_user(BOOLEANCONDITIONS$0);
            return target;
        }
    }
    
    /**
     * Unsets the "booleanConditions" element
     */
    public void unsetBooleanConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BOOLEANCONDITIONS$0, 0);
        }
    }
    
    /**
     * Gets the "dateConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.DateConditions getDateConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateConditions)get_store().find_element_user(DATECONDITIONS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "dateConditions" element
     */
    public boolean isSetDateConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DATECONDITIONS$2) != 0;
        }
    }
    
    /**
     * Sets the "dateConditions" element
     */
    public void setDateConditions(com.hp.schemas.ucmdb._1.types.DateConditions dateConditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateConditions)get_store().find_element_user(DATECONDITIONS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.DateConditions)get_store().add_element_user(DATECONDITIONS$2);
            }
            target.set(dateConditions);
        }
    }
    
    /**
     * Appends and returns a new empty "dateConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.DateConditions addNewDateConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DateConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.DateConditions)get_store().add_element_user(DATECONDITIONS$2);
            return target;
        }
    }
    
    /**
     * Unsets the "dateConditions" element
     */
    public void unsetDateConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DATECONDITIONS$2, 0);
        }
    }
    
    /**
     * Gets the "doubleConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleConditions getDoubleConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleConditions)get_store().find_element_user(DOUBLECONDITIONS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "doubleConditions" element
     */
    public boolean isSetDoubleConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DOUBLECONDITIONS$4) != 0;
        }
    }
    
    /**
     * Sets the "doubleConditions" element
     */
    public void setDoubleConditions(com.hp.schemas.ucmdb._1.types.DoubleConditions doubleConditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleConditions)get_store().find_element_user(DOUBLECONDITIONS$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.DoubleConditions)get_store().add_element_user(DOUBLECONDITIONS$4);
            }
            target.set(doubleConditions);
        }
    }
    
    /**
     * Appends and returns a new empty "doubleConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleConditions addNewDoubleConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleConditions)get_store().add_element_user(DOUBLECONDITIONS$4);
            return target;
        }
    }
    
    /**
     * Unsets the "doubleConditions" element
     */
    public void unsetDoubleConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DOUBLECONDITIONS$4, 0);
        }
    }
    
    /**
     * Gets the "floatConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatConditions getFloatConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatConditions)get_store().find_element_user(FLOATCONDITIONS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "floatConditions" element
     */
    public boolean isSetFloatConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FLOATCONDITIONS$6) != 0;
        }
    }
    
    /**
     * Sets the "floatConditions" element
     */
    public void setFloatConditions(com.hp.schemas.ucmdb._1.types.FloatConditions floatConditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatConditions)get_store().find_element_user(FLOATCONDITIONS$6, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.FloatConditions)get_store().add_element_user(FLOATCONDITIONS$6);
            }
            target.set(floatConditions);
        }
    }
    
    /**
     * Appends and returns a new empty "floatConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatConditions addNewFloatConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatConditions)get_store().add_element_user(FLOATCONDITIONS$6);
            return target;
        }
    }
    
    /**
     * Unsets the "floatConditions" element
     */
    public void unsetFloatConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FLOATCONDITIONS$6, 0);
        }
    }
    
    /**
     * Gets the "intConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.IntConditions getIntConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntConditions)get_store().find_element_user(INTCONDITIONS$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "intConditions" element
     */
    public boolean isSetIntConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTCONDITIONS$8) != 0;
        }
    }
    
    /**
     * Sets the "intConditions" element
     */
    public void setIntConditions(com.hp.schemas.ucmdb._1.types.IntConditions intConditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntConditions)get_store().find_element_user(INTCONDITIONS$8, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IntConditions)get_store().add_element_user(INTCONDITIONS$8);
            }
            target.set(intConditions);
        }
    }
    
    /**
     * Appends and returns a new empty "intConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.IntConditions addNewIntConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntConditions)get_store().add_element_user(INTCONDITIONS$8);
            return target;
        }
    }
    
    /**
     * Unsets the "intConditions" element
     */
    public void unsetIntConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTCONDITIONS$8, 0);
        }
    }
    
    /**
     * Gets the "intListConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListConditions getIntListConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListConditions)get_store().find_element_user(INTLISTCONDITIONS$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "intListConditions" element
     */
    public boolean isSetIntListConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTLISTCONDITIONS$10) != 0;
        }
    }
    
    /**
     * Sets the "intListConditions" element
     */
    public void setIntListConditions(com.hp.schemas.ucmdb._1.types.IntListConditions intListConditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListConditions)get_store().find_element_user(INTLISTCONDITIONS$10, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IntListConditions)get_store().add_element_user(INTLISTCONDITIONS$10);
            }
            target.set(intListConditions);
        }
    }
    
    /**
     * Appends and returns a new empty "intListConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListConditions addNewIntListConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListConditions)get_store().add_element_user(INTLISTCONDITIONS$10);
            return target;
        }
    }
    
    /**
     * Unsets the "intListConditions" element
     */
    public void unsetIntListConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTLISTCONDITIONS$10, 0);
        }
    }
    
    /**
     * Gets the "strConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.StrConditions getStrConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrConditions)get_store().find_element_user(STRCONDITIONS$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "strConditions" element
     */
    public boolean isSetStrConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STRCONDITIONS$12) != 0;
        }
    }
    
    /**
     * Sets the "strConditions" element
     */
    public void setStrConditions(com.hp.schemas.ucmdb._1.types.StrConditions strConditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrConditions)get_store().find_element_user(STRCONDITIONS$12, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrConditions)get_store().add_element_user(STRCONDITIONS$12);
            }
            target.set(strConditions);
        }
    }
    
    /**
     * Appends and returns a new empty "strConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.StrConditions addNewStrConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrConditions)get_store().add_element_user(STRCONDITIONS$12);
            return target;
        }
    }
    
    /**
     * Unsets the "strConditions" element
     */
    public void unsetStrConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STRCONDITIONS$12, 0);
        }
    }
    
    /**
     * Gets the "strListConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListConditions getStrListConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListConditions)get_store().find_element_user(STRLISTCONDITIONS$14, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "strListConditions" element
     */
    public boolean isSetStrListConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STRLISTCONDITIONS$14) != 0;
        }
    }
    
    /**
     * Sets the "strListConditions" element
     */
    public void setStrListConditions(com.hp.schemas.ucmdb._1.types.StrListConditions strListConditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListConditions)get_store().find_element_user(STRLISTCONDITIONS$14, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrListConditions)get_store().add_element_user(STRLISTCONDITIONS$14);
            }
            target.set(strListConditions);
        }
    }
    
    /**
     * Appends and returns a new empty "strListConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListConditions addNewStrListConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListConditions)get_store().add_element_user(STRLISTCONDITIONS$14);
            return target;
        }
    }
    
    /**
     * Unsets the "strListConditions" element
     */
    public void unsetStrListConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STRLISTCONDITIONS$14, 0);
        }
    }
    
    /**
     * Gets the "xmlConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlConditions getXmlConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlConditions)get_store().find_element_user(XMLCONDITIONS$16, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "xmlConditions" element
     */
    public boolean isSetXmlConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(XMLCONDITIONS$16) != 0;
        }
    }
    
    /**
     * Sets the "xmlConditions" element
     */
    public void setXmlConditions(com.hp.schemas.ucmdb._1.types.XmlConditions xmlConditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlConditions)get_store().find_element_user(XMLCONDITIONS$16, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.XmlConditions)get_store().add_element_user(XMLCONDITIONS$16);
            }
            target.set(xmlConditions);
        }
    }
    
    /**
     * Appends and returns a new empty "xmlConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlConditions addNewXmlConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlConditions)get_store().add_element_user(XMLCONDITIONS$16);
            return target;
        }
    }
    
    /**
     * Unsets the "xmlConditions" element
     */
    public void unsetXmlConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(XMLCONDITIONS$16, 0);
        }
    }
    
    /**
     * Gets the "longConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.LongConditions getLongConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongConditions)get_store().find_element_user(LONGCONDITIONS$18, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "longConditions" element
     */
    public boolean isSetLongConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(LONGCONDITIONS$18) != 0;
        }
    }
    
    /**
     * Sets the "longConditions" element
     */
    public void setLongConditions(com.hp.schemas.ucmdb._1.types.LongConditions longConditions)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongConditions)get_store().find_element_user(LONGCONDITIONS$18, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.LongConditions)get_store().add_element_user(LONGCONDITIONS$18);
            }
            target.set(longConditions);
        }
    }
    
    /**
     * Appends and returns a new empty "longConditions" element
     */
    public com.hp.schemas.ucmdb._1.types.LongConditions addNewLongConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongConditions target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongConditions)get_store().add_element_user(LONGCONDITIONS$18);
            return target;
        }
    }
    
    /**
     * Unsets the "longConditions" element
     */
    public void unsetLongConditions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(LONGCONDITIONS$18, 0);
        }
    }
}
