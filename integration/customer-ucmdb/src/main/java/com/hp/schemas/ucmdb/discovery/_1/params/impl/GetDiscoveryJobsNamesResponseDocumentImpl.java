/*
 * An XML document type.
 * Localname: getDiscoveryJobsNamesResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getDiscoveryJobsNamesResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetDiscoveryJobsNamesResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponseDocument
{
    
    public GetDiscoveryJobsNamesResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETDISCOVERYJOBSNAMESRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDiscoveryJobsNamesResponse");
    
    
    /**
     * Gets the "getDiscoveryJobsNamesResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse getGetDiscoveryJobsNamesResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse)get_store().find_element_user(GETDISCOVERYJOBSNAMESRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getDiscoveryJobsNamesResponse" element
     */
    public void setGetDiscoveryJobsNamesResponse(com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse getDiscoveryJobsNamesResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse)get_store().find_element_user(GETDISCOVERYJOBSNAMESRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse)get_store().add_element_user(GETDISCOVERYJOBSNAMESRESPONSE$0);
            }
            target.set(getDiscoveryJobsNamesResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getDiscoveryJobsNamesResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse addNewGetDiscoveryJobsNamesResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse)get_store().add_element_user(GETDISCOVERYJOBSNAMESRESPONSE$0);
            return target;
        }
    }
}
