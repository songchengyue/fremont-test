# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
#
# $Id: CMS_OLSS_LOGIN.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $

package CMS_OLSS_LOGIN;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_CUSTOMER_DB;

our @ISA = qw (Exporter);

our $error;

sub start {

  if (defined $CMS::html_values{accno}{value}) {
    # pass control to OLSS
  
#    my $hostname = `/bin/hostname`;
#    chomp($hostname);
    
#    if ($hostname =~ /-webserver.net.reach.com/) {
#      $hostname = "www.net.reach.com";
#    } else {
#      $hostname =~ "reachdev.net.reach.com";
#    }

    my $hostname = $OS_PARA::values{inthostname}{value};

    if ($ENV{'SERVER_NAME'} =~ /^(la-|hk-)/i) {
        $hostname = $1.$hostname;
    }

    $CMS::html_values{accno}{value} =~ s/\&/\%26/g;

    print "Location: http://$hostname/cgi-bin/tgwciss-login.cgi?staffid=$CMS::html_values{username}{value}&authcode=$CMS::html_values{authentication_code}{value}&accno=$CMS::html_values{accno}{value}&level=5\n\n";

  } else {
    CMS::output_html("olss_login");
  }
}
1;
