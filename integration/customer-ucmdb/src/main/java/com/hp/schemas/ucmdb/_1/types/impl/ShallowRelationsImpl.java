/*
 * XML Type:  ShallowRelations
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ShallowRelations
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ShallowRelations(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ShallowRelationsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ShallowRelations
{
    
    public ShallowRelationsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SHALLOWRELATION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "shallowRelation");
    
    
    /**
     * Gets array of all "shallowRelation" elements
     */
    public com.hp.schemas.ucmdb._1.types.ShallowRelation[] getShallowRelationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SHALLOWRELATION$0, targetList);
            com.hp.schemas.ucmdb._1.types.ShallowRelation[] result = new com.hp.schemas.ucmdb._1.types.ShallowRelation[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "shallowRelation" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowRelation getShallowRelationArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowRelation)get_store().find_element_user(SHALLOWRELATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "shallowRelation" element
     */
    public int sizeOfShallowRelationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SHALLOWRELATION$0);
        }
    }
    
    /**
     * Sets array of all "shallowRelation" element
     */
    public void setShallowRelationArray(com.hp.schemas.ucmdb._1.types.ShallowRelation[] shallowRelationArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(shallowRelationArray, SHALLOWRELATION$0);
        }
    }
    
    /**
     * Sets ith "shallowRelation" element
     */
    public void setShallowRelationArray(int i, com.hp.schemas.ucmdb._1.types.ShallowRelation shallowRelation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowRelation)get_store().find_element_user(SHALLOWRELATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(shallowRelation);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "shallowRelation" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowRelation insertNewShallowRelation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowRelation)get_store().insert_element_user(SHALLOWRELATION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "shallowRelation" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowRelation addNewShallowRelation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowRelation)get_store().add_element_user(SHALLOWRELATION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "shallowRelation" element
     */
    public void removeShallowRelation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SHALLOWRELATION$0, i);
        }
    }
}
