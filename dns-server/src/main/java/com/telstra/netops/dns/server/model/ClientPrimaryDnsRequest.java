package com.telstra.netops.dns.server.model;



public class ClientPrimaryDnsRequest {
	

	private String contactEmailAddress;
	
	private String domain;

	private String host;

	private DnsRecordType recordType;

	private String aRecordIpV4Address; // A record only
	
	private String aRecordIpV6Address; //AAAA

	private String cnameRecordName; // CNAME only

	private String mxRecordPrefix; // MX record only

	private String primarymxpv;   //PDNS_MXPV

	private String nsRecordServer; // NS record only

	private String textRecordText; // TXT record only

	private String customerId;
	
	private String[] recordsToBeDeleted;//array of db_row_Id
	
	private String serviceId ;
		

	public static final String PRIMARY_REM_SUBMIT	="PRIMARY_REM_SUBMIT" ;
	
	public static final String PRIMARY_ADD_SUBMIT 	= "PRIMARY_ADD_SUBMIT";
	
	public static final String PRIMARY_VIEW_SUBMIT 	= "PRIMARY_VIEW_SUBMIT";

	

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public DnsRecordType getRecordType() {
		return recordType;
	}

	public void setRecordType(DnsRecordType recordType) {
		this.recordType = recordType;
	}

	public String getaRecordIpV4Address() {
		return aRecordIpV4Address;
	}

	public void setaRecordIpV4Address(String aRecordIpV4Address) {
		this.aRecordIpV4Address = aRecordIpV4Address;
	}

	public String getCnameRecordName() {
		return cnameRecordName;
	}

	public void setCnameRecordName(String cnameRecordName) {
		this.cnameRecordName = cnameRecordName;
	}
	
	public String getMxRecordPrefix() {
		return mxRecordPrefix;
	}

	public void setMxRecordPrefix(String mxRecordPrefix) {
		this.mxRecordPrefix = mxRecordPrefix;
	}

	public String getNsRecordServer() {
		return nsRecordServer;
	}

	public void setNsRecordServer(String nsRecordServer) {
		this.nsRecordServer = nsRecordServer;
	}

	public String getTextRecordText() {
		return textRecordText;
	}

	public void setTextRecordText(String textRecordText) {
		this.textRecordText = textRecordText;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getPrimarymxpv() {
		return primarymxpv;
	}

	public void setPrimarymxpv(String primarymxpv) {
		this.primarymxpv = primarymxpv;
	}

	public String[] getRecordsToBeDeleted() {
		return recordsToBeDeleted;
	}

	public void setRecordsToBeDeleted(String[] recordsToBeDeleted) {
		this.recordsToBeDeleted = recordsToBeDeleted;
	}

	public String getaRecordIpV6Address() {
		return aRecordIpV6Address;
	}

	public void setaRecordIpV6Address(String aRecordIpV6Address) {
		this.aRecordIpV6Address = aRecordIpV6Address;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	


	

}
