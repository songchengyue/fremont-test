/*
 * An XML document type.
 * Localname: updateCredentialsEntryRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one updateCredentialsEntryRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class UpdateCredentialsEntryRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequestDocument
{
    
    public UpdateCredentialsEntryRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName UPDATECREDENTIALSENTRYREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "updateCredentialsEntryRequest");
    
    
    /**
     * Gets the "updateCredentialsEntryRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest getUpdateCredentialsEntryRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest)get_store().find_element_user(UPDATECREDENTIALSENTRYREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "updateCredentialsEntryRequest" element
     */
    public void setUpdateCredentialsEntryRequest(com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest updateCredentialsEntryRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest)get_store().find_element_user(UPDATECREDENTIALSENTRYREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest)get_store().add_element_user(UPDATECREDENTIALSENTRYREQUEST$0);
            }
            target.set(updateCredentialsEntryRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "updateCredentialsEntryRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest addNewUpdateCredentialsEntryRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.UpdateCredentialsEntryRequest)get_store().add_element_user(UPDATECREDENTIALSENTRYREQUEST$0);
            return target;
        }
    }
}
