package com.telstra.netops.dns.server.model.cgi;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telstra.netops.dns.server.model.ClientDnsRemoveRequest;
import com.telstra.netops.dns.server.model.ClientDnsResolverRequest;
import com.telstra.netops.dns.server.model.ClientPrimaryDnsDeleteRequest;
import com.telstra.netops.dns.server.model.ClientPrimaryDnsRequest;
import com.telstra.netops.dns.server.model.ClientReverseDnsRequest;
import com.telstra.netops.dns.server.model.ClientSecondaryDnsRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CgiDnsRequest {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CgiDnsRequest.class);

	private static final String SUBMIT = "DNS_SUBMIT";

	private static final String CANCEL = "DNS_CANCEL";

	private static final String VIEW = "VIEWDNS_SUBMIT";

	private static final String PRIMARY_REM_SUBMIT = "PRIMARY_REM_SUBMIT";
	
	private static final String VERSION_CHECK = "A";

	@JsonProperty
	private Integer level;

	@JsonProperty("DNS_CONTACT")
	private String contactEmailAddress;

	// secondary

	@JsonProperty("SDNS_DOM")
	private String secondaryDnsDomain;

	@JsonProperty("SDNS_CONTACT")
	private String contactSEmailAddress;
	// SDNS_IP'}, $formels{'SDNS_DBROWID'}, $mrwaccno,$formels{'btnChecksec'})

	@JsonProperty("SDNS_IP")
	private String secondaryIpAddress;

	@JsonProperty("SDNS_DBROWID")
	private String secondaryRowID;

	@JsonProperty("btnChecksec")
	private Integer btnChecksec;

	// end secondary

	@JsonProperty("DNS_IP")
	private String ipAddress;

	@JsonProperty("ip_version")
	private Integer ipVersion;

	@JsonProperty("DNS_SUBMIT")
	private String submit;

	@JsonProperty("DNS_CANCEL")
	private String cancel;

	@JsonProperty("VIEWDNS_SUBMIT")
	private String viewDnsSubmit;

	@JsonProperty
	private String dom;

	@JsonProperty("btnCheck")
	private Integer actionCheck;

	@JsonProperty
	private String customerId;

	@JsonProperty
	private String code;

	@JsonProperty("DNS_HOST")
	private String host;

	@JsonProperty("RevM_CONTACT")
	private String reverseMEmailAddress;

	@JsonProperty("RevD_CONTACT")
	private String reverseDEmailAddress;

	@JsonProperty("RevD_IP")
	private String reverseMIP;

	@JsonProperty("RevD_Dom")
	private String reverseMDomain;

	@JsonProperty("RevD_IPB")
	private String reverseDIPBlock;

	@JsonProperty("RevD_NS")
	private String reverseDDomainNS;

	@JsonProperty("REV_TYPE")
	private String reverseType;

	@JsonProperty("btnCheck3")
	private String btnCheck3;

	@JsonProperty("btnCheck9")
	private String btnCheck9;

	@JsonProperty("btnCheck1")
	private String btnCheck1;

	@JsonProperty("REV_VIEW")
	private String reverseView;

	//
	// primary
	//
	@JsonProperty("PDNS_CONTACT")
	private String contactEmailAddressprimary;

	@JsonProperty("PDNS_DOM")
	private String primaryDnsDomain;

	@JsonProperty("PDNS_HOST")
	private String primaryDnsHost;

	@JsonProperty("TYPE_ID")
	private String primaryTypeId;

	@JsonProperty("PDNS_AIP4")
	private String primaryIPV4;

	@JsonProperty("PDNS_AIP6")
	private String primaryIPV6;

	@JsonProperty("PDNS_DBROWID1")
	private String[] dbRowId;
	
	@JsonProperty("PDNS_DBROWID")
	private String dbId;

	@JsonProperty("PDNS_CNHOST")
	private String primaryCnHost; // not done
	@JsonProperty("PDNS_MXPV")
	private String primarymxpv;// not done
	@JsonProperty("PDNS_MXEX")
	private String primarymxex;
	@JsonProperty("PDNS_NS")
	private String primaryNs;
	@JsonProperty("PDNS_TTTEXT")
	private String primaryTtText;
	@JsonProperty("PRIMARY_ADD_SUBMIT")
	private String primaryAddSubmit;
	@JsonProperty("PRIMARY_VIEW_SUBMIT")
	private String primaryViewSubmit;
	@JsonProperty("PRIMARY_REM_SUBMIT")
	public String primaryRemoveSubmit;
	
	
	//ServiceId
	@JsonProperty("service_id")
	public String serviceid;

	//
	// Primary DNS
	//
	public static CgiDnsRequest createPrimaryDnsAddRecords(
			ClientPrimaryDnsRequest clientPrimaryDnsRequest) {
		CgiDnsRequest request = new CgiDnsRequest();

		LOGGER.debug("in goig to create request objects createPrimaryDnsAddRecords ");
		request.setCustomerId(clientPrimaryDnsRequest.getCustomerId());
		request.setLevel(DnsRequestType.PRIMARY.getValue());
		request.setContactEmailAddressprimary(clientPrimaryDnsRequest
				.getContactEmailAddress());
		request.setPrimaryDnsDomain(clientPrimaryDnsRequest.getDomain());
		request.setPrimaryDnsHost(clientPrimaryDnsRequest.getHost());
		String recType = clientPrimaryDnsRequest.getRecordType().toString();
		request.setPrimaryTypeId(recType);
		LOGGER.debug("in goig to createPrimaryDnsAddRecords " + recType);
		if(recType.equalsIgnoreCase(VERSION_CHECK)){
			request.setPrimaryIPV4(clientPrimaryDnsRequest.getaRecordIpV4Address());
		}else{
			request.setPrimaryIPV6(clientPrimaryDnsRequest.getaRecordIpV6Address());
		}
		request.setPrimarymxpv(clientPrimaryDnsRequest.getPrimarymxpv());
		request.setPrimarymxex(clientPrimaryDnsRequest.getMxRecordPrefix());
		request.setPrimaryCnHost(clientPrimaryDnsRequest.getCnameRecordName());
		request.setPrimaryNs(clientPrimaryDnsRequest.getNsRecordServer());
		request.setPrimaryTtText(clientPrimaryDnsRequest.getTextRecordText());
		LOGGER.debug("in goig to create request objects clientPrimaryDnsRequest.getServiceid() " + clientPrimaryDnsRequest.getServiceId());
		request.setServiceid(clientPrimaryDnsRequest.getServiceId());
		request.setPrimaryAddSubmit(clientPrimaryDnsRequest.PRIMARY_ADD_SUBMIT);

		LOGGER.debug("in createPrimaryDnsAddRecords ");
		return request;
	}

	public static CgiDnsRequest createPrimaryDnsListRecords(String accountId) {
		CgiDnsRequest request = new CgiDnsRequest();

		// input =
		// "{\"level\":\""+level+"\",\"PRIMARY_VIEW_SUBMIT\":\""+PRIMARY_VIEW_SUBMIT+"\"}";

		LOGGER.debug("in goig to create request objects createPrimaryDnsListRecords ");
		request.setCustomerId(accountId);
		request.setPrimaryViewSubmit(ClientPrimaryDnsRequest.PRIMARY_VIEW_SUBMIT);
		request.setLevel(DnsRequestType.PRIMARY.getValue());

		return request;
	}

	public static CgiDnsRequest primaryDnsRemoveRecords(
			ClientDnsRemoveRequest request2) { // ClientPrimaryDnsDeleteRequest
		CgiDnsRequest request = new CgiDnsRequest();

		// "{\"CUSTOMER_ID\":\""+CUSTOMER_ID+"\",\"level\":\""+level+"\",\"PDNS_DBROWID\":\""+PDNS_DBROWID+"\"
		// ,\"PRIMARY_REM_SUBMIT\":\""+PRIMARY_REM_SUBMIT+"\"}";
		LOGGER.debug("in goig to create request objects primaryDnsRemoveRecords ");
		request.setCustomerId(request2.getCustomerId());
		request.setPrimaryRemoveSubmit(CgiDnsRequest.PRIMARY_REM_SUBMIT);
		request.setLevel(DnsRequestType.PRIMARY.getValue());
		LOGGER.debug("primaryDnsRemoveRecords row id ------->>>>"+request2.getIds());
		request.setDbId(request2.getIds());
		/*
		int lengthOfArrayToDelete = request2.getIds().size();
		LOGGER.debug("in goig to create request objects primaryDnsRemoveRecords lengthOfArrayToDelete "+lengthOfArrayToDelete);
		LOGGER.debug("primaryDnsRemoveRecords------->>>>"+request2.getIds().toArray(new String[lengthOfArrayToDelete]));
		request.setDbRowId(request2.getIds().toArray(
				new String[lengthOfArrayToDelete]));*/
		
		

		return request;
	}

	//
	// Secondary DNS
	//
	public static CgiDnsRequest createSecondaryDnsListRecords(String accountId) {
		CgiDnsRequest request = new CgiDnsRequest();
		LOGGER.debug("in goig to create request objects createSecondaryDnsListRecords ");
		request.setCustomerId(accountId);
		request.setBtnChecksec(ActionCheck.VIEW.getValue());
		request.setLevel(DnsRequestType.SECONDARY.getValue());

		return request;
	}

	public static CgiDnsRequest createSecondaryDnsCancelRecords(
			ClientDnsRemoveRequest clientRequest) {
		CgiDnsRequest request = new CgiDnsRequest();
		LOGGER.debug("in goig to create request objects createSecondaryDnsCancelRecords ");
		request.setCustomerId(clientRequest.getCustomerId());
		request.setBtnChecksec(ActionCheck.CANCEL.getValue());
		LOGGER.debug("in goig to create request objects createSecondaryDnsCancelRecords Domain "+clientRequest.getDomain());
		request.setSecondaryDnsDomain(clientRequest.getDomain());
		LOGGER.debug("in goig to create request objects createSecondaryDnsCancelRecords RowId"+clientRequest.getIds());
		request.setSecondaryRowID(clientRequest.getIds());
		request.setLevel(DnsRequestType.SECONDARY.getValue());

		return request;
	}

	public static CgiDnsRequest createSecondaryDnsAddRecordRequest(
			ClientSecondaryDnsRequest clientRequest) {
		CgiDnsRequest request = new CgiDnsRequest();

		// String input = "{\"DNS_CONTACT\":\"" + DNS_CONTACT
		// + "\",\"DNS_IP\":\"" + DNS_IP + "\",\"DNS_DOMAIN\":\""
		// + DNS_DOM + "\",\"btnchecksec\":\"" + btnchecksec
		// + "\",\"cid\":\"" + cid + "\",\"level\":\"" + level + "\"}";
		LOGGER.debug("in goig to create request objects createSecondaryDnsAddRecordRequest ");
		request.setContactSEmailAddress(clientRequest.getContactEmailAddress());
		request.setSecondaryIpAddress(clientRequest.getPrimaryServerIpAddress());
		LOGGER.debug("in goig to create request objects clientRequest.getDomainName " +clientRequest.getDomain());
		request.setSecondaryDnsDomain(clientRequest.getDomain());
		request.setBtnChecksec(ActionCheck.SUBMIT.getValue());
		request.setCustomerId(clientRequest.getCustomerId());
		request.setServiceid(clientRequest.getServiceId());
		request.setLevel(DnsRequestType.SECONDARY.getValue());
		request.setSecondaryRowID(clientRequest.getRowid());

		return request;
	}

	//
	// DNS Resolver
	//

	public static CgiDnsRequest createResolverDnsAdd(
			ClientDnsResolverRequest clientRequest) {
		LOGGER.debug("creating CGI reverse delegation dns add request");
		CgiDnsRequest request = createResolverDelegationDns(clientRequest);
		request.setSubmit(CgiDnsRequest.SUBMIT);

		return request;
	}

	public static CgiDnsRequest createResolverDnsRemove(
			ClientDnsResolverRequest clientRequest) {
		LOGGER.debug("creating CGI reverse delegation dns add request");

		CgiDnsRequest request = createResolverDelegationDnsRemove(clientRequest);


		return request;
	}

	public static CgiDnsRequest createResolverDelegationDns(
			ClientDnsResolverRequest clientRequest) {
		CgiDnsRequest request = CgiDnsRequest.createResolverDns(clientRequest);
		LOGGER.debug("in goig to create request objects createResolverDnsCreateRequest " + clientRequest.getServiceId());
		request.setServiceid(clientRequest.getServiceId());
		request.setActionCheck(ActionCheck.SUBMIT.getValue());
		request.setSubmit(SUBMIT);

		return request;
	}

	public static CgiDnsRequest createResolverDnsList(String customerId) {
		CgiDnsRequest request = new CgiDnsRequest();
		LOGGER.debug("in goig to create request objects createResolverDnsListRequest ");
		request.setLevel(DnsRequestType.RESOLVER.getValue());
		request.setActionCheck(ActionCheck.NO_VALUE.getValue());
		request.setCustomerId(customerId);
		request.setviewDnsSubmit(VIEW);

		return request;
	}

	public static CgiDnsRequest createResolverDelegationDnsRemove(
			ClientDnsResolverRequest clientRequest) {
		CgiDnsRequest request = CgiDnsRequest.createResolverDns(clientRequest);
		LOGGER.debug("in goig to create request objects createResolverDnsCancelRequest ");
		request.setActionCheck(ActionCheck.SUBMIT.getValue());
		request.setCancel(CANCEL);

		return request;
	}

	private static CgiDnsRequest createResolverDns(
			ClientDnsResolverRequest clientRequest) {
		CgiDnsRequest request = new CgiDnsRequest();
		request.setLevel(DnsRequestType.RESOLVER.getValue());
		request.setIpVersion(clientRequest.getIpVersion().getValue());
		request.setContactEmailAddress(clientRequest.getContactEmailAddress());
		request.setHost(clientRequest.getHostname());
		request.setCustomerId(clientRequest.getCustomerId());
		request.setIpAddress(clientRequest.getIpAddress());

		return request;
	}

	//
	// Reverse DNS
	//
	public static CgiDnsRequest createReverseDelegationDnsAdd(
			ClientReverseDnsRequest clientRequest) {
		LOGGER.debug("creating CGI reverse delegation dns add request");

		CgiDnsRequest request = createReverseDelegationDns(clientRequest);
		request.setServiceid(clientRequest.getServiceId());
		request.setBtnCheck3(String.valueOf(ActionCheck.REV_DELEGATION.getValue()));//btn_check=11
		request.setBtnCheck1(String.valueOf(ActionCheck.NO_VALUE.getValue())); // btn_check=1
		request.setBtnCheck9(String.valueOf(ActionCheck.SUBMIT.getValue())); // btn_check=9
		request.setSubmit(CgiDnsRequest.SUBMIT);

		return request;
	}

	public static CgiDnsRequest createReverseDelegationDnsRemove(
			ClientReverseDnsRequest clientRequest) {
		LOGGER.debug("creating CGI reverse deletion dns remove request");

		CgiDnsRequest request = createReverseDelegationDns(clientRequest);
		request.setBtnCheck3(String.valueOf(ActionCheck.REV_DELEGATION.getValue()));//btn_check=11
		request.setBtnCheck1(String.valueOf(ActionCheck.SUBMIT.getValue())); // btn_check=1
		request.setBtnCheck9(String.valueOf(ActionCheck.NO_VALUE.getValue())); // btn_check=9
		request.setCancel(CgiDnsRequest.CANCEL);

		return request;
	}

	public static CgiDnsRequest createReverseMappingDnsAdd(
			ClientReverseDnsRequest clientRequest) {
		LOGGER.debug("creating CGI reverse mapping dns add request");

		CgiDnsRequest request = createReverseMappingDns(clientRequest);
		request.setServiceid(clientRequest.getServiceId());
		request.setBtnCheck1(String.valueOf(ActionCheck.NO_VALUE.getValue())); // btn_check=1
		request.setBtnCheck9(String.valueOf(ActionCheck.SUBMIT.getValue())); // btn_check=9
		request.setSubmit(CgiDnsRequest.SUBMIT);

		return request;
	}

	public static CgiDnsRequest createReverseMappingDnsRemove(
			ClientReverseDnsRequest clientRequest) {
		LOGGER.debug("creating CGI reverse mapping dns add request");

		CgiDnsRequest request = createReverseMappingDns(clientRequest);
		request.setBtnCheck1(String.valueOf(ActionCheck.SUBMIT.getValue())); // btn_check=1
		request.setBtnCheck9(String.valueOf(ActionCheck.NO_VALUE.getValue())); // btn_check=9
		request.setCancel(CgiDnsRequest.CANCEL);

		return request;
	}

	private static CgiDnsRequest createReverseDelegationDns(
			ClientReverseDnsRequest clientRequest) {
		CgiDnsRequest request = createReverseDns(clientRequest);
		request.setReverseDEmailAddress(clientRequest.getContactEmailAddress());
		request.setReverseDIPBlock(clientRequest.getIpBlock());
		request.setReverseDDomainNS(clientRequest.getNameServer());
		request.setReverseType(String.valueOf(ActionCheck.REV_DELEGATION
				.getValue()));
		return request;
	}

	private static CgiDnsRequest createReverseMappingDns(
			ClientReverseDnsRequest clientRequest) {
		CgiDnsRequest request = createReverseDns(clientRequest);
		request.setReverseMEmailAddress(clientRequest.getContactEmailAddress());
		request.setReverseMIP(clientRequest.getIpBlock());
		request.setReverseMDomain(clientRequest.getNameServer());
		request.setReverseType(String.valueOf(ActionCheck.REV_MAPPING
				.getValue()));
		request.setBtnCheck3(String.valueOf(ActionCheck.REV_MAPPING.getValue()));// btn_check=11
		return request;
	}

	private static CgiDnsRequest createReverseDns(
			ClientReverseDnsRequest clientRequest) {
		CgiDnsRequest request = new CgiDnsRequest();
		request.setLevel(DnsRequestType.REVERSE.getValue());
		request.setCustomerId(clientRequest.getCustomerId());
		request.setIpVersion(clientRequest.getIpVersion().getValue());
		request.setReverseType(String.valueOf(ActionCheck.REV_DELEGATION
				.getValue()));
	//	request.setBtnCheck1(String.valueOf(ActionCheck.NO_VALUE.getValue())); // btn_check=1
	//	request.setBtnCheck9(String.valueOf(ActionCheck.SUBMIT.getValue())); // btn_check=9
		
	
		return request;
	}

	public static CgiDnsRequest reverseDnsList(String customerId) {
		CgiDnsRequest request = new CgiDnsRequest();
		LOGGER.debug("create request object for reverseDnsList ");
		request.setLevel(DnsRequestType.REVERSE.getValue());// level=51
		request.setCustomerId(customerId);
		request.setReverseView(VIEW);
		return request;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getIpVersion() {
		return ipVersion;
	}

	public String[] getDbRowId() {
		return dbRowId;
	}

	public void setDbRowId(String[] dbRowId) {
		this.dbRowId = dbRowId;
	}

	public void setIpVersion(Integer ipVersion) {
		this.ipVersion = ipVersion;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public String getViewDnsSubmit() {
		return viewDnsSubmit;
	}

	public void setviewDnsSubmit(String viewDnsSubmit) {
		this.viewDnsSubmit = viewDnsSubmit;
	}

	public String getDom() {
		return dom;
	}

	public void setDom(String dom) {
		this.dom = dom;
	}

	public Integer getActionCheck() {
		return actionCheck;
	}

	public void setActionCheck(Integer actionCheck) {
		this.actionCheck = actionCheck;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getReverseMEmailAddress() {
		return reverseMEmailAddress;
	}

	public void setReverseMEmailAddress(String reverseMEmailAddress) {
		this.reverseMEmailAddress = reverseMEmailAddress;
	}

	public String getReverseDEmailAddress() {
		return reverseDEmailAddress;
	}

	public void setReverseDEmailAddress(String reverseDEmailAddress) {
		this.reverseDEmailAddress = reverseDEmailAddress;
	}

	public String getReverseMIP() {
		return reverseMIP;
	}

	public void setReverseMIP(String reverseMIP) {
		this.reverseMIP = reverseMIP;
	}

	public String getReverseMDomain() {
		return reverseMDomain;
	}

	public void setReverseMDomain(String reverseMDomain) {
		this.reverseMDomain = reverseMDomain;
	}

	public String getReverseDIPBlock() {
		return reverseDIPBlock;
	}

	public void setReverseDIPBlock(String reverseDIPBlock) {
		this.reverseDIPBlock = reverseDIPBlock;
	}

	public String getReverseDDomainNS() {
		return reverseDDomainNS;
	}

	public void setReverseDDomainNS(String reverseDDomainNS) {
		this.reverseDDomainNS = reverseDDomainNS;
	}

	public void setViewDnsSubmit(String viewDnsSubmit) {
		this.viewDnsSubmit = viewDnsSubmit;
	}

	public String getReverseType() {
		return reverseType;
	}

	public void setReverseType(String reverseType) {
		this.reverseType = reverseType;
	}

	public String getBtnCheck3() {
		return btnCheck3;
	}

	public void setBtnCheck3(String btnCheck3) {
		this.btnCheck3 = btnCheck3;
	}

	public String getBtnCheck9() {
		return btnCheck9;
	}

	public void setBtnCheck9(String btnCheck9) {
		this.btnCheck9 = btnCheck9;
	}

	public String getBtnCheck1() {
		return btnCheck1;
	}

	public void setBtnCheck1(String btnCheck1) {
		this.btnCheck1 = btnCheck1;
	}

	public String getReverseView() {
		return reverseView;
	}

	public void setReverseView(String reverseView) {
		this.reverseView = reverseView;
	}

	public String getContactEmailAddressprimary() {
		return contactEmailAddressprimary;
	}

	public void setContactEmailAddressprimary(String contactEmailAddressprimary) {
		this.contactEmailAddressprimary = contactEmailAddressprimary;
	}

	public String getPrimaryDnsDomain() {
		return primaryDnsDomain;
	}

	public void setPrimaryDnsDomain(String primaryDnsDomain) {
		this.primaryDnsDomain = primaryDnsDomain;
	}

	public String getPrimaryDnsHost() {
		return primaryDnsHost;
	}

	public void setPrimaryDnsHost(String primaryDnsHost) {
		this.primaryDnsHost = primaryDnsHost;
	}

	public String getPrimaryTypeId() {
		return primaryTypeId;
	}

	public void setPrimaryTypeId(String primaryTypeId) {
		this.primaryTypeId = primaryTypeId;
	}

	public String getPrimaryIPV4() {
		return primaryIPV4;
	}

	public void setPrimaryIPV4(String primaryIPV4) {
		this.primaryIPV4 = primaryIPV4;
	}

	public String getPrimaryIPV6() {
		return primaryIPV6;
	}

	public void setPrimaryIPV6(String primaryIPV6) {
		this.primaryIPV6 = primaryIPV6;
	}

	public String getPrimaryCnHost() {
		return primaryCnHost;
	}

	public void setPrimaryCnHost(String primaryCnHost) {
		this.primaryCnHost = primaryCnHost;
	}

	public String getPrimarymxpv() {
		return primarymxpv;
	}

	public void setPrimarymxpv(String primarymxpv) {
		this.primarymxpv = primarymxpv;
	}

	public String getPrimarymxex() {
		return primarymxex;
	}

	public void setPrimarymxex(String primarymxex) {
		this.primarymxex = primarymxex;
	}

	public String getPrimaryNs() {
		return primaryNs;
	}

	public void setPrimaryNs(String primaryNs) {
		this.primaryNs = primaryNs;
	}

	public String getPrimaryTtText() {
		return primaryTtText;
	}

	public void setPrimaryTtText(String primaryTtText) {
		this.primaryTtText = primaryTtText;
	}

	public String getPrimaryAddSubmit() {
		return primaryAddSubmit;
	}

	public void setPrimaryAddSubmit(String primaryAddSubmit) {
		this.primaryAddSubmit = primaryAddSubmit;
	}

	public String getPrimaryViewSubmit() {
		return primaryViewSubmit;
	}

	public void setPrimaryViewSubmit(String primaryViewSubmit) {
		this.primaryViewSubmit = primaryViewSubmit;
	}

	public String getPrimaryRemoveSubmit() {
		return primaryRemoveSubmit;
	}

	public void setPrimaryRemoveSubmit(String primaryRemoveSubmit) {
		this.primaryRemoveSubmit = primaryRemoveSubmit;
	}

	public String getContactSEmailAddress() {
		return contactSEmailAddress;
	}

	public void setContactSEmailAddress(String contactSEmailAddress) {
		this.contactSEmailAddress = contactSEmailAddress;
	}

	public String getSecondaryDnsDomain() {
		return secondaryDnsDomain;
	}

	public void setSecondaryDnsDomain(String secondaryDnsDomain) {
		this.secondaryDnsDomain = secondaryDnsDomain;
	}

	public String getSecondaryIpAddress() {
		return secondaryIpAddress;
	}

	public void setSecondaryIpAddress(String secondaryIpAddress) {
		this.secondaryIpAddress = secondaryIpAddress;
	}

	public String getSecondaryRowID() {
		return secondaryRowID;
	}

	public void setSecondaryRowID(String secondaryRowID) {
		this.secondaryRowID = secondaryRowID;
	}

	public Integer getBtnChecksec() {
		return btnChecksec;
	}

	public void setBtnChecksec(Integer btnChecksec) {
		this.btnChecksec = btnChecksec;

	}

	public String getDbId() {
		return dbId;
	}

	public void setDbId(String dbId) {
		this.dbId = dbId;
	}

	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}


}
