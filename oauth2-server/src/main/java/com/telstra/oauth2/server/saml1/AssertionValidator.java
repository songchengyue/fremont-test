package com.telstra.oauth2.server.saml1;


import org.joda.time.DateTime;
import org.opensaml.saml1.core.Assertion;
import org.opensaml.security.SAMLSignatureProfileValidator;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.signature.SignatureValidator;
import org.opensaml.xml.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.telstra.oauth2.server.saml.CredentialResolver;

@Component
public class AssertionValidator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AssertionValidator.class);
	
	@Value("${security.saml.bearer.auth.oauth2.saml.assertion.validation.issuer}")
	private String issuer;
	
	@Value("${security.saml.bearer.auth.oauth2.saml.assertion.validation.conditionsOnOrAfter:true}")
	private boolean conditionsOnOrAfter ;
	

	@Autowired
	private AssertionParser parser;
	
	@Autowired
	private CredentialResolver credentialResolver;	
		
	public AssertionValidator(AssertionParser parser) {
		this.parser = parser;
	}
	
	private AssertionValidator(){
	
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	public boolean isConditionsOnOrAfter() {
		return conditionsOnOrAfter;
	}

	public void setConditionsOnOrAfter(boolean conditionsOnOrAfter) {
		this.conditionsOnOrAfter = conditionsOnOrAfter;
	}

	
	private void validateSignature(Assertion assertion) throws ValidationException {	
		// TODO
		
		// 1. check the message hasn't been tampered by checking the signature
		// 2. check the message was signed by someone we trust!		
		// see https://wiki.shibboleth.net/confluence/display/OpenSAML/OSTwoUserManJavaDSIG
		
		try {
			
			//
			// Validate that signature has all the element we need to perform cryptographic validation.
			//
			SAMLSignatureProfileValidator profileValidator = new SAMLSignatureProfileValidator();
			profileValidator.validate(assertion.getSignature());
			
			//
			// Cryptographic validation.
			//
			Credential validationCredential = credentialResolver.resolve();
			SignatureValidator validator = new SignatureValidator(validationCredential);
		 	validator.validate(assertion.getSignature());
		} catch (ValidationException e) {
			LOGGER.warn("assertion signature validation failed", e);
			throw new ValidationException("Invalid signature!", e);
		}
	}
	
	private void validateWellFormedness(Assertion assertion) throws ValidationException {
		try {
			SAMLSignatureProfileValidator profileValidator = new SAMLSignatureProfileValidator();		
			profileValidator.validate(assertion.getSignature());
		} catch (ValidationException e) {
			LOGGER.warn("assertion well-formedness validation failed", e);
			throw new ValidationException("Assertion is not well-formed!", e);
		}
	}
	
	 void validateIssuer(Assertion assertion) throws ValidationException {
		String validIssuer = parser.parseIssuer(assertion);
		if (!issuer.equals(validIssuer)) {
			LOGGER.warn("assertion issuer validation failed, found " + issuer + ", expected " + validIssuer);
			throw new ValidationException("Issuer must contain our trusted Identity Provider!");
		}
	}
	
	void validateConditions(Assertion assertion, DateTime assertionCheckDateTime) throws ValidationException {
		if(!conditionsOnOrAfter){
			return ;
		}
		DateTime notOnOrAfterDate= parser.parseConditionsNotOnOrAfterDate(assertion);
		Long millisNotOnOrAfterDate = new Long(notOnOrAfterDate.getMillis());
		Long millisAssertionCheckDateTime = new Long(assertionCheckDateTime.getMillis());
		
		// log this value as we need to know what times we are getting in production
		// so we can decide what our strategy should be for OAuth Token timeouts
		LOGGER.info("assertion conditions NotOnOrAfter (ms) " + millisNotOnOrAfterDate);
		
		if(!millisNotOnOrAfterDate.equals(millisAssertionCheckDateTime)) {
			LOGGER.warn("assertion condition not on or after date validation failed for date " + notOnOrAfterDate);
			throw new ValidationException("Condition not on or after date");
		}
		
	}



	public void validate(Assertion assertion) throws ValidationException {
		
		// test comment to demonstrate merging
		
		validateWellFormedness(assertion);
		
		//
		// TODO Check all conditions from http://tools.ietf.org/html/draft-ietf-oauth-saml2-bearer-17 section 3
		//
		
		validateIssuer(assertion);
		
		validateConditions(assertion,DateTime.now());

		validateSignature(assertion);
	}
	
	
}
