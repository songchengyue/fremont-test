/*
 * An XML document type.
 * Localname: getCmdbClassDefinition
 * Namespace: http://schemas.hp.com/ucmdb/1/params/classmodel
 * Java type: com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.classmodel.impl;

import org.apache.axiom.om.OMXMLBuilderFactory;
/**
 * A document containing one getCmdbClassDefinition(@http://schemas.hp.com/ucmdb/1/params/classmodel) element.
 *
 * This is a complex type.
 */
public class GetCmdbClassDefinitionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionDocument
{
    
    public GetCmdbClassDefinitionDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCMDBCLASSDEFINITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getCmdbClassDefinition");
    
    
    /**
     * Gets the "getCmdbClassDefinition" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinition getGetCmdbClassDefinition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinition target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinition)get_store().find_element_user(GETCMDBCLASSDEFINITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCmdbClassDefinition" element
     */
    public void setGetCmdbClassDefinition(com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinition getCmdbClassDefinition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinition target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinition)get_store().find_element_user(GETCMDBCLASSDEFINITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinition)get_store().add_element_user(GETCMDBCLASSDEFINITION$0);
            }
            target.set(getCmdbClassDefinition);
        }
    }
    
    /**
     * Appends and returns a new empty "getCmdbClassDefinition" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinition addNewGetCmdbClassDefinition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinition target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinition)get_store().add_element_user(GETCMDBCLASSDEFINITION$0);
            return target;
        }
    }
}
