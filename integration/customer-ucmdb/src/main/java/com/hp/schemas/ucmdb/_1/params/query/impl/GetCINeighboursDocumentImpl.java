/*
 * An XML document type.
 * Localname: getCINeighbours
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetCINeighboursDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getCINeighbours(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetCINeighboursDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetCINeighboursDocument
{
    
    public GetCINeighboursDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCINEIGHBOURS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getCINeighbours");
    
    
    /**
     * Gets the "getCINeighbours" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCINeighbours getGetCINeighbours()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCINeighbours target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours)get_store().find_element_user(GETCINEIGHBOURS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCINeighbours" element
     */
    public void setGetCINeighbours(com.hp.schemas.ucmdb._1.params.query.GetCINeighbours getCINeighbours)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCINeighbours target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours)get_store().find_element_user(GETCINEIGHBOURS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours)get_store().add_element_user(GETCINEIGHBOURS$0);
            }
            target.set(getCINeighbours);
        }
    }
    
    /**
     * Appends and returns a new empty "getCINeighbours" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCINeighbours addNewGetCINeighbours()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCINeighbours target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours)get_store().add_element_user(GETCINEIGHBOURS$0);
            return target;
        }
    }
}
