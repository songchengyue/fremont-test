/*
 * XML Type:  executeTopologyQueryByNameResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML executeTopologyQueryByNameResponse(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class ExecuteTopologyQueryByNameResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse
{
    
    public ExecuteTopologyQueryByNameResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TOPOLOGYMAP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "topologyMap");
    private static final javax.xml.namespace.QName CHUNKINFO$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "chunkInfo");
    
    
    /**
     * Gets the "topologyMap" element
     */
    public com.hp.schemas.ucmdb._1.types.TopologyMap getTopologyMap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.TopologyMap target = null;
            target = (com.hp.schemas.ucmdb._1.types.TopologyMap)get_store().find_element_user(TOPOLOGYMAP$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "topologyMap" element
     */
    public void setTopologyMap(com.hp.schemas.ucmdb._1.types.TopologyMap topologyMap)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.TopologyMap target = null;
            target = (com.hp.schemas.ucmdb._1.types.TopologyMap)get_store().find_element_user(TOPOLOGYMAP$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.TopologyMap)get_store().add_element_user(TOPOLOGYMAP$0);
            }
            target.set(topologyMap);
        }
    }
    
    /**
     * Appends and returns a new empty "topologyMap" element
     */
    public com.hp.schemas.ucmdb._1.types.TopologyMap addNewTopologyMap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.TopologyMap target = null;
            target = (com.hp.schemas.ucmdb._1.types.TopologyMap)get_store().add_element_user(TOPOLOGYMAP$0);
            return target;
        }
    }
    
    /**
     * Gets the "chunkInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkInfo getChunkInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().find_element_user(CHUNKINFO$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "chunkInfo" element
     */
    public void setChunkInfo(com.hp.schemas.ucmdb._1.types.ChunkInfo chunkInfo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().find_element_user(CHUNKINFO$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().add_element_user(CHUNKINFO$2);
            }
            target.set(chunkInfo);
        }
    }
    
    /**
     * Appends and returns a new empty "chunkInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkInfo addNewChunkInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().add_element_user(CHUNKINFO$2);
            return target;
        }
    }
}
