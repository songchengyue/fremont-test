package com.telstra.netops.lookingglass.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.telstra.netops.lookingglass.server.model.ToolAccess;

public interface ToolAccessRepository extends PagingAndSortingRepository<ToolAccess, Long> {

	@Query("select DISTINCT tool from ToolAccess where productLine.code = :productLine and menuAccess = 'Y'")
	List<String> findByProductLine(@Param("productLine") String productLine);
	
}
