package com.telstra.netops.dns.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.telstra.netops.dns.server.model.Service;

@org.springframework.stereotype.Service
public interface ServiceIdAccessRepository extends PagingAndSortingRepository<Service, Long> {

	@Query("select DISTINCT serviceId from Service where productLine.code = :productLine and customerId = :customerid")
	List<String> findDistinctServiceIdByProductLineAndCustomerId(@Param("productLine") String productLine, @Param("customerid") String customerId);
	
}
