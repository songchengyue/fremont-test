/*
 * XML Type:  DoubleProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.DoubleProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML DoubleProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class DoublePropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.DoubleProps
{
    
    public DoublePropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DOUBLEPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "doubleProp");
    
    
    /**
     * Gets array of all "doubleProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.DoubleProp[] getDoublePropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DOUBLEPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.DoubleProp[] result = new com.hp.schemas.ucmdb._1.types.DoubleProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "doubleProp" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleProp getDoublePropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().find_element_user(DOUBLEPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "doubleProp" element
     */
    public int sizeOfDoublePropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DOUBLEPROP$0);
        }
    }
    
    /**
     * Sets array of all "doubleProp" element
     */
    public void setDoublePropArray(com.hp.schemas.ucmdb._1.types.DoubleProp[] doublePropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(doublePropArray, DOUBLEPROP$0);
        }
    }
    
    /**
     * Sets ith "doubleProp" element
     */
    public void setDoublePropArray(int i, com.hp.schemas.ucmdb._1.types.DoubleProp doubleProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().find_element_user(DOUBLEPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(doubleProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "doubleProp" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleProp insertNewDoubleProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().insert_element_user(DOUBLEPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "doubleProp" element
     */
    public com.hp.schemas.ucmdb._1.types.DoubleProp addNewDoubleProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.DoubleProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.DoubleProp)get_store().add_element_user(DOUBLEPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "doubleProp" element
     */
    public void removeDoubleProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DOUBLEPROP$0, i);
        }
    }
}
