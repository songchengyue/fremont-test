/*
 * XML Type:  ChunkKey
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ChunkKey
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ChunkKey(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ChunkKeyImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ChunkKey
{
    
    public ChunkKeyImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName KEY1$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "key1");
    private static final javax.xml.namespace.QName KEY2$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "key2");
    
    
    /**
     * Gets the "key1" element
     */
    public java.lang.String getKey1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KEY1$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "key1" element
     */
    public org.apache.xmlbeans.XmlString xgetKey1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KEY1$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "key1" element
     */
    public void setKey1(java.lang.String key1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KEY1$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(KEY1$0);
            }
            target.setStringValue(key1);
        }
    }
    
    /**
     * Sets (as xml) the "key1" element
     */
    public void xsetKey1(org.apache.xmlbeans.XmlString key1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KEY1$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(KEY1$0);
            }
            target.set(key1);
        }
    }
    
    /**
     * Gets the "key2" element
     */
    public java.lang.String getKey2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KEY2$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "key2" element
     */
    public org.apache.xmlbeans.XmlString xgetKey2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KEY2$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "key2" element
     */
    public void setKey2(java.lang.String key2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KEY2$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(KEY2$2);
            }
            target.setStringValue(key2);
        }
    }
    
    /**
     * Sets (as xml) the "key2" element
     */
    public void xsetKey2(org.apache.xmlbeans.XmlString key2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KEY2$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(KEY2$2);
            }
            target.set(key2);
        }
    }
}
