/*
 * An XML document type.
 * Localname: deactivateJobRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one deactivateJobRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class DeactivateJobRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequestDocument
{
    
    public DeactivateJobRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DEACTIVATEJOBREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "deactivateJobRequest");
    
    
    /**
     * Gets the "deactivateJobRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequest getDeactivateJobRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequest)get_store().find_element_user(DEACTIVATEJOBREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "deactivateJobRequest" element
     */
    public void setDeactivateJobRequest(com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequest deactivateJobRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequest)get_store().find_element_user(DEACTIVATEJOBREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequest)get_store().add_element_user(DEACTIVATEJOBREQUEST$0);
            }
            target.set(deactivateJobRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "deactivateJobRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequest addNewDeactivateJobRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.DeactivateJobRequest)get_store().add_element_user(DEACTIVATEJOBREQUEST$0);
            return target;
        }
    }
}
