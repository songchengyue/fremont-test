/*
 * An XML document type.
 * Localname: getJobTriggerTqlsRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getJobTriggerTqlsRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetJobTriggerTqlsRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequestDocument
{
    
    public GetJobTriggerTqlsRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETJOBTRIGGERTQLSREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getJobTriggerTqlsRequest");
    
    
    /**
     * Gets the "getJobTriggerTqlsRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest getGetJobTriggerTqlsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest)get_store().find_element_user(GETJOBTRIGGERTQLSREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getJobTriggerTqlsRequest" element
     */
    public void setGetJobTriggerTqlsRequest(com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest getJobTriggerTqlsRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest)get_store().find_element_user(GETJOBTRIGGERTQLSREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest)get_store().add_element_user(GETJOBTRIGGERTQLSREQUEST$0);
            }
            target.set(getJobTriggerTqlsRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "getJobTriggerTqlsRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest addNewGetJobTriggerTqlsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetJobTriggerTqlsRequest)get_store().add_element_user(GETJOBTRIGGERTQLSREQUEST$0);
            return target;
        }
    }
}
