/*
 * An XML document type.
 * Localname: checkViewDiscoveryProgressRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one checkViewDiscoveryProgressRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class CheckViewDiscoveryProgressRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequestDocument
{
    
    public CheckViewDiscoveryProgressRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHECKVIEWDISCOVERYPROGRESSREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "checkViewDiscoveryProgressRequest");
    
    
    /**
     * Gets the "checkViewDiscoveryProgressRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequest getCheckViewDiscoveryProgressRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequest)get_store().find_element_user(CHECKVIEWDISCOVERYPROGRESSREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "checkViewDiscoveryProgressRequest" element
     */
    public void setCheckViewDiscoveryProgressRequest(com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequest checkViewDiscoveryProgressRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequest)get_store().find_element_user(CHECKVIEWDISCOVERYPROGRESSREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequest)get_store().add_element_user(CHECKVIEWDISCOVERYPROGRESSREQUEST$0);
            }
            target.set(checkViewDiscoveryProgressRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "checkViewDiscoveryProgressRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequest addNewCheckViewDiscoveryProgressRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.CheckViewDiscoveryProgressRequest)get_store().add_element_user(CHECKVIEWDISCOVERYPROGRESSREQUEST$0);
            return target;
        }
    }
}
