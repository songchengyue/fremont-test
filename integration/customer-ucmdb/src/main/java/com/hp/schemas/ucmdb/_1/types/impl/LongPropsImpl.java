/*
 * XML Type:  LongProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.LongProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML LongProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class LongPropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.LongProps
{
    
    public LongPropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LONGPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "longProp");
    
    
    /**
     * Gets array of all "longProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.LongProp[] getLongPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(LONGPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.LongProp[] result = new com.hp.schemas.ucmdb._1.types.LongProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "longProp" element
     */
    public com.hp.schemas.ucmdb._1.types.LongProp getLongPropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().find_element_user(LONGPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "longProp" element
     */
    public int sizeOfLongPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(LONGPROP$0);
        }
    }
    
    /**
     * Sets array of all "longProp" element
     */
    public void setLongPropArray(com.hp.schemas.ucmdb._1.types.LongProp[] longPropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(longPropArray, LONGPROP$0);
        }
    }
    
    /**
     * Sets ith "longProp" element
     */
    public void setLongPropArray(int i, com.hp.schemas.ucmdb._1.types.LongProp longProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().find_element_user(LONGPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(longProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "longProp" element
     */
    public com.hp.schemas.ucmdb._1.types.LongProp insertNewLongProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().insert_element_user(LONGPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "longProp" element
     */
    public com.hp.schemas.ucmdb._1.types.LongProp addNewLongProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().add_element_user(LONGPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "longProp" element
     */
    public void removeLongProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(LONGPROP$0, i);
        }
    }
}
