package com.telstra.netops.integration.ipresource.processor;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class IpResource {
	
	private IpResourceStatus status;
	
	private String service;
	
	private String address;
	
	private Integer mask;

	public IpResource(IpResourceStatus status, String service, String address,
			Integer mask) {
		super();
		this.status = status;
		this.service = service;
		this.address = address;
		this.mask = mask;
	}
		
	public IpResourceStatus getStatus() {
		return status;
	}

	public void setStatus(IpResourceStatus status) {
		this.status = status;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getMask() {
		return mask;
	}

	public void setMask(Integer mask) {
		this.mask = mask;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}	

}
