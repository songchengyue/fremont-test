# Customer uCMDB Synchronization

## Synopsis

This project is synchronizes customer information from uCMDB to netops.

uCMDB is queried via a web service and information is inserted in the
Netops database.

## Prerequisites

* CVS
* Java 8 JDK
* Maven

## Run

* Get the code

Check the code out from the CVS server oxfred22.in.reach.com. It's in /usr/local/CVS/cvsroot/CVSROOT/netops/integration/customer-ucmdb

* Install Maven libraries

There are two libraries you need to install in your Maven repository to

```
mvn install:install-file -Dfile=<absolute-oracle-jdbc-file-name> -DgroupId=com.oracle -DartifactId=ojdbc5 -Dversion=11.1.0.7.0 -Dpackaging=jar

cd netops\integration\customer-ucmdb
mvn install:install-file -Dfile=ucmdbservice-generated.jar -DgroupId=com.telstra.netops.integration -DartifactId=ucmdbservice-generated -Dversion=0.1 -Dpackaging=jar
```

* Build and run

```
cd netops\integration\customer-ucmdb
mvn package
java -jar target\customer-ucmdb-<version>.jar <config-file>
```

## Development

### Stub Generation

TODO - document how stubs were generated

### Run Locally Using SoapUI

For development, import the project into Eclipse using the "Existing Maven Projects" import option (assumming you have already checked out the project from CVS).

There are two sample run targets you can use for testing "Customer uCMDB Integration - SIT" and "Customer uCMDB Integration - AU Testing".

You can mock the uCMDB web service using [SoapUI](https://www.soapui.org/):
* install SoapUI
* import project UcmdbService-soapui-project.xml - this was created from the wsdl/localversion/UcmdbService.wsdl
* start up "UcmdbServiceSOAP11Binding MockService"
* point your config to the mock by setting WS_URL=http://localhost:8088/mockUcmdbServiceSOAP11Binding - see the getFilteredCIsByType Response for details


## TODO

Some things that could be improved:

* use DataSource and Spring to inject it rather than manually getting netops database connections
* create "local" mode which uses H2 database for easy testing 
* use JPA for database inserts/update
* convert configuration file to standard Spring one like the other netops modules and inject values with Spring Value annotation
* unit tests
* organize generated source better, put it in a separate source file
* document how to generate Java WSDL Stubs
* convert fields in UcmdbNetopsSynch to non-static fields
* WDSL authentication username/password hard-coded!

## Software used

This project makes use of [Spring Boot](http://projects.spring.io/spring-boot/).
