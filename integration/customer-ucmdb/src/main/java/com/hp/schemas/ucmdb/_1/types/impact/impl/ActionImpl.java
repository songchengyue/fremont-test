/*
 * XML Type:  Action
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.Action
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML Action(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ActionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.Action
{
    
    public ActionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
