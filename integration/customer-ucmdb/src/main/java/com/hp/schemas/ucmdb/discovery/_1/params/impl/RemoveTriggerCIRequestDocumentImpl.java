/*
 * An XML document type.
 * Localname: removeTriggerCIRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerCIRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one removeTriggerCIRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class RemoveTriggerCIRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.RemoveTriggerCIRequestDocument
{
    
    public RemoveTriggerCIRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REMOVETRIGGERCIREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "removeTriggerCIRequest");
    
    
    /**
     * Gets the "removeTriggerCIRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest getRemoveTriggerCIRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest)get_store().find_element_user(REMOVETRIGGERCIREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "removeTriggerCIRequest" element
     */
    public void setRemoveTriggerCIRequest(com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest removeTriggerCIRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest)get_store().find_element_user(REMOVETRIGGERCIREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest)get_store().add_element_user(REMOVETRIGGERCIREQUEST$0);
            }
            target.set(removeTriggerCIRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "removeTriggerCIRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest addNewRemoveTriggerCIRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest)get_store().add_element_user(REMOVETRIGGERCIREQUEST$0);
            return target;
        }
    }
}
