/*
 * An XML document type.
 * Localname: addTriggerCIRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one addTriggerCIRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class AddTriggerCIRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequestDocument
{
    
    public AddTriggerCIRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDTRIGGERCIREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "addTriggerCIRequest");
    
    
    /**
     * Gets the "addTriggerCIRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest getAddTriggerCIRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest)get_store().find_element_user(ADDTRIGGERCIREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "addTriggerCIRequest" element
     */
    public void setAddTriggerCIRequest(com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest addTriggerCIRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest)get_store().find_element_user(ADDTRIGGERCIREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest)get_store().add_element_user(ADDTRIGGERCIREQUEST$0);
            }
            target.set(addTriggerCIRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "addTriggerCIRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest addNewAddTriggerCIRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerCIRequest)get_store().add_element_user(ADDTRIGGERCIREQUEST$0);
            return target;
        }
    }
}
