# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 24 September 2001
# File: CMS_SECDNS.pm
#
# $Id: CMS_SECDNS_DB.pm,v 1.1.1.1 2003/10/19 13:49:52 rxc Exp $

package CMS_SECDNS_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
use Mail::Sendmail;

our $error;

sub add_secdns_entry {
  my $accno = $dbh->quote(shift @_);
  my $domain_name = $dbh->quote(shift @_);
  my $primary_ip = $dbh->quote(shift @_);
  my $sec_contact = $dbh->quote(shift @_);
  my $sth = $dbh->prepare
    ("INSERT INTO secdns
                  (accno, domain_name, primary_ip, contact, request_time, status)
           VALUES ($accno, $domain_name, $primary_ip, $sec_contact, CURRENT_TIMESTAMP,1)");

  # 1 is pending additions

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;
}

sub update_secdns_entry {
  my $accno = $dbh->quote(shift @_);
  my $domain_name = $dbh->quote(shift @_);
  my $primary_ip = $dbh->quote(shift @_);
  my $sec_contact = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("UPDATE secdns
         SET primary_ip = $primary_ip,
	     status = 1,
	     contact = $sec_contact,
             request_time = CURRENT_TIMESTAMP
       WHERE accno = $accno
         AND domain_name = $domain_name");

  # 5 is pending changes

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;

}

sub delete_secdns_entry {
  my $accno =  $dbh->quote(shift @_);
  my $domain_name =  $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("UPDATE secdns 
      SET status = 3,
          request_time = CURRENT_TIMESTAMP
      WHERE accno = $accno
        AND  domain_name = $domain_name");

  # 3 is pending deletions

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;
}

sub get_secdns_entries_for_account {
  my $accno =  $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT domain_name, primary_ip, request_time, status
      FROM secdns
      WHERE accno = $accno");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
}

sub get_secdns_entries {
  my $sth = $dbh->prepare
    ("SELECT domain_name, primary_ip
      FROM secdns");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
}

sub get_pro_secdns_entries {
  my $sth = $dbh->prepare("SELECT domain_name, primary_ip FROM secdns
		WHERE status = 1 or status = 2");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
}

sub complete_secdns_entries {
  my $sth = $dbh->prepare("Update secdns set status = 2 where status = 1");
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }   
 
  $sth = $dbh->prepare("Delete from secdns where status = 3");
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }   
}

sub get_secdns_status {
  my $sth = $dbh->prepare
    ("SELECT status, statustext
      FROM secdns_status");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
}

sub get_accno_for_dns_domain {
  my $domain_name = $dbh->quote(shift @_);

  my $sth = $dbh->prepare 
    ("SELECT accno
      FROM secdns
      WHERE domain_name = $domain_name");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

 

 if (my @result = $sth->fetchrow_array) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result[0];
  } else {
    $error = $dbh->errstr;
    $sth->finish;
    return undef;
  }

}

#fetch request time from DB
sub time_db {
my $temp = "SELECT request_time from secdns";
        my $sth = $dbh->prepare("$temp");
        $sth->execute();
        my $result = $sth->fetchall_arrayref;
        return $result;
}

#fetch all values from DB
sub all_values {
my ($reqtime) = $dbh->quote(shift @_);
my $temp = "SELECT accno, domain_name,primary_ip, status,contact FROM secdns where  request_time like $reqtime";
my $sth = $dbh->prepare("$temp");
$sth->execute();
my $result = $sth->fetchall_arrayref;
return $result;
}

#mail notification
sub SDNS_mail {
 my ($accno, $sdns_contact,$sdns_dom,$sdns_ip,$value_type)= @_;
 my $note = "Note: This email has been automatically generated. Please do not reply to this message as it is unattended.";
 my $message = ("$value_type SECONDARY NAME SERVER Service for following:\n\nAccount:\t$accno\n\nEmail:\t\t$sdns_contact\nDomain:\t\t$sdns_dom\nPrimary IP:\t$sdns_ip\n\nThank you.\n\n$note");
 my $subj = "$value_type SECONDARY NAME SERVER";
  my $to_mail = $sdns_contact;
 # my $cc_mail = "jajati-keshari.samal\@team.telstra.com;Anjan-Babu.Etha\@team.telstra.com;karuna.ballal\@team.telstra.com";
 my $bcc_mail = "TI.DL-GL-TI_EM\@team.telstra.com; TI.infosyscms\@team.telstra.com";
  my $from_mail = "\"Reach OLSS System\" (olss\@TelstraInternational.com)";
  my %mail = (
           To => $to_mail,
           Bcc => $bcc_mail,
           From => $from_mail,
           Subject => $subj
  );
  my $hostname = `hostname`;
  print "$hostname";
#  $mail{Smtp} = 'postoffice.net.reach.com';
   $mail{Smtp} = '$hostname';
  $mail{body} = $message;

  if (sendmail (%mail)) {
    print "\nMail sent success to $to_mail.\n" ;
  } else {
    print "Error sending mail to $to_mail: $Mail::Sendmail::error \n";
  }

}


sub search_secdns_entries {
  
  my $accno = shift @_ || "";
  my $domain_name = shift @_ || "";
  my $primary_ip = shift @_ || "";
  my $status = shift @_ || "";
  my $timestamp_start = shift @_ || "";
  my $timestamp_end = shift @_ || "";

  my $command = "SELECT accno, domain_name, primary_ip,
                 statustext, request_time
                 FROM secdns 
                 NATURAL JOIN secdns_status ";
  
  my $extension = "";

  if ($timestamp_start ne "") {
    $extension .= "and request_time >= '$timestamp_start'
                   and request_time <= '$timestamp_end' ";
  }
  
  if ($accno ne "") {
    $extension .= "and accno ilike '%$accno%' ";
  }
  
  if ($domain_name ne "") {
    $extension .= "and domain_name ilike '%$domain_name%' ";
  }
  
  if ($primary_ip ne "") {
    $extension .= "and primary_ip ilike '%$primary_ip%' ";
  }
  
  
  if ($status ne "") {
    $extension .= "and status = $status ";
  }  

  $extension =~ s/and/where/;
  
  

  $command .= "$extension order by request_time DESC ";
  
  if ($extension eq "") {
    # limit by default
    $command .= "limit 20";
  }

  my $sth = $dbh->prepare($command);

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
  
}

1;
