=head1 Name

    OAuth2Token - valid an OAuth2 token

=head1 Description

    Protect perl CGI scripts by passing an OAuth2 token in the 
    Authorization HTTP header and validating the token with
    this module.

    Config is read for file /etc/netops/site-application-<profile>.yml.
    This file must exist or you need to set an alternative by setting
    $OAuth2Token::config_file_glob

=cut

package OAuth2Token;

use Exporter;
use LWP::UserAgent;  
use HTTP::Request; 
use Carp;
use JSON::PP;
use YAML qw(LoadFile Dump);

use strict;

our $debug = 0;
our $config_file_glob = '/etc/netops/site-application-*.yml';
my $config;

our @ISA = qw(Exporter);
our @EXPORT_OK = qw(invalid $config_file_glob $debug);


=head2 _init_config()

Initialise configuration parameters. For internal use only.

=cut

sub _init_config() {
    if (!$config) {
        my @config_file = glob $config_file_glob;
        $main::logger->logcroak("no files matching config file glob $config_file_glob") unless (@config_file);
        $main::logger->logcroak("multiple config files matching glob $config_file_glob, expected one only") if (@config_file > 1);
        $config = LoadFile($config_file[0]);
        $main::logger->debug("YAML dump...");
        my $yaml_dump = Dump $config;
        $main::logger->debug($yaml_dump);
        $main::logger->logcroak("missing YAML section \"oauth\"") unless ($$config{'oauth'});
        $main::logger->logcroak("missing YAML section \"oauth.server\"") unless ($$config{'oauth'}{'server'});
        $main::logger->logcroak("missing YAML value \"oauth.server.clientId\"") unless ($$config{'oauth'}{'server'}{'clientId'});
        $main::logger->logcroak("missing YAML value \"oauth.server.clientSecret\"") unless ($$config{'oauth'}{'server'}{'clientSecret'});
        $main::logger->logcroak("missing YAML value \"oauth.server.checkTokenUrlTemplate\"") unless ($$config{'oauth'}{'server'}{'checkTokenUrlTemplate'});
    }
}


=head2 invalid($token)

Check if a token is invalid or not. Returns undef in valid or
the reason for the token being invalid.

=cut


sub invalid($) {
    my ($token) = @_;

    _init_config();

    $main::logger->debug("validating token $token...");

    my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 });  

    # Set a reasonable timeout. Remember, somewhere at the end of this request is
    # a user looking at a browser. Probably they don't want to wait more than
    # 20 seconds.  Default of 180 seconds is definitely too long!
    $ua->timeout(20);

    my $template_url = $$config{'oauth'}{'server'}{'checkTokenUrlTemplate'};
    my $url = $template_url;
    $url =~ s/<TOKEN>/$token/;
    $main::logger->debug("url template is $template_url");
    $main::logger->debug("url is $url");
    $main::logger->logcroak("template URL $template_url missing \"<TOKEN>\"") if ($url eq $template_url);

    my $request = HTTP::Request->new('GET', $url);  
    $request->header('Accept' => 'application/json');
    $request->authorization_basic(
        $$config{'oauth'}{'server'}{'clientId'},
        $$config{'oauth'}{'server'}{'clientSecret'});
    my $response = $ua->request($request); 

    if ($response->is_success) {
        $main::logger->debug("response: " . $response->decoded_content);
        return undef;
    } elsif ($response->code == 400) {
        # either the token is invalid or it was valid but has not expired

        $main::logger->debug("response: " . $response->decoded_content);

        my $reason = "unknown - can't parse JSON response";

        eval {
            my $json_response = decode_json($response->decoded_content);
            if ($json_response->{'error_description'}) {
                $reason = $json_response->{'error_description'};
            } else {
                $reason = "unknown - can't find error_description";
            }
        };

        return $reason;
    }

    return "Error contacting OAuth2 Server url: $url, status: " . $response->status_line;
}

1;
