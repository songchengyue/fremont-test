/*
 * XML Type:  Attribute
 * Namespace: http://schemas.hp.com/ucmdb/1/types/classmodel
 * Java type: com.hp.schemas.ucmdb._1.types.classmodel.Attribute
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.classmodel;


/**
 * An XML Attribute(@http://schemas.hp.com/ucmdb/1/types/classmodel).
 *
 * This is a complex type.
 */
public interface Attribute extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Attribute.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("attribute9d6atype");
    
    /**
     * Gets the "name" element
     */
    java.lang.String getName();
    
    /**
     * Gets (as xml) the "name" element
     */
    org.apache.xmlbeans.XmlString xgetName();
    
    /**
     * Sets the "name" element
     */
    void setName(java.lang.String name);
    
    /**
     * Sets (as xml) the "name" element
     */
    void xsetName(org.apache.xmlbeans.XmlString name);
    
    /**
     * Gets the "attrType" element
     */
    com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType.Enum getAttrType();
    
    /**
     * Gets (as xml) the "attrType" element
     */
    com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType xgetAttrType();
    
    /**
     * Sets the "attrType" element
     */
    void setAttrType(com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType.Enum attrType);
    
    /**
     * Sets (as xml) the "attrType" element
     */
    void xsetAttrType(com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType attrType);
    
    /**
     * Gets the "attrTypeName" element
     */
    java.lang.String getAttrTypeName();
    
    /**
     * Gets (as xml) the "attrTypeName" element
     */
    org.apache.xmlbeans.XmlString xgetAttrTypeName();
    
    /**
     * True if has "attrTypeName" element
     */
    boolean isSetAttrTypeName();
    
    /**
     * Sets the "attrTypeName" element
     */
    void setAttrTypeName(java.lang.String attrTypeName);
    
    /**
     * Sets (as xml) the "attrTypeName" element
     */
    void xsetAttrTypeName(org.apache.xmlbeans.XmlString attrTypeName);
    
    /**
     * Unsets the "attrTypeName" element
     */
    void unsetAttrTypeName();
    
    /**
     * Gets the "displayLabel" element
     */
    java.lang.String getDisplayLabel();
    
    /**
     * Gets (as xml) the "displayLabel" element
     */
    org.apache.xmlbeans.XmlString xgetDisplayLabel();
    
    /**
     * Sets the "displayLabel" element
     */
    void setDisplayLabel(java.lang.String displayLabel);
    
    /**
     * Sets (as xml) the "displayLabel" element
     */
    void xsetDisplayLabel(org.apache.xmlbeans.XmlString displayLabel);
    
    /**
     * Gets the "description" element
     */
    java.lang.String getDescription();
    
    /**
     * Gets (as xml) the "description" element
     */
    org.apache.xmlbeans.XmlString xgetDescription();
    
    /**
     * Sets the "description" element
     */
    void setDescription(java.lang.String description);
    
    /**
     * Sets (as xml) the "description" element
     */
    void xsetDescription(org.apache.xmlbeans.XmlString description);
    
    /**
     * Gets the "qualifiers" element
     */
    com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers getQualifiers();
    
    /**
     * Sets the "qualifiers" element
     */
    void setQualifiers(com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers qualifiers);
    
    /**
     * Appends and returns a new empty "qualifiers" element
     */
    com.hp.schemas.ucmdb._1.types.classmodel.Qualifiers addNewQualifiers();
    
    /**
     * Gets the "defaultValue" element
     */
    java.lang.String getDefaultValue();
    
    /**
     * Gets (as xml) the "defaultValue" element
     */
    org.apache.xmlbeans.XmlString xgetDefaultValue();
    
    /**
     * Sets the "defaultValue" element
     */
    void setDefaultValue(java.lang.String defaultValue);
    
    /**
     * Sets (as xml) the "defaultValue" element
     */
    void xsetDefaultValue(org.apache.xmlbeans.XmlString defaultValue);
    
    /**
     * An XML attrType(@http://schemas.hp.com/ucmdb/1/types/classmodel).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.classmodel.Attribute$AttrType.
     */
    public interface AttrType extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AttrType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("attrtypee3a3elemtype");
        
        org.apache.xmlbeans.StringEnumAbstractBase enumValue();
        void set(org.apache.xmlbeans.StringEnumAbstractBase e);
        
        static final Enum INTEGER = Enum.forString("INTEGER");
        static final Enum LONG = Enum.forString("LONG");
        static final Enum FLOAT = Enum.forString("FLOAT");
        static final Enum DOUBLE = Enum.forString("DOUBLE");
        static final Enum INTEGER_LIST = Enum.forString("INTEGER_LIST");
        static final Enum STRING = Enum.forString("STRING");
        static final Enum STRING_LIST = Enum.forString("STRING_LIST");
        static final Enum BYTES = Enum.forString("BYTES");
        static final Enum XML = Enum.forString("XML");
        static final Enum BOOLEAN = Enum.forString("BOOLEAN");
        static final Enum DATE = Enum.forString("DATE");
        static final Enum ENUM = Enum.forString("ENUM");
        static final Enum CMDB_LIST = Enum.forString("CMDB_LIST");
        
        static final int INT_INTEGER = Enum.INT_INTEGER;
        static final int INT_LONG = Enum.INT_LONG;
        static final int INT_FLOAT = Enum.INT_FLOAT;
        static final int INT_DOUBLE = Enum.INT_DOUBLE;
        static final int INT_INTEGER_LIST = Enum.INT_INTEGER_LIST;
        static final int INT_STRING = Enum.INT_STRING;
        static final int INT_STRING_LIST = Enum.INT_STRING_LIST;
        static final int INT_BYTES = Enum.INT_BYTES;
        static final int INT_XML = Enum.INT_XML;
        static final int INT_BOOLEAN = Enum.INT_BOOLEAN;
        static final int INT_DATE = Enum.INT_DATE;
        static final int INT_ENUM = Enum.INT_ENUM;
        static final int INT_CMDB_LIST = Enum.INT_CMDB_LIST;
        
        /**
         * Enumeration value class for com.hp.schemas.ucmdb._1.types.classmodel.Attribute$AttrType.
         * These enum values can be used as follows:
         * <pre>
         * enum.toString(); // returns the string value of the enum
         * enum.intValue(); // returns an int value, useful for switches
         * // e.g., case Enum.INT_INTEGER
         * Enum.forString(s); // returns the enum value for a string
         * Enum.forInt(i); // returns the enum value for an int
         * </pre>
         * Enumeration objects are immutable singleton objects that
         * can be compared using == object equality. They have no
         * public constructor. See the constants defined within this
         * class for all the valid values.
         */
        static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
        {
            /**
             * Returns the enum value for a string, or null if none.
             */
            public static Enum forString(java.lang.String s)
                { return (Enum)table.forString(s); }
            /**
             * Returns the enum value corresponding to an int, or null if none.
             */
            public static Enum forInt(int i)
                { return (Enum)table.forInt(i); }
            
            private Enum(java.lang.String s, int i)
                { super(s, i); }
            
            static final int INT_INTEGER = 1;
            static final int INT_LONG = 2;
            static final int INT_FLOAT = 3;
            static final int INT_DOUBLE = 4;
            static final int INT_INTEGER_LIST = 5;
            static final int INT_STRING = 6;
            static final int INT_STRING_LIST = 7;
            static final int INT_BYTES = 8;
            static final int INT_XML = 9;
            static final int INT_BOOLEAN = 10;
            static final int INT_DATE = 11;
            static final int INT_ENUM = 12;
            static final int INT_CMDB_LIST = 13;
            
            public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
                new org.apache.xmlbeans.StringEnumAbstractBase.Table
            (
                new Enum[]
                {
                    new Enum("INTEGER", INT_INTEGER),
                    new Enum("LONG", INT_LONG),
                    new Enum("FLOAT", INT_FLOAT),
                    new Enum("DOUBLE", INT_DOUBLE),
                    new Enum("INTEGER_LIST", INT_INTEGER_LIST),
                    new Enum("STRING", INT_STRING),
                    new Enum("STRING_LIST", INT_STRING_LIST),
                    new Enum("BYTES", INT_BYTES),
                    new Enum("XML", INT_XML),
                    new Enum("BOOLEAN", INT_BOOLEAN),
                    new Enum("DATE", INT_DATE),
                    new Enum("ENUM", INT_ENUM),
                    new Enum("CMDB_LIST", INT_CMDB_LIST),
                }
            );
            private static final long serialVersionUID = 1L;
            private java.lang.Object readResolve() { return forInt(intValue()); } 
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType newValue(java.lang.Object obj) {
              return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType) type.newValue( obj ); }
            
            public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType newInstance() {
              return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute.AttrType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute newInstance() {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.classmodel.Attribute parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.classmodel.Attribute) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
