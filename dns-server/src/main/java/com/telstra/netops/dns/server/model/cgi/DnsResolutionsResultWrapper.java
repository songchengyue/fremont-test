package com.telstra.netops.dns.server.model.cgi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telstra.netops.dns.server.model.DnsResolution;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DnsResolutionsResultWrapper {
	
	@JsonProperty
	private List<DnsResolution> result;

	public List<DnsResolution> getResult() {
		return result;
	}

	public void setResult(List<DnsResolution> result) {
		this.result = result;
	}	

}
