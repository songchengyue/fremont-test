/*
 * XML Type:  ClassModelQueryGetClassModelIconPathsResponse
 * Namespace: http://schemas.hp.com/ucmdb/ui/1/params
 * Java type: com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.ui._1.params.impl;
/**
 * An XML ClassModelQueryGetClassModelIconPathsResponse(@http://schemas.hp.com/ucmdb/ui/1/params).
 *
 * This is a complex type.
 */
public class ClassModelQueryGetClassModelIconPathsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse
{
    
    public ClassModelQueryGetClassModelIconPathsResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RELATIVEPATHS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/ui/1/params", "relativePaths");
    
    
    /**
     * Gets the "relativePaths" element
     */
    public com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons getRelativePaths()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons target = null;
            target = (com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons)get_store().find_element_user(RELATIVEPATHS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "relativePaths" element
     */
    public void setRelativePaths(com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons relativePaths)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons target = null;
            target = (com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons)get_store().find_element_user(RELATIVEPATHS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons)get_store().add_element_user(RELATIVEPATHS$0);
            }
            target.set(relativePaths);
        }
    }
    
    /**
     * Appends and returns a new empty "relativePaths" element
     */
    public com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons addNewRelativePaths()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons target = null;
            target = (com.hp.schemas.ucmdb.ui._1.types.ClassNameAndIcons)get_store().add_element_user(RELATIVEPATHS$0);
            return target;
        }
    }
}
