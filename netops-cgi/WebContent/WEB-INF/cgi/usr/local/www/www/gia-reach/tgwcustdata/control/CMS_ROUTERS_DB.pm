# (C) Telstra 2001
#
# Author: Ash Garg (ash.garg@telstra.net)
# Date: 10 Dec 2001
#
# $Id: CMS_ROUTERS_DB.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $

###################################################################
# File        : CMS_ROUTERS_DB.pm
# Description : Executes functions called from CMS_ROUTERS.pm and
#               interacts with tables in database for data queries. 
# Parameters to be passed and usage :
# Output of the script
# Input File  : CMS_ROUTERS.pm
# Output File :
# Author      : KETAKI THOMBARE (Infosys Tech. Ltd., Bangalore )
# Started On  : 16 July 2009
# Modification History :
#  Date               Name             Change/Description
# ------------------------------------------------------------------
# 15Jul2009    KETAKI THOMBARE     Baseline for VPLS  Telstra alarm CR.
# 15Jul2009    KETAKI THOMBARE     Added for VPLS  Telstra alarm CR.
# 28Dec2009    Rahul Krishnan	   Country Code update
####################################################################


package CMS_ROUTERS_DB;
use Exporter;
use strict;
use DBI;
use CMS_DB;

#CR-84 Juniper IPVPN Report
use IO::Socket::INET;

our @ISA	= qw(Exporter);

# Error Codes
use constant SUCCESS => 1;
use constant ERROR => undef;

our $error;
#my $dbh;
my $database="CMS_ROUTERS_DB";
my $dbuser="postgres";
my $host="reachdev.telstra.net";
my $debug = 1;
#to disbale debug statements, uncomment following line and vice versa
#$debug = 0;
if ($debug) {open (IPT, ">>/data1/tmp_log/routerslog.log");}

# connects to database
sub connect_to_database
{
        if (defined ($dbh))
        {
                return $dbh;
        }
        else
        {
                $dbh = DBI->connect("dbi:Pg:dbname=$database","cms");
		
        }
}

# disconnect from database
sub disconnect_from_database
{
        $dbh->disconnect;
        undef $dbh;
}

sub edit_router {
	# ++EVPL
	my ($routername, $community, $cos, $det_poll, $pre_upd, $cos_v, $cos_e, $router_switch) = @_;
	# --EVPL
	if (!defined $routername)
        {
                $error="I need a router name to edit!";
                return ERROR;
        }

	$routername =~ tr/A-Z/a-z/;
	##check if the router exists
        my $sth = $dbh->prepare("select routerid from RouterName where routername = '$routername'");
	$sth->execute;
        my $routerid = $sth->fetchrow_array();
        $sth->finish();
        if($routerid == 0)
        {
		$error="Router does not exist ($DBI::errstr)";
		return ERROR;
	}
	
	#update the router details

        $cos = ($cos) ? $cos : 0;
	$cos_v = ($cos_v) ? $cos_v : 0;
	# ++EVPL
	$cos_e = ($cos_e) ? $cos_e : 0;
	# --EVPL
	$det_poll = ($det_poll) ? $det_poll : 0;
	# ++EVPL
	$router_switch =($router_switch) ? $router_switch :0;
        if (defined $community) {
          	$sth = $dbh->prepare("UPDATE RouterName SET
					community=?, qos=?, detail=?, prefix_update=?, vpls=?, evpl=?,router_switch=?
					WHERE routerid = ?");
		$sth->execute($community, $cos, $det_poll, $pre_upd, $cos_v, $cos_e, $router_switch, $routerid);
        } else {
          	$sth = $dbh->prepare("UPDATE RouterName SET
					qos=?, vpls=?, evpl=?, detail=?, prefix_update=? router_switch=?
					WHERE routerid = ?");
		$sth->execute($cos, $cos_v, $cos_e, $det_poll, $pre_upd, $router_switch, $routerid);
        }
	# --EVPL

        #$sth->execute;
        $error=$DBI::errstr;
        $error;
	if ($debug){print IPT "DB EDIT query:UPDATE RouterName SET qos=$cos,vpls=$cos_v,evpl=$cos_e,detail=$det_poll,prefix_update=$pre_upd,router_switch=$router_switch WHERE routerid = $routerid\n";}
        return $routerid;


}

sub add_router
{
	# ++ EVPL
	my($routername, $popname, $community, $router_switch, $cos, $cos_v, $cos_e, $det_poll, $pre_upd) = @_;
	# --EVPL

	if (!defined $routername || !defined $popname) 
	{
		$error="I need a router name and pop name to insert into the database!!";
		return ERROR;
	}

 	$routername =~ tr/A-Z/a-z/;       

	##check if the router exists
        my $sth = $dbh->prepare("select routerid from RouterName where routername = '$routername'");
        $sth->execute;
        my $routerid = $sth->fetchrow_array();
        $sth->finish();
	if($routerid!=0)
	{
        	
		$error="Router already exists in the database $DBI::errstr";
		return ERROR;
	}

	##check if the pop exists
        my $sth = $dbh->prepare("select popid from PopName where popname ilike '%$popname%'");
        $sth->execute;
        my $popid = $sth->fetchrow_array();
        $sth->finish();
	if($popid==0)
	{
        	
		$error="Pop does not exist in the database";
		return ERROR;
	}

	#does not exist so add it in
        my $sth = $dbh->prepare("select nextval ('routername_seq')");
        $sth->execute;
        my $routerid = $sth->fetchrow_array();
        $sth->finish();

	$cos = ($cos) ? $cos : 0;
	$cos_v = ($cos_v) ? $cos_v : 0;
	# ++ EVPL
	$cos_e = ($cos_e) ? $cos_e : 0;
	# --EVPL
	$det_poll = ($det_poll) ? $det_poll : 0;
	# ++EVPL
	#Inserting valus into database.
	if (defined $community) {
	  $sth = $dbh->prepare("insert into RouterName
				(routerid, routername, popid, community, router_switch, qos, detail, prefix_update, vpls, evpl) 
				values (?,?,?,?,?,?,?,?,?,?)");
	  $sth->execute($routerid,$routername,$popid,$community,$router_switch,$cos,$det_poll,$pre_upd,$cos_v,$cos_e);
	} else {
	  $sth = $dbh->prepare("insert into RouterName
				(routerid, routername, popid, router_switch, qos, detail, prefix_update, vpls, evpl) 
				values(?,?,?,?,?,?,?,?,?)");
	  $sth->execute($routerid, $routername, $popid, $router_switch, $cos, $det_poll, $pre_upd, $cos_v, $cos_e);
	}
	# --EVPL

	#CR-84 Juniper IPVPN Report
	$error=$DBI::errstr;
        $error;
        $sth->finish;

        #system("/usr/bin/perl /usr/local/www/wanchai/cms/scripts/update_routeros_ipvpn.pl $routername $community");
        my @result = ();
        my $remote = $OS_PARA::values{gia_network_report_host}{value};
        my $socket = IO::Socket::INET->new(PeerAddr => $remote,
		PeerPort => 50000,
		Proto => "tcp",
		Timeout => 90,
		Type => SOCK_STREAM) or die "ERROR in Socket Creation : $!\n";

        my $cmd = $routername.','.$community;
        print $socket "$cmd\n";
        while(my $l = <$socket>) {
                push @result, $l;
        }
        close($socket);
        my $returned_router_os = $result[0];
        chomp($returned_router_os);
        if($returned_router_os =~ m/[a-z]/i){
                $sth = $dbh->prepare("insert into router_software (routername,router_software) values (?,?)");
                $sth->execute($routername,$returned_router_os);
        }else {
                $sth = $dbh->prepare("insert into router_software (routername) values (?)");
                $sth->execute($routername);
        }
        $sth->finish;

	# IPT Ph5 Bug fix #
	#$sth = $dbh->prepare("insert into router_software (routername) values (?)");
	#$sth->execute($routername);
	#$sth->finish;
	
	#$error=$DBI::errstr;
	#$error;
        
        return $routerid;
}



sub add_pop
{
	my($popname, $cityname) = @_;

	if (!defined $cityname || !defined $cityname) 
	{
		$error="I need a pop name and city name to insert into the database!!";
		return ERROR;
	}

        

        ##check if the name exists
        my $sth = $dbh->prepare("select popid from PopName where popname ilike '%$popname%'"
);
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();
        if($id!=0)
        {
        	
		$error="pop name already exists in the database!! $DBI::errstr";
                return ERROR;
        }

	##check if the city exists
        my $sth = $dbh->prepare("select cityid from CityName where cityname ilike '%$cityname%'");
        $sth->execute;
        my $cityid = $sth->fetchrow_array();
        $sth->finish();
	if($cityid==0)
	{
        	
		$error="City does not exist in the database  $DBI::errstr";
		return ERROR;
	}

	#does not exist so add it in

        my $sth = $dbh->prepare("select nextval ('popname_seq')");
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();

        my $sth = $dbh->prepare("insert into PopName(popid, popname, cityid) values('$id','$popname','$cityid')");
        my $return=$sth->execute;

        
        return $id;
}

sub add_city
{
	
	my($cityname, $countryname, $regionid) = @_;
	open (CCODE, ">/tmp/add_city"); 	
	print CCODE "\n region name is $regionid";
	print CCODE "\n country name is $countryname";

	if (!defined $cityname | !defined $countryname) 
	{
		$error="I need a city name and region name to insert into the database!!";
		return ERROR;
	}

        
        ##check if the name exists
        my $sth = $dbh->prepare("select cityid from CityName where cityname ilike '%$cityname%'");
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();
        if($id!=0)
        {
                
		$error="City does not exist in the database  $DBI::errstr";
		return ERROR;
        }

	##check if the country exists. Added for country code
        my $sth = $dbh->prepare("select countryid from country_region where countryname ilike '%$countryname%'");
        $sth->execute;
        my $countryid = $sth->fetchrow_array();
        $sth->finish();
	if($countryid==0)
	{
        	
		$error="Country does not exist in the database  $DBI::errstr";
		return ERROR;
	}

        #does not exist so add it in

        my $sth = $dbh->prepare("select nextval ('cityname_seq')");
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();
	
	#added for country code. new table created to map country id and region id.
	my $sth_regid = $dbh->prepare("select regionid from country_region where countryname ilike '%$countryname%'");
	$sth_regid->execute;
	my $regid = $sth_regid->fetchrow_array();
	$sth_regid->finish();
	#EoC
	
        my $sth = $dbh->prepare("insert into CityName(cityid, cityname, regionid) values('$id','$cityname','$regid')");
        my $return=$sth->execute;
	
	#Added for country code 
	#my $sth1 = $dbh->prepare("select count(*) from countryname where countryname like '%$countryname%'");
	#$sth1->execute;
	#my $id1 = $sth1->fetchrow_array();
	#$sth1->finish();
	#print CCODE " \n number of rows is $id1\n";
	#if($id1==1)
	#{
		my $sth2 = $dbh->prepare("select min(cityid) from countryname where countryid= $countryid");
		$sth2->execute;
		my $id2 = $sth2->fetchrow_array();
		$sth2->finish();
		if($id2==0)
		{
			my $sth3 = $dbh->prepare("update countryname set cityid = $id where countryid= $countryid");
			$sth3->execute;
			print CCODE "\n execute $sth3\n";

		}
	#}
	else
	{
		my $sth4 = $dbh->prepare("select countrycode from countryname_code where countryname ilike '%$countryname%'");
		$sth4->execute;
		my $code4 = $sth4->fetchrow_array();
		$sth4->finish();
		print CCODE "\n country code is $code4";
		print CCODE "\n query is select countrycode from countryname_code where countryname ilike '%$countryname%'";
		my $sth5 = $dbh->prepare("insert into countryname(countryid, countryname, cityid) values ('$countryid','$code4','$id')");
		$sth5->execute;
	}
	# EoC
        
        return $id;
}

# Added for country code update
sub add_country
{
	my($countryname, $regionname) = @_;
 	open (CCODE1, ">/tmp/add_country");
	print CCODE1 "\n region name is $regionname";	
	if (!defined $countryname | !defined $regionname)
        {
              $error="I need a country name and region name to insert into the database!!";
              return ERROR;
        }
        ##check if the name exists check with countryname_code table for two letter code
        my $sth = $dbh->prepare("select countryid from country_region where countryname ilike '%$countryname%'");
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();
  	if($id!=0)
	{
		$error="Country already exists in the database  $DBI::errstr";
		return ERROR;
	}

	##check if the region exists
	my $sth = $dbh->prepare("select regionid from RegionName where regionname ilike '%$regionname%'");
        $sth->execute;
        my $regionid = $sth->fetchrow_array();
        $sth->finish();
        if($regionid==0)
	{
		
		$error="Region does not exist in the database  $DBI::errstr";
		return ERROR;
	}
 
	#does not exist so add it in
	my $sth2 = $dbh->prepare("select nextval ('countryname_seq')");
	$sth2->execute;
	my $id2 = $sth2->fetchrow_array();
	$sth2->finish();
	
	#select two letter country code	
	my $sth_cd = $dbh->prepare("select countrycode from countryname_code where countryname ilike '%$countryname%'");
	$sth_cd->execute;
	my $cd = $sth_cd->fetchrow_array();
	$sth_cd->finish();
	print CCODE1 "\n query is select countrycode from countryname_code where countryname ilike '%$countryname%'";
	print CCODE1 "\n country code and counter value is $countryname and $cd and $id2\n";	
	if(!$cd)
		{
			$error="Country code not found. Please contact CMS support $DBI::errstr";
			return ERROR;
		}

print CCODE1 "\n country code and counter value is $countryname and $cd and $id2\n";
print CCODE1 "\n query is select countrycode from countryname_code where countryname ilike '%$countryname%'";	
	my $sth = $dbh->prepare("insert into countryname(countryid, countryname, cityid) values('$id2','$cd',0)");
	my $return=$sth->execute;
 	
	my $sth1 = $dbh->prepare("insert into country_region(countryid,countryname,regionid) values ('$id2','$countryname', '$regionid')");
	$sth1->execute;
 
	return $id2;
}

#EoC
sub add_region
{
	my($name) = @_;

	if (!defined $name) 
	{
		$error="I need a region name to insert into the database!!";
		return ERROR;
	}

        
        ##check if the name exists
        my $sth = $dbh->prepare("select regionid from RegionName where regionname ilike '%$name%'"
);
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();
        if($id!=0)
        {
                
                return $id
        }

        #does not exist so add it in

        my $sth = $dbh->prepare("select nextval ('regionname_seq')");
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();

        my $sth = $dbh->prepare("insert into RegionName(regionid, regionname) values('$id','$name')");
        my $return=$sth->execute;

        
        return $id;
}

sub get_routertype {
        my $sth = $dbh->prepare("SELECT router_type FROM routertype");      

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_allroutertype {
        my $sth = $dbh->prepare("SELECT * FROM routertype");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_router
{
	my($type, $name) = @_;

	#check if the type is valid	
        
        my $sth = $dbh->prepare("select typeid from type where typename ilike '%$type%'");
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();
        if($id==0)
        {
                
                $error="Type has not been adequately defined!!!";
                return ERROR;
        }

	if($id==5)
	{
		#All
		#CR-84(Tactical) solution on IPVPN Juniper
		my $sth = $dbh->prepare("select routername, popname, community, cityname, regionname, qos, detail, prefix_update, vpls, evpl,router_software from RouterName natural join PopName natural join CityName natural join RegionName natural join router_software order by routername");
        	$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;
	}

	if($id==4)
	{
		#Region
		my $sth = $dbh->prepare("select routername, popname, cityname, regionname from RouterName natural join PopName natural join CityName natural join RegionName where regionname ilike '%$name%'");
		$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;
	}


	if($id==3)
	{
		#City
		my $sth = $dbh->prepare("select routername, popname, cityname from RouterName natural join PopName natural join CityName where cityname ilike '%$name%'");
		$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;

	}

	if($id==2)
	{
		#pop
		my $sth = $dbh->prepare("select routername, popname from RouterName natural join PopName where popname ilike '%$name%'");

		$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;
	}

	if($id==1)
	{
		#Router
		my $sth = $dbh->prepare("select routername, popname, cityname, regionname from RouterName natural join PopName natural join CityName natural join RegionName where routername ilike '%$name%' order by routername");

		$sth->execute;
		my $rname = $sth->fetchrow_array();
		return $rname;
	}
	
}

sub get_region
{
	my($type, $name) = @_;

	#check if the type is valid	
        
        my $sth = $dbh->prepare("select typeid from type where typename ilike '%$type%'");
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();
        if($id==0)
        {
                
                $error="Type has not been adequately defined!!!";
                return ERROR;
        }

	if($id==5)
	{
		#All
		my $sth = $dbh->prepare("select regionname from RegionName");	
        	$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;
	}

	if($id==4)
	{
		#Region
		my $sth = $dbh->prepare("select regionname from RegionName where RegionName.regionname ilike '%$name%'");

		$sth->execute;
		my $rname = $sth->fetchrow_array();
		return $rname;

	}
	
}

sub get_city
{
	my($type, $name) = @_;

	#check if the type is valid	
        
        my $sth = $dbh->prepare("select typeid from type where typename ilike '%$type%'");
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();
        if($id==0)
        {
                
                $error="Type has not been adequately defined!!!";
                return ERROR;
        }

	if($id==5)
	{
		#All
		my $sth = $dbh->prepare("select cityname, regionname from CityName natural join RegionName");
        	$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;
	}

	if($id==4)
	{
		#Region
		my $sth = $dbh->prepare("select cityname, regionname from CityName natural join RegionName where regionname ilike '%$name%'");
		$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;
	}


	if($id==3)
	{
		#City
		my $sth = $dbh->prepare("select cityname from CityName where CityName.cityname ilike '%$name%'");

		$sth->execute;
		my $rname = $sth->fetchrow_array();
		return $rname;

	}
	
}

sub get_pop
{
	my($type, $name) = @_;

	#check if the type is valid	
        
        my $sth = $dbh->prepare("select typeid from type where typename ilike '%$type%'");
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();
        if($id==0)
        {
                
                $error="Type has not been adequately defined!!!";
                return ERROR;
        }

	if($id==5)
	{
		#All
		my $sth = $dbh->prepare("select popname, cityname, regionname from PopName natural join CityName natural join RegionName");
        	$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;
	}

	if($id==4)
	{
		#Region
		my $sth = $dbh->prepare("select popname, cityname, regionname from PopName natural join CityName natural join RegionName where regionname ilike '%$name%'");
		$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;
	}


	if($id==3)
	{
		#City
		my $sth = $dbh->prepare("select popname, cityname from PopName natural join CityName where cityname ilike '%$name%'");
		$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;

	}

	if($id==2)
	{
		#pop
		my $sth = $dbh->prepare("select popname from PopName where PopName.popname ilike '%$name%'");

		$sth->execute;
		my $rname = $sth->fetchrow_array();
		return $rname;

	}
	
}

# Added for country code update
sub get_country
{
	open(DEBUG,">/tmp/get_country");
	my($type, $name) = @_;
	print DEBUG "\n type and name are $type, $name";
	#check if the type is valid

	my $sth = $dbh->prepare("select typeid from type where typename ilike '%$type%'");
	$sth->execute;
	my $id = $sth->fetchrow_array();
	$sth->finish();
	print DEBUG "\n typeid is $id";
	if($id==0)
	{
		
		$error="Type has not been adequately defined!!!";
		return ERROR;
	
	}

	if($id==5)
	{
	
		#All
		print DEBUG "\n inside ALL";
		my $sth = $dbh->prepare("select countryname, regionname from country_region a, regionname c where a.regionid = c.regionid");
	 	$sth->execute;
		print DEBUG "\n query is select countryname, regionname from country_region a, regionname c where a.regionid = c.regionid"; 	
		my $array_ref = $sth->fetchall_arrayref();
		$sth->finish();
		return $array_ref;
	}	

	if($id==4)
	{
	
		#Region
		my $sth = $dbh->prepare("select distinct countryname, regionname from country_region a, regionname c where a.regionid = c.regionid and c.regionname ilike '%$name%'");
		$sth->execute;
		my $array_ref = $sth->fetchall_arrayref();
		return $array_ref;
	}

	if($id==3)
	{
		
		#Country
		my $sth = $dbh->prepare("select distinct countryname from country_region a where a.countryname ilike '%$name%'");
		$sth->execute;
		my $rname = $sth->fetchrow_array();
		return $rname;
	}

}
# End change

sub delete_router
{
open (DEBUG,">/tmp/delete_router");
	my($name) = @_;
	my $sth = $dbh->prepare("delete from RouterName where routername ilike '$name%'");
	my $result= $sth->execute;
	$error=$DBI::errstr;
	return $result;
}

sub delete_pop
{
	my($name) = @_;
	my $sth = $dbh->prepare("delete from PopName where popname ilike '%$name%'");
	my $result= $sth->execute;
	$error=$DBI::errstr;
	return $result;
}

sub delete_city
{
	open (DEBUG1,">/tmp/delete_city");
	my($name) = @_;
	print DEBUG1 "\n city name is $name";
	#Added for Country code
	my $sth = $dbh->prepare("select cityid from cityname where cityname ilike '%$name%'");
	print DEBUG1 "query is select cityid from cityname where cityname ilike '%$name%'";
	$sth->execute;
	my $id = $sth->fetchrow_array();
	$sth->finish();
	print DEBUG1 "\n city id is $id\n";

	my $sth2 = $dbh->prepare("select count(*) from countryname where countryid = (select countryid from countryname where cityid =$id)");
	$sth2->execute;
	my $id2 = $sth2->fetchrow_array();
	$sth2->finish();
	print DEBUG1 "\n count of country is $id2";
	
	if($id2 > 1)
	{
		my $sth3 = $dbh->prepare("delete from countryname where cityid = $id");
		print DEBUG1 "\n query is delete from countryname where cityid = $id";
		$sth3->execute;
	}
	else
	{
		my $sth4 = $dbh->prepare("update countryname set cityid = 0 where cityid = $id");
		$sth4->execute;
	}	
	#EoC The above code will delete rows from countyrname also since there is a linkage to cityid

	my $sth1 = $dbh->prepare("delete from CityName where cityname ilike '%$name%'");
	my $result1= $sth1->execute;
	$error=$DBI::errstr;
	return $result1;
}

#Added for Country Code changes
sub delete_country
{
	my($name) = @_;
 	open (DEBUG2,">/tmp/delete_country");
	print DEBUG2 "\n country name is $name";

	my $sth_chk = $dbh->prepare("select distinct a.countryid from countryname a,country_region b where a.countryid = b.countryid and b.countryname ilike '%$name%'");
	$sth_chk->execute;
	my $chk = $sth_chk->fetchrow_array();
	$sth_chk->finish();
	print DEBUG2 "\n country code is $chk";
	#if($chk == 0) 
	#{
	#	$error="No country found. Please check country name. $DBI::errstr";
	#	return ERROR;
	#}
	if($chk!=0){ 	
	my $sth = $dbh->prepare("select max(cityid) from countryname where countryid = $chk");
	$sth->execute;
	my $id = $sth->fetchrow_array();
	print DEBUG2 "\n query is select max(cityid) from countryname where countryid = $chk";
	$sth->finish();
 	print DEBUG2 "\n city id is $id\n";	

	if($id ==0)
	{
		my $sth1 = $dbh->prepare("delete from countryname where countryid =$chk");
		my $result= $sth1->execute;
		print DEBUG2 "\n query is delete from countryname where countryid =$chk";
	
		my $sth2 = $dbh->prepare("delete from country_region where countryname ilike '%$name%'");
		my $result2= $sth2->execute;
		$error=$DBI::errstr;
		return $result2;
	}
	else
	{
		$error="Cities are present for this country. Please delete all cities before deleting country  $DBI::errstr";
		return ERROR;
	}
	}
	else
	{
	$error="No country found. Please check country name. $DBI::errstr";
	return ERROR;
	}
		
}
#EoC
sub delete_region
{
	my($name) = @_;
	my $sth = $dbh->prepare("delete from RegionName where regionname ilike '%$name%'");
	my $result= $sth->execute;
	$error=$DBI::errstr;
	return $result;
}

sub auto_update
{
	my($router) = @_;
	my $sth = $dbh->prepare("select prefix_update from routername where routername ilike '$router'");
	$sth->execute;
	my $res = ($sth->fetchrow_array)[0];
	$sth->finish;
	return $res;
}


sub get_router_detail
{
	my($type, $name) = @_;

	#check if the type is valid	
        
        my $sth = $dbh->prepare("select typeid from type where typename ilike '%$type%'");
        $sth->execute;
        my $id = $sth->fetchrow_array();
        $sth->finish();

	my $sql_begin = 
		"SELECT c.routername, c.routerid, d.popname, c.popid, e.cityname, e.cityid, f.countryname, f.countryid, g.regionname, g.regionid
		, c.community, c.qos, c.detail, c.prefix_update, c.vpls, c.router_switch
		FROM routername c, popname d, cityname e LEFT OUTER JOIN countryname f ON (e.cityid = f.cityid)
		, regionname g
		WHERE
		c.routerid is not null
		and c.popid = d.popid  
		and d.cityid = e.cityid
		and e.regionid = g.regionid ";
	my $sql_end = " order by c.routername";

        if($id==0)
        {
                $error="Type has not been adequately defined!!!";
                return ERROR;
        }

	if($id==6)
	{
		#Country
		$sql_begin .= " and f.countryname ilike '%$name%' ";
	}

	if($id==5)
	{
		#All
		# nothing to do
	}

	if($id==4)
	{
		#Region
		$sql_begin .= " and g.regionname ilike '%$name%' ";
	}


	if($id==3)
	{
		#City
		$sql_begin .= " and e.cityname ilike '%$name%' ";
	}

	if($id==2)
	{
		#pop
		$sql_begin .= " and d.popname ilike '%$name%' ";
	}

	if($id==1)
	{
		#Router
		$sql_begin .= " and c.routername ilike '%$name%' ";
	}

	my $sth = $dbh->prepare("$sql_begin$sql_end");
	$sth->execute;
	my $rname = $sth->fetchall_arrayref();
	return $rname;
	
}

sub get_routersw {
        my $sth = $dbh->prepare("select routername.routername, popname, community, cityname, regionname, qos, detail, prefix_update, vpls, evpl, router_switch, router_software from RouterName natural join PopName natural join CityName natural join RegionName LEFT OUTER JOIN router_software ON (router_software.routername = routername.routername) order by routername.routername");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return $error;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return $error;
        }
}

#CR-84 Juniper IPVPN Report
sub get_routeros {
  my $service_id = shift @_;
  my $sql_command = "select router_software from router_software join routername on router_software.routername = routername.routername left outer join link on routername.routerid = link.routerid where link.serviceid like '$service_id'";
  my $sth = $dbh->prepare ($sql_command);
  unless ($sth->execute) {
          $error = $dbh->errstr;
          return $error;
  }
  my $result = ($sth->fetchrow_array)[0];
  $sth->finish;
  return $result;
}


###############################################################################
# Name		:fetch_routers
# Author	:Karuna Ballal
# Description	:fetches the router details as a string for updation on the Edit Router screen, IP Transit ph5
# Input		:call from the front end
# Output	:comma separated router details - routername, popname, community, cityname, regionname, qos, detail, prefix_update, vpls, evpl
###############################################################################
sub fetch_routers {
		if ($debug){print IPT "DB entered fetch_routers\n";}
		##fetch details from the DB
                my $sth = $dbh->prepare("select routername, popname, community, cityname, regionname, qos, detail, prefix_update, vpls, evpl from RouterName natural join PopName natural join CityName natural join RegionName order by routername");
		unless ($sth->execute) {
                $error = $dbh->errstr;
		}
		my @db_vals = @{$sth->fetchall_arrayref()};
		if ($error){
			if ($debug){print IPT "DB error:$error\n";}
			return $error;
		}
		my (@new_array,$str_ret);
		foreach my $array (@db_vals){
			#if ($debug){print IPT "DB array:$$array[0]\n";}
			##Join details with comma
			my $str_vals = join ",",@$array;
			#if ($debug){print IPT "DB str_vals:$str_vals\n";}
			push (@new_array,$str_vals);
			##join each string with semi colon
			$str_ret = join ":",@new_array;
			#if ($debug){print IPT "DB str_ret:$str_ret\n";} 
		}
		$sth->finish();
		return $str_ret;
}

###############################################################################
# Name          :fetch_routers
# Author        :Karuna Ballal
# Description   :fetches the router details as a string for updation on the Edit Router screen, IP Transit ph5
# Input         :call from the front end
# Output        :comma separated router details - routername, popname, community, cityname, regionname, qos, detail, prefix_update, vpls, evpl
###############################################################################
sub get_router_switch {
	if ($debug){print IPT "DB entered get_router_switch \n";}
        ##fetch details from the DB
	my $routerid = shift @_;
	if ($debug){print IPT "DB routerid:$routerid\n";}
        my $sth = $dbh->prepare("select router_switch,router_software from RouterName,router_software WHERE routername.routername = router_software.routername AND routername.routername = '$routerid'");
        #my $sth = $dbh->prepare("select router_switch from RouterName WHERE routername = '1142'");
	unless ($sth->execute) {
                $error = $dbh->errstr;
        }
	if ($error){
		return $error;
	}
	#my @switch_os = @{$sth->fetchall_arrayref()};
	#my $switch_os = join ",",@switch_os;
	my @router_switch = $sth->fetchrow_array();
	#if ($debug){print IPT "DB get_router_switch router_switch:$switch_os\n";}
	my $router_switch = join ",",@router_switch;
	if ($debug){print IPT "DB get_router_switch router_switch:$router_switch\n";}
	return $router_switch;
	#return $switch_os;
}
1;

