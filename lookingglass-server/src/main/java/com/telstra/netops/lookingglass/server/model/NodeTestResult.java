package com.telstra.netops.lookingglass.server.model;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NodeTestResult {

	@JsonProperty("date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "EEE MMM d HH:mm:ss yyyy z")
	// e.g. Tue May 24 23:20:46 2016 GMT
	private Date date;

	@JsonProperty("MinLatency")
	private Float minLatency;

	@JsonProperty("MaxLatency")
	private Float maxLatency;

	@JsonProperty("AvgLatency")
	private Float avgLatency;

	@JsonProperty("StdDeviation")
	private Float stdDeviation;

	@JsonProperty("AvgLoss")
	private Float avgLoss;

	@JsonProperty("result1")
	private String result1;
	
	@JsonProperty("result2")
	private String result2;
	
	@JsonProperty("result3")
	private String result3;
	
	@JsonProperty("result4")
	private String result4;
	
	@JsonProperty("result5")
	private String result5;
	
	@JsonProperty("result6")
	private String result6;
	
	@JsonProperty("result7")
	private String result7;
	
	@JsonProperty("result8")
	private String result8;
	
	@JsonProperty("result9")
	private String result9;
	
	@JsonProperty("result10")
	private String result10;

	@JsonProperty("result11")
	private String result11;
	
	@JsonProperty("result12")
	private String result12;
	
	@JsonProperty("result13")
	private String result13;
	
	@JsonProperty("result14")
	private String result14;
	
	@JsonProperty("result15")
	private String result15;
	
	@JsonProperty("result16")
	private String result16;
	
	@JsonProperty("result17")
	private String result17;
	
	@JsonProperty("result18")
	private String result18;
	
	@JsonProperty("result19")
	private String result19;
	
	@JsonProperty("result20")
	private String result20;
	
	public NodeTestResult() {
	}

	public NodeTestResult(Date date, Float minLatency, Float maxLatency,
			Float avgLatency, Float stdDeviation, Float avgLoss, String result1, 
			String result2, String result3, 
			String result4, String result5, String result6, 
			String result7, String result8, String result9, String result10,
			String result11, 
			String result12, String result13, 
			String result14, String result15, String result16, 
			String result17, String result18, String result19, String result20) {
		super();
		this.date = date;
		this.minLatency = minLatency;
		this.maxLatency = maxLatency;
		this.avgLatency = avgLatency;
		this.stdDeviation = stdDeviation;
		this.avgLoss = avgLoss;
		this.result1=result1;
		this.result2=result2;
		this.result3=result3;
		this.result4=result4;
		this.result5=result5;
		this.result6=result6;
		this.result7=result7;
		this.result8=result8;
		this.result9=result9;
		this.result10=result10;
		this.result11=result11;
		this.result12=result12;
		this.result13=result13;
		this.result14=result14;
		this.result15=result15;
		this.result16=result16;
		this.result17=result17;
		this.result18=result18;
		this.result19=result19;
		this.result20=result20;		
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Float getMinLatency() {
		return minLatency;
	}

	public void setMinLatency(Float minLatency) {
		this.minLatency = minLatency;
	}

	public Float getMaxLatency() {
		return maxLatency;
	}

	public void setMaxLatency(Float maxLatency) {
		this.maxLatency = maxLatency;
	}

	public Float getAvgLatency() {
		return avgLatency;
	}

	public void setAvgLatency(Float avgLatency) {
		this.avgLatency = avgLatency;
	}

	public Float getStdDeviation() {
		return stdDeviation;
	}

	public void setStdDeviation(Float stdDeviation) {
		this.stdDeviation = stdDeviation;
	}

	public Float getAvgLoss() {
		return avgLoss;
	}

	public void setAvgLoss(Float avgLoss) {
		this.avgLoss = avgLoss;
	}
	public String getResult1() {
		return result1;
	}

	public void setResult1(String result1) {
		this.result1 = result1;
	}

	public String getResult2() {
		return result2;
	}

	public void setResult2(String result2) {
		this.result2 = result2;
	}

	public String getResult3() {
		return result3;
	}

	public void setResult3(String result3) {
		this.result3 = result3;
	}

	public String getResult4() {
		return result4;
	}

	public void setResult4(String result4) {
		this.result4 = result4;
	}

	public String getResult5() {
		return result5;
	}

	public void setResult5(String result5) {
		this.result5 = result5;
	}

	public String getResult6() {
		return result6;
	}

	public void setResult6(String result6) {
		this.result6 = result6;
	}

	public String getResult7() {
		return result7;
	}

	public void setResult7(String result7) {
		this.result7 = result7;
	}

	public String getResult8() {
		return result8;
	}

	public void setResult8(String result8) {
		this.result8 = result8;
	}

	public String getResult9() {
		return result9;
	}

	public void setResult9(String result9) {
		this.result9 = result9;
	}

	public String getResult10() {
		return result10;
	}

	public void setResult10(String result10) {
		this.result10 = result10;
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (obj == this) {
			return true;
		}

		if (obj.getClass() != getClass()) {
			return false;
		}

		NodeTestResult rhs = (NodeTestResult) obj;
		return EqualsBuilder.reflectionEquals(this, rhs);
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
