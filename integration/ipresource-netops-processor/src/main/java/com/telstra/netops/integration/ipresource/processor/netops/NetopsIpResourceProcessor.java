package com.telstra.netops.integration.ipresource.processor.netops;

import org.springframework.stereotype.Component;

import com.telstra.netops.integration.ipresource.processor.IpResource;
import com.telstra.netops.integration.ipresource.processor.IpResourceProcessor;

@Component
public class NetopsIpResourceProcessor implements IpResourceProcessor {

	@Override
	public void process(IpResource ipResource) {
		System.out.println("Netops is processing " + ipResource);
	}

}
