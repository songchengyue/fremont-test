package com.telstra.oauth2.server.saml1;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.opensaml.DefaultBootstrap;
import org.opensaml.saml1.core.Assertion;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

@Component
public class XMLAssertionUnmarshaller {

	/**
	 * Given the XML, unmarshall to an assertion object.
	 * Should be in org.springframework.security.saml.util.SAMLUtil!
	 * 
	 * @param in
	 * @return
	 * @throws XMLParserException 
	 * @throws UnmarshallingException 
	 * @throws ConfigurationException 
	 */
	public Assertion unmarshallAssertion(InputStream in) {
		try {
			
			//
			// From https://wiki.shibboleth.net/confluence/display/OpenSAML/OSTwoUsrManJavaCreateFromXML
			//
			
			// Initialize the library
			DefaultBootstrap.bootstrap(); 
			
			// Get parser pool manager
			BasicParserPool ppMgr = new BasicParserPool();
			ppMgr.setNamespaceAware(true);
			Document inCommonMDDoc = ppMgr.parse(in);
			Element assertionRoot = inCommonMDDoc.getDocumentElement();
			
			Unmarshaller unmarshaller = Configuration.getUnmarshallerFactory().getUnmarshaller(assertionRoot);
			return (Assertion)unmarshaller.unmarshall(assertionRoot);
		} catch (XMLParserException e) {
			throw new RuntimeException("can't unmarshall assertion", e);
		} catch (UnmarshallingException e) {
			throw new RuntimeException("can't unmarshall assertion", e);
		} catch (ConfigurationException e) {
			throw new RuntimeException("can't unmarshall assertion", e);
		}
		
	}	

	public Assertion unmarshallAssertion(byte[] assertionXml)  {
		return unmarshallAssertion(new ByteArrayInputStream(assertionXml));
	}
	
	
}
