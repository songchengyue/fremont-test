package com.telstra.netops.routemanagement.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for Deleted_Links_Routing table
 *
 */
@Entity
@Table(name = "Deleted_Links_Routing")
public class Deleted_Links_Routing {

	@Id
	@GeneratedValue
	@Column(name = "SERVICEID")
	private String serviceId;

	@Column(name = "LINKID")
	private String linkId;

	@Column(name = "BGP_NEIGHBOR_IP")
	private String bgpNeighborIp;

	@Column(name = "BHA_BGP_NEIGHBOR_IP")
	private String bhaBgpNeighborIp;

	@Column(name = "BGP_NEIGHBOR_IP_V6")
	private String bgpNeighborIpV6;

	@Column(name = "BHA_BGP_NEIGHBOR_IP_V6")
	private String bhaBgpNeighborIpV6;

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getBgpNeighborIp() {
		return bgpNeighborIp;
	}

	public void setBgpNeighborIp(String bgpNeighborIp) {
		this.bgpNeighborIp = bgpNeighborIp;
	}

	public String getBhaBgpNeighborIp() {
		return bhaBgpNeighborIp;
	}

	public void setBhaBgpNeighborIp(String bhaBgpNeighborIp) {
		this.bhaBgpNeighborIp = bhaBgpNeighborIp;
	}

	public String getBgpNeighborIpV6() {
		return bgpNeighborIpV6;
	}

	public void setBgpNeighborIpV6(String bgpNeighborIpV6) {
		this.bgpNeighborIpV6 = bgpNeighborIpV6;
	}

	public String getBhaBgpNeighborIpV6() {
		return bhaBgpNeighborIpV6;
	}

	public void setBhaBgpNeighborIpV6(String bhaBgpNeighborIpV6) {
		this.bhaBgpNeighborIpV6 = bhaBgpNeighborIpV6;
	}

}