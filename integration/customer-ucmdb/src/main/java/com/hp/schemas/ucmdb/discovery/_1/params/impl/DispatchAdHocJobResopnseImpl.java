/*
 * XML Type:  dispatchAdHocJobResopnse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML dispatchAdHocJobResopnse(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class DispatchAdHocJobResopnseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.DispatchAdHocJobResopnse
{
    
    public DispatchAdHocJobResopnseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName OSHVRESULT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "OSHVResult");
    
    
    /**
     * Gets the "OSHVResult" element
     */
    public java.lang.String getOSHVResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OSHVRESULT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "OSHVResult" element
     */
    public org.apache.xmlbeans.XmlString xgetOSHVResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(OSHVRESULT$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "OSHVResult" element
     */
    public void setOSHVResult(java.lang.String oshvResult)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OSHVRESULT$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(OSHVRESULT$0);
            }
            target.setStringValue(oshvResult);
        }
    }
    
    /**
     * Sets (as xml) the "OSHVResult" element
     */
    public void xsetOSHVResult(org.apache.xmlbeans.XmlString oshvResult)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(OSHVRESULT$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(OSHVRESULT$0);
            }
            target.set(oshvResult);
        }
    }
}
