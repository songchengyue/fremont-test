/*
 * An XML document type.
 * Localname: ClassModelQueryGetClassModelIconPathsResponse
 * Namespace: http://schemas.hp.com/ucmdb/ui/1/params
 * Java type: com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.ui._1.params.impl;
/**
 * A document containing one ClassModelQueryGetClassModelIconPathsResponse(@http://schemas.hp.com/ucmdb/ui/1/params) element.
 *
 * This is a complex type.
 */
public class ClassModelQueryGetClassModelIconPathsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponseDocument
{
    
    public ClassModelQueryGetClassModelIconPathsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLASSMODELQUERYGETCLASSMODELICONPATHSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/ui/1/params", "ClassModelQueryGetClassModelIconPathsResponse");
    
    
    /**
     * Gets the "ClassModelQueryGetClassModelIconPathsResponse" element
     */
    public com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse getClassModelQueryGetClassModelIconPathsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse target = null;
            target = (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse)get_store().find_element_user(CLASSMODELQUERYGETCLASSMODELICONPATHSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ClassModelQueryGetClassModelIconPathsResponse" element
     */
    public void setClassModelQueryGetClassModelIconPathsResponse(com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse classModelQueryGetClassModelIconPathsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse target = null;
            target = (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse)get_store().find_element_user(CLASSMODELQUERYGETCLASSMODELICONPATHSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse)get_store().add_element_user(CLASSMODELQUERYGETCLASSMODELICONPATHSRESPONSE$0);
            }
            target.set(classModelQueryGetClassModelIconPathsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "ClassModelQueryGetClassModelIconPathsResponse" element
     */
    public com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse addNewClassModelQueryGetClassModelIconPathsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse target = null;
            target = (com.hp.schemas.ucmdb.ui._1.params.ClassModelQueryGetClassModelIconPathsResponse)get_store().add_element_user(CLASSMODELQUERYGETCLASSMODELICONPATHSRESPONSE$0);
            return target;
        }
    }
}
