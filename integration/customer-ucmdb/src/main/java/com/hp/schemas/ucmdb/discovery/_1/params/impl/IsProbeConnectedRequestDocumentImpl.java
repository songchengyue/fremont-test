/*
 * An XML document type.
 * Localname: isProbeConnectedRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one isProbeConnectedRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class IsProbeConnectedRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequestDocument
{
    
    public IsProbeConnectedRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ISPROBECONNECTEDREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "isProbeConnectedRequest");
    
    
    /**
     * Gets the "isProbeConnectedRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequest getIsProbeConnectedRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequest)get_store().find_element_user(ISPROBECONNECTEDREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "isProbeConnectedRequest" element
     */
    public void setIsProbeConnectedRequest(com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequest isProbeConnectedRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequest)get_store().find_element_user(ISPROBECONNECTEDREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequest)get_store().add_element_user(ISPROBECONNECTEDREQUEST$0);
            }
            target.set(isProbeConnectedRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "isProbeConnectedRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequest addNewIsProbeConnectedRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedRequest)get_store().add_element_user(ISPROBECONNECTEDREQUEST$0);
            return target;
        }
    }
}
