#
#
# $Id: CMS_CUSTOMER.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $
#
#

package CMS_CUSTOMER;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_CUSTOMER_DB;

our @ISA = qw (Exporter);

our $error;



sub start {
  
  
  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} =~ /cancel/i))) {
    # default to view_current_outages
     
    CMS::clear_html_values();
    $CMS::html_values{subsystem}{value} = "customers";
    $CMS::html_values{command}{value} = "edit_customer";
    
  }

  if (defined $CMS::html_values{stage}{value}) {
    unless (CMS::check_screen($CMS::html_values{current_screen}{value})){
      $CMS::html_values{stage}{value}--;
    }
  }

  if ($CMS::html_values{command}{value} eq "add_customer") {
    # add a new customer
    
    if (not defined ($CMS::html_values{stage}{value})) {
      # first stage
      $CMS::html_values{wholesaler}{options} = 
	 CMS_CUSTOMER_DB::get_wholesalers();
      $CMS::html_values{accountattributeid}{options} =
	 CMS_CUSTOMER_DB::get_accountattribute();
      $CMS::html_values{mrwlinkid}{options} = 
	 CMS_CUSTOMER_DB::get_mrwlink();
      $CMS::html_values{slareportcode}{options} =
	 CMS_CUSTOMER_DB::get_slareportcode();

      $CMS::html_values{epaccess}{options} = [['FALSE'],['TRUE']];
      

      CMS::output_html("customer_add");
    } elsif ($CMS::html_values{stage}{value} == 2) {
      #$CMS::html_values{mrw_accno}{value} = $CMS::html_values{mrwlinkid}{value};    

      if (CMS_CUSTOMER_DB::add_customer
	  ($CMS::html_values{accno}{value},
	   $CMS::html_values{fnn}{value},
	   $CMS::html_values{company}{value},
	   $CMS::html_values{accountattributeid}{value},
	   $CMS::html_values{wholesaler}{value},
	   $CMS::html_values{mrwlinkid}{value},
	   $CMS::html_values{slareportcode}{value},
	   $CMS::html_values{epaccess}{value})) {
	# have added customer

	
	CMS::add_error("Account added successfully.");
	$CMS::html_values{command}{value} = "edit_customer";
	CMS::output_html("customer_edit");

      } else {
	# Failed to add customer
	CMS::add_error("Failed to add customer.");
	CMS::add_error($CMS_CUSTOMER_DB::error);
	$CMS::html_values{wholesaler}{options} = 
	  CMS_CUSTOMER_DB::get_wholesalers();
	$CMS::html_values{accountattributeid}{options} =
         CMS_CUSTOMER_DB::get_accountattribute();
        $CMS::html_values{slareportcode}{options} =
 	 CMS_CUSTOMER_DB::get_slareportcode();

	CMS::output_html("customer_add");
      }
      
    }

  } elsif ($CMS::html_values{command}{value} eq "edit_customer") {

    if (not defined ($CMS::html_values{stage}{value})) {
      # first stage
      CMS::output_html("customer_edit");
    } elsif ($CMS::html_values{stage}{value} == 2) {
      # second stage

      if (my $customer = CMS_CUSTOMER_DB::get_customer($CMS::html_values{accno}{value}))
	{ # CUSTOMER

	  $CMS::html_values{accno}{value} = $$customer[0];
	  $CMS::html_values{fnn}{value} = $$customer[1];
	  $CMS::html_values{company}{value} = $$customer[2];
	  $CMS::html_values{billing}{value} = $$customer[3];
	  $CMS::html_values{wholesaler}{value} = $$customer[4];
	  $CMS::html_values{accountattributeid}{value} = $$customer[5];
	  $CMS::html_values{mrwlinkid}{value} = $$customer[6];
	  $CMS::html_values{slareportcode}{value} = $$customer[7];
          $CMS::html_values{epaccess}{value} = $$customer[8];
	  my $contract_ended = $$customer[9];

	  $CMS::html_values{mrwlinkid}{options} =
	    CMS_CUSTOMER_DB::get_mrwlink();
	  $CMS::html_values{wholesaler}{options} = 
	    CMS_CUSTOMER_DB::get_wholesalers();
	  $CMS::html_values{accountattributeid}{options} =
            CMS_CUSTOMER_DB::get_accountattribute();
          $CMS::html_values{slareportcode}{options} =
    	    CMS_CUSTOMER_DB::get_slareportcode();

	  $CMS::html_values{epaccess}{options} =
	    CMS_CUSTOMER_DB::epaccess_as_true_or_false($$customer[8]);
	  
	  if ($contract_ended eq "t") {
	    CMS::output_html("customer_edit2a");
	  } else {
	    CMS::output_html("customer_edit2");
	  }
      } else {
	CMS::add_error("That account is not valid.");
	CMS::output_html("customer_edit");
      }

    } 
    else {
      if (CMS_CUSTOMER_DB::update_customer
	  ($CMS::html_values{accno}{value},
	   $CMS::html_values{fnn}{value},
	   $CMS::html_values{company}{value},
	   $CMS::html_values{billing}{value}, # not used yet
	   $CMS::html_values{wholesaler}{value},
	   $CMS::html_values{resetpassword}{value},
	   $CMS::html_values{accountattributeid}{value},
	   $CMS::html_values{slareportcode}{value},
	   $CMS::html_values{epaccess}{value})) {
	CMS::add_error("Update successful.");
	CMS::output_html("customer_edit");
      } else {
	CMS::add_error("Update failed.");
	$CMS::html_values{wholesaler}{options} = 
	    CMS_CUSTOMER_DB::get_wholesalers();
	$CMS::html_values{accountattributeid}{options} =
            CMS_CUSTOMER_DB::get_accountattribute();
        $CMS::html_values{slareportcode}{options} =
    	    CMS_CUSTOMER_DB::get_slareportcode();
	
	$CMS::html_values{epaccess}{options} =
	  CMS_CUSTOMER_DB::epaccess_as_true_or_false
	      ($CMS::html_values{epaccess}{value});

	CMS::output_html("customer_edit2");
      }

      #CMS::output_html("customer");
    }
    
  } 
  
  elsif ($CMS::html_values{command}{value} eq "delete_customer") {
    if (not defined ($CMS::html_values{stage}{value})) {
      # first stage
      CMS::output_html("customer_delete");
    } elsif ($CMS::html_values{stage}{value} == 2) {
      # second stage
      
      if (CMS_CUSTOMER_DB::delete_customer
	  ($CMS::html_values{accno}{value})) {
	CMS::add_error("Account $CMS::html_values{accno}{value} is no longer in the database.");
	$CMS::html_values{command}{value} = "edit_customer";
	$CMS::html_values{stage}{value} = undef;
	CMS::output_html("customer_edit");
      } else {
	CMS::add_error("Deletion failed.");
	CMS::output_html("customer_delete");
      }

    }


;
  } elsif ($CMS::html_values{command}{value} eq "search_customer") {

    if ((defined $CMS::html_values{submit}{value}) &&
	($CMS::html_values{submit}{value} eq "Search")) {

      $CMS::html_values{customers}{table}
	= CMS_CUSTOMER_DB::search_customer
	($CMS::html_values{search_accno}{value},
	 $CMS::html_values{search_fnn}{value},
	 $CMS::html_values{search_company}{value},
	 $CMS::html_values{search_wholesaler}{value});

      unshift (@{$CMS::html_values{customers}{table}}, 
	       ["Accno","FNN", "Company", "Wholesaler", "Account Type", "MRW Account", "SLA Reporting"]);
      
      $CMS::html_values{customers}{header_rows} = 1;

    } else {
      $CMS::html_values{search_accno}{value} = "";
      $CMS::html_values{search_fnn}{value} = "";
      $CMS::html_values{search_company}{value} = "";
      $CMS::html_values{search_wholesaler}{value} = "";

    }
    
    CMS::output_html("customer_search");
  }
  
  
} 
  
  

1;
