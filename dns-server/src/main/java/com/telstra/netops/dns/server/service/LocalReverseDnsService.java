package com.telstra.netops.dns.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.dns.server.exception.DnsApplicationGenericBusinessRuleException;
import com.telstra.netops.dns.server.model.ClientReverseDnsRequest;
import com.telstra.netops.dns.server.model.DnsReverseRecord;
import com.telstra.netops.dns.server.model.Success;
import com.telstra.netops.dns.server.model.cgi.DnsResultWrapper;

@Service
@Profile("local")
public class LocalReverseDnsService implements ReverseDnsService {

	private static final java.lang.String HEADER = "List of Allowed IPs for DNS Resolver Service";
	
	@Value("${dns.localtest.success}")
	private boolean success;

	@Override
	public Success addDelegation(ClientReverseDnsRequest request) {
		if (success) {
			return new Success("SUCCESS!");
		} else {
			throw new DnsApplicationGenericBusinessRuleException("ERROR!!!");
		}
	}

	@Override
	public Success removeDelegation(ClientReverseDnsRequest request) {
		if (success) {
			return new Success("SUCCESS!");
		} else {
			throw new DnsApplicationGenericBusinessRuleException("ERROR!!!");
		}
	}

	@Override
	public DnsResultWrapper addMapping(ClientReverseDnsRequest request) {
		if (success) {

			DnsResultWrapper wrapper = new DnsResultWrapper();
			wrapper.setStatusMessage("SUCCESS!");
			return wrapper;
		} else {
			DnsResultWrapper wrapper = new DnsResultWrapper();
			wrapper.setErrorMessage("ERROR!!");
			return wrapper;
		}
	}

	@Override
	public List<DnsReverseRecord> list(String customerId) {
		List<DnsReverseRecord> records = new ArrayList<DnsReverseRecord>();
		records.add(new DnsReverseRecord("yy1234", "123.19.2.0/24", "raj@gmail.com", customerId, HEADER,"abc","Reverse"));
		records.add(new DnsReverseRecord("ttttt1234", "123.18.2.0/24", "ram@gmail.com", customerId, HEADER,"abc","Reverse"));
		records.add(new DnsReverseRecord("ssss1234", "123.17.2.0/24", "uday@gmail.com", customerId, HEADER,"abc","Reverse"));
		return records;
	}

	@Override
	public DnsResultWrapper removeMapping(ClientReverseDnsRequest request) {
		if (success) {

			DnsResultWrapper wrapper = new DnsResultWrapper();
			wrapper.setStatusMessage("SUCCESS!");
			return wrapper;
		} else {
			DnsResultWrapper wrapper = new DnsResultWrapper();
			wrapper.setErrorMessage("ERROR!!");
			return wrapper;
		}
	}

}
