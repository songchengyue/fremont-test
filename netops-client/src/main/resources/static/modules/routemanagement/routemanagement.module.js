(function() {
	angular.module('netops.routeManagementModule', [ 'ngRoute',
			'netops.utilModule' ]);
	var test;
	angular
			.module('netops.routeManagementModule')

			.config(function(moduleRegistryProvider) {
				moduleRegistryProvider.register({
					name : 'routemanagement',
					baseUrl : '/routemanagement-server/'
				});
			})
			.service('selectedService', function() {
				var serviceList = "";

				var addSelectedService = function(newObj) {
					serviceList = newObj;
				};

				var getSelectedService = function() {
					return serviceList;
				};

				return {
					addSelectedService : addSelectedService,
					getSelectedService : getSelectedService
				};

			})
			.controller(
					'ServiceListController',
					function($http, $scope, $rootScope, $routeParams, Data,
							selectedService, localStorageService, ERROR_TEXT) {
						$scope.name = "ServiceListController";
						$scope.params = $routeParams;
						$scope.productLine = Data.getProductLine();
						$scope.resetPage = function() {
							$scope.error = false;
							$scope.table = false;
						}

						$scope.sendValue = function(serviceObj) {
							selectedService.addSelectedService(serviceObj);
						}

						$scope.loadServiceList = function() {
							$http
									.get(
											'/routemanagement-server/service?productLine='
													+ $scope.productLine,
											$scope.user)
									.then(
											function(response) {
												// this callback will be called
												// asynchronously
												// when the response is
												// available

												$scope.ServVal = response.data;
											},
											function(response) {
												// called asynchronously if an
												// error occurs
												// or server returns response
												// with an error status.
												// in this case redirected to
												// error page
												$scope.error = response.errorText ? response.errorText
														: ERROR_TEXT.SERVER;

											});
						}

					})
			.controller(
					'SinglePrefixAddController',
					function($scope, $http, Data, $rootScope, $location,
							$routeParams, selectedService, localStorageService,
							ERROR_TEXT) {
						$scope.name = 'SinglePrefixAddController';
						$scope.serviceSelected = selectedService
								.getSelectedService().serviceId[0];
						$scope.role = localStorageService.get('role');
						var submit = function() {
							$http
									.post(
											'/routemanagement-server/singleprefix?service='
													+ $scope.serviceSelected,
											$scope.user)
									.then(
											function(response) {
												// this callback will be called
												// asynchronously
												// when the response is
												// available
												console.log(response.data);
												$scope.loading = false;
												var result = response.data.message;
												Data.setSuccessCode(result);
												$location.path('/Success');
											},
											function(response) {
												// called asynchronously if an
												// error occurs
												// or server returns response
												// with an error status.
												$scope.loading = false;
												console
														.log(response.data.message);

												$scope.error = response.errorText ? response.errorText: ERROR_TEXT.SERVER;
												Data.setErrorFromServer(response.data.message);
												$location.path('/ServerError');

											});
						}
						$scope.submitipv4 = function() {
							$scope.user.ipVersion = "IPv4";
							submit();
						}
						// /to pass the data entered from the front end to the
						// back end.
						$scope.submitipv6 = function() {
							$scope.user.ipVersion = "IPv6";
							submit();
						}

						$scope.clear = function() {
							$scope.user = {};
							$scope.SinglePrefix.$setPristine();
							$scope.SinglePrefixIpv6.$setPristine();
						}

						$scope.openbgpPolicy = function() {
							$location.path('/RM_BgpPolicyView');
						}
					})

			.controller(
					'MultiplePrefixAddController',
					function($scope, $http, Data, $rootScope, $location,
							$routeParams, selectedService, localStorageService,
							ERROR_TEXT) {
						$scope.name = 'MultiplePrefixAddController';
						$scope.files = [];
						$scope.serviceSelected = selectedService
								.getSelectedService().serviceId[0];
						$scope.role = localStorageService.get('role');
						var fileSelected = "";
						$scope.HandleBrowseClick = function() {
							var fileinput = document.getElementById("browse");
							fileinput.click();
						}
						$scope.$on("fileSelected", function(event, args) {
							fileSelected = args.file[0];
							fileinput = document.getElementById("browse");
							var textinput = fileinput.value;
							var nameOfFile = textinput.split('\\').pop();
							var ext = textinput.split('.').pop();
							if (ext != "csv") {
							} else {
								$scope.textinput = nameOfFile;
								$scope.path = fileinput.value;
								$scope.files.length = 0;
							}
						});
						// $scope.submit = function() {
						var submit = function() {
							if (fileSelected != "") {
								$scope.loading = true;
								$scope.user.fileContent = fileinput.value;
								var formData = new FormData();
								formData.append("file", new Blob(
										[ fileSelected ], {
											type : "text/csv"
										}));
								formData.append("props", JSON
										.stringify($scope.user));
								$http(
										{
											method : 'POST',
											url : '/routemanagement-server/multipleprefix?service='
													+ $scope.serviceSelected,
											data : formData,
											transformRequest : angular.identity,
											headers : {
												'Content-Type' : undefined
											}
										})
										.then(
												function(response) {
													// this callback will be
													// called asynchronously
													// when the response is
													// available
													console.log(response.data);
													$scope.loading = false;
													var result = response.data.message;
													Data.setSuccessCode(result);
													$location.path('/Success');
												},
												function(response) {
													// called asynchronously if
													// an error occurs
													// or server returns
													// response with an error
													// status.
													$scope.loading = false;
													console.log("An error encountered"	+ response);
													$scope.error = response.errorText ? response.errorText: ERROR_TEXT.SERVER;
													Data.setErrorFromServer(response.data.message);
													$location.path('/ServerError');
												});
							}
						};
						$scope.submitipv4 = function() {
							$scope.user.ipVersion = "IPv4";
							submit();
						}
						// /to pass the data entered from the front end to the
						// back end.
						$scope.submitipv6 = function() {
							$scope.user.ipVersion = "IPv6";
							submit();
						}

						$scope.openbgpPolicy = function() {
							$location.path('/RM_BgpPolicyView');
						}
					})

			// Controller for BGP Ploicy
			.controller(
					'BgpPolicyViewController',
					[ '$scope', 'Data', '$location',
							function($scope, Data, $location, ERROR_TEXT) {
								console.log('in controller');

								$scope.BackToHomePage = function() {
									$location.path('/Netops');
								}

							} ])

			// Controller for View/Delete Prefix
			.controller(
					'RoutePrefixController',
					function($scope, $http, Data, $location, $routeParams,
							selectedService, ERROR_TEXT) {
						$scope.name = 'RoutePrefixController';
						$scope.params = $routeParams;
						$scope.serviceSelected = selectedService.getSelectedService().serviceId[0];
						$scope.valueToDelete;
						console.log("in viewprefix");
						var array = [];
						$scope.sendToBackendForDeletion = function() {
							$http.delete(
											'/routemanagement-server/prefix?ids='
													+ $scope.valueToDelete
													+ '&service='
													+ $scope.serviceSelected)
									.then(
											function(response) {
												console.log(response.data);
												$scope.loading = false;
												var result = response.data.message;
												console.log(result);
												Data.setSuccessCode(result);
												$location.path('/Success');
											},
											function(response) {
												$scope.loading = false;
												console.log("An error encountered"+ response);
												$scope.error = response.errorText ? response.errorText: ERROR_TEXT.SERVER;
												Data.setErrorFromServer(response.data.message);
												$location.path('/ServerError');
											});
						};

						$scope.checked_value = 0;
						$scope.message;

						$scope.viewdetails = function() {
							$scope.loading = true;
							$http
									.get(
											'/routemanagement-server/prefix?service='
													+ $scope.serviceSelected)
									.then(
											function(response) {
												$scope.loading = false;

												if (response.data.length <= 1) {
													// todo --- response.data
													// will be empty as its an
													// array
													$scope.viewRecords = response.data;
													$scope.AS = $scope.viewRecords[0].AS;
													$scope.error = "There are no records to be displayed";
												} else {
													$scope.table = true;
													$scope.viewRecords = response.data;
													console.log($scope.viewRecords);
													console.log($scope.viewRecords[0].AS);
													$scope.AS = $scope.viewRecords[0].AS;
													$scope.trackno = $scope.viewRecords[0].trackno;

													var remove = response.data;
													var removed = remove.splice(1,remove.length);
													$scope.viewRecords = removed;
												}
											},
											function(response) {
												// called asynchronously if an
												// error occurs
												// or server returns response
												// with an error status.
												console.log("An error encountered"+ response);
												$scope.error = true;
												Data.setErrorCode(response.status);
												$scope.loading = false;
											});

						}

						// added for select all

						$scope.checkAll = function($event) {

							if ($event.target.checked == true) {
								$scope.selectedAll = true;
								array.length = 0;
								$scope.allChecked = 1;
								// $scope.networkprefix.selected=true;
								angular.forEach($scope.viewRecords, function(
										viewrecord) {
									// alert("in check all for each");
									viewrecord.Selected = $scope.selectedAll;
									// alert("in check all function all
									// checked");
								});
								for (var i = 0; i < $scope.viewRecords.length; i++) {
									array.push($scope.viewRecords[i].prefix);
								}
								$scope.checked = array.length;
							} else {

								$scope.selectedAll = false;
								$scope.allChecked = 0;
								angular.forEach($scope.viewRecords, function(
										viewrecord) {
									viewrecord.Selected = $scope.selectedAll;
									array.length = 0;
								});
								$scope.checked = array.length;
							}
						};

						$scope.addToList = function($event, id) {
							$scope.id = id;
							if ($scope.allChecked == 0) {
								$scope.viewrecord.Selectall = false;
							}
							console.log("addToList(): id: " + id);
							console.log($event.target.checked
									+ 'checkbox checked ');
							if ($event.target.checked == true) {
								$scope.valueToDelete = id;
								array.push(id);
								// arrayToDelete=array;
								console.log("final list add list");
								console.log(array);
								$scope.checked = array.length;
								console.log(' checked' + $scope.checked);

							} else {
								// $scope.removeUncheckedData(array,id);
								array = $scope.removeUncheckedData(array, id);
								// alert(arrayToDelete+'final');
								console.log("final list remove in else");
								console.log(array);
								$scope.checked = array.length;
								console.log(' checked' + $scope.checked);

							}

						};

						/*
						 * $scope.addToList = function($event,id) {
						 * $scope.id=id; console.log("addToList(): id: " + id);
						 * console.log($event.target.checked)
						 * if($event.target.checked==true){ array.push(id);
						 * arrayToDelete=array;
						 * $scope.checked_value=arrayToDelete.length;
						 * console.log("checked scope "+$scope.checked_value); }
						 * else { // $scope.removeUncheckedData(array,id);
						 * arrayToDelete=$scope.removeUncheckedData(array,id);
						 * $scope.checked_value=arrayToDelete.length; } };
						 */

						/*
						 * $scope.removeUncheckedData = function(array,id) { for
						 * (var i=array.length-1; i>=0; i--) { if (array[i] ===
						 * id) { array.splice(i, 1); return array; // break; //<--
						 * Uncomment if only the first term has to be removed } } }
						 */
						$scope.removeUncheckedData = function(array, id) {
							// alert (array);
							for (var i = array.length - 1; i >= 0; i--) {
								if (array[i] === id) {
									// alert('insearch');
									array.splice(i, 1);
									// alert(array);
									return array;

								}
							}
						}

						$scope.clearAll = function() {
							$scope.valueToDelete = '';
							arrayToDelete.length = 0; // removing the values
							// from the array if
							// there are any before
							// hitting clear
							$scope.checked_value = 0;// setting this to zero
							// so that delete and
							// clear buttons are not
							// visible until atleast
							// one record is
							// checked.
						};

					})

			// New page Router information View Page
			.controller(
					'RouterInformationViewController',
					function($http, $scope, $rootScope, Data, $routeParams,
							selectedService, ERROR_TEXT) {
						$scope.name = "RouterInformationViewController";
						$scope.params = $routeParams;
						$scope.productLine = Data.getProductLine();
						$scope.resetPage = function() {
							$scope.error = false;
							$scope.table = false;
						}

						$scope.services = function() {
							$http
									.get(
											'/routemanagement-server/routerinformation?productLine='
													+ $scope.productLine,
                                                    $scope.user)
									.then(
											function(response) {
												// this callback will be called
												// asynchronously
												// when the response is
												// available
												$scope.table = true;
												$scope.routerInformations = response.data;
												console.log($scope.routerInformations);

											},
											function(response) {
												// unhandled exception
												// called asynchronously if an
												// error occurs
												// or server returns response
												// with an error status.
												// in this case redirected to
												// error page
												$scope.loading = false;
												console.log(response.data.message);

												$scope.error = response.errorText ? response.errorText: ERROR_TEXT.SERVER;
												Data.setErrorFromServer(response.data.message);
												$location.path('/ServerError');

											});
						}

						$scope.sendValue = function(serviceObj) {
							selectedService.addSelectedService(serviceObj);
						}

					})

			// //for displaying err from server on validation failure from

			.controller(
					'RouterInformationEditController',
					function($scope, $rootScope, $http, Data, $location,
							$routeParams, selectedService, ERROR_TEXT) {
						// data to be fetched from back end.
						$scope.name = 'RouterInformationEditController';
						$scope.params = $routeParams;
						$scope.user = selectedService.getSelectedService();

						$(document).ready(function() {
							$('[data-toggle="tooltip"]').tooltip();
						});
						$scope.cancel = function() {
							$scope.user = {};
							$location.path('/RM_RouterInformationView');
						}
						$scope.updateRouterInfoRecords = function() {
							console.log($scope.user);
							$scope.loading = true;
							$http
									.put(
											'/routemanagement-server/routerinformation',
											$scope.user)
									.then(
											function(response) {
												// this callback will be called
												// asynchronously
												// when the response is
												// available
												console.log(response.data);
												$scope.loading = false;
												$scope.routerInformations = response.data;
												var result = response.data.message;
												Data.setSuccessCode(result);
												$location
														.path('/RM_RouterInformationView');
											},
											function(response) {
												// called asynchronously if an
												// error occurs
												// or server returns response
												// with an error status.
												$scope.loading = false;
												console.log(response.data.message);

												$scope.error = response.errorText ? response.errorText: ERROR_TEXT.SERVER;
												Data.setErrorFromServer(response.data.message);
												$location.path('/ServerError');

											});
						}
					})

			.directive('fileUpload', function() {
				return {
					scope : true, // create a new scope
					link : function(scope, el, attrs) {
						el.bind('change', function(event) {
							var files = event.target.files;
							scope.fileObj = files[0];
							scope.$emit("fileSelected", {
								file : files
							});
						});
					}
				};
			})
			.config(
					function($routeProvider, $locationProvider) {
						$routeProvider
								.when('/Netops', {
									templateUrl : 'templates/Netops.jsp'
								})
								.when(
										'/RM_RouteManagement',
										{
											templateUrl : 'modules/routemanagement/templates/RouteManagement.html'
										})
								.when(
										'/RM_ServiceListSingle',
										{
											templateUrl : 'modules/routemanagement/templates/ServiceListSingle.html',
										})
								.when(
										'/RM_ServiceListMultiple',
										{
											templateUrl : 'modules/routemanagement/templates/ServiceListMultiple.html',
										})
								.when(
										'/RM_ServiceListView',
										{
											templateUrl : 'modules/routemanagement/templates/ServiceListView.html',
										})
								.when(
										'/RM_RoutePrefixView',
										{
											templateUrl : 'modules/routemanagement/templates/RoutePrefixView.html',
										})
								.when(
										'/RM_SinglePrefixAdd',
										{
											templateUrl : 'modules/routemanagement/templates/SinglePrefixAdd.html',
										})
								.when(
										'/RM_MultiplePrefixAdd',
										{
											templateUrl : 'modules/routemanagement/templates/MultiplePrefixAdd.html',
										})
								.when(
										'/RM_BgpPolicyView',
										{
											templateUrl : 'modules/routemanagement/templates/BgpPolicyView.html',
										})
								.when(
										'/RM_RouterInformationView',
										{
											templateUrl : 'modules/routemanagement/templates/RouterInformationView.html',
										})
								.when(
										'/RM_RouterInformationEdit',
										{
											templateUrl : 'modules/routemanagement/templates/RouterInformationEdit.html',
										}).when('', {})
					});
})();