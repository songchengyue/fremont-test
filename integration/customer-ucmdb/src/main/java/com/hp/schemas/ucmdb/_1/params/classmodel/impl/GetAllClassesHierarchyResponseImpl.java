/*
 * XML Type:  getAllClassesHierarchyResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/classmodel
 * Java type: com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.classmodel.impl;
/**
 * An XML getAllClassesHierarchyResponse(@http://schemas.hp.com/ucmdb/1/params/classmodel).
 *
 * This is a complex type.
 */
public class GetAllClassesHierarchyResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponse
{
    
    public GetAllClassesHierarchyResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLASSESHIERARCHY$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "classesHierarchy");
    private static final javax.xml.namespace.QName COMMENTS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "comments");
    
    
    /**
     * Gets the "classesHierarchy" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy getClassesHierarchy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy)get_store().find_element_user(CLASSESHIERARCHY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "classesHierarchy" element
     */
    public void setClassesHierarchy(com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy classesHierarchy)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy)get_store().find_element_user(CLASSESHIERARCHY$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy)get_store().add_element_user(CLASSESHIERARCHY$0);
            }
            target.set(classesHierarchy);
        }
    }
    
    /**
     * Appends and returns a new empty "classesHierarchy" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy addNewClassesHierarchy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassModelHierarchy)get_store().add_element_user(CLASSESHIERARCHY$0);
            return target;
        }
    }
    
    /**
     * Gets the "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments getComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "comments" element
     */
    public void setComments(com.hp.schemas.ucmdb._1.types.ResComments comments)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            }
            target.set(comments);
        }
    }
    
    /**
     * Appends and returns a new empty "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments addNewComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            return target;
        }
    }
}
