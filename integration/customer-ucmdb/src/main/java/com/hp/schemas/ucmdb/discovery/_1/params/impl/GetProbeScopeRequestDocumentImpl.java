/*
 * An XML document type.
 * Localname: getProbeScopeRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getProbeScopeRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetProbeScopeRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequestDocument
{
    
    public GetProbeScopeRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETPROBESCOPEREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbeScopeRequest");
    
    
    /**
     * Gets the "getProbeScopeRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequest getGetProbeScopeRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequest)get_store().find_element_user(GETPROBESCOPEREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getProbeScopeRequest" element
     */
    public void setGetProbeScopeRequest(com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequest getProbeScopeRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequest)get_store().find_element_user(GETPROBESCOPEREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequest)get_store().add_element_user(GETPROBESCOPEREQUEST$0);
            }
            target.set(getProbeScopeRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "getProbeScopeRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequest addNewGetProbeScopeRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeRequest)get_store().add_element_user(GETPROBESCOPEREQUEST$0);
            return target;
        }
    }
}
