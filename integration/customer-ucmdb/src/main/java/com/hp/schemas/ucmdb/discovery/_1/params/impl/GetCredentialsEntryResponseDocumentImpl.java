/*
 * An XML document type.
 * Localname: getCredentialsEntryResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getCredentialsEntryResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetCredentialsEntryResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponseDocument
{
    
    public GetCredentialsEntryResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCREDENTIALSENTRYRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getCredentialsEntryResponse");
    
    
    /**
     * Gets the "getCredentialsEntryResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse getGetCredentialsEntryResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse)get_store().find_element_user(GETCREDENTIALSENTRYRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCredentialsEntryResponse" element
     */
    public void setGetCredentialsEntryResponse(com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse getCredentialsEntryResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse)get_store().find_element_user(GETCREDENTIALSENTRYRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse)get_store().add_element_user(GETCREDENTIALSENTRYRESPONSE$0);
            }
            target.set(getCredentialsEntryResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getCredentialsEntryResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse addNewGetCredentialsEntryResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryResponse)get_store().add_element_user(GETCREDENTIALSENTRYRESPONSE$0);
            return target;
        }
    }
}
