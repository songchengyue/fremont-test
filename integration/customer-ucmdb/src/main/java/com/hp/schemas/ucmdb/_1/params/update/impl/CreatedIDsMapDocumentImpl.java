/*
 * An XML document type.
 * Localname: CreatedIDsMap
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.CreatedIDsMapDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * A document containing one CreatedIDsMap(@http://schemas.hp.com/ucmdb/1/params/update) element.
 *
 * This is a complex type.
 */
public class CreatedIDsMapDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.CreatedIDsMapDocument
{
    
    public CreatedIDsMapDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CREATEDIDSMAP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "CreatedIDsMap");
    
    
    /**
     * Gets the "CreatedIDsMap" element
     */
    public com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID getCreatedIDsMap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID)get_store().find_element_user(CREATEDIDSMAP$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CreatedIDsMap" element
     */
    public void setCreatedIDsMap(com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID createdIDsMap)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID)get_store().find_element_user(CREATEDIDSMAP$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID)get_store().add_element_user(CREATEDIDSMAP$0);
            }
            target.set(createdIDsMap);
        }
    }
    
    /**
     * Appends and returns a new empty "CreatedIDsMap" element
     */
    public com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID addNewCreatedIDsMap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.ClientIDToCmdbID)get_store().add_element_user(CREATEDIDSMAP$0);
            return target;
        }
    }
}
