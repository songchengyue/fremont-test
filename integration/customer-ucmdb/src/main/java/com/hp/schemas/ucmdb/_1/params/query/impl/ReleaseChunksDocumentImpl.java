/*
 * An XML document type.
 * Localname: releaseChunks
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ReleaseChunksDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one releaseChunks(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class ReleaseChunksDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ReleaseChunksDocument
{
    
    public ReleaseChunksDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RELEASECHUNKS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "releaseChunks");
    
    
    /**
     * Gets the "releaseChunks" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ReleaseChunks getReleaseChunks()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ReleaseChunks target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ReleaseChunks)get_store().find_element_user(RELEASECHUNKS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "releaseChunks" element
     */
    public void setReleaseChunks(com.hp.schemas.ucmdb._1.params.query.ReleaseChunks releaseChunks)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ReleaseChunks target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ReleaseChunks)get_store().find_element_user(RELEASECHUNKS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.ReleaseChunks)get_store().add_element_user(RELEASECHUNKS$0);
            }
            target.set(releaseChunks);
        }
    }
    
    /**
     * Appends and returns a new empty "releaseChunks" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ReleaseChunks addNewReleaseChunks()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ReleaseChunks target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ReleaseChunks)get_store().add_element_user(RELEASECHUNKS$0);
            return target;
        }
    }
}
