/*
 * XML Type:  executeTopologyQueryByName
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML executeTopologyQueryByName(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class ExecuteTopologyQueryByNameImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName
{
    
    public ExecuteTopologyQueryByNameImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "cmdbContext");
    private static final javax.xml.namespace.QName QUERYNAME$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "queryName");
    private static final javax.xml.namespace.QName QUERYTYPEDPROPERTIES$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "queryTypedProperties");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets the "queryName" element
     */
    public java.lang.String getQueryName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUERYNAME$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "queryName" element
     */
    public org.apache.xmlbeans.XmlString xgetQueryName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUERYNAME$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "queryName" element
     */
    public void setQueryName(java.lang.String queryName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUERYNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUERYNAME$2);
            }
            target.setStringValue(queryName);
        }
    }
    
    /**
     * Sets (as xml) the "queryName" element
     */
    public void xsetQueryName(org.apache.xmlbeans.XmlString queryName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUERYNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(QUERYNAME$2);
            }
            target.set(queryName);
        }
    }
    
    /**
     * Gets the "queryTypedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection getQueryTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().find_element_user(QUERYTYPEDPROPERTIES$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "queryTypedProperties" element
     */
    public boolean isSetQueryTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(QUERYTYPEDPROPERTIES$4) != 0;
        }
    }
    
    /**
     * Sets the "queryTypedProperties" element
     */
    public void setQueryTypedProperties(com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection queryTypedProperties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().find_element_user(QUERYTYPEDPROPERTIES$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().add_element_user(QUERYTYPEDPROPERTIES$4);
            }
            target.set(queryTypedProperties);
        }
    }
    
    /**
     * Appends and returns a new empty "queryTypedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection addNewQueryTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection)get_store().add_element_user(QUERYTYPEDPROPERTIES$4);
            return target;
        }
    }
    
    /**
     * Unsets the "queryTypedProperties" element
     */
    public void unsetQueryTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(QUERYTYPEDPROPERTIES$4, 0);
        }
    }
}
