/*
 * XML Type:  StrListProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.StrListProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML StrListProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class StrListPropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.StrListProps
{
    
    public StrListPropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STRLISTPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strListProp");
    
    
    /**
     * Gets array of all "strListProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.StrListProp[] getStrListPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(STRLISTPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.StrListProp[] result = new com.hp.schemas.ucmdb._1.types.StrListProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "strListProp" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListProp getStrListPropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListProp)get_store().find_element_user(STRLISTPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "strListProp" element
     */
    public int sizeOfStrListPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STRLISTPROP$0);
        }
    }
    
    /**
     * Sets array of all "strListProp" element
     */
    public void setStrListPropArray(com.hp.schemas.ucmdb._1.types.StrListProp[] strListPropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(strListPropArray, STRLISTPROP$0);
        }
    }
    
    /**
     * Sets ith "strListProp" element
     */
    public void setStrListPropArray(int i, com.hp.schemas.ucmdb._1.types.StrListProp strListProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListProp)get_store().find_element_user(STRLISTPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(strListProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "strListProp" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListProp insertNewStrListProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListProp)get_store().insert_element_user(STRLISTPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "strListProp" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListProp addNewStrListProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListProp)get_store().add_element_user(STRLISTPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "strListProp" element
     */
    public void removeStrListProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STRLISTPROP$0, i);
        }
    }
}
