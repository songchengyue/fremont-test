package com.telstra.netops.lookingglass.server.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Test;

import com.telstra.netops.lookingglass.server.model.ClientNodeTestRequest;
import com.telstra.netops.lookingglass.server.model.NodeTestResult;

public class JsonSerializerDeserializerTest {
	
	private static final double TOLERANCE = 0.0001;
	private static final double TOLERANCE2 = 0.0001;
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy z");
	
	private JsonSerializerDeserializer jsonSerializerDeserializer;
	
	@Before
	public void before() {
		jsonSerializerDeserializer = new JsonSerializerDeserializer();
	}
	
	@Test
	public void deserialiseNodeTestResult() throws ParseException {
		NodeTestResult expected = new NodeTestResult(
													DATE_FORMAT.parse("Tue May 24 23:20:46 2016 GMT"),
													1.271f,
													1.82f,
													1.422f,
													0.146f,
													0f,
													"1","1","1","1","1","1","1","1","1","1",
													"1","1","1","1","1","1","1","1","1","1"
													);
				
		String content = "{\"AvgLatency\":1.422,\"StdDeviation\":0.146,\"MaxLatency\":1.82,\"MinLatency\":1.271,\"AvgLoss\":0, \"date\": \"Tue May 24 23:20:46 2016 GMT\"}";
		
		NodeTestResult actual = jsonSerializerDeserializer.deserialize(content, NodeTestResult.class);
		
		assertEquals(expected.getDate(), actual.getDate());
		assertEquals(expected.getMinLatency(), actual.getMinLatency(), TOLERANCE);
		assertEquals(expected.getMaxLatency(), actual.getMaxLatency(), TOLERANCE);
		assertEquals(expected.getAvgLatency(), actual.getAvgLatency(), TOLERANCE);
		assertEquals(expected.getAvgLoss(), actual.getAvgLoss(), TOLERANCE);
		assertEquals(expected.getStdDeviation(), actual.getStdDeviation(), TOLERANCE);
	}
	
	@Test
	public void serialiseClientNodeTestResult() {
		ClientNodeTestRequest request = ClientNodeTestRequest.createNodeTestRequest(1L, 1L, 5);
		String content = jsonSerializerDeserializer.serialize(request);
		assertTrue(content.contains("\"level\":21"));
		System.out.println(content);
	}

}
