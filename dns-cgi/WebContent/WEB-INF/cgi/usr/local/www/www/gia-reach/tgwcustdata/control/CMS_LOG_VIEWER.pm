# (C) Telstra 2001
#
# Author: Robert Beverley (robb@telstra.net)
# Date: 10 September 2001
# File: CMS_LOG_VIEWER.pm
#
# This module allows users to view log files
#
# $Id: CMS_LOG_VIEWER.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $

package CMS_LOG_VIEWER;
use 5.6.1;
use strict;
use warnings;

use lib "../../modules";
use Exporter;

our @ISA = qw (Exporter);

sub start {

	warn $CMS::html_values{user}{value};
	warn $CMS::html_values{message_type}{value};
	warn $CMS::html_values{message}{value};

	$CMS::html_values{"log_entries"}{table}=CMS_DB::get_logs
		($CMS::html_values{user}{value}, 
		$CMS::html_values{message_type}{value}, 
		$CMS::html_values{message}{value});

	unshift @{$CMS::html_values{"log_entries"}{table}},
		["Timestamp", "Message Type", "User", "Message"];

	$CMS::html_values{"log_entries"}{header_rows}=1;
	CMS::output_html("log_viewer_display");

 } 

1;
