/*
 * XML Type:  getImpactPathResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * An XML getImpactPathResponse(@http://schemas.hp.com/ucmdb/1/params/impact).
 *
 * This is a complex type.
 */
public class GetImpactPathResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponse
{
    
    public GetImpactPathResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTPATHTOPOLOGY$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "ImpactPathTopology");
    private static final javax.xml.namespace.QName COMMENTS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "comments");
    
    
    /**
     * Gets the "ImpactPathTopology" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactTopology getImpactPathTopology()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactTopology target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactTopology)get_store().find_element_user(IMPACTPATHTOPOLOGY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ImpactPathTopology" element
     */
    public void setImpactPathTopology(com.hp.schemas.ucmdb._1.types.impact.ImpactTopology impactPathTopology)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactTopology target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactTopology)get_store().find_element_user(IMPACTPATHTOPOLOGY$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactTopology)get_store().add_element_user(IMPACTPATHTOPOLOGY$0);
            }
            target.set(impactPathTopology);
        }
    }
    
    /**
     * Appends and returns a new empty "ImpactPathTopology" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactTopology addNewImpactPathTopology()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactTopology target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactTopology)get_store().add_element_user(IMPACTPATHTOPOLOGY$0);
            return target;
        }
    }
    
    /**
     * Gets the "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments getComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "comments" element
     */
    public void setComments(com.hp.schemas.ucmdb._1.types.ResComments comments)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            }
            target.set(comments);
        }
    }
    
    /**
     * Appends and returns a new empty "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments addNewComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            return target;
        }
    }
}
