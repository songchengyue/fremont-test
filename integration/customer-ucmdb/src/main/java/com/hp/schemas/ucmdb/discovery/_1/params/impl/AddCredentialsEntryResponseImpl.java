/*
 * XML Type:  addCredentialsEntryResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML addCredentialsEntryResponse(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class AddCredentialsEntryResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.AddCredentialsEntryResponse
{
    
    public AddCredentialsEntryResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CREDENTIALSENTRYID$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "credentialsEntryID");
    
    
    /**
     * Gets the "credentialsEntryID" element
     */
    public java.lang.String getCredentialsEntryID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREDENTIALSENTRYID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "credentialsEntryID" element
     */
    public org.apache.xmlbeans.XmlString xgetCredentialsEntryID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CREDENTIALSENTRYID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "credentialsEntryID" element
     */
    public void setCredentialsEntryID(java.lang.String credentialsEntryID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREDENTIALSENTRYID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CREDENTIALSENTRYID$0);
            }
            target.setStringValue(credentialsEntryID);
        }
    }
    
    /**
     * Sets (as xml) the "credentialsEntryID" element
     */
    public void xsetCredentialsEntryID(org.apache.xmlbeans.XmlString credentialsEntryID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CREDENTIALSENTRYID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CREDENTIALSENTRYID$0);
            }
            target.set(credentialsEntryID);
        }
    }
}
