package com.telstra.netops.dns.server.model;

public class Success {

	//
	// Id of created object if there is one.
	// TODO - populate from CGI calls and add ability to GET individual objects
	//
	
	private String id;

	private String message;

	public Success() {
	}
	
	public Success(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
