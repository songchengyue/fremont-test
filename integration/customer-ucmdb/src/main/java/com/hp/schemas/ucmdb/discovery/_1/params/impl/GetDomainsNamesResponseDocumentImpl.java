/*
 * An XML document type.
 * Localname: getDomainsNamesResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getDomainsNamesResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetDomainsNamesResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponseDocument
{
    
    public GetDomainsNamesResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETDOMAINSNAMESRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDomainsNamesResponse");
    
    
    /**
     * Gets the "getDomainsNamesResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse getGetDomainsNamesResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse)get_store().find_element_user(GETDOMAINSNAMESRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getDomainsNamesResponse" element
     */
    public void setGetDomainsNamesResponse(com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse getDomainsNamesResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse)get_store().find_element_user(GETDOMAINSNAMESRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse)get_store().add_element_user(GETDOMAINSNAMESRESPONSE$0);
            }
            target.set(getDomainsNamesResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getDomainsNamesResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse addNewGetDomainsNamesResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainsNamesResponse)get_store().add_element_user(GETDOMAINSNAMESRESPONSE$0);
            return target;
        }
    }
}
