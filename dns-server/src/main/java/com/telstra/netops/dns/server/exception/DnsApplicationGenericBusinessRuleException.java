package com.telstra.netops.dns.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "There was a DNS business rule error!")
public class DnsApplicationGenericBusinessRuleException extends RuntimeException {
	
	private final Error error;
	
	public DnsApplicationGenericBusinessRuleException(String message) {
		super(message);
		this.error = new Error(message);
	}

	public Error getError() {
		return error;
	}

}
