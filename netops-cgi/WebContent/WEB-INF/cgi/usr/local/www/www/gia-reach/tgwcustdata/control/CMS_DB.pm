# Date: 12 July 2001
# File: CMS_DB.pm
#
# This module contains subroutines used to interact with the database
#
# $Id: CMS_DB.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $

package CMS_DB;
use Exporter;
use DBI;
use DBD::Oracle;
use warnings;
use strict;
#use LOGGER;

our @ISA = qw (Exporter);
our @EXPORT = qw ($dbh $dbh_tr);

my $error_message;
our $dbh;
our $dbh_tr;
#editted by abhijith
my $sid="NETOPSIT";
my $dbuser="netops";
my $passwd="netops";
my $host="192.168.31.208";
my $trace_level = 2;
use constant SUCCESS => 1;
use constant FAIL => 0;

# connects to database
sub connect_to_database {
  if (defined ($dbh)) { 
    return FAIL;
  } else { 
   	        	
    $dbh = DBI->connect("dbi:Oracle:host=$host;sid=$sid", $dbuser, $passwd);
	my $sth = $dbh->prepare("SET TIME_ZONE='+00:00'"); 
   $sth->execute;
    return SUCCESS;
  }
 
}

sub begin_transaction {
  # because of the stupid way DBI handles this, I have to start
  # a second connection

  if (defined ($dbh_tr)) {
    return FAIL;
  } else {
    my %options;
    $options{AutoCommit} = 0;
    $dbh_tr = DBI->connect("dbi:Oracle:host=$host;sid=$sid", $dbuser,$passwd,\%options);
    my $sqlQuery = $dbh_tr->prepare("SET TIME_ZONE='+00:00'");
    my $sth = $sqlQuery->execute;
    return SUCCESS;
  }
}

# disconnect from database
sub disconnect_from_database {
  $dbh->disconnect;
  undef $dbh;
}

# disconnect from transaction
sub disconnect_from_transaction {
  $dbh_tr->disconnect;
  undef $dbh_tr;
}

sub log {
  LOGGER::log($dbh, $CMS::html_values{username}{value}, @_);
}

sub get_logs {
  LOGGER::get_logs($dbh,@_);
}

1;
