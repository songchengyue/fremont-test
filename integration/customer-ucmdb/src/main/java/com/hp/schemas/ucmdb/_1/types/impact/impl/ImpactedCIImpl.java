/*
 * XML Type:  ImpactedCI
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactedCI
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML ImpactedCI(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ImpactedCIImpl extends com.hp.schemas.ucmdb._1.types.impl.ShallowCIImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactedCI
{
    
    public ImpactedCIImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
