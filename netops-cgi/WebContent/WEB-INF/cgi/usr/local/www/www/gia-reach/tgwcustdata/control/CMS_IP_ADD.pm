package CMS_IP_ADD;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_IP_ADD_DB;
use Mail::Sendmail;

our @ISA = qw (Exporter);
our $subj;
our $inet_val;
our $error;
our $chng_chk = "ipNOC\@team.telstra.com";
our $date_chk_val;
our $date_chk;
our $dt_chk;

#NIC Delete mail changes
our $assigned_date_chk_val;
our $assigned_date_chk;
our $assigned_dt_chk;

our $ip_blk_value;
our $ip_del_count = 1;
#our $temp_val_edit = "";

sub generate_prefix_list {
  my $bgp_ip = shift @_;
  my $found = 0;

  my @wday = qw {sun mon tue wed thu fri sat};
  my @month = qw {Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec};

  my $output_dir = $OS_PARA::values{data_path}{value}."routing/prefix_list/";
  my $prefix_list = "";

  my $ops = CMS_SERVICE_DB::get_service_by_bgp_ip($bgp_ip);

  for my $j (0 .. $#{$ops}) {
    my @curr_gtime = gmtime(time);
    my $opshandle = $ops->[$j][0];

    my $today = "$month[$curr_gtime[4]]$curr_gtime[3]";
    my $count = $curr_gtime[6];

    my $file_status = `ls -l $output_dir$wday[$curr_gtime[6]]/$opshandle`;
    my @seg_status = split(/\s+/, $file_status);

#$prefix_list .= "@curr_gtime<br>\n";
#$prefix_list .= "$today,$seg_status[5]$seg_status[6]<br>";

    if ($today ne $seg_status[5].$seg_status[6]) {
	$count = $curr_gtime[6]-1;
    }

    for my $i (0 .. 2) {
#      my $prefix_file = "$output_dir$wday[$curr_gtime[6]--]/$opshandle";

      if ($wday[$count-$i] eq "sat") {
  	   $count--;
      }

      my $prefix_file = "$output_dir$wday[$count-$i]/$opshandle";


#print "Content-type:html/text\n\n";
#print "$prefix_file<br>";

      if (-e $prefix_file) {
        $prefix_list .= "<b>Service: $opshandle</b><br>\n";
        open(PRE, "<$prefix_file");
        while(my $line = <PRE>) {
          chomp($line);
          $prefix_list .= "$line<br>\n";
        }
        close PRE;
        $prefix_list .= "<br><hr><br>";
        $found++;
      }
    }
  }

  $CMS::html_values{bgp_ip}{value} = "$bgp_ip";
  if ($found > 0) {
    $CMS::html_values{prefix_list}{value} = "$prefix_list";
    $CMS::html_values{noresultsmessage}{value} = "";
  } else {
    $CMS::html_values{noresultsmessage}{value} = "No prefix list found.";
  }
  CMS::output_html("service_prefix_list");
}

sub get_result {
  my @check_log = ("\n","password: ascot328","\n","password: ascot328","\n","inetnum:  $inet_val","\n","netname:  $CMS::html_values{netname}{value}","\n","descr:    $CMS::html_values{descr}{value}","\n","country:  $CMS::html_values{country}{value}","\n","admin-c:  $CMS::html_values{admin_c}{value}","\n","tech-c:   $CMS::html_values{tech_c}{value}","\n","status:   $CMS::html_values{status}{value}","\n","mnt-by:   $CMS::html_values{mnt_by}{value}","\n","mnt-irt:   $CMS::html_values{notify}{value}","\n","changed:  $chng_chk $dt_chk","\n","source:   $CMS::html_values{source}{value}","\n");

  my $firststring = join(",",@check_log);

  #send update log to TI.provision team
  if ($firststring ne "") {
     send_result($firststring);
  }
}

sub get_del_result {

my @check_log = ("\n","password: ascot328","\n","password: ascot328","\n","inetnum:  $inet_val","\n","netname:  $CMS::html_values{netname}{value}","\n","descr:    $CMS::html_values{descr}{value}","\n","country:  $CMS::html_values{country}{value}","\n","admin-c:  $CMS::html_values{admin_c}{value}","\n","tech-c:   $CMS::html_values{tech_c}{value}","\n","status:   $CMS::html_values{status}{value}","\n","mnt-by:   $CMS::html_values{mnt_by}{value}","\n","mnt-irt:   $CMS::html_values{notify}{value}","\n","changed:  $chng_chk $assigned_dt_chk","\n","source:   $CMS::html_values{source}{value}","\n","delete:   $chng_chk $dt_chk","\n" );

  my $firststring = join(",",@check_log);

  #send update log to TI.provision team
  if ($firststring ne "") {
     send_result($firststring);
  }
}

sub get_del_c3 {
my $i;
my $j;
my @check_log_c3;
my $firststring_c3;
my $records=0;
$subj = "GIA: IP block deleted,  associated service removed";
my @ip_deleted_c3 = @_;
@check_log_c3 =("ip-block $ip_blk_value is deleted form cms\n\n" );
if (@ip_deleted_c3)
{
push(@check_log_c3,"Following IP's removed from RDNS\n");
for ($j=0;$j<=$#ip_deleted_c3;$j=$j+2)
{
$ip_deleted_c3[$j] =~/(\d+)\.(\d+)\.(\d+).*/;
push(@check_log_c3,$3."\.".$2."\.".$1."\.".$ip_deleted_c3[$j+1]."\n");
$records++;
}
push(@check_log_c3,"$records updated\n");
}
else
{
push(@check_log_c3,"No records found in RDNS\n");
}
 $firststring_c3 = join(",",@check_log_c3);

  #send update log to TI.provision team
  if ($firststring_c3 ne "") {
     send_result_c3($firststring_c3);
  }
}

sub send_result_c3 {
  my $firststr = shift(@_);
  my @array = split("#",$firststr);
  my $message = shift(@array);
  $message =~ s/,//g;

  my $to_mail = "TI.DL-GL-TI_EM\@team.telstra.com";
  my $cc_mail = "TI.DL-GL-TI_EM\@team.telstra.com";
  my $from_mail = "ipnoc\@team.telstra.com";
  my %mail = (
           To => $to_mail,
           Cc => $cc_mail,
           From => $from_mail,
           Subject => $subj
  );

  $mail{Smtp} = 'postoffice';
  $mail{body} = $message;

  if (sendmail (%mail)) {
    print "\nMail sent success to $to_mail.\n" ;
  } else {
    print "Error sending mail to $to_mail: $Mail::Sendmail::error \n";
  }

}


sub send_result {
  my $firststr = shift(@_);
  my @array = split("#",$firststr);
  my $message = shift(@array);
  $message =~ s/,//g; 

  #my $to_mail = "Chi-wai.Lau\@team.telstra.com, Carmen.m.Chow\@team.telstra.com, Yuan.Jiun.Lai\@team.telstra.com";
  my $to_mail = "auto-dbm\@apnic.net";
  my $cc_mail = "TI.nic-register\@telstraglobal.com";
  my $from_mail = "TI.nic-register\@telstraglobal.com";
  #Commented out for domain name change from reach to telstraglobal 
  #my $cc_mail = "nic-register\@reach.com";
  #my $from_mail = "nic-register\@reach.com";
  my %mail = (
           To => $to_mail,
	   Cc => $cc_mail,
	   From => $from_mail,
           Subject => $subj
  );

  $mail{Smtp} = 'postoffice';
  $mail{body} = $message;

  if (sendmail (%mail)) {
    print "\nMail sent success to $to_mail.\n" ;
  } else {
    print "Error sending mail to $to_mail: $Mail::Sendmail::error \n";
  }

}


# load html page options 
#
sub get_html_options {
	$CMS::html_values{dns_service}{options} = CMS_IP_ADD_DB::get_dns_service_option();
	$CMS::html_values{ntp_service}{options} = CMS_IP_ADD_DB::get_ntp_service_option();
	$CMS::html_values{confirm}{options} = CMS_IP_ADD_DB::get_confirm_option();
	$CMS::html_values{search_dns_service}{options} = $CMS::html_values{dns_service}{options};
        unshift(@{$CMS::html_values{search_dns_service}{options}});
	$CMS::html_values{search_ntp_service}{options} = $CMS::html_values{ntp_service}{options};
        unshift(@{$CMS::html_values{search_ntp_service}{options}});
}

sub get_html_opt_search {
	$CMS::html_values{dns_service}{options} = CMS_IP_ADD_DB::get_dns_service_option();
	$CMS::html_values{ntp_service}{options} = CMS_IP_ADD_DB::get_ntp_service_option();
	$CMS::html_values{confirm}{options} = CMS_IP_ADD_DB::get_confirm_option();
	$CMS::html_values{search_dns_service}{options} = $CMS::html_values{dns_service}{options};
	unshift(@{$CMS::html_values{search_dns_service}{options}}, [""]);
	$CMS::html_values{search_ntp_service}{options} = $CMS::html_values{ntp_service}{options};
	unshift(@{$CMS::html_values{search_ntp_service}{options}}, [""]);
 }

#
# validate fields
#
sub validate_fields {
	# convert to lowercase and remove [, ] and SPACE
	$CMS::html_values{service}{value} =~ tr/A-Z/a-z/;
	$CMS::html_values{service}{value} =~ s/(\[|\])//g;

	# check mandatory fields
	if (       ($CMS::html_values{ip_block}{value} eq '')
		|| ($CMS::html_values{netname}{value} eq '')
		|| ($CMS::html_values{descr}{value} eq '')
		|| ($CMS::html_values{admin_c}{value} eq '')
		|| ($CMS::html_values{tech_c}{value} eq '')
		|| ($CMS::html_values{status}{value} eq '')
		|| ($CMS::html_values{mnt_by}{value} eq '')
		|| ($CMS::html_values{source}{value} eq '')
		|| ($CMS::html_values{service}{value} eq '')
	) {
		# Failed to add 
		CMS::add_error("Failed to add ip resource.");
		CMS::add_error("Required fields are not filled.");
		return 0;

	# check space
	} elsif (  ($CMS::html_values{ip_block}{value} =~ / /)
		|| ($CMS::html_values{service}{value} =~ / /)
		|| ($CMS::html_values{netname}{value} =~ / /)
		|| ($CMS::html_values{service}{value} =~ " ")
		) {

		# Failed to add 
		CMS::add_error("Failed to add ip resource.");
		CMS::add_error("Space is not allowed in following fields:");
		CMS::add_error("'IP Address Block', 'Service ID', 'Netname' ");
		return 0;

	# check format
	} elsif ($CMS::html_values{ip_block}{value} !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/ ) {
		CMS::add_error("Failed to add ip resource.");
		CMS::add_error("Invalid format in 'IP Address Block'");
		return 0;

        }

	return 1;
}

sub edit_val_fields {
        # convert to lowercase and remove [, ] and SPACE
        $CMS::html_values{service}{value} =~ tr/A-Z/a-z/;
        $CMS::html_values{service}{value} =~ s/(\[|\])//g;

        # check mandatory fields
        if (       ($CMS::html_values{ip_block}{value} eq '')
                || ($CMS::html_values{service}{value} eq '')
        ) {
                # Failed to add
                CMS::add_error("Failed to edit ip resource.");
                CMS::add_error("Required fields are not filled.");
                return 0;

        # check space
        } elsif (  ($CMS::html_values{ip_block}{value} =~ / /)
                || ($CMS::html_values{service}{value} =~ / /)
                || ($CMS::html_values{service}{value} =~ " ")
                ) {

                # Failed to add
                CMS::add_error("Failed to edit ip resource.");
                CMS::add_error("Space is not allowed in following fields:");
                CMS::add_error("'IP Address Block', 'Service ID'");
                return 0;

        # check format
        } elsif ($CMS::html_values{ip_block}{value} !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/ ) {
                CMS::add_error("Failed to edit ip resource.");
                CMS::add_error("Invalid format in 'IP Address Block'");
                return 0;

        }
        return 1;
}
sub list_ipadd {
       $CMS::html_values{ipadds}{table} = CMS_IP_ADD_DB::list_ipadd(@_);
       unshift (@{$CMS::html_values{ipadds}{table}}, [
	"IP Address Block", "Customer", "Service ID", "DNS Service", "NTP Service", "Remark"
	, "Assigned Date (Record Creation)", "Last Update Date (By User)"
	, "Router", "Interface"
	] );

       $CMS::html_values{ipadds}{header_rows} = 1;
       CMS::output_html("ipadd_list2", "csv");
}

sub reports_ipadd {
       $CMS::html_values{ipadds}{table} = CMS_IP_ADD_DB::get_reports_ipadd(@_);
       unshift (@{$CMS::html_values{ipadds}{table}}, [
	"Netname", "IP Address*", "Subnet Mask", "Hosts now", "Hosts in 6 months", "Hosts in 1 year"
	, "Subnets now", "Subnets in 6 months", "Subnets in 1 year"
	, "IP Assign Date" ]);

       $CMS::html_values{ipadds}{header_rows} = 1;
       CMS::output_html("ipadd_reports3", "csv");
}

sub utilrep_ipadd {
        $CMS::html_values{ipadds}{table} = CMS_IP_ADD_DB::get_Ureports_ipadd(@_);
        unshift (@{$CMS::html_values{ipadds}{table}}, [
               "IPBlock","Utilisation"]);

       $CMS::html_values{ipadds}{header_rows} = 1;

       CMS::output_html("ipadd_reports3", "csv");
}

sub search_ipadd {
        $CMS::html_values{ipadds}{table} = CMS_IP_ADD_DB::search_ipadd(
				$CMS::html_values{search_ip_block}{value}
				,$CMS::html_values{search_accno}{value}
				,$CMS::html_values{search_service}{value}
				,$CMS::html_values{search_dns_service}{value}
				,$CMS::html_values{search_ntp_service}{value}
				,$CMS::html_values{search_remark}{value}
				,$CMS::html_values{assignedip}{value}
				);
		if ($CMS::html_values{assignedip}{value} eq 'y'){
			unshift (@{$CMS::html_values{ipadds}{table}}, [
				"IP Address Block", "Customer", "Service ID", "DNS Service", "NTP Service", "Remark"
				, "Assigned Date (Record Creation)", "Last Update Date (By User)"
				, "Netname", "Mask"
				, "Hosts now", "Hosts in 6 months", "Hosts in 1 year"
				, "Subnets now", "Subnets in 6 months", "Subnets in 1 year"
				, "inetnum", "descr", "country", "admin-c", "tech-c", "rev-srv", "status", "remarks", "mnt-irt"				, "mnt-by", "mnt-lower", "mnt-routes", "changed", "source"
				] ); 	

       $CMS::html_values{ipadds}{header_rows} = 1;
       CMS::output_html("ipadd_search2", "csv");
		}#end of if 'y'

		if ($CMS::html_values{assignedip}{value} eq 'n'){
                        unshift (@{$CMS::html_values{ipadds}{table}}, [
				"IP Address Block"
				]);
		$CMS::html_values{ipadds}{header_rows} = 1;
		CMS::output_html("ipadd_search2", "csv");
		}#end of if 'n'
		
		if ($CMS::html_values{assignedip}{value} eq 'b'){
 		       unshift (@{$CMS::html_values{ipadds}{table}}, [
                		"IP Address Block","Status"
                		]);
		$CMS::html_values{ipadds}{header_rows} = 1;
		CMS::output_html("ipadd_search2", "csv");
		}#end of if 'b'
}

sub start {

##        # check billing options
	my $billerrmsg = '';
##        if ($CMS::html_values{'billable'}{value} <= 0) {
##                $billerrmsg .= "Invalid Billable<br>";
##        }
##        # xxxdateok
##        # 0 = FAIL, 1 = OK, 2 = ALL NULL
##        my $billstartdateok = 0;
##        my $billenddateok = 0;
##        my ($tmpsd, $tmped);
##        # start date
##        $tmpsd  = $CMS::html_values{'billstartdate.year'}{value};
##        $tmpsd .= $CMS::html_values{'billstartdate.month'}{value};
##        $tmpsd .= $CMS::html_values{'billstartdate.day'}{value};
##        #$tmpsd .= $CMS::html_values{'billstarttime.hour'}{value};
##        #$tmpsd .= $CMS::html_values{'billstarttime.minute'}{value};
##        #$tmpsd .= $CMS::html_values{'billstarttime.second'}{value};
##        #if ($tmpsd =~ /^--------------$/) {
##        if ($tmpsd =~ /^--------$/) {
##                $billstartdateok = 2;
##        } elsif ($tmpsd =~ /-/) {
##                $billstartdateok = 0;
##        } else {
##                $billstartdateok = 1;
##        }
##        # end date
##        $tmped  = $CMS::html_values{'billenddate.year'}{value};
##        $tmped .= $CMS::html_values{'billenddate.month'}{value};
##        $tmped .= $CMS::html_values{'billenddate.day'}{value};
##        #$tmped .= $CMS::html_values{'billendtime.hour'}{value};
##        #$tmped .= $CMS::html_values{'billendtime.minute'}{value};
##        #$tmped .= $CMS::html_values{'billendtime.second'}{value};
##        #if ($tmped =~ /^--------------$/) {
##        if ($tmped =~ /^--------$/) {
##                $billenddateok = 2;
##        } elsif ($tmped =~ /-/) {
##                $billenddateok = 0;
##        } else {
##                $billenddateok = 1;
##        }
##        #
##        #if ( ($billstartdateok==1) && ($billenddateok==1) && ($tmped <= $tmpsd) ) {
##        if ( ($billstartdateok==1) && ($billenddateok==1) && ($tmped < $tmpsd) ) {
##                $billerrmsg .= "Invalid Billing End Date &amp; Time - it must be later than Billing Start Date &amp; Time<br>";
##        }
##        if ( $billstartdateok==0 ) {
##                $billerrmsg .= "Invalid Billing Start Date &amp; Time<br>";
##        }
##        if ( $billenddateok==0 ) {
##                $billerrmsg .= "Invalid Billing End Date &amp; Time<br>";
##        }
##        if ($CMS::html_values{'billable'}{value} == 1) {
##                if ( $billstartdateok == 2 ) {
##                        $billerrmsg .= "Invalid Billing Start Date &amp; Time - it must be filled when service is usage based billing<br>";
##                }
##        }


	# No command defined  
	if ((not defined($CMS::html_values{command}{value})) ||
		((defined $CMS::html_values{submit}{value}) &&
		($CMS::html_values{submit}{value} =~ /cancel/i))) {
		# default to add ip resource

		CMS::clear_html_values();
		$CMS::html_values{subsystem}{value} = "ipadd";
		$CMS::html_values{command}{value} = "add_ipadd";
	}

	if (defined $CMS::html_values{stage}{value}) {
		unless (CMS::check_screen($CMS::html_values{current_screen}{value})){
			$CMS::html_values{stage}{value}--;
		}
	}

	# add new ip resource
	if ($CMS::html_values{command}{value} eq "add_ipadd") {

		if (not defined ($CMS::html_values{stage}{value})) {
		# first stage

			&get_html_options();
			CMS::output_html("ipadd_add");

		} elsif ($CMS::html_values{stage}{value} == 2) {

			if (! &validate_fields()) {

				&get_html_options();
				CMS::output_html("ipadd_add");

			} else {

				#initial adding of IP
                                my $return_result = CMS_IP_ADD_DB::add_ipadd (
                                        $CMS::html_values{ip_block}{value}
                                        ,$CMS::html_values{service}{value}
                                        ,$CMS::html_values{dns_service}{value}
                                        ,$CMS::html_values{ntp_service}{value}
                                        ,$CMS::html_values{remark}{value}

                                        ,$CMS::html_values{netname}{value}
                                        ,$CMS::html_values{hosts_now}{value}
                                        ,$CMS::html_values{hosts_in_6_months}{value}
                                        ,$CMS::html_values{hosts_in_1_year}{value}
                                        ,$CMS::html_values{subnets_now}{value}
                                        ,$CMS::html_values{subnets_in_6_months}{value}
                                        ,$CMS::html_values{subnets_in_1_year}{value}

                                        ,$CMS::html_values{descr}{value}
                                        ,$CMS::html_values{admin_c}{value}
                                        ,$CMS::html_values{tech_c}{value}
                                        ,$CMS::html_values{rev_srv}{value}
                                        ,$CMS::html_values{status}{value}
                                        ,$CMS::html_values{apnic_remarks}{value}
                                        ,$CMS::html_values{notify}{value}
                                        ,$CMS::html_values{mnt_by}{value}
                                        ,$CMS::html_values{mnt_lower}{value}
                                        ,$CMS::html_values{mnt_routes}{value}
                                        ,$CMS::html_values{source}{value}
                                );

				if ($return_result == 1 ) {

				# have added service
				my $result = CMS_IP_ADD_DB::get_ipadd ($CMS::html_values{ip_block}{value}
				,$CMS::html_values{service}{value});

                                ($CMS::html_values{accno}{value}
                             #   ,$CMS::html_values{service}{value}
                                ,$CMS::html_values{dns_service}{value}
                                ,$CMS::html_values{ntp_service}{value}
                                ,$CMS::html_values{remark}{value}

                                ,$CMS::html_values{netname}{value}
                                ,$CMS::html_values{mask}{value}
                                ,$CMS::html_values{hosts_now}{value}
                                ,$CMS::html_values{hosts_in_6_months}{value}
                                ,$CMS::html_values{hosts_in_1_year}{value}
                                ,$CMS::html_values{subnets_now}{value}
                                ,$CMS::html_values{subnets_in_6_months}{value}
                                ,$CMS::html_values{subnets_in_1_year}{value}

                                ,$CMS::html_values{inetnum}{value}
                                ,$CMS::html_values{inet_num_name}{value}
                                ,$CMS::html_values{descr}{value}
                                ,$CMS::html_values{country}{value}
                                ,$CMS::html_values{admin_c}{value}
                                ,$CMS::html_values{tech_c}{value}
                                ,$CMS::html_values{rev_srv}{value}
                                ,$CMS::html_values{status}{value}
                                ,$CMS::html_values{apnic_remarks}{value}
                                ,$CMS::html_values{notify}{value}
                                ,$CMS::html_values{mnt_by}{value}
                                ,$CMS::html_values{mnt_lower}{value}
                                ,$CMS::html_values{mnt_routes}{value}
                                ,$CMS::html_values{changed}{value}
                                ,$CMS::html_values{source}{value}
				,$CMS::html_values{date_assigned}{value}
                                ) = @$result;

					$inet_val = $CMS::html_values{inetnum}{value};

					$date_chk_val = CMS_IP_ADD_DB::get_date_current ($CMS::html_values{ip_block}{value});
					$date_chk = $date_chk_val->[0]->[0];
                                        my @check_date = split(/ /,$date_chk);
                                        $dt_chk = shift(@check_date);
                                        $dt_chk =~ s/-//g;

					CMS::add_error("IP resource added successfully and Email notification sent.");
					&get_html_options();
					CMS::output_html("ipadd_add2");
                                        if ($CMS::html_values{source}{value} eq "APNIC" ) {
					#$subj = 'ADD INETNUM OBJECT';
					$subj = "Network $inet_val";
					&get_result();
					}
				} else {
					# Failed to add ip resource
					CMS::add_error("Failed to add ip resource.");
					CMS::add_error("$return_result");
					CMS::add_error($CMS_IP_ADD_DB::error);
					&get_html_options();
					CMS::output_html("ipadd_add");
				}
			} # end of data checking
		} # end of stage value checking


	# edit ip address resource
	} elsif ($CMS::html_values{command}{value} eq "edit_ipadd") {
    
		if (not defined ($CMS::html_values{stage}{value})) {
		# first stage
      
			CMS::output_html("ipadd_edit");

		} elsif ($CMS::html_values{stage}{value} == 2) {

        # check mandatory fields
                        if (! &edit_val_fields()) {

                                &get_html_options();
                                CMS::output_html("ipadd_edit");

                         } else {

			if (my $res1 = CMS_IP_ADD_DB::edit_ip_existval ($CMS::html_values{ip_block}{value}
				,$CMS::html_values{service}{value}) ) {

				($CMS::html_values{accno}{value}
#				,$CMS::html_values{service}{value}
				,$CMS::html_values{dns_service}{value}
				,$CMS::html_values{ntp_service}{value}
				,$CMS::html_values{remark}{value}

				,$CMS::html_values{netname}{value}
				,$CMS::html_values{mask}{value}
				,$CMS::html_values{hosts_now}{value}
				,$CMS::html_values{hosts_in_6_months}{value}
				,$CMS::html_values{hosts_in_1_year}{value}
				,$CMS::html_values{subnets_now}{value}
				,$CMS::html_values{subnets_in_6_months}{value}
				,$CMS::html_values{subnets_in_1_year}{value}

				,$CMS::html_values{inetnum}{value}
				,$CMS::html_values{inet_num_name}{value}
				,$CMS::html_values{descr}{value}
				,$CMS::html_values{country}{value}
				,$CMS::html_values{admin_c}{value}
				,$CMS::html_values{tech_c}{value}
				,$CMS::html_values{rev_srv}{value}
				,$CMS::html_values{status}{value}
				,$CMS::html_values{apnic_remarks}{value}
				,$CMS::html_values{notify}{value}
				,$CMS::html_values{mnt_by}{value}
				,$CMS::html_values{mnt_lower}{value}
				,$CMS::html_values{mnt_routes}{value}
				,$CMS::html_values{changed}{value}
				,$CMS::html_values{source}{value}
				,$CMS::html_values{date_assigned}{value}
				) = @$res1;

				&get_html_options();
				CMS::output_html("ipadd_edit2");

			} else {
				CMS_IP_ADD_DB::delete_edited_val();
				# service does not exist
				CMS::add_error("IP Address Block/Service ID combination does not exist.");
				CMS::output_html("ipadd_edit");
      			}
			}

		} elsif ($CMS::html_values{stage}{value} == 3) {

			if (! &validate_fields() ) {

				&get_html_options();
				CMS::output_html("ipadd_edit2");

			} else {

open (DEBUG, ">/tmp/h999755_ipedit");
print DEBUG "ADD.pm file - STAGE 3: ip_block: $CMS::html_values{ip_block}{value}\t service: $CMS::html_values{service}{value}\n";
close (DEBUG);

				my $return_result = CMS_IP_ADD_DB::update_ipadd (
					 $CMS::html_values{ip_block}{value}
					,$CMS::html_values{accno}{value}
					,$CMS::html_values{service}{value}
					,$CMS::html_values{dns_service}{value}
					,$CMS::html_values{ntp_service}{value}
					,$CMS::html_values{remark}{value}
	
					,$CMS::html_values{netname}{value}
					,$CMS::html_values{mask}{value}
					,$CMS::html_values{hosts_now}{value}
					,$CMS::html_values{hosts_in_6_months}{value}
					,$CMS::html_values{hosts_in_1_year}{value}
					,$CMS::html_values{subnets_now}{value}
					,$CMS::html_values{subnets_in_6_months}{value}
					,$CMS::html_values{subnets_in_1_year}{value}
	
					,$CMS::html_values{inetnum}{value}
					,$CMS::html_values{inet_num_name}{value}
					,$CMS::html_values{descr}{value}
					,$CMS::html_values{country}{value}
					,$CMS::html_values{admin_c}{value}
					,$CMS::html_values{tech_c}{value}
					,$CMS::html_values{rev_srv}{value}
					,$CMS::html_values{status}{value}
					,$CMS::html_values{apnic_remarks}{value}
					,$CMS::html_values{notify}{value}
					,$CMS::html_values{mnt_by}{value}
					,$CMS::html_values{mnt_lower}{value}
					,$CMS::html_values{mnt_routes}{value}
					,$CMS::html_values{changed}{value}
					,$CMS::html_values{source}{value}
					);
open (DEBUG, ">/tmp/h999755_ipedit");
print DEBUG "ADD.pm file - RESULT of update function: $return_result\n";
close (DEBUG);
				if ($return_result == 1 ) {

				# have edited ip resource
open (DEBUG, ">/tmp/h999755_ipedit");
print DEBUG "ADD.pm file - sevice: $CMS::html_values{service}{value} \n";
                           	     my $result = CMS_IP_ADD_DB::get_ipadd ($CMS::html_values{ip_block}{value}
					,$CMS::html_values{service}{value});
print DEBUG "ADD.pm file - result of get_ipadd $result \n";
close (DEBUG);
                                	($CMS::html_values{accno}{value}
             #                   	,$CMS::html_values{service}{value}
                                	,$CMS::html_values{dns_service}{value}
                                	,$CMS::html_values{ntp_service}{value}
                                	,$CMS::html_values{remark}{value}

                                	,$CMS::html_values{netname}{value}
                                	,$CMS::html_values{mask}{value}
                                	,$CMS::html_values{hosts_now}{value}
                                	,$CMS::html_values{hosts_in_6_months}{value}
                                	,$CMS::html_values{hosts_in_1_year}{value}
                                	,$CMS::html_values{subnets_now}{value}
                                	,$CMS::html_values{subnets_in_6_months}{value}
                                	,$CMS::html_values{subnets_in_1_year}{value}

                                	,$CMS::html_values{inetnum}{value}
                                	,$CMS::html_values{inet_num_name}{value}
                                	,$CMS::html_values{descr}{value}
                                	,$CMS::html_values{country}{value}
                                	,$CMS::html_values{admin_c}{value}
                                	,$CMS::html_values{tech_c}{value}
                                	,$CMS::html_values{rev_srv}{value}
                                	,$CMS::html_values{status}{value}
                                	,$CMS::html_values{apnic_remarks}{value}
                                	,$CMS::html_values{notify}{value}
                                	,$CMS::html_values{mnt_by}{value}
                                	,$CMS::html_values{mnt_lower}{value}
                                	,$CMS::html_values{mnt_routes}{value}
                                	,$CMS::html_values{changed}{value}
                                	,$CMS::html_values{source}{value}
                                	,$CMS::html_values{date_assigned}{value}
                                	) = @$result;

					$inet_val = $CMS::html_values{inetnum}{value};

                                        $date_chk_val = CMS_IP_ADD_DB::get_date_current ($CMS::html_values{ip_block}{value});
                                        $date_chk = $date_chk_val->[0]->[0];
                                        my @check_date = split(/ /,$date_chk);
                                        $dt_chk = shift(@check_date);
                                        $dt_chk =~ s/-//g;

					CMS_IP_ADD_DB::delete_edited_val();
					CMS::add_error("IP resource edited successfully and Email notification sent.");
					&get_html_options();
					CMS::output_html("ipadd_edit3");

                                        if ($CMS::html_values{source}{value} eq "APNIC" ) {
					#$subj = 'EDIT INETNUM OBJECT';
                                        $subj = "Network $inet_val";
					&get_result();	

					}
				} else {
					# Failed to edit service
					CMS::add_error("Failed to edit ip resource.");
					CMS::add_error("$return_result");
					CMS::add_error($CMS_IP_ADD_DB::error);

					&get_html_options();
					CMS::output_html("ipadd_edit2");
				}
			}
		}

	# delete ip resource
	} elsif ($CMS::html_values{command}{value} eq "delete_ipadd") {
    
		if (not defined ($CMS::html_values{stage}{value})) {
		# first stage
      
			CMS::output_html("ipadd_delete");

		} elsif ($CMS::html_values{stage}{value} == 2) {

			if (my $result = CMS_IP_ADD_DB::get_ipadd ($CMS::html_values{ip_block}{value}
				,$CMS::html_values{service}{value})
				) {
				($CMS::html_values{accno}{value}
#				,$CMS::html_values{service}{value}
				,$CMS::html_values{dns_service}{value}
				,$CMS::html_values{ntp_service}{value}
				,$CMS::html_values{remark}{value}

				,$CMS::html_values{netname}{value}
				,$CMS::html_values{mask}{value}
				,$CMS::html_values{hosts_now}{value}
				,$CMS::html_values{hosts_in_6_months}{value}
				,$CMS::html_values{hosts_in_1_year}{value}
				,$CMS::html_values{subnets_now}{value}
				,$CMS::html_values{subnets_in_6_months}{value}
				,$CMS::html_values{subnets_in_1_year}{value}

				,$CMS::html_values{inetnum}{value}
				,$CMS::html_values{inet_num_name}{value}
				,$CMS::html_values{descr}{value}
				,$CMS::html_values{country}{value}
				,$CMS::html_values{admin_c}{value}
				,$CMS::html_values{tech_c}{value}
				,$CMS::html_values{rev_srv}{value}
				,$CMS::html_values{status}{value}
				,$CMS::html_values{apnic_remarks}{value}
				,$CMS::html_values{notify}{value}
				,$CMS::html_values{mnt_by}{value}
				,$CMS::html_values{mnt_lower}{value}
				,$CMS::html_values{mnt_routes}{value}
				,$CMS::html_values{changed}{value}
				,$CMS::html_values{source}{value}
				,$CMS::html_values{date_assigned}{value}
				) = @$result;

				&get_html_options();
				CMS::output_html("ipadd_delete2");

			} else {
				# service does not exist
				CMS::add_error("IP Address Block/Service ID combination  does not exist.");
				CMS::output_html("ipadd_delete");
      			}


		} elsif ($CMS::html_values{stage}{value} == 3) {

			if ($CMS::html_values{confirm}{value} ne 'YES') {
				# user not confirm

				&get_html_options();
				CMS::add_error("Failed to delete ip resource.");
				CMS::add_error("Please select YES if confirm.");
				CMS::output_html("ipadd_delete2");

			} else {
                                $assigned_date_chk_val = CMS_IP_ADD_DB::get_date_assigned ($CMS::html_values{ip_block}{value});
                                $assigned_date_chk = $assigned_date_chk_val->[0]->[0];
				
                                $ip_blk_value = $CMS::html_values{ip_block}{value};
                                $date_chk_val = CMS_IP_ADD_DB::get_date_current ($CMS::html_values{ip_block}{value});
                                $date_chk = $date_chk_val->[0]->[0];

				my ($return_result, $del_value) = CMS_IP_ADD_DB::delete_ipadd (
					 $CMS::html_values{ip_block}{value},
					 $CMS::html_values{service}{value}
					);

				if ($return_result == 1 ) {
					# have deleted ip resource

					$inet_val = $CMS::html_values{inetnum}{value};

                                        my @check_date = split(/ /,$date_chk);
                                        $dt_chk = shift(@check_date);
                                        $dt_chk =~ s/-//g;

					my @assigned_check_date = split(/ /,$assigned_date_chk);
                                        $assigned_dt_chk = shift(@assigned_check_date);
                                        $assigned_dt_chk =~ s/-//g;

					CMS::add_error("IP resource $CMS::html_values{ip_block}{value}, Service ID $CMS::html_values{service}{value} combination deleted successfully  and Email notification sent.");
					# &get_html_options();
					# $CMS::html_values{stage}{value} = 2;  # reload data from database
                                        my $ipblock_c3=$CMS::html_values{ip_block}{value};
					$CMS::html_values{ip_block}{value} = "";
					$CMS::html_values{service}{value}= "";
					$CMS::html_values{'previous.ip_block'}{value} = "";
					$CMS::html_values{'previous.serviceid'}{value}= "";
					CMS::output_html("ipadd_delete");
                                        if ( ($CMS::html_values{source}{value} eq "APNIC" ) && ($del_value == 1) ) {
					#$subj = 'INETNUM OBJECT DELETION';
				$ip_del_count = CMS_IP_ADD_DB::get_ip_block_count ($CMS::html_values{ip_block}{value});
					if ($ip_del_count == 0) {
                                        my @deleted_ipblock_c3 = CMS_IP_ADD_DB::delete_ip_block_reverse($ipblock_c3);
					&get_del_c3(@deleted_ipblock_c3);
					$subj = "Network $inet_val";
					&get_del_result();
					} else {
					$subj = "Network $inet_val";
					&get_del_result();
					 }
					}
				} else {
					# Failed to delete service
					CMS::add_error("Failed to delete ip resource.");
					#CMS::add_error("$return_result");
					#CMS::add_error($CMS_IP_ADD_DB::error);

					&get_html_options();
					CMS::output_html("ipadd_delete2");
				}
			}
		}

	# list ip resource
	} elsif ($CMS::html_values{command}{value} eq "list_ipadd") {

                if (not defined ($CMS::html_values{stage}{value})) {
			# first stage

			$CMS::html_values{ipadds}{table} = CMS_IP_ADD_DB::list_ipadd();
			unshift (@{$CMS::html_values{ipadds}{table}}, [
				"IP Address Block", "Customer", "Service ID", "DNS Service", "NTP Service", "Remark"
				, "Assigned Date (Record Creation)", "Last Update Date (By User)"
			 	, "Router", "Interface"
				] );
      			$CMS::html_values{ipadds}{header_rows} = 1;

			CMS::output_html("ipadd_list");

                } elsif ($CMS::html_values{stage}{value} == 2) {
			list_ipadd();
		}

        # REPORTS
        } elsif ($CMS::html_values{command}{value} eq "reports_ipadd") {

                if (not defined ($CMS::html_values{stage}{value})) {
                # first stage

                        CMS::add_error("Click below to display reports.");
                        CMS::output_html("ipadd_reports");

                } elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "NIC Audit Report")) {

                        CMS::add_error("NIC AUDIT Report generated successfully");

                        $CMS::html_values{ipadds}{table} = CMS_IP_ADD_DB::get_reports_ipadd();
                        unshift (@{$CMS::html_values{ipadds}{table}}, [
			"Netname", "IP Address*", "Subnet Mask", "Hosts now", "Hosts in 6 months", "Hosts in 1 year"
			, "Subnets now", "Subnets in 6 months", "Subnets in 1 year"
			, "IP Assign Date"
			]);
                        $CMS::html_values{ipadds}{header_rows} = 1;

                        CMS::output_html("ipadd_reports2");

		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Utilization Report")) {

                         CMS::add_error("Utilization Report generated successfully");
		
                         $CMS::html_values{ipadds}{table} = CMS_IP_ADD_DB::get_Ureports_ipadd(@_);

                         unshift (@{$CMS::html_values{ipadds}{table}}, [
				"IPBlock","Utilisation" ]);
                         $CMS::html_values{ipadds}{header_rows} = 1;

                         CMS::output_html("ipadd_reports4");

                 } elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Export NIC Audit Report To Excel")) {
                       reports_ipadd();

                 } elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Export Util Report To Excel")) {

                       utilrep_ipadd();
                 }

	# search ip resource
	} elsif ($CMS::html_values{command}{value} eq "search_ipadd") {
	                        if (not defined ($CMS::html_values{stage}{value})) {
                &get_html_opt_search();
                CMS::output_html("ipadd_search");

		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Search")) {

			$CMS::html_values{ipadds}{table} = CMS_IP_ADD_DB::search_ipadd(
				 $CMS::html_values{search_ip_block}{value}
				,$CMS::html_values{search_accno}{value}
				,$CMS::html_values{search_service}{value}
				,$CMS::html_values{search_dns_service}{value}
				,$CMS::html_values{search_ntp_service}{value}
				,$CMS::html_values{search_remark}{value}
				,$CMS::html_values{assignedip}{value}
				);
				if ($CMS::html_values{assignedip}{value} eq 'n'){
				unshift (@{$CMS::html_values{ipadds}{table}}, [
				"IP Address Block"]);
			 	$CMS::html_values{ipadds}{header_rows} = 1;

                &get_html_opt_search();
                CMS::output_html("ipadd_search");	
				}
	if ($CMS::html_values{assignedip}{value} eq 'y'){		
	unshift (@{$CMS::html_values{ipadds}{table}}, [
				"IP Address Block", "Customer", "Service ID", "DNS Service", "NTP Service", "Remark"
				, "Assigned Date (Record Creation)", "Last Update Date (By User)"
				, "Netname", "Mask"
				, "Hosts now", "Hosts in 6 months", "Hosts in 1 year"
				, "Subnets now", "Subnets in 6 months", "Subnets in 1 year"
				, "inetnum", "descr", "country", "admin-c", "tech-c", "rev-srv", "status", "remarks", "mnt-irt", "mnt-by", "mnt-lower", "mnt-routes", "changed", "source"
				] );
      			$CMS::html_values{ipadds}{header_rows} = 1;

                &get_html_opt_search();
                CMS::output_html("ipadd_search");
		}
	if ($CMS::html_values{assignedip}{value} eq 'b'){
	 unshift (@{$CMS::html_values{ipadds}{table}}, [
	 "IP Address Block", "Status"]);
	 $CMS::html_values{ipadds}{header_rows} = 1;
	 &get_html_opt_search();
	 CMS::output_html("ipadd_search");
	 }	
		} 
elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Clear search parameters" )) {
			
                        # first stage

                        # reset parameters
                        $CMS::html_values{search_ip_block}{value} = "";
                        $CMS::html_values{search_accno}{value} = "";
                        $CMS::html_values{search_service}{value} = "";
                        $CMS::html_values{search_dns_service}{value} = "";
                        $CMS::html_values{search_ntp_service}{value} = "";
                        $CMS::html_values{search_remark}{value} = "";
			$CMS::html_values{assignedip}{value} = "";

                &get_html_opt_search();
                CMS::output_html("ipadd_search");

		} elsif ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Export To Excel" )) { 
                        search_ipadd();
		}

       
  } elsif ($CMS::html_values{command}{value} eq "delete_service") {
    # delete a new service
    
    if (not defined ($CMS::html_values{stage}{value})) {
      # first stage
      
      CMS::output_html("service_delete");
    } elsif ($CMS::html_values{stage}{value} == 2) {
      
      if (CMS_SERVICE_DB::delete_service
	  ($CMS::html_values{service}{value},
	     $CMS::html_values{accno}{value})) {
	# have deleted service
       
	CMS::add_error("Service deleted successfully.");
	CMS::output_html("service_delete");
	
      } else {
	# Failed to delete service
	CMS::add_error("Failed to delete service.");
	CMS::add_error($CMS_SERVICE_DB::error);
	  CMS::output_html("service_delete");
      }
    }
  } elsif ($CMS::html_values{command}{value} eq "view_prefix_list") {
    generate_prefix_list($CMS::html_values{view_bgp_ip}{value});
  } elsif ($CMS::html_values{command}{value} eq "search_service") {
    if ((defined $CMS::html_values{submit}{value}) &&
	($CMS::html_values{submit}{value} eq "Search")) {

      $CMS::html_values{services}{table}
	= CMS_SERVICE_DB::search_service
	  ($CMS::html_values{search_serviceid}{value},
	 $CMS::html_values{search_accno}{value},
	 $CMS::html_values{search_description}{value},
	 $CMS::html_values{search_bandwidth}{value},
	 $CMS::html_values{search_productcode}{value},
	 $CMS::html_values{search_as}{value},
         $CMS::html_values{search_irrd}{value},
         $CMS::html_values{search_serviceid_alt}{value},
	 $CMS::html_values{search_aend_jobid}{value},
	 $CMS::html_values{search_zend_jobid}{value},
	 $CMS::html_values{search_aend_jobno}{value},
	 $CMS::html_values{search_zend_jobno}{value}         
        );

      foreach my $request (@{$CMS::html_values{services}{table}}) {
##         if ($$request[7] ne "") {
         if ($$request[7]) {
            $$request[11] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=service&command=view_prefix_list&view_bgp_ip=$$request[8]\" target=\"new_win\">View</a>";
         } 
      }

##      unshift (@{$CMS::html_values{services}{table}}, 
##	       ["Service", "Customer", "Description", "Bandwidth", "Product Code", "Router", "Interface", "AS No.", "BGP Neighbor IP", "Auto Prefix List", "IRRD Object", "Prefix List", "Aggregate Service", "Gold ClassMap", "Gold RateACL", "Silver ClassMap", "Silver RateACL", "Default ClassMap", "Default RateACL", "Usage Based Billing", "Billing Start", "Billing End"]);
      unshift (@{$CMS::html_values{services}{table}},
               ["Service", "Customer", "Description", "Bandwidth", "Product Code", "Router", "Interface", "AS No.", "BGP Neighbor IP", "Auto Prefix List", "IRRD Object", "Prefix List", "Aggregate Service", "IPVPN Polling Type", "Voice/Gold [IN] RateACL No. / ClassMap Name", "Voice/Gold [OUT] ClassMap Name", "Video/Silver Plus [IN] RateACL No. / ClassMap Name", "Video/Silver Plus [OUT] ClassMap Name", "Critical/Silver [IN] RateACL No. / ClassMap Name", "Critical/Silver [OUT] ClassMap Name", "Interactive Data (Telstra only) [IN] RateACL No. / ClassMap Name", "Interactive Data (Telstra only) [OUT] ClassMap Name", "Standard (Telstra only) [IN] RateACL No. / ClassMap Name", "Standard (Telstra only) [OUT] ClassMap Name", "Low Priority Data/Bronze [IN] RateACL No. / ClassMap Name", "Low Priority Data/Bronze [OUT] ClassMap Name", "Usage Based Billing", "Billing Start", "Billing End", "ATM/FR Service ID", "GBS A-End Job ID", "GBS Z-End Job ID", "GBS A-End Job Note", "GBS Z-End Job Note"]);
      $CMS::html_values{services}{header_rows} = 1;

      $CMS::html_values{search_productcode}{options} = CMS_SERVICE_DB::get_productcode();
      unshift(@{$CMS::html_values{search_productcode}{options}}, [""]);

    } else {
      $CMS::html_values{search_serviceid}{value} = "";
      $CMS::html_values{search_accno}{value} = "";
      $CMS::html_values{search_description}{value} = "";
      $CMS::html_values{search_bandwidth}{value} = "";
      $CMS::html_values{search_productcode}{options} = CMS_SERVICE_DB::get_productcode();
      unshift(@{$CMS::html_values{search_productcode}{options}}, [""]);
      $CMS::html_values{search_productcode}{value} = "";
      $CMS::html_values{search_as}{value} = "";
    }
    
    CMS::output_html("service_search");
  }
} 
  
1;
