package com.telstra.netops.routemanagement.server.model.cgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telstra.netops.routemanagement.server.model.MultiplePrefixUploadRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MultiplePrefixRequest {

	private static final Logger LOGGER = LoggerFactory.getLogger(MultiplePrefixRequest.class);

	private static final String SUBMIT = "RR_SUBMIT";

	@JsonProperty
	private Integer level;

	@JsonProperty("svc")
	private String service;

	@JsonProperty("et")
	private String product;

	@JsonProperty("prefix")
	private String networkPrefix;

	@JsonProperty
	private String route;

	@JsonProperty("maintained_by")
	private String maintainedBy;

	@JsonProperty("customer_origin")
	private String orgin;

	@JsonProperty("email_id")
	private String emailId;

	@JsonProperty
	private String submit;

	@JsonProperty("ip_version")
	private String ipVersion;

	@JsonProperty("input_file")
	private String fileContent;

	@JsonProperty()
	private String disclaimer;

	public static MultiplePrefixRequest createPrimaryDnsAddRecords(
			MultiplePrefixUploadRequest multiplePrefixUploadRequest) {
		MultiplePrefixRequest request = new MultiplePrefixRequest();

		LOGGER.debug("in goig to create request objects createPrimaryDnsAddRecords ");
		request.setLevel(multiplePrefixUploadRequest.getLevel());
		request.setService(multiplePrefixUploadRequest.getService());
		request.setEt(multiplePrefixUploadRequest.getProduct());
		request.setNetworkPrefix(multiplePrefixUploadRequest.getNetworkPrefix());
		request.setFileContent(multiplePrefixUploadRequest.getFileContent());
		request.setRoute(multiplePrefixUploadRequest.getRoute());
		request.setMaintainedBy(multiplePrefixUploadRequest.getMaintainedBy());
		request.setOrgin(multiplePrefixUploadRequest.getOrgin());
		request.setEmailId(multiplePrefixUploadRequest.getEmailId());
		request.setDisclaimer(multiplePrefixUploadRequest.getDisclaimer());
		request.setSubmit(SUBMIT);
		request.setIpVersion(multiplePrefixUploadRequest.getIpVersion());
		LOGGER.debug("in createPrimaryDnsAddRecords ");
		return request;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getProduct() {
		return product;
	}

	public void setEt(String product) {
		this.product = product;
	}

	public String getNetworkPrefix() {
		return networkPrefix;
	}

	public void setNetworkPrefix(String networkPrefix) {
		this.networkPrefix = networkPrefix;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getMaintainedBy() {
		return maintainedBy;
	}

	public void setMaintainedBy(String maintainedBy) {
		this.maintainedBy = maintainedBy;
	}

	public String getOrgin() {
		return orgin;
	}

	public void setOrgin(String orgin) {
		this.orgin = orgin;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(String ipVersion) {
		this.ipVersion = ipVersion;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public String getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

}
