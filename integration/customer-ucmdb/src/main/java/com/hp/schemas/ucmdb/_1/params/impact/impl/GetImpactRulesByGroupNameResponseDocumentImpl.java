/*
 * An XML document type.
 * Localname: getImpactRulesByGroupNameResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * A document containing one getImpactRulesByGroupNameResponse(@http://schemas.hp.com/ucmdb/1/params/impact) element.
 *
 * This is a complex type.
 */
public class GetImpactRulesByGroupNameResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponseDocument
{
    
    public GetImpactRulesByGroupNameResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETIMPACTRULESBYGROUPNAMERESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactRulesByGroupNameResponse");
    
    
    /**
     * Gets the "getImpactRulesByGroupNameResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponse getGetImpactRulesByGroupNameResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponse)get_store().find_element_user(GETIMPACTRULESBYGROUPNAMERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getImpactRulesByGroupNameResponse" element
     */
    public void setGetImpactRulesByGroupNameResponse(com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponse getImpactRulesByGroupNameResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponse)get_store().find_element_user(GETIMPACTRULESBYGROUPNAMERESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponse)get_store().add_element_user(GETIMPACTRULESBYGROUPNAMERESPONSE$0);
            }
            target.set(getImpactRulesByGroupNameResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getImpactRulesByGroupNameResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponse addNewGetImpactRulesByGroupNameResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponse)get_store().add_element_user(GETIMPACTRULESBYGROUPNAMERESPONSE$0);
            return target;
        }
    }
}
