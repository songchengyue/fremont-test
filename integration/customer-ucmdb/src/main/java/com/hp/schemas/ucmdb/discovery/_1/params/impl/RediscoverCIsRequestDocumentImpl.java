/*
 * An XML document type.
 * Localname: rediscoverCIsRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one rediscoverCIsRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class RediscoverCIsRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequestDocument
{
    
    public RediscoverCIsRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REDISCOVERCISREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "rediscoverCIsRequest");
    
    
    /**
     * Gets the "rediscoverCIsRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest getRediscoverCIsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest)get_store().find_element_user(REDISCOVERCISREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "rediscoverCIsRequest" element
     */
    public void setRediscoverCIsRequest(com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest rediscoverCIsRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest)get_store().find_element_user(REDISCOVERCISREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest)get_store().add_element_user(REDISCOVERCISREQUEST$0);
            }
            target.set(rediscoverCIsRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "rediscoverCIsRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest addNewRediscoverCIsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest)get_store().add_element_user(REDISCOVERCISREQUEST$0);
            return target;
        }
    }
}
