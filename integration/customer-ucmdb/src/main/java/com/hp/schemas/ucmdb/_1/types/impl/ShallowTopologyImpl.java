/*
 * XML Type:  ShallowTopology
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ShallowTopology
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ShallowTopology(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ShallowTopologyImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ShallowTopology
{
    
    public ShallowTopologyImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CIS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "CIs");
    private static final javax.xml.namespace.QName RELATIONS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "relations");
    private static final javax.xml.namespace.QName COMPOUNDRELATIONS$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "compoundRelations");
    
    
    /**
     * Gets the "CIs" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCIs getCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCIs)get_store().find_element_user(CIS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CIs" element
     */
    public void setCIs(com.hp.schemas.ucmdb._1.types.ShallowCIs cIs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCIs)get_store().find_element_user(CIS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ShallowCIs)get_store().add_element_user(CIS$0);
            }
            target.set(cIs);
        }
    }
    
    /**
     * Appends and returns a new empty "CIs" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCIs addNewCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCIs)get_store().add_element_user(CIS$0);
            return target;
        }
    }
    
    /**
     * Gets the "relations" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowRelations getRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowRelations target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowRelations)get_store().find_element_user(RELATIONS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "relations" element
     */
    public boolean isSetRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RELATIONS$2) != 0;
        }
    }
    
    /**
     * Sets the "relations" element
     */
    public void setRelations(com.hp.schemas.ucmdb._1.types.ShallowRelations relations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowRelations target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowRelations)get_store().find_element_user(RELATIONS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ShallowRelations)get_store().add_element_user(RELATIONS$2);
            }
            target.set(relations);
        }
    }
    
    /**
     * Appends and returns a new empty "relations" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowRelations addNewRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowRelations target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowRelations)get_store().add_element_user(RELATIONS$2);
            return target;
        }
    }
    
    /**
     * Unsets the "relations" element
     */
    public void unsetRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RELATIONS$2, 0);
        }
    }
    
    /**
     * Gets the "compoundRelations" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations getCompoundRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations)get_store().find_element_user(COMPOUNDRELATIONS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "compoundRelations" element
     */
    public boolean isSetCompoundRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(COMPOUNDRELATIONS$4) != 0;
        }
    }
    
    /**
     * Sets the "compoundRelations" element
     */
    public void setCompoundRelations(com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations compoundRelations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations)get_store().find_element_user(COMPOUNDRELATIONS$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations)get_store().add_element_user(COMPOUNDRELATIONS$4);
            }
            target.set(compoundRelations);
        }
    }
    
    /**
     * Appends and returns a new empty "compoundRelations" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations addNewCompoundRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations)get_store().add_element_user(COMPOUNDRELATIONS$4);
            return target;
        }
    }
    
    /**
     * Unsets the "compoundRelations" element
     */
    public void unsetCompoundRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(COMPOUNDRELATIONS$4, 0);
        }
    }
}
