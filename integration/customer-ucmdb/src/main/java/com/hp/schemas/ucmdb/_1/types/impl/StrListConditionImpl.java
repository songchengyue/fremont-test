/*
 * XML Type:  StrListCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.StrListCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML StrListCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class StrListConditionImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.StrListCondition
{
    
    public StrListConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "condition");
    private static final javax.xml.namespace.QName STRLISTOPERATOR$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strListOperator");
    
    
    /**
     * Gets the "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.StrProp getCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "condition" element
     */
    public void setCondition(com.hp.schemas.ucmdb._1.types.StrProp condition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().add_element_user(CONDITION$0);
            }
            target.set(condition);
        }
    }
    
    /**
     * Appends and returns a new empty "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.StrProp addNewCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrProp)get_store().add_element_user(CONDITION$0);
            return target;
        }
    }
    
    /**
     * Gets the "strListOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator.Enum getStrListOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STRLISTOPERATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "strListOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator xgetStrListOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator)get_store().find_element_user(STRLISTOPERATOR$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "strListOperator" element
     */
    public void setStrListOperator(com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator.Enum strListOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STRLISTOPERATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STRLISTOPERATOR$2);
            }
            target.setEnumValue(strListOperator);
        }
    }
    
    /**
     * Sets (as xml) the "strListOperator" element
     */
    public void xsetStrListOperator(com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator strListOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator)get_store().find_element_user(STRLISTOPERATOR$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator)get_store().add_element_user(STRLISTOPERATOR$2);
            }
            target.set(strListOperator);
        }
    }
    /**
     * An XML strListOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.StrListCondition$StrListOperator.
     */
    public static class StrListOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.StrListCondition.StrListOperator
    {
        
        public StrListOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected StrListOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
