/*
 * An XML document type.
 * Localname: isProbeConnectedResponce
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponceDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one isProbeConnectedResponce(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class IsProbeConnectedResponceDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponceDocument
{
    
    public IsProbeConnectedResponceDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ISPROBECONNECTEDRESPONCE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "isProbeConnectedResponce");
    
    
    /**
     * Gets the "isProbeConnectedResponce" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponce getIsProbeConnectedResponce()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponce target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponce)get_store().find_element_user(ISPROBECONNECTEDRESPONCE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "isProbeConnectedResponce" element
     */
    public void setIsProbeConnectedResponce(com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponce isProbeConnectedResponce)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponce target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponce)get_store().find_element_user(ISPROBECONNECTEDRESPONCE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponce)get_store().add_element_user(ISPROBECONNECTEDRESPONCE$0);
            }
            target.set(isProbeConnectedResponce);
        }
    }
    
    /**
     * Appends and returns a new empty "isProbeConnectedResponce" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponce addNewIsProbeConnectedResponce()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponce target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsProbeConnectedResponce)get_store().add_element_user(ISPROBECONNECTEDRESPONCE$0);
            return target;
        }
    }
}
