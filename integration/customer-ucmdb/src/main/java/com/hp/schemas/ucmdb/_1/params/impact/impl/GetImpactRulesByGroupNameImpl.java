/*
 * XML Type:  getImpactRulesByGroupName
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * An XML getImpactRulesByGroupName(@http://schemas.hp.com/ucmdb/1/params/impact).
 *
 * This is a complex type.
 */
public class GetImpactRulesByGroupNameImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupName
{
    
    public GetImpactRulesByGroupNameImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "cmdbContext");
    private static final javax.xml.namespace.QName RULEGROUPNAMEFILTER$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "ruleGroupNameFilter");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets array of all "ruleGroupNameFilter" elements
     */
    public java.lang.String[] getRuleGroupNameFilterArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(RULEGROUPNAMEFILTER$2, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "ruleGroupNameFilter" element
     */
    public java.lang.String getRuleGroupNameFilterArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RULEGROUPNAMEFILTER$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "ruleGroupNameFilter" elements
     */
    public org.apache.xmlbeans.XmlString[] xgetRuleGroupNameFilterArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(RULEGROUPNAMEFILTER$2, targetList);
            org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "ruleGroupNameFilter" element
     */
    public org.apache.xmlbeans.XmlString xgetRuleGroupNameFilterArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RULEGROUPNAMEFILTER$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return (org.apache.xmlbeans.XmlString)target;
        }
    }
    
    /**
     * Returns number of "ruleGroupNameFilter" element
     */
    public int sizeOfRuleGroupNameFilterArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RULEGROUPNAMEFILTER$2);
        }
    }
    
    /**
     * Sets array of all "ruleGroupNameFilter" element
     */
    public void setRuleGroupNameFilterArray(java.lang.String[] ruleGroupNameFilterArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(ruleGroupNameFilterArray, RULEGROUPNAMEFILTER$2);
        }
    }
    
    /**
     * Sets ith "ruleGroupNameFilter" element
     */
    public void setRuleGroupNameFilterArray(int i, java.lang.String ruleGroupNameFilter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RULEGROUPNAMEFILTER$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(ruleGroupNameFilter);
        }
    }
    
    /**
     * Sets (as xml) array of all "ruleGroupNameFilter" element
     */
    public void xsetRuleGroupNameFilterArray(org.apache.xmlbeans.XmlString[]ruleGroupNameFilterArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(ruleGroupNameFilterArray, RULEGROUPNAMEFILTER$2);
        }
    }
    
    /**
     * Sets (as xml) ith "ruleGroupNameFilter" element
     */
    public void xsetRuleGroupNameFilterArray(int i, org.apache.xmlbeans.XmlString ruleGroupNameFilter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RULEGROUPNAMEFILTER$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(ruleGroupNameFilter);
        }
    }
    
    /**
     * Inserts the value as the ith "ruleGroupNameFilter" element
     */
    public void insertRuleGroupNameFilter(int i, java.lang.String ruleGroupNameFilter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(RULEGROUPNAMEFILTER$2, i);
            target.setStringValue(ruleGroupNameFilter);
        }
    }
    
    /**
     * Appends the value as the last "ruleGroupNameFilter" element
     */
    public void addRuleGroupNameFilter(java.lang.String ruleGroupNameFilter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RULEGROUPNAMEFILTER$2);
            target.setStringValue(ruleGroupNameFilter);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ruleGroupNameFilter" element
     */
    public org.apache.xmlbeans.XmlString insertNewRuleGroupNameFilter(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(RULEGROUPNAMEFILTER$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ruleGroupNameFilter" element
     */
    public org.apache.xmlbeans.XmlString addNewRuleGroupNameFilter()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(RULEGROUPNAMEFILTER$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "ruleGroupNameFilter" element
     */
    public void removeRuleGroupNameFilter(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RULEGROUPNAMEFILTER$2, i);
        }
    }
}
