package com.telstra.netops.dns.server.service;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.dns.server.exception.DnsApplicationGenericBusinessRuleException;
import com.telstra.netops.dns.server.model.ClientDnsRemoveRequest;
import com.telstra.netops.dns.server.model.ClientPrimaryDnsRequest;
import com.telstra.netops.dns.server.model.DnsPrimaryRecord;
import com.telstra.netops.dns.server.model.Success;
import com.telstra.netops.dns.server.model.cgi.CgiDnsRequest;
import com.telstra.netops.dns.server.model.cgi.DnsPrimaryRecordsResultWrapper;
import com.telstra.netops.dns.server.model.cgi.DnsResultWrapper;
import com.telstra.netops.dns.server.util.JsonSerializerDeserializer;
import com.telstra.oauth2.client.OAuth2TokenResolver;

@Service
@Profile("!local")
public class CgiPrimaryDnsService implements PrimaryDnsService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CgiPrimaryDnsService.class);

	@Value("${dns.cgi.url}")
	private String cgiURL;
	
	@Autowired
	private JsonSerializerDeserializer jsonSerializerDeserializer;
	
	@Autowired
    private OAuth2TokenResolver tokenResolver;
	
	@Override
	public Success add(ClientPrimaryDnsRequest request) {
		
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createPrimaryDnsAddRecords(request);
		
		CgiDnsRequester<DnsResultWrapper> requester = new CgiDnsRequester<DnsResultWrapper>(DnsResultWrapper.class, jsonSerializerDeserializer, cgiURL);
		
		DnsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isError()) {
			throw new DnsApplicationGenericBusinessRuleException(wrapper.getErrorMessage());
		}
		
		LOGGER.debug(wrapper.toString());
		return new Success(wrapper.getSuccessMessage());
	}
	

	@Override
	public List<DnsPrimaryRecord> list(String customerId) {
		
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createPrimaryDnsListRecords(customerId);
		
		CgiDnsRequester<DnsPrimaryRecordsResultWrapper> requester = new CgiDnsRequester<DnsPrimaryRecordsResultWrapper>(DnsPrimaryRecordsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsPrimaryRecordsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isEmpty()) {
			return Collections.<DnsPrimaryRecord>emptyList();
		}

		LOGGER.debug(wrapper.toString());
		return wrapper.getResult();
	}

	@Override
	public Success remove(ClientDnsRemoveRequest request) {

		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.primaryDnsRemoveRecords(request);
		LOGGER.debug("remove--------------->>>>"+cgiDnsRequest);
		
		CgiDnsRequester<DnsResultWrapper> requester = new CgiDnsRequester<DnsResultWrapper>(DnsResultWrapper.class, jsonSerializerDeserializer, cgiURL);
		LOGGER.debug("remove requester--------------->>>>"+requester);
		DnsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isError()) {
			throw new DnsApplicationGenericBusinessRuleException(wrapper.getErrorMessage());
		}
		
		LOGGER.debug(wrapper.toString());
		return new Success(wrapper.getSuccessMessage());
	}


}
