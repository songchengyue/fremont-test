/*
 * An XML document type.
 * Localname: UcmdbClass
 * Namespace: http://schemas.hp.com/ucmdb/1/types/classmodel
 * Java type: com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.classmodel.impl;
/**
 * A document containing one UcmdbClass(@http://schemas.hp.com/ucmdb/1/types/classmodel) element.
 *
 * This is a complex type.
 */
public class UcmdbClassDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClassDocument
{
    
    public UcmdbClassDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName UCMDBCLASS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "UcmdbClass");
    
    
    /**
     * Gets the "UcmdbClass" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass getUcmdbClass()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass)get_store().find_element_user(UCMDBCLASS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "UcmdbClass" element
     */
    public void setUcmdbClass(com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass ucmdbClass)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass)get_store().find_element_user(UCMDBCLASS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass)get_store().add_element_user(UCMDBCLASS$0);
            }
            target.set(ucmdbClass);
        }
    }
    
    /**
     * Appends and returns a new empty "UcmdbClass" element
     */
    public com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass addNewUcmdbClass()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass target = null;
            target = (com.hp.schemas.ucmdb._1.types.classmodel.UcmdbClass)get_store().add_element_user(UCMDBCLASS$0);
            return target;
        }
    }
}
