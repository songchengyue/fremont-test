/*
 * An XML document type.
 * Localname: removeCustomerResponse
 * Namespace: http://schemas.hp.com/ucmdb/management/1/params
 * Java type: com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.management._1.params.impl;
/**
 * A document containing one removeCustomerResponse(@http://schemas.hp.com/ucmdb/management/1/params) element.
 *
 * This is a complex type.
 */
public class RemoveCustomerResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponseDocument
{
    
    public RemoveCustomerResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REMOVECUSTOMERRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/management/1/params", "removeCustomerResponse");
    
    
    /**
     * Gets the "removeCustomerResponse" element
     */
    public com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse getRemoveCustomerResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse)get_store().find_element_user(REMOVECUSTOMERRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "removeCustomerResponse" element
     */
    public void setRemoveCustomerResponse(com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse removeCustomerResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse)get_store().find_element_user(REMOVECUSTOMERRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse)get_store().add_element_user(REMOVECUSTOMERRESPONSE$0);
            }
            target.set(removeCustomerResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "removeCustomerResponse" element
     */
    public com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse addNewRemoveCustomerResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.RemoveCustomerResponse)get_store().add_element_user(REMOVECUSTOMERRESPONSE$0);
            return target;
        }
    }
}
