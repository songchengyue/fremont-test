/*
 * XML Type:  FloatProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.FloatProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML FloatProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class FloatPropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.FloatProps
{
    
    public FloatPropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FLOATPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "floatProp");
    
    
    /**
     * Gets array of all "floatProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.FloatProp[] getFloatPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(FLOATPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.FloatProp[] result = new com.hp.schemas.ucmdb._1.types.FloatProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "floatProp" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatProp getFloatPropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().find_element_user(FLOATPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "floatProp" element
     */
    public int sizeOfFloatPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FLOATPROP$0);
        }
    }
    
    /**
     * Sets array of all "floatProp" element
     */
    public void setFloatPropArray(com.hp.schemas.ucmdb._1.types.FloatProp[] floatPropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(floatPropArray, FLOATPROP$0);
        }
    }
    
    /**
     * Sets ith "floatProp" element
     */
    public void setFloatPropArray(int i, com.hp.schemas.ucmdb._1.types.FloatProp floatProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().find_element_user(FLOATPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(floatProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "floatProp" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatProp insertNewFloatProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().insert_element_user(FLOATPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "floatProp" element
     */
    public com.hp.schemas.ucmdb._1.types.FloatProp addNewFloatProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.FloatProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.FloatProp)get_store().add_element_user(FLOATPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "floatProp" element
     */
    public void removeFloatProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FLOATPROP$0, i);
        }
    }
}
