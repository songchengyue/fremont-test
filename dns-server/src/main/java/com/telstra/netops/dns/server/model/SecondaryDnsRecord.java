package com.telstra.netops.dns.server.model;




import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.telstra.netops.dns.server.model.JsonDateSerializer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.telstra.netops.dns.server.service.CgiResolverService;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SecondaryDnsRecord {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SecondaryDnsRecord.class);
	
	@JsonProperty("Domain")
	private String domain;
	
	@JsonProperty("DNS_IP")
	private String primaryDnsServerIpAddress;
	
	@JsonProperty("Date")
	@JsonFormat(shape = JsonFormat.Shape.STRING , pattern = "yyyy-dd-MM HH:mm:ss" ,timezone = "UTC")
	private Date requestDate;
	
	@JsonProperty("ROWID")
	private String DBrowId;
	
	@JsonProperty("Error")
	private String error;
	
	

	public SecondaryDnsRecord() {
	}
	
	public SecondaryDnsRecord(String domain, String primaryDnsServerIpAddress,Date requestDate ,String  DBrowId ) {
		super();
		this.domain = domain;
		this.primaryDnsServerIpAddress = primaryDnsServerIpAddress;
		this.requestDate = requestDate;
		//this.error = error;
		this.DBrowId = DBrowId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getPrimaryDnsServerIpAddress() {
		return primaryDnsServerIpAddress;
	}

	public void setPrimaryDnsServerIpAddress(String primaryDnsServerIpAddress) {
		this.primaryDnsServerIpAddress = primaryDnsServerIpAddress;
	}
	
	//@JsonSerialize(using=JsonDateSerializer.class)
	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		LOGGER.debug("SecondaryDnsRecord ----->>>>setRequestDate "+requestDate);

		this.requestDate = requestDate;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
   
	public boolean notARecord() {
		return error != null;
	}

	public String getDBrowId() {
		return DBrowId;
	}

	public void setDBrowId(String dBrowId) {
		DBrowId = dBrowId;
	}	
}
