# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Mods: Ash Garg (Ash.Garg@telstra.net)
# Date: 2 August 2001
# File: NOTIFICATION.pm
#
# This module contains subroutines used by CMS to notify customers by mail
#
# $Id: NOTIFICATION.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $

package NOTIFICATION;
use 5.6.1;
use strict;
use warnings;
use Mail::Sendmail;

use Exporter;

use constant REACH_CONTACT
  => "Reach Customer Management System <cms\@net.reach.com>";

our @ISA = qw (Exporter);

#the module sends no errors to STDERR, but puts anything it has to complain about in here.
our $error;

#A summary that you could write to a log file after each send
our $log;


# This subroutine is used to send a message to a recipient.

# Inputs: - an array. The format for the array is
#			Type, Name, Contact Details
#         - subject
#         - message
#         - from name
#         - from email
#         - from phone no
#
# It returns 1 on success or 0 on error
# and rewrites $NOTIFICATION::error and $NOTIFICATION::log.
sub send {
	my ($recipients, $subject, $message, $fromname, $from) = @_;
	my $compounderror;
	my $compoundlog;
	my $return=0;

	my $i=0;  
	foreach my $recipient (@$recipients) 
	{
		$i++;
		my $subreturn=NOTIFICATION::send_individual(@$recipient, $subject, $message, $fromname, $from);
		$compounderror=$compounderror . $error;
		$compoundlog = $compoundlog . $log;

		if($subreturn==0)
		{
			$compounderror= $compounderror . "---- NOTIFICATION was unable to process item [$i]=@$recipient\n";
			$compoundlog= $compoundlog . "---- NOTIFICATION was unable to process item [$i]=@$recipient\n";
		}
		$return = $return + $subreturn;
	}
	$error=$compounderror;
	$log=$compoundlog;
	return $return;
}

sub send_individual
{
  # This subroutine is used to send a message to a recipient.

  # Inputs: - an array. The format for the array is
  #			Type, Name, Contact Details
  #         - subject
  #         - message
  #         - from name
  #         - from email
  #         - from phone no
  #
  # It returns 1 on success or 0 on error
  # and rewrites $NOTIFICATION::error and $NOTIFICATION::log.

	my ($type, $toname, $to, $subject, $message, $fromname, $from) = @_;
  	my %mail;

	if($type=~/email/i)
	{
		$mail{to}=$to;
		$mail{subject}=$subject;
		$mail{message}=$message;
		$mail{from}=$from;
  		$mail{date} = Mail::Sendmail::time_to_date(time());
	

  		my $return=sendmail(%mail);
		$error=$Mail::Sendmail::error;
		$log=$Mail::Sendmail::log;
		return $return;
	}
}

1;
