package com.telstra.netops.client.exception;

import org.springframework.web.bind.ServletRequestBindingException;

public class MissingModelAttributeException extends ServletRequestBindingException {
	
	public MissingModelAttributeException(String attribute) {
		super("Missing model attribute '" + attribute + "'");
	}
	
}
