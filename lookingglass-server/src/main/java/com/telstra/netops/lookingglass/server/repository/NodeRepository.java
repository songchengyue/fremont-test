package com.telstra.netops.lookingglass.server.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telstra.netops.lookingglass.server.model.IPVersion;
import com.telstra.netops.lookingglass.server.model.Node;

public interface NodeRepository extends PagingAndSortingRepository<Node, Long> {
	
	List<Node> findByIpVersionOrderByCityNameAscCountryNameAsc(IPVersion ipVersion);

	
}
