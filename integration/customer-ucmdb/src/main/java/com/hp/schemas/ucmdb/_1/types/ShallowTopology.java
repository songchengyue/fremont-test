/*
 * XML Type:  ShallowTopology
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ShallowTopology
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types;


/**
 * An XML ShallowTopology(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public interface ShallowTopology extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ShallowTopology.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("shallowtopologye03dtype");
    
    /**
     * Gets the "CIs" element
     */
    com.hp.schemas.ucmdb._1.types.ShallowCIs getCIs();
    
    /**
     * Sets the "CIs" element
     */
    void setCIs(com.hp.schemas.ucmdb._1.types.ShallowCIs cIs);
    
    /**
     * Appends and returns a new empty "CIs" element
     */
    com.hp.schemas.ucmdb._1.types.ShallowCIs addNewCIs();
    
    /**
     * Gets the "relations" element
     */
    com.hp.schemas.ucmdb._1.types.ShallowRelations getRelations();
    
    /**
     * True if has "relations" element
     */
    boolean isSetRelations();
    
    /**
     * Sets the "relations" element
     */
    void setRelations(com.hp.schemas.ucmdb._1.types.ShallowRelations relations);
    
    /**
     * Appends and returns a new empty "relations" element
     */
    com.hp.schemas.ucmdb._1.types.ShallowRelations addNewRelations();
    
    /**
     * Unsets the "relations" element
     */
    void unsetRelations();
    
    /**
     * Gets the "compoundRelations" element
     */
    com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations getCompoundRelations();
    
    /**
     * True if has "compoundRelations" element
     */
    boolean isSetCompoundRelations();
    
    /**
     * Sets the "compoundRelations" element
     */
    void setCompoundRelations(com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations compoundRelations);
    
    /**
     * Appends and returns a new empty "compoundRelations" element
     */
    com.hp.schemas.ucmdb._1.types.ShallowCompoundRelations addNewCompoundRelations();
    
    /**
     * Unsets the "compoundRelations" element
     */
    void unsetCompoundRelations();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology newInstance() {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.ShallowTopology parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.ShallowTopology) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
