/*
 * An XML document type.
 * Localname: getClassAncestorsResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/classmodel
 * Java type: com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.classmodel.impl;
/**
 * A document containing one getClassAncestorsResponse(@http://schemas.hp.com/ucmdb/1/params/classmodel) element.
 *
 * This is a complex type.
 */
public class GetClassAncestorsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponseDocument
{
    
    public GetClassAncestorsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCLASSANCESTORSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getClassAncestorsResponse");
    
    
    /**
     * Gets the "getClassAncestorsResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse getGetClassAncestorsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse)get_store().find_element_user(GETCLASSANCESTORSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getClassAncestorsResponse" element
     */
    public void setGetClassAncestorsResponse(com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse getClassAncestorsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse)get_store().find_element_user(GETCLASSANCESTORSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse)get_store().add_element_user(GETCLASSANCESTORSRESPONSE$0);
            }
            target.set(getClassAncestorsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getClassAncestorsResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse addNewGetClassAncestorsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponse)get_store().add_element_user(GETCLASSANCESTORSRESPONSE$0);
            return target;
        }
    }
}
