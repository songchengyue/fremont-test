/*
 * An XML document type.
 * Localname: isJobActiveRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one isJobActiveRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class IsJobActiveRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequestDocument
{
    
    public IsJobActiveRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ISJOBACTIVEREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "isJobActiveRequest");
    
    
    /**
     * Gets the "isJobActiveRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequest getIsJobActiveRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequest)get_store().find_element_user(ISJOBACTIVEREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "isJobActiveRequest" element
     */
    public void setIsJobActiveRequest(com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequest isJobActiveRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequest)get_store().find_element_user(ISJOBACTIVEREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequest)get_store().add_element_user(ISJOBACTIVEREQUEST$0);
            }
            target.set(isJobActiveRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "isJobActiveRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequest addNewIsJobActiveRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.IsJobActiveRequest)get_store().add_element_user(ISJOBACTIVEREQUEST$0);
            return target;
        }
    }
}
