/*
 * XML Type:  IntProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML IntProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IntPropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.IntProps
{
    
    public IntPropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INTPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intProp");
    
    
    /**
     * Gets array of all "intProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.IntProp[] getIntPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INTPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.IntProp[] result = new com.hp.schemas.ucmdb._1.types.IntProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "intProp" element
     */
    public com.hp.schemas.ucmdb._1.types.IntProp getIntPropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().find_element_user(INTPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "intProp" element
     */
    public int sizeOfIntPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTPROP$0);
        }
    }
    
    /**
     * Sets array of all "intProp" element
     */
    public void setIntPropArray(com.hp.schemas.ucmdb._1.types.IntProp[] intPropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(intPropArray, INTPROP$0);
        }
    }
    
    /**
     * Sets ith "intProp" element
     */
    public void setIntPropArray(int i, com.hp.schemas.ucmdb._1.types.IntProp intProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().find_element_user(INTPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(intProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "intProp" element
     */
    public com.hp.schemas.ucmdb._1.types.IntProp insertNewIntProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().insert_element_user(INTPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "intProp" element
     */
    public com.hp.schemas.ucmdb._1.types.IntProp addNewIntProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().add_element_user(INTPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "intProp" element
     */
    public void removeIntProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTPROP$0, i);
        }
    }
}
