
/**
 * UcmdbServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package com.hp.ucmdb.generated;

    /**
     *  UcmdbServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class UcmdbServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public UcmdbServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public UcmdbServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getAllClassesHierarchy method
            * override this method for handling normal response from getAllClassesHierarchy operation
            */
           public void receiveResultgetAllClassesHierarchy(
                    com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchyResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAllClassesHierarchy operation
           */
            public void receiveErrorgetAllClassesHierarchy(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for executeTopologyQueryByName method
            * override this method for handling normal response from executeTopologyQueryByName operation
            */
           public void receiveResultexecuteTopologyQueryByName(
                    com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from executeTopologyQueryByName operation
           */
            public void receiveErrorexecuteTopologyQueryByName(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for releaseChunks method
            * override this method for handling normal response from releaseChunks operation
            */
           public void receiveResultreleaseChunks(
                    com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from releaseChunks operation
           */
            public void receiveErrorreleaseChunks(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for executeTopologyQueryByNameWithParameters method
            * override this method for handling normal response from executeTopologyQueryByNameWithParameters operation
            */
           public void receiveResultexecuteTopologyQueryByNameWithParameters(
                    com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from executeTopologyQueryByNameWithParameters operation
           */
            public void receiveErrorexecuteTopologyQueryByNameWithParameters(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getImpactRulesByGroupName method
            * override this method for handling normal response from getImpactRulesByGroupName operation
            */
           public void receiveResultgetImpactRulesByGroupName(
                    com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByGroupNameResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getImpactRulesByGroupName operation
           */
            public void receiveErrorgetImpactRulesByGroupName(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteCIsAndRelations method
            * override this method for handling normal response from deleteCIsAndRelations operation
            */
           public void receiveResultdeleteCIsAndRelations(
                    com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteCIsAndRelations operation
           */
            public void receiveErrordeleteCIsAndRelations(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getImpactPath method
            * override this method for handling normal response from getImpactPath operation
            */
           public void receiveResultgetImpactPath(
                    com.hp.schemas.ucmdb._1.params.impact.GetImpactPathResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getImpactPath operation
           */
            public void receiveErrorgetImpactPath(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateCIsAndRelations method
            * override this method for handling normal response from updateCIsAndRelations operation
            */
           public void receiveResultupdateCIsAndRelations(
                    com.hp.schemas.ucmdb._1.params.update.UpdateCIsAndRelationsResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateCIsAndRelations operation
           */
            public void receiveErrorupdateCIsAndRelations(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTopologyQueryResultCountByName method
            * override this method for handling normal response from getTopologyQueryResultCountByName operation
            */
           public void receiveResultgetTopologyQueryResultCountByName(
                    com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTopologyQueryResultCountByName operation
           */
            public void receiveErrorgetTopologyQueryResultCountByName(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for calculateImpact method
            * override this method for handling normal response from calculateImpact operation
            */
           public void receiveResultcalculateImpact(
                    com.hp.schemas.ucmdb._1.params.impact.CalculateImpactResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from calculateImpact operation
           */
            public void receiveErrorcalculateImpact(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFilteredCIsByType method
            * override this method for handling normal response from getFilteredCIsByType operation
            */
           public void receiveResultgetFilteredCIsByType(
                    com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFilteredCIsByType operation
           */
            public void receiveErrorgetFilteredCIsByType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTopologyQueryExistingResultByName method
            * override this method for handling normal response from getTopologyQueryExistingResultByName operation
            */
           public void receiveResultgetTopologyQueryExistingResultByName(
                    com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryExistingResultByNameResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTopologyQueryExistingResultByName operation
           */
            public void receiveErrorgetTopologyQueryExistingResultByName(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getClassAncestors method
            * override this method for handling normal response from getClassAncestors operation
            */
           public void receiveResultgetClassAncestors(
                    com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getClassAncestors operation
           */
            public void receiveErrorgetClassAncestors(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCIsByType method
            * override this method for handling normal response from getCIsByType operation
            */
           public void receiveResultgetCIsByType(
                    com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCIsByType operation
           */
            public void receiveErrorgetCIsByType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getChangedCIs method
            * override this method for handling normal response from getChangedCIs operation
            */
           public void receiveResultgetChangedCIs(
                    com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getChangedCIs operation
           */
            public void receiveErrorgetChangedCIs(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCmdbClassDefinition method
            * override this method for handling normal response from getCmdbClassDefinition operation
            */
           public void receiveResultgetCmdbClassDefinition(
                    com.hp.schemas.ucmdb._1.params.classmodel.GetCmdbClassDefinitionResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCmdbClassDefinition operation
           */
            public void receiveErrorgetCmdbClassDefinition(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRelationsById method
            * override this method for handling normal response from getRelationsById operation
            */
           public void receiveResultgetRelationsById(
                    com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRelationsById operation
           */
            public void receiveErrorgetRelationsById(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for pullTopologyMapChunks method
            * override this method for handling normal response from pullTopologyMapChunks operation
            */
           public void receiveResultpullTopologyMapChunks(
                    com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from pullTopologyMapChunks operation
           */
            public void receiveErrorpullTopologyMapChunks(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for executeTopologyQueryWithParameters method
            * override this method for handling normal response from executeTopologyQueryWithParameters operation
            */
           public void receiveResultexecuteTopologyQueryWithParameters(
                    com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParametersResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from executeTopologyQueryWithParameters operation
           */
            public void receiveErrorexecuteTopologyQueryWithParameters(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCINeighbours method
            * override this method for handling normal response from getCINeighbours operation
            */
           public void receiveResultgetCINeighbours(
                    com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCINeighbours operation
           */
            public void receiveErrorgetCINeighbours(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getImpactRulesByNamePrefix method
            * override this method for handling normal response from getImpactRulesByNamePrefix operation
            */
           public void receiveResultgetImpactRulesByNamePrefix(
                    com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getImpactRulesByNamePrefix operation
           */
            public void receiveErrorgetImpactRulesByNamePrefix(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCIsById method
            * override this method for handling normal response from getCIsById operation
            */
           public void receiveResultgetCIsById(
                    com.hp.schemas.ucmdb._1.params.query.GetCIsByIdResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCIsById operation
           */
            public void receiveErrorgetCIsById(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getQueryNameOfView method
            * override this method for handling normal response from getQueryNameOfView operation
            */
           public void receiveResultgetQueryNameOfView(
                    com.hp.schemas.ucmdb._1.params.query.GetQueryNameOfViewResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getQueryNameOfView operation
           */
            public void receiveErrorgetQueryNameOfView(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addCIsAndRelations method
            * override this method for handling normal response from addCIsAndRelations operation
            */
           public void receiveResultaddCIsAndRelations(
                    com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelationsResponseDocument result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addCIsAndRelations operation
           */
            public void receiveErroraddCIsAndRelations(java.lang.Exception e) {
            }
                


    }
    