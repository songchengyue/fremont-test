package com.telstra.oauth2.server.saml1;

import static org.junit.Assert.*;

import java.io.InputStream;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Before;
import org.junit.Test;
import org.opensaml.saml1.core.Assertion;
import org.springframework.util.Assert;

import com.telstra.oauth2.server.saml1.XMLAssertionUnmarshaller;

public class XMLAssertionUnmarshallerTest {
	
	private static final String ASSERTION_FILE_NAME = "assertion.xml";

	private XMLAssertionUnmarshaller unmarshaller;
	
	@Before
	public void before() {
		unmarshaller = new XMLAssertionUnmarshaller();
	}
	
	@Test
	public void unmarshall() {
		InputStream in = this.getClass().getResourceAsStream(ASSERTION_FILE_NAME);
		Assertion assertion = unmarshaller.unmarshallAssertion(in);
		Assert.notNull(assertion);
		assertEquals("cn=SCC3ax9rl,cn=cluster,cn=nids,ou=accessManagerContainer,o=novell", assertion.getIssuer());
		assertEquals("cn=toshit123@abc.com,ou=INTERNAL,ou=USERS,o=ti", assertion.getAuthenticationStatements().get(0).getSubject().getNameIdentifier().getNameIdentifier());
		assertEquals(new DateTime("2016-04-26T05:53:50Z", DateTimeZone.UTC),assertion.getConditions().getNotOnOrAfter());
		
	}

}
