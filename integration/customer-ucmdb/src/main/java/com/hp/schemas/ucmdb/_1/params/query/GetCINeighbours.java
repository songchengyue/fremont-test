/*
 * XML Type:  getCINeighbours
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetCINeighbours
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query;


/**
 * An XML getCINeighbours(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public interface GetCINeighbours extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetCINeighbours.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("getcineighbours148ctype");
    
    /**
     * Gets the "cmdbContext" element
     */
    com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext();
    
    /**
     * Sets the "cmdbContext" element
     */
    void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext);
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext();
    
    /**
     * Gets the "ID" element
     */
    com.hp.schemas.ucmdb._1.types.ID getID();
    
    /**
     * Sets the "ID" element
     */
    void setID(com.hp.schemas.ucmdb._1.types.ID id);
    
    /**
     * Appends and returns a new empty "ID" element
     */
    com.hp.schemas.ucmdb._1.types.ID addNewID();
    
    /**
     * Gets the "neighbourType" element
     */
    java.lang.String getNeighbourType();
    
    /**
     * Gets (as xml) the "neighbourType" element
     */
    org.apache.xmlbeans.XmlString xgetNeighbourType();
    
    /**
     * Sets the "neighbourType" element
     */
    void setNeighbourType(java.lang.String neighbourType);
    
    /**
     * Sets (as xml) the "neighbourType" element
     */
    void xsetNeighbourType(org.apache.xmlbeans.XmlString neighbourType);
    
    /**
     * Gets the "CIProperties" element
     */
    com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection getCIProperties();
    
    /**
     * True if has "CIProperties" element
     */
    boolean isSetCIProperties();
    
    /**
     * Sets the "CIProperties" element
     */
    void setCIProperties(com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection ciProperties);
    
    /**
     * Appends and returns a new empty "CIProperties" element
     */
    com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection addNewCIProperties();
    
    /**
     * Unsets the "CIProperties" element
     */
    void unsetCIProperties();
    
    /**
     * Gets the "relationProperties" element
     */
    com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection getRelationProperties();
    
    /**
     * True if has "relationProperties" element
     */
    boolean isSetRelationProperties();
    
    /**
     * Sets the "relationProperties" element
     */
    void setRelationProperties(com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection relationProperties);
    
    /**
     * Appends and returns a new empty "relationProperties" element
     */
    com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection addNewRelationProperties();
    
    /**
     * Unsets the "relationProperties" element
     */
    void unsetRelationProperties();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours newInstance() {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.params.query.GetCINeighbours parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.params.query.GetCINeighbours) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
