package com.hp.ucmdb.generated;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UcmdbNetopsSynch{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UcmdbNetopsSynch.class);
	static  UcmdbServiceStub ucmdbStub =null;
	static Properties props = new Properties();
	static String sync_start_date = "";
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
    public static void main(java.lang.String args[]){
        try{
        	String mainConfigPath = args[0];
			loadParams(mainConfigPath);
			sync_start_date = getCurrentTimeStamp();
        	String wsUrl = props.getProperty("WS_URL");
        	ucmdbStub =new UcmdbServiceStub(wsUrl);

        	//Looking Glass Related Fetch
        	UcmdbLg ucmdbLg = new UcmdbLg();
        	ucmdbLg.getCfsDataLg(ucmdbStub,mainConfigPath);
        	
        	//DNS and RR Related Fetch
        	UcmdbCfs ucmdbCfs = new UcmdbCfs();
        	ucmdbCfs.getCfsIds(ucmdbStub,mainConfigPath);
        	UcmdbRfs ucmdbRfs = new UcmdbRfs();
        	ucmdbRfs.getServicesFromRfs(ucmdbStub,mainConfigPath);
			UcmdbRfsResource ucmdbRfsResource = new UcmdbRfsResource();
			ucmdbRfsResource.getResourceIds(ucmdbStub,mainConfigPath);
			UcmdbWolResource ucmdbWolResource = new UcmdbWolResource();
			ucmdbWolResource.getSiteIds(ucmdbStub,mainConfigPath);
			UcmdbTiSite ucmdbTiSite = new UcmdbTiSite();
			ucmdbTiSite.getParentSiteIds(ucmdbStub,mainConfigPath,sync_start_date);

        } catch(Exception e){
        	LOGGER.error("exception inside main", e);
        }
    }


	public static void loadParams(String configPath) {
	    InputStream is = null;
	 
	    try {
	    	LOGGER.info("configPath : " +configPath);
	        File f = new File (configPath);
	    	//File f = new File("D:\\WorkSpace5\\testUcmdb5\\src\\config.txt");
	    	//File f = new File("E:\\Ahana\\Workspace5\\Ucmdb5\\src\\config.txt");
	    //File f = new File("E:\\Ashwini\\workspaceAhana\\ucmdbSrv\\src\\config.txt");
	        is = new FileInputStream( f );
	    }
	    catch ( Exception e ) { is = null; }
	 
	    try {
	        props.load( is );
	    }
	    catch ( Exception e ) {
	    	LOGGER.error("exception inside loadParams", e);
	    }

	   
	}
	
	private static String getCurrentTimeStamp() {
		java.util.Date today = new java.util.Date();
		return dateFormat.format(today.getTime());
	}
	
}