/*
 * XML Type:  ImpactRelations
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactRelations
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML ImpactRelations(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ImpactRelationsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactRelations
{
    
    public ImpactRelationsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTRELATION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "impactRelation");
    
    
    /**
     * Gets array of all "impactRelation" elements
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRelation[] getImpactRelationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IMPACTRELATION$0, targetList);
            com.hp.schemas.ucmdb._1.types.impact.ImpactRelation[] result = new com.hp.schemas.ucmdb._1.types.impact.ImpactRelation[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "impactRelation" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRelation getImpactRelationArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRelation)get_store().find_element_user(IMPACTRELATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "impactRelation" element
     */
    public int sizeOfImpactRelationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IMPACTRELATION$0);
        }
    }
    
    /**
     * Sets array of all "impactRelation" element
     */
    public void setImpactRelationArray(com.hp.schemas.ucmdb._1.types.impact.ImpactRelation[] impactRelationArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(impactRelationArray, IMPACTRELATION$0);
        }
    }
    
    /**
     * Sets ith "impactRelation" element
     */
    public void setImpactRelationArray(int i, com.hp.schemas.ucmdb._1.types.impact.ImpactRelation impactRelation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRelation)get_store().find_element_user(IMPACTRELATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(impactRelation);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "impactRelation" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRelation insertNewImpactRelation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRelation)get_store().insert_element_user(IMPACTRELATION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "impactRelation" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRelation addNewImpactRelation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRelation)get_store().add_element_user(IMPACTRELATION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "impactRelation" element
     */
    public void removeImpactRelation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IMPACTRELATION$0, i);
        }
    }
}
