package com.telstra.netops.routemanagement.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.routemanagement.server.exception.RMApplicationCustomException;
import com.telstra.netops.routemanagement.server.model.DeletePrefixRecord;
import com.telstra.netops.routemanagement.server.model.PrefixRecord;
import com.telstra.netops.routemanagement.server.model.PrefixUploadRequest;
import com.telstra.netops.routemanagement.server.model.Success;

@Service
@Profile("local")
public class LocalPrefixService implements PrefixService {

	@Value("${routemanagement.localtest.success}")
	private boolean success;

	@Override
	public List<PrefixRecord> list(PrefixUploadRequest request) {
		List<PrefixRecord> records = new ArrayList<PrefixRecord>();
		records.add(new PrefixRecord("", "", "", "", "", "AS1212", "", "", "", ""));
		records.add(new PrefixRecord("120.15.12.45/20", "1", "2016-02-22 10:30:22 GMT", "2016-02-22 10:30:22 GMT", "0",
				"AS1212", request.getCustomerId(), "785", "", ""));
		records.add(new PrefixRecord("202.15.123.45/24", "1", "2011-02-22 10:30:22 GMT", "2011-02-22 10:30:22 GMT", "0",
				"AS1212", request.getCustomerId(), "125", "", ""));
		return records;
	}

	@Override
	public Success remove(DeletePrefixRecord request) {
		if (success) {
			return new Success("SUCCESS!");
		} else {
			throw new RMApplicationCustomException("An error ocurred while fetching prefix records");
		}
	}

}
