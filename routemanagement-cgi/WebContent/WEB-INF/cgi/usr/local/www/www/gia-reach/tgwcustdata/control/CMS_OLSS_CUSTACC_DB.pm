package CMS_OLSS_CUSTACC_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;


our $error;

sub is_olss_cust_acc {
	my $loginid = $dbh->quote(shift @_);

	my $sth = $dbh->prepare("SELECT username from olss_cust_login_acc WHERE username ilike $loginid");

	if (my $result = $sth->execute) {
		if (($sth->fetchrow_array())[0]) {
			return 1;
		} else {
			return 0;
		}
	} else {
		$error = $dbh->errstr;
		return 0;
	}
}


sub verify_newpassword {
	my $loginid = $dbh->quote(shift @_);
	my $password = shift @_;

	$password = $dbh->quote(crypt ($password, $password));

	my $sth = $dbh->prepare("SELECT username FROM olss_cust_login_acc WHERE username ilike $loginid AND password_last1 != $password AND password_last2 != $password AND password_last3 != $password");

	if (my $result = $sth->execute) {
		if (($sth->fetchrow_array())[0]) {
			return 1;
		} else {
			return 0;
		}
	} else {
		$error = $dbh->errstr;
		return 0;
	}
}


sub change_password {
	my $loginid = shift @_;
	my $oldpassword = shift @_;
	my $password = shift @_;

	$password = $dbh->quote(crypt ($password, $password));

	if (not verify_plaintext_password($loginid, $oldpassword)) {
		return 0;
	}

	$loginid = $dbh->quote($loginid);

	my $sth = $dbh->prepare ("UPDATE olss_cust_login_acc SET password_last3 = password_last2, password_last2 = password_last1, password_last1 = password, password = $password, password_expirydate = CURRENT_DATE + interval '90 day', login_failure = '0' WHERE username ilike $loginid");

	my $result = $sth->execute;
	$error = $dbh->errstr;
	return $result;
}


sub verify_plaintext_password {
	my $loginid = $dbh->quote(shift @_);
	my $password = shift @_;
	$password = crypt $password, $password;

	my $sth = $dbh->prepare("SELECT password FROM olss_cust_login_acc WHERE username ilike $loginid");

	if (my $result = $sth->execute) {
		my $dbpassword = ($sth->fetchrow_array())[0];

		if ($dbpassword eq $password) {
			return $dbpassword;
		} else {
			$error = "Incorrect username and password.";
			return 0;
		}
	} else {
		$error = $dbh->errstr;
		return 0;
	}
}


sub verify_encrypted_password {
	my $loginid = $dbh->quote(shift @_);
	my $password = shift @_;

	my $sth = $dbh->prepare("SELECT password FROM olss_cust_login_acc WHERE username Like $loginid");
	
	if (my $result = $sth->execute) {
	
	#my @array=$sth->fetchrow_array());
		#$dbpassword = $array[0];
		#&print_error("Cannot validate user: $password");
		my $dbpassword=($sth->fetchrow_array())[0];
		
		if ($dbpassword eq $password) {
		
			return $dbpassword;
			
		} else {
			$error = "Incorrect username and password.";
			return 0;
		}
	} else {
		$error = $dbh->errstr;
		return 0;
	}
}


sub set_login_success {
	my $loginid = $dbh->quote(shift @_);

	my $sth = $dbh->prepare("UPDATE olss_cust_login_acc SET login_failure = '0' WHERE username ilike $loginid");
	my $result = $sth->execute;
	$error = $dbh->errstr;
	return $result;
}


sub set_login_failure {
	my $loginid = $dbh->quote(shift @_);

	my $sth = $dbh->prepare("UPDATE olss_cust_login_acc SET login_failure = login_failure + '1' WHERE username ilike $loginid");
	my $result = $sth->execute;
	$error = $dbh->errstr;
	return $result;
}


sub get_login_failure {
	my $loginid = $dbh->quote(shift @_);

	my $sth = $dbh->prepare("SELECT login_failure from olss_cust_login_acc WHERE username ilike $loginid");
	if (my $result = $sth->execute) {
		my $login_failure = ($sth->fetchrow_array())[0];
		return $login_failure;
	} else {
		$error = $dbh->errstr;
		return 5;
	}
}


sub check_password_expired {
	my $loginid = $dbh->quote(shift @_);

	my $sth = $dbh->prepare("SELECT date_part('epoch', password_expirydate) as unixtime FROM olss_cust_login_acc WHERE username ilike $loginid");

	if (my $result = $sth->execute) {
		my $expirydate = ($sth->fetchrow_array())[0];
		if ($expirydate) {
			if ($expirydate <= time) {
				return "Y";
			} else {
				return "N";
			}
		} else {
			return "Y";
		}
	} else {
		$error = $dbh->errstr;
		return "Y";
	}
}


sub get_olss_cust_access_accno {
	my $loginid = $dbh->quote(shift @_);

	my $sth = $dbh->prepare("SELECT olss_cust_access_acc.accno FROM olss_cust_access_acc, customer WHERE olss_cust_access_acc.accno = customer.accno AND olss_cust_access_acc.username ilike $loginid AND (customer.ep_access IS NULL OR customer.ep_access != 't') AND (customer.contract_ended IS NULL OR customer.contract_ended != 't') ORDER by accno");

	unless ($sth->execute) {
    		$error = $dbh->errstr;
    		return 0;
  	}

	my $result;
	my @access_accno;
	while ($result = $sth->fetchrow_arrayref) {
		push (@access_accno, $$result[0]);
	}
	$error = $dbh->errstr;
	$sth->finish;

	return \@access_accno;
}


sub get_customer_accno {
	my $sth = $dbh->prepare("SELECT accno FROM customer WHERE (ep_access IS NULL OR ep_access != 't') AND (contract_ended IS NULL OR contract_ended != 't') ORDER BY accno");

	unless ($sth->execute) {
    		$error = $dbh->errstr;
    		return 0;
  	}

  	my $result = $sth->fetchall_arrayref;

  	if ($result) {
    		$error = $dbh->errstr;
    		$sth->finish;
    		return $result;
  	} else {
    		$error = $dbh->errstr;
    		return 0;
  	}
}


sub get_account {
	my $loginid = $dbh->quote(shift @_);
	my @result;

	my $sth = $dbh->prepare ("SELECT firstname, lastname, company, division, phone, email, remark FROM olss_cust_login_acc WHERE username ilike $loginid");

	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

	my $result1 = $sth->fetchrow_arrayref;
	if ($result1) {
		$error = $dbh->errstr;
		$sth->finish;
		push (@result, @$result1);

		$sth = $dbh->prepare ("SELECT accno FROM olss_cust_access_acc WHERE username ilike $loginid");

		unless ($sth->execute) {
		$error = $dbh->errstr;
			return 0;
		}
		
		my $result2;
		my @access_accno;
		while ($result2 = $sth->fetchrow_arrayref) {
			push (@access_accno, $$result2[0]);
		}
		$error = $dbh->errstr;
		$sth->finish;
		push (@result, join("\0", @access_accno));

		return \@result;
	} else {
		$error = $dbh->errstr;
		return 0;
	}
}


sub add_account {
	CMS_DB::begin_transaction();
	my $loginid_raw = shift @_;
	my $loginid = $dbh_tr->quote($loginid_raw);
	my $firstname = $dbh_tr->quote(shift @_);
	my $lastname = $dbh_tr->quote(shift @_);
	my $company = $dbh_tr->quote(shift @_);
	my $division = $dbh_tr->quote(shift @_);
	my $phone = $dbh_tr->quote(shift @_);
	my $email = $dbh_tr->quote(shift @_);
	my $accno_access_raw = shift @_;
	my $remark = $dbh_tr->quote(shift @_);
	my $password = 'CUSTOMER';
	
	my @accno_access = split("\0", $accno_access_raw);

	my $sth = $dbh_tr->prepare ("SELECT username FROM olss_cust_login_acc WHERE username ilike $loginid");
	$sth->execute();
	my $r = $sth->fetchrow_hashref;
	if($$r{username} =~ /^$loginid_raw$/i) {
		return "Duplicated Login username.";
	}

	$password = $dbh_tr->quote(crypt($password, $password));

	my $success = 1;
	$sth = $dbh_tr->prepare ("INSERT INTO olss_cust_login_acc (username, password, password_expirydate, firstname, lastname, company, division, phone, email, remark, login_failure, password_last1, password_last2, password_last3)
				VALUES ($loginid, $password, CURRENT_DATE + interval '7 day', $firstname, $lastname, $company, $division, $phone, $email, $remark, '0', '', '', '')");
	$success &&= $sth->execute;
	$error = $dbh_tr->errstr;
	if ($success) {
		foreach my $a (@accno_access) {
			my $accno = $dbh_tr->quote($a);
			$sth = $dbh->prepare ("INSERT INTO olss_cust_access_acc (username, accno)
						VALUES ($loginid, $accno)");
			$success &&= $sth->execute;
			$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
		}
	}

	$success ? $dbh_tr->commit : $dbh_tr->rollback;
	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

	CMS_DB::disconnect_from_transaction();

	return $success;
}


sub update_account {
	CMS_DB::begin_transaction();
	my $loginid_raw = shift @_;
	my $loginid = $dbh_tr->quote($loginid_raw);
	my $firstname = $dbh_tr->quote(shift @_);
	my $lastname = $dbh_tr->quote(shift @_);
	my $company = $dbh_tr->quote(shift @_);
	my $division = $dbh_tr->quote(shift @_);
	my $phone = $dbh_tr->quote(shift @_);
	my $email = $dbh_tr->quote(shift @_);
	my $accno_access_raw = shift @_;
	my $remark = $dbh_tr->quote(shift @_);
	my $resetpassword = shift @_;
	my $password = 'CUSTOMER';

	my @accno_access = split("\0", $accno_access_raw);

	my $sth = $dbh_tr->prepare ("SELECT username FROM olss_cust_login_acc WHERE username ilike $loginid");
	$sth->execute();
	my $r = $sth->fetchrow_hashref;
	if(!defined($$r{username})) {
		return "Login username does not exist.";
	}

	my $success = 1;
	if (!$resetpassword) {
		$sth = $dbh_tr->prepare ("UPDATE olss_cust_login_acc set firstname = $firstname, lastname = $lastname, company = $company, division = $division, phone = $phone, email = $email, remark = $remark WHERE username ilike $loginid");
	} else {
		$password = $dbh_tr->quote(crypt($password, $password));
		$sth = $dbh_tr->prepare ("UPDATE olss_cust_login_acc set password_last3 = password_last2, password_last2 = password_last1, password_last1 = password, password = $password, password_expirydate = CURRENT_DATE + interval '7 day', firstname = $firstname, lastname = $lastname, company = $company, division = $division, phone = $phone, email = $email, remark = $remark, login_failure = '0' WHERE username ilike $loginid");
	}

	$success &&= $sth->execute;
	$error = $dbh_tr->errstr;
	if ($success) {
		$sth = $dbh_tr->prepare ("DELETE FROM olss_cust_access_acc WHERE username ilike $loginid");
		$success &&= $sth->execute;
		$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
		foreach my $a (@accno_access) {
			my $accno = $dbh_tr->quote($a);
			$sth = $dbh->prepare ("INSERT INTO olss_cust_access_acc (username, accno)
						VALUES ($loginid, $accno)");
			$success &&= $sth->execute;
			$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
		}
	}

	$success ? $dbh_tr->commit : $dbh_tr->rollback;
	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

	CMS_DB::disconnect_from_transaction();

	return $success;
}


sub delete_account {
	CMS_DB::begin_transaction();
	my $loginid_raw = shift @_;
	my $loginid = $dbh_tr->quote($loginid_raw);

	my $sth = $dbh_tr->prepare ("SELECT username FROM olss_cust_login_acc WHERE username ilike $loginid");
	$sth->execute();
	my $r = $sth->fetchrow_hashref;
	if(!defined($$r{username})) {
		return "Login username does not exist.";
	}

	my $success = 1;
	$sth = $dbh_tr->prepare ("DELETE FROM olss_cust_login_acc WHERE username ilike $loginid");
	$success &&= $sth->execute;
	$error = $dbh_tr->errstr;
	if ($success) {
		$sth = $dbh_tr->prepare ("DELETE FROM olss_cust_access_acc WHERE username = $loginid");
		$success &&= $sth->execute;
		$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
	}

	$success ? $dbh_tr->commit : $dbh_tr->rollback;
	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

	CMS_DB::disconnect_from_transaction();

	return $success;
}


sub search_account {
	my $loginid = shift @_;
	my $firstname = shift @_;
	my $lastname = shift @_;
	my $company = shift @_;
	my $accno_access = shift @_;
	my @retvals;

	my $command = "SELECT DISTINCT olss_cust_login_acc.username, firstname, lastname, company, division FROM olss_cust_login_acc, olss_cust_access_acc ";

	if ($loginid ne "") {
		$command .= "AND olss_cust_login_acc.username ilike '%$loginid%' "; 
	}
	if ($firstname ne "") {
		$command .= "AND firstname ilike '%$firstname%' "; 
	}
	if ($lastname ne "") {
		$command .= "AND lastname ilike '%$lastname%' "; 
	}
	if ($company ne "") {
		$command .= "AND company ilike '%$company%' "; 
	}
	if ($accno_access ne "") {
		$command .= "AND accno ilike '%$accno_access%' "; 
	}

	$command .= "AND olss_cust_login_acc.username = olss_cust_access_acc.username ";

	$command =~ s/AND/WHERE/; # change first AND
	$command .= "ORDER by olss_cust_login_acc.username";

	my $sth = $dbh->prepare($command);
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

	my @result;
	my $result1;
	while ($result1 = $sth->fetchrow_arrayref) {
		push (@result, @$result1);

		my $id = $dbh->quote($$result1[0]);
		my $sth1 = $dbh->prepare ("SELECT accno FROM olss_cust_access_acc WHERE username ilike $id");

		unless ($sth1->execute) {
		$error = $dbh->errstr;
			return 0;
		}

		my $result2;
		my @access_accno;
		while ($result2 = $sth1->fetchrow_arrayref) {
			push (@access_accno, $$result2[0]);
		}
		$error = $dbh->errstr;
		$sth1->finish;
		push (@result, join("<br>", @access_accno));

		push(@retvals, [@result]);
		undef(@result);
	}
	$error = $dbh->errstr;
	$sth->finish;

	return \@retvals;
}


sub delete_access_acc {
	my $access_accno = $dbh->quote(shift @_);

	my $sth = $dbh->prepare ("DELETE FROM olss_cust_access_acc WHERE accno = $access_accno");
	my $result = $sth->execute;
	$error = $dbh->errstr;

	return $result;
}

1;
