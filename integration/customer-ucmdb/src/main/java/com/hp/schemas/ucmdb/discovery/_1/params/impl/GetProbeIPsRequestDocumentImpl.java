/*
 * An XML document type.
 * Localname: getProbeIPsRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getProbeIPsRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetProbeIPsRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequestDocument
{
    
    public GetProbeIPsRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETPROBEIPSREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbeIPsRequest");
    
    
    /**
     * Gets the "getProbeIPsRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequest getGetProbeIPsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequest)get_store().find_element_user(GETPROBEIPSREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getProbeIPsRequest" element
     */
    public void setGetProbeIPsRequest(com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequest getProbeIPsRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequest)get_store().find_element_user(GETPROBEIPSREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequest)get_store().add_element_user(GETPROBEIPSREQUEST$0);
            }
            target.set(getProbeIPsRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "getProbeIPsRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequest addNewGetProbeIPsRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsRequest)get_store().add_element_user(GETPROBEIPSREQUEST$0);
            return target;
        }
    }
}
