package com.telstra.netops.lookingglass.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NodeTestRequest {
	
	@JsonProperty
	private String level;
	
	@JsonProperty
	private String pop;
	
	@JsonProperty
	private String toHost;
	
	@JsonProperty
	private String submit;

	public NodeTestRequest() {
	}	
	
	public NodeTestRequest(String level, String pop, String toHost,
			String submit) {
		super();
		this.level = level;
		this.pop = pop;
		this.toHost = toHost;
		this.submit = submit;
	}


	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getPop() {
		return pop;
	}

	public void setPop(String pop) {
		this.pop = pop;
	}

	public String getToHost() {
		return toHost;
	}

	public void setToHost(String toHost) {
		this.toHost = toHost;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

}
