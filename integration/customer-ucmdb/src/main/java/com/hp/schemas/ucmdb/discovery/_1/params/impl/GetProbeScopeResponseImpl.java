/*
 * XML Type:  getProbeScopeResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML getProbeScopeResponse(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class GetProbeScopeResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetProbeScopeResponse
{
    
    public GetProbeScopeResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PROBESCOPE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "probeScope");
    
    
    /**
     * Gets the "probeScope" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.Scope getProbeScope()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.Scope target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.Scope)get_store().find_element_user(PROBESCOPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "probeScope" element
     */
    public void setProbeScope(com.hp.schemas.ucmdb.discovery._1.types.Scope probeScope)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.Scope target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.Scope)get_store().find_element_user(PROBESCOPE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.types.Scope)get_store().add_element_user(PROBESCOPE$0);
            }
            target.set(probeScope);
        }
    }
    
    /**
     * Appends and returns a new empty "probeScope" element
     */
    public com.hp.schemas.ucmdb.discovery._1.types.Scope addNewProbeScope()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.types.Scope target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.types.Scope)get_store().add_element_user(PROBESCOPE$0);
            return target;
        }
    }
}
