/*
 * XML Type:  TypedPropertiesCollection
 * Namespace: http://schemas.hp.com/ucmdb/1/types/props
 * Java type: com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.props.impl;
/**
 * An XML TypedPropertiesCollection(@http://schemas.hp.com/ucmdb/1/types/props).
 *
 * This is a complex type.
 */
public class TypedPropertiesCollectionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection
{
    
    public TypedPropertiesCollectionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TYPEDPROPERTIES$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "typedProperties");
    
    
    /**
     * Gets array of all "typedProperties" elements
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedProperties[] getTypedPropertiesArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(TYPEDPROPERTIES$0, targetList);
            com.hp.schemas.ucmdb._1.types.props.TypedProperties[] result = new com.hp.schemas.ucmdb._1.types.props.TypedProperties[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "typedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedProperties getTypedPropertiesArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedProperties)get_store().find_element_user(TYPEDPROPERTIES$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "typedProperties" element
     */
    public int sizeOfTypedPropertiesArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TYPEDPROPERTIES$0);
        }
    }
    
    /**
     * Sets array of all "typedProperties" element
     */
    public void setTypedPropertiesArray(com.hp.schemas.ucmdb._1.types.props.TypedProperties[] typedPropertiesArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(typedPropertiesArray, TYPEDPROPERTIES$0);
        }
    }
    
    /**
     * Sets ith "typedProperties" element
     */
    public void setTypedPropertiesArray(int i, com.hp.schemas.ucmdb._1.types.props.TypedProperties typedProperties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedProperties)get_store().find_element_user(TYPEDPROPERTIES$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(typedProperties);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "typedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedProperties insertNewTypedProperties(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedProperties)get_store().insert_element_user(TYPEDPROPERTIES$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "typedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.TypedProperties addNewTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.TypedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.TypedProperties)get_store().add_element_user(TYPEDPROPERTIES$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "typedProperties" element
     */
    public void removeTypedProperties(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TYPEDPROPERTIES$0, i);
        }
    }
}
