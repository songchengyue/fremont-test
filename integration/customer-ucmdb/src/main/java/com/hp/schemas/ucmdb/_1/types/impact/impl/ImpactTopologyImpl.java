/*
 * XML Type:  ImpactTopology
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactTopology
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML ImpactTopology(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ImpactTopologyImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactTopology
{
    
    public ImpactTopologyImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CIS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "CIs");
    private static final javax.xml.namespace.QName IMPACTRELATIONS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "impactRelations");
    
    
    /**
     * Gets the "CIs" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs getCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(CIS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CIs" element
     */
    public void setCIs(com.hp.schemas.ucmdb._1.types.CIs cIs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(CIS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(CIS$0);
            }
            target.set(cIs);
        }
    }
    
    /**
     * Appends and returns a new empty "CIs" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs addNewCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(CIS$0);
            return target;
        }
    }
    
    /**
     * Gets the "impactRelations" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRelations getImpactRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRelations target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRelations)get_store().find_element_user(IMPACTRELATIONS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "impactRelations" element
     */
    public void setImpactRelations(com.hp.schemas.ucmdb._1.types.impact.ImpactRelations impactRelations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRelations target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRelations)get_store().find_element_user(IMPACTRELATIONS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRelations)get_store().add_element_user(IMPACTRELATIONS$2);
            }
            target.set(impactRelations);
        }
    }
    
    /**
     * Appends and returns a new empty "impactRelations" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRelations addNewImpactRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRelations target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRelations)get_store().add_element_user(IMPACTRELATIONS$2);
            return target;
        }
    }
}
