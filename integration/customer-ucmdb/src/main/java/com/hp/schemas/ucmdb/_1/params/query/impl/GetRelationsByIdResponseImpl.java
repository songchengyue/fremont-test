/*
 * XML Type:  getRelationsByIdResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML getRelationsByIdResponse(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class GetRelationsByIdResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetRelationsByIdResponse
{
    
    public GetRelationsByIdResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RELATIONS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "Relations");
    private static final javax.xml.namespace.QName CHUNKINFO$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "chunkInfo");
    
    
    /**
     * Gets the "Relations" element
     */
    public com.hp.schemas.ucmdb._1.types.Relations getRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().find_element_user(RELATIONS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Relations" element
     */
    public void setRelations(com.hp.schemas.ucmdb._1.types.Relations relations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().find_element_user(RELATIONS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().add_element_user(RELATIONS$0);
            }
            target.set(relations);
        }
    }
    
    /**
     * Appends and returns a new empty "Relations" element
     */
    public com.hp.schemas.ucmdb._1.types.Relations addNewRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().add_element_user(RELATIONS$0);
            return target;
        }
    }
    
    /**
     * Gets the "chunkInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkInfo getChunkInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().find_element_user(CHUNKINFO$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "chunkInfo" element
     */
    public void setChunkInfo(com.hp.schemas.ucmdb._1.types.ChunkInfo chunkInfo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().find_element_user(CHUNKINFO$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().add_element_user(CHUNKINFO$2);
            }
            target.set(chunkInfo);
        }
    }
    
    /**
     * Appends and returns a new empty "chunkInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkInfo addNewChunkInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().add_element_user(CHUNKINFO$2);
            return target;
        }
    }
}
