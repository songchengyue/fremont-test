
/**
 * UcmdbFault.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

package com.hp.ucmdb.generated;

public class UcmdbFault extends java.lang.Exception{
    
    private com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument faultMessage;
    
    public UcmdbFault() {
        super("UcmdbFault");
    }
           
    public UcmdbFault(java.lang.String s) {
       super(s);
    }
    
    public UcmdbFault(java.lang.String s, java.lang.Throwable ex) {
      super(s, ex);
    }
    
    public void setFaultMessage(com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument msg){
       faultMessage = msg;
    }
    
    public com.hp.schemas.ucmdb._1.params.ucmdb.UcmdbFaultDocument getFaultMessage(){
       return faultMessage;
    }
}
    