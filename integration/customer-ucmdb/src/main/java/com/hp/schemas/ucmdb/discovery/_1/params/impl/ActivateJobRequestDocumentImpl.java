/*
 * An XML document type.
 * Localname: activateJobRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one activateJobRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class ActivateJobRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequestDocument
{
    
    public ActivateJobRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ACTIVATEJOBREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "activateJobRequest");
    
    
    /**
     * Gets the "activateJobRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequest getActivateJobRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequest)get_store().find_element_user(ACTIVATEJOBREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "activateJobRequest" element
     */
    public void setActivateJobRequest(com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequest activateJobRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequest)get_store().find_element_user(ACTIVATEJOBREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequest)get_store().add_element_user(ACTIVATEJOBREQUEST$0);
            }
            target.set(activateJobRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "activateJobRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequest addNewActivateJobRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.ActivateJobRequest)get_store().add_element_user(ACTIVATEJOBREQUEST$0);
            return target;
        }
    }
}
