/*
 * An XML document type.
 * Localname: executeTopologyQueryByNameResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one executeTopologyQueryByNameResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class ExecuteTopologyQueryByNameResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponseDocument
{
    
    public ExecuteTopologyQueryByNameResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EXECUTETOPOLOGYQUERYBYNAMERESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryByNameResponse");
    
    
    /**
     * Gets the "executeTopologyQueryByNameResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse getExecuteTopologyQueryByNameResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse)get_store().find_element_user(EXECUTETOPOLOGYQUERYBYNAMERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "executeTopologyQueryByNameResponse" element
     */
    public void setExecuteTopologyQueryByNameResponse(com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse executeTopologyQueryByNameResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse)get_store().find_element_user(EXECUTETOPOLOGYQUERYBYNAMERESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse)get_store().add_element_user(EXECUTETOPOLOGYQUERYBYNAMERESPONSE$0);
            }
            target.set(executeTopologyQueryByNameResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "executeTopologyQueryByNameResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse addNewExecuteTopologyQueryByNameResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameResponse)get_store().add_element_user(EXECUTETOPOLOGYQUERYBYNAMERESPONSE$0);
            return target;
        }
    }
}
