/*
 * An XML document type.
 * Localname: getCredentialsEntryRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getCredentialsEntryRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetCredentialsEntryRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequestDocument
{
    
    public GetCredentialsEntryRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCREDENTIALSENTRYREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getCredentialsEntryRequest");
    
    
    /**
     * Gets the "getCredentialsEntryRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest getGetCredentialsEntryRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest)get_store().find_element_user(GETCREDENTIALSENTRYREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCredentialsEntryRequest" element
     */
    public void setGetCredentialsEntryRequest(com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest getCredentialsEntryRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest)get_store().find_element_user(GETCREDENTIALSENTRYREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest)get_store().add_element_user(GETCREDENTIALSENTRYREQUEST$0);
            }
            target.set(getCredentialsEntryRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "getCredentialsEntryRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest addNewGetCredentialsEntryRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntryRequest)get_store().add_element_user(GETCREDENTIALSENTRYREQUEST$0);
            return target;
        }
    }
}
