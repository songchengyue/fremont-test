package com.telstra.netops.lookingglass.server.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.lookingglass.server.model.ClientNodeTestRequest;
import com.telstra.netops.lookingglass.server.model.TracerouteHop;

@Service
@Profile("local")
public class LocalTracerouteTestService implements TracerouteTestService {
	
	@Autowired
    private LocalErrorThrowingService errorThrowingService; 
	
	@Override
	public List<TracerouteHop> performTracerouteTest(ClientNodeTestRequest request) {
		errorThrowingService.throwError();
		
		return Arrays.asList(
				new TracerouteHop[] {
					new TracerouteHop("  1 2001::10:66:66:2 0 msec 4 msec 0 msec"),
					new TracerouteHop("  2 FC00:1::71 [AS 2] [MPLS: Labels 1048491/16007 Exp 0] 4 msec 0 msec 4 msec"),
					new TracerouteHop("  3 2403:E800:E800:1000::1 [AS 2] [MPLS: Labels 16313/16007 Exp 0] 8 msec 0 msec 8 msec"),
					new TracerouteHop("  4 2001:2:4::1 [AS 2] [MPLS: Labels 1048567/16007 Exp 0] 0 msec 4 msec 4 msec"),
					new TracerouteHop("  5 2001::10:0:0:20 [AS 2] 8 msec 8 msec 8 msec"),
					}
				);
	}
	
}
