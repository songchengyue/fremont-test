# Copyright Maettr Pty Ltd.
#
# File: CMS_PC_DB.pm
#
# $Id: CMS_PC_DB.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $
# $Log: CMS_PC_DB.pm,v $
# Revision 1.3  2016/07/07 15:19:16  d804709
# sit fixes
#
# Revision 1.7  2004/02/02 10:51:42  rxc
# Updated get_product codes - cannot use CMS_OLSS as that function destroys dbh
#
# Revision 1.6  2004/01/15 22:25:27  paull
# *** empty log message ***
#
# Revision 1.5  2004/01/15 22:08:23  paull
# In CMS_PC_DB, accounted for wholesale customers in subroutine get_product_codes_for_accno()
# In CMS_CUSTOMER_DB.pm, removed $dbh->quote() around $product_code argument since it is explicitly quoted in the SQL statement.
#
# Revision 1.4  2003/12/31 00:51:18  paull
# Minor changes made to routines verify_accno_product(), get_product_codes_for_accno(),
# in module CMS_PC_DB by rchew.
# "use CMS_PC_DB;" added to module CMS_OLSS.pm.
# Changes in default behaviour for routines get_serviceid_for_accno() and get_serviceid_for_wholesaler_accno()
# in module CMS_CUSTOMER_DB.pm.
#
# Revision 1.3  2003/10/30 12:40:28  paull
# Added routine get_productname_for_productcode() to CMS_OLSS.pm.
#
# Revision 1.2  2003/10/27 11:53:39  paull
# In module CMS_PC_DB.pm, forced all productcodeid's to be uppercase and
# ordered output of methods alphabetically based on productcodeid.
#
# Changed return value for get_product_codes_for_accno() to be reference
# to hash of hash references.
#
# Added directory 'eportal' which lists changes made and includes psql
# script to update the cms database.
#
# Revision 1.1  2003/10/20 15:00:22  paull
# Changed modules and new modules by Paul Libauer
#

package CMS_PC_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
#use CMS_OLSS;
use CMS_CUSTOMER_DB;

our $error;

sub add_productcode {
  my $productcode = uc $dbh->quote(shift @_);
  my $productname = $dbh->quote(shift @_);
  
  my $sth = $dbh->prepare
    ("INSERT INTO productcode VALUES($productcode,$productname)");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  return 1;
}

sub get_productcodes {
  
  my $sth = $dbh->prepare
    ("SELECT productcodeid FROM productcode ORDER BY productcodeid");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  
  if($result->[0]) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } 
  else {
    $error = $dbh->errstr;
    return 0;
  }
}


sub get_productname {
  my $productcode = uc $dbh->quote(shift @_);
  
  my $sth = $dbh->prepare
    ("SELECT productname FROM productcode WHERE productcodeid = $productcode");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my @result = $sth->fetchrow_array;
  
  if($result[0]) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result[0];
  } 
  else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub update_productname {
  my $productcode = uc $dbh->quote(shift @_);
  my $productname = $dbh->quote(shift @_);
  
  my $sth = $dbh->prepare
    ("UPDATE productcode SET productname = $productname WHERE ".
     "productcodeid = $productcode");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  return 1;
}


sub delete_productcode {
  my $productcode = uc $dbh->quote(shift @_);
  
  my $sth = $dbh->prepare
    ("DELETE FROM productcode WHERE productcodeid = $productcode");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  return 1;
}

sub get_product_codes_for_accno {
  my $accno       = shift @_;
  # Determine if the account is a wholesaler
  #my $isWholesaler = CMS_OLSS::is_wholesaler($accno);
  my $isWholesaler = CMS_CUSTOMER_DB::is_wholesaler($accno);
  if($isWholesaler){

    my %results;
    #--------------------------------------------------------------------------
    # get children
    my $sth = $dbh->prepare
      ("SELECT DISTINCT productcode.productcodeid,productname FROM service,customer,".
       "productcode WHERE ".
       "service.accno = customer.accno AND ".
       "customer.wholesaler_accno = ? AND ".
       "service.productcodeid = productcode.productcodeid");
    
    unless ($sth->execute($accno)) {
      $error = $dbh->errstr;
      return 0;
    }
    
    if (my $result = $sth->fetchall_arrayref) {
      for(@$result){
	$results{$_->[0]} = { code => $_->[0], name => $_->[1] };
      }
    } 
    else {
      $error = $dbh->errstr;
      $sth->finish;
      return 0;
    }

    #--------------------------------------------------------------------------
    # get grand children
    $sth = $dbh->prepare
      ("SELECT DISTINCT productcode.productcodeid,productname FROM service,productcode,".
       "customer AS child, customer AS grandchild WHERE ".
       "service.accno = grandchild.accno AND ".
       "grandchild.wholesaler_accno = child.accno AND ".
       "child.wholesaler_accno = ? AND ".
       "service.productcodeid = productcode.productcodeid");

    unless ($sth->execute($accno)) {
      $error = $dbh->errstr;
      return 0;
    }

    if (my $result = $sth->fetchall_arrayref) {
      for(@$result){
	$results{$_->[0]} = { code => $_->[0], name => $_->[1] };
      }
    } 
    else {
      $error = $dbh->errstr;
      $sth->finish;
      return \%results;
    }
#-------------------------------------------------------------------------------------------
#added as a fix to get all the product codes for master accno
$sth = $dbh->prepare
("SELECT DISTINCT productcode.productcodeid,productname FROM service,productcode,".
 "customer AS child, customer AS grandchild ,customer as greatgrandchild ,".
 "customer as greatgreatgrandchild WHERE service.accno = child.accno AND ".
 "grandchild.accno=child.wholesaler_accno AND greatgrandchild.accno=grandchild.wholesaler_accno AND ".
 "greatgreatgrandchild.accno=greatgrandchild.wholesaler_accno AND greatgrandchild.wholesaler_accno = ? AND ".
 "service.productcodeid = productcode.productcodeid");

      unless ($sth->execute($accno)) {
      $error = $dbh->errstr;
      return 0;
    }

    if (my $result = $sth->fetchall_arrayref) {
      for(@$result){
        $results{$_->[0]} = { code => $_->[0], name => $_->[1] };
      }
    }
    else {
      $error = $dbh->errstr;
      $sth->finish;
      return \%results;
    }


    # dont want to get past here
    return \%results;

    #--------------------------------------------------------------------------
    # get great grand children
    # WARNING: This select can chew a lot of process for a long time for large customer
    #          tables.

    $sth = $dbh->prepare
      ("SELECT DISTINCT productcode.productcodeid,productname FROM service,productcode,".
       "customer AS child, customer AS grandchild, customer AS greatgrandchild WHERE ".
       "service.accno = greatgrandchild.wholesaler_accno AND ".
       "greatgrandchild.wholesaler_accno = grandchild.wholesaler_accno AND ".
       "grandchild.wholesaler_accno = child.accno AND ".
       "child.wholesaler_accno = ? AND ".
       "service.productcodeid = productcode.productcodeid");
   
    unless ($sth->execute($accno)) {
      $error = $dbh->errstr;
      return 0;
    }

    if (my $result = $sth->fetchall_arrayref) {
      for(@$result){
	$results{$_->[0]} = { code => $_->[0], name => $_->[1] };
      }
    } 
    else {
      $error = $dbh->errstr;
      $sth->finish;
      return \%results;
    }
  }
  else {

    my $sth = $dbh->prepare
      ("SELECT DISTINCT productcode.productcodeid,productname FROM service,productcode ".
       "WHERE service.accno = ? AND service.productcodeid = ".
       "productcode.productcodeid ORDER BY productcode.productcodeid");
    unless ($sth->execute($accno)) {
      $error = $dbh->errstr;
      return 0;
    }
    my $result = $sth->fetchall_arrayref;
    if ($result->[0]) {
      $error = $dbh->errstr;
      $sth->finish;
      
      my %ret;
      for(@$result){
	$ret{$_->[0]} = { code => $_->[0], name => $_->[1] };
      }
      return \%ret;
    }
    else {
      $error = $dbh->errstr;

      return 0;
    }
  }
}

sub verify_accno_product {
  my $accno       = shift @_;
  #my $productcode = uc $dbh->quote(shift @_);
  my $productcode = shift @_;
  my $current = get_product_codes_for_accno($accno);


  #if(ref($current) eq 'ARRAY'){
  if(ref($current) eq 'HASH'){
    #for(@$current){
    
    foreach my $lk (keys %$current) {
        
      #print "<P>DD ($lk) ($$current{$lk}{code}) ($productcode)\n";
      #return 'TRUE' if ($_->{productcodeid} =~ /^$productcode$/i);
      return 'TRUE' if ($$current{$lk}{code} =~ /^$productcode$/i);
    }
  }

  return 'FALSE'
}

1;
