/*
 * XML Type:  ImpactRules
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactRules
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML ImpactRules(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ImpactRulesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactRules
{
    
    public ImpactRulesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTRULE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "impactRule");
    
    
    /**
     * Gets array of all "impactRule" elements
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRule[] getImpactRuleArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IMPACTRULE$0, targetList);
            com.hp.schemas.ucmdb._1.types.impact.ImpactRule[] result = new com.hp.schemas.ucmdb._1.types.impact.ImpactRule[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "impactRule" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRule getImpactRuleArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRule target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRule)get_store().find_element_user(IMPACTRULE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "impactRule" element
     */
    public int sizeOfImpactRuleArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IMPACTRULE$0);
        }
    }
    
    /**
     * Sets array of all "impactRule" element
     */
    public void setImpactRuleArray(com.hp.schemas.ucmdb._1.types.impact.ImpactRule[] impactRuleArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(impactRuleArray, IMPACTRULE$0);
        }
    }
    
    /**
     * Sets ith "impactRule" element
     */
    public void setImpactRuleArray(int i, com.hp.schemas.ucmdb._1.types.impact.ImpactRule impactRule)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRule target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRule)get_store().find_element_user(IMPACTRULE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(impactRule);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "impactRule" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRule insertNewImpactRule(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRule target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRule)get_store().insert_element_user(IMPACTRULE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "impactRule" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRule addNewImpactRule()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRule target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRule)get_store().add_element_user(IMPACTRULE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "impactRule" element
     */
    public void removeImpactRule(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IMPACTRULE$0, i);
        }
    }
}
