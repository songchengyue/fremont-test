#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;

use lib "/usr/local/www/wanchai/netops/modules";
use lib "/usr/local/www/wanchai/modules";
use CMS_DB;
use CMS_OLSS;
use CMS_DNSRESOLVER_DB;


#use constant FILE_PATH_ACL => "/var/named/etc/namedb/resolver/indonesia/reach.acl";
use constant FILE_PATH_ACL => '/usr/local/www/wanchai/netops/dns/resolver/indonesia/reach.acl';

use constant FILE_PATH_BK => "/var/named/etc/namedb/resolver/backup/indonesia/reach.acl.old";
#use constant FILE_PATH_BK =>'D:\perl\New folder\DNSresolver\WebContent\WEB-INF\cgi\var\named\etc\namedb\resolver\backup\reach.acl.old';

use constant FILE_PATH_ACL_TEMPL =>  "/usr/local/www/wanchai/netops/scripts/make_dnsresolv_conf.template.conf";
#use constant FILE_PATH_ACL_TEMPL =>  'D:\perl\New folder\DNSresolver\WebContent\WEB-INF\cgi\usr\local\www\wanchai\cms\scripts\make_dnsresolv_conf.template.conf';
my $new_ip2 = '';
my $all_val = '';
my $diff = '';
my @new_ip1 = '';
my @new_ip = '';
my @records = '';
my $stat = '';
my @statip = '';
my $ips = '';
my $hr = '';
my $mth = '';
my $day = '';
my $mailagent = "/usr/sbin/sendmail -t";

CMS_DB::connect_to_database;
#fetch IP addresses
my $dnsres = CMS_DNSRESOLVER_DB::get_acl_parm_location(44892);
my $dnstime = CMS_DNSRESOLVER_DB::time_db(44892);
my $stat_ip = CMS_DNSRESOLVER_DB::status_ip;

#calculate current time
my ($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = gmtime;
my $year = 1900 + $yearOffset;
my $month1 = $month + 1;
if ($month1<=9){
$mth = "0".$month1;}
if ($dayOfMonth<=9){$day = "0".$dayOfMonth;}
else {$day = $dayOfMonth;}
if ($hour<=9){$hr = "0".$hr;}
my $serial_number = "$year$mth$day$hr";
my $cur_sec = (($hour*3600)+($minute*60)+$second);
my $cur_time = "$hour:$minute:$second";

my @ar_time = '';
my @hr_format = '';
my @sec_format = '';
my @db_date1 = '';
my @db_mon = '';
my @db_day = '';
my @db_year = '';
my @req_time ='';

#get request_time from DB
foreach my $row (@$dnstime){
push (@ar_time, $$row[0]);
push (@req_time, $$row[0]);
        my @db_hr1 = split(/\+/,$$row[0]);
        my @db_hr = split(/ /,$db_hr1[0]);
        my @db_sec1 = split(/\./,$db_hr[1]);
        my $db_sec = (($db_sec1[0]*3600)+ ($db_sec1[1]*60) + ($db_sec1[2]));
        my @db_date1 = split (/-/,$db_hr[0]);
        push (@sec_format, $db_sec);
        push (@hr_format, $db_hr[1]);
        push (@db_year,$db_date1[0]);
        push (@db_mon,$db_date1[1]);
        push (@db_day,$db_date1[2]);
shift (@ar_time);
}
my $trial = $db_mon[1]*5;
my $len_ar = scalar @hr_format;

my $file_build_date = `date`;
chomp ($file_build_date);
        my @value = '';


#populate a local array with the IP addresses
push (@value, "localhost;");
push (@value, "\n");
push (@value, "\t");

foreach my $record (@$dnsres){
       push (@value, $$record[0]);
       push (@value, ";");
      push (@value, "\n");
      push (@value, "\t");
#print PRE "value: @value\n";
}

### construct  template file
my $base = '';
my @iphcode = '';
        open(TEMPL, FILE_PATH_ACL_TEMPL) || die "cant open ACL template file: $!";
        while (my $l = <TEMPL>) { $base .= $l; }
        close(TEMPL);
#hard code the IP addresses as of now
#@iphcode = (" localhost;","\n","\t","202.84.128.0\/24;","\n","\t","160.83.128.0\/19;","\n","\t","202.128.129.19;","\n","\t","
#202.84.248.57\/32;","\n","\t","202.128.129.20;");

my $dnsacl = '';
my $p_name = "reach.permitted";
#replace constants in template with actual values
$base =~ s/\*REACH.PERMITTED\*/$p_name/;
$base =~ s/\*ALLOWED_IP\*/@value/;
$base =~ s/\*FILEBUILDDATE\*/$file_build_date/;

#appends the base to a variable
$dnsacl .= $base;
my $lpct = 0;

for ($lpct = 0; $lpct <= ($len_ar-1); $lpct++)
{
$dayOfMonth =~ s/^\s+|\s+$//g;
my $db_day1 = shift @db_day;
$db_day1 =~ s/^\s+|\s+$//g;
$month1 =~ s/^\s+|\s+$//g;
my $db_mon1 = shift @db_mon;
$db_mon1  =~ s/^\s+|\s+$//g;
$year =~ s/^\s+|\s+$//g;
my $db_year1 = shift @db_year;
$db_year1 =~ s/^\s+|\s+$//g;
my $trial1 = $db_mon1*5;
my $sec_format1 = shift @sec_format;

print "bfr write";
print "$day: $month1:  $year:  $cur_sec:  \n";
print "$db_day1: $trial1:  $db_year1:  $sec_format1:  \n";

#update acl only if recently added/deleted entries
if (($day eq $db_day1) && (($month1*5) == $trial1) && ($year eq $db_year1) && (($cur_sec - $sec_format1)<=3600) ){
print "inside time check \n ";
my $reqtime = $req_time[$lpct];
my $all_val = CMS_DNSRESOLVER_DB::all_values($reqtime,44892);
        foreach my $rec (@$all_val){
        push (@records, $$rec[0]);
        push (@records, $$rec[1]);
        push (@records, $$rec[2]);
        push (@records, $$rec[3]);
        push (@records, $$rec[4]);
        push (@records, $$rec[5]);
        #push (@records, $$rec[6]);
        my $value_type = '';
        if ($records[6] == 1) { $value_type = "REQUESTED"; } elsif ($records[6] ==3) { $value_type = "CANCELLED";}
        my $uid = $records[1];
        my $accno = $records[2];
        my $dns_contact = $records[3];
        my $dns_dom = $records[4];
        my $dns_aip= $records[5];
        my $mail = CMS_DNSRESOLVER_DB::DNSRESOLVER_mail_REQ($uid, $accno, $dns_contact, $dns_dom, $dns_aip, $value_type);
        @records = '';
}	
	
#backup
`cp -p /usr/local/www/wanchai/netops/dns/resolver/indonesia/reach.acl /etc/namedb/backup/indonesia/reach.acl.$serial_number`;
print "writing ACL file...\n";
#writes the ACL file
open(CONF, ">".FILE_PATH_ACL);
print CONF "$dnsacl\n";
close(CONF);
        } 
}
CMS_DB::disconnect_from_database;
