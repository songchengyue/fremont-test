/*
 * An XML document type.
 * Localname: executeTopologyQueryByNameWithParameters
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one executeTopologyQueryByNameWithParameters(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class ExecuteTopologyQueryByNameWithParametersDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersDocument
{
    
    public ExecuteTopologyQueryByNameWithParametersDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EXECUTETOPOLOGYQUERYBYNAMEWITHPARAMETERS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryByNameWithParameters");
    
    
    /**
     * Gets the "executeTopologyQueryByNameWithParameters" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParameters getExecuteTopologyQueryByNameWithParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParameters target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParameters)get_store().find_element_user(EXECUTETOPOLOGYQUERYBYNAMEWITHPARAMETERS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "executeTopologyQueryByNameWithParameters" element
     */
    public void setExecuteTopologyQueryByNameWithParameters(com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParameters executeTopologyQueryByNameWithParameters)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParameters target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParameters)get_store().find_element_user(EXECUTETOPOLOGYQUERYBYNAMEWITHPARAMETERS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParameters)get_store().add_element_user(EXECUTETOPOLOGYQUERYBYNAMEWITHPARAMETERS$0);
            }
            target.set(executeTopologyQueryByNameWithParameters);
        }
    }
    
    /**
     * Appends and returns a new empty "executeTopologyQueryByNameWithParameters" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParameters addNewExecuteTopologyQueryByNameWithParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParameters target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParameters)get_store().add_element_user(EXECUTETOPOLOGYQUERYBYNAMEWITHPARAMETERS$0);
            return target;
        }
    }
}
