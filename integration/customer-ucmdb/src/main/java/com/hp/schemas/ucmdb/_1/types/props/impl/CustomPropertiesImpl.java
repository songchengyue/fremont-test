/*
 * XML Type:  CustomProperties
 * Namespace: http://schemas.hp.com/ucmdb/1/types/props
 * Java type: com.hp.schemas.ucmdb._1.types.props.CustomProperties
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.props.impl;
/**
 * An XML CustomProperties(@http://schemas.hp.com/ucmdb/1/types/props).
 *
 * This is a complex type.
 */
public class CustomPropertiesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.props.CustomProperties
{
    
    public CustomPropertiesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PROPERTIESLIST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "propertiesList");
    private static final javax.xml.namespace.QName PREDEFINEDPROPERTIES$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "predefinedProperties");
    
    
    /**
     * Gets the "propertiesList" element
     */
    public com.hp.schemas.ucmdb._1.types.props.PropertiesList getPropertiesList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PropertiesList target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PropertiesList)get_store().find_element_user(PROPERTIESLIST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "propertiesList" element
     */
    public boolean isSetPropertiesList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PROPERTIESLIST$0) != 0;
        }
    }
    
    /**
     * Sets the "propertiesList" element
     */
    public void setPropertiesList(com.hp.schemas.ucmdb._1.types.props.PropertiesList propertiesList)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PropertiesList target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PropertiesList)get_store().find_element_user(PROPERTIESLIST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.PropertiesList)get_store().add_element_user(PROPERTIESLIST$0);
            }
            target.set(propertiesList);
        }
    }
    
    /**
     * Appends and returns a new empty "propertiesList" element
     */
    public com.hp.schemas.ucmdb._1.types.props.PropertiesList addNewPropertiesList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PropertiesList target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PropertiesList)get_store().add_element_user(PROPERTIESLIST$0);
            return target;
        }
    }
    
    /**
     * Unsets the "propertiesList" element
     */
    public void unsetPropertiesList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PROPERTIESLIST$0, 0);
        }
    }
    
    /**
     * Gets the "predefinedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.PredefinedProperties getPredefinedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PredefinedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PredefinedProperties)get_store().find_element_user(PREDEFINEDPROPERTIES$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "predefinedProperties" element
     */
    public boolean isSetPredefinedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREDEFINEDPROPERTIES$2) != 0;
        }
    }
    
    /**
     * Sets the "predefinedProperties" element
     */
    public void setPredefinedProperties(com.hp.schemas.ucmdb._1.types.props.PredefinedProperties predefinedProperties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PredefinedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PredefinedProperties)get_store().find_element_user(PREDEFINEDPROPERTIES$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.PredefinedProperties)get_store().add_element_user(PREDEFINEDPROPERTIES$2);
            }
            target.set(predefinedProperties);
        }
    }
    
    /**
     * Appends and returns a new empty "predefinedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.PredefinedProperties addNewPredefinedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PredefinedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PredefinedProperties)get_store().add_element_user(PREDEFINEDPROPERTIES$2);
            return target;
        }
    }
    
    /**
     * Unsets the "predefinedProperties" element
     */
    public void unsetPredefinedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREDEFINEDPROPERTIES$2, 0);
        }
    }
}
