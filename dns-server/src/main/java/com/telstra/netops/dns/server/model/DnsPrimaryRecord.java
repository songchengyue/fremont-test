package com.telstra.netops.dns.server.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DnsPrimaryRecord {
	
	@JsonProperty("DBrowId")
	private String id;

	@JsonProperty("Hostname")
	private String hostName;
	
	@JsonProperty("primaryDNS")
	private String ipBlock;
	
	@JsonProperty("Contact")
	private String contactEmailAddress;
	

	@JsonProperty("Account")
	private String customerId;
	
	@JsonProperty("header")
	private String header;
	
	@JsonProperty("Error")
	private String error;
	
	@JsonProperty("recordType")
	private String recordType;
	

	@JsonProperty("parameter")
	private String parameter;
	
	@JsonProperty("request")
	private String request;
	
	@JsonProperty("Domain") //
	private String domain;
	
	@JsonProperty("RequestTime")
	@JsonFormat(shape = JsonFormat.Shape.STRING , pattern = "yyyy-dd-MM HH:mm:ss" ,timezone = "UTC")
	private Date requestDate;

	public DnsPrimaryRecord() {
	}
	
	public DnsPrimaryRecord(String id, String hostName, String ipBlock,
			String contactEmailAddress, String customerId, String header,String recordType,String parameter,String request,String domain,Date requestDate  ) {
		super();
		this.id = id;
		this.hostName = hostName;
		this.ipBlock = ipBlock;
		this.contactEmailAddress = contactEmailAddress;
		this.customerId = customerId;
		this.header = header;
		this.recordType = recordType;
		this.parameter = parameter;
		this.request = request;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getIpBlock() {
		return ipBlock;
	}

	public void setIpBlock(String ipBlock) {
		this.ipBlock = ipBlock;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean notARecord() {
		return error != null;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}	

}
