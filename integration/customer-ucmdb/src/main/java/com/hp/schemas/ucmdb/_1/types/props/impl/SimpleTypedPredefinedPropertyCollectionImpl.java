/*
 * XML Type:  SimpleTypedPredefinedPropertyCollection
 * Namespace: http://schemas.hp.com/ucmdb/1/types/props
 * Java type: com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.props.impl;
/**
 * An XML SimpleTypedPredefinedPropertyCollection(@http://schemas.hp.com/ucmdb/1/types/props).
 *
 * This is a complex type.
 */
public class SimpleTypedPredefinedPropertyCollectionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedPropertyCollection
{
    
    public SimpleTypedPredefinedPropertyCollectionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SIMPLETYPEDPREDEFINEDPROPERTY$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "SimpleTypedPredefinedProperty");
    
    
    /**
     * Gets array of all "SimpleTypedPredefinedProperty" elements
     */
    public com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty[] getSimpleTypedPredefinedPropertyArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SIMPLETYPEDPREDEFINEDPROPERTY$0, targetList);
            com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty[] result = new com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "SimpleTypedPredefinedProperty" element
     */
    public com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty getSimpleTypedPredefinedPropertyArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty)get_store().find_element_user(SIMPLETYPEDPREDEFINEDPROPERTY$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "SimpleTypedPredefinedProperty" element
     */
    public int sizeOfSimpleTypedPredefinedPropertyArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SIMPLETYPEDPREDEFINEDPROPERTY$0);
        }
    }
    
    /**
     * Sets array of all "SimpleTypedPredefinedProperty" element
     */
    public void setSimpleTypedPredefinedPropertyArray(com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty[] simpleTypedPredefinedPropertyArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(simpleTypedPredefinedPropertyArray, SIMPLETYPEDPREDEFINEDPROPERTY$0);
        }
    }
    
    /**
     * Sets ith "SimpleTypedPredefinedProperty" element
     */
    public void setSimpleTypedPredefinedPropertyArray(int i, com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty simpleTypedPredefinedProperty)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty)get_store().find_element_user(SIMPLETYPEDPREDEFINEDPROPERTY$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(simpleTypedPredefinedProperty);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "SimpleTypedPredefinedProperty" element
     */
    public com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty insertNewSimpleTypedPredefinedProperty(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty)get_store().insert_element_user(SIMPLETYPEDPREDEFINEDPROPERTY$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "SimpleTypedPredefinedProperty" element
     */
    public com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty addNewSimpleTypedPredefinedProperty()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty)get_store().add_element_user(SIMPLETYPEDPREDEFINEDPROPERTY$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "SimpleTypedPredefinedProperty" element
     */
    public void removeSimpleTypedPredefinedProperty(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SIMPLETYPEDPREDEFINEDPROPERTY$0, i);
        }
    }
}
