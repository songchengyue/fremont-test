/*
 * An XML document type.
 * Localname: getTopologyQueryResultCountByName
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getTopologyQueryResultCountByName(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetTopologyQueryResultCountByNameDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByNameDocument
{
    
    public GetTopologyQueryResultCountByNameDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETTOPOLOGYQUERYRESULTCOUNTBYNAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getTopologyQueryResultCountByName");
    
    
    /**
     * Gets the "getTopologyQueryResultCountByName" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByName getGetTopologyQueryResultCountByName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByName target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByName)get_store().find_element_user(GETTOPOLOGYQUERYRESULTCOUNTBYNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getTopologyQueryResultCountByName" element
     */
    public void setGetTopologyQueryResultCountByName(com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByName getTopologyQueryResultCountByName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByName target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByName)get_store().find_element_user(GETTOPOLOGYQUERYRESULTCOUNTBYNAME$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByName)get_store().add_element_user(GETTOPOLOGYQUERYRESULTCOUNTBYNAME$0);
            }
            target.set(getTopologyQueryResultCountByName);
        }
    }
    
    /**
     * Appends and returns a new empty "getTopologyQueryResultCountByName" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByName addNewGetTopologyQueryResultCountByName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByName target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetTopologyQueryResultCountByName)get_store().add_element_user(GETTOPOLOGYQUERYRESULTCOUNTBYNAME$0);
            return target;
        }
    }
}
