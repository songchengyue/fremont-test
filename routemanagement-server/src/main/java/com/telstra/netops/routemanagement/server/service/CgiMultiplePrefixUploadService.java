package com.telstra.netops.routemanagement.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.routemanagement.server.exception.RMApplicationCustomException;
import com.telstra.netops.routemanagement.server.model.PrefixUploadRequest;
import com.telstra.netops.routemanagement.server.model.Success;
import com.telstra.netops.routemanagement.server.model.cgi.CgiRouteRequest;
import com.telstra.netops.routemanagement.server.model.cgi.RouteResultWrapper;
import com.telstra.netops.routemanagement.server.util.JsonSerializerDeserializer;
import com.telstra.oauth2.client.OAuth2TokenResolver;

/**
 * 
 * Post multiple prefix upload IPv4 request to routemanagement-cgi module with
 * the details.
 *
 */
@Service
@Profile("!local")
public class CgiMultiplePrefixUploadService implements MultiplePrefixUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CgiMultiplePrefixUploadService.class);

	@Value("${routemanagement.cgi.url}")
	private String cgiURL;

	@Autowired
	private JsonSerializerDeserializer jsonSerializerDeserializer;

	@Autowired
	private OAuth2TokenResolver tokenResolver;

	@Override
	public Success upload(PrefixUploadRequest request) {

		CgiRouteRequest cgiRouteRequest = CgiRouteRequest.createMultiplePrefixUploadRecords(request);

		CgiRouteRequester<RouteResultWrapper> requester = new CgiRouteRequester<RouteResultWrapper>(
				RouteResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		RouteResultWrapper wrapper = requester.doRequest(cgiRouteRequest, tokenResolver);

		if (wrapper.isError()) {
			throw new RMApplicationCustomException(wrapper.getErrorMessage());
		}

		LOGGER.debug(wrapper.toString());
		return new Success(wrapper.getSuccessMessage());
	}

}