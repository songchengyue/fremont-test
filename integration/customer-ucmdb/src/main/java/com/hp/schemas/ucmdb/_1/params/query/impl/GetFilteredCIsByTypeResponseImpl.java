/*
 * XML Type:  getFilteredCIsByTypeResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML getFilteredCIsByTypeResponse(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class GetFilteredCIsByTypeResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponse
{
    
    public GetFilteredCIsByTypeResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CIS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "CIs");
    private static final javax.xml.namespace.QName CHUNKINFO$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "chunkInfo");
    
    
    /**
     * Gets the "CIs" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs getCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(CIS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CIs" element
     */
    public void setCIs(com.hp.schemas.ucmdb._1.types.CIs cIs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(CIS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(CIS$0);
            }
            target.set(cIs);
        }
    }
    
    /**
     * Appends and returns a new empty "CIs" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs addNewCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(CIS$0);
            return target;
        }
    }
    
    /**
     * Gets the "chunkInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkInfo getChunkInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().find_element_user(CHUNKINFO$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "chunkInfo" element
     */
    public void setChunkInfo(com.hp.schemas.ucmdb._1.types.ChunkInfo chunkInfo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().find_element_user(CHUNKINFO$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().add_element_user(CHUNKINFO$2);
            }
            target.set(chunkInfo);
        }
    }
    
    /**
     * Appends and returns a new empty "chunkInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.ChunkInfo addNewChunkInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ChunkInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.ChunkInfo)get_store().add_element_user(CHUNKINFO$2);
            return target;
        }
    }
}
