/*
 * An XML document type.
 * Localname: addCustomerResponse
 * Namespace: http://schemas.hp.com/ucmdb/management/1/params
 * Java type: com.hp.schemas.ucmdb.management._1.params.AddCustomerResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.management._1.params.impl;
/**
 * A document containing one addCustomerResponse(@http://schemas.hp.com/ucmdb/management/1/params) element.
 *
 * This is a complex type.
 */
public class AddCustomerResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.management._1.params.AddCustomerResponseDocument
{
    
    public AddCustomerResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDCUSTOMERRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/management/1/params", "addCustomerResponse");
    
    
    /**
     * Gets the "addCustomerResponse" element
     */
    public com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse getAddCustomerResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse)get_store().find_element_user(ADDCUSTOMERRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "addCustomerResponse" element
     */
    public void setAddCustomerResponse(com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse addCustomerResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse)get_store().find_element_user(ADDCUSTOMERRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse)get_store().add_element_user(ADDCUSTOMERRESPONSE$0);
            }
            target.set(addCustomerResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "addCustomerResponse" element
     */
    public com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse addNewAddCustomerResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.AddCustomerResponse)get_store().add_element_user(ADDCUSTOMERRESPONSE$0);
            return target;
        }
    }
}
