/*
 * XML Type:  QualifierProperties
 * Namespace: http://schemas.hp.com/ucmdb/1/types/props
 * Java type: com.hp.schemas.ucmdb._1.types.props.QualifierProperties
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.props.impl;
/**
 * An XML QualifierProperties(@http://schemas.hp.com/ucmdb/1/types/props).
 *
 * This is a complex type.
 */
public class QualifierPropertiesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.props.QualifierProperties
{
    
    public QualifierPropertiesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUALIFIERNAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "qualifierName");
    
    
    /**
     * Gets array of all "qualifierName" elements
     */
    public java.lang.String[] getQualifierNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(QUALIFIERNAME$0, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "qualifierName" element
     */
    public java.lang.String getQualifierNameArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUALIFIERNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "qualifierName" elements
     */
    public org.apache.xmlbeans.XmlString[] xgetQualifierNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(QUALIFIERNAME$0, targetList);
            org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "qualifierName" element
     */
    public org.apache.xmlbeans.XmlString xgetQualifierNameArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUALIFIERNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return (org.apache.xmlbeans.XmlString)target;
        }
    }
    
    /**
     * Returns number of "qualifierName" element
     */
    public int sizeOfQualifierNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(QUALIFIERNAME$0);
        }
    }
    
    /**
     * Sets array of all "qualifierName" element
     */
    public void setQualifierNameArray(java.lang.String[] qualifierNameArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(qualifierNameArray, QUALIFIERNAME$0);
        }
    }
    
    /**
     * Sets ith "qualifierName" element
     */
    public void setQualifierNameArray(int i, java.lang.String qualifierName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUALIFIERNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(qualifierName);
        }
    }
    
    /**
     * Sets (as xml) array of all "qualifierName" element
     */
    public void xsetQualifierNameArray(org.apache.xmlbeans.XmlString[]qualifierNameArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(qualifierNameArray, QUALIFIERNAME$0);
        }
    }
    
    /**
     * Sets (as xml) ith "qualifierName" element
     */
    public void xsetQualifierNameArray(int i, org.apache.xmlbeans.XmlString qualifierName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUALIFIERNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(qualifierName);
        }
    }
    
    /**
     * Inserts the value as the ith "qualifierName" element
     */
    public void insertQualifierName(int i, java.lang.String qualifierName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(QUALIFIERNAME$0, i);
            target.setStringValue(qualifierName);
        }
    }
    
    /**
     * Appends the value as the last "qualifierName" element
     */
    public void addQualifierName(java.lang.String qualifierName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUALIFIERNAME$0);
            target.setStringValue(qualifierName);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "qualifierName" element
     */
    public org.apache.xmlbeans.XmlString insertNewQualifierName(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(QUALIFIERNAME$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "qualifierName" element
     */
    public org.apache.xmlbeans.XmlString addNewQualifierName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(QUALIFIERNAME$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "qualifierName" element
     */
    public void removeQualifierName(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(QUALIFIERNAME$0, i);
        }
    }
}
