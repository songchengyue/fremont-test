####################################################################################################
# File        : CMS_AGGREGATES.pm
# Description : Passes parameters to CMS_AGGREGATES_DB.pm file from the GUI and call the appropiate functions for # adding,editing,listing or deleting an aggregate/switch service.
# Parameters to be passed : aggregate_serviceID,description,
# Author      : KETAKI THOMBARE (Infosys Tech. Ltd., Bangalore )
# Started On  : 09 July 2009
# Modification History :
#  Date               Name                 Change/Description
# ------------------------------------------------------------------
# 11-Aug-2009    Ketaki Popat Thombare    Baseline for EVPL
# 12-Aug-2009    Deepti Jaisoor           Added changes for add and edit aggregates for EVPL
# 15-Aug-2009    Ketaki Popat Thombare    Added changes for list and delete aggregates for EVPL 
##################################################################################################
package CMS_AGGREGATES;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_AGGREGATES_DB;

sub start {
#	print "Content-type: text/html\n\n";

	if ((not defined($CMS::html_values{command}{value})) || ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} =~ /cancel/i))) {
		# default to add_aggregate_code
		CMS::clear_html_values();
		$CMS::html_values{subsystem}{value} = "aggregates";
		$CMS::html_values{command}{value} = "add_aggregates";
	}

	if (defined $CMS::html_values{stage}{value}) {
    		unless (CMS::check_screen($CMS::html_values{current_screen}{value})){
      			$CMS::html_values{stage}{value}--;
    		}
  	}

	if($CMS::html_values{command}{value} eq "add_aggregates") {
		# Add a new aggregate service
    		if (not defined ($CMS::html_values{stage}{value})) {  
      			CMS::output_html("aggregates_add");
    		} elsif ($CMS::html_values{stage}{value} == 2) {
      			$CMS::html_values{aggregate_serviceid}{value} =~ s/^\s+|\s+$//g;
			$CMS::html_values{aggregate_serviceid}{value} =~ tr/A-Z/a-z/;
      			$CMS::html_values{description}{value} =~ s/^\s+|\s+$//g;
        
      			if($CMS::html_values{aggregate_serviceid}{value} =~ /^$/){
        			CMS::add_error("Fail to add New Aggregate/Switch Service.");
       				CMS::add_error("Service cannot be null or consist of whitespace characters only.");
        			CMS::output_html("aggregates_add");
      			} else {
        			my $res = CMS_AGGREGATES_DB::add_aggregate_service($CMS::html_values{aggregate_serviceid}{value}, $CMS::html_values{description}{value});
				if ($res eq "1") {
			        	CMS::add_error("Aggregate/Switch Service added successfully.");
				} else {
                          CMS::add_error($res);
	       			}
          			CMS::output_html("aggregates_add");
			}
		}
	} elsif ($CMS::html_values{command}{value} eq "edit_aggregates") {
    		# Edit an existing aggregate service  
    		if (not defined ($CMS::html_values{stage}{value})) {
      			# first stage
      			if(my $codes = CMS_AGGREGATES_DB::get_aggregate_serviceid) {
        			$CMS::html_values{aggregate_serviceid}{options} = $codes;  
			        CMS::output_html("aggregates_edit"); 
      			} else {  
        			CMS::add_error("No Aggregate/Switch service exist.");
        			CMS::output_html("aggregates_edit");
			}
      		} elsif ($CMS::html_values{stage}{value} == 2) {
     	 		my $result = CMS_AGGREGATES_DB::get_description($CMS::html_values{aggregate_serviceid}{value});
			if ($result ne "0") {
        			$CMS::html_values{aggregate_serviceid}{value} = $CMS::html_values{aggregate_serviceid}{value};
        			$CMS::html_values{description}{value} = $result;
       	 			CMS::output_html("aggregates_edit2");
      			} else {
       	 			# aggregate service does not exist
        			CMS::add_error("The Aggregate/Switch Service entered does not exists.");
        			CMS::output_html("aggregates_edit");
      			}
    		} elsif ($CMS::html_values{stage}{value} == 3) {
      			if(CMS_AGGREGATES_DB::update_description($CMS::html_values{aggregate_serviceid}{value}, $CMS::html_values{description}{value})) {
        			my $codes = CMS_AGGREGATES_DB::get_aggregate_serviceid;
        			$CMS::html_values{aggregate_serviceid}{options} = $codes;
        			CMS::add_error("Aggregate/Switch Service updated successfully.");
        			CMS::output_html("aggregates_edit");
      			} else {
        			CMS::add_error("Failed to edit Aggregate/Switch Service.");
        			CMS::add_error($CMS_AGGREGATES_DB::error);
        			CMS::output_html("aggregates_edit2");   
      			}
    		}  
    	}  elsif ($CMS::html_values{command}{value} eq "delete_aggregates") {
    		if (not defined ($CMS::html_values{stage}{value})) {
      			# first stage
      			if(my $codes = CMS_AGGREGATES_DB::get_aggregate_serviceid){
        			$CMS::html_values{aggregate_serviceid}{options} = $codes;
        			CMS::output_html("aggregates_delete");
      			} else {
        			CMS::add_error("No Aggregate/Switch Service exists.");
        			CMS::output_html("aggregates_delete");
      			}
    		} elsif ($CMS::html_values{stage}{value} == 2) {
			my $agg_servid = $CMS::html_values{aggregate_serviceid}{value};
			my $agg_servid_z;
			my $res_z;
                        #++EVPL Checking for EVPL switch service ID pattern and replacing 'sw' with 'asw' and 'zsw' for the same 
			if ($agg_servid =~ /.*\[.*\].*ec.*sw$/){
			$agg_servid_z = $agg_servid;
			$agg_servid_z =~ s/sw/zsw/;
			$agg_servid =~ s/sw/asw/;
			}
      			# second stage
			my $res = CMS_AGGREGATES_DB::delete_aggregate($agg_servid);
			if($agg_servid_z){
			my $res_z = CMS_AGGREGATES_DB::delete_aggregate($agg_servid_z);
			}
			if ($res eq "1" || $res_z eq "1") {
                        #--EVPL
        			CMS::add_error("Aggregate/Switch Service deleted successfully.");
        			$CMS::html_values{aggregate_serviceid}{options} = CMS_AGGREGATES_DB::get_aggregate_serviceid;
        			CMS::output_html("aggregates_delete");
      			} else {
        			#my $error = $CMS_AGGREGATES_DB::error;
        			$CMS::html_values{aggregate_serviceid}{options} = CMS_AGGREGATES_DB::get_aggregate_serviceid;
        			CMS::add_error("Deletion failed.");
        			CMS::add_error("Link record exists for the Aggregate/Switch service.");
        			CMS::output_html("aggregates_delete");
      			}
    		}
  	}  elsif ($CMS::html_values{command}{value} eq "list_aggregates") {
                # list out existing aggregate services
                if (not defined ($CMS::html_values{stage}{value})) {
      			if(my $codes = CMS_AGGREGATES_DB::get_allaggregate_serviceid) {
        			$CMS::html_values{aggregate_serviceid}{options} = $codes;
        			CMS::output_html("aggregates_list");
      			} else {
        			CMS::add_error("No Aggregate/Switch Service exists.");
        			CMS::output_html("aggregates_list");
      			}

		} elsif ($CMS::html_values{stage}{value} == 4) {
open (DEBUG,">/tmp/list_agg");
			my $agg_servid = $CMS::html_values{aggregate_serviceid}{value};
			my $agg_servid_z;
                        #++EVPL - Checking for EVPL switch service ID pattern and replacing 'sw' with 'asw' and 'zsw' for the same 
			if ($agg_servid =~ /.*\[.*\].*ec.*sw$/){
                        $agg_servid_z = $agg_servid;
                        $agg_servid_z =~ s/sw/zsw/;
                        $agg_servid =~ s/sw/asw/;
                        }
                        #--EVPL
			#$CMS::html_values{aggregates}{table} = CMS_AGGREGATES_DB::list_aggregateservice($agg_servid);
		 	my $res = CMS_AGGREGATES_DB::list_aggregateservice($agg_servid);	
			my $count= $#$res+1;
 print DEBUG "count:$count\n";
		        for(my $i=0;$i<$count;$i++){
			my $aggserv=$$res[$i][0];
                        my $servid=$$res[$i][1];
print DEBUG "aggserv:$aggserv\n";
                        #++EVPL- Checking for VLAN/Transparent switch sercvice ID pattern
                        #For vlan switch service (consisiting of 'evp' and 'sw' tariling a and z are removed and for Transparebt trailing asw and zsw are removed and sw is concatenated back 
			if (($aggserv =~ /.*\[.*\].*ec.*sw$/) || ($aggserv =~ /.*evp.*sw$/)){
	print DEBUG "INSIDE IF\n";
			     #for(my $i=0;$i<$count;$i++){	
print DEBUG "INSIDE LOOP\n";
				$$res[$i][0]=~ s/asw/sw/;
				$$res[$i][0]=~ s/zsw/sw/;
				$$res[$i][1]=~ s/a$//;
				$$res[$i][1] =~ s/z$//; 
			     #}
			}
			}
	                #--EVPL	
			print DEBUG "count----->$count\n";
			$CMS::html_values{aggregates}{table} = $res;
			unshift(@{$CMS::html_values{aggregates}{table}}, ["Service","Service ID","Acc No.","Description"]);

			$CMS::html_values{aggregates}{header_rows} = 1;
 print DEBUG "HEADER:$CMS::html_values{aggregates}{header_rows}\n";
      			my $codes = CMS_AGGREGATES_DB::get_allaggregate_serviceid;
        		$CMS::html_values{aggregate_serviceid}{options} = $codes;

			CMS::output_html("aggregates_list2");
		}
	}




}

1
