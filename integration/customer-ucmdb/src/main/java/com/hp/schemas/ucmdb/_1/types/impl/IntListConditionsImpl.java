/*
 * XML Type:  IntListConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntListConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML IntListConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IntListConditionsImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.IntListConditions
{
    
    public IntListConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INTLISTCONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intListCondition");
    
    
    /**
     * Gets array of all "intListCondition" elements
     */
    public com.hp.schemas.ucmdb._1.types.IntListCondition[] getIntListConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INTLISTCONDITION$0, targetList);
            com.hp.schemas.ucmdb._1.types.IntListCondition[] result = new com.hp.schemas.ucmdb._1.types.IntListCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "intListCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListCondition getIntListConditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListCondition)get_store().find_element_user(INTLISTCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "intListCondition" element
     */
    public int sizeOfIntListConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTLISTCONDITION$0);
        }
    }
    
    /**
     * Sets array of all "intListCondition" element
     */
    public void setIntListConditionArray(com.hp.schemas.ucmdb._1.types.IntListCondition[] intListConditionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(intListConditionArray, INTLISTCONDITION$0);
        }
    }
    
    /**
     * Sets ith "intListCondition" element
     */
    public void setIntListConditionArray(int i, com.hp.schemas.ucmdb._1.types.IntListCondition intListCondition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListCondition)get_store().find_element_user(INTLISTCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(intListCondition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "intListCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListCondition insertNewIntListCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListCondition)get_store().insert_element_user(INTLISTCONDITION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "intListCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListCondition addNewIntListCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListCondition)get_store().add_element_user(INTLISTCONDITION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "intListCondition" element
     */
    public void removeIntListCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTLISTCONDITION$0, i);
        }
    }
}
