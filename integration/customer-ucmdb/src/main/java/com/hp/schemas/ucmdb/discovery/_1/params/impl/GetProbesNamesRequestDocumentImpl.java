/*
 * An XML document type.
 * Localname: getProbesNamesRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getProbesNamesRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetProbesNamesRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequestDocument
{
    
    public GetProbesNamesRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETPROBESNAMESREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbesNamesRequest");
    
    
    /**
     * Gets the "getProbesNamesRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequest getGetProbesNamesRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequest)get_store().find_element_user(GETPROBESNAMESREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getProbesNamesRequest" element
     */
    public void setGetProbesNamesRequest(com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequest getProbesNamesRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequest)get_store().find_element_user(GETPROBESNAMESREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequest)get_store().add_element_user(GETPROBESNAMESREQUEST$0);
            }
            target.set(getProbesNamesRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "getProbesNamesRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequest addNewGetProbesNamesRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesRequest)get_store().add_element_user(GETPROBESNAMESREQUEST$0);
            return target;
        }
    }
}
