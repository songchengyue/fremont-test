package com.telstra.netops.dns.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason="There was an internal application error")
public class DnsApplicationInternalException extends RuntimeException {
	
	public DnsApplicationInternalException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public DnsApplicationInternalException(String message) {
		super(message);
	}

}
