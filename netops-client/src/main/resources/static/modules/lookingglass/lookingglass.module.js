(function(){
angular.module('netops.lookingGlassModule', ['ngRoute', 'netops.utilModule']);

angular.module('netops.lookingGlassModule')
    
.config(function (moduleRegistryProvider) {
	moduleRegistryProvider.register({ name: 'lookingglass', baseUrl: '/lookingglass-server-1.0/'});	
})

  .controller('NodeTestController', function($http, $scope,$rootScope, $routeParams, ERROR_TEXT) {
     $scope.name = "NodeTestController";
     $scope.params = $routeParams;
    // $scope.pops ;
    // $scope.result;
     //$scope.error;

     $scope.resetPage =function(){
	console.log("in reset function");
	  $scope.error=false;
      $scope.table=false;
      
	}
     
     $scope.loadIPv4 = function(){ 
    	 
    	 $scope.source4=null;
         $scope.target4=null;
         $scope.numPings=null;
         $scope.pingItems = [{
        	  id: 5,
        	  label: '5',
        	}, {
        	  id: 10,
        	  label: '10',
        	}, {
        	  id: 15,
        	  label: '15',
        	}, {
        	  id: 20,
        	  label: '20',
        	}];
 
     $http({
    	  method	: 'GET',
    	  url		: '/lookingglass-server-1.0/nodes?ipv=IPv4'
    	}).then(function (response) {
    	    // this callback will be called asynchronously
    	    // when the response is available
    	    console.log("response "+response.data);
       		$scope.nodes = response.data;
    		
    	  }, function (response) {
    		  //unhandled exception
    		// called asynchronously if an error occurs
      	    // or server returns response with an error status.
    		//in this case redirected to error page
    		$scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
    	  });
  	}
     $scope.loadIPv6 = function(){ 
    	 
         $scope.source6=null;
         $scope.target6=null;
         $scope.numPings=null;
         $scope.pingItems = [{
       	  id: 5,
       	  label: '5',
       		}, {
       	  id: 10,
       	  label: '10',
       		}, {
       	  id: 15,
       	  label: '15',
       		}, {
       	  id: 20,
       	  label: '20',
       		}];
	
     $http({
    	  method	: 'GET',
    	  url		: '/lookingglass-server-1.0/nodes?ipv=IPv6'
    	}).then(function (response) {
    	    // this callback will be called asynchronously
    	    // when the response is available
    		console.log(response.data);
    		console.log(response.status);
       		$scope.nodes = response.data;   		

			},
			function(response) {
    		// called asynchronously if an error occurs
           	// or server returns response with an error status.
		    $scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
    	  });
  	}
     		
   
     $scope.submitIPv4Form = function(){
    	 $scope.ipv4disable=true;
    	 var sourceNode=$scope.source4;
    	 var targetNode=$scope.target4;
    	 var numPings=$scope.numPings
    	 $scope.error=false;
    	 $scope.table=false;
    	 console.log("source "+sourceNode+ " Target "+targetNode);
    	  //http call to backend
    	 $http({
       	   method	: 'GET',
       	   url		: '/lookingglass-server-1.0/ping?snode='+sourceNode.id+'&dnode='+targetNode.id+'&ipv=IPv4'+'&numpings='+numPings.id
       	
       	}).then(function (response) {
       	    // this callback will be called asynchronously
       	    // when the response is available
			$scope.ipv4disable=false;
       		$rootScope.loading=false;
			console.log(response.data);
       		$scope.table=response.data;
       	  }, function (response) {
       		$scope.ipv4disable=false;
       		// called asynchronously if an error occurs
         	// or server returns response with an error status.
       		console.log("An error encountered " + response.status);
       		$scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
       	  });
     }
     $scope.submitIPv6Form = function(){
    	 var sourceNode=$scope.source6;
    	 var targetNode=$scope.target6;
    	 var numPings=$scope.numPings
    	 $scope.ipv6disable=true;
    	 $scope.error=false;
    	 $scope.table=false;
	  //http call to backend
     var pings = 10
     if (numPings) {
    	 pings = numPings.id
     }
	 $http({
   	   method	: 'GET',
   	   url		: '/lookingglass-server-1.0/ping?snode='+sourceNode.id+'&dnode='+targetNode.id+'&ipv=IPv6'+'&numpings='+pings
   	
   	}).then(function (response) {
   	    // this callback will be called asynchronously
   	    // when the response is available
		$scope.ipv6disable=false;
   		$scope.table=response.data;
   	  }, function (response) {
   		// called asynchronously if an error occurs
     	    // or server returns response with an error status.
     	$scope.ipv6disable=false;
   		// called asynchronously if an error occurs
     	// or server returns response with an error status.
   		console.log("An error encountered " + response.status);
   		$scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
   	  });
     }
 })
 .controller('NodeHostTestController', function($http, $scope, $routeParams, ERROR_TEXT) {
	   $scope.name = "NodeTestController";
	   $scope.params = $routeParams;
	   $scope.pops ;
	   $scope.result;
	   $scope.error;

	$scope.resetPage =function(){
	console.log("in reset function");
	  $scope.error=false;
      $scope.table=false;
	}

	$scope.pristine=function(){
	  $scope.IPv4form.host4.$setPristine(true);
	  $scope.IPv6form.host6.$setPristine(true);
	}

	   
	   $scope.loadIPv4 = function(){ 
		   
		   $scope.source4=null;
		   $scope.target4="";
		   
		     $http({
		    	  method	: 'GET',
		    	  url		: '/lookingglass-server-1.0/nodes?ipv=IPv4'
		    	}).then(function (response) {
		    	    // this callback will be called asynchronously
		    	    // when the response is available

		    		$scope.nodes = response.data;
		    		
		    	  }, function (response) {
		    		  //unhandled exception
		    		// called asynchronously if an error occurs
		      	    // or server returns response with an error status.
		    		//in this case redirected to error page
		    		$scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
		    	  });
		  	}
	 $scope.loadIPv6 = function(){ 

		 	$scope.source6=null;
			$scope.target6="";

		     $http({
		    	  method	: 'GET',
		    	  url		: '/lookingglass-server-1.0/nodes?ipv=IPv6'
		    	}).then(function (response) {
		    	    // this callback will be called asynchronously
		    	    // when the response is available

		    		$scope.nodes = response.data;
		    		console.log($scope.nodes);
		    		
		    	  }, function (response) {
		    		// called asynchronously if an error occurs
		      	    // or server returns response with an error status.
		    		//in this case redirected to error page
		    		  $scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
		    	  });
		  	}
		     		
		   
		     $scope.submitIPv4Form = function(){
		    	 var sourceNode=$scope.source4;
		    	 var targetNode=$scope.target4;
			 $scope.ipv4disable=true;
		    	 $scope.error=false;
		    	 $scope.table=false;
		    	  //http call to backend
		    	 $http({
		       	   method	: 'GET',
		       	   url		: '/lookingglass-server-1.0/node?snode='+sourceNode.id+'&dnode='+targetNode+'&ipv=IPv4'
		       	
		       	}).then(function (response) {
		       	    // this callback will be called asynchronously
		       	    // when the response is available

					$scope.ipv4disable=false;
		       		$scope.table=response.data;
		       	  }, function (response) {
		       		// called asynchronously if an error occurs
		         	    // or server returns response with an error status.
					$scope.ipv4disable=false;
					$scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
		       	  });
		     }
		     $scope.submitIPv6Form = function(){
		    	 var sourceNode=$scope.source6;
		    	 var targetNode=$scope.target6;
			 $scope.ipv6disable=true;
		    	 $scope.error=false;
		    	 $scope.table=false;
			  //http call to backend
			 $http({
		   	   method	: 'GET',
		   	   url		: '/lookingglass-server-1.0/node?snode='+sourceNode.id+'&dnode='+targetNode+'&ipv=IPv6'
		   	
		   	}).then(function (response) {
		   	    // this callback will be called asynchronously
		   	    // when the response is available

				$scope.ipv6disable=false;
		   		$scope.table=response.data;
		   		
		   	  }, function (response) {
		   		// called asynchronously if an error occurs
		     	    // or server returns response with an error status.
				$scope.ipv6disable=false;
				$scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
		   	  });
		     }
 })
 .controller('NodeHostTraceController', function($http, $scope, $routeParams, ERROR_TEXT) {
	   $scope.name = "NodeTestController";
	   $scope.params = $routeParams;
	   $scope.pops ;
	   $scope.result;
	   $scope.error;

	 $scope.resetPage =function(){
	console.log("in reset function");
	  $scope.error=false;
    	  $scope.result=false;
	}
	
	$scope.pristine=function(){
	  $scope.IPv4form.host4.$setPristine(true);
	  $scope.IPv6form.host6.$setPristine(true);
	}

	   
	   $scope.loadIPv4 = function(){
		   
		   $scope.source4=null;
		   $scope.target4="";
		     $http({
		    	  method	: 'GET',
		    	  url		: '/lookingglass-server-1.0/nodes?ipv=IPv4'
		    	}).then(function (response) {
		    	    // this callback will be called asynchronously
		    	    // when the response is available

		       		$scope.nodes = response.data;
		    		
		    	  }, function (response) {
		    		  //unhandled exception
		    		// called asynchronously if an error occurs
		      	    // or server returns response with an error status.
		    		//in this case redirected to error page
		    		  $scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
		    	  });
		  	}
		$scope.loadIPv6 = function(){ 
			
			$scope.source6=null;
			$scope.target6="";
			
		     $http({
		    	  method	: 'GET',
		    	  url		: '/lookingglass-server-1.0/nodes?ipv=IPv6'
		    	}).then(function (response) {
		    	    // this callback will be called asynchronously
		    	    // when the response is available
		       		$scope.nodes = response.data;
		    	  }, function (response) {
		    		// called asynchronously if an error occurs
		      	    // or server returns response with an error status.
		    		//in this case redirected to error page
		    		  $scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
		    	  });
		  	}
		     		
		   
		     $scope.submitIPv4Form = function(){
		    	 $scope.error=false;
		    	 $scope.result=false;
			 $scope.ipv4disable=true;
		    	 var sourceNode=$scope.source4;
		    	 var targetNode=$scope.target4;
		    	 
		    	  //http call to backend
		    	 $http({
		       	   method	: 'GET',
		       	   url		: '/lookingglass-server-1.0/trace?snode='+sourceNode.id+'&dnode='+targetNode+'&ipv=IPv4'
		       	
		       	}).then(function (response) {
		       	    // this callback will be called asynchronously
		       	    // when the response is available
					$scope.ipv4disable=false;
		       			var trace=response.data;
						var res = [];
						var key =Object.keys(trace);
						key.sort(function(a, b){return a-b});						

						for (i = 0; i < key.length; i++) {
								var val = key[i];
							res.push(trace[val]);
							
						}
						$scope.result=res;
		       		
		       	  }, function (response) {
		       		// called asynchronously if an error occurs
		         	    // or server returns response with an error status.
					$scope.ipv4disable=false;
					$scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
		       	  });
		     }
		     $scope.submitIPv6Form = function(){
		    	 $scope.error=false;
		    	 $scope.result=false;
			 $scope.ipv6disable=true;
		    	 var sourceNode=$scope.source6;
		    	 var targetNode=$scope.target6;
		    	 
			  //http call to backend
			 $http({
		   	   method	: 'GET',
		   	   url		: '/lookingglass-server-1.0/trace?snode='+sourceNode.id+'&dnode='+targetNode+'&ipv=IPv6'
		   	
		   	}).then(function (response) {
		   	    // this callback will be called asynchronously
		   	    // when the response is available
				$scope.ipv6disable=false;		   			
		   			var trace=response.data;
					var res = [];
					var key =Object.keys(trace);
					key.sort(function(a, b){return a-b});		

					for (i = 0; i < key.length; i++) {
							var val = key[i];
						res.push(trace[val]);
						
					}
					$scope.result=res;
		   		
		   	  }, function (response) {
		   		// called asynchronously if an error occurs
		     	    // or server returns response with an error status.
				$scope.ipv6disable=false;
				$scope.error = response.errorText ? response.errorText : ERROR_TEXT.SERVER;
		   	  });
		     }
 })
 
  .config(function($routeProvider, $locationProvider) {
  $routeProvider
  .when('/LG_NodeTest', {
	    templateUrl: 'modules/lookingglass/templates/NodeTest.html',
	    controller: 'NodeTestController'
	  })
	  .when('/LG_NodeHostTest', {
	    templateUrl: 'modules/lookingglass/templates/NodeHostTest.html',
	    controller: 'NodeHostTestController'
	  })
	  .when('/LG_NodeHostTrace', {
	    templateUrl: 'modules/lookingglass/templates/NodeHostTrace.html',
	    controller: 'NodeHostTraceController'
	  })
	  .when('/LG_LookingGlass', {
		templateUrl: 'modules/lookingglass/templates/LookingGlass.html'  
	  })
 });

})();