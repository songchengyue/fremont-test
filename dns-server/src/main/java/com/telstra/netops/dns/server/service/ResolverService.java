package com.telstra.netops.dns.server.service;

import java.util.List;

import com.telstra.netops.dns.server.model.ClientDnsResolverRequest;
import com.telstra.netops.dns.server.model.DnsResolverRecord;
import com.telstra.netops.dns.server.model.Success;

public interface ResolverService {

	Success add(ClientDnsResolverRequest request);

	List<DnsResolverRecord> list(String customerId);

	Success remove(ClientDnsResolverRequest request);

}
