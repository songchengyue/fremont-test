/*
 * XML Type:  IntListProp
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntListProp
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML IntListProp(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IntListPropImpl extends com.hp.schemas.ucmdb._1.types.impl.CIPropImpl implements com.hp.schemas.ucmdb._1.types.IntListProp
{
    
    public IntListPropImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INTVALUES$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intValues");
    
    
    /**
     * Gets the "intValues" element
     */
    public com.hp.schemas.ucmdb._1.types.IntList getIntValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntList target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntList)get_store().find_element_user(INTVALUES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "intValues" element
     */
    public boolean isNilIntValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntList target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntList)get_store().find_element_user(INTVALUES$0, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "intValues" element
     */
    public boolean isSetIntValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTVALUES$0) != 0;
        }
    }
    
    /**
     * Sets the "intValues" element
     */
    public void setIntValues(com.hp.schemas.ucmdb._1.types.IntList intValues)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntList target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntList)get_store().find_element_user(INTVALUES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IntList)get_store().add_element_user(INTVALUES$0);
            }
            target.set(intValues);
        }
    }
    
    /**
     * Appends and returns a new empty "intValues" element
     */
    public com.hp.schemas.ucmdb._1.types.IntList addNewIntValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntList target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntList)get_store().add_element_user(INTVALUES$0);
            return target;
        }
    }
    
    /**
     * Nils the "intValues" element
     */
    public void setNilIntValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntList target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntList)get_store().find_element_user(INTVALUES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.IntList)get_store().add_element_user(INTVALUES$0);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "intValues" element
     */
    public void unsetIntValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTVALUES$0, 0);
        }
    }
}
