/*
 * XML Type:  IntListCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntListCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types;


/**
 * An XML IntListCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public interface IntListCondition extends com.hp.schemas.ucmdb._1.types.Condition
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IntListCondition.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("intlistcondition21batype");
    
    /**
     * Gets the "condition" element
     */
    com.hp.schemas.ucmdb._1.types.IntListProp getCondition();
    
    /**
     * Sets the "condition" element
     */
    void setCondition(com.hp.schemas.ucmdb._1.types.IntListProp condition);
    
    /**
     * Appends and returns a new empty "condition" element
     */
    com.hp.schemas.ucmdb._1.types.IntListProp addNewCondition();
    
    /**
     * Gets the "intListOperator" element
     */
    com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator.Enum getIntListOperator();
    
    /**
     * Gets (as xml) the "intListOperator" element
     */
    com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator xgetIntListOperator();
    
    /**
     * Sets the "intListOperator" element
     */
    void setIntListOperator(com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator.Enum intListOperator);
    
    /**
     * Sets (as xml) the "intListOperator" element
     */
    void xsetIntListOperator(com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator intListOperator);
    
    /**
     * An XML intListOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.IntListCondition$IntListOperator.
     */
    public interface IntListOperator extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IntListOperator.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("intlistoperator2bbdelemtype");
        
        org.apache.xmlbeans.StringEnumAbstractBase enumValue();
        void set(org.apache.xmlbeans.StringEnumAbstractBase e);
        
        static final Enum IN_LIST = Enum.forString("InList");
        static final Enum IS_NULL = Enum.forString("IsNull");
        
        static final int INT_IN_LIST = Enum.INT_IN_LIST;
        static final int INT_IS_NULL = Enum.INT_IS_NULL;
        
        /**
         * Enumeration value class for com.hp.schemas.ucmdb._1.types.IntListCondition$IntListOperator.
         * These enum values can be used as follows:
         * <pre>
         * enum.toString(); // returns the string value of the enum
         * enum.intValue(); // returns an int value, useful for switches
         * // e.g., case Enum.INT_IN_LIST
         * Enum.forString(s); // returns the enum value for a string
         * Enum.forInt(i); // returns the enum value for an int
         * </pre>
         * Enumeration objects are immutable singleton objects that
         * can be compared using == object equality. They have no
         * public constructor. See the constants defined within this
         * class for all the valid values.
         */
        static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
        {
            /**
             * Returns the enum value for a string, or null if none.
             */
            public static Enum forString(java.lang.String s)
                { return (Enum)table.forString(s); }
            /**
             * Returns the enum value corresponding to an int, or null if none.
             */
            public static Enum forInt(int i)
                { return (Enum)table.forInt(i); }
            
            private Enum(java.lang.String s, int i)
                { super(s, i); }
            
            static final int INT_IN_LIST = 1;
            static final int INT_IS_NULL = 2;
            
            public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
                new org.apache.xmlbeans.StringEnumAbstractBase.Table
            (
                new Enum[]
                {
                    new Enum("InList", INT_IN_LIST),
                    new Enum("IsNull", INT_IS_NULL),
                }
            );
            private static final long serialVersionUID = 1L;
            private java.lang.Object readResolve() { return forInt(intValue()); } 
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator newValue(java.lang.Object obj) {
              return (com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator) type.newValue( obj ); }
            
            public static com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator newInstance() {
              return (com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.hp.schemas.ucmdb._1.types.IntListCondition.IntListOperator) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.types.IntListCondition newInstance() {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.IntListCondition parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.IntListCondition) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
