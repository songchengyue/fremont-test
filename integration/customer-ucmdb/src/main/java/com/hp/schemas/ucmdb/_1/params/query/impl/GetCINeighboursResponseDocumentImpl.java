/*
 * An XML document type.
 * Localname: getCINeighboursResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getCINeighboursResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetCINeighboursResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponseDocument
{
    
    public GetCINeighboursResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCINEIGHBOURSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getCINeighboursResponse");
    
    
    /**
     * Gets the "getCINeighboursResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse getGetCINeighboursResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse)get_store().find_element_user(GETCINEIGHBOURSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCINeighboursResponse" element
     */
    public void setGetCINeighboursResponse(com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse getCINeighboursResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse)get_store().find_element_user(GETCINEIGHBOURSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse)get_store().add_element_user(GETCINEIGHBOURSRESPONSE$0);
            }
            target.set(getCINeighboursResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getCINeighboursResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse addNewGetCINeighboursResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCINeighboursResponse)get_store().add_element_user(GETCINEIGHBOURSRESPONSE$0);
            return target;
        }
    }
}
