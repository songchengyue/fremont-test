package com.telstra.netops.routemanagement.server.model.cgi;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Wrapper class to wrap success and error messages
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteResultWrapper {

	@JsonProperty
	private List<PrefixResult> result;

	@JsonIgnore
	public boolean isError() {
		return getErrorMessage() != null;
	}

	@JsonIgnore
	public String getErrorMessage() {
		return result.get(0).getErrorMessage();
	}

	public void setStatusMessage(String successMessage) {
		if (result == null) {
			result = new ArrayList<PrefixResult>();
		}
		PrefixResult singlePrefixResult = new PrefixResult();
		singlePrefixResult.setSuccessMessage(successMessage);
		result.add(singlePrefixResult);

	}

	public void setErrorMessage(String errorMessage) {
		if (result == null) {
			result = new ArrayList<PrefixResult>();
		}
		PrefixResult prefixResult = new PrefixResult();
		prefixResult.setErrorMessage(errorMessage);
		result.add(prefixResult);

	}

	public String getSuccessMessage() {
		return result.get(0).getSuccessMessage();
	}

}
