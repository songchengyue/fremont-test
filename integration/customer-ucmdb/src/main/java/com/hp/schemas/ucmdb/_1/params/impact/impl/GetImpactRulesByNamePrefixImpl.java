/*
 * XML Type:  getImpactRulesByNamePrefix
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * An XML getImpactRulesByNamePrefix(@http://schemas.hp.com/ucmdb/1/params/impact).
 *
 * This is a complex type.
 */
public class GetImpactRulesByNamePrefixImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix
{
    
    public GetImpactRulesByNamePrefixImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "cmdbContext");
    private static final javax.xml.namespace.QName RULENAMEPREFIXFILTER$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "ruleNamePrefixFilter");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets array of all "ruleNamePrefixFilter" elements
     */
    public java.lang.String[] getRuleNamePrefixFilterArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(RULENAMEPREFIXFILTER$2, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "ruleNamePrefixFilter" element
     */
    public java.lang.String getRuleNamePrefixFilterArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RULENAMEPREFIXFILTER$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "ruleNamePrefixFilter" elements
     */
    public org.apache.xmlbeans.XmlString[] xgetRuleNamePrefixFilterArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(RULENAMEPREFIXFILTER$2, targetList);
            org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "ruleNamePrefixFilter" element
     */
    public org.apache.xmlbeans.XmlString xgetRuleNamePrefixFilterArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RULENAMEPREFIXFILTER$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return (org.apache.xmlbeans.XmlString)target;
        }
    }
    
    /**
     * Returns number of "ruleNamePrefixFilter" element
     */
    public int sizeOfRuleNamePrefixFilterArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RULENAMEPREFIXFILTER$2);
        }
    }
    
    /**
     * Sets array of all "ruleNamePrefixFilter" element
     */
    public void setRuleNamePrefixFilterArray(java.lang.String[] ruleNamePrefixFilterArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(ruleNamePrefixFilterArray, RULENAMEPREFIXFILTER$2);
        }
    }
    
    /**
     * Sets ith "ruleNamePrefixFilter" element
     */
    public void setRuleNamePrefixFilterArray(int i, java.lang.String ruleNamePrefixFilter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RULENAMEPREFIXFILTER$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(ruleNamePrefixFilter);
        }
    }
    
    /**
     * Sets (as xml) array of all "ruleNamePrefixFilter" element
     */
    public void xsetRuleNamePrefixFilterArray(org.apache.xmlbeans.XmlString[]ruleNamePrefixFilterArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(ruleNamePrefixFilterArray, RULENAMEPREFIXFILTER$2);
        }
    }
    
    /**
     * Sets (as xml) ith "ruleNamePrefixFilter" element
     */
    public void xsetRuleNamePrefixFilterArray(int i, org.apache.xmlbeans.XmlString ruleNamePrefixFilter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(RULENAMEPREFIXFILTER$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(ruleNamePrefixFilter);
        }
    }
    
    /**
     * Inserts the value as the ith "ruleNamePrefixFilter" element
     */
    public void insertRuleNamePrefixFilter(int i, java.lang.String ruleNamePrefixFilter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(RULENAMEPREFIXFILTER$2, i);
            target.setStringValue(ruleNamePrefixFilter);
        }
    }
    
    /**
     * Appends the value as the last "ruleNamePrefixFilter" element
     */
    public void addRuleNamePrefixFilter(java.lang.String ruleNamePrefixFilter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RULENAMEPREFIXFILTER$2);
            target.setStringValue(ruleNamePrefixFilter);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ruleNamePrefixFilter" element
     */
    public org.apache.xmlbeans.XmlString insertNewRuleNamePrefixFilter(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(RULENAMEPREFIXFILTER$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ruleNamePrefixFilter" element
     */
    public org.apache.xmlbeans.XmlString addNewRuleNamePrefixFilter()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(RULENAMEPREFIXFILTER$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "ruleNamePrefixFilter" element
     */
    public void removeRuleNamePrefixFilter(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RULENAMEPREFIXFILTER$2, i);
        }
    }
}
