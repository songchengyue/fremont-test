#!/usr/local/bin/perl

#
# Written by: Richard Chew, rchew@telstra.net
# TGW Custdata Looking Glass script
#
# $Id: tgwciss-lg.cgi,v 1.6 2016/07/14 14:19:07 d804704 Exp $
# 

no warnings;
#  use DB_File;
use lib './usr/local/www/www/gia-reach/tgwcustdata/control';
use Log::Log4perl;
#use Log::Dispatch::FileRotate;
$| = 1;
use Cwd;
use Data::Dumper;
use Net::Telnet;
use Net::Telnet::Cisco;
require "ctime.pl";
use Time::Local;
use Socket;
use POSIX ":sys_wait_h";
use OS_PARA;


my $MB = 1024 * 1024;
my $user = "natm001";
my $passwd = "Reachatm";

#============================================================================================
#our $hostname = $OS_PARA::values{inthostname}{value};  #for deletion
#chop($hostname);
#$hostname = ($hostname =~ /reachdev.telstra.net/) ? "reachdev.telstra.net" : $hostname;
#=============================================================================================

# First determine which lib file to use
        #require "/usr/local/www/data/gia-reach/tgwcustdata/control/tgwcisslib.pl";
        #require "/usr/local/www/www/gia-reach/tgwcustdata/control/tgwcisslib.pl";
	require "$OS_PARA::values{libfile}{value}";

#=============================================================================================	
#our $urlname = $OS_PARA::values{inthostname}{value};
# Referrer re-write URLNAME
#if($ENV{HTTP_HOST} =~ /myservices.telstra-global.com|pccw-services.pccw.com|134.159.2.123/) {
#        $urlname = $ENV{HTTP_HOST};
#        $rmask = 1; #used for remasking as DocRoot is reseller-telstra/reseller-pccw
#}
#==============================================================================================

use JSON::PP;
use CGI;
use File::Basename;
use CGI::Carp qw ( fatalsToBrowser );
use OAuth2Token;

my $req = new CGI;
my $input = decode_json( $req->param('POSTDATA') );

my $code    		=$input->{'code'};
my $customerId    	=$input->{'customerId'};
my $level			=$input->{'level'};
my $et	 			=$input->{'et'};
my $pop	 			=$input->{'pop'};
my $toHost			=$input->{'toHost'};
my $submit			=$input->{'submit'};

#Logger Initialisation
my $log_conf =$OS_PARA::values{logfile}{value}; 
Log::Log4perl::init($log_conf);
our $logger = Log::Log4perl->get_logger();

## Definitions ##

our %formels;
#our $custfrom = &parse_form_data(*formels);
our $currtime = time;

$formels{'code'} 		= $code;
$formels{'customerId'} 	= $customerId;
$formels{'level'}		= $level;
$formels{'POP0'}		= $pop; #from host
$formels{'POP1'}		= $toHost; #to host
$formels{'DEST'}		= $toHost;
$formels{'submit'}		= $submit;

sub oauth2_error($) {
    my ($message) = @_;
    print "Status: 401 Unauthorized\n";
    print "Content-type: application/json\n\n";
    my %error = ('error' => $message);
    my $json = encode_json \%error;
    print $json;
    $logger->error($message);
}

# validate token
my $token = $req->http('OAuth2-Bearer-Token');
if (!$token) {
    oauth2_error("Missing HTTP header \"OAuth2-Bearer-Token\"");
    exit;
}

my $invalid = OAuth2Token::invalid($token);
if ($invalid) {
    oauth2_error("Invalid token $token: $invalid");
    exit;
} else {
    $logger->debug("OAuth2 token check successful");
}

# first validate user
open(my $LGDBG,'>', "/tmp/lookinggl");
print LGDBG "code val user: $formels{'POP0'}";
$logger->debug("code val user: $formels{'POP0'}");


our $result = $formels{'customerId'};
print LGDBG "L $result: result vfg: $vfg  $upass: upass\n"; 
$logger->debug("L $result: result vfg: $vfg  $upass: upass\n");
if($result =~ /FAIL/) {
	&print_error("Invalid Customer: $result");
}

# Set some meaningful vars
our $uid = $result;
our $userid = $uid;
our $oenc = &encrypt($uid,$upass,$currtime);
our ($accno, $staffid) = &ex_staffid($uid);  
our $logo_accno = $accno;

our $tmplroot = $tgwr."control/";

&log_entry($custlogfile, "LG", "$uid");     

#======================================
# now get template path
#our $tmplp = &get_template($accno);
#======================================

$tmplroot .= "testpath";

#single sign-on start - Maettr 20/10/2003
our $tdate = gmtime(time);
$tdate .= " GMT";
#================================================================
#our $acctype = CMS_OLSS::get_accountattribute_for_accno($accno);
#our $mrwaccno;
#if($acctype =~ /^RO$/) {
#        $mrwaccno = CMS_OLSS::get_mrwaccno_for_ro_acc($accno);
#        $mrwaccno = (!$mrwaccno) ? $acctype : $mrwaccno;
#}
#=================================================================

our $encet = $formels{et};
#our $etproduct = &dcrypt($formels{et});
print LGDBG "etproduct:$etproduct\n";
$logger->debug("etproduct:$etproduct\n");
my $ac = ($mrwaccno) ? $mrwaccno : $accno;

#=================================================================
#if(CMS_OLSS::verify_accno_product($ac, $etproduct) =~ /false/i) {
#        &print_error("Account does not have such product.");
#}
#================================================================= 

my $letproduct = lc($etproduct);
$tmplroot .= "/" . $letproduct;
# complete set up of variables for single sign-on

our $ctrlname = "looking_glass";


SWITCH: {

	# sub-levels
	$level = $formels{'level'};
        ($formels{'level'} == 41) && do {
			&local_trace($oenc, $userid,$formels{'POP0'}, $formels{'POP1'}, "Node", $tmplroot, $level);
			
                last SWITCH;
        };
        ($formels{'level'} == 21) && do {
			&local_trace($oenc, $userid,$formels{'POP0'}, $formels{'DEST'}, "PING", $tmplroot, $level); #DEST is destination ie $toHost
		
		last SWITCH;
	};
	($formels{'level'} == 31) && do {
			&local_trace($oenc, $userid,$formels{'POP0'}, $formels{'DEST'}, "TRACE", $tmplroot, $level);

		last SWITCH;
	};
		

	&print_error("Level unrecognised");
	exit;
};

close (DEBUG);
# For LG specific routines

### Proc ###

sub local_trace {
	print LGDBG "Inside local trace\n";
	$logger->debug("Inside local trace\n");
	my($code, $uid, $pop, $target, $action, $tmplroot, $level) = @_;  #action = node 4 NODE TO HOST TEST
	my $vals = &screen_trace($code, $uid, $pop, $target, $action, $tmplroot, $level) ;

		if($vals =~ /error/i) {
					my $errmsg= $vals;
					&print_error($errmsg);

				}
}			

####################
#CRQ000000004261
####################


sub screen_trace {
	my($code, $uid, $pop, $target, $action, $tmplroot) = @_;   
	
	my $ipv;
	my $errmsg="Error! " ;
	
	my $rpop = $pop;
	$logger->debug("Inside sceen trace. Before get router\n");
	my $routername = &get_router_of_customer($pop);
	my $host = $routername;
	
	if($host !~ /\.$/) {
        	#$host .= ".net.reach.com";
                $host .= ".telstraglobal.net";
	}
	# Now get target - either host or Reach node
	# See if action is node - then target is a reach node
	my $tip = "";
	my $origtarget = $target;
	
	
	if($action =~ /node/i) {
		if($target eq "") {
			return $errmsg."Cannot get Target Node.";
		}
		$origtarget = $target;
		if($pop eq "") {
			return $errmsg."Cannot get Host Node.";
		}
		else {
			#$host = "$loop_back{$pop}";		
		}

	
	}
	#####################IPv6 check and IPv4/6 confirmation##############
	if($target=~ m/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/){
		if($target=~ m/^(((?=(?>.*?::)(?!.*::)))(::)?(([0-9A-F]{1,4})::?){0,5}|((?5):){6})(\2((?5)(::?|$)){0,2}|((25[0-5]|(2[0-4]|1[0-9]|[1-9])?[0-9])(\.|$)){4}|(?5):(?5))()(?<![^:]:)(?<!\.)\z/i){
			if($target !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1.3}$/) {
				$logger->debug("IPv6 address $target\n");
				$ipv=6;
				$tip = $target; 
				#eval {
				#	$tip = intet_pton("$target");#takes a string of an IP address and translates that to a packed binary address 
										#structure suitable to pass to pack_sockaddr_in(). Supports only IPV4;use intet_pton() for IpV6
				#};
				if(!$tip) {
					return $errmsg."Cannot get IP for $origtarget";
				}
				#eval { $tip = inet_ntop(AF_INET6,$tip); };#Takes a packed binary address structure such as returned by unpack_sockaddr_in()
										 #supports IPv4; use inet_ntop() for IpV6.
			} else {
				$tip = $target;
			}
		}else{
			if($target !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1.3}$/) {
	
				eval {
					$tip = inet_aton("$target");#takes a string of an IP address and translates that to a packed binary address 
										#structure suitable to pass to pack_sockaddr_in(). Supports only IPV4;use intet_pton() for IpV6
				};
				if(!$tip) {
					return $errmsg."Cannot get IP for $origtarget";
				}
				eval { $tip = inet_ntoa($tip); };#Takes a packed binary address structure such as returned by unpack_sockaddr_in()
										 #supports IPv4; use inet_ntop() for IpV6.
			} else {
				$tip = $target;
			}
		}
	}else{
		return $errmsg."You have entered an invalid IP address. Please check your input!";
	}
	print LGDBG "HOST:$host,DEST:$tip\n";
	$logger->debug("HOST:$host,DEST:$tip\n");

	# Instantiate object
	my $ok = 0;
        my $cs;
	my @cmd_output;
	my $match='/(?m:^(?:[\w.\/]+\:)?[\w.-]+\s?(?:\(config[^\)]*\))?\s?[\$#>]\s?(?:\(enable\))?\s*$)/';
	#$routername .= '%';
	my $router_software = &get_router_os_looking_glass($routername);
	eval {
	print LGDBG "router : $routername , software : $router_software\n";
		$logger->debug("router : $routername , software : $router_software\n");
                if ($router_software =~ /juniper/i){
			$cs = new Net::Telnet (Timeout => 30); 										###Telneting starts here###
			$cs->open("$host");
			$cs->login("$user", "$passwd");
			$cs->max_buffer_length(100 * $MB);
	        } else{
			$logger->debug("Trying to connect to host : $pop \n");
			$cs = Net::Telnet::Cisco->new( Host => $pop,Timeout => 300,-prompt => '/(?m:^(?:[\w.\/]+\:)?[\w.-]+\s?(?:\(config[^\)]*\))?\s?[\$#>]\s?(?:\(enable\))?\s*$)/' );
			$logger->debug("Trying to login to router : $routername \n");
			# Login to router
			#if($router_software =~ /Cisco XR/i){
			# 	$cs->login( Name => 'natm001',
			#		Password => 'Reachatm',
			#		Timeout => 300
			#		 );
			#}else{
				$cs->login( Name => 'natm001',
					Password => 'Reachatm',
					Timeout => 300 );
			#}
			$logger->debug("logged in to router : $routername \n");
			 #$cs->login('natm001','Reachatm');
			if($router_software =~ /Cisco XR/i){
				@cmd_output = $cs->cmd( String =>'terminal length 0' );
				$logger->debug("ran command set length 0 on : $routername \n")
			}else{
			 	@cmd_output = $cs->cmd( 'terminal length 0' );
			 	$logger->debug("ran command terminal length 0 on : $routername \n");
			}
			  #return $errmsg."This host is not accesible at this point of time. Please try after some time!";
			
                }
		$cmd = "trace $tip"; ##trace command; "trace <IP>"
		my @routerval = ();
		my @screenvals = ();
		my @results = ();
		
		if($action =~ /trace/i) {												###for node to host trace###
#	        	my @trace = $cs->cmd( String => $cmd,
#        	                      Timeout => 600);
#	        	foreach my $v (@trace) {
				$logger->debug("Tracevalue :$v \n");
#        			next if($v !~ /^\s+\d/);
#	                	push @routerval, $v;
#		        }
			my $ctr = 0;
			my %result;
			my $res1;
			my $res2;
			my $res3;
			my $resFail;
			$logger->debug("running command :$cmd \n");
                        $cs->print("$cmd");
                        while(my $l = $cs->getline(Timeout => 25)) {
				#$logger->debug("getline :$l \n");
                                chomp($l);
								
				my $countfail = 0;
                                next if($l =~ /(Type|Trac)/i);
				next if($l !~ /(^\s+\d+|msec)/i);
				$logger->debug("getline :$l \n");
				push @results , $l;
				if($ctr == 0) {
					#$logger->debug("in if $ctr \n");
					#push @results , $l;
		                   #&rt_print_results("$action", $rpop, $tip, $origtarget, "$action from $rpop to $tip ($target):", 1, 0, $l);
		                } else {
					#$logger->debug("in else $ctr \n");
					#push @results , $l;
		                	#&rt_print_results("$action", $rpop, $tip, $origtarget, "$action from $rpop to $tip ($target):", 0, 0, $l);
		                }
				$ctr++;
				if($ipv==6){
				  if($router_software =~ /Cisco XR/i){
					$count++ if($l =~ /\?\s+\?\s+\?/);
					#last if($l =~ /\*/i);
					last if($l =~ /\s+30\s+/);
					last if($l =~ /\?\s+\*\s+\?/);
					last if($l =~ /$tip/i);
					}else{
					  $count++ if($l =~ /\*\s+\*\s+\*/);
				 	  last if($l =~ /\s+30\s+/);
				 	  last if($l =~ /$tip/i);
					  last if($l =~ /\!U/i);
					}
				}else{
				  if($router_software =~ /Cisco XR/i){
				 	$count++ if($l =~ /\*\s+\*\s+\*/);
				 	last if($l =~ /\s+30\s+/);
				 	last if($l =~ /\*/i);
				 	last if($l =~ /$tip/i);
					}else{
					$count++ if($l =~ /\*\s+\*\s+\*/);
				 	last if($l =~ /\s+30\s+/);
				 	last if($l =~ /$tip/i);
					}
				}
				if($count > 4) {
					$l = " Destination host unreachable";
					#push @results , $l;
					#print_results("$action", $rpop, $tip, $origtarget, "$action from $rpop to $tip ($target):", 0, @results);
					last;
				}
				if($l =~ /\*/i){
					if($l !~ /\*\s+\*\s+\*/){
						$logger->debug("end of trace \n");
						&print_results("$action", $rpop, $tip, $origtarget, "$action from $rpop to $tip ", 0, @results );
					}
				    
				}
				
			$logger->debug("end of while loop \n");
                        }
			$logger->debug("going to print_result \n");
			&print_results("$action", $rpop, $tip, $origtarget, "$action from $rpop to $tip ($target):", 0, @results );


		} else {																###for node test####
			if ($router_software =~ /juniper/i){
                                $cs->max_buffer_length(100 * $MB);
				my $ppcmd = "ping $tip source $host count 10 size 64";
				print DDD "\n $ppcmd\n"; 
$logger->debug("\n $ppcmd\n");				
				my @test = $cs->cmd( String => $ppcmd,
                      				Timeout => 10);
                                foreach my $v (@test) {
					next if($v =~ /^\n/);

					next if($v =~ /ping/i);
					next if($v =~ /olss/i);
					next if($v =~ /\!/i);
					push @routerval, $v;
				}
			} else {	
				print LGDBG "Loeeged in to router. Next execute ping\n";
				$logger->debug("Loeeged in to router. Next execute ping\n");
				if($host =~ /^(pad|nzsx|wel|per|syd)/) {
					$cs->print ("enable"); $cs->waitfor('/User.*$/');
					$cs->print("cisadmin"); $cs->waitfor('/Pass.*$/');
					$cs->print("two24pm"); $cs->waitfor('/.*#$/');
				}
				$cs->print("ping"); $cs->waitfor('/Protocol.*$/');
					if($ipv==6){
					$cs->print("ipv6"); $cs->waitfor('/Target.*$/');
					}
					else{
					$cs->print("ip"); $cs->waitfor('/Target.*$/');
					}
				$cs->print("$tip");
				$cs->waitfor(Match => '/Repeat.*$/', Timeout => 10);
				$cs->print("10"); $cs->waitfor('/Datagram.*$/');   
				$cs->print("64"); $cs->waitfor('/Timeout.*$/');
				$cs->print("");
				print LGDBG "before interval prompt\n";
					
				$logger->debug("before interval prompt\n");
				$cs->waitfor(Match => '/Interval.*$|Extended.*$/');
				my $prmptmatch = $cs->last_prompt;
				print LGDBG "prompt: $prmptmatch\n";
			$logger->debug("prompt: $prmptmatch\n");
				if($prmptmatch =~ /Interval.*$/){ 
					print LGDBG "matched prompt\n";
					$logger->debug("matched prompt\n");
					$cs->print("");
					$cs->waitfor('/Extended.*$/');
					$cs->print("y");}
				else 
				{$cs->print("y");}

			if($router_software =~ /Cisco XR/i){
				$logger->debug("router software is ciscoXR");
				if($ipv==6){
				  $logger->debug("IP version is 6");
				  $cs->waitfor('/UDP.*$/'); $cs->print("");
				  $cs->waitfor('/Priority.*$/'); $cs->print("");
				  $cs->waitfor('/Include.*$/'); $cs->print("");
				  $cs->waitfor('/Set.*$/'); $cs->print("");
				}else{
				  $logger->debug("IP version is 4");
				  $cs->waitfor('/Source.*$/'); $cs->print("");
				  $cs->waitfor('/Type.*$/'); $cs->print("");
				  $cs->waitfor('/Set.*$/'); $cs->print("");
				  $cs->waitfor('/Validate.*$/'); $cs->print("");
				  $cs->waitfor('/Data.*$/'); $cs->print("");
				  $cs->waitfor('/Loose.*$/');
				  $cs->print("Verbose");
				  $cs->waitfor('/Loose.*$/'); $cs->print("");
				}
			}else{
				if($action =~ /node/i) {
					print LGDBG "Action is node\n";
					$logger->debug("Action is node and host is $host\n");
                                        $cs->waitfor('/Source.*$/'); $cs->print("");
										$logger->debug("wait is over for $host\n");
                                } else {
                                        $cs->waitfor('/Source.*$/'); $cs->print("");
                                }
				if($ipv==6){
					$cs->waitfor('/UDP.*$/'); $cs->print("");
					$cs->waitfor('/Verbose.*$/'); $cs->print("y");
					$cs->waitfor('/Precedence.*$/'); $cs->print("");
					$cs->waitfor('/DSCP.*$/'); $cs->print("");
					$cs->waitfor('/Include.*$/'); $cs->print("");
					$cs->waitfor('/Include.*$/'); $cs->print("");
					}
					else{
				$cs->waitfor('/Type.*$/'); $cs->print("");
				$cs->waitfor('/Set.*$/'); $cs->print("");
				$cs->waitfor('/Validate.*$/'); $cs->print("");
				$cs->waitfor('/Data.*$/'); $cs->print("");
				$cs->waitfor('/Loose.*$/');
				$cs->print("Verbose");
				$cs->waitfor('/Loose.*$/'); $cs->print("");
				}
			}
				
				
				$cs->waitfor('/Sweep.*$/'); $cs->print("");
				my $last = 0;
				$logger->debug("before while and after last= $last\n");
				while( (!$last) && (my $v = $cs->getline(Timeout => 300)) )  {
					next if($v =~ /^\n/);
					next if($v =~ /type escape/i);
					print DDD "\n $v\n";
					$logger->debug("\n $v\n");					
					push @routerval, $v;
					$last = 1 if($v =~ /^Succ/);
				}
			}		
		}
		$cs->close;
		$ok = 1;
		if($action !~ /trace/i) {
			&print_results("$action", $rpop, $tip, $origtarget,"$action from $rpop to $tip ($target):", $router_software, @routerval);
		}
	};
	if($action =~ /trace/i) {
		if( (!$ok) || ($@) ) {
			&log_entry($internalerror, "LG TRACE", "$@");
			if ($@ !~ /read timed-out*/i) {
				return $errmsg."Host ($tip) not reachable.  This may be due to the existence of a firewall at the target site.";
			}
		}
	} else {
		&log_entry($internalerror, "LG", "$@");
		my $te = "The gateway in $target has currently reached its capacity limit and will temporarily be unavailable from this location.";
		$te .= " Please select another PoP location or retry this location later.";
		return $errmsg.$te
		if( (!$ok) || ($@) );
	}
	close(DDD);
}

#IPSA_CR
sub rt_print_results {
	my($action, $src, $tip, $target, $msg, $init, $end, $val) = @_;
	#my $tracestring;
	my %out;
        if($init) {

	return $val;
        } elsif($end) {

        }  else {

			 %out=('val3' =>$val);
        }
	print "Content-Type: application/json\n\n";
	my $output = encode_json \%out;		
	print $output;
}

####################
#CRQ000000004261
####################
#IPSA_CR
sub print_results {
	my($action, $src, $tip, $target, $msg, $router_software,@vals) = @_;
	my $result;
	my %output;
#	$output{'val0'} = $msg;
	if($action =~ /trace/i) {
		my $count=1;
		foreach my $l (@vals) {
			chomp($l);
	$logger->debug("In print_trace: $l \n");
		$output{$count} = $l;
		$count++;
		}
	print "Content-Type: application/json\n\n";

	my $traceres = encode_json \%output;		
	print $traceres ;
	} else {
		$tip = ($action =~ /node/i) ? "" : "($tip)";
		if($router_software =~ /juniper/i) {
			$result=&process_ping_juniper($src, $target, $tip, @vals);
			print "Content-Type: application/json\n\n";
			
			print $result;
                } else {
				$result=&process_ping($src, $target, $tip, @vals);
				print "Content-Type: application/json\n\n";
			

				print $result;
                }                
	}
}
#Added by deepti on 02-10-2009 
# Input Parameters : ping command output
# Output Parameters : %loss,min latency,max latency and average latency

sub process_ping_juniper {
my($src, $target, $tip, @vals) = @_;
my %pingresults;
my @testresults = ();
        my($loss, $min, $avg, $max);
my $date = gmtime(time);
        chomp($date);
        $date .= " GMT";
foreach my $v (@vals) {
        print LGDBG "v is $v\n";
		$logger->debug("v is $v\n");		
                chomp($v);
if($v =~ /icmp_seq/i){
                my($var1,$var2,$var3,$var4) = split(/=/, $v);
                my @t1 = split(/ /, $var4);
                my $val1 = $t1[$#t1-1];
                push @testresults, $val1;
                }
if($v =~ /^round-trip/i) {
#call function proc_rtt_juniper if the ping output consists of round-trip
                ($min, $avg, $max, $stddev) = &proc_rtt_juniper($v);
                }
                 if($v =~ /packets/i){
                 $loss = &proc_loss_juniper($v);
}
}


        my $idx = 0;

        if($#testresults != 9) {
                for my $i ($#testresults+1 .. 9) {
                        push @testresults, '-';
                }
        }

        foreach my $vx (@testresults) {
                $idx++;
                $pingresults{'result'.$idx}= $vx;
        }
	
	my %out=('date' =>$date,'MinLatency' =>$min,'MaxLatency' =>$max,'AvgLatency' =>$avg,'StdDeviation' =>$stddev,'AvgLoss' =>$loss);
	%out = (%out, %pingresults);
	my $output = encode_json \%out;		
	return $output;
}

sub process_ping {
	my($src, $target, $tip, @vals) = @_;
	
	my %pingresults;
	my @testresults = ();
	my($loss, $min, $avg, $max);

	my $date = gmtime(time);
	chomp($date);
	$date .= " GMT";
	foreach my $v (@vals) {
		chomp($v);
		if($v =~ /Reply to request/i) {
			my @t = split(/\s+/, $v);
			my $val = $t[$#t-1];
			$val =~ s/\(//g;
			push @testresults, $val;
		}
		if($v =~ /^Success rate/i) {
                ($loss, $min, $avg, $max) = &proc_rtt($v);
                }
	}
	my $stddev = &calc_stddev(@testresults);

	my $idx = 0;

	if($#testresults != 9) {
		for my $i ($#testresults+1 .. 9) {
			push @testresults, '-';
		}
	}

	foreach my $vx (@testresults) {
		$idx++;
		$pingresults{'result'.$idx}= $vx;
	}
	
	my %out=('date' =>$date,'MinLatency' =>$min,'MaxLatency' =>$max,'AvgLatency' =>$avg,'StdDeviation' =>$stddev,'AvgLoss' =>$loss);
	%out = (%out, %pingresults);
	my $output = encode_json \%out;		
	return $output;	
}
sub proc_loss_juniper {
my($line) = shift;
chomp($line);
my($a,$b,$c) = split(/\,/, $line);
print LGDBG "$c\n";
$logger->debug("$c\n");	
my($c,$d,$e) = split(/ /,$c);
$d =~ s/%//;
return $d;
}
#Added by Deepti on 03-10-2009 for juniper routers(VPLS and EVPL) to calculate min,max,avg latency and std deviation 
sub proc_rtt_juniper {
        my($line) = shift;
        chomp($line);
        my($a, $b) = split(/\=/, $line);

        # calculate RTT
        $b =~ s/ //g; $b =~ s/ms//g;
        my($min, $avg, $max, $stddev) = split(/\//, $b);
        $min = sprintf("%2.2f", $min);
        $avg = sprintf("%2.2f", $avg);
        $max = sprintf("%2.2f", $max);
        $stddev = sprintf("%2.2f", $stddev);
        return ($min, $avg, $max, $stddev);
}

#
# Pre: line from ping output from Cisco
# Post: percentage_loss, minrtt, avgrtt, maxrtt
#
sub proc_rtt {
        my($line) = shift;
        chomp($line);
        my($a, $b) = split(/\,/, $line);
        # Calculate percentage success
        $a = (split(/\(/, $a))[1];
        $a =~ s/\)//g;
        my($sent, $count) = split(/\//, $a);
        my $rate = ($count - $sent) / 10;
        $rate = $rate * 100;                        
 
        # calculate RTT  
        $b = (split(/\=/, $b))[1];
        $b =~ s/ //g; $b =~ s/ms//g;
        my($min, $avg, $max) = split(/\//, $b);
        $min = sprintf("%2.2f", $min);
        $avg = sprintf("%2.2f", $avg);
        $max = sprintf("%2.2f", $max); 
        
        return ($rate, $min, $avg, $max);
}               

sub calc_stddev {
        my(@vals) = @_; 

	return 0 if($#vals < 0);

        #my @vals = split(/\s+/, $line); 
        # 0 = timestamp - not required
        #shift(@vals);
        my $i = ($#vals) ? $#vals + 1 : 1;
        my $avg = my $sum = my $variance = my $stddev = 0;
        foreach my $v (@vals) { 
                $sum += $v;
        }
	#print "($avg) ($sum) ($i) (@vals) ($#vals)\n";
        $avg = $sum / $i;

        $sum = 0;
        foreach my $v (@vals) {
                $v -= $avg;
                $v = $v * $v;
                $sum += $v;
        }

        $variance = $sum / $i;

        $stddev = sqrt($variance);
        $stddev = sprintf("%2.2g", $stddev);
        return $stddev;  
}
 
