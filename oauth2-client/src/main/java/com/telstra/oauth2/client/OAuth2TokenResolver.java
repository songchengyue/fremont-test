package com.telstra.oauth2.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Component;

import com.telstra.oauth2.client.exception.TokenValueNotFoundException;

/**
 * Helper class for propagating an OAuth2 token to other OAuth2-secured services that we use!
 * 
 * @author d772392
 *
 */
@Component
public class OAuth2TokenResolver {
	
	//
	// we use a non-standard header name rather than the header 'Authorization'
	// because Tomcat doesn't play nicely with Bearer authorization. It strips
	// Authorization headers. You can turn this behaviour off with the CGIPassAuth
	// directive for Apache HTTP server but I can't find any documentation for
	// how to do this for Tomcat.
	//
	
	@Value("${oauth2.server.cgi.headerName:OAuth2-Bearer-Token}")
	private String headerName;
	
	public String getAuthorizationHeaderName() {
		return headerName;
	}
	
	public String getAuthorizationHeaderValue() {
		// change to "getTokenType() + " " + getTokenValue() if we ever work out
		// how to pass bearer tokens
		return getTokenValue();
	}
	
	public String getTokenValue() {	
		return getDetails().getTokenValue();
	}
	
	public String getTokenType() {	
		return getDetails().getTokenType();
	}

	private OAuth2AuthenticationDetails getDetails() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null) {
			throw new TokenValueNotFoundException("No authentication object available from security context");
		}
		
		if (!(auth.getDetails() instanceof OAuth2AuthenticationDetails)) {
			throw new TokenValueNotFoundException("Don't know how to get oauth2 token value from details object of class " + auth.getDetails().getClass());
		}
		
		OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails)auth.getDetails();
		return details;
	}

}
