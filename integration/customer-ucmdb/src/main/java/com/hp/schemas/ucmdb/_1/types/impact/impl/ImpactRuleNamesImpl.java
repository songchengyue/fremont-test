/*
 * XML Type:  ImpactRuleNames
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNames
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML ImpactRuleNames(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ImpactRuleNamesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactRuleNames
{
    
    public ImpactRuleNamesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTRULENAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "impactRuleName");
    
    
    /**
     * Gets array of all "impactRuleName" elements
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName[] getImpactRuleNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IMPACTRULENAME$0, targetList);
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName[] result = new com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "impactRuleName" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName getImpactRuleNameArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName)get_store().find_element_user(IMPACTRULENAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "impactRuleName" element
     */
    public int sizeOfImpactRuleNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IMPACTRULENAME$0);
        }
    }
    
    /**
     * Sets array of all "impactRuleName" element
     */
    public void setImpactRuleNameArray(com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName[] impactRuleNameArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(impactRuleNameArray, IMPACTRULENAME$0);
        }
    }
    
    /**
     * Sets ith "impactRuleName" element
     */
    public void setImpactRuleNameArray(int i, com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName impactRuleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName)get_store().find_element_user(IMPACTRULENAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(impactRuleName);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "impactRuleName" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName insertNewImpactRuleName(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName)get_store().insert_element_user(IMPACTRULENAME$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "impactRuleName" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName addNewImpactRuleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName)get_store().add_element_user(IMPACTRULENAME$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "impactRuleName" element
     */
    public void removeImpactRuleName(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IMPACTRULENAME$0, i);
        }
    }
}
