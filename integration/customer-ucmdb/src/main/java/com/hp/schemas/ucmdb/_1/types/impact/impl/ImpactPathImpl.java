/*
 * XML Type:  ImpactPath
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactPath
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML ImpactPath(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is a complex type.
 */
public class ImpactPathImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.impact.ImpactPath
{
    
    public ImpactPathImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTSTEP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/impact", "ImpactStep");
    
    
    /**
     * Gets array of all "ImpactStep" elements
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactStep[] getImpactStepArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IMPACTSTEP$0, targetList);
            com.hp.schemas.ucmdb._1.types.impact.ImpactStep[] result = new com.hp.schemas.ucmdb._1.types.impact.ImpactStep[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "ImpactStep" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactStep getImpactStepArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactStep target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactStep)get_store().find_element_user(IMPACTSTEP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "ImpactStep" element
     */
    public int sizeOfImpactStepArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IMPACTSTEP$0);
        }
    }
    
    /**
     * Sets array of all "ImpactStep" element
     */
    public void setImpactStepArray(com.hp.schemas.ucmdb._1.types.impact.ImpactStep[] impactStepArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(impactStepArray, IMPACTSTEP$0);
        }
    }
    
    /**
     * Sets ith "ImpactStep" element
     */
    public void setImpactStepArray(int i, com.hp.schemas.ucmdb._1.types.impact.ImpactStep impactStep)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactStep target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactStep)get_store().find_element_user(IMPACTSTEP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(impactStep);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ImpactStep" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactStep insertNewImpactStep(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactStep target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactStep)get_store().insert_element_user(IMPACTSTEP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ImpactStep" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactStep addNewImpactStep()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactStep target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactStep)get_store().add_element_user(IMPACTSTEP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "ImpactStep" element
     */
    public void removeImpactStep(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IMPACTSTEP$0, i);
        }
    }
}
