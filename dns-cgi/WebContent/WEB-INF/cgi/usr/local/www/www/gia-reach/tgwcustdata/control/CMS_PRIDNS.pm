# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
#
# $Id: CMS_PRIDNS.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $

package CMS_PRIDNS;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_PRIDNS_DB;

# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;

our @ISA = qw (Exporter);

our $error;

sub get_pridns_requests {
print TTG " FUNC CALL get_pridns_requests - VALUES:\n $CMS::html_values{search_accno}{value} \n $CMS::html_values{search_domain}{value}\n $CMS::html_values{search_primaryip}{value}\n $CMS::html_values{search_status}{value}\n ";

  $CMS::html_values{requests}{table} =
	CMS_PRIDNS_DB::search_pridns_entries(@_);

print TTG " table: @{$CMS::html_values{requests}{table}}\n\n";
  unshift(@{$CMS::html_values{requests}{table}},
	["Operations Handle", "Domain Name", "Primary IP", "Status",
	"Request time"]);
  $CMS::html_values{requests}{header_rows} = 1;
}

sub start {
  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} eq "Cancel"))) {
    # default to view_recent_requests
    
    $CMS::html_values{command}{value} = "view_recent_requests";
    
  }

  $CMS::html_values{search_status}{options} = 
    CMS_PRIDNS_DB::get_pridns_status;

open ( TTG, ">/tmp/pridnspm");
print TTG "ENTER\n"; 
  if ($CMS::html_values{command}{value} eq "view_recent_requests") {
    get_pridns_requests();

    TI_HTML::set_datetime_search_values
	(\%CMS::html_values, "search_date",
	 "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");
print TTG "CLICKED ON VIEW\n";
      CMS::output_html("pridns_list");
  
  } elsif ($CMS::html_values{command}{value} eq "search_requests") {
print TTG "CLICKED ON SEARCH\n";
    if (defined($CMS::html_values{submit}{value}) && $CMS::html_values{submit}{value} eq "Search") {
      TI_HTML::set_datetime_search_values
        (\%CMS::html_values, "search_date",
         "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");

      my ($start,$end) = "";
      ($start,$end) = TI_HTML::get_datetime_search_values
	(\%CMS::html_values, "search_date");
print TTG "start: $start\n end: $end\n search_date: $CMS::html_values{search_date}{value}\n";
print TTG " VALUES:\n $CMS::html_values{search_accno}{value} \n $CMS::html_values{search_domain}{value}\n $CMS::html_values{search_primaryip}{value}\n $CMS::html_values{search_status}{value}\n $start\n $end\n ";
      get_pridns_requests
	($CMS::html_values{search_accno}{value},
	 $CMS::html_values{search_domain}{value},
	 $CMS::html_values{search_primaryip}{value},
	 $CMS::html_values{search_status}{value},
	 $start, $end);

    } elsif (defined($CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq
 "Clear search parameters" )) {

      $CMS::html_values{search_accno}{value} = "";
      $CMS::html_values{search_domain}{value} = "";
      $CMS::html_values{search_primaryip}{value} = "";
      $CMS::html_values{search_status}{value} = "";

      TI_HTML::set_datetime_search_values
	  (\%CMS::html_values, "search_date",
	   "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");

    }
close (TTG);
        CMS::output_html("pridns_list");
  }

}
1;
