#!/usr/bin/perl

#use strict;
use File::Copy;

my $time = time;
my $date = date;
my $datestr = `date +%Y%m%d`;
my $old = "old";
my ($diff,$flag) = 0;
my $file1 = "/tmp/reverse_mail_india";
#my $cmd = `ps -ef | grep named | grep -v grep | awk '{ print $2 }'`;
my $cmd = `ps -ef | grep named | grep -i netops | grep -i india | grep -v grep | cut -c9-15`;
#print "$date";
print "before killing $cmd\n";

my @files = </var/webzone/india/*>;
foreach $file (@files)
{
	#print "$file\n";
	my @direc= split(/\//,$file);
	$last_file = pop(@direc);
	print "$last_file\n"; 
	#my $destdir = '/usr/local/dns/dns1.net.reach.com/zones/';
	my $destdir = '/usr/local/dns/netops/india/zones/';
	$last_file1 = $destdir.$last_file;
	print "last_file1:$last_file1\n";
        #check if the zone file exists
	if (-e $last_file1){
		#take a diff
		$diff = `diff $file  /usr/local/dns/netops/india/zones/$last_file |wc -l`;
		print "DIFF: $diff\n";
		if ($diff > 0)
        		{
        		#backup
			`cp -p  /usr/local/dns/netops/india/zones/$last_file  /usr/local/dns/netops/india/zones/$last_file.$datestr`;
        		print "taking backup..\n";
			print "file $file to /usr/local/dns/netops/india/zones/$last_file.$datestr \n";
			`mv $file  /usr/local/dns/netops/india/zones`;
			$flag = 1;
        		}
	}else {
                print "$last_file1 does not exist\n";
                #send a mail in case of new zone file
		open (FILE,">/usr/local/dns/netops/india/ipnocmail.txt");
                print FILE "Zone file not found\n$last_file does not exist\n";
                close (FILE);
                print "before sending mail\n";
		`mailx -s "IPNOC mail" -r netops.net\@reach.com -c  JignaChandrakant_T\@infosys.com Pradeep_Raturi\@infosys.com Hari_V02\@infosys.com < /usr/local/dns/netops/india/ipnocmail.txt`;
		print "mail sent to IPNOC\n";
        }#end of if
	
}
if ($flag == 1) {
                    	#kill the process
	               print "killing the process $cmd\n";
	              `sudo kill -9 $cmd`;
     	               #restart
                       print "restarting the server\n";
			`sudo /usr/local/dns/sbin/named -c /usr/local/dns/netops/india/conf/named.conf -u named`;
			$cmd = `ps -ef | grep named | grep -i netops | grep -i india | grep -v grep | cut -c9-15`;
			#$cmd = `ps -ef | grep named  | awk '{ print $2 }'`;
			print "after restart: $cmd \n";
}
