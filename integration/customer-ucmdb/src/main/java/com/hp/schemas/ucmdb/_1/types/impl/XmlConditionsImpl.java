/*
 * XML Type:  XmlConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.XmlConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML XmlConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class XmlConditionsImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.XmlConditions
{
    
    public XmlConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName XMLCONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "xmlCondition");
    
    
    /**
     * Gets array of all "xmlCondition" elements
     */
    public com.hp.schemas.ucmdb._1.types.XmlCondition[] getXmlConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(XMLCONDITION$0, targetList);
            com.hp.schemas.ucmdb._1.types.XmlCondition[] result = new com.hp.schemas.ucmdb._1.types.XmlCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "xmlCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlCondition getXmlConditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlCondition)get_store().find_element_user(XMLCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "xmlCondition" element
     */
    public int sizeOfXmlConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(XMLCONDITION$0);
        }
    }
    
    /**
     * Sets array of all "xmlCondition" element
     */
    public void setXmlConditionArray(com.hp.schemas.ucmdb._1.types.XmlCondition[] xmlConditionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(xmlConditionArray, XMLCONDITION$0);
        }
    }
    
    /**
     * Sets ith "xmlCondition" element
     */
    public void setXmlConditionArray(int i, com.hp.schemas.ucmdb._1.types.XmlCondition xmlCondition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlCondition)get_store().find_element_user(XMLCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(xmlCondition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "xmlCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlCondition insertNewXmlCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlCondition)get_store().insert_element_user(XMLCONDITION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "xmlCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlCondition addNewXmlCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlCondition)get_store().add_element_user(XMLCONDITION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "xmlCondition" element
     */
    public void removeXmlCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(XMLCONDITION$0, i);
        }
    }
}
