####################################################################################################
# File        : CMS_AGGREGATES_DB.pm
# Description : Gets parameters from CMS_AGGREGATES.pm file and directly interacts with database for adding,editing,listing # or deleting an aggregate/switch service.
# Parameters to be passed : aggregate_serviceID,description,
# Author      : KETAKI THOMBARE (Infosys Tech. Ltd., Bangalore )
# Started On  : 09 July 2009
# Modification History :
#  Date               Name                 Change/Description
# ------------------------------------------------------------------
# 11-Aug-2009    Ketaki Popat Thombare    Baseline for EVPL
# 12-Aug-2009    Deepti Jaisoor           Added changes for add and edit aggregates for EVPL
# 15-Aug-2009    Ketaki Popat Thombare    Added changes for list and delete aggregates for EVPL 
##################################################################################################

package CMS_AGGREGATES_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
#use CMS_OLSS;
use CMS_CUSTOMER_DB;

our $error;

sub add_aggregate_service {
open (DEBUG,">/tmp/ADD_AGGRAGTE");
  my $aggregate_serviceid = shift @_;
  my $description = shift @_;
  
#++EVPL - Checking for VLAN/Transparent aggregate serviceIDs 
if ($aggregate_serviceid =~ /.*\[.*\].*ec.*sw$/ || $aggregate_serviceid =~ /.*\[.*\].*ec/ || $aggregate_serviceid =~ /.*evp./){
  my $aggregate_serviceid_pre = $aggregate_serviceid;
  $aggregate_serviceid_pre =~ s/sw$/asw/;
  my $aggregate_serviceid_a = $aggregate_serviceid_pre;
  my $sth = $dbh->prepare ("SELECT aggregate_serviceid FROM aggregate_service WHERE aggregate_serviceid ilike '$aggregate_serviceid_a'");
        $sth->execute();
        my $r = $sth->fetchrow_hashref;
        if($r){
                return "Duplicated Aggregate/Switch ServiceID.";
        }
}else{
   my $sth = $dbh->prepare ("SELECT aggregate_serviceid FROM aggregate_service WHERE aggregate_serviceid ilike '$aggregate_serviceid'");
  $sth->execute();
        my $r = $sth->fetchrow_hashref;
        if($r){
                return "Duplicated Aggregate ServiceID.";
        }
}

# Checking aggregate_serviceid for square brackets and "ec" for EVPL and inserting two rows for an aggregatre service one for A End and one for Z End
if ($aggregate_serviceid =~ /.*\[.*\].*ec.*sw$/){

  my $aggregate_serviceid_pre = $aggregate_serviceid;
  $aggregate_serviceid_pre =~ s/sw$/asw/;
  my $aggregate_serviceid_a = $aggregate_serviceid_pre;
  my $full_a = "x-alias-".$aggregate_serviceid_a;

  $aggregate_serviceid =~ s/sw$/zsw/;
  my $aggregate_serviceid_z =$aggregate_serviceid;
  my $full_z = "x-alias-".$aggregate_serviceid_z;

   my $sth_a = $dbh->prepare("INSERT INTO aggregate_service (aggregate_serviceid, aggregate_service_full, active, description) VALUES('$aggregate_serviceid_a','$full_a','active','$description')");
   my $sth_z = $dbh->prepare("INSERT INTO aggregate_service (aggregate_serviceid, aggregate_service_full, active, description) VALUES('$aggregate_serviceid_z','$full_z','active','$description')");

  unless ($sth_a->execute) {
    $error = $dbh->errstr;
close DEBUG;
    return $error;
  }
 unless ($sth_z->execute) {
    $error = $dbh->errstr;
    return $error;
  }
#--EVPL End of EVPL
  }else{
   my $full = "x-alias-".$aggregate_serviceid;
   my $sth = $dbh->prepare("INSERT INTO aggregate_service (aggregate_serviceid, aggregate_service_full, active, description) VALUES('$aggregate_serviceid','$full','active','$description')");
    unless ($sth->execute) {
    $error = $dbh->errstr;
    return $error;
    }
  }

  return "1";
}

sub get_aggregate_serviceid {
  
   my $sth = $dbh->prepare("select aggregate_serviceid as SID from aggregate_service where aggregate_serviceid not like '%asw' and aggregate_serviceid not like '%zsw' union select trim(trailing 'asw' from aggregate_serviceid)||'sw' as SID FROM aggregate_service where aggregate_serviceid like '%asw'");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  
  if($result->[0]) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } 
  else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub get_allaggregate_serviceid {
  
   my $sth = $dbh->prepare("select aggregate_serviceid as SID from aggregate_service where aggregate_serviceid not like '%[%]%ec%asw' and aggregate_serviceid not like '%[%]%ec%zsw' union select trim(trailing 'asw' from aggregate_serviceid)||'sw' as SID FROM aggregate_service where aggregate_serviceid like '%[%]%ec%asw'");
  
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchall_arrayref;
  
  if($result->[0]) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } 
  else {
    $error = $dbh->errstr;
    return 0;
  }
}


sub get_description {
  my $agg_serviceid = shift @_;
#++EVPL - Searchinmg for square brackets and ec,sw for EVPL switch service
 if ($agg_serviceid =~ /.*\[.*\].*ec.*sw$/){ 
  $agg_serviceid =~ s/sw$/asw/;
  my $aggregate_serviceid_a = $agg_serviceid;
print DEBUG "INSIDE SERVICE\n";
 my $sth = $dbh->prepare
    ("SELECT description FROM aggregate_service WHERE aggregate_serviceid = '$aggregate_serviceid_a'");
  unless ($sth->execute) {
print DEBUG "INSIDE UNLESS\n";
    $error = $dbh->errstr;
    return "0";
  }

  my @result = $sth->fetchrow_array;

    $sth->finish;
    return $result[0];

 }
#--EVPL
else{
print DEBUG "INSIDE ELSE\n";
  my $sth = $dbh->prepare
    ("SELECT description FROM aggregate_service WHERE aggregate_serviceid = '$agg_serviceid'");
 print DEBUG "sth: SELECT description FROM aggregate_service WHERE aggregate_serviceid = '$agg_serviceid'\n"; 
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return "0";
  }
  
  my @result = $sth->fetchrow_array;

    $sth->finish;
    return $result[0];
print DEBUG "result:$result[0]\n";
}
close DEBUG;
}

sub update_description {
  my $agg_serviceid = shift @_;
  my $description = $dbh->quote(shift @_);
#++EVPL
  if ($agg_serviceid =~ /.*\[.*\].*ec.*sw$/){
  my $aggregate_serviceid_pre = $agg_serviceid;
  $aggregate_serviceid_pre =~ s/sw$/asw/;
  my $aggregate_serviceid_a = $aggregate_serviceid_pre;
  $agg_serviceid =~ s/sw$/zsw/;
  my $aggregate_serviceid_z = $agg_serviceid;
      my $sth_a = $dbh->prepare("UPDATE aggregate_service SET description = $description WHERE aggregate_serviceid = '$aggregate_serviceid_a'");
      my $sth_z = $dbh->prepare("UPDATE aggregate_service SET description = $description WHERE aggregate_serviceid = '$aggregate_serviceid_z'");
      unless ($sth_a->execute) {
      $error = $dbh->errstr;
      return 0;
      }
      unless ($sth_z->execute) {
      $error = $dbh->errstr;
      return 0;
      }
#--EVPL
  }else{

      my $sth = $dbh->prepare("UPDATE aggregate_service SET description = $description WHERE aggregate_serviceid = '$agg_serviceid'");
  
     unless ($sth->execute) {
     $error = $dbh->errstr;
     return 0;
     }
  }
  return "1";
}


sub delete_aggregate {
open (DEBUG ,">/tmp/delete_aggregate");
  my $aggregate_serviceid = $dbh->quote(shift @_);
print DEBUG "aggregate_serviceid:$aggregate_serviceid\n";
  #++Telstra Alarm CR
  #Aggregate service should be delted only when there is no link record associated with it.
  my $sth = $dbh->prepare("select linkid from link where serviceid = $aggregate_serviceid");
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return $error;
  }
  my $result_link = $sth->fetchrow_arrayref;
  my $linkid  = $$result_link[0];
  $sth->finish;
print DEBUG "linkid:$linkid\n";
   if ($linkid  ne "") {
	$error = "Link record Exists for the switch/aggregate service.";
return $error;
}
 #--Telstra Alarm CR
  $sth = $dbh->prepare("UPDATE service set aggregate_serviceid='' where aggregate_serviceid=$aggregate_serviceid");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return $error;
  }

  $sth = $dbh->prepare("DELETE FROM aggregate_service WHERE aggregate_serviceid = $aggregate_serviceid");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return $error;
  }

  return "1";
}

sub list_aggregateservice {
  my $aggregate_serviceid = shift @_;
  my $sth;

  if ($aggregate_serviceid eq "") {	# list all aggregates and their corresponding serviceid
    $sth = $dbh->prepare
      ("SELECT DISTINCT aggregate_service.aggregate_serviceid, serviceid, accno, aggregate_service.description   FROM aggregate_service left join service on aggregate_service.aggregate_serviceid=service.aggregate_serviceid WHERE aggregate_service.aggregate_serviceid<>'' and aggregate_service.aggregate_serviceid not like '%[%]%ec%zsw' and aggregate_service.active<>'inactive' ");
 } else {
    $sth = $dbh->prepare
      ("SELECT DISTINCT aggregate_service.aggregate_serviceid, serviceid, accno, aggregate_service.description    
	FROM aggregate_service left join service on aggregate_service.aggregate_serviceid=service.aggregate_serviceid 
	WHERE aggregate_service.aggregate_serviceid='$aggregate_serviceid'");
  }
    
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
                
  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub verify_accno_product {
  my $accno       = shift @_;
  #my $productcode = uc $dbh->quote(shift @_);
  my $productcode = shift @_;
  my $current = get_product_codes_for_accno($accno);

  #if(ref($current) eq 'ARRAY'){
  if(ref($current) eq 'HASH'){
    #for(@$current){
    foreach my $lk (keys %$current) {
      #print "<P>DD ($lk) ($$current{$lk}{code}) ($productcode)\n";
      #return 'TRUE' if ($_->{productcodeid} =~ /^$productcode$/i);
      return 'TRUE' if ($$current{$lk}{code} =~ /^$productcode$/i);
    }
  }
  return 'FALSE'
}

sub get_aggregate_by_acc {
  my $accno = shift @_;
  my $product_code = shift @_;

  my $sth = $dbh->prepare
      ("SELECT DISTINCT aggregate_service.aggregate_serviceid, aggregate_service.aggregate_service_full
        FROM aggregate_service left join service on aggregate_service.aggregate_serviceid = service.aggregate_serviceid
        WHERE aggregate_service.aggregate_serviceid != '' and accno='$accno' AND productcodeid ilike '%$product_code%'"); 

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
 
  my $result = $sth->fetchall_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}

sub get_aggregate_by_acc_aggrmast {
  my $accno = shift @_;
  my $product_code = shift @_;
  my $sth;
if ($product_code =~ /VPLS/i) {
  	$sth = $dbh->prepare
      ("SELECT DISTINCT aggregate_service.aggregate_serviceid, aggregate_service.aggregate_service_full,aggregate_service.svclabel,aggregate_service.reseller_svclabel
        FROM aggregate_service left join service on aggregate_service.aggregate_serviceid = service.aggregate_serviceid
        WHERE aggregate_service.aggregate_serviceid != '' and accno='$accno' AND productcodeid ilike '%$product_code%' AND aggregate_service.aggregate_serviceid NOT like '%sw'");
} else {
	$sth = $dbh->prepare
      ("SELECT DISTINCT aggregate_service.aggregate_serviceid, aggregate_service.aggregate_service_full,aggregate_service.svclabel,aggregate_service.reseller_svclabel
        FROM aggregate_service left join service on aggregate_service.aggregate_serviceid = service.aggregate_serviceid
        WHERE aggregate_service.aggregate_serviceid != '' and accno='$accno' AND productcodeid ilike '%$product_code%'");
}

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}


sub get_aggregate_by_serviceid {
  my $serviceid = shift @_;
  #my $product_code = shift @_;

  my $sth = $dbh->prepare
      ("SELECT DISTINCT aggregate_service.aggregate_serviceid, aggregate_service.aggregate_service_full
        FROM aggregate_service left join service on aggregate_service.aggregate_serviceid = service.aggregate_serviceid
        WHERE aggregate_service.aggregate_serviceid != '' and serviceid='$serviceid' ");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  my $result = $sth->fetchrow_arrayref;

  $error = $dbh->errstr;
  $sth->finish;
  return $result;
}
sub get_aggr_label_forservice {
        my $sid = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT svclabel FROM aggregate_service
                                WHERE aggregate_serviceid = $sid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}
sub get_aggr_reseller_label_forservice {
        my $sid = $dbh->quote(shift);
        my $sth = $dbh->prepare("SELECT reseller_svclabel as svclabel  FROM aggregate_service
                                WHERE aggregate_serviceid = $sid");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

1;
