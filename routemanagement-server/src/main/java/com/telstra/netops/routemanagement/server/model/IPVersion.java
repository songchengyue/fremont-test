package com.telstra.netops.routemanagement.server.model;

public enum IPVersion {

	IPv4(4), IPv6(6);

	private final Integer value;

	private IPVersion(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public static IPVersion findForValue(Integer value) {
		for (IPVersion ipVersion : IPVersion.values()) {
			if (value.equals(ipVersion.getValue())) {
				return ipVersion;
			}
		}

		throw new IllegalArgumentException("not such IPVersion for value " + value);
	}

}
