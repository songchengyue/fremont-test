/*
 * XML Type:  getDiscoveryJobsNamesResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML getDiscoveryJobsNamesResponse(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class GetDiscoveryJobsNamesResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesResponse
{
    
    public GetDiscoveryJobsNamesResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STRLIST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "strList");
    
    
    /**
     * Gets the "strList" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList getStrList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(STRLIST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "strList" element
     */
    public void setStrList(com.hp.schemas.ucmdb._1.types.StrList strList)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(STRLIST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(STRLIST$0);
            }
            target.set(strList);
        }
    }
    
    /**
     * Appends and returns a new empty "strList" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList addNewStrList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(STRLIST$0);
            return target;
        }
    }
}
