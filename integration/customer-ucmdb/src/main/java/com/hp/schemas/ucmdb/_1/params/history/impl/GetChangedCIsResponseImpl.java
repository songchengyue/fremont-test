/*
 * XML Type:  getChangedCIsResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/history
 * Java type: com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.history.impl;
/**
 * An XML getChangedCIsResponse(@http://schemas.hp.com/ucmdb/1/params/history).
 *
 * This is a complex type.
 */
public class GetChangedCIsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.history.GetChangedCIsResponse
{
    
    public GetChangedCIsResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHANGEDATAINFO$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/history", "changeDataInfo");
    
    
    /**
     * Gets array of all "changeDataInfo" elements
     */
    public com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo[] getChangeDataInfoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CHANGEDATAINFO$0, targetList);
            com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo[] result = new com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "changeDataInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo getChangeDataInfoArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo)get_store().find_element_user(CHANGEDATAINFO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "changeDataInfo" element
     */
    public int sizeOfChangeDataInfoArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CHANGEDATAINFO$0);
        }
    }
    
    /**
     * Sets array of all "changeDataInfo" element
     */
    public void setChangeDataInfoArray(com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo[] changeDataInfoArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(changeDataInfoArray, CHANGEDATAINFO$0);
        }
    }
    
    /**
     * Sets ith "changeDataInfo" element
     */
    public void setChangeDataInfoArray(int i, com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo changeDataInfo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo)get_store().find_element_user(CHANGEDATAINFO$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(changeDataInfo);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "changeDataInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo insertNewChangeDataInfo(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo)get_store().insert_element_user(CHANGEDATAINFO$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "changeDataInfo" element
     */
    public com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo addNewChangeDataInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo target = null;
            target = (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo)get_store().add_element_user(CHANGEDATAINFO$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "changeDataInfo" element
     */
    public void removeChangeDataInfo(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CHANGEDATAINFO$0, i);
        }
    }
}
