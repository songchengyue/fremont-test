package CMS_OLSS_CUSTACC;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_OLSS_CUSTACC_DB;
use AUTH;
our @ISA = qw (Exporter);

our $error;

sub start {
  
  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} =~ /cancel/i))) {

    # default to search customer login account
    CMS::clear_html_values();
    $CMS::html_values{subsystem}{value} = "olsscustacc";
    $CMS::html_values{command}{value} = "search_olsscustacc";
    
  }

  if (defined $CMS::html_values{stage}{value}) {
    unless (CMS::check_screen($CMS::html_values{current_screen}{value})){
      $CMS::html_values{stage}{value}--;
    }
  }

  if($CMS::html_values{command}{value} eq "add_olsscustacc") {
	# add OLSS Customer Login Account
       my $loginid = $CMS::html_values{'username'}{value};
       my $return_result1 = AUTH::get_superuser($loginid);
       
       #CMS::add_error("return result is $return_result1 and loginid is $loginid");
       if (!$return_result1){ 
       CMS::add_error("User $loginid not authorized to add OLSS account. Please contact C3 team.");
       $CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
       CMS::output_html("olsscustacc_search");
       } 
	elsif (not defined ($CMS::html_values{stage}{value})) {
		$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
		CMS::output_html("olsscustacc_add");
	} elsif ($CMS::html_values{stage}{value} == 2) {
		$CMS::html_values{loginid}{value} =~ tr/A-Z/a-z/;
		$CMS::html_values{loginid}{value} =~ s/ //g;
		if ($CMS::html_values{loginid}{value} eq "" ) {
			# Failed to add account
			CMS::add_error("Failed to add customer login account.");
			CMS::add_error("Login username is required");
			$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
			CMS::output_html("olsscustacc_add");
		} elsif ($CMS::html_values{loginid}{value} =~ /[^\w\-\@\.\&]/) {
			# Failed to add account
			CMS::add_error("Failed to add customer login account.");
			CMS::add_error("Invalid login username");
			$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
			CMS::output_html("olsscustacc_add");
		} elsif (($CMS::html_values{firstname}{value} eq "" ) || ($CMS::html_values{lastname}{value} eq "" )) {
			# Failed to add account
			CMS::add_error("Failed to add customer login account.");
			CMS::add_error("First name & Last name are required");
			$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
			CMS::output_html("olsscustacc_add");
		} elsif ($CMS::html_values{company}{value} eq "" ) {
			# Failed to add account
			CMS::add_error("Failed to add customer login account.");
			CMS::add_error("Company name is required");
			$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
			CMS::output_html("olsscustacc_add");
		} elsif ($CMS::html_values{accno_access}{value} eq "" ) {
			# Failed to add account
			CMS::add_error("Failed to add customer login account.");
			CMS::add_error("Account number to be accessed is required");
			$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
			CMS::output_html("olsscustacc_add");
		} else {
			my $return_result = CMS_OLSS_CUSTACC_DB::add_account
			($CMS::html_values{loginid}{value},
			 $CMS::html_values{firstname}{value},
			 $CMS::html_values{lastname}{value},
			 $CMS::html_values{company}{value},
			 $CMS::html_values{division}{value},
			 $CMS::html_values{phone}{value},
			 $CMS::html_values{email}{value},
			 $CMS::html_values{accno_access}{value},
			 $CMS::html_values{remark}{value});

			if ($return_result > 0) {
				$CMS::html_values{accno_access}{value} = $CMS::html_values{accno_access}{value};
				$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
				CMS::add_error("OLSS Customer Login Account added successfully");
				CMS::output_html("olsscustacc_add");
			} else {
				# Failed to add account
				CMS::add_error("Failed to add customer login account.");
				CMS::add_error("$return_result");
				CMS::add_error($CMS_OLSS_CUSTACC_DB::error);
				$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
				CMS::output_html("olsscustacc_add");
			}
		}
	}
  } elsif ($CMS::html_values{command}{value} eq "edit_olsscustacc") {
       my $loginid = $CMS::html_values{'username'}{value};
       my $return_result1 = AUTH::get_superuser($loginid);

       #CMS::add_error("return result is $return_result1 and loginid is $loginid");
       if (!$return_result1){
       CMS::add_error("User $loginid not authorized to edit OLSS account. Please contact C3 team.");
       $CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
       CMS::output_html("olsscustacc_search");
	}
	elsif (not defined ($CMS::html_values{stage}{value})) {
		CMS::output_html("olsscustacc_edit");
	} elsif ($CMS::html_values{stage}{value} == 2) {
		if ($CMS::html_values{loginid}{value} eq "" ) {
			# Failed to edit account
			CMS::add_error("Failed to edit customer login account.");
			CMS::add_error("Login username is required");
			CMS::output_html("olsscustacc_edit");
		} else {
			$CMS::html_values{loginid}{value} =~ tr/A-Z/a-z/;
			if (my $result = CMS_OLSS_CUSTACC_DB::get_account($CMS::html_values{loginid}{value})) {
				($CMS::html_values{firstname}{value},
				 $CMS::html_values{lastname}{value},
				 $CMS::html_values{company}{value},
				 $CMS::html_values{division}{value},
				 $CMS::html_values{phone}{value},
				 $CMS::html_values{email}{value},
				 $CMS::html_values{remark}{value},
				 $CMS::html_values{accno_access}{value}
				) = @$result;

				$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
				CMS::output_html("olsscustacc_edit2");
			} else {
				# Login username does not exist
				CMS::add_error("Failed to edit customer login account.");
				CMS::add_error("Login username does not exist.");
				CMS::output_html("olsscustacc_edit");
			}
		}
	} elsif ($CMS::html_values{stage}{value} == 3) {
		if (($CMS::html_values{firstname}{value} eq "" ) || ($CMS::html_values{lastname}{value} eq "" )) {
			# Failed to edit account
			CMS::add_error("Failed to edit customer login account.");
			CMS::add_error("First name & Last name are required");
			$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
			CMS::output_html("olsscustacc_edit2");
		} elsif ($CMS::html_values{company}{value} eq "" ) {
			# Failed to edit account
			CMS::add_error("Failed to edit customer login account.");
			CMS::add_error("Company name is required");
			$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
			CMS::output_html("olsscustacc_edit2");
		} elsif ($CMS::html_values{accno_access}{value} eq "" ) {
			# Failed to edit account
			CMS::add_error("Failed to edit customer login account.");
			CMS::add_error("Account number to be accessed is required");
			$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
			CMS::output_html("olsscustacc_edit2");
		} else {
			my $return_result = CMS_OLSS_CUSTACC_DB::update_account
			($CMS::html_values{loginid}{value},
			 $CMS::html_values{firstname}{value},
			 $CMS::html_values{lastname}{value},
			 $CMS::html_values{company}{value},
			 $CMS::html_values{division}{value},
			 $CMS::html_values{phone}{value},
			 $CMS::html_values{email}{value},
			 $CMS::html_values{accno_access}{value},
			 $CMS::html_values{remark}{value},
			 $CMS::html_values{resetpassword}{value});

			if ($return_result > 0) {
				CMS::add_error("OLSS Customer Login Account update successfully");
				CMS::output_html("olsscustacc_edit");
			} else {
				# Failed to edit account
				CMS::add_error("Failed to edit customer login account.");
				CMS::add_error("$return_result");
				CMS::add_error($CMS_OLSS_CUSTACC_DB::error);
				$CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
				CMS::output_html("olsscustacc_edit2");
			}
		}
	}
  } elsif ($CMS::html_values{command}{value} eq "delete_olsscustacc") {
	# delete OLSS Customer Login Account
       my $loginid = $CMS::html_values{'username'}{value};
       my $return_result1 = AUTH::get_superuser($loginid);

       #CMS::add_error("return result is $return_result1 and loginid is $loginid");
       if (!$return_result1){
       CMS::add_error("User $loginid not authorized to delete OLSS account. Please contact C3 team.");
       $CMS::html_values{accno_access}{options} = CMS_OLSS_CUSTACC_DB::get_customer_accno();
       CMS::output_html("olsscustacc_search");
       }
     elsif (not defined ($CMS::html_values{stage}{value})) {	
		CMS::output_html("olsscustacc_delete");
	} elsif ($CMS::html_values{stage}{value} == 2) {
		if ($CMS::html_values{loginid}{value} eq "" ) {
			# Failed to delete account
			CMS::add_error("Failed to delete customer login account.");
			CMS::add_error("Login username is required");
			CMS::output_html("olsscustacc_delete");
		} else {
			$CMS::html_values{loginid}{value} =~ tr/A-Z/a-z/;
			my $return_result = CMS_OLSS_CUSTACC_DB::delete_account($CMS::html_values{loginid}{value});
			if ($return_result > 0) {
				CMS::add_error("OLSS Customer Login Account deleted successfully");
				CMS::output_html("olsscustacc_delete");
			} else {
				# Failed to delete account
				CMS::add_error("Failed to delete customer login account.");
				CMS::add_error("$return_result");
				CMS::add_error($CMS_OLSS_CUSTACC_DB::error);
				CMS::output_html("olsscustacc_delete");
			}
		}
	}
  } elsif ($CMS::html_values{command}{value} eq "search_olsscustacc") {
	if ((defined $CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} eq "Search")) {
		$CMS::html_values{olss_custacc}{table}
		      = CMS_OLSS_CUSTACC_DB::search_account
			($CMS::html_values{search_loginid}{value},
			 $CMS::html_values{search_firstname}{value},
			 $CMS::html_values{search_lastname}{value},
			 $CMS::html_values{search_company}{value},
			 $CMS::html_values{search_accno_access}{value});

		unshift (@{$CMS::html_values{olss_custacc}{table}},
		["Login username", "First name", "Last name", "Company", "Division", "Account number can be accessed"]);
      
		$CMS::html_values{olss_custacc}{header_rows} = 1;

	} else {
		$CMS::html_values{search_loginid}{value} = "";
		$CMS::html_values{search_firstname}{value} = "";
		$CMS::html_values{search_lastname}{value} = "";
		$CMS::html_values{search_company}{value} = "";
		$CMS::html_values{search_accno_access}{value} = "";
	}
	CMS::output_html("olsscustacc_search");
  }
}

1;
