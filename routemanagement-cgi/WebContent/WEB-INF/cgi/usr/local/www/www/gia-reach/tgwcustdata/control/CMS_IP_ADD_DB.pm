package CMS_IP_ADD_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;

# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;
use Data::Dumper;

# Header
#print "Content-Type: text/plain\r\n\r\n";

use CMS_DB;
use CMS_ROUTING_QUEUE_DB;

our $error;
our $ip_status = 1;
our $sub_net_mask = "";
our $inet_val = "";
#our $serviceid_val;

sub add_ipadd {
	CMS_DB::begin_transaction();
	my $ip_block_raw = shift @_;
	my $serviceid_raw = shift @_;
	my $ip_block = $dbh_tr->quote($ip_block_raw);
	my $serviceid = $dbh_tr->quote($serviceid_raw);
	my $dns_service = $dbh_tr->quote(shift @_);
	my $ntp_service = $dbh_tr->quote(shift @_);
	my $remark = $dbh_tr->quote(shift @_);

	my $netname = $dbh_tr->quote(shift @_);
	my $hosts_now = $dbh_tr->quote(shift @_);
	my $hosts_in_6_months = $dbh_tr->quote(shift @_);
	my $hosts_in_1_year = $dbh_tr->quote(shift @_);
	my $subnets_now = $dbh_tr->quote(shift @_);
	my $subnets_in_6_months = $dbh_tr->quote(shift @_);
	my $subnets_in_1_year = $dbh_tr->quote(shift @_);

	my $descr = $dbh_tr->quote(shift @_);
	my $admin_c = $dbh_tr->quote(shift @_);
	my $tech_c = $dbh_tr->quote(shift @_);
	my $rev_srv = $dbh_tr->quote(shift @_);
	my $status = $dbh_tr->quote(shift @_);
	my $apnic_remarks = $dbh_tr->quote(shift @_);
	my $notify = $dbh_tr->quote(shift @_);
	my $mnt_by = $dbh_tr->quote(shift @_);
	my $mnt_lower = $dbh_tr->quote(shift @_);
	my $mnt_routes = $dbh_tr->quote(shift @_);
	my $source = $dbh_tr->quote(shift @_);

	my $date_assigned = 'CURRENT_TIMESTAMP';
	my $date_last_update = 'CURRENT_TIMESTAMP';

	my $inetnum = "";
	my $accno = "";
	my $new_ip_flag = 0;
	my $accno1 = "";
	my ($r, $sth);


open (DEBUG, ">/tmp/add_ipadd");

        # check for whether the accno exist in service with given serviceid
        if ($serviceid_raw ne '') {
                my $sth = $dbh_tr->prepare ("SELECT serviceid FROM service WHERE serviceid ilike $serviceid");
                $sth->execute();
                my $r = $sth->fetchrow_hashref;
                my $temp_serviceid = $serviceid;
                $temp_serviceid =~ s/\'//g;
                if($$r{serviceid} !~ /^$temp_serviceid$/i) {
print DEBUG "Service ID does not exist.";
                        return "Service ID does not exist.";
                } else {

	#check for accno for given serviceid from service
        my $sth2 = $dbh_tr->prepare ("SELECT accno FROM service WHERE serviceid like $serviceid");
        $sth2->execute();
        my $success = $sth2->fetchrow_hashref;
        $accno = "\'$$success{accno}\'";
print DEBUG "accno:  $accno\n";
		}

print DEBUG "CHECK for Accno based on Service ID from service COMPLETE\n";
        }



print DEBUG "ACCNO: $accno  IP: $ip_block  SERVICE: $serviceid \n";


        # check for whether the ip_block and service id  exist or not
        if ($ip_block ne '') {

     $sth = $dbh_tr->prepare ("SELECT count(ip_block) FROM ip_resource WHERE ip_block like $ip_block AND ip_status = '1'");
     $sth->execute();
     $r = $sth->fetchall_arrayref;
print DEBUG "COUNT: $r->[0]->[0]\n";
     if($r->[0]->[0] >= 1) {
print DEBUG "IP-Block EXISTS\n\n";



	#ip_block exists so check serviceid for same
        #my $sth1 = $dbh_tr->prepare ("SELECT count(serviceid) from ip_resource WHERE ip_block like $ip_block and serviceid like $serviceid and ip_status = '1'");
        #$sth1->execute();
        #my $res1 = $sth1->fetchall_arrayref;
	#my $x = $res1->[0]->[0];
	#if ($x >= 1) {
	#print DEBUG "Combination of  Serviceid + IP_BLOCK exists.\n serviceid: $serviceid\n For Loop: $res1->[0]->[0]\n\n";
	#return "Combination of  Serviceid + IP_BLOCK exists.";
	#}
		#check for accno for given ip_block from ip_resource
        	$sth = $dbh_tr->prepare ("SELECT distinct accno FROM ip_resource WHERE ip_block like $ip_block AND ip_status = '1'");
        	$sth->execute();
        	my $success = $sth->fetchall_arrayref;
		$accno1 = $success->[0]->[0];
        	$accno1 = "\'$accno1\'";
print DEBUG "New serviceid +ip-block ($ip_block) :  accno1: $accno1 accno : $accno\n";

      } else {

print DEBUG "INSIDE add_ipadd - BEFORE FLAG set: $new_ip_flag\n";
	#Set Flag to Go forward with INSERT for new-ip_block
	$new_ip_flag = 1;
print DEBUG "INSIDE add_ipadd - AFTER FLAG set: $new_ip_flag\n";

        }
      }
print DEBUG "BEFORE if loop: accno1: $accno1 accno : $accno new_ip_flag: $new_ip_flag\n";
 if ( ($accno eq $accno1) || ($new_ip_flag == 1) ) {

print DEBUG "ENTERED if loop: accno1: $accno1 accno : $accno new_ip_flag: $new_ip_flag\n";
	my $success = 1;

        $sth = $dbh_tr->prepare("DELETE FROM ip_resource WHERE ip_block = $ip_block AND serviceid like $serviceid AND ip_status = 2");
        $success &&= $sth->execute;
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

        my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

	my $sth2 = "";
	my $r2 = "";
	my $sth3 = "";
	my $r3 = "";
	my $input_ip = "";
	my $x = "";
	my $y = "";
	my $z1 = "";
	my $z2 = "";
	my @z1_octets = "";
	my $z1_octet1 = "";
	my $z1_octet2 = "";
	my $z1_octet3 = "";
	my $z1_octet4 = "";
	my $z1_bin_1 = "";
	my $z1_bin_2 = "";
	my $z1_bin_3 = "";
	my $z1_bin_4 = "";
	my @z1_binary = "";
	my $z1_binary_stat = "";
	my @z1_binary_bit = "";
	my @ip_octets = "";
        my $ip_octet1 = "";
        my $ip_octet2 = "";
        my $ip_octet3 = "";
        my $ip_octet4 = "";
        my $ip_bin_1 = "";
        my $ip_bin_2 = "";
        my $ip_bin_3 = "";
        my $ip_bin_4 = "";
        my $r_octets = "";
 	my $r_octet1 = "";
        my $r_octet2 = "";
        my $r_octet3 = "";
        my $r_octet4 = "";
        my $r_bin_1 = "";
        my $r_bin_2 = "";
        my $r_bin_3 = "";
        my $r_bin_4 = "";
        my @r_binary = "";
        my $r_binary_stat = "";
        my @r_binary_bit = "";	
	my $rip_new ="";
	my $rip_prefix = "";
	my $Ar1 = "";
	my @ip_binary = "";
        my $ip_binary_stat = "";
        my @ip_binary_bit = "";
	my $ip_new = "";
	my $ipprefix = "";
	my $Ar = "";	
	my $ip_length = "";
	my $ip_binary_result = "";
	my $z1_binary_result = "";
	my $r_binary_result = "";
	my $compare_flag = "";
	my $loop_count2 = "";
 	my $loop_count = "";	
	my $ip_db_block = "";
	my $db_block = "";
	my $reach_flag = "";
	my $loop_count20 = "";
	my $m = "";
	my $lst1 = "";
	my $lst2 = "";
	my $lst3 = "";
	my $lst4 = "";
	my @Chk_Ar = "";
	my @g = "";
	my $k1 = "";
	my $k2 = "";
	my @amat = "";
	my $compare_flag2 = "";

	$sth2 = $dbh_tr->prepare ("SELECT ip_block FROM ip_resource WHERE ip_status = '1'");
        $sth2->execute();
        $r2 = $sth2->fetchall_arrayref;
	$sth3 = $dbh_tr->prepare ("SELECT ip_block FROM reach_ip_block");
        $sth3->execute();
        $r3 = $sth3->fetchall_arrayref;
	$x = $ip_block;
	$x =~ s/'//;
 	$x =~ s/\\//;	
	my @y = split(/\//,$x);
	$z1= shift(@y);#z1 has ip
	$z2= shift(@y);#z2 has prefix
 	$z2 =~ s/'//;	
	#$resource_index = $#Ar;
	#code to split the IPs into octets and changing octets to binary.
	@z1_octets = split(/\./,$z1);
	$z1_octet1 = shift(@z1_octets);
	$z1_octet2 = shift(@z1_octets);
	$z1_octet3 = shift(@z1_octets);
	$z1_octet4 = shift(@z1_octets);

if ($z2 == 24) { 

my @Chk_Ar = ("192.0.0.0/24", "192.0.2.0/24", "223.255.255.0/24", "192.168.1.0/24");
foreach my $i (0..$#Chk_Ar)
{
my $Chk = shift(@Chk_Ar);
my @g = split(/\//,$Chk);
my $k1= shift(@g);
my $k2=  shift(@g);
my @amat = split(/\./,$k1);
my $lst1 = shift(@amat);
my $lst2 = shift(@amat);
my $lst3 = shift(@amat);
my $lst4 = shift(@amat);

if($z1_octet1 == $lst1 && $z1_octet2 == $lst2 && $z1_octet3 == $lst3 && $z1_octet4 == $lst4) {
return "IP Block Entered belongs to Private/Experimental Ip block ranges."; 
}
}}

if ( $z2 > 24) {
#SUBNET MASK WHEN LAST OCTET > 24
my $diff_prefix = $z2-24;
my $h = 2**($diff_prefix);
my $div_val = 256/$h;
my $last_octet = $div_val * ($h-1);
$sub_net_mask = "\'255.255.255.$last_octet\'";
} else {
$sub_net_mask = "\'255.255.255.0\'";
}

	$z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
	$z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
	$z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
	$z1_bin_4 = unpack("B8", pack('C',$z1_octet4));
if($z1_octet1 < 0 || $z1_octet1 >255) {
	return "Invalid IP. IP octets must be less than 255 or greater than 0.";}
elsif($z1_octet2 < 0 || $z1_octet2 >255) {
return "Invalid IP. IP octets must be less than 255 or greater than 0.";}
elsif($z1_octet3 < 0 || $z1_octet3 >255) {
return "Invalid IP. IP octets must be less than 255 or greater than 0.";}
elsif($z1_octet4 < 0 || $z1_octet4 >255) {
return "Invalid IP. IP octets must be less than 255 or greater than 0.";}
elsif($z2 < 24 || $z2 > 32) {
return "Invalid IP. Prefix length must be between 24 and 32.";}
#combining the individual IPs into one variable/array
@z1_binary = ();
push(@z1_binary,$z1_bin_1);
push(@z1_binary,$z1_bin_2);
push(@z1_binary,$z1_bin_3);
push(@z1_binary,$z1_bin_4);
$z1_binary_stat = join("",@z1_binary);
@z1_binary_bit = split(//,$z1_binary_stat);
#print DEBUG "input is @z1_binary_bit\n";
#$reach_flag = 0;
$compare_flag2 = 1;
#check if IP Add lies with in Reach IP Block
for my $loop_count10 (0 .. $#{$r3}) {
$Ar1 = $r3->[$loop_count10]->[0];
$db_block = $Ar1;
#print DEBUG "ip from REACH is $db_block";
$reach_flag = 0;
my @rip = split(/\//,$db_block);
my $ripnew  = shift(@rip);#ipnew has reach ip
my $ripprefix = shift(@rip);#has reach ip prefix
my @r_octets = split(/\./,$ripnew);
#code to split the stored IPs into octets and changing them to binary
$r_octet1 = shift(@r_octets);
$r_octet2 = shift(@r_octets);
$r_octet3 = shift(@r_octets);
$r_octet4 = shift(@r_octets);
$r_bin_1 = unpack("B8", pack('C',$r_octet1));
$r_bin_2 = unpack("B8", pack('C',$r_octet2));
$r_bin_3 = unpack("B8", pack('C',$r_octet3));
$r_bin_4 = unpack("B8", pack('C',$r_octet4));

#combining individual IPs into one variable/array
my @r_binary = ();
push(@r_binary, $r_bin_1);
push(@r_binary, $r_bin_2);
push(@r_binary, $r_bin_3);
push(@r_binary, $r_bin_4);
$r_binary_stat = join("",@r_binary);
@r_binary_bit = split(//,$r_binary_stat);
#print DEBUG "\n Binary bit from reach is @r_binary_bit\n";
if($z2 > $ripprefix){$ip_length = $ripprefix;}
else {$ip_length = $z2;}
#print DEBUG "prefix for reach comparision is $ip_length \n";
for ($loop_count20 = 0; $loop_count20 < $ip_length;++$loop_count20){
      if ($reach_flag == 0) {  
        $z1_binary_result = "";
	$r_binary_result = "";
	$z1_binary_result= $z1_binary_bit[$loop_count20] & 1;
        $r_binary_result= $r_binary_bit[$loop_count20] & 1;
        if ($z1_binary_result != $r_binary_result){
                $reach_flag = 1;
                }
	elsif ($z1_binary_result == $r_binary_result) {
		$reach_flag = 0;
		}
}
#print DEBUG "\n FLAG VALUE +++++++++++++++++ $reach_flag";
 }
if($reach_flag == 1 && $compare_flag2 ==1) {$compare_flag2 = 1;}#end of both if loops and for loop
else {$compare_flag2 = 0;}
}
if ($compare_flag2 == 1) {
return "Entered IP is not a sub range of IPs in Reach IP Block\n";
}
#check with IP_Resource DB
for my $loop_count (0 .. $#{$r2}) {
$Ar = $r2->[$loop_count]->[0];
$ip_db_block = $Ar;
#print DEBUG "ip from db is $ip_db_block";
my @ip = split(/\//,$ip_db_block);
my $ipnew  = shift(@ip);#ipnew has ip
$ipprefix = shift(@ip);#has prefix
if($ipnew != $z1){
my @ip_octets = split(/\./,$ipnew);
#code to split the stored IPs into octets and changing them to binary
$ip_octet1 = shift(@ip_octets);
$ip_octet2 = shift(@ip_octets);
$ip_octet3 = shift(@ip_octets);
$ip_octet4 = shift(@ip_octets);
$ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
$ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
$ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
$ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

#combining individual IPs into one variable/array
my @ip_binary = ();
push(@ip_binary, $ip_bin_1);
push(@ip_binary, $ip_bin_2);
push(@ip_binary, $ip_bin_3);
push(@ip_binary, $ip_bin_4);
$ip_binary_stat = join("",@ip_binary);
@ip_binary_bit = split(//,$ip_binary_stat);
#print"ip binary is @ip_binary_bit\n";

if($z2 > $ipprefix){$ip_length = $ipprefix;}
else {$ip_length = $z2;}
$compare_flag = 0;
for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
        if ($compare_flag == 0){
        $z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
        $ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
        if ($z1_binary_result != $ip_binary_result){
                $compare_flag = 1;
                }} }#end of both if loops and for loop
if($compare_flag == 0 && $ip_length != $z2) {
        return "Entered ip is a subnet of a stored ip\n";
}
elsif($compare_flag == 0 && $ip_length == $z2) {
return "Entered IP is partially used. Please check utilization and enter a new IP.\n";
}	
 }
}
# end of changes
close (DEBUG);
            open (DEBUG, ">/tmp/add_ipadd");
	    my @y = "";
            my $z1= "";
            my $z2= "";
            my $comb = "";
            my @amatch = "";
            my $last = "";
            my $len_last = "";
            my $ri2 = "";
            my $y1 = "";
            my $y2 = "";
            my $ri = "";
            my $val_ip = "";
            my @comb_ar = "";
            my $ti = "";
            my $match_ip = "";
            my $addend = "";
	    $ip_block =~ s/'//g;
            @y = split(/\//,$ip_block);
            #print DEBUG "Y: @y \n";
            $z1= shift(@y);
            $z2= shift(@y);
            $comb = 2**(32-$z2) -1;
            #print DEBUG "z1: $z1 \t\tz2: $z2 \t comb: $comb\n";
	    @amatch = split(/\./,$z1);
            $last = pop(@amatch);
            if($comb <= 255 && $comb > 1) {
            $len_last = length($last);
            $ri2 = rindex($z1,".");
            $ri2 = $ri2 + 1;
            #print DEBUG "amatch: @amatch z1: $z1\t ri2: $ri2\t length: $len_last\t last: $last\n\n";
            my @final= ();
            for (my $count=1;$count <= $comb;$count++) {
                  #substr($z1,$ri2,$len_last,$last);
		  my $new_ti = rindex($z1,"."); 
		  my $new_matchip = substr($z1,0,$new_ti); 
		  my $new_addend = $new_matchip.".$last";
		  $last = $last + 1;
                  push(@final,$new_addend);
            }
	    #print DEBUG "FINAL ARRAY: @final\n";

	      my $y1 = shift(@final);
              		  
	      if ($last < 256) {
               $ti = rindex($y1,".");
               $match_ip  = substr($y1,0,$ti);
               $addend = $last;
               $y2 = $match_ip.".$addend";
		#print DEBUG "\n Comb greater than ZERO: ti: $ti \t match_ip: $match_ip \t addend: $addend\n"; 
	      } else {
   	       $ti = rindex($y1,".");
	       $match_ip  = substr($y1,0,$ti);	
	       $y2 = $match_ip.'.255';
	      } 	
            my @comb_ar = ($y1,$y2);
            #print DEBUG "\n Not Inside 255 LOop y1: $y1  y2: $y2\n";
            $inet_val = join(" - ",@comb_ar);
	    $inetnum = "\'$inet_val\'";
	    #print DEBUG "\n\n IP: $z1\t WHEN < 255 - inet_val: $inet_val\n\n";
            } elsif ($comb > 255) {
            my $ri = rindex($ip_block,".");
            my $val_ip = substr($ip_block,0,$ri);
            $y2 = $val_ip.'.255';
            #print DEBUG " Inside 255 LOop y1: $z1  y2: $y2\n";
            @comb_ar = ($z1,$y2);
            $inet_val = join(" - ",@comb_ar);
	    $inetnum = "\'$inet_val\'";
	    #print DEBUG "\n\n WHEN > 255 - inet_val: $inet_val\n\n";
            } elsif ($comb == 0) {
	    $inet_val = $z1;
	    $inetnum = "\'$inet_val\'";
	    #print DEBUG "\n\n When ZERO: inet_val: $inet_val\n\n";
	    } elsif ($comb == 1) {
	    my $ri = rindex($ip_block,".");
            my $val_ip = substr($ip_block,0,$ri);
	    $addend = $last+1;
	    $y2 = $val_ip.".$addend";
	    @comb_ar = ($z1,$y2);
	    $inet_val = join(" - ",@comb_ar); 
            $inetnum = "\'$inet_val\'";
	    #print DEBUG "\n\n When COMB = 1: inet_val: $inet_val\n\n";
	    }
            close (DEBUG);

	$sth = $dbh_tr->prepare ("SELECT distinct d.countryname from GIA_countryname d,cityname c,  popname p, routername r, link l,service i where i.serviceid = l.serviceid and r.routerid = l.routerid and p.popid = r.popid and c.cityid = p.cityid and d.cityid = c.cityid and i.serviceid ilike $serviceid");
	$sth->execute();
	my $cntry = $sth->fetchall_arrayref;
        my $country = "\'$cntry->[0]->[0]\'";

	$ip_status = '1';
	$ip_status = "\'$ip_status\'";
	my $mask = $sub_net_mask;
	my $inet_num_name = $netname;
	$ip_block = "\'$ip_block\'";
	my $changed = "ipNOC\@reach.com ";
	$changed = "\'$changed\'";
        my $address = $z1;
        $address = "\'$address\'";

	my $temp_sql = "INSERT INTO ip_resource (
		ip_block, accno, serviceid, ip_status, dns_service, ntp_service, remark
		,address,  netname,mask, hosts_now, hosts_in_6_months, hosts_in_1_year, subnets_now, subnets_in_6_months, subnets_in_1_year
		, inetnum, inet_num_name, descr, country, admin_c, tech_c, rev_srv, status, apnic_remarks, notify, mnt_by, mnt_lower, mnt_routes, changed, source
		, date_assigned, date_last_update
		) VALUES (
		$ip_block, $accno, $serviceid, $ip_status, $dns_service, $ntp_service, $remark
		, $address, $netname,$mask, $hosts_now, $hosts_in_6_months, $hosts_in_1_year, $subnets_now, $subnets_in_6_months, $subnets_in_1_year
		, $inetnum, $inet_num_name, $descr, $country, $admin_c, $tech_c, $rev_srv, $status, $apnic_remarks, $notify, $mnt_by, $mnt_lower, $mnt_routes, $changed, $source
		, $date_assigned, $date_last_update
		) ";

open (DEBUG, ">/tmp/add_ipadd");
print DEBUG "dateassn: $date_assigned dateupd: $date_last_update country $country \n\n";
print DEBUG "prepare: $temp_sql\n";
close (DEBUG);

	$success = 1;

	$sth = $dbh_tr->prepare($temp_sql);
	$success &&= $sth->execute;
	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
	
	my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
	CMS_DB::disconnect_from_transaction();
	return $error if ($error);
	return $result;

} else {

 $error = "Entered Service ID $serviceid is having different account number $accno for the IP_BLOCK $ip_block.";
        return $error;
}

}


sub get_date_current {
        my $ip_block = $dbh->quote(shift @_);
	my $date_last_update = 'CURRENT_TIMESTAMP';

	CMS_DB::begin_transaction();
	my $success = 1;

        my $sth = $dbh_tr->prepare
	("UPDATE ip_resource
	SET date_last_update = $date_last_update
	WHERE ip_block = $ip_block ");

        $success &&= $sth->execute;
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

        my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
        CMS_DB::disconnect_from_transaction();


        $sth = $dbh->prepare ("SELECT distinct date_last_update FROM ip_resource WHERE ip_block ilike $ip_block  AND ip_status = 1");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


sub get_date_assigned {
        my $ip_block = $dbh->quote(shift @_);

        CMS_DB::begin_transaction();
        my $success = 1;
        my $sth = $dbh->prepare ("SELECT distinct date_last_update FROM ip_resource WHERE ip_block ilike $ip_block  AND ip_status = 1");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

open (DEBUG, ">/tmp/h999755_ipedit");
sub edit_ip_existval {
        my $ip_block = $dbh->quote(shift @_);
        my $serviceid = $dbh->quote(shift @_);
print DEBUG "INSIDE edit_ip_existval \t \t serviceid: $serviceid \t ip_block: $ip_block\n";
        my $serviceid_val = $serviceid;
 print DEBUG "INSIDE edit_ip_existval serviceid_val: $serviceid_val\n";

        if ($serviceid_val ne '') {
        CMS_DB::begin_transaction();
        my $temp_sql = "INSERT INTO temp_service_val (service_id,status) VALUES ($serviceid_val,'1')";

print DEBUG "INSIDE edit_ip_existval  prepare: $temp_sql\n";
        my $success = 1;

        my $sth1 = $dbh_tr->prepare($temp_sql);

        $success = $sth1->execute;
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

        my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
        CMS_DB::disconnect_from_transaction();
}
        my $sth = $dbh->prepare ("SELECT accno,dns_service,ntp_service,remark,netname,mask,hosts_now,hosts_in_6_months,hosts_in_1_year,subnets_now,subnets_in_6_months,subnets_in_1_year,inetnum,inet_num_name,descr,country,admin_c,tech_c,rev_srv,status,apnic_remarks,notify,mnt_by,mnt_lower,mnt_routes,changed||TO_CHAR(date_assigned, 'YYYYMMDD') AS date_assigned,source,TO_CHAR(date_assigned, 'YYYYMMDD') AS date_assigned,TO_CHAR(date_last_update, 'YYYYMMDD') AS date_last_update  FROM ip_resource WHERE ip_block ilike $ip_block  AND serviceid ilike $serviceid AND ip_status = 1");

        $sth->execute;
        my $result = $sth->fetchrow_arrayref;
                return ($result);
close (DEBUG);
}



open (DEBUG, ">/tmp/h999755_ipedit");
sub get_ipadd {
	my $ip_block = $dbh->quote(shift @_);
	my $serviceid = $dbh->quote(shift @_);
print DEBUG "INSIDE get_ipadd \t \t serviceid: $serviceid \t ip_block: $ip_block\n";

#	my $sth = $dbh->prepare ("
#	SELECT
#	accno,dns_service,ntp_service,remark
#	,netname,mask,hosts_now,hosts_in_6_months,hosts_in_1_year,subnets_now,subnets_in_6_months,subnets_in_1_year
#	,inetnum,inet_num_name,descr,country,admin_c,tech_c,rev_srv,status,apnic_remarks,notify,mnt_by,mnt_lower,mnt_routes,changed||TO_CHAR(date_assigned, 'YYYYMMDD') AS date_assigned,source
#	,TO_CHAR(date_assigned, 'YYYYMMDD') AS date_assigned,TO_CHAR(date_last_update, 'YYYYMMDD') AS date_last_update
#	FROM ip_resource
#	WHERE ip_block ilike $ip_block  AND serviceid ilike $serviceid AND ip_status = 1
#	");

	my $sth = $dbh->prepare ("SELECT accno,dns_service,ntp_service,remark,netname,mask,hosts_now,hosts_in_6_months,hosts_in_1_year,subnets_now,subnets_in_6_months,subnets_in_1_year,inetnum,inet_num_name,descr,country,admin_c,tech_c,rev_srv,status,apnic_remarks,notify,mnt_by,mnt_lower,mnt_routes,changed||TO_CHAR(date_assigned, 'YYYYMMDD') AS date_assigned,source,TO_CHAR(date_assigned, 'YYYYMMDD') AS date_assigned,TO_CHAR(date_last_update, 'YYYYMMDD') AS date_last_update FROM ip_resource WHERE ip_block ilike $ip_block  AND serviceid ilike $serviceid AND ip_status = 1");

	$sth->execute;
  
	my $result = $sth->fetchrow_arrayref;
print DEBUG "INSIDE get_ipadd \t result: $result\n";
	return $result;
close(DEBUG);
}

sub get_ip_block_count {
	my $ip_block = $dbh->quote(shift @_);
	my $sth = $dbh->prepare ("SELECT count(*) from ip_resource where ip_block like $ip_block and ip_status = 1");
	
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}
	
	my $result = $sth->fetchrow_arrayref;

	if ($result) {
		$error = $dbh->errstr;
		$sth->finish;
		$result = $#$result;
		 return $result;
	} else {
		$error = $dbh->errstr;
		return 0;
	}
}

# Function to update the status of Ptrrec record in reverse table when deleted from cms
sub delete_ip_block_reverse {
my $ip_block = shift @_;
chomp $ip_block;

#splitting the ip block by "." to get the ptr record
my @ip=split(/\./,$ip_block);
my $new_ip=$ip[2]."\.".$ip[1]."\.".$ip[0];
my @oct=split(/\//,$ip[3]);
my $block=$ip[3];

#calculating sub net range for Ip block
my $subnet_in=32-$oct[1];
my $subnet_fi=2**$subnet_in;

my @lat_oct;
my $type_ns;
my $type_ptr;
my @updated_rec;

#Checking if The record for given ipblock exist in reverse table
my $sth = $dbh->prepare("SELECT rec_type  from reverse where ptrrec like". " '$new_ip%'");
 unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
if (my $result = $sth->fetchall_arrayref) {
#checking if the record type is PTR or NS
      foreach(@$result){
        if ($$_[0]=~/NS/)
        {
        $type_ns="NS";
        }
        if ($$_[0]=~/PTR/)
        {
        $type_ptr="PTR";
   }
        }
        } else {
                $error = $dbh->errstr;
                return ;
        }

#If record type is PTR
if($type_ptr eq "PTR")
{
for (my $i=0;$i<$subnet_fi;$i++)
{
my $ip_exist=0;
$lat_oct[$i]=$oct[0]+$i;
#checking if record with given ptrrec and last_oct exists or not
my $sth = $dbh->prepare("SELECT ptrrec,last_oct  from reverse where ptrrec like". " '$new_ip%'". " and last_oct='$lat_oct[$i]' and status!=3");
 unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
        $ip_exist=1;  #setting  the variable if record found
        #pushing the record to be updated in an array
        push (@updated_rec,@$result);

        } else {
                $error = $dbh->errstr;
        }
#updating the status of record to be 3 if it exists
if($ip_exist)
 {
my $sth = $dbh->prepare("update reverse set status=3,request_time=current_timestamp where ptrrec like". " '$new_ip%'". " and last_oct ='$lat_oct[$i]'");
               unless ($sth->execute) {
               $error = $dbh->errstr;
        }

 }

} #End of For loop for last oct value
} #End of If block for Record type=PTR


# Checking if the record type is NS type
if($type_ns eq "NS")
{
my $ip_exist=0;
#checking if the record exist
my $sth = $dbh->prepare("SELECT ptrrec,last_oct  from reverse where ptrrec like". " '$new_ip%'". " and last_oct ='$block' and status!=3");
 unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_arrayref;

        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
        $ip_exist=1; #Setting the variable if record found
        push (@updated_rec,@$result); #pushing the record to be updated in an array

        } else {
                $error = $dbh->errstr;
                return ;
        }

#If record exist update the record
 if($ip_exist)
 {
my $sth = $dbh->prepare("update reverse set status=3,request_time=current_timestamp where ptrrec like". " '$new_ip%'". " and last_oct ='$block'");
              unless ($sth->execute) {
               $error = $dbh->errstr;
        }

 }
} # End of If block for record type=NS
return  @updated_rec;  #returning the array having all the records which are updated
} #End of function

sub get_reports_ipadd {

        my $command = "
	SELECT
	netname,address,mask,hosts_now,hosts_in_6_months,hosts_in_1_year
	,subnets_now,subnets_in_6_months,subnets_in_1_year,TO_CHAR(date_assigned, 'YYYYMMDD') AS date_assigned
	FROM ip_resource
	WHERE ip_block is not null AND ip_status = 1
	ORDER BY ip_block
	";

       my $sth = $dbh->prepare($command);
       unless ($sth->execute) {
                 $error = $dbh->errstr;
                 return 0;
       }

       my $result = $sth->fetchall_arrayref;

       if ($result) {

                 $error = $dbh->errstr;
                 $sth->finish;
                 return $result;
       } else {
                 $error = $dbh->errstr;
                 return 0;
       }

}

sub get_Urep_ipadd {

        my $command = "
        SELECT ip_block FROM reach_ip_block ORDER BY ip_block";

         my $sth = $dbh->prepare($command);
         unless ($sth->execute) {
                   $error = $dbh->errstr;
                   return 0;
         }

        my $result = $sth->fetchall_arrayref;
       if ($result) {

                 $error = $dbh->errstr;
                 $sth->finish;
                 return $result;
       } else {
                 $error = $dbh->errstr;
                 return 0;
       }
}

sub get_utilpercent {

         my $command = "
	SELECT distinct(ip_block) from ip_resource WHERE ip_status = 1 ORDER BY ip_block";

         my $sth = $dbh->prepare($command);
         unless ($sth->execute) {
                    $error = $dbh->errstr;
                    return 0;
         }

         my $result = $sth->fetchall_arrayref;

          if ($result) {

                    $error = $dbh->errstr;
                    $sth->finish;
                    return $result;
          } else {
                    $error = $dbh->errstr;
                    return 0;
          }
}

sub get_Urep_res {
&del_Ureports_ipadd("DELETE from ip_temp");
open (DEBUG, ">/tmp/urep_ipadd");
my $result = "";
$result = &get_Urep_ipadd(); 
for my $i (0 .. $#{$result}) {
my $x = $result->[$i]->[0];
#my $x = $result->[9]->[0];

my @y = split(/\//,$x);
my $z1= shift(@y);
my $z2= shift(@y);

my $X = 16;
my $n = 8;
my $z3 = $z2 - $X;
my $comb = 2**($n-$z3)-1;
my $diff = 24-$z2;
my $char = '.';
my $res = "";
my $octet_match = "";
my $ip = "";
my $ip_util = "";
my $val_match = "";
my @amatch = "";
my $last = "";
my $len_last = "";
my $ri2 = "";
my @final= ();
my $big = "";
my $utilization = "";
my $m = "";
my $Ar = ""; 
my $resource_index = ""; 
my @ip_chk = "";
my $loop_count3 = "";
my $ipnew = ""; 
my $ipnew_index = "";
my $ip_comparision = ""; 
my $ipprefix = ""; 
my $ipmatch = ""; 
my $range = ""; 
 my $division = ""; 
my @octets = ""; 
my $last_octet = "";
my $range_util = ""; 
my $digit_flag = "";
my $ri = ""; 
#my $val_match = "";
my $index_offset = "";
my $left_i = "";


$octet_match = $diff/8;
$octet_match = $octet_match*100;
$octet_match = int($octet_match);
if ($octet_match <= 100) {
$ri = rindex($x,$char);
$val_match = substr($x,0,$ri);
#print DEBUG "\nx: $x ri: $ri val_match: $val_match\n";
#print DEBUG "less than 100\n";
}

if ($octet_match > 100 && $octet_match <= 200){
$index_offset = index($x,$char);
$index_offset = $index_offset+1;
$left_i = index($x,$char,$index_offset);
$val_match = substr($x,0,$left_i);
#print DEBUG "more than 100\n";
}

@amatch = split(/\./,$val_match);

$last = pop(@amatch);
$len_last = length($last);

$ri2 = rindex($val_match,".");
$ri2 = $ri2 + 1;
#print DEBUG "\n ri: $ri\n val_match: $val_match\n amatch:@amatch\n last: $last\n len_last: $len_last\n ri2: $ri2\n"; 
if ($last>10){$digit_flag = 1;}
if ($last>100){$digit_flag = 2;}
push(@final,$val_match);
for (my $count=0;$count < $comb;++$count)
{
$last = $last + 1;
if($last>10){
        if($digit_flag == 0){
                $len_last = $len_last+1;
                $digit_flag = 1;}}
if($last>100){
        if($digit_flag !=2){
        $len_last = $len_last+1;
        $digit_flag = 2;}}

substr($val_match,$ri2,$len_last,$last);
push(@final,$val_match);
}
#print DEBUG "@final\n";
my $count=0;
do{
#$count++;
$big = shift(@final);
$utilization = 0;
$res = &get_utilpercent();
for $m (0 .. $#{$res}) {
my $Ar = "";
$Ar = $res->[$m]->[0];
#print Dumper($Ar);
my @ip_chk = split(/\//,$Ar);
my $ipnew  = shift(@ip_chk);
my $ipnew_index = rindex($ipnew,".");
my $ip_comparision = substr($ipnew, 0, $ipnew_index);
$ipprefix = shift(@ip_chk);
if ($ip_comparision eq $big)
{
$range = 2**($ipprefix - 24);
$division = 256/$range;
@octets = split(/\./,$ipnew);
$last_octet = $octets[3];
$range_util = 100/$range;
#$utilization = 0;
for (my $loop_count3 = 0; $loop_count3<256; $loop_count3 = $loop_count3+$division){
if( $last_octet eq $loop_count3 ){
 $utilization = $utilization+$range_util;
  }}}
}

#print DEBUG "FINAL VALUES:\n\n big: $big\n Array_val: $res->[$m]->[0] \n Ar: $Ar\n ipnew: $ipnew\n ip_comparision: $ip_comparision\nbig: $big\n last_octet: $last_octet \n loop_count3: $loop_count3\n utilization: $utilization\n\n";

CMS_DB::begin_transaction();

if($octet_match <= 100){
$ip = $big.'.0/24';
$ip_util = $utilization;
}

if($octet_match > 100){
$ip = $big.'.0.0/24';
$ip_util = $utilization;
}

        # check for whether the ip exist or not
        my $sth = $dbh_tr->prepare ("SELECT ip FROM ip_temp WHERE ip like '$ip'");
        $sth->execute();
        my $r = $sth->fetchrow_hashref;
	if($r){
        my $temp_x = $ip;
        $temp_x =~ s/\'//g;
        if($$r{ip} =~ /^$temp_x/) {
                # "Duplicated IP Address Block.";
        }
	}
       else {
print DEBUG "ip: $ip ip_util: $ip_util\n\n";
	my $temp_sql = "INSERT INTO ip_temp (ip,ip_util) VALUES ('$ip','$ip_util')";

        my $success = 1;

        $sth = $dbh_tr->prepare($temp_sql);
        $success &&= $sth->execute;
        #$error = ($error) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
        my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = ($error) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
       }
#close (DEBUG);
CMS_DB::disconnect_from_transaction();
$count++;
}
while($count<=$comb & $count<256);
}
close (DEBUG);
}

sub get_Ureports_ipadd {

	&get_Urep_res(); 


	#my $command = "
	#SELECT ip_block from ip_resource WHERE ip_status = 1";

	my $command = "SELECT * FROM ip_temp";

        my $sth = $dbh->prepare($command);
        unless ($sth->execute) {
                   $error = $dbh->errstr;
                   return 0;
        }

        my $result = $sth->fetchall_arrayref;

         if ($result) {

                   $error = $dbh->errstr;
                   $sth->finish;
                   return $result;
         } else {
                   $error = $dbh->errstr;
                   return 0;
         }

}

sub del_Ureports_ipadd {

	my $command = shift;

	my $sth = $dbh->prepare($command);
	unless ($sth->execute) {
                   $error = $dbh->errstr;
                   return 0;
        }

        my $result = $sth->fetchall_arrayref;

         if ($result) {
                   $error = $dbh->errstr;
                   $sth->finish;
                   return $result;
         } else {
                   $error = $dbh->errstr;
                   return 0;
         }

}

sub list_ipadd {
       
       my $ip_block_raw = shift @_;
       my $accno_raw = shift @_;
       my $serviceid_raw = shift @_;
       my $dns_service_raw = shift @_;
       my $ntp_service_raw = shift @_;
       my $remark_raw = shift @_;
       my $routername_raw = shift @_;
       my $interface_raw = shift @_;
       my $ip_block = $dbh->quote($ip_block_raw);
       my $accno = $dbh->quote($accno_raw);
       my $serviceid = $dbh->quote($serviceid_raw);
       my $dns_service = $dbh->quote($dns_service_raw);
       my $ntp_service = $dbh->quote($ntp_service_raw);
       my $remark = $dbh->quote($remark_raw);
       my $routername = $dbh->quote($routername_raw);
       my $interface = $dbh->quote($interface_raw);

       my $cond = "";

       my $command = "
	SELECT
	ip_block,accno,a.serviceid,dns_service,ntp_service,remark
	,TO_CHAR(date_assigned, 'YYYYMMDD') AS date_assigned,TO_CHAR(date_assigned, 'YYYYMMDD') AS date_last_update
	,routername,interface
	FROM ip_resource a, link b, routername c
	WHERE ip_status = 1 AND b.serviceid = a.serviceid AND b.routerid = c.routerid AND a.ip_block is not null COND
	ORDER BY a.ip_block
	";

        if ($ip_block_raw ne "") {
                 $cond .= "AND ip_block ilike '%$ip_block_raw%' ";
         }
         if ($serviceid_raw ne "") {
                 $cond .= "AND serviceid ilike '%$serviceid_raw%' ";
         }
         if ($accno_raw ne "") {
                $cond .= "AND accno ilike '%$accno_raw%' ";
         }
         if ($dns_service_raw ne "") {
                 $cond .= "AND dns_service ilike $dns_service ";
         }
         if ($ntp_service_raw ne "") {
                 $cond .= "AND ntp_service ilike $ntp_service ";
         }
         if ($remark_raw ne "") {
                 $cond .= "AND remark ilike '%$remark_raw%' ";
         }
         if ($routername_raw ne "") {
                 $cond .= "AND router ilike '%$routername_raw%' ";
         }
         if ($interface_raw ne "") {
                 $cond .= "AND interface ilike '%$interface_raw%' ";
         }

         $command =~ s/COND/$cond/g;

         my $sth = $dbh->prepare($command);
         unless ($sth->execute) {
                 $error = $dbh->errstr;
                 return 0;
         }

         my $result = $sth->fetchall_arrayref;

         if ($result) {

                 $error = $dbh->errstr;
                 $sth->finish;
                 return $result;
         } else {
                 $error = $dbh->errstr;
                 return 0;
         }
}

sub drop_tables {
	my $username = shift @_;
	&del_Ureports_ipadd("DROP table ip_search_temp_$username");
	&del_Ureports_ipadd("DROP table ip_util_temp_$username");
	&del_Ureports_ipadd("DROP table ip_assigned_b_temp_$username");
	&del_Ureports_ipadd("DROP table ip_assigned_b_temp_no_$username");
	&del_Ureports_ipadd("DROP table ip_consolidated_final_$username");
	&del_Ureports_ipadd("DROP table ip_consolidate_temp_$username");
	&del_Ureports_ipadd("DROP table ip_step_down_temp_$username");
}

sub create_tables {
	my $username = shift @_;
	&del_Ureports_ipadd("Create table ip_search_temp_$username (ip character varying (30))");
	&del_Ureports_ipadd("grant all on ip_search_temp_$username to public");
	&del_Ureports_ipadd("Create table ip_util_temp_$username(ip character varying (30),prefix character varying(4))");
	&del_Ureports_ipadd("grant all on ip_util_temp_$username to public");
	&del_Ureports_ipadd("Create table ip_assigned_b_temp_$username(ip character varying (30),status character varying(15))");
	&del_Ureports_ipadd("grant all on ip_assigned_b_temp_$username to public");
	&del_Ureports_ipadd("Create table ip_assigned_b_temp_no_$username(ip character varying (30),status character varying(15))");
	&del_Ureports_ipadd("grant all on ip_assigned_b_temp_no_$username to public");
	&del_Ureports_ipadd("Create table ip_consolidated_final_$username (ip character varying(30), prefix character varying(4))");
	&del_Ureports_ipadd("grant all on ip_consolidated_final_$username to public");
	&del_Ureports_ipadd("Create table ip_consolidate_temp_$username (ip character varying(30), prefix character varying(4))");
	&del_Ureports_ipadd("grant all on ip_consolidate_temp_$username to public");
	&del_Ureports_ipadd("Create table ip_step_down_temp_$username (ip character varying(30))");
	&del_Ureports_ipadd("grant all on ip_step_down_temp_$username to public");
} 	

sub search_ipadd {
	#CMS_DB::begin_transaction();
	my $assigned_state = "";
	my $ipstatus = 'y';
	my $ip_block_raw = shift @_;
	my $accno_raw = shift @_;
	my $serviceid_raw = shift @_;
	my $dns_service_raw = shift @_;
	my $ntp_service_raw = shift @_;
	my $remark_raw = shift @_;
	$assigned_state = shift @_;
 	open (RAHUL, ">/tmp/h999755_ipadd");
	my $username = $CMS::html_values{username}{value};	
	print RAHUL "\n 1091 assigned state is $assigned_state and username is $username\n";
	&drop_tables($username);
	&create_tables($username);
	if ($assigned_state eq 'y') {
	my $ip_block = $dbh->quote($ip_block_raw);
	my $accno = $dbh->quote($accno_raw);
	my $serviceid = $dbh->quote($serviceid_raw);
	my $dns_service = $dbh->quote($dns_service_raw);
	my $ntp_service = $dbh->quote($ntp_service_raw);
	my $remark = $dbh->quote($remark_raw);
	my $cond = "";

	my $command = " 
	SELECT
	ip_block,accno,serviceid,dns_service,ntp_service,remark
	,TO_CHAR(date_assigned, 'YYYYMMDD') AS date_assigned,TO_CHAR(date_assigned, 'YYYYMMDD') AS date_last_update
	,netname,mask,hosts_now,hosts_in_6_months,hosts_in_1_year,subnets_now,subnets_in_6_months,subnets_in_1_year
	,inetnum,descr,country,admin_c,tech_c,rev_srv,status,apnic_remarks,notify,mnt_by,mnt_lower,mnt_routes,changed||TO_CHAR(date_assigned, 'YYYYMMDD') AS date_assigned,source
	FROM ip_resource
	WHERE ip_status = 1 AND ip_block is not null COND
	ORDER BY ip_block
	";

	if ($ip_block_raw ne "") {
		$cond .= "AND ip_block ilike '%$ip_block_raw%' "; 
	}
	if ($serviceid_raw ne "") {
		$cond .= "AND serviceid ilike '%$serviceid_raw%' "; 
	}
	if ($accno_raw ne "") {
		$cond .= "AND accno ilike '%$accno_raw%' "; 
	}
	if ($dns_service_raw ne "") {
		$cond .= "AND dns_service ilike $dns_service "; 
	}
	if ($ntp_service_raw ne "") {
		$cond .= "AND ntp_service ilike $ntp_service "; 
	}
	if ($remark_raw ne "") {
		$cond .= "AND remark ilike '%$remark_raw%' "; 
	}

	$command =~ s/COND/$cond/g; 

	my $sth = $dbh->prepare($command);
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

	my $result = $sth->fetchall_arrayref;

	if ($result) {

		$error = $dbh->errstr;
		$sth->finish;
		return $result;
	} else {
		$error = $dbh->errstr;
		return 0;
	}

	}
	if ($assigned_state eq 'n' || $assigned_state eq 'b'){
	print RAHUL "\nInside loop";
	&get_Urep_res();
	#&del_Ureports_ipadd("DROP table ip_search_temp_$username");
	#&del_Ureports_ipadd("DROP table ip_util_temp_$username");
	my $compare_flag = 0;
	CMS_DB::begin_transaction();
	
	&ip_step_down(1,$ip_block_raw,$username);
	CMS_DB::disconnect_from_transaction();
	print RAHUL "\n 1164 error---?$error\n";
	#r5 has ips with non zero utilization
	my $sth5 = $dbh->prepare ("SELECT ip FROM ip_temp where ip like '$ip_block_raw%' and ip_util NOT LIKE '0' and ip_util NOT LIKE '100'");
	$sth5->execute();
        my $r5  = $sth5->fetchall_arrayref;
	my @r6 = "";
	#r6 has ips from ip_resource with the same first 2 octets 
        my $sth6 = $dbh->prepare ("SELECT ip_block FROM ip_resource where ip_block like '$ip_block_raw%' and ip_status = 1 order by ip_block");	
	$sth6->execute();
	#print RAHUL "\n ip_block_raw is $ip_block_raw";
	my $r6 = $sth6->fetchall_arrayref;
	my @y = "";
	my $z1= "";
	my $z2= "";
	my @z1_octets= "";
	my $z1_octet1= "";
	my $z1_octet2= "";
	my $z1_octet3= "";
	my $z1_octet4= "";
	my $z1_bin_1= ""; 
	my $z1_bin_2= ""; 
	my $z1_bin_3= ""; 
	my $z1_bin_4= "";
	my @z1_binary= "";
	my $z1_binary_stat= "";
	my @z1_binary_bit= "";
        my @y2= "";
        my $z3= "";
        my $z4= "";
        my @z4_octets= "";
        my $z4_octet1= "";
	my $z4_octet2= "";
	my $z4_octet3= "";
	my $z4_octet4= "";
	my $z4_bin_1= ""; 
	my $z4_bin_2= ""; 
	my $z4_bin_3= ""; 
	my $z4_bin_4= "";
	my @z4_binary= "";
	my $z4_binary_stat= "";
	my @z4_binary_bit= "";	
	my $l = 0;
	my $m = 0;
	my $n = 0;
	my $ip_length = "";
	my $z1_binary_result = "";
	my $z4_binary_result = "";
	my $loop_count4 = 0;
	my $Arr1 = "";
	my @ip_resource_octets = "";
  	my $ip_resource_octet1 = "";
	my $ip_resource_octet2 = "";
	my $ip_resource_octet3 = "";
	my $ip_resource_octet4 = "";
	my $command = "";
	my $command1 = "";
	my $command2 = "";
	my $command3 = "";
	my $com1 = "";
	my $Arr4 = "";
	my $Arr5 = "";
	my $Arr6 = "";
	my @r5 = "";
	my @r10 = "";
	my @r14 = "";
	my @z1_output = "";
	my $z1_final_out = "";
	my @z1_final_out = "";
	my $z1_output_stat = "";
	my $com = "";
	my $loop_n = "";
	my $k = "";
	my $octet_step = 0;
	my $loop_count40 = 0;
	my $prefix_diff = 0;
	my $octet_select = "";
	my $ip_resource_octet_step = ""; 

#the prefixes of ip_resource and reach_ip are compared.The fourth octet is calculated based on which interval the ip resource octet falls in.
print RAHUL "\n 1243 count arr ---> $#$r5------------------>$#$r6\n";
			for $k(0 ..$#$r6){
			$Arr4 = $r6->[$k]->[0];
			@y2 = split(/\//,$Arr4);
			$z3= shift(@y2);
			$z4= shift(@y2);
			print RAHUL "\n 1249 Inserting z3 and zz4 values $z3  $z4\n";
			my $sth16 = $dbh->prepare("INSERT into ip_util_temp_$username values('$z3','$z4')");
			$sth16->execute();
			}

			#create_table_search();
			print RAHUL "\n Before for::"; 
			for $m(0 ..$#$r5 ){
			$Arr5 = $r5->[$m]->[0];
			print RAHUL "Util report data is $Arr5\n";
			@y = split(/\//,$Arr5);
			$z1=shift(@y);
			$z2=shift(@y);
			print RAHUL "z1 and z2 is $z1, $z2\n";
			@z1_octets=  split(/\./,$z1);
			$z1_octet1 = shift(@z1_octets);
			$z1_octet2 = shift(@z1_octets);
			$z1_octet3 = shift(@z1_octets);
			$z1_octet4 = shift(@z1_octets);
			my $sth17 = $dbh->prepare("SELECT * from ip_util_temp_$username where ip like '$z1_octet1.$z1_octet2.$z1_octet3%' order by prefix desc");
			$sth17->execute();
			my $r17 = $sth17->fetchall_arrayref;
			print RAHUL "\n 1271 search query is SELECT max(prefix) from ip_util_temp_$username where ip ilike '$z1_octet1.$z1_octet2.$z1_octet3% \n";
		 	my $sth18 = $dbh->prepare("SELECT max(prefix) from ip_util_temp_$username where ip ilike '$z1_octet1.$z1_octet2.$z1_octet3%'");
			$sth18->execute();
			my $r18 = $sth18->fetchrow_arrayref;
			my $query = "";
			$com1 = $$r18[0];
			print RAHUL "\n 1277 com value is $com1 \n";
			$octet_select = "";	
			for $l(0 ..$#$r17){
			#&create_table_search();
			$Arr4 = $r17->[$l]->[0];
			$z3 = $Arr4;
			$z4 = $r17->[$l]->[1];
			#print RAHUL "IP Resource data is $Arr4\n";
			#print RAHUL "\n z4 is $z4; z3 is $z3";
			@ip_resource_octets = split(/\./,$z3);
			$ip_resource_octet1 = shift(@ip_resource_octets);
			$ip_resource_octet2 = shift(@ip_resource_octets);
			$ip_resource_octet3 = shift(@ip_resource_octets);
			$ip_resource_octet4 = shift(@ip_resource_octets);
			#print RAHUL "\n ip_resource_oc 4 is $ip_resource_octet4";
			$prefix_diff = ($com1 - $z4);
			if($prefix_diff > 0) {$prefix_diff = 2**($com1 - $z4);}
			#print RAHUL "\n PREFIX DIFF IS $prefix_diff";
			if($prefix_diff == 0) {
				if ($l == 0){
					$octet_select.=$ip_resource_octet4;
				}
			else{
			$octet_select.=",".$ip_resource_octet4;}
			}
			else {
			$octet_step = 256/(2**($com1 - 24));
			#print RAHUL "Octet STEP is $octet_step\n";
			#$octet_select.=$ip_resource_octet4.",";
			$ip_resource_octet_step = $ip_resource_octet4;
				for ($loop_count40 = 1; $loop_count40 < $prefix_diff; $loop_count40++){
				$ip_resource_octet_step = $ip_resource_octet_step + $octet_step;
				if($l == 0) {
					$octet_select.=$ip_resource_octet_step.",".$ip_resource_octet4;
				}
				else {
				$octet_select.= ",".$ip_resource_octet_step.",".$ip_resource_octet4;
				}}
			}
			#print RAHUL "\n octet select is -----------> $octet_select\n";
			
		}	
		       $z1_final_out = join "",$z1_octet1, ".",$z1_octet2,".",$z1_octet3,".";
			print RAHUL "\n value of com1 is $com1";
			#consolidation of ips

			if ($com1 > 27){
			print RAHUL "\n 1324 entering consolidation";
			&del_Ureports_ipadd("DELETE from ip_consolidate_temp_$username");
			&del_Ureports_ipadd("DELETE from ip_consolidated_final_$username");
			my $sth11 = $dbh->prepare("INSERT into ip_consolidate_temp_$username(Select ip from ip_unassigned_$com1 where ip NOT IN($octet_select))");
			$sth11->execute();
			&consolidate_ip($com1,$username);
				
			$query = "Insert into ip_search_temp_$username (Select '$z1_final_out'||ip||'/'||prefix from ip_consolidated_final_$username)";
			print RAHUL "\n 1332 end of consolidation for >27";
			}
			if ($com1 ==27) {
			print RAHUL "\n 1335 Enter if for com1 = 27";
			$query = "Insert into ip_search_temp_$username (Select '$z1_final_out'||ip||'/$com1' from ip_unassigned_$com1 where ip NOT IN($octet_select))";
			}
			if ($com1 <27) {
			&del_Ureports_ipadd("DELETE from ip_step_down_temp_$username");
			print RAHUL "\n1339  INSERT into ip_step_down_temp_$username(Select ip from ip_unassigned_$com1 where ip NOT IN($octet_select))";
			my $sth12 = $dbh->prepare("INSERT into ip_step_down_temp_$username(Select ip from ip_unassigned_$com1 where ip NOT IN($octet_select))");
			$sth12->execute();
			&ip_step_down(2,$com1,$username);
			
			$query = "Insert into ip_search_temp_$username  (Select '$z1_final_out'||ip||'/27' from ip_step_down_temp_$username)";
			print RAHUL "\n 1346  query is Insert into ip_search_temp_$username  (Select ip from ip_step_down_temp_$username)";
			}
					
			my $sth10 = $dbh->prepare($query);
			$sth10->execute();

	}		
 	#print RAHUL "test \n";
#close (RAHUL);	

 	if ($assigned_state eq 'n'){
	my $sth12 = $dbh->prepare("Select distinct(ip) from ip_search_temp_$username");
	$sth12->execute();
	my $r12 = $sth12->fetchall_arrayref;
	&drop_tables($username);
	print RAHUL "\n 1361 before returning for no";
	return $r12; 	
	}
	if ($assigned_state eq 'b') {
	&get_Urep_res();
 	&del_Ureports_ipadd("DELETE from ip_assigned_b_temp_$username");
	&del_Ureports_ipadd("DELETE from ip_assigned_b_temp_no_$username");	
	my $sth12 = $dbh->prepare("Insert into ip_assigned_b_temp_$username(Select ip_block,ip_status from ip_resource where ip_block ilike '$ip_block_raw%' and ip_status = 1)");
	$sth12->execute();
	my $sth13 = $dbh->prepare("Update ip_assigned_b_temp_$username set Status = 'Assigned' where Status = 1");
	$sth13->execute();	
	my $sth14 = $dbh->prepare("Insert into ip_assigned_b_temp_no_$username(Select ip FROM ip_search_temp_$username where ip like '$ip_block_raw%')");
	$sth14->execute();
	my $sth15 = $dbh->prepare("Update ip_assigned_b_temp_no_$username set Status = 'Not Assigned'");
 	$sth15->execute();
	my $sth17 = $dbh->prepare("Insert into ip_assigned_b_temp_$username(Select * from ip_assigned_b_temp_no_$username)");
	$sth17->execute();	
	my $sth16 = $dbh->prepare("Select distinct(ip),status FROM ip_assigned_b_temp_$username order by status"); 
	$sth16->execute();
	my $r16 = $sth16->fetchall_arrayref;
	&drop_tables($username);
	print RAHUL "\n 1382 before returning for both";
	return $r16;
	}
	}
}

sub ip_step_down {
	print RAHUL "\n 1389 inside ip_step_down";
	my $code_value = shift @_;	
	my $prefix_diff = 0;
	my $l = 0;
	my $ip_block_raw = ""; 
	my $r1 = "";
	my $prefix_insert = 27;
	my $Arr1=  "";
	my $Arr2 = "";
	my @y;
	my $z1;
	my $z2;
	my @z1_octets;
	my $z1_octet1;
	my $z1_octet2;
	my $z1_octet3;
	my $z1_octet4;
	my $octet_step = 0;
	my $z1_insert = 0;
	my $m = 0;
	my $z1_final_out = "";
	my $count = 0;
	my $prefix = 0;
	my $sth1 = "";
	my $sth2 = "";
	my $ip = "";
	my $username = "";

	if($code_value ==1){
	print RAHUL "\n 1418 code value is 1, prefix is 24";
	my $ip_block_raw = shift @_;
	$username = shift @_;
	my $sth1 = $dbh->prepare("SELECT ip from ip_temp where ip like '$ip_block_raw%' and ip_util = '0'");
	$sth1->execute();
	$r1 = $sth1->fetchall_arrayref;
	for ($l=0; $l<=$#$r1; $l++){
		$Arr1 = $r1->[$l]->[0];
		@y = split(/\//,$Arr1);
		$z1=shift(@y);
		$z2=shift(@y);
		print RAHUL "z1 and z2 is $z1, $z2\n";
		@z1_octets=  split(/\./,$z1);
		$z1_octet1 = shift(@z1_octets);
		$z1_octet2 = shift(@z1_octets);
		$z1_octet3 = shift(@z1_octets);
		$z1_octet4 = shift(@z1_octets);
		$prefix_diff = 27 - $z2;	
		$octet_step = 256/2**($prefix_diff);
		$count = 2**$prefix_diff;
		$z1_insert = 0;
		for ($m=0;$m<$count; $m++){
			$z1_final_out = join "",$z1_octet1, ".",$z1_octet2,".",$z1_octet3,".",$z1_insert;
			my $sth2 = $dbh->prepare("INSERT into ip_search_temp_$username values('$z1_final_out'||'/27')");
			$sth2->execute();
			$z1_insert = $z1_insert+$octet_step;
		}
	}
	}
	if($code_value ==2){
	
	print RAHUL "\n 1449 inside code_value = 2 prefix is 25 or 26";
	$prefix = shift @_;
	$username = shift @_;
	$sth1 = $dbh->prepare("SELECT ip from ip_step_down_temp_$username");
	$sth1->execute();
	$r1 = $sth1->fetchall_arrayref;
	&del_Ureports_ipadd("DELETE from ip_step_down_temp_$username ");	
	$prefix_diff = 2**(27 - $prefix);
	$octet_step = 32;
	$ip =0;
	print RAHUL "\n 1459 loop counter is $#$r1";
	for($m=0;$m<=$#$r1;$m++){
	$Arr1 = $r1->[$m]->[0];
	print RAHUL "\n 1462 isnert is is $ip";
	$ip =$Arr1;
		for($l=0;$l<$prefix_diff;$l++){
			print RAHUL "\n 1465 ip inside loop is $ip";
			$sth2 = $dbh->prepare("INSERT into ip_step_down_temp_$username values($ip)");
			$sth2->execute();
			$ip =$ip+$octet_step;	
		}
		}	
	}			
		 
}
		  

	
	
sub consolidate_ip {
	print RAHUL "\n 1479 inside consolidate_ip";
	my $Arr1 = "";
 	my $l = 0;
 	my $r2 = "";
	my $Arr2 = "";
	my $prefix_insert = 0;
 	my $nxt_octet = 0;	
 	my $prefix_diff = 0;
 	my $step_count = 0;
 	my $prefix = shift @_;
	my $username = shift @_;
	my $m =0;
	print RAHUL "\n 1491------------------Delete from ip_consolidated_final_$username --------------------";
	&del_Ureports_ipadd("DELETE from ip_consolidated_final_$username");
	#my $r_result = "";
	$prefix_insert = $prefix;
	my $step1 = 256/2**($prefix-24);
	my $step2 = 256/2**($prefix-25);
	print RAHUL "\n 1497 value of com1 is $prefix";
	&del_Ureports_ipadd("UPDATE ip_consolidate_temp_$username set prefix = $prefix");
	my $sth2 = $dbh->prepare("SELECT * from ip_consolidate_temp_$username");
	$sth2->execute();
	$r2 = $sth2->fetchall_arrayref;
	$prefix_diff = $prefix - 27;
	if($prefix_diff > 0){
	$prefix_insert = $prefix - 1;
	for($m=0; $m <($prefix - 27); $m++){
	print RAHUL "\n 1506 ----------------------------------->m is $m";
#	$prefix_insert = $prefix - 1;
		if($m >0){
		#	$prefix_insert = $prefix_insert - 1;
			my $sth4 = $dbh->prepare("SELECT * from ip_consolidated_final_$username where prefix = '$prefix_insert'");
			$sth4->execute();
			$r2 = $sth4->fetchall_arrayref;
			print RAHUL "\n 1513 query is SELECT * from ip_consolidated_final_$username where prefix = '$prefix_insert'";
			&del_Ureports_ipadd("DELETE from ip_consolidated_final_$username where prefix = '$prefix_insert'");
			$step1 = $step2;
			$step2 = 2*$step2;
			$prefix_insert = $prefix_insert - 1;
		}
	 
		for ($l=0;$l<=$#$r2;$l++){
		print RAHUL"\n 1521 value of hash ref is $#$r2";
		$Arr1 = $r2->[$l]->[0];
			if($Arr1 == "" && $Arr1 ne '0'){
			print RAHUL "\n 1524 Arr1 value is $Arr1";
			last;}
		print RAHUL "\n 1523 value of arr is -------------------->$l-----> $Arr1";
		if($Arr1%$step2 != 0){
		
		print RAHUL "\n 1526 query is Insert into ip_consolidated_final_$username('$Arr1','$r2->[$l]->[1]')";
		my $sth3 = $dbh->prepare("Insert into ip_consolidated_final_$username values('$Arr1','$r2->[$l]->[1]')");
		$sth3->execute();
		#next;
		}
		if($Arr1%$step2 == 0) {
	#	print RAHUL "\n value of arr1 by step 1 is $Arr1%$step2";
		$Arr2 = $r2->[$l+1]->[0];
		$nxt_octet = $Arr1+$step1;
		
			if($Arr2 == $nxt_octet){
			 print RAHUL "\n 1537 l value---->$l query is Insert into ip_consolidated_final_$username values('$Arr1','$prefix_insert')";
			&del_Ureports_ipadd("Insert into ip_consolidated_final_$username values('$Arr1','$prefix_insert')");
			$l = $l+1;
			print RAHUL "\n 1540 l value is---------------> $l";
			}
			if($Arr2 != $nxt_octet){
			print RAHUL "\n 1543 l value---->$l query is Insert into ip_consolidated_final_$username values('$Arr1','$r2->[$l]->[1]')";
			&del_Ureports_ipadd("Insert into ip_consolidated_final_$username values('$Arr1','$r2->[$l]->[1]')");
			}
		}		
		}
	
	}
	}	
}
		

	
 

        open (DEBUG, ">/tmp/h999755_ipedit");
sub update_ipadd {
print DEBUG "ENTERED update function\n";
	CMS_DB::begin_transaction();
	my $ip_block_raw = shift @_;
	my $accno_raw = shift @_;
	my $serviceid_raw = shift @_;
	my $ip_block = $dbh_tr->quote($ip_block_raw);
	my $accno = $dbh_tr->quote($accno_raw);
	my $serviceid = $dbh_tr->quote($serviceid_raw);
	my $dns_service = $dbh_tr->quote(shift @_);
	my $ntp_service = $dbh_tr->quote(shift @_);
	my $remark = $dbh_tr->quote(shift @_);

	my $netname = $dbh_tr->quote(shift @_);
	my $mask = $dbh_tr->quote(shift @_);
	my $hosts_now = $dbh_tr->quote(shift @_);
	my $hosts_in_6_months = $dbh_tr->quote(shift @_);
	my $hosts_in_1_year = $dbh_tr->quote(shift @_);
	my $subnets_now = $dbh_tr->quote(shift @_);
	my $subnets_in_6_months = $dbh_tr->quote(shift @_);
	my $subnets_in_1_year = $dbh_tr->quote(shift @_);

	my $inetnum = $dbh_tr->quote(shift @_);
	my $inet_num_name = $dbh_tr->quote(shift @_);
	my $descr = $dbh_tr->quote(shift @_);
	my $country = $dbh_tr->quote(shift @_);
	my $admin_c = $dbh_tr->quote(shift @_);
	my $tech_c = $dbh_tr->quote(shift @_);
	my $rev_srv = $dbh_tr->quote(shift @_);
	my $status = $dbh_tr->quote(shift @_);
	my $apnic_remarks = $dbh_tr->quote(shift @_);
	my $notify = $dbh_tr->quote(shift @_);
	my $mnt_by = $dbh_tr->quote(shift @_);
	my $mnt_lower = $dbh_tr->quote(shift @_);
	my $mnt_routes = $dbh_tr->quote(shift @_);
	my $changed = $dbh_tr->quote(shift @_);
	my $source = $dbh_tr->quote(shift @_);
        #my $serviceid_val = $dbh_tr->quote(shift @_);

	# my $date_assigned = 'CURRENT_TIMESTAMP';  # assigned date is not editable
        my $date_last_update = 'CURRENT_TIMESTAMP';

	my $temp_serviceid = "";
	my $accno1 = "";

	# # get service id - legacy - may be upper or lower case
	# $serviceid =~ s/\'//g;
	# $serviceid =~ s/\"//g;

	my ($r, $sth);
                open (XX, ">/tmp/ip_block");
                print XX $ip_block;
                close (XX);
	# check for whether the ip block is exist or not
	#$sth = $dbh_tr->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like $ip_block AND serviceid like $serviceid AND ip_status = 1");
#	#print RAHUL "Query is SELECT ip_block FROM ip_resource WHERE ip_block like $ip_block AND serviceid like $serviceid\n";
	#$sth->execute();
	#$r = $sth->fetchrow_hashref;
	#my $temp_x = $ip_block;
	#$temp_x =~ s/\'//g;
	#print RAHUL "\n temporary value is ------------------ $temp_x\n";
	#print RAHUL "\n $r  value is ------------------ $$r{ip_block}\n";
	#unless ($$r{ip_block} =~ /^$temp_x$/) {
	#	return "IP Address Block/Service ID combination does not exist.";
	#}

        # check for whether the accno exist in service with given serviceid
        if ($serviceid_raw ne '') {
                my $sth = $dbh_tr->prepare ("SELECT serviceid FROM service WHERE serviceid ilike $serviceid");
                $sth->execute();
                my $r = $sth->fetchrow_hashref;
                $temp_serviceid = $serviceid;
                $temp_serviceid =~ s/\'//g;
                if($$r{serviceid} !~ /^$temp_serviceid$/i) {
print DEBUG "INSIDE update_ipadd - Service ID does not exist.";
                        return "Service ID does not exist.";
                } else {
print DEBUG "INSIDE update_ipadd - SERVICE ID EXISTS\n";
        #check for accno for given serviceid from service
        my $sth2 = $dbh_tr->prepare ("SELECT accno FROM service WHERE serviceid ilike $serviceid");
        $sth2->execute();
        my $success = $sth2->fetchrow_hashref;
        $accno = "\'$$success{accno}\'";
print DEBUG "INSIDE update_ipadd - accno:  $accno\n";
                }
        }




        $sth = $dbh_tr->prepare ("SELECT service_id from temp_service_val where status = '1'");

        $sth->execute();
        my $r1 = $sth->fetchall_arrayref;
        my $temp_val_serv = $r1->[0]->[0];

print DEBUG "INSIDE  update_ipadd service_id from temp_service_val: $temp_val_serv \n";
print DEBUG "INSIDE  update_ipadd IP- $ip_block SERVICE: $serviceid \n";

        #ip_block exists so check serviceid for same
        #my $sth1 = $dbh_tr->prepare ("SELECT count(serviceid) from ip_resource WHERE ip_block like $ip_block and serviceid like $serviceid and ip_status = '1'");
        #$sth1->execute();
        #my $res1 = $sth1->fetchall_arrayref;
        #my $x = $res1->[0]->[0];
        #if ( ($x >= 1)  ){
        #print DEBUG "update_ipadd - Combination of  Serviceid + IP_BLOCK exists.\n serviceid: $serviceid\n For Loop: $res1->[0]->[0]\n\n";
        #return "Combination of  Serviceid + IP_BLOCK exists.";
        #}

                #check for accno for given ip_block from ip_resource
                $sth = $dbh_tr->prepare ("SELECT distinct accno FROM ip_resource WHERE ip_block ilike $ip_block AND ip_status = '1'");
                $sth->execute();
                my $success = $sth->fetchall_arrayref;
                $accno1 = $success->[0]->[0];
                $accno1 = "\'$accno1\'";
print DEBUG "INSIDE  update_ipadd -  New serviceid ($serviceid) + ip-block ($ip_block) :  accno1: $accno1 accno : $accno\n";


print DEBUG "INSIDE update_ipadd - BEFORE if loop: accno1: $accno1 accno : $accno \n";
 if  ($accno eq $accno1) {
print DEBUG "INSIDE update_ipadd - INSIDE if loop: accno1: $accno1 accno : $accno \n";

	$sth = $dbh_tr->prepare ("SELECT distinct d.countryname from GIA_countryname d,cityname c,  popname p, routername r, link l,service i where i.serviceid = l.serviceid and r.routerid = l.routerid and p.popid = r.popid and c.cityid = p.cityid and d.cityid = c.cityid and i.serviceid ilike $serviceid");
	$sth->execute();
        my $cntry = $sth->fetchall_arrayref;
        $country = "\'$cntry->[0]->[0]\'";

	
	# extend the update sql statement
	my $morefield;
	# $morefield .= " , ip_block = $ip_block";  # ip block is not editable
	$morefield .= " , accno = $accno";
	$morefield .= " , serviceid = $serviceid";
	$morefield .= " , dns_service = $dns_service";
	$morefield .= " , ntp_service = $ntp_service";
	$morefield .= " , remark = $remark";
	$morefield .= " , netname = $netname";
	$morefield .= " , mask = $mask";
	$morefield .= " , hosts_now = $hosts_now";
	$morefield .= " , hosts_in_6_months = $hosts_in_6_months";
	$morefield .= " , hosts_in_1_year = $hosts_in_1_year";
	$morefield .= " , subnets_now = $subnets_now";
	$morefield .= " , subnets_in_6_months = $subnets_in_6_months";
	$morefield .= " , subnets_in_1_year = $subnets_in_1_year";
	$morefield .= " , inetnum = $inetnum";
	#$morefield .= " , inet_num_name = $inet_num_name";
	$morefield .= " , descr = $descr";
	$morefield .= " , country = $country";
	$morefield .= " , admin_c = $admin_c";
	$morefield .= " , tech_c = $tech_c";
	$morefield .= " , rev_srv = $rev_srv";
	$morefield .= " , status = $status";
	$morefield .= " , apnic_remarks = $apnic_remarks";
	$morefield .= " , notify = $notify";
	$morefield .= " , mnt_by = $mnt_by";
	$morefield .= " , mnt_lower = $mnt_lower";
	$morefield .= " , mnt_routes = $mnt_routes";
	#$morefield .= " , changed = $changed";
	$morefield .= " , source = $source";
	# $morefield .= " , date_assigned = $date_assigned";  # assigned date is not editable
	# $morefield .= " , date_last_update = $date_last_update";

	$success = 1;
        my $success1 = 1;

print DEBUG "INSIDE update_ipadd - BEFORE update: serviceid: $serviceid\n";
	$sth = $dbh_tr->prepare ("UPDATE ip_resource SET date_last_update = $date_last_update $morefield WHERE ip_block = $ip_block AND serviceid = '$temp_val_serv'");

	$success &&= $sth->execute;
	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
print DEBUG "INSIDE update_ipadd - AFTER UPDATE - update_ipadd - UPD QUERY: $sth\n";
	
	my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

	CMS_DB::disconnect_from_transaction();

	#delete_edited_val();

print DEBUG "INSIDE update_ipadd - RESULT : $result\n";
	return $error if ($error);
	return $result;
} else {

 $error = "Entered Service ID $serviceid is having different account number $accno for the IP_BLOCK $ip_block.";
        return $error;
}
close (DEBUG);
}
        open (DEBUG, ">/tmp/h999755_ipedit");
sub delete_edited_val {
print DEBUG "ENTERED delete_edited_val\n";
        CMS_DB::begin_transaction();
        my $sth2 = $dbh_tr->prepare ("DELETE from temp_service_val");
#print DEBUG "INSIDE delete_edited_val - QUERY: $sth2\n"
        my $success = $sth2->execute;
	$sth2->execute;
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

        my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;


        CMS_DB::disconnect_from_transaction();

print DEBUG "INSIDE delete_edited_val - error: $error\n";
print DEBUG "INSIDE delete_edited_val - result: $result\n";
print DEBUG "INSIDE delete_edited_val - EXITING\n";
close (DEBUG);
}


sub delete_ipadd {
	CMS_DB::begin_transaction();
	my $ip_block_raw = shift @_;
	my $serviceid_raw = shift @_;
	my $ip_block = $dbh_tr->quote($ip_block_raw);
	my $serviceid = $dbh_tr->quote($serviceid_raw);
        my $date_last_update = 'CURRENT_TIMESTAMP';
	open (RAHUL, ">/tmp/h999755_ipdel");
	my ($r1, $sth);

	# check for whether the ip block is exist or not
	$sth = $dbh_tr->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block ilike $ip_block AND serviceid ilike $serviceid AND ip_status = 1");
	$sth->execute();
	my $r1 = $sth->fetchrow_hashref;
	my $temp_x = $ip_block;
	print RAHUL "Delete temp value is $temp_x\n";
	print RAHUL "\n Query is SELECT ip_block FROM ip_resource WHERE ip_block like $ip_block AND serviceid like $serviceid";
	$temp_x =~ s/\'//g;
	print RAHUL "\n value of sth is $sth->fetchrow_hashref value of $r1 is $$r1{ip_block}";
	unless ($$r1{ip_block} =~ /^$temp_x$/) {
		print RAHUL "\n $$r1{ip_block} and temp is $temp_x\n";
		print RAHUL "\n within Unless\n";
		return "IP Address Resource/Service ID Combination does not exist.";
	}



        # check count for ip_block
        $sth = $dbh_tr->prepare ("SELECT count(ip_block) FROM ip_resource WHERE ip_block ilike $ip_block AND ip_status = 1");
        $sth->execute();
        my $r2 = $sth->fetchall_arrayref;

        print RAHUL "\n value of sth is $r2 \n count: $r2->[0]->[0] \n";

        my $del_val = 0;
        if ($r2->[0]->[0] <=1) {
        $del_val = 1;
        } else {
        $del_val = 2;
        }
        print RAHUL "\n value of del_val: $del_val\n";


	$ip_status = '2';
	print RAHUL "\n ip_status $ip_status\n";

        #Inserting the deleted ips into table delete_ip_res
        my $temp_sql = "INSERT INTO delete_ip_res select * from ip_resource where ip_block ilike $ip_block and serviceid ilike  $serviceid";
        my $success = 1;

        my $sth1 = $dbh_tr->prepare($temp_sql);

        $success = $sth1->execute;
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

        my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

        # Deleteing the ips from ip_resource
        my $temp1_sql = "DELETE from ip_resource where ip_block ilike $ip_block and serviceid ilike $serviceid";

        my $success1 = 1;

        my $sth2 = $dbh_tr->prepare($temp1_sql);

        $success1 = $sth2->execute;

        my $result1 = ($success1 ? $dbh_tr->commit : $dbh_tr->rollback);
        CMS_DB::disconnect_from_transaction();


        print RAHUL "\n result is $result\n";

        return $error if ($error);
        return ($result, $del_val);
}
close(RAHUL);


sub get_service {
  my $serviceid = $dbh->quote(shift @_);
  my $accno = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT description, currentbw, productcodeid,
             switchsourcodeid, switchdestcodeid, pollingtype, costype, gold_classmap, gold_rateacl, silver_classmap, silver_rateacl, default_classmap, default_rateacl, class1_classmap, class1_rateacl, class2_classmap, class2_rateacl, class4_classmap, class4_rateacl, as_no, auto_gen_prefix, bgp_neighbor_ip, irrd_object, aggregate_serviceid, billing_optionid, billing_starttime, billing_endtime
, serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno, aend_country, zend_country
      FROM service
      WHERE serviceid ilike $serviceid
        AND accno = $accno");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchrow_arrayref;

  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub get_switchcode {
	#my $asname = shift @_;
	#if ($asname eq '') {
	#	$asname = 'switchcodeid';
	#}
	my $sth = $dbh->prepare("SELECT switchcodeid FROM switchcode order by switchcodeid");

	unless ($sth->execute) {
    		$error = $dbh->errstr;
    		return 0;
  	}

  	my $result = $sth->fetchall_arrayref;

  	if ($result) {
    		$error = $dbh->errstr;
    		$sth->finish;
    		return $result;
  	} else {
    		$error = $dbh->errstr;
    		return 0;
  	}
}

sub get_productcode {
	my $sth = $dbh->prepare("SELECT productcodeid FROM productcode");

	unless ($sth->execute) {
    		$error = $dbh->errstr;
    		return 0;
  	}

  	my $result = $sth->fetchall_arrayref;

  	if ($result) {
    		$error = $dbh->errstr;
    		$sth->finish;
    		return $result;
  	} else {
    		$error = $dbh->errstr;
    		return 0;
  	}
}

sub get_productcode_for_all_service {
        my $sth = $dbh->prepare("SELECT serviceid, productcodeid, slareportcodeid
				 FROM service, customer
				 where service.accno=customer.accno");
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_productcode_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT productcodeid FROM service
				WHERE serviceid = $sid");
	unless ($sth->execute) {
		$error = $dbh->errstr;
		return 0;
	}

	my $result = $sth->fetchrow_hashref;
	$error = $dbh->errstr;
	$sth->finish;
	return $result;
}

sub get_serviceclass {
	my $sth = $dbh->prepare("SELECT serviceclassid FROM serviceclass");

	unless ($sth->execute) {
    		$error = $dbh->errstr;
    		return 0;
  	}

  	my $result = $sth->fetchall_arrayref;

  	if ($result) {
    		$error = $dbh->errstr;
    		$sth->finish;
    		return $result;
  	} else {
    		$error = $dbh->errstr;
    		return 0;
  	}
}

sub get_label_for_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT svclabel FROM service
				WHERE serviceid = $sid");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_reseller_label_for_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT reseller_svclabel as svclabel FROM service
				WHERE serviceid = $sid");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_auto_gen_prefix {
	my $sth = $dbh->prepare("SELECT option FROM gen_prefix_option");
  
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

#
# Added - for QOS polling
# rchew@maettr.bpa.nu, 13 August 2006
#
sub get_pollingtype {
	## XX to be completed
	my $sth = $dbh->prepare("SELECT pollingtype FROM pollingtype");
  
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }

}

#
# Added for Telstra/PCCW labels
# rchew@maettr.bpa.nu, 25 October 2006
#
sub get_costype {
        my $sth = $dbh->prepare("SELECT costype FROM costype");
  
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
  
}

#
# TI_CHECKBOX single option
#
sub get_checkbox_single_option {
	my $value = shift @_;
        my $sth = $dbh->prepare("SELECT '$value'");
  
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


#
# dns_service options
#
sub get_dns_service_option {
         my $sth = $dbh->prepare("SELECT option FROM dns_service_option ORDER BY id desc");
        # id 1 = default value
 
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


#
# ntp_service options
#
sub get_ntp_service_option {
  
        my $sth = $dbh->prepare("SELECT option FROM ntp_service_option ORDER BY id desc");
        # id 1 = default value


        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


#
# confirm options
#
sub get_confirm_option {
        my $sth = $dbh->prepare("SELECT option FROM confirm_option ORDER BY id");
	# id 0 = default value
  
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}


sub get_gen_prefix_by_service {
	my $sid = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT auto_gen_prefix FROM service where serviceid = $sid");
 
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
 
        my $result = $sth->fetchall_arrayref;
 
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_aggregate_service {
	my $sth = $dbh->prepare("SELECT aggregate_serviceid from aggregate_service where active<>'inactive' order by aggregate_serviceid");
  
        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_service_by_as {
        my $as = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT serviceid from service where as_no = $as");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub get_service_by_bgp_ip {
        my $bgp_ip = $dbh->quote(shift @_);
        my $sth = $dbh->prepare("SELECT serviceid from service where bgp_neighbor_ip = $bgp_ip");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub set_label_for_serviceid {
	my $serviceid = $dbh->quote(shift @_);
	my $label = $dbh->quote(shift @_);

	my $sth = $dbh->prepare("UPDATE service SET svclabel = $label WHERE serviceid ilike $serviceid");
	my $result = $sth->execute;
	$error = $dbh->errstr;
	return $result;

}

sub set_reseller_label_for_serviceid {
	my $serviceid = $dbh->quote(shift @_);
	my $label = $dbh->quote(shift @_);

	my $sth = $dbh->prepare("UPDATE service SET reseller_svclabel = $label WHERE serviceid ilike $serviceid");
	my $result = $sth->execute;
	$error = $dbh->errstr;
	return $result;

}

sub get_serviceid_alt_for_service {
	my $sid = $dbh->quote(shift @_);
	my $sth = $dbh->prepare("SELECT serviceid_alt FROM service
				WHERE serviceid = $sid");
	unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchrow_hashref;
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub update_service {
  CMS_DB::begin_transaction();
  my $serviceid_raw = shift @_;
  my $serviceid = $dbh_tr->quote($serviceid_raw);
  my $accno_raw = shift @_;
  my $accno = $dbh_tr->quote($accno_raw);
  my $description = $dbh_tr->quote(shift @_);
  my $currentbw = $dbh_tr->quote(shift @_);
  my $productcode_raw = shift @_;
  my $productcode = $dbh_tr->quote($productcode_raw);
  my $switchsourcode = $dbh_tr->quote(shift @_);
  my $switchdestcode = $dbh_tr->quote(shift @_);
  my $pollingtype = $dbh_tr->quote(shift @_);
  my $costype = $dbh_tr->quote(shift @_);
  my $gold_classmap_raw    = shift @_;
  my $gold_rateacl_raw     = shift @_;
  my $silver_classmap_raw  = shift @_;
  my $silver_rateacl_raw   = shift @_;
  my $default_classmap_raw = shift @_;
  my $default_rateacl_raw  = shift @_;
  my $class1_classmap_raw = shift @_;
  my $class1_rateacl_raw  = shift @_;
  my $class2_classmap_raw = shift @_;
  my $class2_rateacl_raw  = shift @_;
  my $class4_classmap_raw = shift @_;
  my $class4_rateacl_raw  = shift @_;
  my $gold_classmap    = $dbh_tr->quote($gold_classmap_raw);
  my $gold_rateacl     = $dbh_tr->quote($gold_rateacl_raw);
  my $silver_classmap  = $dbh_tr->quote($silver_classmap_raw);
  my $silver_rateacl   = $dbh_tr->quote($silver_rateacl_raw);
  my $default_classmap = $dbh_tr->quote($default_classmap_raw);
  my $default_rateacl  = $dbh_tr->quote($default_rateacl_raw);
  my $class1_classmap = $dbh_tr->quote($class1_classmap_raw);
  my $class1_rateacl  = $dbh_tr->quote($class1_rateacl_raw);
  my $class2_classmap = $dbh_tr->quote($class2_classmap_raw);
  my $class2_rateacl  = $dbh_tr->quote($class2_rateacl_raw);
  my $class4_classmap = $dbh_tr->quote($class4_classmap_raw);
  my $class4_rateacl  = $dbh_tr->quote($class4_rateacl_raw);
  my $as_no = $dbh_tr->quote(shift @_);
  my $auto_gen_prefix = $dbh_tr->quote(shift @_);
  my $bgp_neighbor_ip = $dbh_tr->quote(shift @_);
  my $irrd_object = $dbh_tr->quote(shift @_);
#  my $serviceclass = $dbh_tr->quote(shift @_);
  my $aggregate_serviceid = $dbh_tr->quote(shift @_);
  # for billing dates
  #my $billable = $dbh_tr->quote(shift @_);  # changed to following 2 lines to fix the billing option updating problem
  my $billable_raw = shift @_;
  my $billable = $dbh_tr->quote($billable_raw);
  my $billstartdate_year = shift @_;
  my $billstartdate_month = shift @_;
  my $billstartdate_day = shift @_;
  my $billstarttime_hour = shift @_;
  my $billstarttime_minute = shift @_;
  my $billstarttime_second = shift @_;
  my $billenddate_year = shift @_;
  my $billenddate_month = shift @_;
  my $billenddate_day = shift @_;
  my $billendtime_hour = shift @_;
  my $billendtime_minute = shift @_;
  my $billendtime_second = shift @_;
  my $serviceid_alt_raw = shift @_;
  my $serviceid_alt = $dbh_tr->quote($serviceid_alt_raw);
  my $aend_jobid = $dbh_tr->quote(shift @_);
  my $zend_jobid = $dbh_tr->quote(shift @_);
  my $aend_jobno = $dbh_tr->quote(shift @_);
  my $zend_jobno = $dbh_tr->quote(shift @_);
  my $aend_country = $dbh_tr->quote(shift @_);
  my $zend_country = $dbh_tr->quote(shift @_);

  # get service id - legacy - may be upper or lower case

  $serviceid =~ s/\'//g;
  $serviceid =~ s/\"//g;

  my $sth = $dbh_tr->prepare("SELECT serviceid FROM service WHERE serviceid ilike ?");
  $sth->execute($serviceid);
  my $r = $sth->fetchrow_hashref;
  if($$r{serviceid} =~ /^$serviceid$/i) {
	$serviceid = "'$$r{serviceid}'";
  }

  my $billstartdt = 'null';
  if ($billstartdate_year !~ /-/) {
        $billstartdt =
                $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $billstartdate_year, $billstartdate_month, $billstartdate_day,
                        $billstarttime_hour, $billstarttime_minute, $billstarttime_second));
  }
  my $billenddt = 'null';
  if ($billenddate_year !~ /-/) {
        $billenddt =
                $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $billenddate_year, $billenddate_month, $billenddate_day,
                        $billendtime_hour, $billendtime_minute, $billendtime_second));
  }

  # extend the update sql statement
  my $morefield;
  $currentbw =~ s/\'//g;  # remove quote for bandwidth - int
  if($currentbw =~ /^\d+$/) {
	$morefield .= " , currentbw = $currentbw";
  }
  $morefield .= " , productcodeid = $productcode";
  $morefield .= " , switchsourcodeid = $switchsourcode";
  $morefield .= " , switchdestcodeid = $switchdestcode";
  $morefield .= " , pollingtype = $pollingtype";
  $morefield .= " , costype = $costype";
  $morefield .= " , gold_classmap = $gold_classmap";
  $morefield .= " , gold_rateacl = $gold_rateacl";
  $morefield .= " , silver_classmap = $silver_classmap";
  $morefield .= " , silver_rateacl = $silver_rateacl";
  $morefield .= " , default_classmap = $default_classmap";
  $morefield .= " , default_rateacl = $default_rateacl";
  $morefield .= " , class1_classmap = $class1_classmap";
  $morefield .= " , class1_rateacl = $class1_rateacl";
  $morefield .= " , class2_classmap = $class2_classmap";
  $morefield .= " , class2_rateacl = $class2_rateacl";
  $morefield .= " , class4_classmap = $class4_classmap";
  $morefield .= " , class4_rateacl = $class4_rateacl";
  $morefield .= " , as_no = $as_no";
  $morefield .= " , bgp_neighbor_ip = $bgp_neighbor_ip";
  $morefield .= " , irrd_object = $irrd_object";
#  $morefield .= " , serviceclassid = $serviceclass";
  $morefield .= " , aggregate_serviceid = $aggregate_serviceid"; 
  # for billing dates
  $morefield .= " , billing_optionid = $billable";
  $morefield .= " , billing_starttime = $billstartdt";
  $morefield .= " , billing_endtime = $billenddt";
  # for serviceid for atm/fr
  $morefield .= " , serviceid_alt = $serviceid_alt";
  # for gbs
  $morefield .= " , aend_jobid = $aend_jobid";
  $morefield .= " , zend_jobid = $zend_jobid";
  $morefield .= " , aend_jobno = $aend_jobno";
  $morefield .= " , zend_jobno = $zend_jobno";
  $morefield .= " , aend_country = $aend_country";
  $morefield .= " , zend_country = $zend_country";

  $sth = $dbh_tr->prepare
    ("UPDATE service
         SET description = $description $morefield
      WHERE accno = $accno
        AND serviceid = $serviceid");

  my $success = 1;
  $success &&= $sth->execute;
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
  if ($success) {
        $success &&= update_billing(
                        $serviceid_raw, $accno_raw, $billable_raw,
                        $billstartdate_year, $billstartdate_month, $billstartdate_day,
                        $billstarttime_hour, $billstarttime_minute, $billstarttime_second,
                        $billenddate_year, $billenddate_month, $billenddate_day,
                        $billendtime_hour, $billendtime_minute, $billendtime_second,
                        $productcode_raw,
                        $gold_classmap_raw, $gold_rateacl_raw,
                        $silver_classmap_raw, $silver_rateacl_raw,
                        $default_classmap_raw, $default_rateacl_raw
                        )
  }
  if ($success) {
	$sth = $dbh_tr->prepare ("update service set auto_gen_prefix = $auto_gen_prefix, irrd_object = $irrd_object where as_no = $as_no");
  	$success &&= $sth->execute;
       	$error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
  }

  my $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

  CMS_DB::disconnect_from_transaction();

  return $success;
}

sub delete_service {
  my $serviceid = shift @_;
  my $accno_raw = shift @_;
  my $accno = $dbh->quote($accno_raw);
  my ($sth, $result);

  # retrieve service's as no 
  $sth = $dbh->prepare("select as_no from service where serviceid = '$serviceid'");
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  $result = $sth->fetchall_arrayref;

  if ($result->[0][0] ne "") {
     my $sth2 = $dbh->prepare("select serviceid from service where as_no = '$result->[0][0]' and serviceid != '$serviceid'");
     unless ($sth2->execute) {
       $error = $dbh->errstr;
       return 0;
     }

     my $result2 = $sth2->fetchall_arrayref;
     if ($#{$result2} >= 0) {
        # update routing queue's service id
        CMS_ROUTING_QUEUE_DB::update_opshandle($serviceid, $result2->[0][0]);
     }
  }     

  CMS_DB::begin_transaction();
  my ($billable, $billing_starttime, $billing_endtime);
  my ($productcode, $gold_classmap, $gold_rateacl, $silver_classmap, $silver_rateacl, $default_classmap, $default_rateacl);
  my ($billstartdate_year, $billstartdate_month, $billstartdate_day, $billstarttime_hour, $billstarttime_minute, $billstarttime_second);
  my ($billenddate_year, $billenddate_month, $billenddate_day, $billendtime_hour, $billendtime_minute, $billendtime_second);

  if (my $tmp = get_service($serviceid, $accno_raw) ) {
        $productcode = $$tmp[2];
#        $gold_classmap = $$tmp[5];
#        $gold_rateacl = $$tmp[6];
#        $silver_classmap = $$tmp[7];
#        $silver_rateacl = $$tmp[8];
#        $default_classmap = $$tmp[9];
#        $default_rateacl = $$tmp[10];
#        $billable = $$tmp[16];
#        $billing_starttime = $$tmp[17];
#        $billing_endtime = $$tmp[18];
        $gold_classmap = $$tmp[7];
        $gold_rateacl = $$tmp[8];
        $silver_classmap = $$tmp[9];
        $silver_rateacl = $$tmp[10];
        $default_classmap = $$tmp[11];
        $default_rateacl = $$tmp[12];
        $billable = $$tmp[24];
        $billing_starttime = $$tmp[25];
        $billing_endtime = $$tmp[26];

        # startdate
        my @tmpdt  = split(/-|\s|\:|\+/, $billing_starttime);
        my @billdt;
        if ($tmpdt[0] ne '') {
                @billdt = (@tmpdt[0,1,2,3,4,5]);
                #@billdt = (@tmpdt[0,1,2]);
        } else {
                @billdt = ('----','--','--','--','--','--');
                #@billdt = ('----','--','--');
        }
        ( $billstartdate_year, $billstartdate_month, $billstartdate_day,
                $billstarttime_hour, $billstarttime_minute, $billstarttime_second
                ) = @billdt;
        # enddate
         @tmpdt  = split(/-|\s|\:|\+/, $billing_endtime);
         #@billdt;
        if ($tmpdt[0] ne '') {
                @billdt = (@tmpdt[0,1,2,3,4,5]);
                #@billdt = (@tmpdt[0,1,2]);
        } else {
                @billdt = ('----','--','--','--','--','--');
                #@billdt = ('----','--','--');
        }
        ( $billenddate_year, $billenddate_month, $billenddate_day,
                $billendtime_hour, $billendtime_minute, $billendtime_second
                ) = @billdt;
  } else {
	my $tmperr = "That account does not have such a service.";
        $error = (length($error)!=0) ? $error."<br>"."$tmperr" : $tmperr;
        return 0;
  }

  $sth = $dbh_tr->prepare
    ("DELETE FROM service
      WHERE accno = $accno
        AND serviceid ilike ?");

  my $success = 1;
  $success &&= $sth->execute($serviceid);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
  if ($success) {
        $success &&= update_billing(
                        $serviceid, $accno_raw, $billable,
                        $billstartdate_year, $billstartdate_month, $billstartdate_day,
                        $billstarttime_hour, $billstarttime_minute, $billstarttime_second,
                        $billenddate_year, $billenddate_month, $billenddate_day,
                        $billendtime_hour, $billendtime_minute, $billendtime_second,
                        $productcode,
                        $gold_classmap, $gold_rateacl,
                        $silver_classmap, $silver_rateacl,
                        $default_classmap, $default_rateacl
                        )
  }

  $result = ($success ? $dbh_tr->commit : $dbh_tr->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

  CMS_DB::disconnect_from_transaction();
##  return $success;

##  $sth = $dbh->prepare
##    ("DELETE FROM service 
##      WHERE accno = $accno
##        AND serviceid ilike ?");
##
##  unless ($sth->execute($serviceid)) {
##    $error = $dbh->errstr;
##    return 0;
##  }

  # delete route from routing queue
  CMS_ROUTING_QUEUE_DB::delete_routing_request_by_opshandle($serviceid);

  return $success;
##  return 1;
}

sub get_accno_for_serviceid {
  # For OLSS
  my $serviceid = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT accno
      FROM service
      WHERE serviceid = $serviceid");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }
  
  my $result = $sth->fetchrow_arrayref;

  if ($result) {
    $error = $dbh->errstr;
    $sth->finish;
    return $$result[0];
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub search_service {
  my $serviceid = shift @_;
  my $accno = shift @_;
  my $description = shift @_;
  my $bandwidth = shift @_;
  my $productcode = shift @_;
  my $as = shift @_;
  my $irrd_obj = shift @_;
  my $serviceid_alt = shift @_;
  my $aend_jobid = shift @_;
  my $zend_jobid = shift @_;
  my $aend_jobno = shift @_;
  my $zend_jobno = shift @_;  
  my $cond = "";
  
#  my $command = 
#    ("SELECT serviceid, accno, description, currentbw, productcodeid, null, null, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, gold_classmap, gold_rateacl, class4_classmap, class4_rateacl, silver_classmap, silver_rateacl, class2_classmap, class2_rateacl, class1_classmap, class1_rateacl, default_classmap, default_rateacl, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno FROM service left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where productcodeid != 'GIA' COND 
#     UNION 
#     SELECT service.serviceid, accno, service.description, currentbw, productcodeid, routername, interface, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, gold_classmap, gold_rateacl, class4_classmap, class4_rateacl, silver_classmap, silver_rateacl, class2_classmap, class2_rateacl, class1_classmap, class1_rateacl, default_classmap, default_rateacl, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where productcodeid = 'GIA' COND"
#    );

  my $command =
    ("SELECT serviceid, accno, description, currentbw, productcodeid, null, null, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno FROM service left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid != 'GIA' AND productcodeid != 'MPLS') COND
     UNION
     SELECT service.serviceid, accno, service.description, currentbw, productcodeid, routername, interface, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'GIA' OR productcodeid = 'MPLS') COND"
    );

#  my $command =
#    ("SELECT serviceid, accno, description, currentbw, productcodeid, null, null, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno FROM service left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid != 'GIA' AND productcodeid != 'MPLS') COND
#     UNION
#     SELECT service.serviceid, accno, service.description, currentbw, productcodeid, routername, interface, as_no, bgp_neighbor_ip, auto_gen_prefix, irrd_object, null, aggregate_serviceid, pollingtype, gold_rateacl, gold_classmap, class4_rateacl, class4_classmap, silver_rateacl, silver_classmap, class2_rateacl, class2_classmap, class1_rateacl, class1_classmap, default_rateacl, default_classmap, billing_option.billing_option, service.billing_starttime, service.billing_endtime, service.serviceid_alt, aend_jobid, zend_jobid, aend_jobno, zend_jobno FROM service left outer join link on service.serviceid = link.serviceid left outer join routername on link.routerid = routername.routerid left outer join billing_option on billing_option.billing_optionid = service.billing_optionid where (productcodeid = 'GIA' OR productcodeid = 'MPLS') COND"
#    );

  if ($serviceid ne "") {
    $cond .= "AND service.serviceid ilike '%$serviceid%' "; 
  }

  if ($accno ne "") {
    $cond .= "AND service.accno ilike '%$accno%' "; 
  }
  
  if ($description ne "") {
    $cond .= "AND service.description ilike '%$description%' "; 
  }

  if ($bandwidth ne "") {
    $cond .= "AND service.currentbw = $bandwidth "; 
  }

  if ($productcode ne "") {
    $cond .= "AND service.productcodeid = '$productcode' ";
  }

  if ($as ne "") {
    $cond .= "AND service.as_no = '$as' ";
  }

  if ($irrd_obj ne "") {
    $cond .= "AND service.irrd_object ilike '%$irrd_obj%' ";
  }

  if ($serviceid_alt ne "") {
    $cond .= "AND service.serviceid_alt ilike '%$serviceid_alt%' ";
  }

  if($aend_jobid ne "") {
    $cond .= "AND service.aend_jobid ilike '%$aend_jobid%' ";
  }

  if($zend_jobid ne "") {
    $cond .= "AND service.zend_jobid ilike '%$zend_jobid%' ";
  }

  if($aend_jobno ne "") {
    $cond .= "AND service.aend_jobno ilike '%$aend_jobno%' ";
  }

  if($zend_jobno ne "") {
    $cond .= "AND service.zend_jobno ilike '%$zend_jobno%' ";
  }

  $command =~ s/COND/$cond/g; 

  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }

}

sub get_qosservice {
  my $sth = $dbh->prepare("select serviceid, gold_classmap, gold_rateacl, silver_classmap, silver_rateacl, default_classmap, default_rateacl from service
                           where gold_classmap is not null or gold_rateacl is not null or silver_classmap is not null or silver_rateacl is not null or default_classmap is not null or default_rateacl is not null
                           order by serviceid");
  $sth->execute;
  my $array_ref = $sth->fetchall_arrayref();
  return $array_ref;
}

sub get_aut_num_obj {
  my $sth = $dbh->prepare("SELECT as_no FROM service group by as_no");
  $sth->execute;

  my $result = $sth->fetchall_arrayref();
  $sth->finish;

  my %aut_num = ();

  for my $i (0 .. $#{$result}) {
     $sth = $dbh->prepare("SELECT description FROM service where as_no = '$result->[$i][0]'");
     $sth->execute;
     my $res = $sth->fetchall_arrayref();
     $sth->finish;
     if ($#{$res} >= 0) {
        $aut_num{$result->[$i][0]} = $res->[0][0] ;
     }
  }
  return %aut_num;
}

sub get_customer_as {
  my $sql_command = "select distinct as_no from service where as_no != ''";
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }
 
  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}    

sub get_irrd_object {
  my $sql_command = "select distinct irrd_object, as_no from service where irrd_object != '' and as_no != ''";
  my $sth = $dbh->prepare($sql_command);

  unless ($sth->execute) {
    $error = $dbh::err;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $sth->finish;
  $error = $dbh::err;
  return $result;
}
 
sub get_gold {
        my ($serviceid) = @_;
 
        my $sth = $dbh->prepare("select distinct gold_classmap, gold_rateacl from service
                                where serviceid='$serviceid' and (gold_classmap<>'' or gold_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}  

sub get_silver {
        my ($serviceid) = @_;

        my $sth = $dbh->prepare("select distinct silver_classmap, silver_rateacl from service
                                where serviceid='$serviceid' and (silver_classmap<>'' or silver_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}

sub get_default {
        my ($serviceid) = @_;

        my $sth = $dbh->prepare("select distinct default_classmap, default_rateacl from service
                                where serviceid='$serviceid' and (default_classmap<>'' or default_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}

sub get_class1 {
        my ($serviceid) = @_;

        my $sth = $dbh->prepare("select distinct class1_classmap, class1_rateacl from service
                                where serviceid='$serviceid' and (class1_classmap<>'' or class1_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}

sub get_class2 {
        my ($serviceid) = @_;
  
        my $sth = $dbh->prepare("select distinct class2_classmap, class2_rateacl from service
                                where serviceid='$serviceid' and (class2_classmap<>'' or class2_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}
  
sub get_class3 {
        my ($serviceid) = @_;
  
        my $sth = $dbh->prepare("select distinct class3_classmap, class3_rateacl from service
                                where serviceid='$serviceid' and (class3_classmap<>'' or class3_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}

sub get_class4 {
        my ($serviceid) = @_;
  
        my $sth = $dbh->prepare("select distinct class4_classmap, class4_rateacl from service
                                where serviceid='$serviceid' and (class4_classmap<>'' or class4_rateacl<>'')");
        $sth->execute;
        my $array_ref = $sth->fetchall_arrayref();
        return $array_ref;
}

sub get_billing_option {
        my $opt = shift @_;
  
        my $tmpsql = "SELECT billing_optionid, billing_option FROM billing_option ";
        if ($opt !~ /^all$/i) {  # "all" option not specified
                # ignore id 0
                $tmpsql .= "where billing_optionid != 0 ";
        }
        $tmpsql .= "order by billing_optionid";
        #my $sth = $dbh->prepare("SELECT billing_optionid, billing_option FROM billing_option order by billing_optionid");
        my $sth = $dbh->prepare($tmpsql);
  
        unless ($sth->execute) {  
                $error = $dbh->errstr;
                return 0;
        }
  
        my $result = $sth->fetchall_arrayref;
  
        if ($result) {
                $error = $dbh->errstr;
                $sth->finish;
                return $result;
        } else {
                $error = $dbh->errstr;
                return 0;
        }
}

sub update_billing {
        if (scalar @_ != 22) {
                my $tmperr = "update_billing: Incorrect number of parameters.";
                $error = (length($error)!=0) ? $error."<br>"."$tmperr" : $tmperr;
                return 0;
        }
        my $serviceid_raw = shift @_;
        my $serviceid = $dbh_tr->quote($serviceid_raw);
        my $accno = $dbh_tr->quote(shift @_);
        my $billable = shift @_;
        my $billstartdate_year = shift @_;
        my $billstartdate_month = shift @_;
        my $billstartdate_day = shift @_;
        my $billstarttime_hour = shift @_;
        my $billstarttime_minute = shift @_;
        my $billstarttime_second = shift @_;
        my $billenddate_year = shift @_;
        my $billenddate_month = shift @_;
        my $billenddate_day = shift @_;
        my $billendtime_hour = shift @_;
        my $billendtime_minute = shift @_;
        my $billendtime_second = shift @_;
        my $productcode = $dbh_tr->quote(shift @_);
        my $gold_classmap = $dbh_tr->quote(shift @_);
        my $gold_rateacl = $dbh_tr->quote(shift @_);
        my $silver_classmap = $dbh_tr->quote(shift @_);
        my $silver_rateacl = $dbh_tr->quote(shift @_);
        my $default_classmap = $dbh_tr->quote(shift @_);
        my $default_rateacl = $dbh_tr->quote(shift @_);

        my $actiontype;
        my $sth = $dbh->prepare ("SELECT serviceid FROM billing WHERE serviceid ilike ?");
        $sth->execute($serviceid_raw);
        my $r = $sth->fetchrow_hashref;
        if($$r{serviceid} =~ /^$serviceid_raw$/i) {
                #return 0;
                $actiontype = 'U';  #update
        } else {
                $actiontype = 'A';  #add
        }

        # format dates in ISO-8601 standard
        # (YYYY-MM-DD HH:MM:SS[+/-]XX where XX is the difference from UTC)

        my $billstartdt = 'null';
        if ($billstartdate_year !~ /-/) {
        $billstartdt =
                $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $billstartdate_year, $billstartdate_month, $billstartdate_day,
                        $billstarttime_hour, $billstarttime_minute, $billstarttime_second));
        }
        my $billenddt = 'null';
        if ($billenddate_year !~ /-/) {
        $billenddt =
                $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                              $billenddate_year, $billenddate_month, $billenddate_day,
                              $billendtime_hour, $billendtime_minute, $billendtime_second));
        }

        my @now = gmtime;
        my $timestamp = $dbh_tr->quote(sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                                1900+$now[5], $now[4]+1, $now[3],
                                $now[2], $now[1], $now[0]));

	if ($billable == '') {
		$billable = 2;  # default billing option = 2, non usage billing
	}

        my $success = 1;

        if ($actiontype =~ /^U$/) {
                $sth = $dbh_tr->prepare
                ("DELETE FROM billing where serviceid = $serviceid");
                $success &&= $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;
        }
        if ($success) {
##                $sth = $dbh_tr->prepare
##                ("
##                INSERT INTO billing(timestamp, serviceid, accno, billing_optionid, billing_starttime, billing_endtime,
##                        productcodeid,
##                        gold_classmap, gold_rateacl,
##                        silver_classmap, silver_rateacl,
##                        default_classmap, default_rateacl
##                        )
##                VALUES($timestamp, $serviceid, $accno, $billable, $billstartdt, $billenddt,
##                        $productcode,
##                        $gold_classmap, $gold_rateacl,
##                        $silver_classmap, $silver_rateacl,
##                        $default_classmap, $default_rateacl
##                        )
##                ");
                my $xx ="
                INSERT INTO billing(timestamp, serviceid, accno, billing_optionid, billing_starttime, billing_endtime,
                        productcodeid,
                        gold_classmap, gold_rateacl,
                        silver_classmap, silver_rateacl,
                        default_classmap, default_rateacl
                        )
                VALUES($timestamp, $serviceid, $accno, $billable, $billstartdt, $billenddt,
                        $productcode,
                        $gold_classmap, $gold_rateacl,
                        $silver_classmap, $silver_rateacl,
                        $default_classmap, $default_rateacl
                        )
                ";
		open (XX, ">/tmp/leo_debug~CMS_SERVICE_DB.pm~update_billing");
		print XX $xx;
		close (XX);
		$sth = $dbh_tr->prepare
		("$xx");
                $success &&= $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh_tr->errstr : $dbh_tr->errstr;

        }

        #my $result = $sth->execute;
        #$error = $dbh_tr->errstr;
        #return $result;
        return $success;

}

sub search_billing {
  my $serviceid = shift @_;
  my $accno = shift @_;
  my $description = shift @_;
  my $bandwidth = shift @_;

 
##  my $command =
##    ("SELECT a.serviceid, a.accno, a.description, a.currentbw, a.gold_classmap, a.gold_rateacl,
##      a.silver_classmap, a.silver_rateacl, a.default_classmap, a.default_rateacl, a.as_no, a.bgp_neighbor_ip,
##      b.billing_option, a.billing_starttime, a.billing_endtime
##      FROM service a
##      left outer join billing_option b on a.billing_optionid = b.billing_optionid ");
##  my $command =
##    ("select a.timestamp, date_part('epoch', a.timestamp) as timestamp_in_uxtime,
##        a.serviceid, a.accno, a.billing_optionid, b.billing_option,
##        a.billing_starttime, date_part('epoch', a.billing_starttime) as billing_starttime_in_uxtime,
##        a.billing_endtime, date_part('epoch', a.billing_endtime) as billing_endtime_in_uxtime,
##        a.productcodeid,
##        a.gold_classmap, a.gold_rateacl, a.silver_classmap, a.silver_rateacl, a.default_classmap, a.default_rateacl
##        from billing a
##        left outer join billing_option b on a.billing_optionid = b.billing_optionid ");
  my $command =
	("select  
	a.timestamp, date_part('epoch', a.timestamp) as timestamp_in_uxtime,
	a.serviceid, a.accno, a.billing_optionid, b.billing_option,
	a.billing_starttime, date_part('epoch', a.billing_starttime) as billing_starttime_in_uxtime,
	a.billing_endtime, date_part('epoch', a.billing_endtime) as billing_endtime_in_uxtime,
	a.productcodeid,
	a.gold_classmap, a.gold_rateacl, a.silver_classmap, a.silver_rateacl, a.default_classmap, a.default_rateacl,
	d.type
	from billing a
	left outer join billing_option b on a.billing_optionid = b.billing_optionid
	left outer join link c on a.serviceid = c.serviceid
	left outer join link_type d on c.typeid = d.typeid ");

  if ($serviceid ne "") {
    $command .= "AND serviceid ilike '%$serviceid%' ";
  }

  if ($accno ne "") {
    $command .= "AND accno ilike '%$accno%' ";
  }
 
  if ($description ne "") {
    $command .= "AND description ilike '%$description%' ";
  }

  if ($bandwidth ne "") {
    $command .= "AND currentbw = $bandwidth ";
  }

  $command =~ s/AND/WHERE/; # change first AND


  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }

}

#
# Added for new qos polling - only used for getting polling type
# rchew@maettr.bpa.nu, 13 August 2006
# Pre: serviceid
# Post: 
sub get_pollingtype_sid {
	my $serviceid = shift @_;
    
  my $command =  
    "SELECT pollingtype
     FROM service where serviceid = '$serviceid'";
    
  my $sth = $dbh->prepare($command);
  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;

  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
}

sub get_costype_sid {
        my $serviceid = shift @_;
  
  my $command =
    "SELECT costype
     FROM service where serviceid = '$serviceid'";
  my $sth = $dbh->prepare($command);

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  
  my $result = $sth->fetchall_arrayref;
  
  if ($result) {

    $error = $dbh->errstr;
    $sth->finish;
    return $result;
  } else {
    $error = $dbh->errstr;
    return 0;
  }
  
  
}

# This is to get all classes
sub get_cos_all {
        my ($serviceid) = shift @_;
  
        my %ret;  # $ret{classmap}{classname} = value / $ret{rateacl}{classname} = value
  
        # need to get class 0 (default), class 1, class 2, class 3 (silver), class 4 and class 5 (gold)
  
        my $sth = $dbh->prepare("select distinct default_classmap, default_rateacl, class1_classmap, class1_rateacl,
                                class2_classmap, class2_rateacl, silver_classmap, silver_rateacl, class4_classmap, class4_rateacl,
                                gold_classmap, gold_rateacl from service where serviceid='$serviceid'");
  
        $sth->execute;
        my $res = $sth->fetchall_arrayref();
  
        foreach my $d2 (@$res) {
                $ret{classmap}{class0} = $$d2[0];
                $ret{rateacl}{class0} = $$d2[1];
                $ret{classmap}{class1} = $$d2[2];
                $ret{rateacl}{class1} = $$d2[3];
                $ret{classmap}{class2} = $$d2[4];
                $ret{rateacl}{class2} = $$d2[5];
                $ret{classmap}{class3} = $$d2[6];
                $ret{rateacl}{class3} = $$d2[7];
                $ret{classmap}{class4} = $$d2[8];
                $ret{rateacl}{class4} = $$d2[9];
                $ret{classmap}{class5} = $$d2[10];
                $ret{rateacl}{class5} = $$d2[11];
        }
  
        return \%ret;

}



1;
