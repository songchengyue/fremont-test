package com.telstra.netops.dns.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.dns.server.exception.DnsApplicationGenericBusinessRuleException;
import com.telstra.netops.dns.server.model.ClientDnsResolverRequest;
import com.telstra.netops.dns.server.model.DnsResolverRecord;
import com.telstra.netops.dns.server.model.DnsReverseRecord;
import com.telstra.netops.dns.server.model.Success;
import com.telstra.netops.dns.server.model.cgi.DnsResultWrapper;

@Service
@Profile("local")
public class LocalResolverService implements ResolverService {

	private static final java.lang.String HEADER = "List of Allowed IPs for DNS Resolver Service";
	
	@Value("${dns.localtest.success}")
	private boolean success;
	
	@Override
	public Success add(ClientDnsResolverRequest request) {
		if (success) {
			return new Success("SUCCESS!");
		} else {
			throw new DnsApplicationGenericBusinessRuleException("ERROR!!!");
		}
	}

	@Override
	public List<DnsResolverRecord> list(String customerId) {
		List<DnsResolverRecord> records = new ArrayList<DnsResolverRecord>();
		records.add(new DnsResolverRecord("yy1234", "123.19.2.0/24", "raj@gmail.com", customerId, HEADER));
		records.add(new DnsResolverRecord("ttttt1234", "123.18.2.0/24", "ram@gmail.com", customerId, HEADER));
		records.add(new DnsResolverRecord("ssss1234", "123.17.2.0/24", "uday@gmail.com", customerId, HEADER));
		return records;
	}

	@Override
	public Success remove(ClientDnsResolverRequest request) {
		if (success) {
			return new Success("SUCCESS!");
		} else {
			throw new DnsApplicationGenericBusinessRuleException("ERROR!!!");
		}
	}
	
	

}
