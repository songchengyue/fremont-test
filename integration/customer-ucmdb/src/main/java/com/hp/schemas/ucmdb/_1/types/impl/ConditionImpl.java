/*
 * XML Type:  Condition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.Condition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML Condition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ConditionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.Condition
{
    
    public ConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
