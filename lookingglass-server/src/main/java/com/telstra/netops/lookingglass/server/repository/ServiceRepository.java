package com.telstra.netops.lookingglass.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.telstra.netops.lookingglass.server.model.ProductLine;
import com.telstra.netops.lookingglass.server.model.Service;

public interface ServiceRepository extends PagingAndSortingRepository<Service, Long> {
	
	@Query("SELECT DISTINCT s.productLine FROM Service s WHERE s.customerId = :customerId ORDER BY s.productLine.code")
	List<ProductLine> findDistinctProductLineByCustomerId(@Param("customerId") String customerId);

}
