package com.telstra.netops.dns.server.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.dns.server.exception.DnsApplicationGenericBusinessRuleException;
import com.telstra.netops.dns.server.model.ClientDnsRemoveRequest;
import com.telstra.netops.dns.server.model.ClientSecondaryDnsRequest;
import com.telstra.netops.dns.server.model.SecondaryDnsRecord;
import com.telstra.netops.dns.server.model.Success;

@Service
@Profile("local")
public class LocalSecondaryDnsService implements SecondaryDnsService {

	@Value("${dns.localtest.success}")
	private boolean success;
	
	Date dateCheck = new Date();
	
	@Override
	public Success add(ClientSecondaryDnsRequest request) {
		if (success) {
			return new Success("SUCCESS!");
		} else {
			throw new DnsApplicationGenericBusinessRuleException("ERROR!!!");
		}
	}

	@Override
	public List<SecondaryDnsRecord> list(String accountId) {
		List<SecondaryDnsRecord> records = new ArrayList<SecondaryDnsRecord>();
		records.add(new SecondaryDnsRecord("yy1234", "123.19.2.0/24", dateCheck ,"ASSSA44545"));
		records.add(new SecondaryDnsRecord("ttttt1234", "123.18.2.0/24", dateCheck,"ASSSA44545"));
		records.add(new SecondaryDnsRecord("ssss1234", "123.17.2.0/24", dateCheck,"ASSSA44545"));
		return records;
	}

	@Override
	public Success remove(ClientDnsRemoveRequest request) {
		if (success) {
			return new Success("SUCCESS!");
		} else {
			throw new DnsApplicationGenericBusinessRuleException("ERROR!!!");
		}
	}

	

}
