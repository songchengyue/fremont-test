/*
 * XML Type:  TopologyMap
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.TopologyMap
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML TopologyMap(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class TopologyMapImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.TopologyMap
{
    
    public TopologyMapImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CINODES$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "CINodes");
    private static final javax.xml.namespace.QName RELATIONNODES$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "relationNodes");
    
    
    /**
     * Gets the "CINodes" element
     */
    public com.hp.schemas.ucmdb._1.types.CINodes getCINodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CINodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.CINodes)get_store().find_element_user(CINODES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "CINodes" element
     */
    public boolean isSetCINodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CINODES$0) != 0;
        }
    }
    
    /**
     * Sets the "CINodes" element
     */
    public void setCINodes(com.hp.schemas.ucmdb._1.types.CINodes ciNodes)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CINodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.CINodes)get_store().find_element_user(CINODES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CINodes)get_store().add_element_user(CINODES$0);
            }
            target.set(ciNodes);
        }
    }
    
    /**
     * Appends and returns a new empty "CINodes" element
     */
    public com.hp.schemas.ucmdb._1.types.CINodes addNewCINodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CINodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.CINodes)get_store().add_element_user(CINODES$0);
            return target;
        }
    }
    
    /**
     * Unsets the "CINodes" element
     */
    public void unsetCINodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CINODES$0, 0);
        }
    }
    
    /**
     * Gets the "relationNodes" element
     */
    public com.hp.schemas.ucmdb._1.types.RelationNodes getRelationNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.RelationNodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.RelationNodes)get_store().find_element_user(RELATIONNODES$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "relationNodes" element
     */
    public boolean isSetRelationNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RELATIONNODES$2) != 0;
        }
    }
    
    /**
     * Sets the "relationNodes" element
     */
    public void setRelationNodes(com.hp.schemas.ucmdb._1.types.RelationNodes relationNodes)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.RelationNodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.RelationNodes)get_store().find_element_user(RELATIONNODES$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.RelationNodes)get_store().add_element_user(RELATIONNODES$2);
            }
            target.set(relationNodes);
        }
    }
    
    /**
     * Appends and returns a new empty "relationNodes" element
     */
    public com.hp.schemas.ucmdb._1.types.RelationNodes addNewRelationNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.RelationNodes target = null;
            target = (com.hp.schemas.ucmdb._1.types.RelationNodes)get_store().add_element_user(RELATIONNODES$2);
            return target;
        }
    }
    
    /**
     * Unsets the "relationNodes" element
     */
    public void unsetRelationNodes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RELATIONNODES$2, 0);
        }
    }
}
