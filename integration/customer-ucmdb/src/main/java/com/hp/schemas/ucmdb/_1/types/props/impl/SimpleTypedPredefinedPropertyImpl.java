/*
 * XML Type:  SimpleTypedPredefinedProperty
 * Namespace: http://schemas.hp.com/ucmdb/1/types/props
 * Java type: com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.props.impl;
/**
 * An XML SimpleTypedPredefinedProperty(@http://schemas.hp.com/ucmdb/1/types/props).
 *
 * This is a complex type.
 */
public class SimpleTypedPredefinedPropertyImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty
{
    
    public SimpleTypedPredefinedPropertyImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "name");
    
    
    /**
     * Gets the "name" element
     */
    public com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name.Enum getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "name" element
     */
    public com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name)get_store().find_element_user(NAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "name" element
     */
    public void setName(com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name.Enum name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NAME$0);
            }
            target.setEnumValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "name" element
     */
    public void xsetName(com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name)get_store().add_element_user(NAME$0);
            }
            target.set(name);
        }
    }
    /**
     * An XML name(@http://schemas.hp.com/ucmdb/1/types/props).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty$Name.
     */
    public static class NameImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.props.SimpleTypedPredefinedProperty.Name
    {
        
        public NameImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected NameImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
