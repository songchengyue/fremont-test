/*
 * XML Type:  CustomTypedProperties
 * Namespace: http://schemas.hp.com/ucmdb/1/types/props
 * Java type: com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.props.impl;
/**
 * An XML CustomTypedProperties(@http://schemas.hp.com/ucmdb/1/types/props).
 *
 * This is a complex type.
 */
public class CustomTypedPropertiesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties
{
    
    public CustomTypedPropertiesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PROPERTIESLIST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "propertiesList");
    private static final javax.xml.namespace.QName PREDEFINEDTYPEDPROPERTIES$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "predefinedTypedProperties");
    
    
    /**
     * Gets the "propertiesList" element
     */
    public com.hp.schemas.ucmdb._1.types.props.PropertiesList getPropertiesList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PropertiesList target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PropertiesList)get_store().find_element_user(PROPERTIESLIST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "propertiesList" element
     */
    public boolean isSetPropertiesList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PROPERTIESLIST$0) != 0;
        }
    }
    
    /**
     * Sets the "propertiesList" element
     */
    public void setPropertiesList(com.hp.schemas.ucmdb._1.types.props.PropertiesList propertiesList)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PropertiesList target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PropertiesList)get_store().find_element_user(PROPERTIESLIST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.PropertiesList)get_store().add_element_user(PROPERTIESLIST$0);
            }
            target.set(propertiesList);
        }
    }
    
    /**
     * Appends and returns a new empty "propertiesList" element
     */
    public com.hp.schemas.ucmdb._1.types.props.PropertiesList addNewPropertiesList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PropertiesList target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PropertiesList)get_store().add_element_user(PROPERTIESLIST$0);
            return target;
        }
    }
    
    /**
     * Unsets the "propertiesList" element
     */
    public void unsetPropertiesList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PROPERTIESLIST$0, 0);
        }
    }
    
    /**
     * Gets the "predefinedTypedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties getPredefinedTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties)get_store().find_element_user(PREDEFINEDTYPEDPROPERTIES$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "predefinedTypedProperties" element
     */
    public boolean isSetPredefinedTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREDEFINEDTYPEDPROPERTIES$2) != 0;
        }
    }
    
    /**
     * Sets the "predefinedTypedProperties" element
     */
    public void setPredefinedTypedProperties(com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties predefinedTypedProperties)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties)get_store().find_element_user(PREDEFINEDTYPEDPROPERTIES$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties)get_store().add_element_user(PREDEFINEDTYPEDPROPERTIES$2);
            }
            target.set(predefinedTypedProperties);
        }
    }
    
    /**
     * Appends and returns a new empty "predefinedTypedProperties" element
     */
    public com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties addNewPredefinedTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.props.PredefinedTypedProperties)get_store().add_element_user(PREDEFINEDTYPEDPROPERTIES$2);
            return target;
        }
    }
    
    /**
     * Unsets the "predefinedTypedProperties" element
     */
    public void unsetPredefinedTypedProperties()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREDEFINEDTYPEDPROPERTIES$2, 0);
        }
    }
}
