/*
 * XML Type:  StrListProp
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.StrListProp
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML StrListProp(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class StrListPropImpl extends com.hp.schemas.ucmdb._1.types.impl.CIPropImpl implements com.hp.schemas.ucmdb._1.types.StrListProp
{
    
    public StrListPropImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STRVALUES$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strValues");
    
    
    /**
     * Gets the "strValues" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList getStrValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(STRVALUES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "strValues" element
     */
    public boolean isNilStrValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(STRVALUES$0, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "strValues" element
     */
    public boolean isSetStrValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STRVALUES$0) != 0;
        }
    }
    
    /**
     * Sets the "strValues" element
     */
    public void setStrValues(com.hp.schemas.ucmdb._1.types.StrList strValues)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(STRVALUES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(STRVALUES$0);
            }
            target.set(strValues);
        }
    }
    
    /**
     * Appends and returns a new empty "strValues" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList addNewStrValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(STRVALUES$0);
            return target;
        }
    }
    
    /**
     * Nils the "strValues" element
     */
    public void setNilStrValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(STRVALUES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(STRVALUES$0);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "strValues" element
     */
    public void unsetStrValues()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STRVALUES$0, 0);
        }
    }
}
