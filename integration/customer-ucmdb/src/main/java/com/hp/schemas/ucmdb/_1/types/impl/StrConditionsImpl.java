/*
 * XML Type:  StrConditions
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.StrConditions
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML StrConditions(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class StrConditionsImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.StrConditions
{
    
    public StrConditionsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STRCONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "strCondition");
    
    
    /**
     * Gets array of all "strCondition" elements
     */
    public com.hp.schemas.ucmdb._1.types.StrCondition[] getStrConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(STRCONDITION$0, targetList);
            com.hp.schemas.ucmdb._1.types.StrCondition[] result = new com.hp.schemas.ucmdb._1.types.StrCondition[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "strCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.StrCondition getStrConditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrCondition)get_store().find_element_user(STRCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "strCondition" element
     */
    public int sizeOfStrConditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STRCONDITION$0);
        }
    }
    
    /**
     * Sets array of all "strCondition" element
     */
    public void setStrConditionArray(com.hp.schemas.ucmdb._1.types.StrCondition[] strConditionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(strConditionArray, STRCONDITION$0);
        }
    }
    
    /**
     * Sets ith "strCondition" element
     */
    public void setStrConditionArray(int i, com.hp.schemas.ucmdb._1.types.StrCondition strCondition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrCondition)get_store().find_element_user(STRCONDITION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(strCondition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "strCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.StrCondition insertNewStrCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrCondition)get_store().insert_element_user(STRCONDITION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "strCondition" element
     */
    public com.hp.schemas.ucmdb._1.types.StrCondition addNewStrCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrCondition target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrCondition)get_store().add_element_user(STRCONDITION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "strCondition" element
     */
    public void removeStrCondition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STRCONDITION$0, i);
        }
    }
}
