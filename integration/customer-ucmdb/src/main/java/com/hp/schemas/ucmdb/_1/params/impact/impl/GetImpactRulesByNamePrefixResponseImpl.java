/*
 * XML Type:  getImpactRulesByNamePrefixResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * An XML getImpactRulesByNamePrefixResponse(@http://schemas.hp.com/ucmdb/1/params/impact).
 *
 * This is a complex type.
 */
public class GetImpactRulesByNamePrefixResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefixResponse
{
    
    public GetImpactRulesByNamePrefixResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPACTRULES$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "impactRules");
    
    
    /**
     * Gets the "impactRules" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRules getImpactRules()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRules target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRules)get_store().find_element_user(IMPACTRULES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "impactRules" element
     */
    public boolean isSetImpactRules()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IMPACTRULES$0) != 0;
        }
    }
    
    /**
     * Sets the "impactRules" element
     */
    public void setImpactRules(com.hp.schemas.ucmdb._1.types.impact.ImpactRules impactRules)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRules target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRules)get_store().find_element_user(IMPACTRULES$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRules)get_store().add_element_user(IMPACTRULES$0);
            }
            target.set(impactRules);
        }
    }
    
    /**
     * Appends and returns a new empty "impactRules" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.ImpactRules addNewImpactRules()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.ImpactRules target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.ImpactRules)get_store().add_element_user(IMPACTRULES$0);
            return target;
        }
    }
    
    /**
     * Unsets the "impactRules" element
     */
    public void unsetImpactRules()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IMPACTRULES$0, 0);
        }
    }
}
