/*
 * XML Type:  PropertiesList
 * Namespace: http://schemas.hp.com/ucmdb/1/types/props
 * Java type: com.hp.schemas.ucmdb._1.types.props.PropertiesList
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.props.impl;
/**
 * An XML PropertiesList(@http://schemas.hp.com/ucmdb/1/types/props).
 *
 * This is a complex type.
 */
public class PropertiesListImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.props.PropertiesList
{
    
    public PropertiesListImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PROPERTYNAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/props", "propertyName");
    
    
    /**
     * Gets array of all "propertyName" elements
     */
    public java.lang.String[] getPropertyNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(PROPERTYNAME$0, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "propertyName" element
     */
    public java.lang.String getPropertyNameArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTYNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "propertyName" elements
     */
    public org.apache.xmlbeans.XmlString[] xgetPropertyNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(PROPERTYNAME$0, targetList);
            org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "propertyName" element
     */
    public org.apache.xmlbeans.XmlString xgetPropertyNameArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PROPERTYNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return (org.apache.xmlbeans.XmlString)target;
        }
    }
    
    /**
     * Returns number of "propertyName" element
     */
    public int sizeOfPropertyNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PROPERTYNAME$0);
        }
    }
    
    /**
     * Sets array of all "propertyName" element
     */
    public void setPropertyNameArray(java.lang.String[] propertyNameArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(propertyNameArray, PROPERTYNAME$0);
        }
    }
    
    /**
     * Sets ith "propertyName" element
     */
    public void setPropertyNameArray(int i, java.lang.String propertyName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTYNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(propertyName);
        }
    }
    
    /**
     * Sets (as xml) array of all "propertyName" element
     */
    public void xsetPropertyNameArray(org.apache.xmlbeans.XmlString[]propertyNameArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(propertyNameArray, PROPERTYNAME$0);
        }
    }
    
    /**
     * Sets (as xml) ith "propertyName" element
     */
    public void xsetPropertyNameArray(int i, org.apache.xmlbeans.XmlString propertyName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PROPERTYNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(propertyName);
        }
    }
    
    /**
     * Inserts the value as the ith "propertyName" element
     */
    public void insertPropertyName(int i, java.lang.String propertyName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(PROPERTYNAME$0, i);
            target.setStringValue(propertyName);
        }
    }
    
    /**
     * Appends the value as the last "propertyName" element
     */
    public void addPropertyName(java.lang.String propertyName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROPERTYNAME$0);
            target.setStringValue(propertyName);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "propertyName" element
     */
    public org.apache.xmlbeans.XmlString insertNewPropertyName(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(PROPERTYNAME$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "propertyName" element
     */
    public org.apache.xmlbeans.XmlString addNewPropertyName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PROPERTYNAME$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "propertyName" element
     */
    public void removePropertyName(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PROPERTYNAME$0, i);
        }
    }
}
