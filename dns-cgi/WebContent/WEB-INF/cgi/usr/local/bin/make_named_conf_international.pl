#!/usr/bin/perl

use strict;
use warnings;

use lib "/usr/local/www/wanchai/netops/modules";
#use lib "/usr/local/www/wanchai/modules";
use CMS_DB;
use CMS_SECDNS_DB;

my $FILE_PATH;

my @now = gmtime;
my ($yr,$mth,$mday,$second, $minute, $hour) = ($now[5], $now[4], $now[3],$now[0],$now[1],$now[2]);
$yr += 1900;
$mth += 1;
if ($mth <= 9) { $mth = "0".$mth; }
if ($mday <= 9) { $mday = "0".$mday; }
if ($hour<=9){$hour = "0".$hour;}
my $cur_sec = (($hour*3600)+($minute*60)+$second);
my $cur_time = "$hour:$minute:$second";
my @ar_time;
my @hr_format = '';
my @sec_format = '';
my @db_date1 = '';
my @db_mon = '';
my @db_day = '';
my @db_year = '';
my @req_time ='';
my @records = '';


CMS_DB::connect_to_database;

my $secdnstime = CMS_SECDNS_DB::time_db(0);



my $secdns = CMS_SECDNS_DB::get_pro_secdns_entries_location(0);
use constant FILE_PATH_INTERNATIONAL => "/usr/local/www/wanchai/netops/data/secdns/international/secondary";
open (NAMED2, ">".FILE_PATH_INTERNATIONAL);
foreach my $entry (@$secdns) {
  my ($domain, $primary_ip,) = @$entry;
  print "$domain,$primary_ip\n";
  print NAMED2 <<END_OF_ENTRY
  zone "$domain" in {
        type slave;
        transfer-source 192.168.39.58;
        file "slave/$domain.bak";
        masters {
                $primary_ip;
        };
  };
END_OF_ENTRY
}



#get request_time from DB
foreach my $row (@$secdnstime){
        push (@ar_time, $$row[0]);
        push (@req_time, $$row[0]);
	 print "time:$$row[0]\n";
        my @db_hr1 = split(/\+/,$$row[0]);
        my @db_hr = split(/ /,$db_hr1[0]);
        my @db_sec1 = split(/\./,$db_hr[1]);
        my $db_sec = (($db_sec1[0]*3600)+ ($db_sec1[1]*60) + ($db_sec1[2]));
        my @db_date1 = split (/-/,$db_hr[0]);
        push (@sec_format, $db_sec);
        push (@hr_format, $db_hr[1]);
        push (@db_year,$db_date1[0]);
        push (@db_mon,$db_date1[1]);
        push (@db_day,$db_date1[2]);
shift (@ar_time);
}

my $len_ar = scalar @hr_format;
print $len_ar;
CMS_DB::connect_to_database;
my $lpct = 0;
        for ($lpct = 0; $lpct <= ($len_ar-1); $lpct++)
        {
        $mday =~ s/^\s+|\s+$//g;
        my $db_day1 = shift @db_day;
        $db_day1 =~ s/^\s+|\s+$//g;
        $mth =~ s/^\s+|\s+$//g;
        my $db_mon1 = shift @db_mon;
        $db_mon1  =~ s/^\s+|\s+$//g;
        $yr =~ s/^\s+|\s+$//g;
        my $db_year1 = shift @db_year;
        $db_year1 =~ s/^\s+|\s+$//g;
        #my $trial1 = $db_mon1*5;
        my $sec_format1 = shift @sec_format;

	print  "mday:$mday\tdb_day:$db_day1\tmth:$mth\tdb_mon:$db_mon1\tcur_sec:$cur_sec\tsec_format:$sec_format1 \n\n";

        #update acl only if recently added/deleted entries
        if (($mday eq $db_day1) && ($mth eq $db_mon1) && ($yr eq $db_year1) && (($cur_sec - $sec_format1)<=3600) ){
				my $secdns = CMS_SECDNS_DB::get_pro_secdns_entries;

			print "inside time check \n";	
                my $reqtime = $req_time[$lpct];
                 my $all_val = CMS_SECDNS_DB::all_values($reqtime,0);
                foreach my $rec (@$all_val){
                        push (@records, $$rec[0]);
                        push (@records, $$rec[1]);
                        push (@records, $$rec[2]);
                        push (@records, $$rec[3]);
                        push (@records, $$rec[4]);
                        push (@records, $$rec[5]);
                        print "record1 $records[0]\n";
                        print "record2 $records[1]\n";
                        print "record3 $records[2]\n";
                        print "record4 $records[3]\n";
                        print "record5 $records[4]\n";
                        print "record5 $records[5]\n";
                        my $value_type = '';
                        if (($records[4] == 1)||($records[4] == 2)) { $value_type = "REQUESTED"; }
                        elsif (($records[4] == 3)||($records[4] == 4)) { $value_type = "CANCELLED";}
                        my $accno = $records[1];
                        my $sdns_dom = $records[2];
                        my $sdns_ip = $records[3];
                        my $sdns_contact = $records[5];
                        my $mail = CMS_SECDNS_DB::SDNS_mail($accno, $sdns_contact,$sdns_dom,$sdns_ip,$value_type);
                        @records = '';

                }#end of foreach loop
        }#end of if loop
        }#end of for loop

CMS_SECDNS_DB::complete_secdns_entries;

CMS_DB::disconnect_from_database;
#close (NAMED);
