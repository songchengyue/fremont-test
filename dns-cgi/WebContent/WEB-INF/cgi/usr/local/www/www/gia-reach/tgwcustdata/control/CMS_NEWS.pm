# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
#
# $Id: CMS_NEWS.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $

package CMS_NEWS;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_NEWS_DB;

our @ISA = qw (Exporter);

our $error;

sub get_news_requests {
  $CMS::html_values{requests}{table} =
    CMS_NEWS_DB::search_news_feeds(@_);
  unshift(@{$CMS::html_values{requests}{table}},
	  ["Operations Handle", "Customers' incoming", "Customer's outgoing",
	   "Path", "Delay", "Request time"]);
  $CMS::html_values{requests}{header_rows} = 1;
}

sub start {
  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} eq "Cancel"))) {
    # default to view_recent_requests
    
    $CMS::html_values{command}{value} = "view_recent_requests";

    
    
  }

  if ($CMS::html_values{command}{value} eq "view_recent_requests") {
    get_news_requests;

    TI_HTML::set_datetime_search_values
	(\%CMS::html_values, "search_date",
	 "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");


    CMS::output_html("news_list");
  
  
  } elsif ($CMS::html_values{command}{value} eq "search_requests") {

    if (defined($CMS::html_values{submit}{value}) &&
	$CMS::html_values{submit}{value} eq "Search") {
      my ($start,$end) = TI_HTML::get_datetime_search_values
	(\%CMS::html_values, "search_date");
      get_news_requests
	($CMS::html_values{search_accno}{value},
	 $CMS::html_values{search_incoming}{value},
	 $CMS::html_values{search_outgoing}{value},
	 $CMS::html_values{search_path}{value},
	 $CMS::html_values{search_delay}{value},
	 $start, $end);
	
    } else {
    
      $CMS::html_values{search_accno}{value} = "";
      $CMS::html_values{search_incoming}{value} = "";
      $CMS::html_values{search_outgoing}{value} = "";
      $CMS::html_values{search_path}{value} = "";
      $CMS::html_values{search_delay}{value} = "";

      TI_HTML::set_datetime_search_values
	  (\%CMS::html_values, "search_date",
	   "2001-01-01 00:00:00", ((gmtime(time))[5]+1900)."-12-31 23:59:59");
      
    
    }
    # nothing entered
    CMS::output_html("news_list");
  } 
  
  

}

1;
