/*
 * XML Type:  IntParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML IntParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IntParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.IntParams
{
    
    public IntParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INTPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intParam");
    
    
    /**
     * Gets array of all "intParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.IntProp[] getIntParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INTPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.IntProp[] result = new com.hp.schemas.ucmdb._1.types.IntProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "intParam" element
     */
    public com.hp.schemas.ucmdb._1.types.IntProp getIntParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().find_element_user(INTPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "intParam" element
     */
    public int sizeOfIntParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTPARAM$0);
        }
    }
    
    /**
     * Sets array of all "intParam" element
     */
    public void setIntParamArray(com.hp.schemas.ucmdb._1.types.IntProp[] intParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(intParamArray, INTPARAM$0);
        }
    }
    
    /**
     * Sets ith "intParam" element
     */
    public void setIntParamArray(int i, com.hp.schemas.ucmdb._1.types.IntProp intParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().find_element_user(INTPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(intParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "intParam" element
     */
    public com.hp.schemas.ucmdb._1.types.IntProp insertNewIntParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().insert_element_user(INTPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "intParam" element
     */
    public com.hp.schemas.ucmdb._1.types.IntProp addNewIntParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntProp)get_store().add_element_user(INTPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "intParam" element
     */
    public void removeIntParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTPARAM$0, i);
        }
    }
}
