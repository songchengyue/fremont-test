/*
 * XML Type:  getImpactRulesByNamePrefix
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact;


/**
 * An XML getImpactRulesByNamePrefix(@http://schemas.hp.com/ucmdb/1/params/impact).
 *
 * This is a complex type.
 */
public interface GetImpactRulesByNamePrefix extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetImpactRulesByNamePrefix.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("getimpactrulesbynameprefix2551type");
    
    /**
     * Gets the "cmdbContext" element
     */
    com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext();
    
    /**
     * Sets the "cmdbContext" element
     */
    void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext);
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext();
    
    /**
     * Gets array of all "ruleNamePrefixFilter" elements
     */
    java.lang.String[] getRuleNamePrefixFilterArray();
    
    /**
     * Gets ith "ruleNamePrefixFilter" element
     */
    java.lang.String getRuleNamePrefixFilterArray(int i);
    
    /**
     * Gets (as xml) array of all "ruleNamePrefixFilter" elements
     */
    org.apache.xmlbeans.XmlString[] xgetRuleNamePrefixFilterArray();
    
    /**
     * Gets (as xml) ith "ruleNamePrefixFilter" element
     */
    org.apache.xmlbeans.XmlString xgetRuleNamePrefixFilterArray(int i);
    
    /**
     * Returns number of "ruleNamePrefixFilter" element
     */
    int sizeOfRuleNamePrefixFilterArray();
    
    /**
     * Sets array of all "ruleNamePrefixFilter" element
     */
    void setRuleNamePrefixFilterArray(java.lang.String[] ruleNamePrefixFilterArray);
    
    /**
     * Sets ith "ruleNamePrefixFilter" element
     */
    void setRuleNamePrefixFilterArray(int i, java.lang.String ruleNamePrefixFilter);
    
    /**
     * Sets (as xml) array of all "ruleNamePrefixFilter" element
     */
    void xsetRuleNamePrefixFilterArray(org.apache.xmlbeans.XmlString[] ruleNamePrefixFilterArray);
    
    /**
     * Sets (as xml) ith "ruleNamePrefixFilter" element
     */
    void xsetRuleNamePrefixFilterArray(int i, org.apache.xmlbeans.XmlString ruleNamePrefixFilter);
    
    /**
     * Inserts the value as the ith "ruleNamePrefixFilter" element
     */
    void insertRuleNamePrefixFilter(int i, java.lang.String ruleNamePrefixFilter);
    
    /**
     * Appends the value as the last "ruleNamePrefixFilter" element
     */
    void addRuleNamePrefixFilter(java.lang.String ruleNamePrefixFilter);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ruleNamePrefixFilter" element
     */
    org.apache.xmlbeans.XmlString insertNewRuleNamePrefixFilter(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ruleNamePrefixFilter" element
     */
    org.apache.xmlbeans.XmlString addNewRuleNamePrefixFilter();
    
    /**
     * Removes the ith "ruleNamePrefixFilter" element
     */
    void removeRuleNamePrefixFilter(int i);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix newInstance() {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.params.impact.GetImpactRulesByNamePrefix) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
