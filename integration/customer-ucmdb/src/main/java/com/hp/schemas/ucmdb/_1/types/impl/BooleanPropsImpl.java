/*
 * XML Type:  BooleanProps
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.BooleanProps
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML BooleanProps(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class BooleanPropsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.BooleanProps
{
    
    public BooleanPropsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName BOOLEANPROP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "booleanProp");
    
    
    /**
     * Gets array of all "booleanProp" elements
     */
    public com.hp.schemas.ucmdb._1.types.BooleanProp[] getBooleanPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(BOOLEANPROP$0, targetList);
            com.hp.schemas.ucmdb._1.types.BooleanProp[] result = new com.hp.schemas.ucmdb._1.types.BooleanProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "booleanProp" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanProp getBooleanPropArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().find_element_user(BOOLEANPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "booleanProp" element
     */
    public int sizeOfBooleanPropArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BOOLEANPROP$0);
        }
    }
    
    /**
     * Sets array of all "booleanProp" element
     */
    public void setBooleanPropArray(com.hp.schemas.ucmdb._1.types.BooleanProp[] booleanPropArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(booleanPropArray, BOOLEANPROP$0);
        }
    }
    
    /**
     * Sets ith "booleanProp" element
     */
    public void setBooleanPropArray(int i, com.hp.schemas.ucmdb._1.types.BooleanProp booleanProp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().find_element_user(BOOLEANPROP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(booleanProp);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "booleanProp" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanProp insertNewBooleanProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().insert_element_user(BOOLEANPROP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "booleanProp" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanProp addNewBooleanProp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().add_element_user(BOOLEANPROP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "booleanProp" element
     */
    public void removeBooleanProp(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BOOLEANPROP$0, i);
        }
    }
}
