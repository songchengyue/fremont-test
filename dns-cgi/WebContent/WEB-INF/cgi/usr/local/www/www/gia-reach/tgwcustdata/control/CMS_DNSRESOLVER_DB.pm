# (C) Telstra 2009
#
# Author: Meena Prakash
# Date: 1 April 2009
# File: CMS_DNSRESOLVER_DB.pm
#
# $Id: CMS_DNSRESOLVER_DB.pm,v 1.6 2016/07/20 15:37:02 d804709 Exp $


package CMS_DNSRESOLVER_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;
use CMS_OLSS;
use Mail::Sendmail;
use Log::Log4perl;
#Logger Initialisation
my $log_conf =$OS_PARA::values{logfile}{value}; 
Log::Log4perl::init($log_conf);
my $logger = Log::Log4perl->get_logger();


# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;

our $error;

# Select dns record types
# Pre  : contact_email, host, record_parameter
# Prost: $result
sub get_dns_resolver_entry {
open (DEBUG, ">/tmp/DB_DNS");
 $logger->debug("in resolver sub accno : out------->>>>>>>>>>>: inside");
CMS_DB::begin_transaction();
print DEBUG "INSIDE ACL\n";
        my $uid     = '';
        my $accno   = '';
        my $contact = '';
        my $host    = '';
        my $parm    = '';
        my $parm_val = '';
        my $uid     = $dbh->quote(shift @_);
        my $accno   = $dbh->quote(shift @_);
        my $contact = $dbh->quote(shift @_);
        my $host    = $dbh->quote(shift @_);
        my $parm    = $dbh->quote(shift @_);
        my $service_id   = $dbh->quote(shift @_);
        my $ip_version = $dbh->quote(shift @_);		## uday added
        my $parm_val = $parm;
        my $temp_sql = '';
        my $success = 1;
        my $ti = '';
        my $match_ip = '';
        my $r2 = '';
        my $r3 = '';
        my $sth = '';
        my $r1 = '';
        my $r = '';
        my $temp_x  = '';
	my ($div_val,$ip_block_chk1) = '';
	 my @z1_octets = "";
        my $z1_octet1 = "";
        my $z1_octet2 = "";
        my $z1_octet3 = "";
        my $z1_octet4 = "";
		my $z1_octet5 = "";
	    my $z1_octet6 = "";
	    my $z1_octet7 = "";
	    my $z1_octet8 = "";
        my $z1_bin_1 = "";
        my $z1_bin_2 = "";
        my $z1_bin_3 = "";
        my $z1_bin_4 = "";
		my $z1_bin_5 = "";
	    my $z1_bin_6 = "";
	    my $z1_bin_7 = "";
	    my $z1_bin_8 = "";
        my @z1_binary = "";
        my $z1_binary_stat = "";
        my @z1_binary_bit = "";
        my @ip_octets = "";
        my $ip_octet1 = "";
        my $ip_octet2 = "";
        my $ip_octet3 = "";
        my $ip_octet4 = "";
		my $ip_octet5 = "";
	     my $ip_octet6 = "";
	     my $ip_octet7 = "";
	     my $ip_octet8 = "";
        my $ip_bin_1 = "";
        my $ip_bin_2 = "";
        my $ip_bin_3 = "";
        my $ip_bin_4 = "";
			my $ip_bin_5 = "";
	        my $ip_bin_6 = "";
	        my $ip_bin_7 = "";
	        my $ip_bin_8 = "";
        my @ip_binary = "";
        my $ip_binary_stat = "";
        my @ip_binary_bit = "";
        my $ip_length = "";
        my $ip_binary_result = "";
        my $z1_binary_result = "";
        my $loop_count2 = 0;
	my $ipprefix = "";
	my $ipnew_chk1='';
	my $compare_flag=0;
 	my $ip_flag = 1;	
$logger->debug("DB accno: $accno\n contact-INITIAL: $contact\t host: $host\t parm: $parm\n match_ip:$match_ip\n, ip version:$ip_version ");
print DEBUG "DB accno: $accno\n contact-INITIAL: $contact\t host: $host\t parm: $parm\n match_ip:$match_ip\n"; 
#################### for version 6 uday added#################
 my @y;
  my $z1;
   my $z2;
   my @ip_octet;
   my $ip_octets1;
   my $ip_octets2;
   my $ip_octets3;
   my $ip_octets4;
   my $ip_octets5 = "";
	my $ip_octets6 = "";
	my $ip_octets7 = "";
	my $ip_octets8 = "";
if ($ip_version =~ '4'){
 $logger->debug("ipversion 4 ");
        $parm_val =~ s/\'//g;
        @y = split(/\//,$parm_val);
        $z1= shift(@y);
        $z2= shift(@y);
	$ti = rindex($z1,".");
        $match_ip  = substr($z1,0,$ti);
	@ip_octet =  split(/\./,$z1);
	$ip_octets1 = shift(@ip_octet);
	$ip_octets2 = shift(@ip_octet);
	$ip_octets3 = shift(@ip_octet);
	$ip_octets4 = shift(@ip_octet);
		$logger->debug("ip_octets4 : $ip_octets4  and Z2: $z2");
} else {
$logger->debug("ipversion 6 ");
   $parm_val =~ s/\'//g;
   @y = split(/\//,$parm_val);
    $z1= shift(@y);
	  $z2= shift(@y);
   $logger->debug("iparam value $parm_val ");
   my $ipv6 = Net::IP::ip_expand_address($z1, 6);
   $logger->debug("ipv666: $ipv6");
    @ip_octets = split(/\:/,$ipv6);
   foreach my $count (3 .. $#ip_octets){
			$logger->debug("ipv octet loopppppp");
			$ip_octets4 = join ":",$ip_octets[$count] ;					#---------check 
			$match_ip  = substr($z1,0,14);	
		}
  
  
	$logger->debug("ip_octets4 : $ip_octets4 Z2:$z2 ");
}
print DEBUG "DB match_ip before entering loop:$match_ip ti:$ti\n";
print DEBUG "DB parm_value: $parm_val z1:$z1 z2:$z2\n";
$logger->debug("in resolver sub accno : out------->>>>>>>>>>>: $service_id, $ip_octets4");
$logger->debug("in resolver sub uid : out------->>>>>>>>>>>: $uid");
$logger->debug("in resolver sub param : out------->>>>>>>>>>>: $parm_val");
# check for whether the accno exist or not
        my $sth = $dbh->prepare ("SELECT count(accno) FROM olss_cust_access_acc WHERE accno like $accno");
        #$sth->execute();
       #my $r = $sth->fetchall_arrayref;
	my $r =1;
$logger->debug("if : in 2nd------->>>>>>>>>>>: $r");

if($r >= 1) {
$logger->debug("if : in------->>>>>>>>>>>: $match_ip");
# username checked in ip_resource block
$logger->debug("check : accno------->>>>>>>>>>>: $accno");
	my $sth3 = $dbh->prepare ("SELECT count(ip_block) FROM ip_resource WHERE ip_block like '$match_ip%' AND customer_id like $accno AND dns_service like 'Yes' ");
	$sth3->execute();
	my $rt = $sth3->fetchall_arrayref;
$logger->debug("if : out 2nd------->>>>>>>>>>>:kkkk $rt->[0]->[0]");
if ($rt->[0]->[0] >= 1){
$logger->debug("if : in 2nd------->>>>>>>>>>>: $rt->[0]->[0]");
# Same User not allowed for DNS Resolver Service for more than 10 requests.
        #my $sth = $dbh->prepare ("SELECT count(o.username) FROM  dns_resolver p, olss_cust_access_acc o where o.accno = p.accno and o.username ilike $uid");
        my $sth = $dbh->prepare ("SELECT count(username) FROM  dns_resolver where username like $uid");
        $sth->execute();
        my $r1 = $sth->fetchall_arrayref;

  if($r1->[0]->[0] < 10) {
$logger->debug("if : in 3------->>>>>>>>>>>: $r1->[0]->[0]");

        $ti = rindex($z1,".");
		if($ip_version =~ '4'){
		 $match_ip  = substr($z1,0,$ti);
		}else {
		$match_ip  = substr($z1,0,14);
		}
       
print DEBUG "accno: $accno\n contact-INITIAL: $contact\t host: $host\t parm: $parm\n match_ip:$match_ip\n";

        # check for whether the IP already exist in dns_resolver
        my $sth = $dbh->prepare ("SELECT count(value) FROM dns_resolver WHERE value like $parm and username like $uid and status = 1");
        $sth->execute();
        $r3 = $sth->fetchall_arrayref;
print DEBUG "username check r3:$r3\n";
$logger->debug("ip check : ------->>>>>>>>>>>: $r3->[0]->[0]");

	# CHECK WHEN IP IS EXACTLY SAME
$logger->debug("ip & accno check : ------->>>>>>>>>>>: $parm_val, $accno ");
        # check for whether the IP exist or not
        my $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$parm_val' AND customer_id like $accno AND dns_service like 'Yes'");
        $sth->execute();
        my $r2 = $sth->fetchrow_hashref;
$logger->debug("actual check : ------->>>>>>>>>>>: $r2");
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
print DEBUG "DB temp_x:$temp_x\t \n";
$logger->debug(" check : ------->>>>>>>>>>>: $$r2{ip_block}");
	  if($$r2{ip_block} =~ /$parm_val/) {
$logger->debug(" check : ------->>>>>>>>>>>: ffff");	
	print DEBUG "DB inside IP same r2 loop\n";
	   if($r3->[0]->[0] < 1) {
	print DEBUG "inside r3 loop\n";
	my $sth3 = $dbh->prepare ("SELECT count(value) FROM dns_resolver WHERE value like $parm and username like $uid and status = 3");
	$sth3->execute();
	my $r4 = $sth3->fetchall_arrayref;
	my $sth6 = $dbh->prepare ("SELECT value from dns_resolver WHERE value like '$match_ip%' and username like $uid and status = 1");
	$sth6->execute();
	my $r6 = $sth6->fetchall_arrayref;
	if($r4->[0]->[0] < 1) {
	for my $q_count (0 .. $#{$r6}){
	print DEBUG "inside for r6 loop\n";
	$ip_flag = binary_add($z1,$parm_val,$match_ip);
	print DEBUG "ip_flag:$ip_flag\n";
	}
	$logger->debug(" before ip_flag check : ------->>>>>>>>>>>: $ip_flag");
        if ($ip_flag == 0){
		$logger->debug(" in update query : ------->>>>>>>>>>>: $ip_flag");
		print DEBUG "inside ip_flag 0 condition\n";
		my $temp_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$match_ip%' and username like $uid"; 
	        $sth = $dbh->prepare ("$temp_sql");
		my $success = $sth->execute;
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
        	my $result = ($success ? $dbh->commit : $dbh->rollback);
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
        	CMS_DB::disconnect_from_transaction();
        	return $error if ($error);
        	#return $result;
		return $success;
		}else{
print DEBUG "CHECK when IP is SAME\n";
		#$uid = 'h999748';
		#$uid = "\'$uid\'";
print DEBUG "uid: $uid\n";
###############################added by uday for expanded ip to be stored####################################
if($ip_version=~'4'){
               ##beow query added by uday with service_id
			   $parm=uc$parm;
        	my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $parm, 1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";

	        $sth = $dbh->prepare ("$temp_sql");
print DEBUG "prepare: $temp_sql\n";
       		#my $success &&= $sth->execute;
		my $success = $sth->execute;
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "success-initial: $success\n";
        	my $result = ($success ? $dbh->commit : $dbh->rollback);
print DEBUG "result before: $result\n";
print DEBUG "success: $success\n";
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "error: $error\n result: $result\n";
        	CMS_DB::disconnect_from_transaction();
        	return $error if ($error);
#        	return $result;
		return $success;
		}else {
		my @z = split(/\//,$parm);
         my $ip= shift(@z);
	     my $prefix= shift(@z);
        $logger->debug("iparm value $parm ");
        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
       $logger->debug("ipv666: $ipv6");
	       my $ipblock6 = join "/",$ipv6,$prefix;
		   $ipblock6=uc$ipblock6;
		my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $ipblock6, 1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";

	        $sth = $dbh->prepare ("$temp_sql");
print DEBUG "prepare: $temp_sql\n";
       		#my $success &&= $sth->execute;
		my $success = $sth->execute;
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "success-initial: $success\n";
        	my $result = ($success ? $dbh->commit : $dbh->rollback);
print DEBUG "result before: $result\n";
print DEBUG "success: $success\n";
        	$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "error: $error\n result: $result\n";
        	CMS_DB::disconnect_from_transaction();
        	return $error if ($error);
#        	return $result;
		return $success;
		}
	}} else {
	$logger->debug(" in update query else : ------->>>>>>>>>>>:");
	my $temp_sql = "UPDATE dns_resolver SET status=1, request_time=CURRENT_TIMESTAMP,date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value = $parm and username like $uid";
	$sth = $dbh->prepare ("$temp_sql");
        print DEBUG "prepare: $temp_sql\n";
                #my $success &&= $sth->execute;
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "success-initial: $success\n";
                my $result = ($success ? $dbh->commit : $dbh->rollback);
print DEBUG "success: $success\n";
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                CMS_DB::disconnect_from_transaction();
print DEBUG "error: $error\n result: $result\n";
                return $error if ($error);
                #return $result;
                return $success;
	
        	}
	}
	 print DEBUG "parm else of r3: $parm\n";

	CMS_DB::disconnect_from_transaction();
           return 1;
	  } 


        #  START_CHECK_4_OCTETS_SAME
$logger->debug(" to insert check : ------->>>>>>>>>>>: $z1");
        my $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$z1%' and customer_id like $accno AND dns_service like 'Yes'");
        $sth->execute();
        my $r4 = $sth->fetchall_arrayref;
$logger->debug(" array check : ------->>>>>>>>>>>: $r4");
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
print DEBUG "DB 4octets same temp_x:$temp_x\t \n";
	for my $lp_count (0 .. $#{$r4}) {
	my $Ar_chk = $r4->[$lp_count]->[0];
	my $ip_block_chk = $Ar_chk;
	print DEBUG "DB ip from db is $ip_block_chk\n";

	my @ip_chk = split(/\//,$ip_block_chk);
	my $ipnew_chk  = shift(@ip_chk);#ipnew_chk has ip
	my $ipprefixip_chk = shift(@ip_chk);#has prefix
 $logger->debug(" ip checks ------->>>>>>>>>>>:$ipprefixip_chk, $ipnew_chk");
print DEBUG "DB ipnew_chk: $ipnew_chk\t \t ipprefixip_chk: $ipprefixip_chk\n"; 
	# CHECK WHEN 4 Octets are SAME and IP PREFIX is 24
        if ( ($ipnew_chk =~ /$z1/) && ($ipprefixip_chk == 24) ) {
 $logger->debug(" insdied ------->>>>>>>>>>>:$ipprefixip_chk");
print DEBUG "ipprefixip_chk: $ipprefixip_chk\n";
                if ($z2 > $ipprefixip_chk) {
                        print DEBUG "entered z2> loop\n";
                        my $ct_ip = ($z2 - $ipprefixip_chk);
                        print DEBUG "ct_ip:$ct_ip\n";
                        my $ct_ip1 = 0;
                        my $new_z2 = 0;
                        print DEBUG "z2:$z2\n";
                        my $z3 = $z2;
                                for ($ct_ip1 = 0; $ct_ip1 <= $ct_ip; $ct_ip1++){
                                        print DEBUG "ct_ip1:$ct_ip1\n";
                                        $new_z2 = ($z3 - 1);
                                        $z3 = ($z3 - 1);
                                        my $x2 = `$z2 - 1`;
                                        print DEBUG "x2 : $x2\n";
                                        print DEBUG "new_z2:$new_z2\n";
                                        print DEBUG "z3:$z3\n";
                                        my $new_last = join "/",$z1, $new_z2;
                                        print DEBUG "new_last:$new_last\n";
                                        my $sth = $dbh -> prepare("SELECT count(value) from dns_resolver where value like '$new_last'  and customer_id like $accno and username like $uid and status = 1");
                                        $sth->execute();
                                        my $query = $sth->fetchall_arrayref;
                                $logger->debug(" check : ------->>>>>>>>>>>:$query->[0]->[0]");
                                        if ($query->[0]->[0]>=1){
                                                print DEBUG "already exists 24\n";
				return 1;
                                                }#end of if query loop
                                }#end of for loop

                        }#end of ip z2 loop
                my $sth = $dbh->prepare("SELECT count(*) from dns_resolver where customer_id like $accno and username like $uid");
                $sth -> execute();
                my $count_ptr = $sth->fetchall_arrayref;
$logger->debug(" out insert : ------->>>>>>>>>>>:$count_ptr->[0]->[0]");
                print DEBUG "count ptr:$count_ptr->[0]->[0]\n";
                if ($count_ptr->[0]->[0] > 0) {
$logger->debug(" insert 1 if : ------->>>>>>>>>>>:$count_ptr->[0]->[0]");
                        if($r3->[0]->[0] >= 1) {
$logger->debug(" insert 2 if : ------->>>>>>>>>>>:$r3->[0]->[0]");
                        print DEBUG "already exists in the DB\n";
$logger->debug("in resolver sub details inseert : out------->>>>>>>>>>>: $uid, $accno, $contact, $host , $parm");
                            ## below query added by uday with service id 
#########################added by uday for ipv6 expanded  insertion##################
if($ip_version=~'4'){			
$parm=uc$parm;				
                         my $temp_sql = "INSERT into dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";
                        $sth = $dbh->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         my $result = ($success ? $dbh->commit : $dbh->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         CMS_DB::disconnect_from_transaction(); ## using dbh 7/8/16
                         return $error if ($error);
                         #return $result;
		                 return $success;
                        
						}else{
						my @z = split(/\//,$parm);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $parm ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc$ipblock6;
						my $temp_sql = "INSERT into dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $ipblock6,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";
                        $sth = $dbh->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         my $result = ($success ? $dbh->commit : $dbh->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         CMS_DB::disconnect_from_transaction(); ## using dbh 7/8/16
                         return $error if ($error);
                         #return $result;
		 return $success;
						}
						}
                        } else {
                        print DEBUG "4 oct before insert1\n";
                              ## below query added by uday with service id 
#########################added by uday for ipversion6  expandedd toinsert###########################						  
						if($ip_version=~'4'){	
                        $parm=uc$parm;						
                         my $temp_sql = "INSERT into dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";
                        $sth = $dbh->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         my $result = ($success ? $dbh->commit : $dbh->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         #CMS_DB::disconnect_from_transaction(); ## using dbh_tr 7/8/16
                         return $error if ($error);
                         #return $result;
		 return $success;
		 }else{
		              my @z = split(/\//,$parm);
                        my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $parm ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc$ipblock6;
						my $temp_sql = "INSERT into dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $ipblock6,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";
                        $sth = $dbh->prepare ("$temp_sql");
                         my $success = $sth->execute;
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         my $result = ($success ? $dbh->commit : $dbh->rollback);
                         $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                         #CMS_DB::disconnect_from_transaction(); ## using dbh_tr 7/8/16
                         return $error if ($error);
                         #return $result;
		 return $success;
		 }
                }#end of if else 
                if($r3->[0]->[0] < 1) {
                        my $sth = $dbh->prepare("SELECT count(value) from dns_resolver where value like '$match_ip%' and customer_id like $accno and username like $uid'");
                        print DEBUG "sth:$sth\n";
                        $sth->execute();
                        my $result = $sth->fetchall_arrayref;
                        if (($result->[0]->[0]>=1) && ($z2 == 24)){
				    $logger->debug(" in update query second loop : ------->>>>>>>>>>>result: $result->[0]->[0]");
                                print DEBUG "most recently added update\n";
                                print DEBUG "UPDATE1error\n";
                                my $temp_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$match_ip%' and username like $uid";
                                $sth = $dbh->prepare ("$temp_sql");
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                my $result = ($success ? $dbh->commit : $dbh->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                CMS_DB::disconnect_from_transaction();
                                return $error if ($error);
                                #return $result;
                                return $success; 
		        
                                }else {
                        if ($z2 != 24){
print DEBUG "inside z2 ne 24 loop\n";
my %eq_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);

my $eq_initial = 0;
my $eq_count_ip = (32 - $z2);
my $eq_count_ip1 = 0;
my $eq_latestz2 = $z2;
my $check_rs = 0;
for ($eq_count_ip1 = 0; $eq_count_ip1 <= $eq_count_ip; $eq_count_ip1++) {
print DEBUG "eq_count_ip:$eq_count_ip\teq_count_ip1:$eq_count_ip1\n";
my $v_next = $eq_latestz2 + 1;
my $eq_w_range = $eq_range{$v_next};
my $eq_hop = 256/$eq_w_range;
my $eq_i = 0;

        for ($eq_i = 1; $eq_i <= $eq_hop; $eq_i++) {
                print DEBUG "inside hop loop for z2 ne 24 condition\n";
                my $sth = $dbh-> prepare("SELECT count(value) from dns_resolver where value like '$match_ip.$eq_initial/$v_next'  and username like $uid and customer_id like $accno and status = 1");
                $sth->execute();
                my $rs = $sth->fetchall_arrayref;
                print DEBUG "eq_initial/:$eq_initial\t$eq_w_range\tv_next:$v_next\trs:$rs->[0]->[0]\n";
                        if ($rs->[0]->[0]>=1){
                                print DEBUG "ip_octet4:$ip_octet4 before if check\n";
                                if (($eq_initial >= $ip_octet4) && ($ip_octet4 <= ($eq_initial+$eq_w_range))){
                                $check_rs = $check_rs + 1;
                                 $logger->debug(" in update query second loop : ------->>>>>>>>>>>result2: $rs->[0]->[0]");
                                print DEBUG "UPDATE2error\n";
                                my  $temp_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$match_ip.$eq_initial/$v_next' and username like $uid";
                $sth = $dbh->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                CMS_DB::disconnect_from_transaction();
                return $error if ($error);
                #return $result;
                return $success;
		 }#end of if ipoct check
                                }#end of rs >= 1

                $eq_initial = $eq_initial+$eq_w_range;

        }#end for for i

        $eq_initial = 0;
        $eq_w_range = $eq_w_range+$eq_w_range;
        $eq_latestz2 = $eq_latestz2 +1;
        if ($eq_latestz2 == 32) { last;}
        print DEBUG "eq_latestz2:$eq_latestz2\teq_w_range:$eq_w_range\n";


}#end of for count ip
        if ($check_rs >= 1){
                    return 1;
                }else {
                        print DEBUG "4 oct before insert2 prefix not 24\n";
                                     ## below query added by uday with service id 
########################################## added by uday for ipv6 expanded   insertion#########	
if($ip_version=~'4'){						$parm=uc$parm; 
                                 my $temp_sql = "INSERT into dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";
                                $sth = $dbh->prepare ("$temp_sql");
                                $check_rs = $check_rs+1;
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                my $result = ($success ? $dbh->commit : $dbh->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction(); ## using dbh_tr 7/8/16
                                return $error if ($error);
                                return $result;
                                
								}else{
								 my @z = split(/\//,$parm);
                                my $ip= shift(@z);
	                               my $prefix= shift(@z);
                                  $logger->debug("iparm value $parm ");
                               my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                               $logger->debug("ipv666: $ipv6");
	                              my $ipblock6 = join "/",$ipv6,$prefix;
								  $ipblock6=uc$ipblock6;
								 my $temp_sql = "INSERT into dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $ipblock6,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";
                                $sth = $dbh->prepare ("$temp_sql");
                                $check_rs = $check_rs+1;
                                my $success = $sth->execute;
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                my $result = ($success ? $dbh->commit : $dbh->rollback);
                                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction(); ## using dbh_tr 7/8/16
                                return $error if ($error);
                                return $result;
                                }
								}
								

                }#end of z2 if
                        }#end of result else
                }else {
                        #print DEBUG "inside 24 ipp update cond\n";
                        print DEBUG "UPDATE3error\n";
                        #my $temp_sql = "UPDATE dns_resolver SET fqdn = $dom, request_time = current_timestamp, contact = $contact,status = 1,value = '$value' WHERE ptrrec like $ptr and accno like $accno and rec_type like 'NS' and status = 1 and last_oct like '$value'";
                                #$sth = $dbh->prepare ("$temp_sql");
                                #my $success = $sth->execute;
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #my $result = ($success ? $dbh->commit : $dbh->rollback);
                                #$error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                                #CMS_DB::disconnect_from_transaction();
                                #return $error if ($error);
                                #return $result;
                                return 1;
                                }


        #CMS_DB::disconnect_from_transaction();
        #return 1;
        }


	  # CHECK WHEN 4 Octets are SAME and IP PREFIX is different
 
	elsif ( ($ipnew_chk =~ /$z1/) && ($ipprefixip_chk != $z2) ) {
	$logger->debug(" inside $ip_block_chk");
print DEBUG "DB 4 Octets are SAME and IP PREFIX is different\n";
print DEBUG "DB ipprefixip_chk: $ipprefixip_chk\n";
print DEBUG "DB before check z2:$z2\n";
            if($z2 >= $ipprefixip_chk) {
print DEBUG "DB after check z2:$z2\n";
	my $sth5 = $dbh->prepare("SELECT count(value) FROM dns_resolver WHERE value like '$ip_block_chk' and username like $uid");
	$sth5->execute();
	my $rt1 = $sth5->fetchall_arrayref; 
	if ($rt1->[0]->[0] < 1) {
		if($r3->[0]->[0] < 1) {
		$logger->debug(" bfr insert");
                  ## below query added by uday with service id 
				  if($ip_version=~'4'){
				  $parm=uc$parm;
                my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";

                $sth = $dbh->prepare ("$temp_sql");
print DEBUG "DB inside loop CHECK WHEN 4 Octets are SAME and IP PREFIX is different\n";
print DEBUG "prepare2: $temp_sql\n";
                #my $success &&= $sth->execute;
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "success-initial: $success\n";
                my $result = ($success ? $dbh->commit : $dbh->rollback);
print DEBUG "success: $success\n";
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                CMS_DB::disconnect_from_transaction();
print DEBUG "error: $error\n result: $result\n";
                return $error if ($error);
                #return $result;
                return $success;
				}else{
				 my @z = split(/\//,$parm);
                                my $ip= shift(@z);
	                               my $prefix= shift(@z);
                                  $logger->debug("iparm value $parm ");
                               my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                               $logger->debug("ipv666: $ipv6");
	                              my $ipblock6 = join "/",$ipv6,$prefix;
								  $ipblock6=uc$ipblock6;
					 my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $ipblock6,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";

                $sth = $dbh->prepare ("$temp_sql");
print DEBUG "DB inside loop CHECK WHEN 4 Octets are SAME and IP PREFIX is different\n";
print DEBUG "prepare2: $temp_sql\n";
                #my $success &&= $sth->execute;
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
print DEBUG "success-initial: $success\n";
                my $result = ($success ? $dbh->commit : $dbh->rollback);
print DEBUG "success: $success\n";
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                CMS_DB::disconnect_from_transaction();
print DEBUG "error: $error\n result: $result\n";
                return $error if ($error);
                #return $result;
                return $success;			  
				
				}
	        }
	   }
	      CMS_DB::disconnect_from_transaction();		
              return 1;
	   }	
         } 

	} # ENDED_CHECK_4_OCTETS_SAME



        #  START_CHECK_3_OCTETS_SAME
print DEBUG "DB CHECK_3_OCTETS_SAME\n";
	my $db_flag = 0;
	print DEBUG "initial db_flag:$db_flag\n";
        my $sth = $dbh->prepare ("SELECT ip_block FROM ip_resource WHERE ip_block like '$match_ip%' and customer_id like $accno AND dns_service like 'Yes'");
        $sth->execute();
        my $r5 = $sth->fetchall_arrayref;
        $temp_x = $match_ip;
        $temp_x =~ s/\'//g;
print DEBUG "DB 3 temp_x:$temp_x\n";
	for my $lp_count (0 .. $#{$r5}) {
	my $Ar_chk1 = $r5->[$lp_count]->[0];
	$ip_block_chk1 = $Ar_chk1;
	print DEBUG "DB ip from db is $ip_block_chk1\n\n";

if ($ip_version =~ '4'){
        my @ip_chk1 = split(/\//,$ip_block_chk1);
        $ipnew_chk1  = shift(@ip_chk1);#ipnew_chk has ip
	$logger->debug("DB ip new check1 from db is $ipnew_chk1");

	my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
	$logger->debug("DB ip prefix check1 from db is $ipprefixip_chk1");

        my @ip_octets = split(/\./,$ipnew_chk1);
	$logger->debug("AFTER OCTET");

	#code to split the stored IPs into octets and changing them to binary
	$ip_octet1 = shift(@ip_octets);
	$ip_octet2 = shift(@ip_octets);
	$ip_octet3 = shift(@ip_octets);
	$ip_octet4 = shift(@ip_octets);
	#$last_oct_db = join "/",$ip_octet4,$ipprefixip_chk1;
	#print DEBUG "last oct from DB:$last_oct_db\n";

	$ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
	$ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
	$ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
	$ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

	#combining individual IPs into one variable/array
	@ip_binary = ();
	push(@ip_binary, $ip_bin_1);
	push(@ip_binary, $ip_bin_2);
	push(@ip_binary, $ip_bin_3);
	push(@ip_binary, $ip_bin_4);
	$ip_binary_stat = join("",@ip_binary);
	@ip_binary_bit = split(//,$ip_binary_stat);
	#print"ip binary is @ip_binary_bit\n";

	#code to split the entered IPs into octets and changing octets to binary.
	@z1_octets = split(/\./,$z1);
        $z1_octet1 = shift(@z1_octets);
        $z1_octet2 = shift(@z1_octets);
        $z1_octet3 = shift(@z1_octets);
        $z1_octet4 = shift(@z1_octets);

        $z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
        $z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
        $z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
        $z1_bin_4 = unpack("B8", pack('C',$z1_octet4));
	
	#combining the individual IPs into one variable/array
        @z1_binary = ();
        push(@z1_binary,$z1_bin_1);
        push(@z1_binary,$z1_bin_2);
        push(@z1_binary,$z1_bin_3);
        push(@z1_binary,$z1_bin_4);
        $z1_binary_stat = join("",@z1_binary);
        @z1_binary_bit = split(//,$z1_binary_stat);
        print DEBUG "input is @z1_binary_bit\n";
        $ipprefix = $ipprefixip_chk1;
			}
			if ($ip_version =~ '6'){
				my @ip_chk1 = split(/\//,$ip_block_chk1);
			    $ipnew_chk1= shift(@ip_chk1);#ipnew_chk has ip
				$logger->debug("DB ip check $ipnew_chk1");
				my $ipv6db = Net::IP::ip_expand_address($ipnew_chk1, 6);
				my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
				my @ip_octets = split(/\:/,$ipv6db);

			#code to split the stored IPs into octets and changing them to binary
				$ip_octet1 = shift(@ip_octets);
				$ip_octet2= shift(@ip_octets);
				$ip_octet3 = shift(@ip_octets);   
				$ip_octet4 = shift(@ip_octets);      
				$ip_octet5 = shift(@ip_octets);      
				$ip_octet6 = shift(@ip_octets);    
				$ip_octet7 = shift(@ip_octets);     
				$ip_octet8 = shift(@ip_octets);
     
				#$last_oct_db = join "/",$ip_octet8,$ipprefixip_chk1;
         #print $last_oct_db;
				#print DEBUG "last oct from DB:$last_oct_db\n";
		#close STDOUT;

				$ip_bin_1 = unpack("B16", pack('H4',$ip_octet1));
				$ip_bin_2 = unpack("B16", pack('H4',$ip_octet2));
				$ip_bin_3= unpack("B16", pack('H4',$ip_octet3));
				$ip_bin_4 = unpack("B16", pack('H4',$ip_octet4));
				$ip_bin_5 = unpack("B16", pack('H4',$ip_octet5));
				$ip_bin_6 = unpack("B16", pack('H4',$ip_octet6));
				$ip_bin_7 = unpack("B16", pack('H4',$ip_octet7));
				$ip_bin_8= unpack("B16", pack('H4',$ip_octet8));
        #combining individual IPs into one variable/array
				@ip_binary = ();
				push(@ip_binary, $ip_bin_1);
				push(@ip_binary, $ip_bin_2);
				push(@ip_binary, $ip_bin_3);
				push(@ip_binary, $ip_bin_4);
				push(@ip_binary, $ip_bin_5);
				push(@ip_binary, $ip_bin_6);
				push(@ip_binary, $ip_bin_7);
				push(@ip_binary, $ip_bin_8);
				$ip_binary_stat = join(",",@ip_binary);
				@ip_binary_bit = split(//,$ip_binary_stat);
				my @y = split(/\//,$parm_val);
		$z1= shift(@y);
		$logger->debug("z1: $z1");
        my $ipv6 = Net::IP::ip_expand_address($z1, 6);
                   $logger->debug("IPbinory $ip_binary_stat ");
		#code to split the entered IPs into octets and changing octets to binary.
		$logger->debug("IPv6  $ipv6 ");
				@z1_octets = split(/\:/,$ipv6);
				$z1_octet1 = shift(@z1_octets);
				$z1_octet2 = shift(@z1_octets);
				$z1_octet3 = shift(@z1_octets);
				$z1_octet4 = shift(@z1_octets);
				$z1_octet5 = shift(@z1_octets);
				$z1_octet6 = shift(@z1_octets);
				$z1_octet7 = shift(@z1_octets);
				$z1_octet8 = shift(@z1_octets);

				$z1_bin_1 = unpack("B16", pack('H4',$z1_octet1));
				$z1_bin_2 = unpack("B16", pack('H4',$z1_octet2));
				$z1_bin_3 = unpack("B16", pack('H4',$z1_octet3));
				$z1_bin_4 = unpack("B16", pack('H4',$z1_octet4));
				$z1_bin_5 = unpack("B16", pack('H4',$z1_octet5));
				$z1_bin_6 = unpack("B16", pack('H4',$z1_octet6));
				$z1_bin_7 = unpack("B16", pack('H4',$z1_octet7));
				$z1_bin_8 = unpack("B16", pack('H4',$z1_octet8));

        #combining the individual IPs into one variable/array
				@z1_binary = ();
				push(@z1_binary,$z1_bin_1);
				push(@z1_binary,$z1_bin_2);
				push(@z1_binary,$z1_bin_3);
				push(@z1_binary,$z1_bin_4);
				push(@z1_binary,$z1_bin_5);
				push(@z1_binary,$z1_bin_6);
				push(@z1_binary,$z1_bin_7);
				push(@z1_binary,$z1_bin_8);
				$z1_binary_stat = join(",",@z1_binary);
				@z1_binary_bit = split(//,$z1_binary_stat);
				print DEBUG "input is @z1_binary_bit\n";
				$logger->debug("Binary : $z1_binary_stat ");
				$ipprefix = $ipprefixip_chk1;
			}
      $logger->debug("ipnew check $ipnew_chk1 match $match_ip, prefix:$ipprefix");
	# CHECK WHEN 3 Octets are SAME and IP PREFIX is diff 
        if  ($ipnew_chk1 =~ /$match_ip/)  {
	print DEBUG "DB entered match ip case\n";
		$logger->debug("MATCHES!!!!");
        if($r3->[0]->[0] < 1) {
		$logger->debug("MATCHES R3XXXXXX");
		print DEBUG "DB does not already exist in DB\n";
		print DEBUG "z2:$z2\tipprefix:$ipprefix\n";
		if($z2 > $ipprefix){$ip_length = $ipprefix;}
		else {$ip_length = $z2;}
		print DEBUG "ip length value:$ip_length\n";
		$logger->debug("ip length value:$ip_length"); 
		$compare_flag = 1;
	 	$db_flag = 0;	
		for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
		print DEBUG "entered for condition\n";
        		if ($db_flag == 0){
			print DEBUG "DB entered if compare flag condition\n";
       		 	$z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
        		$ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
				$logger->debug( " OUTSIDE if loop DB db flag 1   bin rsult = $z1_binary_result  ip_bin rsult = $ip_binary_result  \n");
        			if ($z1_binary_result != $ip_binary_result){
				print DEBUG "DB binary ADD false\n";
				print DEBUG "DB db flag 1\n";
					 $logger->debug( "DB db flag 1   bin rsult = $z1_binary_result  ip_bin rsult = $ip_binary_result  \n");
                		$db_flag = 1;
                		}
				elsif ($z1_binary_result == $ip_binary_result)
				{
				print DEBUG "DB equal to binary add\n";	
				print DEBUG "DB db flag 0\n";
				$db_flag = 0;}
			} 
			
		}#end of both if loops and for loop
		$logger->debug( "FLAG checks db flag:$db_flag, compare flag:$compare_flag ");
	if ($db_flag == 1 && $compare_flag ==1)
		{
		print DEBUG "DB compare flag 1\n";
		$compare_flag = 1;}
	else 
		{
		print DEBUG "DB compare flag 0\n";
		$compare_flag = 0;
		last;
		}
        }#end of r3 if condition
else {	CMS_DB::disconnect_from_transaction();
        return 1;
}#end of else r3
        }#end of ip if condition
}#end of for condition
if ($compare_flag == 1) {
print DEBUG "COMPARE flag found to be 1\n";
print DEBUG "throw error\n";
CMS_DB::disconnect_from_transaction();
return "DNS Resolve Request cannot be proceeded with the entered IP. Please Enter A New IP";
}#end of if
elsif ($compare_flag == 0){
	print DEBUG "DB final check of compare flag 0\n";
	$logger->debug( "DB final check of compare flag 0  z2: $z2  ipprefix : $ipprefix");
	if ($z2 < $ipprefix) {
	print DEBUG "DB entered IP prefix less thn Db ip prefix\n";
	print DEBUG "parent sub range\n";
	if($$r2{ip_block} =~ /$parm_val/) {
		if($r3->[0]->[0] < 1) {
		my $t_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$z1%' and username like $uid";
		print DEBUG "$t_sql\n";
		CMS_DB::begin_transaction();
		my $sth = $dbh->prepare ("$t_sql");
		my $result = $sth->execute;
		print DEBUG "result:$result\n";
		$error = $dbh->errstr;
		print DEBUG "error:$error\n";
		#return $result;
                return $success;
		CMS_DB::disconnect_from_transaction();
		}	
	}}
	else {
	print DEBUG "success\n";
	print DEBUG "entered else of $z2 > $ipprefix\n";
	 print DEBUG "sub range\n";
	print DEBUG "parm_val:$parm_val\n";
	print DEBUG "entered subrange parm block\n";
	$logger->debug( "entered else of $z2 > $ipprefix  param: $parm uid : $uid");
 my $sthr = $dbh->prepare ("SELECT count(value) FROM dns_resolver WHERE value like $parm and username like $uid and status = 1");
         $sthr->execute();
        my $r_1 = $sthr->fetchall_arrayref;
        print DEBUG "outside r_1 condn\n";
        print DEBUG "($r_1->[0]->[0])\n";
        if($r_1->[0]->[0] < 1) {
        print DEBUG "inside r_1 condition\n";
		$logger->debug( "inside r_1 condition");
        if($r3->[0]->[0] < 1) {
        print DEBUG "entered r3 loop\n";
		$logger->debug( "entered r3 loop");
################ changes for IPV6##############uday#############
my %v_range;

my $v_initial;
my $count_ip;
my $count_ip1;	
my $latestz2;	
my $v_previous;
if($ip_version =~ '4'){		
%v_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2,32=>1);
$v_initial = 0;
$count_ip = ($z2 - $ipprefix);
print DEBUG "ipprefix:$ipprefix\n";
$count_ip1 = 0;
 $latestz2 = $z2;
		$logger->debug( "latest z3 : $latestz2 count ip[  : $count_ip");
		} else{
		
	  %v_range = (48=>65536, 49=>32768, 50=>16384, 51=>8192, 52=>4096, 53=>2048, 54=>1024, 55=>512, 56=>256, 57=>128, 58=>64, 59=>32, 60=>16, 61=>8, 62=>4, 63=>2, 64=>1);
				$logger->debug("VERSION 6 ");
				$v_initial = 0;
$count_ip = ($z2 - $ipprefix);
print DEBUG "ipprefix:$ipprefix\n";
$count_ip1 = 0;
 $latestz2 = $z2;
		$logger->debug( "latest z3 : $latestz2 count ip[  : $count_ip");
		}
for ($count_ip1 = 1; $count_ip1 <= $count_ip; $count_ip1++) {
if($latestz2 eq '48' or $latestz2 eq '24'){
 $v_previous = $latestz2;
}else{
 $v_previous = $latestz2 -1;
 }
my $w_range = $v_range{$v_previous};
my $v_hop;
print DEBUG "w_range:$w_range\n";
if($ip_version =~ '4'){
$v_hop = (256/($w_range));
} else{
$v_hop = (65536/($w_range));
}
print DEBUG "v_previous:$v_previous\tw_range:$w_range\tv_hop:$v_hop\n";
my $i = 0;
print DEBUG "beforei loop\n";
print DEBUG "count_ip:$count_ip\tcount_ip1:$count_ip1\n";
        for ($i = 1; $i <= $v_hop; $i++) {
                print DEBUG "inside i-v_hop loop\n";
                my $sth = $dbh-> prepare("SELECT count(value) from dns_resolver where value like '$match_ip.$v_initial/$v_previous' and customer_id like $accno and status = 1");
                $sth->execute();
                print DEBUG "i inside :$i\n";
                my $rs = $sth->fetchall_arrayref;
                        print DEBUG  "SELECT count(value) from dns_resolver where value like '$match_ip.$v_initial/$v_previous'  and customer_id like $accno and status = 1\n";
                        print DEBUG "rs:$rs->[0]->[0]\n";
                        print DEBUG "ip_octets4:$ip_octets4 before if check\n";
                        if ($rs->[0]->[0]>=1){
                                print DEBUG "v_initial:$v_initial\tw_range:$w_range\n";
                                if (($v_initial <= $ip_octets4) && ($ip_octets4 <= ($v_initial+$w_range))){
                                                print DEBUG "just before throwin error..newly removed compare condition\n";
                                                return 1;
                                }#end of ipoct if
                        }#end of if rs

                $v_initial = $v_initial+$w_range;
                print DEBUG "after rs loop:v_initial:$v_initial\tw_range:$w_range\n";
                print DEBUG "for i loop ends\n";
        }#end for for i
        $v_initial = 0;
		if($latestz2 eq '48' or $latestz2 eq '24'){
		$latestz2 = $latestz2;
		}else{
        $latestz2 = $latestz2 -1;
		}
        $w_range = $w_range+$w_range;

}#end of for count

#to check for parent IP
my %new_v_range;
my $check_rs;
my $new_v_initial;
my $new_count_ip;
my $new_count_ip1;
my $new_latestz2;
my $v_next ;
if($ip_version =~ '4'){
%new_v_range = (24=>256, 25 => 128, 26=>64, 27=>32, 28=>16, 29=>8, 30=>4, 31=>2, 32=>1);
 $check_rs = 0;
 $new_v_initial = 0;
 $new_count_ip = (32 - $z2);
 $new_count_ip1 = 0;
$new_latestz2 = $z2;
 $v_next ;
 } else {
 %new_v_range= (48=>65536, 49=>32768, 50=>16384, 51=>8192, 52=>4096, 53=>2048, 54=>1024, 55=>512, 56=>256, 57=>128, 58=>64, 59=>32, 60=>16, 61=>8, 62=>4, 63=>2, 64=>1);
 $check_rs = 0;
 $new_v_initial = 0;
 $new_count_ip = (64 - $z2);
 $new_count_ip1 = 0;
$new_latestz2 = $z2;
 $v_next ;
 }
print DEBUG "new_latestz2:$new_latestz2\n";

for ($new_count_ip1 = 0; $new_count_ip1 <= $new_count_ip; $new_count_ip1++) {
print DEBUG "new_count_ip1:$new_count_ip1\tnew_count_ip:$new_count_ip\n";

if($new_latestz2 eq '32' or  $new_latestz2 eq '64' or $new_latestz2 eq '24' or $new_latestz2 eq '48'){
 $v_next = $new_latestz2;
}
else{
$v_next = $new_latestz2 + 1;
}
my $new_w_range = $new_v_range{$v_next};
my $new_v_hop;
if($ip_version =~ '4'){
$new_v_hop = 256/$new_w_range;
}else{
$new_v_hop = 65536/$new_w_range;
 $logger->debug( "version 6 $new_v_hop");
}
my $new_i = 0;
print DEBUG "v_next:$v_next\tnew_w_range:$new_w_range\tnew_v_hop:$new_v_hop\n";
 $logger->debug( "un initialised value ip octect $ip_octets4, new initial $new_v_initial");
 ###################################### version 4 and 6 differentiated #######################################################
 if ($ip_version =~'4'){
        for ($new_i = 1; $new_i <= $new_v_hop; $new_i++) {
                print DEBUG "new_i:$new_i\tnew_v_hop:$new_v_hop\n";
                print DEBUG "before select count:new_v_initial:$new_v_initial\tv_next:$v_next\n";
                my $t_r = "SELECT count(value) from dns_resolver where value like '$match_ip.$new_v_initial/$v_next'  and customer_id like $accno and status = 1";
                print DEBUG "t_r:$t_r\n";
                my $sth_r = $dbh->prepare("$t_r");
                $sth_r->execute();
                my $rs_r = $sth_r->fetchall_arrayref;
                                print DEBUG "rs:$rs_r->[0]->[0]\n";
                       print DEBUG "ip_octets4:$ip_octets4 before if check\n";
					  
                        if ($rs_r->[0]->[0]>=1){
                                print DEBUG "inside rs 1 condition rs:$rs_r->[0]->[0]\tip_octets4:$ip_octets4\n";
								 $logger->debug( "un initialised value : $new_v_initial   : $new_w_range");
                                if (($new_v_initial >= $ip_octets4) && ($ip_octets4 <= ($new_v_initial+$new_w_range)))					{
                                                print DEBUG "UPDATE6error\n";
												  
						$check_rs = $check_rs+1;
		my $temp_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$match_ip.$new_v_initial/$v_next' and username like $uid";
		print DEBUG "temp_sql:$temp_sql\n";
                $sth = $dbh->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                CMS_DB::disconnect_from_transaction();
                return $error if ($error);
                #return $result;
                return $success;

    }#end of if oct
                        }#end of if rs

                $new_v_initial = $new_v_initial+$new_w_range;

        }#end for for i ip version 4 ###################
		}else {
		for ($new_i = 1; $new_i <= 2; $new_i++) {
                print DEBUG "new_i:$new_i\tnew_v_hop:$new_v_hop\n";
                print DEBUG "before select count:new_v_initial:$new_v_initial\tv_next:$v_next\n";
                my $t_r = "SELECT count(value) from dns_resolver where value like '$match_ip.$new_v_initial/$v_next'  and customer_id like $accno and status = 1";
                print DEBUG "t_r:$t_r\n";
                my $sth_r = $dbh->prepare("$t_r");
                $sth_r->execute();
                my $rs_r = $sth_r->fetchall_arrayref;
                                print DEBUG "rs:$rs_r->[0]->[0]\n";
                       print DEBUG "ip_octets4:$ip_octets4 before if check\n";
					  
                        if ($rs_r->[0]->[0]>=1){
                                print DEBUG "inside rs 1 condition rs:$rs_r->[0]->[0]\tip_octets4:$ip_octets4\n";
								 $logger->debug( "un initialised value : $new_v_initial   : $new_w_range");
                                if (($new_v_initial >= $ip_octets4) && ($ip_octets4 <= ($new_v_initial+$new_w_range)))					{
                                                print DEBUG "UPDATE6error\n";
												  
						$check_rs = $check_rs+1;
		my $temp_sql = "UPDATE dns_resolver SET status = 1,request_time = CURRENT_TIMESTAMP, date_assigned = CURRENT_TIMESTAMP, date_last_update = CURRENT_TIMESTAMP, contact = $contact, host = $host, value = $parm where value like '$match_ip.$new_v_initial/$v_next' and username like $uid";
		print DEBUG "temp_sql:$temp_sql\n";
                $sth = $dbh->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                CMS_DB::disconnect_from_transaction();
                return $error if ($error);
                #return $result;
                return $success;

    }#end of if oct
                        }#end of if rs

                $new_v_initial = $new_v_initial+$new_w_range;

        }###end of for loop v6
		
		} #end of else v6
###################################### version 4 and 6 differentiated #######################################################
          $logger->debug( "after end of for loop 1 $new_v_initial");    
        $new_v_initial = 0;
        $new_w_range = $new_w_range+$new_w_range;
        $new_latestz2 = $new_latestz2 +1;
		    $logger->debug( "b/w $new_v_initial");   
        if ($new_latestz2 == 32) { last;}

}#end of for count ip
$logger->debug( "after end of for loop 2 $new_v_initial"); 
if ($check_rs >= 1){ 
return 1;
         }

 print DEBUG "before insert\n";
 $logger->debug( "before insert $new_v_initial");
## below query added by uday with service id 
##################added by uday for ipv6 expandedn insertion#########
if($ip_version=~'4'){ $parm=uc$parm;
                my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";
                $sth = $dbh->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                #CMS_DB::disconnect_from_transaction(); ##dbh_tr 7/8/16
                return $error if ($error);
#                return $result;
	   return $success;
	   }else{
	   my @z = split(/\//,$parm);
                                my $ip= shift(@z);
	                               my $prefix= shift(@z);
                                  $logger->debug("iparm value $parm ");
                               my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                               $logger->debug("ipv666: $ipv6");
	                              my $ipblock6 = join "/",$ipv6,$prefix;
								  $ipblock6=uc$ipblock6;
			 my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update, service_id) VALUES ($uid, $accno, $contact, $host , $ipblock6,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";
                $sth = $dbh->prepare ("$temp_sql");
                my $success = $sth->execute;
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                #CMS_DB::disconnect_from_transaction(); ##dbh_tr 7/8/16
                return $error if ($error);
#                return $result;
	   return $success;					  
	   
	   }
                } else {
				print DEBUG "IP block already exists in the DB, else of r3\n";
				if($ip_version=~'4'){$parm=uc$parm;
## below query added by uday with service id 
                        my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update,service_id) VALUES ($uid, $accno, $contact, $host , $parm,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";
                        $sth = $dbh->prepare ("$temp_sql");
                        my $success = $sth->execute;
                        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                        my $result = ($success ? $dbh->commit : $dbh->rollback);
                        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                        #CMS_DB::disconnect_from_transaction(); ##dbh_tr 7/8/16
                        return $error if ($error);
                        return $success;
						}
						else{
						 my @z = split(/\//,$parm);
                                my $ip= shift(@z);
	                               my $prefix= shift(@z);
                                  $logger->debug("iparm value $parm ");
                               my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                               $logger->debug("ipv666: $ipv6");
	                              my $ipblock6 = join "/",$ipv6,$prefix;
								  $ipblock6=uc$ipblock6;
								   my $temp_sql = "INSERT INTO dns_resolver (username, customer_id, contact, host, value,status, request_time, date_assigned, date_last_update,service_id) VALUES ($uid, $accno, $contact, $host , $ipblock6,1, CURRENT_TIMESTAMP, null, CURRENT_TIMESTAMP, $service_id)";
                        $sth = $dbh->prepare ("$temp_sql");
                        my $success = $sth->execute;
                        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                        my $result = ($success ? $dbh->commit : $dbh->rollback);
                        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;
                        #CMS_DB::disconnect_from_transaction(); ##dbh_tr 7/8/16
                        return $error if ($error);
                        return $success;
						
						}
                        }#else of r3

        }else {print DEBUG "range already exists in DB\n";
                if ($ip_block_chk1 eq $parm){
                print DEBUG "UPDATE7error\n";
 return 1;
                } else {
                print DEBUG "sub range case..throw error\n";
                return 1;
                }
        }
 print DEBUG "r3 neither <1 nor else condition\n";
        }
}#end of elsif


  # ENDED_CHECK_3_OCTETS_SAME


        print DEBUG "FINAL ERROR: DNS RESOLVE REQUEST cannot be proceeded with the Entered IP. Please Enter A New IP.\n";
        return "DNS Resolve Request cannot be proceeded with the entered IP. Please Enter A New IP";

	} else {
	print DEBUG "User Exceeded Allowed Limit for DNS Resolve Service (max limit 10).\n";
	return "User Exceeded Allowed Limit for DNS Resolve Service - max limit 10";
	}
}else {
 print DEBUG "IP Address block does not exist in ip_resource\n";
return "DNS Resolve Request cannot be proceeded with the entered IP. Please Enter A New IP"; 
}
} else {
     print DEBUG "Account does not exist.\n";
     return "Account does not exist";
}

close (DEBUG);
}

open (DBG , ">/tmp/view_dnsreslv");
print DBG "CHECK1\n";

###
### uses CMS_OLSS to get the DATA using dbh handler
###
sub view_dns_resolver {
print DBG "CHECK2\n";
        my $username = $dbh->quote(shift @_);
$logger->debug("in sub username : out------->>>>>>>>>>>: $username");
print DBG "CHECK3 username: $username\n";
        my $sth = $dbh->prepare
                ("SELECT customer_id, contact, host, value, request_time
                        FROM dns_resolver
                        WHERE username = $username and status = 1
                        ORDER BY request_time desc
                ");

        unless ($sth->execute) {
                $error = $dbh->errstr;
$logger->debug("in resolver view sub : out------->>>>>>>>>>>:$error");
                return 0;
        }
    
        my $result = $sth->fetchall_arrayref;
print DBG "result: $result->[0]->[0] \n";
$logger->debug("in resolver view sub : out------->>>>>>>>>>>: $result->[0]->[0]");
        $error = $dbh->errstr;
close (DBG);
$logger->debug("in resolver vw sub : out------->>>>>>>>>>>: $result->[0]->[0]");
        return $result;
}

open (HKG, ">/tmp/get_acl");
sub get_acl_parm {
print HKG "ENTERED\n";
        my $sth = $dbh->prepare("SELECT value from dns_resolver where status=1");

        unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;

        $error = $dbh->errstr;
        return $error if ($error);
        return $result;
}
close (HKG);
open (HKG, ">/tmp/cancel_resolver");
sub cancel_dns_resolver {
print HKG "ENTERED\n";
        my $uid     = $dbh->quote(shift @_);
        my $accno   = $dbh->quote(shift @_);
        my $contact = $dbh->quote(shift @_);
        my $host    = $dbh->quote(shift @_);
        my $parm    = $dbh->quote(shift @_);
		my $ip_version    = $dbh->quote(shift @_);
        my $parm_val = $parm;
        my $result = '';
print HKG "uid: $uid\n accno: $accno\n contact: $contact\n host: $host\n parm: $parm\n parm_val: $parm_val\n\n";

        $parm_val =~ s/\'//g;
        my @y = split(/\//,$parm_val);
        my $z1= shift(@y);
        my $z2= shift(@y);
print HKG "z1: $z1\n z2: $z2\n";

# Users who have requested for DNS Service are only allowed to cancel the Service.
       # my $sth = $dbh->prepare ("SELECT count(p.username) FROM  dns_resolver p, olss_cust_access_acc o where o.accno = p.accno and p.username like $uid");
	 my $sth = $dbh->prepare ("SELECT count(username) FROM  dns_resolver  where username like $uid");
        $sth->execute();
        my $r1 = $sth->fetchall_arrayref;
if($r1->[0]->[0] >= 1) {

# username checked in ip_resource block
      #  my $sth3 = $dbh->prepare ("SELECT count(ip_block) FROM ip_resource WHERE ip_block like '$parm_val', accno like $accno");
#print HKG "DB ip resource check parm_val:$parm_val accno:$accno\n";
      #  $sth3->execute();
#print HKG "DB sth3:$sth\n";
      #  my $rt = $sth3->fetchall_arrayref;
##print HKG "DB rt:$rt\n";
#print HKG "entered ip resource check\n";
if($ip_version=~'4'){
$parm=uc$parm;
my $sth1 = $dbh->prepare ("SELECT count(value) FROM dns_resolver where value = $parm and username = $uid");
        $sth1->execute();
        my $r2 = $sth1->fetchall_arrayref;
print HKG "DB r2:$r2\n";
print HKG "DB inside r1 loop\n";
        if($r2->[0]->[0] >= 1){

        my $success = 1;

       # $sth = $dbh->prepare("DELETE FROM dns_resolver WHERE value = $parm");
	$sth = $dbh->prepare("UPDATE dns_resolver SET status = 3, request_time = CURRENT_TIMESTAMP  WHERE value = $parm and username = $uid and status=1");
        $success &&= $sth->execute;
        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;

print HKG "error: $error\n";
        $result = ($success ? $dbh->commit : $dbh->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;

print HKG "error: $error\t result: $result\n";
print HKG "OUTSIDE1 result: $result\n ";
        return $error if ($error);
        #return $result;
	return $success;
}else{
$result = 0;
print HKG "OUTSIDE2 result: $result\n ";
return $result;
}
}elsif($ip_version=~'6'){
                        my @z = split(/\//,$parm);
                         my $ip= shift(@z);
	                    my $prefix= shift(@z);
                        $logger->debug("iparm value $parm ");
                        my $ipv6 = Net::IP::ip_expand_address($ip, 6);
                        $logger->debug("ipv666: $ipv6");
	                    my $ipblock6 = join "/",$ipv6,$prefix;
						$ipblock6=uc$ipblock6;
my $sth1 = $dbh->prepare ("SELECT count(value) FROM dns_resolver where value = $ipblock6 and username = $uid");
        $sth1->execute();
        my $r2 = $sth1->fetchall_arrayref;
print HKG "DB r2:$r2\n";
print HKG "DB inside r1 loop\n";
        if($r2->[0]->[0] >= 1){

        my $success = 1;

       # $sth = $dbh->prepare("DELETE FROM dns_resolver WHERE value = $parm");
	$sth = $dbh->prepare("UPDATE dns_resolver SET status = 3, request_time = CURRENT_TIMESTAMP  WHERE value = $ipblock6 and username = $uid and status=1");
        $success &&= $sth->execute;
        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;

print HKG "error: $error\n";
        $result = ($success ? $dbh->commit : $dbh->rollback);
        $error = (length($error)!=0) ? $error."<br>".$dbh->errstr : $dbh->errstr;

print HKG "error: $error\t result: $result\n";
print HKG "OUTSIDE1 result: $result\n ";
        return $error if ($error);
        #return $result;
	return $success;
}else{
$result = 0;
print HKG "OUTSIDE2 result: $result\n ";
return $result;
}

}
} else {
$result = "User $uid does not have the rights to cancel DNS Service for this IP Block\n";;
print HKG "OUTSIDE2 result: $result\n ";
return $result;
}
close (HKG);
}


sub count_rows {
open (HKG, ">/tmp/count_r");
print HKG "ENTERED\n";
        my $sth = $dbh->prepare("SELECT count(*) from dns_resolver");
print HKG "sth: $sth\n";
       unless ($sth->execute) {
                $error = $dbh->errstr;
                return 0;
        }

        my $result = $sth->fetchall_arrayref;
	my $count = ($result->[0]->[0]);
print HKG "result: ($result->[0]->[0])\n";
print HKG "count: $count\n";
        $error = $dbh->errstr;
        return $error if ($error);
       return $count;

close (HKG);
}

sub all_values {
open (HKG, ">/tmp/all_val");
print HKG "ENTERED\n";
#my ($new_ip) = $dbh->quote(shift @_);
my ($reqtime) = $dbh->quote(shift @_);
print HKG "$reqtime\n";
#my $temp = "SELECT username, accno, contact, host, value, status FROM dns_resolver where value like $new_ip";
my $temp = "SELECT username, accno, contact, host, value, status FROM dns_resolver where  request_time like $reqtime";
my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
$sth->execute();
my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
  return $result;

close (HKG);
}

sub DNSRESOLVER_mail_REQ {
  #my $firststr = shift(@_);
 my ($uid, $accno, $dns_contact, $dns_dom, $dns_aip, $value_type) = @_;
my @email_ar = split('^^', $uid);
  my $em_ar = shift(@email_ar);
  my $note = "Note: This email has been automatically generated. Please do not reply to this message as it is unattended.";
  my $message = ("$value_type DNS RESOLVER Service for following IP:\n\nUsername:\t$em_ar\nIP REQ:\t\t$dns_aip\nEmail:\t\t$dns_contact\nAccount:\t$accno\nHostname:\t$dns_dom\n\nThank you.\n\n$note");
  my $subj = "$value_type DNS RESOLVER SERVICE";

  my $to_mail = $dns_contact;
  my $bcc_mail = "TI.DL-GL-TI_EM\@team.telstra.com; TI.infosyscms\@team.telstra.com";
  my $from_mail = "\"Reach OLSS System\" (olss\@TelstraInternational.com)";
  my %mail = (
           To => $to_mail,
	   Bcc => $bcc_mail,
           From => $from_mail,
           Subject => $subj
  );
  my $hostname = `hostname`;
  print "$hostname";
#  $mail{Smtp} = 'postoffice.net.reach.com';
   $mail{Smtp} = '$hostname'; 
  $mail{body} = $message;

  if (sendmail (%mail)) {
    print "\nMail sent success to $to_mail.\n" ;
  } else {
    print "Error sending mail to $to_mail: $Mail::Sendmail::error \n";
  }

}

sub status_ip {
open (HKG, ">/tmp/status_ip");
print HKG "ENTERED\n";
my $temp = "SELECT value from dns_resolver where status=1";
        my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
	$sth->execute();
	my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
  	return $result;

close (HKG);
}

sub time_db {
open (HKG, ">/tmp/dns_time_rec");
print HKG "ENTERED\n";
my $temp = "SELECT request_time from dns_resolver";
        my $sth = $dbh->prepare("$temp");
print HKG "sth: $sth\n";
        $sth->execute();
        my $result = $sth->fetchall_arrayref;
print HKG "result: $result\n";
        return $result;

close (HKG);
}

sub binary_add {#binary and of ip blocks
my @z1_octets = "";
        my $z1_octet1 = "";
        my $z1_octet2 = "";
        my $z1_octet3 = "";
        my $z1_octet4 = "";
        my $z1_bin_1 = "";
        my $z1_bin_2 = "";
        my $z1_bin_3 = "";
        my $z1_bin_4 = "";
        my @z1_binary = "";
        my $z1_binary_stat = "";
        my @z1_binary_bit = "";
        my @ip_octets = "";
        my $ip_octet1 = "";
        my $ip_octet2 = "";
        my $ip_octet3 = "";
        my $ip_octet4 = "";
        my $ip_bin_1 = "";
        my $ip_bin_2 = "";
        my $ip_bin_3 = "";
        my $ip_bin_4 = "";
        my @ip_binary = "";
        my $ip_binary_stat = "";
        my @ip_binary_bit = "";
        my $ip_length = "";
        my $ip_binary_result = "";
        my $z1_binary_result = "";
        my $loop_count2 = 0;
        my $ipprefix = "";
        my $compare_flag=0;
my $db_flag=0;
my ($parm_val,$z1,$z2) = @_;

my @ip_chk1 = split(/\//,$parm_val);
my $ipnew_chk1  = shift(@ip_chk1);#ipnew_chk has ip
my $ipprefixip_chk1 = shift(@ip_chk1);#has prefix
my @ip_octets = split(/\./,$ipnew_chk1);
#code to split the stored IPs into octets and changing them to binary
$ip_octet1 = shift(@ip_octets);
$ip_octet2 = shift(@ip_octets);
$ip_octet3 = shift(@ip_octets);
$ip_octet4 = shift(@ip_octets);

$ip_bin_1 = unpack("B8", pack('C',$ip_octet1));
$ip_bin_2 = unpack("B8", pack('C',$ip_octet2));
$ip_bin_3 = unpack("B8", pack('C',$ip_octet3));
$ip_bin_4 = unpack("B8", pack('C',$ip_octet4));

#combining individual IPs into one variable/array
my @ip_binary = ();
push(@ip_binary, $ip_bin_1);
push(@ip_binary, $ip_bin_2);
push(@ip_binary, $ip_bin_3);
push(@ip_binary, $ip_bin_4);
$ip_binary_stat = join("",@ip_binary);
@ip_binary_bit = split(//,$ip_binary_stat);
#print"ip binary is @ip_binary_bit\n";

#code to split the entered IPs into octets and changing octets to binary.
        @z1_octets = split(/\./,$z1);
        $z1_octet1 = shift(@z1_octets);
        $z1_octet2 = shift(@z1_octets);
        $z1_octet3 = shift(@z1_octets);
        $z1_octet4 = shift(@z1_octets);

        $z1_bin_1 = unpack("B8", pack('C',$z1_octet1));
        $z1_bin_2 = unpack("B8", pack('C',$z1_octet2));
        $z1_bin_3 = unpack("B8", pack('C',$z1_octet3));
        $z1_bin_4 = unpack("B8", pack('C',$z1_octet4));

 #combining the individual IPs into one variable/array
        @z1_binary = ();
        push(@z1_binary,$z1_bin_1);
        push(@z1_binary,$z1_bin_2);
        push(@z1_binary,$z1_bin_3);
        push(@z1_binary,$z1_bin_4);
        $z1_binary_stat = join("",@z1_binary);
        @z1_binary_bit = split(//,$z1_binary_stat);
        print DEBUG "input is @z1_binary_bit\n";
        $ipprefix = $ipprefixip_chk1;
if($z2 > $ipprefix){$ip_length = $ipprefix;}
                else {$ip_length = $z2;}
                print DEBUG "ip length value:$ip_length\n";
                $compare_flag = 1;
                for ($loop_count2 = 0; $loop_count2 < $ip_length;++$loop_count2){
                print DEBUG "entered for condition\n";
                        if ($db_flag == 0){
                        print DEBUG "DB entered if compare flag condition\n";
                        $z1_binary_result= $z1_binary_bit[$loop_count2] & 1;
                        $ip_binary_result= $ip_binary_bit[$loop_count2] & 1;
                                if ($z1_binary_result != $ip_binary_result){
                                print DEBUG "DB binary ADD false\n";
                                print DEBUG "DB db flag 1\n";
                                $db_flag = 1;
                                }
                                elsif ($z1_binary_result == $ip_binary_result)
                                {
                                print DEBUG "DB equal to binary add\n";
                                print DEBUG "DB db flag 0\n";
                                $db_flag = 0;}
                        }
                }#end of both if loops and for loop
        if ($db_flag == 1 && $compare_flag ==1)
                {
                print DEBUG "DB compare flag 1\n";
                $compare_flag = 1;}
        else
                {
                print DEBUG "DB compare flag 0\n";
                $compare_flag = 0;}

return $compare_flag;

}
1;
