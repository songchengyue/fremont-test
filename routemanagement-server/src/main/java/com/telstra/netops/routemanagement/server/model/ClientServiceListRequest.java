package com.telstra.netops.routemanagement.server.model;

/**
 * Modal class for Service list
 *
 */
public class ClientServiceListRequest {

	private String contactEmailAddress;

	private String hostname;

	private String ipAddress;

	private IPVersion ipVersion; // default

	private String customerId;

	public ClientServiceListRequest() {
	}

	public ClientServiceListRequest(String contactEmailAddress, String hostname, String ipAddress, String customerId) {
		super();
		this.contactEmailAddress = contactEmailAddress;
		this.hostname = hostname;
		this.ipAddress = ipAddress;
		this.customerId = customerId;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public IPVersion getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(IPVersion ipVersion) {
		this.ipVersion = ipVersion;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}
