/*
 * XML Type:  CIsAndRelationsUpdates
 * Namespace: http://schemas.hp.com/ucmdb/1/types/update
 * Java type: com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.update.impl;
/**
 * An XML CIsAndRelationsUpdates(@http://schemas.hp.com/ucmdb/1/types/update).
 *
 * This is a complex type.
 */
public class CIsAndRelationsUpdatesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates
{
    
    public CIsAndRelationsUpdatesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CISFORUPDATE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/update", "CIsForUpdate");
    private static final javax.xml.namespace.QName RELATIONSFORUPDATE$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/update", "relationsForUpdate");
    private static final javax.xml.namespace.QName REFERENCEDCIS$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/update", "referencedCIs");
    private static final javax.xml.namespace.QName REFERENCEDRELATIONS$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types/update", "referencedRelations");
    
    
    /**
     * Gets the "CIsForUpdate" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs getCIsForUpdate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(CISFORUPDATE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "CIsForUpdate" element
     */
    public boolean isSetCIsForUpdate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CISFORUPDATE$0) != 0;
        }
    }
    
    /**
     * Sets the "CIsForUpdate" element
     */
    public void setCIsForUpdate(com.hp.schemas.ucmdb._1.types.CIs cIsForUpdate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(CISFORUPDATE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(CISFORUPDATE$0);
            }
            target.set(cIsForUpdate);
        }
    }
    
    /**
     * Appends and returns a new empty "CIsForUpdate" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs addNewCIsForUpdate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(CISFORUPDATE$0);
            return target;
        }
    }
    
    /**
     * Unsets the "CIsForUpdate" element
     */
    public void unsetCIsForUpdate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CISFORUPDATE$0, 0);
        }
    }
    
    /**
     * Gets the "relationsForUpdate" element
     */
    public com.hp.schemas.ucmdb._1.types.Relations getRelationsForUpdate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().find_element_user(RELATIONSFORUPDATE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "relationsForUpdate" element
     */
    public boolean isSetRelationsForUpdate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RELATIONSFORUPDATE$2) != 0;
        }
    }
    
    /**
     * Sets the "relationsForUpdate" element
     */
    public void setRelationsForUpdate(com.hp.schemas.ucmdb._1.types.Relations relationsForUpdate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().find_element_user(RELATIONSFORUPDATE$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().add_element_user(RELATIONSFORUPDATE$2);
            }
            target.set(relationsForUpdate);
        }
    }
    
    /**
     * Appends and returns a new empty "relationsForUpdate" element
     */
    public com.hp.schemas.ucmdb._1.types.Relations addNewRelationsForUpdate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().add_element_user(RELATIONSFORUPDATE$2);
            return target;
        }
    }
    
    /**
     * Unsets the "relationsForUpdate" element
     */
    public void unsetRelationsForUpdate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RELATIONSFORUPDATE$2, 0);
        }
    }
    
    /**
     * Gets the "referencedCIs" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs getReferencedCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(REFERENCEDCIS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "referencedCIs" element
     */
    public boolean isSetReferencedCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REFERENCEDCIS$4) != 0;
        }
    }
    
    /**
     * Sets the "referencedCIs" element
     */
    public void setReferencedCIs(com.hp.schemas.ucmdb._1.types.CIs referencedCIs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().find_element_user(REFERENCEDCIS$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(REFERENCEDCIS$4);
            }
            target.set(referencedCIs);
        }
    }
    
    /**
     * Appends and returns a new empty "referencedCIs" element
     */
    public com.hp.schemas.ucmdb._1.types.CIs addNewReferencedCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIs target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIs)get_store().add_element_user(REFERENCEDCIS$4);
            return target;
        }
    }
    
    /**
     * Unsets the "referencedCIs" element
     */
    public void unsetReferencedCIs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REFERENCEDCIS$4, 0);
        }
    }
    
    /**
     * Gets the "referencedRelations" element
     */
    public com.hp.schemas.ucmdb._1.types.Relations getReferencedRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().find_element_user(REFERENCEDRELATIONS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "referencedRelations" element
     */
    public boolean isSetReferencedRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REFERENCEDRELATIONS$6) != 0;
        }
    }
    
    /**
     * Sets the "referencedRelations" element
     */
    public void setReferencedRelations(com.hp.schemas.ucmdb._1.types.Relations referencedRelations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().find_element_user(REFERENCEDRELATIONS$6, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().add_element_user(REFERENCEDRELATIONS$6);
            }
            target.set(referencedRelations);
        }
    }
    
    /**
     * Appends and returns a new empty "referencedRelations" element
     */
    public com.hp.schemas.ucmdb._1.types.Relations addNewReferencedRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.Relations target = null;
            target = (com.hp.schemas.ucmdb._1.types.Relations)get_store().add_element_user(REFERENCEDRELATIONS$6);
            return target;
        }
    }
    
    /**
     * Unsets the "referencedRelations" element
     */
    public void unsetReferencedRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REFERENCEDRELATIONS$6, 0);
        }
    }
}
