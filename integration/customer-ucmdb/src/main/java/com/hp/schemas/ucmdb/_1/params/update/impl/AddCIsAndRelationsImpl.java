/*
 * XML Type:  addCIsAndRelations
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * An XML addCIsAndRelations(@http://schemas.hp.com/ucmdb/1/params/update).
 *
 * This is a complex type.
 */
public class AddCIsAndRelationsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.AddCIsAndRelations
{
    
    public AddCIsAndRelationsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "cmdbContext");
    private static final javax.xml.namespace.QName DATASTORE$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "dataStore");
    private static final javax.xml.namespace.QName UPDATEEXISTING$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "updateExisting");
    private static final javax.xml.namespace.QName CISANDRELATIONSUPDATES$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "CIsAndRelationsUpdates");
    private static final javax.xml.namespace.QName IGNOREVALIDATION$8 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "ignoreValidation");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
    
    /**
     * Gets the "dataStore" element
     */
    public java.lang.String getDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATASTORE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "dataStore" element
     */
    public org.apache.xmlbeans.XmlString xgetDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DATASTORE$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "dataStore" element
     */
    public boolean isSetDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DATASTORE$2) != 0;
        }
    }
    
    /**
     * Sets the "dataStore" element
     */
    public void setDataStore(java.lang.String dataStore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATASTORE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DATASTORE$2);
            }
            target.setStringValue(dataStore);
        }
    }
    
    /**
     * Sets (as xml) the "dataStore" element
     */
    public void xsetDataStore(org.apache.xmlbeans.XmlString dataStore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DATASTORE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DATASTORE$2);
            }
            target.set(dataStore);
        }
    }
    
    /**
     * Unsets the "dataStore" element
     */
    public void unsetDataStore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DATASTORE$2, 0);
        }
    }
    
    /**
     * Gets the "updateExisting" element
     */
    public boolean getUpdateExisting()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UPDATEEXISTING$4, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "updateExisting" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetUpdateExisting()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(UPDATEEXISTING$4, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "updateExisting" element
     */
    public boolean isNilUpdateExisting()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(UPDATEEXISTING$4, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "updateExisting" element
     */
    public boolean isSetUpdateExisting()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UPDATEEXISTING$4) != 0;
        }
    }
    
    /**
     * Sets the "updateExisting" element
     */
    public void setUpdateExisting(boolean updateExisting)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UPDATEEXISTING$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UPDATEEXISTING$4);
            }
            target.setBooleanValue(updateExisting);
        }
    }
    
    /**
     * Sets (as xml) the "updateExisting" element
     */
    public void xsetUpdateExisting(org.apache.xmlbeans.XmlBoolean updateExisting)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(UPDATEEXISTING$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(UPDATEEXISTING$4);
            }
            target.set(updateExisting);
        }
    }
    
    /**
     * Nils the "updateExisting" element
     */
    public void setNilUpdateExisting()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(UPDATEEXISTING$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(UPDATEEXISTING$4);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "updateExisting" element
     */
    public void unsetUpdateExisting()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UPDATEEXISTING$4, 0);
        }
    }
    
    /**
     * Gets the "CIsAndRelationsUpdates" element
     */
    public com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates getCIsAndRelationsUpdates()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().find_element_user(CISANDRELATIONSUPDATES$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CIsAndRelationsUpdates" element
     */
    public void setCIsAndRelationsUpdates(com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates cIsAndRelationsUpdates)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().find_element_user(CISANDRELATIONSUPDATES$6, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().add_element_user(CISANDRELATIONSUPDATES$6);
            }
            target.set(cIsAndRelationsUpdates);
        }
    }
    
    /**
     * Appends and returns a new empty "CIsAndRelationsUpdates" element
     */
    public com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates addNewCIsAndRelationsUpdates()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates target = null;
            target = (com.hp.schemas.ucmdb._1.types.update.CIsAndRelationsUpdates)get_store().add_element_user(CISANDRELATIONSUPDATES$6);
            return target;
        }
    }
    
    /**
     * Gets the "ignoreValidation" element
     */
    public boolean getIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IGNOREVALIDATION$8, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "ignoreValidation" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(IGNOREVALIDATION$8, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "ignoreValidation" element
     */
    public boolean isNilIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(IGNOREVALIDATION$8, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "ignoreValidation" element
     */
    public boolean isSetIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IGNOREVALIDATION$8) != 0;
        }
    }
    
    /**
     * Sets the "ignoreValidation" element
     */
    public void setIgnoreValidation(boolean ignoreValidation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IGNOREVALIDATION$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IGNOREVALIDATION$8);
            }
            target.setBooleanValue(ignoreValidation);
        }
    }
    
    /**
     * Sets (as xml) the "ignoreValidation" element
     */
    public void xsetIgnoreValidation(org.apache.xmlbeans.XmlBoolean ignoreValidation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(IGNOREVALIDATION$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(IGNOREVALIDATION$8);
            }
            target.set(ignoreValidation);
        }
    }
    
    /**
     * Nils the "ignoreValidation" element
     */
    public void setNilIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(IGNOREVALIDATION$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(IGNOREVALIDATION$8);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "ignoreValidation" element
     */
    public void unsetIgnoreValidation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IGNOREVALIDATION$8, 0);
        }
    }
}
