/*
 * An XML document type.
 * Localname: calculateImpact
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.CalculateImpactDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * A document containing one calculateImpact(@http://schemas.hp.com/ucmdb/1/params/impact) element.
 *
 * This is a complex type.
 */
public class CalculateImpactDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.CalculateImpactDocument
{
    
    public CalculateImpactDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CALCULATEIMPACT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "calculateImpact");
    
    
    /**
     * Gets the "calculateImpact" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.CalculateImpact getCalculateImpact()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.CalculateImpact target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.CalculateImpact)get_store().find_element_user(CALCULATEIMPACT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "calculateImpact" element
     */
    public void setCalculateImpact(com.hp.schemas.ucmdb._1.params.impact.CalculateImpact calculateImpact)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.CalculateImpact target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.CalculateImpact)get_store().find_element_user(CALCULATEIMPACT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.impact.CalculateImpact)get_store().add_element_user(CALCULATEIMPACT$0);
            }
            target.set(calculateImpact);
        }
    }
    
    /**
     * Appends and returns a new empty "calculateImpact" element
     */
    public com.hp.schemas.ucmdb._1.params.impact.CalculateImpact addNewCalculateImpact()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.impact.CalculateImpact target = null;
            target = (com.hp.schemas.ucmdb._1.params.impact.CalculateImpact)get_store().add_element_user(CALCULATEIMPACT$0);
            return target;
        }
    }
}
