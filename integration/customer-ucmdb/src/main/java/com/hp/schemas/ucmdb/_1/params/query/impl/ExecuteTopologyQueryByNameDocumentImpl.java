/*
 * An XML document type.
 * Localname: executeTopologyQueryByName
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one executeTopologyQueryByName(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class ExecuteTopologyQueryByNameDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameDocument
{
    
    public ExecuteTopologyQueryByNameDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EXECUTETOPOLOGYQUERYBYNAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryByName");
    
    
    /**
     * Gets the "executeTopologyQueryByName" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName getExecuteTopologyQueryByName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName)get_store().find_element_user(EXECUTETOPOLOGYQUERYBYNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "executeTopologyQueryByName" element
     */
    public void setExecuteTopologyQueryByName(com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName executeTopologyQueryByName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName)get_store().find_element_user(EXECUTETOPOLOGYQUERYBYNAME$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName)get_store().add_element_user(EXECUTETOPOLOGYQUERYBYNAME$0);
            }
            target.set(executeTopologyQueryByName);
        }
    }
    
    /**
     * Appends and returns a new empty "executeTopologyQueryByName" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName addNewExecuteTopologyQueryByName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByName)get_store().add_element_user(EXECUTETOPOLOGYQUERYBYNAME$0);
            return target;
        }
    }
}
