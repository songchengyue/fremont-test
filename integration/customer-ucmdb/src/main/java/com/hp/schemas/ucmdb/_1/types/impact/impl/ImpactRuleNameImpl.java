/*
 * XML Type:  ImpactRuleName
 * Namespace: http://schemas.hp.com/ucmdb/1/types/impact
 * Java type: com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impact.impl;
/**
 * An XML ImpactRuleName(@http://schemas.hp.com/ucmdb/1/types/impact).
 *
 * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName.
 */
public class ImpactRuleNameImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.hp.schemas.ucmdb._1.types.impact.ImpactRuleName
{
    
    public ImpactRuleNameImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, true);
    }
    
    protected ImpactRuleNameImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    
    
}
