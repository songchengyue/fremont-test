/*
 * An XML document type.
 * Localname: setTriggerTQLProbesLimitRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one setTriggerTQLProbesLimitRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class SetTriggerTQLProbesLimitRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequestDocument
{
    
    public SetTriggerTQLProbesLimitRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SETTRIGGERTQLPROBESLIMITREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "setTriggerTQLProbesLimitRequest");
    
    
    /**
     * Gets the "setTriggerTQLProbesLimitRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest getSetTriggerTQLProbesLimitRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest)get_store().find_element_user(SETTRIGGERTQLPROBESLIMITREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "setTriggerTQLProbesLimitRequest" element
     */
    public void setSetTriggerTQLProbesLimitRequest(com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest setTriggerTQLProbesLimitRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest)get_store().find_element_user(SETTRIGGERTQLPROBESLIMITREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest)get_store().add_element_user(SETTRIGGERTQLPROBESLIMITREQUEST$0);
            }
            target.set(setTriggerTQLProbesLimitRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "setTriggerTQLProbesLimitRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest addNewSetTriggerTQLProbesLimitRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest)get_store().add_element_user(SETTRIGGERTQLPROBESLIMITREQUEST$0);
            return target;
        }
    }
}
