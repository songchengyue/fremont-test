/*
 * XML Type:  rediscoverCIsResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML rediscoverCIsResponse(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class RediscoverCIsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsResponse
{
    
    public RediscoverCIsResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ISSUCCEED$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "isSucceed");
    
    
    /**
     * Gets the "isSucceed" element
     */
    public boolean getIsSucceed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ISSUCCEED$0, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "isSucceed" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetIsSucceed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(ISSUCCEED$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "isSucceed" element
     */
    public void setIsSucceed(boolean isSucceed)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ISSUCCEED$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ISSUCCEED$0);
            }
            target.setBooleanValue(isSucceed);
        }
    }
    
    /**
     * Sets (as xml) the "isSucceed" element
     */
    public void xsetIsSucceed(org.apache.xmlbeans.XmlBoolean isSucceed)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(ISSUCCEED$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(ISSUCCEED$0);
            }
            target.set(isSucceed);
        }
    }
}
