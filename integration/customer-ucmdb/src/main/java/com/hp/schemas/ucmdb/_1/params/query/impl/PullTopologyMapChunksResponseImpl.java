/*
 * XML Type:  pullTopologyMapChunksResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * An XML pullTopologyMapChunksResponse(@http://schemas.hp.com/ucmdb/1/params/query).
 *
 * This is a complex type.
 */
public class PullTopologyMapChunksResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponse
{
    
    public PullTopologyMapChunksResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TOPOLOGYMAP$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "topologyMap");
    private static final javax.xml.namespace.QName COMMENTS$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "comments");
    
    
    /**
     * Gets the "topologyMap" element
     */
    public com.hp.schemas.ucmdb._1.types.TopologyMap getTopologyMap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.TopologyMap target = null;
            target = (com.hp.schemas.ucmdb._1.types.TopologyMap)get_store().find_element_user(TOPOLOGYMAP$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "topologyMap" element
     */
    public void setTopologyMap(com.hp.schemas.ucmdb._1.types.TopologyMap topologyMap)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.TopologyMap target = null;
            target = (com.hp.schemas.ucmdb._1.types.TopologyMap)get_store().find_element_user(TOPOLOGYMAP$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.TopologyMap)get_store().add_element_user(TOPOLOGYMAP$0);
            }
            target.set(topologyMap);
        }
    }
    
    /**
     * Appends and returns a new empty "topologyMap" element
     */
    public com.hp.schemas.ucmdb._1.types.TopologyMap addNewTopologyMap()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.TopologyMap target = null;
            target = (com.hp.schemas.ucmdb._1.types.TopologyMap)get_store().add_element_user(TOPOLOGYMAP$0);
            return target;
        }
    }
    
    /**
     * Gets the "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments getComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "comments" element
     */
    public boolean isSetComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(COMMENTS$2) != 0;
        }
    }
    
    /**
     * Sets the "comments" element
     */
    public void setComments(com.hp.schemas.ucmdb._1.types.ResComments comments)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().find_element_user(COMMENTS$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            }
            target.set(comments);
        }
    }
    
    /**
     * Appends and returns a new empty "comments" element
     */
    public com.hp.schemas.ucmdb._1.types.ResComments addNewComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ResComments target = null;
            target = (com.hp.schemas.ucmdb._1.types.ResComments)get_store().add_element_user(COMMENTS$2);
            return target;
        }
    }
    
    /**
     * Unsets the "comments" element
     */
    public void unsetComments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(COMMENTS$2, 0);
        }
    }
}
