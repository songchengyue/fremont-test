package com.telstra.netops.dns.server.model;

public enum DnsRecordType {
	
	A, //
	CNAME, //
	MX, //
	NS, //
	TXT,
	AAAA;

}
