/*
 * An XML document type.
 * Localname: addTriggerTQLRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one addTriggerTQLRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class AddTriggerTQLRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequestDocument
{
    
    public AddTriggerTQLRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDTRIGGERTQLREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "addTriggerTQLRequest");
    
    
    /**
     * Gets the "addTriggerTQLRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequest getAddTriggerTQLRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequest)get_store().find_element_user(ADDTRIGGERTQLREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "addTriggerTQLRequest" element
     */
    public void setAddTriggerTQLRequest(com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequest addTriggerTQLRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequest)get_store().find_element_user(ADDTRIGGERTQLREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequest)get_store().add_element_user(ADDTRIGGERTQLREQUEST$0);
            }
            target.set(addTriggerTQLRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "addTriggerTQLRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequest addNewAddTriggerTQLRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.AddTriggerTQLRequest)get_store().add_element_user(ADDTRIGGERTQLREQUEST$0);
            return target;
        }
    }
}
