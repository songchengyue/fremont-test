/*
 * XML Type:  ParameterizedNodes
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.ParameterizedNodes
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML ParameterizedNodes(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class ParameterizedNodesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.ParameterizedNodes
{
    
    public ParameterizedNodesImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PARAMETERIZEDNODE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "parameterizedNode");
    
    
    /**
     * Gets array of all "parameterizedNode" elements
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedNode[] getParameterizedNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(PARAMETERIZEDNODE$0, targetList);
            com.hp.schemas.ucmdb._1.types.ParameterizedNode[] result = new com.hp.schemas.ucmdb._1.types.ParameterizedNode[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "parameterizedNode" element
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedNode getParameterizedNodeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedNode)get_store().find_element_user(PARAMETERIZEDNODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "parameterizedNode" element
     */
    public int sizeOfParameterizedNodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PARAMETERIZEDNODE$0);
        }
    }
    
    /**
     * Sets array of all "parameterizedNode" element
     */
    public void setParameterizedNodeArray(com.hp.schemas.ucmdb._1.types.ParameterizedNode[] parameterizedNodeArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(parameterizedNodeArray, PARAMETERIZEDNODE$0);
        }
    }
    
    /**
     * Sets ith "parameterizedNode" element
     */
    public void setParameterizedNodeArray(int i, com.hp.schemas.ucmdb._1.types.ParameterizedNode parameterizedNode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedNode)get_store().find_element_user(PARAMETERIZEDNODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(parameterizedNode);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "parameterizedNode" element
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedNode insertNewParameterizedNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedNode)get_store().insert_element_user(PARAMETERIZEDNODE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "parameterizedNode" element
     */
    public com.hp.schemas.ucmdb._1.types.ParameterizedNode addNewParameterizedNode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ParameterizedNode target = null;
            target = (com.hp.schemas.ucmdb._1.types.ParameterizedNode)get_store().add_element_user(PARAMETERIZEDNODE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "parameterizedNode" element
     */
    public void removeParameterizedNode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PARAMETERIZEDNODE$0, i);
        }
    }
}
