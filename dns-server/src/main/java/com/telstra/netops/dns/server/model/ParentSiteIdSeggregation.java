package com.telstra.netops.dns.server.model;

public class ParentSiteIdSeggregation {
	
	private String serviceId;
	
	public ParentSiteIdSeggregation(String serviceId){
		super();
		this.serviceId = serviceId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

}
