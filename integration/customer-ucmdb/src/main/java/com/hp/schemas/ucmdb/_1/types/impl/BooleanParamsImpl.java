/*
 * XML Type:  BooleanParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.BooleanParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML BooleanParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class BooleanParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.BooleanParams
{
    
    public BooleanParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName BOOLEANPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "booleanParam");
    
    
    /**
     * Gets array of all "booleanParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.BooleanProp[] getBooleanParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(BOOLEANPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.BooleanProp[] result = new com.hp.schemas.ucmdb._1.types.BooleanProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "booleanParam" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanProp getBooleanParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().find_element_user(BOOLEANPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "booleanParam" element
     */
    public int sizeOfBooleanParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BOOLEANPARAM$0);
        }
    }
    
    /**
     * Sets array of all "booleanParam" element
     */
    public void setBooleanParamArray(com.hp.schemas.ucmdb._1.types.BooleanProp[] booleanParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(booleanParamArray, BOOLEANPARAM$0);
        }
    }
    
    /**
     * Sets ith "booleanParam" element
     */
    public void setBooleanParamArray(int i, com.hp.schemas.ucmdb._1.types.BooleanProp booleanParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().find_element_user(BOOLEANPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(booleanParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "booleanParam" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanProp insertNewBooleanParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().insert_element_user(BOOLEANPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "booleanParam" element
     */
    public com.hp.schemas.ucmdb._1.types.BooleanProp addNewBooleanParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.BooleanProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.BooleanProp)get_store().add_element_user(BOOLEANPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "booleanParam" element
     */
    public void removeBooleanParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BOOLEANPARAM$0, i);
        }
    }
}
