/*
 * XML Type:  XmlCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.XmlCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML XmlCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class XmlConditionImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.XmlCondition
{
    
    public XmlConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "condition");
    private static final javax.xml.namespace.QName XMLOPERATOR$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "xmlOperator");
    
    
    /**
     * Gets the "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlProp getCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "condition" element
     */
    public void setCondition(com.hp.schemas.ucmdb._1.types.XmlProp condition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().add_element_user(CONDITION$0);
            }
            target.set(condition);
        }
    }
    
    /**
     * Appends and returns a new empty "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlProp addNewCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlProp)get_store().add_element_user(CONDITION$0);
            return target;
        }
    }
    
    /**
     * Gets the "xmlOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator.Enum getXmlOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(XMLOPERATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "xmlOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator xgetXmlOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator)get_store().find_element_user(XMLOPERATOR$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "xmlOperator" element
     */
    public void setXmlOperator(com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator.Enum xmlOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(XMLOPERATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(XMLOPERATOR$2);
            }
            target.setEnumValue(xmlOperator);
        }
    }
    
    /**
     * Sets (as xml) the "xmlOperator" element
     */
    public void xsetXmlOperator(com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator xmlOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator)get_store().find_element_user(XMLOPERATOR$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator)get_store().add_element_user(XMLOPERATOR$2);
            }
            target.set(xmlOperator);
        }
    }
    /**
     * An XML xmlOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.XmlCondition$XmlOperator.
     */
    public static class XmlOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.XmlCondition.XmlOperator
    {
        
        public XmlOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected XmlOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
