/*
 * XML Type:  CI
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.CI
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML CI(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class CIImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.CI
{
    
    public CIImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ID$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "ID");
    private static final javax.xml.namespace.QName TYPE$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "type");
    private static final javax.xml.namespace.QName PROPS$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "props");
    
    
    /**
     * Gets the "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID getID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ID" element
     */
    public void setID(com.hp.schemas.ucmdb._1.types.ID id)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().find_element_user(ID$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ID$0);
            }
            target.set(id);
        }
    }
    
    /**
     * Appends and returns a new empty "ID" element
     */
    public com.hp.schemas.ucmdb._1.types.ID addNewID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ID target = null;
            target = (com.hp.schemas.ucmdb._1.types.ID)get_store().add_element_user(ID$0);
            return target;
        }
    }
    
    /**
     * Gets the "type" element
     */
    public java.lang.String getType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "type" element
     */
    public org.apache.xmlbeans.XmlString xgetType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "type" element
     */
    public void setType(java.lang.String type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TYPE$2);
            }
            target.setStringValue(type);
        }
    }
    
    /**
     * Sets (as xml) the "type" element
     */
    public void xsetType(org.apache.xmlbeans.XmlString type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TYPE$2);
            }
            target.set(type);
        }
    }
    
    /**
     * Gets the "props" element
     */
    public com.hp.schemas.ucmdb._1.types.CIProperties getProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().find_element_user(PROPS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "props" element
     */
    public boolean isSetProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PROPS$4) != 0;
        }
    }
    
    /**
     * Sets the "props" element
     */
    public void setProps(com.hp.schemas.ucmdb._1.types.CIProperties props)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().find_element_user(PROPS$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().add_element_user(PROPS$4);
            }
            target.set(props);
        }
    }
    
    /**
     * Appends and returns a new empty "props" element
     */
    public com.hp.schemas.ucmdb._1.types.CIProperties addNewProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CIProperties target = null;
            target = (com.hp.schemas.ucmdb._1.types.CIProperties)get_store().add_element_user(PROPS$4);
            return target;
        }
    }
    
    /**
     * Unsets the "props" element
     */
    public void unsetProps()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PROPS$4, 0);
        }
    }
}
