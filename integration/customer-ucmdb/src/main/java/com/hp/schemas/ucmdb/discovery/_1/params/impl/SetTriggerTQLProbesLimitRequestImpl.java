/*
 * XML Type:  setTriggerTQLProbesLimitRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML setTriggerTQLProbesLimitRequest(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class SetTriggerTQLProbesLimitRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.SetTriggerTQLProbesLimitRequest
{
    
    public SetTriggerTQLProbesLimitRequestImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName JOBNAME$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "jobName");
    private static final javax.xml.namespace.QName TQLNAME$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "tqlName");
    private static final javax.xml.namespace.QName PROBESLIMIT$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "probesLimit");
    private static final javax.xml.namespace.QName CMDBCONTEXT$6 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "cmdbContext");
    
    
    /**
     * Gets the "jobName" element
     */
    public java.lang.String getJobName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(JOBNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "jobName" element
     */
    public org.apache.xmlbeans.XmlString xgetJobName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(JOBNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "jobName" element
     */
    public void setJobName(java.lang.String jobName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(JOBNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(JOBNAME$0);
            }
            target.setStringValue(jobName);
        }
    }
    
    /**
     * Sets (as xml) the "jobName" element
     */
    public void xsetJobName(org.apache.xmlbeans.XmlString jobName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(JOBNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(JOBNAME$0);
            }
            target.set(jobName);
        }
    }
    
    /**
     * Gets the "tqlName" element
     */
    public java.lang.String getTqlName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TQLNAME$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "tqlName" element
     */
    public org.apache.xmlbeans.XmlString xgetTqlName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TQLNAME$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "tqlName" element
     */
    public void setTqlName(java.lang.String tqlName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TQLNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TQLNAME$2);
            }
            target.setStringValue(tqlName);
        }
    }
    
    /**
     * Sets (as xml) the "tqlName" element
     */
    public void xsetTqlName(org.apache.xmlbeans.XmlString tqlName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TQLNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TQLNAME$2);
            }
            target.set(tqlName);
        }
    }
    
    /**
     * Gets the "probesLimit" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList getProbesLimit()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(PROBESLIMIT$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "probesLimit" element
     */
    public void setProbesLimit(com.hp.schemas.ucmdb._1.types.StrList probesLimit)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(PROBESLIMIT$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(PROBESLIMIT$4);
            }
            target.set(probesLimit);
        }
    }
    
    /**
     * Appends and returns a new empty "probesLimit" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList addNewProbesLimit()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(PROBESLIMIT$4);
            return target;
        }
    }
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$6, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$6);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$6);
            return target;
        }
    }
}
