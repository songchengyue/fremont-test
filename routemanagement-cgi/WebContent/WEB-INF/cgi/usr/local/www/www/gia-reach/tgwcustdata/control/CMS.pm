# Date: 5 July 2001
# File: CMS.pm
#
# This module contains subroutines used in many of the CMS screens.
#
# $Id: CMS.pm,v 1.3 2004/01/05 12:56:56 paull Exp $

# 1 line added

package CMS;
use Exporter;

use 5.6.1;
#use strict;
use warnings;

# Print errors to browser, rather than server 500 internal errors
use CGI::Carp qw/fatalsToBrowser/;

use lib "../../modules";
use AUTH; 
use VALIDATE;
use TI_HTML;
use CMS_DB;
use CMS_OUTAGE;
use CMS_LOG_VIEWER;
use CMS_NOTICES;
use CMS_ROUTING_QUEUE;
use CMS_TROUBLE_TICKET;
use CMS_SECMX;
use CMS_SECDNS;
use CMS_NEWS;
use CMS_ROUTERS;
use CMS_LINKS;
use CMS_CUSTOMER;
use CMS_SERVICE;
use CMS_WHOLESALER;
use CMS_OLSS_LOGIN;
use CMS_COMMUNITIES;
use CMS_AGGREGATES;
use CMS_PC;
use CMS_IP_BLOCK;
use CMS_OLSS_CUSTACC;
use CMS_IP_ADD;
use CMS_PRIDNS;
use CMS_MASTER;
use CMS_CUST_SERVICE;
use CMS_OLSS_CUSTACC_1;

use constant TEMPLATE_PATH => "../templates";
use constant SUCCESS => 1;
use constant FAIL => 0;


our @ISA        = qw(Exporter);
our @EXPORT     = qw(%html_values);

our %html_values;
my $debug = 1;
#to disable debug statements, uncomment following line  and vice versa
#$debug = 0;

if ($debug) {open (IPT,">>/data1/tmp_log/routers.log");}


sub authenticate {

  if (scalar @_ != 1) {
    warn "incorrect number of arguments";
    return 0;
  }

  my $task = shift @_;

  

  $html_values{authentication_code}{value} = 
    AUTH::authenticate_user($html_values{username}{value}, 
			    $html_values{authentication_code}{value}, 
			    "cms", $task);

  if ($html_values{authentication_code}{value}) {
    return SUCCESS;
  } else {
    return FAIL;
  }
}

sub clear_html_values {
  TI_HTML::clear (\%html_values);
}

sub add_error {
  TI_HTML::add_error (\%html_values, @_);
}

sub output_error {
  $html_values{error_message}{value} = shift;
  output_html ("error");
}

sub output_html {
  my $screenname = shift @_;
  my $type = shift @_;
  if (not defined ($type)) {
     $type = "html";
  }
  unless(TI_HTML::output_html($screenname, \%html_values, $type)) {
    output_error "Failed to generate HTML for the $screenname screen.\n";
  }
}

sub check_screen {
  my $screenname = shift @_;
  TI_HTML::check_screen($screenname, \%html_values);
}


# This subroutine takes are message type and message
# and logs it in the database
sub log {
  CMS_DB::log(@_);
}


sub process {
  $TI_HTML::base_template_path = TEMPLATE_PATH;
  #TI_HTML::get_key_value_pairs \%html_values;

  CMS_DB::connect_to_database;

  $html_values{page}{title} = "Customer Management System";

  if (defined $html_values{username}{value}) {
    $html_values{page}{title} 
      .= " ($html_values{username}{value})";
  }

  if (defined $html_values{subsystem}{value}) {
    
    if (not authenticate "login") {
      output_html "time_out";
    } elsif ($html_values{subsystem}{value} eq "outage") {
      CMS_OUTAGE::start;
    } elsif ($html_values{subsystem}{value} eq "notices") {
      CMS_NOTICES::start;
    } elsif ($html_values{subsystem}{value} eq "troubleticket") {
      CMS_TROUBLE_TICKET::start;
    } elsif ($html_values{subsystem}{value} eq "routingqueue") {
      CMS_ROUTING_QUEUE::start;
    } elsif ($html_values{subsystem}{value} eq "secdns") {
      CMS_SECDNS::start;
    } elsif ($html_values{subsystem}{value} eq "secmx") {
      CMS_SECMX::start;
    } elsif ($html_values{subsystem}{value} eq "news") {
      CMS_NEWS::start;
    } elsif ($html_values{subsystem}{value} eq "monitoring") {
      #CMS_MONITORING::start;
      CMS::add_error("Active customer monitoring functionality has not been implemented.");
      CMS::output_html("menu");
    } elsif ($html_values{subsystem}{value} eq "routers") {
      if($debug){print IPT "CMS PM router condition\n";}
      CMS_ROUTERS::start;
    } elsif ($html_values{subsystem}{value} eq "customers") {
      CMS_CUSTOMER::start;
    } elsif ($html_values{subsystem}{value} eq "wholesalers") {
      CMS_WHOLESALER::start;
    } elsif ($html_values{subsystem}{value} eq "service") {
      CMS_SERVICE::start;
    } elsif ($html_values{subsystem}{value} eq "links") {
      CMS_LINKS::start;
    } elsif ($html_values{subsystem}{value} eq "olss") {
      CMS_OLSS_LOGIN::start;
    } elsif ($html_values{subsystem}{value} eq "logviewer") {
      CMS::add_error("Full functionality has not been implemented.");
      CMS_LOG_VIEWER::start;
    } elsif ($html_values{subsystem}{value} eq "communities") {
      CMS_COMMUNITIES::start;
    } elsif ($html_values{subsystem}{value} eq "aggregates") {
      CMS_AGGREGATES::start;
    } elsif ($html_values{subsystem}{value} eq "master") {
      CMS_MASTER::start;
    } elsif ($html_values{subsystem}{value} eq "menu") {
      if ($html_values{username}{value}=~/c3_account/)
        {
         CMS::output_html("menu_1");
        }
      else
       {
         CMS::output_html("menu");
       }
    } elsif ($html_values{subsystem}{value} eq "productcodes"){
      CMS_PC::start;
    } elsif ($html_values{subsystem}{value} eq "ipblock"){
      CMS_IP_BLOCK::start;
    } elsif ($html_values{subsystem}{value} eq "olsscustacc"){
       if ($html_values{username}{value}=~/c3_account/)
       {
           CMS_OLSS_CUSTACC_1::start;
       }
       else
       {
           CMS_OLSS_CUSTACC::start;
       }
    } elsif ($html_values{subsystem}{value} eq "ipadd"){
      CMS_IP_ADD::start;
    } elsif ($html_values{subsystem}{value} eq "pridns"){
      CMS_PRIDNS::start;
    ### Added by Karuna for IP Transit ph 3
    } elsif ($html_values{subsystem}{value} eq "cust_service"){
      CMS_CUST_SERVICE::start;
    } 
    ### End of code for IPT 3 
   					
  } elsif (defined $html_values{password}{value}) {
    # trying to log on
    
    $html_values{authentication_code}{value} = 
      AUTH::generate_code($html_values{username}{value},
			  $html_values{password}{value});

    if (authenticate "login") {
      CMS::log("authentication","passed login");
     if ($html_values{username}{value}=~/c3_account/)
       {
           output_html "menu_1";
       }
     else
        {
            output_html "menu";
        }

    } else {
      # first time user or something has gone wrong
      CMS::log("authentication","failed login");
      output_html "failed_auth";
      
    }

  } else {
    # first logon attempt

    output_html "login";
  }

  CMS_DB::disconnect_from_database;

}


1;
