/*
 * An XML document type.
 * Localname: getDomainTypeResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getDomainTypeResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetDomainTypeResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponseDocument
{
    
    public GetDomainTypeResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETDOMAINTYPERESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDomainTypeResponse");
    
    
    /**
     * Gets the "getDomainTypeResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponse getGetDomainTypeResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponse)get_store().find_element_user(GETDOMAINTYPERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getDomainTypeResponse" element
     */
    public void setGetDomainTypeResponse(com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponse getDomainTypeResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponse)get_store().find_element_user(GETDOMAINTYPERESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponse)get_store().add_element_user(GETDOMAINTYPERESPONSE$0);
            }
            target.set(getDomainTypeResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getDomainTypeResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponse addNewGetDomainTypeResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDomainTypeResponse)get_store().add_element_user(GETDOMAINTYPERESPONSE$0);
            return target;
        }
    }
}
