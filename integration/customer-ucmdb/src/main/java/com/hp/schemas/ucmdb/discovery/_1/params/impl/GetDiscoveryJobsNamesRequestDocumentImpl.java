/*
 * An XML document type.
 * Localname: getDiscoveryJobsNamesRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getDiscoveryJobsNamesRequest(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetDiscoveryJobsNamesRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequestDocument
{
    
    public GetDiscoveryJobsNamesRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETDISCOVERYJOBSNAMESREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDiscoveryJobsNamesRequest");
    
    
    /**
     * Gets the "getDiscoveryJobsNamesRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequest getGetDiscoveryJobsNamesRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequest)get_store().find_element_user(GETDISCOVERYJOBSNAMESREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getDiscoveryJobsNamesRequest" element
     */
    public void setGetDiscoveryJobsNamesRequest(com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequest getDiscoveryJobsNamesRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequest)get_store().find_element_user(GETDISCOVERYJOBSNAMESREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequest)get_store().add_element_user(GETDISCOVERYJOBSNAMESREQUEST$0);
            }
            target.set(getDiscoveryJobsNamesRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "getDiscoveryJobsNamesRequest" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequest addNewGetDiscoveryJobsNamesRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequest target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetDiscoveryJobsNamesRequest)get_store().add_element_user(GETDISCOVERYJOBSNAMESREQUEST$0);
            return target;
        }
    }
}
