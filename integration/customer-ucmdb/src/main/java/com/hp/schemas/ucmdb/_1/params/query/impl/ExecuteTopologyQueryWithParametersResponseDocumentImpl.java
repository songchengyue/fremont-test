/*
 * An XML document type.
 * Localname: executeTopologyQueryWithParametersResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParametersResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one executeTopologyQueryWithParametersResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class ExecuteTopologyQueryWithParametersResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryWithParametersResponseDocument
{
    
    public ExecuteTopologyQueryWithParametersResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EXECUTETOPOLOGYQUERYWITHPARAMETERSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryWithParametersResponse");
    
    
    /**
     * Gets the "executeTopologyQueryWithParametersResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse getExecuteTopologyQueryWithParametersResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse)get_store().find_element_user(EXECUTETOPOLOGYQUERYWITHPARAMETERSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "executeTopologyQueryWithParametersResponse" element
     */
    public void setExecuteTopologyQueryWithParametersResponse(com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse executeTopologyQueryWithParametersResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse)get_store().find_element_user(EXECUTETOPOLOGYQUERYWITHPARAMETERSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse)get_store().add_element_user(EXECUTETOPOLOGYQUERYWITHPARAMETERSRESPONSE$0);
            }
            target.set(executeTopologyQueryWithParametersResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "executeTopologyQueryWithParametersResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse addNewExecuteTopologyQueryWithParametersResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ExecuteTopologyQueryByNameWithParametersResponse)get_store().add_element_user(EXECUTETOPOLOGYQUERYWITHPARAMETERSRESPONSE$0);
            return target;
        }
    }
}
