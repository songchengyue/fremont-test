# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 6 September 2001
# File: CMS.pm
#
# This module contains subroutines for the outage system.
#
# $Id: CMS_OUTAGE.pm,v 1.1.1.1 2016/06/09 22:21:46 d772392 Exp $

package CMS_OUTAGE;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_OUTAGE_DB;

use lib "../../modules";

our @ISA        = qw(Exporter);

sub generate_notification_message {
  # get email message from template
  open (MAIL, "../templates/create.mail");
  my $message;
  
  # substitute values for keys in template
  while (my $line = <MAIL>) {
    while ($line =~ /\[\[\[.+\]\]\]/) {
      no warnings;
      $line =~ 
	s/\[\[\[(.+?)\]\]\]/$CMS::html_values{"$1"}{value}/i;
    }
    $message .= $line;
  }
  close MAIL;
  return $message;
}

################## GENERATE VIEW ###################

sub generate_view {
  # display list of outages

  if (not defined 
      $CMS::html_values{"search_startdate.startyear"}{value}) {
    TI_HTML::set_datetime_search_values(\%CMS::html_values,
					"search_startdate",
					"2001-01-01 00:00:00",
					((gmtime(time))[5]+1900)."-12-31 23:59:59");

  }

  if ((not defined($_[0])) || ($_[0] ne "nodata")) {
    if (scalar @_ == 0) {

      $CMS::html_values{outages}{table} = 
	CMS_OUTAGE_DB::get_outage_list ("","","","","","","1");
    } else {
      $CMS::html_values{outages}{table} = 
	CMS_OUTAGE_DB::get_outage_list (@_);
    }
    
    my $count = 0;
    foreach my $row (@{$CMS::html_values{outages}{table}}) {
      push @$row, "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=outage&command=update_outage&ticketno=$$row[0]\">Update</a>";
      $$row[0] = "<a href=\"index.pl?username=$CMS::html_values{username}{value}&authentication_code=$CMS::html_values{authentication_code}{value}&subsystem=outage&command=view_outage&ticketno=$$row[0]\">$$row[0]</a>";
      $count++;
    }
    
    if ($count) {

      unshift @{$CMS::html_values{outages}{table}}, 
	["Ticket Number", "Starting", "Created by", "Type", "Item", "Status", "Update"];
      $CMS::html_values{outages}{header_rows} = 1;
      $CMS::html_values{noresultsmessage}{value} = "";
    } elsif (not defined($_[0])) {
      
      $CMS::html_values{noresultsmessage}{value} = 
	"No current outage notices.";
    } else {
      $CMS::html_values{noresultsmessage}{value} = 
	"No matching results found.";
      
    }
  }
  $CMS::html_values{search_outageitemtype}{options} = 
    CMS_OUTAGE_DB::get_outage_item_types;
  $CMS::html_values{search_outagestatus}{options} = 
    CMS_OUTAGE_DB::get_outage_status;
  
  CMS::output_html("outage_view_list");
}


################ GET OUTAGE NOTICE ##################3

sub get_outage_notice {

  if ((defined $_[0]) && ($_[0] eq "refresh")) {

    $CMS::html_values{cust_message}{table} = 
      CMS_OUTAGE_DB::get_customer_comments
	  ($CMS::html_values{ticketno}{value});
    unshift @{$CMS::html_values{cust_message}{table}},
      ["Time", "User", "Comment"];
    $CMS::html_values{cust_message}{header_rows} = 1;
    $CMS::html_values{staff_message}{table} = 
      CMS_OUTAGE_DB::get_staff_comments
	  ($CMS::html_values{ticketno}{value});
    unshift @{$CMS::html_values{staff_message}{table}},
      ["Time", "User", "Comment"];

    $CMS::html_values{staff_message}{header_rows} = 1;
    $CMS::html_values{outagestatusid}{options} = 
	CMS_OUTAGE_DB::get_outage_status;
      
  } elsif (my $db_values = CMS_OUTAGE_DB::get_outage_notice
      ($CMS::html_values{ticketno}{value})) {
    
    # get existing values from database
    ($CMS::html_values{reported_username}{value},
     $CMS::html_values{starttime}{value},
     $CMS::html_values{endtime}{value},
     $CMS::html_values{reported}{value},
     $CMS::html_values{lastupdated}{value},
     $CMS::html_values{outagetypetext}{value},
     $CMS::html_values{outageleveltext}{value},
     $CMS::html_values{outageitemtypetext}{value},
     $CMS::html_values{outageitem}{value},
     $CMS::html_values{outagenotifyareatext}{value},
     $CMS::html_values{outagestatusid}{value},
     $CMS::html_values{outagestatustext}{value}) = @$db_values;
    
    $CMS::html_values{cust_message}{table} = 
      CMS_OUTAGE_DB::get_customer_comments
	  ($CMS::html_values{ticketno}{value});
    unshift @{$CMS::html_values{cust_message}{table}},
      ["Time", "User", "Comment"];
    $CMS::html_values{cust_message}{header_rows} = 1;
    $CMS::html_values{staff_message}{table} = 
      CMS_OUTAGE_DB::get_staff_comments
	  ($CMS::html_values{ticketno}{value});
    unshift @{$CMS::html_values{staff_message}{table}},
      ["Time", "User", "Comment"];

    $CMS::html_values{staff_message}{header_rows} = 1;
    
    if ((defined $_[0]) && ($_[0] eq "update")) {
      # extra things that need to be generated for update screen
      $CMS::html_values{outagestatusid}{options} = 
	CMS_OUTAGE_DB::get_outage_status;
      
      # set up time
      if ($CMS::html_values{starttime}{value} =~ 
	  /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/) {
	$CMS::html_values{'startdate.year'}{value} = $1;
	$CMS::html_values{'startdate.month'}{value} = $2;
	$CMS::html_values{'startdate.day'}{value} = $3;
	$CMS::html_values{'starttime.hour'}{value} = $4;
	$CMS::html_values{'starttime.minute'}{value} = $5;
      } 

      if ($CMS::html_values{endtime}{value} =~ 
	  /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/) {
	$CMS::html_values{'enddate.year'}{value} = $1;
	$CMS::html_values{'enddate.month'}{value} = $2;
	$CMS::html_values{'enddate.day'}{value} = $3;
	$CMS::html_values{'endtime.hour'}{value} = $4;
	$CMS::html_values{'endtime.minute'}{value} = $5;
      }
      
    } 
    
    return 1;
  } else {
    return 0;
  }
}


###################### MAIN HANDLING ROUTINE #################

sub start {
  
  # could be any screen sent in from CMS



  if ((not defined($CMS::html_values{command}{value})) ||
      ((defined $CMS::html_values{submit}{value}) &&
       ($CMS::html_values{submit}{value} eq "Cancel"))) {
    # default to view_current_outages
    
    $CMS::html_values{command}{value} = "view_current_outages";
    
  }

  if ($CMS::html_values{command}{value} eq "view_current_outages")  {
    # display current outages
    CMS::clear_html_values();
    $CMS::html_values{subsystem}{value} = "outage";
    $CMS::html_values{command}{value} = "view_current_outages";
    generate_view;


    ############### CREATE NEW OUTAGE ####################

  } elsif ($CMS::html_values{command}{value} eq "create_new_outage") {

   

    # determined whether to continue or not
    if(defined($CMS::html_values{submit}{value}) &&
	    ($CMS::html_values{submit}{value} eq "Go Back")) {
      $CMS::html_values{stage}{value} -= 1;
    } elsif ((not defined($CMS::html_values{current_screen}{value})) || 
       CMS::check_screen($CMS::html_values{current_screen}{value})){
      $CMS::html_values{stage}{value} += 1;
    }
  
    # Load required information
    if ($CMS::html_values{stage}{value} <= 1){
      # STAGE 1

      # fetch info from database
      $CMS::html_values{outagetypeid}{options} = 
	CMS_OUTAGE_DB::get_outage_types;
      $CMS::html_values{outagelevelid}{options} = 
	CMS_OUTAGE_DB::get_outage_levels;
      $CMS::html_values{outageitemtypeid}{options} = 
	CMS_OUTAGE_DB::get_outage_item_types;


 # set up time
      my @now = gmtime;
 

      $now[1] = int($now[1] / 5) * 5;

      $CMS::html_values{'startdate.year'}{value} = 1900+$now[5];
      $CMS::html_values{'startdate.month'}{value} = 
	sprintf("%02d",$now[4] + 1);
      $CMS::html_values{'startdate.day'}{value} = 
	sprintf("%02d",$now[3]);
      $CMS::html_values{'starttime.hour'}{value} = 
	sprintf("%02d",$now[2]);
      $CMS::html_values{'starttime.minute'}{value} = 
	sprintf("%02d",$now[1]);

      $CMS::html_values{'enddate.year'}{value} = 1900+$now[5];
      $CMS::html_values{'enddate.month'}{value} =
	sprintf("%02d", $now[4] + 1);
      $CMS::html_values{'enddate.day'}{value} = sprintf("%02d",$now[3]);
      $CMS::html_values{'endtime.hour'}{value} = sprintf("%02d",$now[2]);
      $CMS::html_values{'endtime.minute'}{value} = 
	sprintf("%02d",$now[1]);

      CMS::output_html("outage_create1");

    } elsif ($CMS::html_values{stage}{value} eq "2") {
      # STAGE 2
   
      $CMS::html_values{outagetypetext}{value} =
	CMS_OUTAGE_DB::get_outage_type_text
	    ($CMS::html_values{outagetypeid}{value});

      $CMS::html_values{outageleveltext}{value} =
	CMS_OUTAGE_DB::get_outage_level_text 
	    ($CMS::html_values{outagelevelid}{value});

      $CMS::html_values{outageitemtypetext}{value} =
	CMS_OUTAGE_DB::get_outage_item_type_text 
	    ($CMS::html_values{outageitemtypeid}{value});

      $CMS::html_values{outageitemid}{value} = 
	CMS_OUTAGE_DB::get_outage_items
	    ($CMS::html_values{outageitemtypeid}{value});

      $CMS::html_values{outagenotifyareaid}{options} =
	CMS_OUTAGE_DB::get_outage_notify_areas
	    ($CMS::html_values{outageitemtypeid}{value});

      $CMS::html_values{outageitem}{options} = 
	CMS_OUTAGE_DB::get_outage_items
	    ($CMS::html_values{outageitemtypeid}{value});

      CMS::output_html("outage_create2");
      
    } elsif ($CMS::html_values{stage}{value} eq "3") {
    
      $CMS::html_values{outagenotifyareatext}{value} =
	CMS_OUTAGE_DB::get_outage_notify_area_text
	    ($CMS::html_values{outagenotifyareaid}{value});
      CMS::output_html("outage_create3");
    } elsif ($CMS::html_values{stage}{value} eq "4") {
    
      $CMS::html_values{ticket_number}{value} = "#NOT GENERATED YET#";
      $CMS::html_values{notification_message}{value} = 
	generate_notification_message;
      CMS::output_html("outage_create4");
    } elsif ($CMS::html_values{stage}{value} eq "5") {
    
      my $ticketno = CMS_OUTAGE_DB::add_outage_notice
	    ($CMS::html_values{username}{value},
	     $CMS::html_values{'startdate.day'}{value},
	     $CMS::html_values{'startdate.month'}{value},
	     $CMS::html_values{'startdate.year'}{value},
	     $CMS::html_values{'starttime.hour'}{value},
	     $CMS::html_values{'starttime.minute'}{value},
	     $CMS::html_values{'enddate.day'}{value},
	     $CMS::html_values{'enddate.month'}{value},
	     $CMS::html_values{'enddate.year'}{value},
	     $CMS::html_values{'endtime.hour'}{value},
	     $CMS::html_values{'endtime.minute'}{value},,
	     $CMS::html_values{outagetypeid}{value},
	     $CMS::html_values{outagelevelid}{value},
	     $CMS::html_values{outageitemtypeid}{value},
	     $CMS::html_values{outageitem}{value},
	     $CMS::html_values{outagenotifyareaid}{value},
	     $CMS::html_values{cust_message}{value},
	     $CMS::html_values{staff_message}{value}
	    );

      
      $CMS::html_values{success_message}{value} = 
	"Outage notice created and sent.";
      $CMS::html_values{command}{value} = "view_current_outages";

      # SEND EMAIL

      my $recipients;

      if ($CMS::html_values{outageitemtypeid}{value} == 1) {
	# router 
	$recipients = CMS_OUTAGE_DB::get_outage_contacts_for_router
	  ($CMS::html_values{outageitem}{value},
	   $CMS::html_values{outagenotifyareaid}{value});
      } elsif ($CMS::html_values{outageitemtypeid}{value} == 2) {
	# link
	$recipients = CMS_OUTAGE_DB::get_outage_contacts_for_all;
	  
      } elsif ($CMS::html_values{outageitemtypeid}{value} == 3) {
	# system

	$recipients = CMS_OUTAGE_DB::get_outage_contacts_for_all;
	  
      }

      # ALL Recipients get email for now.
      foreach my $recep (@$recipients) {
	unshift (@$recep, "email");
      }


      unless (my $return = NOTIFICATION::send
	      ($recipients, 
	       "Outage Notice",
	       $CMS::html_values{cust_message}{value},
	       "Reach Outage System",
	       "outages\@TelstraInternational.com")) {
	  
	CMS::add_error($NOTIFICATION::error);	
      }





      CMS::log("outage_created", "$ticketno");
      generate_view; # menu
    } else {
      
      generate_view; # menu
    }

    ###################### END CREATE NEW OUTAGE ###############

    ###################### SEARCH OUTAGES ######################

  } elsif ($CMS::html_values{command}{value} eq "search_outages") {

    if (defined $CMS::html_values{submit}{value}) {
      
      if($CMS::html_values{submit}{value} =~ /clear/i) {
	
	$CMS::html_values{search_ticketno}{value} = "";
	$CMS::html_values{serach_date}{value} = "";
	$CMS::html_values{search_reportedby}{value} = "";
	$CMS::html_values{search_outageitemtype}{value} = "";
	$CMS::html_values{search_outageitem}{value} = "";
	$CMS::html_values{search_outagestatus}{value} = "";
	$CMS::html_values{clear_search}{value} = "";
	$CMS::html_values{search}{value} = "";
	TI_HTML::set_datetime_search_values(\%CMS::html_values,
					    "search_startdate",
					    "2001-01-01 00:00:00",
					    ((gmtime(time))[5]+1900)."-12-31 23:59:59");

	
	generate_view("nodata");
      } else {
	# user has searched by parameters

	my ($start, $end) = TI_HTML::get_datetime_search_values
	  (\%CMS::html_values, "search_startdate");


	generate_view($CMS::html_values{search_ticketno}{value},
		      $start, $end,
		      $CMS::html_values{search_reportedby}{value},
		      $CMS::html_values{search_outageitemtype}{value},
		      $CMS::html_values{search_outageitem}{value},
		      $CMS::html_values{search_outagestatus}{value});
	
      }
    } else {
      # user wants to search
      # go to just inputs
      generate_view("nodata");
    }
    ########################## END SEARCH OUTAGES ###############

    ########################## VIEW OUTAGES #####################
  } elsif ($CMS::html_values{command}{value} =~ /view_outage/){
    
    get_outage_notice;
    $CMS::html_values{othermenu}{value} = "View Outage";
    CMS::output_html("outage_update1");
  

    ########################## END VIEW OUTAGES #################

    ########################## UPDATE OUTAGES ###################
  } elsif ($CMS::html_values{command}{value} =~ /update_outage/) {
   
    if ((defined $CMS::html_values{submit}{value}) &&
	($CMS::html_values{submit}{value} eq "Go Back")) {
      get_outage_notice ("refresh");
      CMS::output_html("outage_update1");
	
    } else {

      

      if ((not defined($CMS::html_values{current_screen}{value})) || 
	  ($CMS::html_values{stage}{value} eq "0")) {
	$CMS::html_values{append_staff_message}{value} = "";
	$CMS::html_values{append_customer_message}{value} = "";
	get_outage_notice("update");
	$CMS::html_values{stage}{value} += 1;
      } elsif(CMS::check_screen
	      ($CMS::html_values{current_screen}{value})){
	get_outage_notice("refresh");
	$CMS::html_values{stage}{value} += 1;
      } else {
	get_outage_notice("refresh");
      }


      if ($CMS::html_values{stage}{value} eq "1") {


	$CMS::html_values{othermenu}{value} = "Update Outage";
	CMS::output_html("outage_update1");

      } elsif ($CMS::html_values{stage}{value} eq "2") {

	
	
	if ($CMS::html_values{append_customer_message}{value} ne "") {
	  $CMS::html_values{message}{value} = "";
	  # send email to customers
	  open (MAIL, "../templates/update.mail");
	  while (my $line = <MAIL>) {
	    while ($line =~ /\[\[\[.+\]\]\]/) {
	      no warnings;
	      $line =~ 
		s/\[\[\[(.+?)\]\]\]/$CMS::html_values{"$1"}{value}/i;
	    }
	    $CMS::html_values{message}{value} .= $line;
	  }
	} else {
	  $CMS::html_values{message}{value} =
	    "No message is to be sent.\nNote if you have changed ".
	      "times you should write a message for customers.";
	}
	
	CMS::output_html("outage_update2");
	
      } elsif ($CMS::html_values{stage}{value} eq "3") {
	
	CMS_OUTAGE_DB::update_outage
	    ($CMS::html_values{ticketno}{value},
	     $CMS::html_values{username}{value},
	     $CMS::html_values{'startdate.day'}{value},
	     $CMS::html_values{'startdate.month'}{value},
	     $CMS::html_values{'startdate.year'}{value},
	     $CMS::html_values{'starttime.hour'}{value},
	     $CMS::html_values{'starttime.minute'}{value},
	     $CMS::html_values{'enddate.day'}{value},
	     $CMS::html_values{'enddate.month'}{value},
	     $CMS::html_values{'enddate.year'}{value},
	     $CMS::html_values{'endtime.hour'}{value},
	     $CMS::html_values{'endtime.minute'}{value},
	     $CMS::html_values{outagestatusid}{value},
	     $CMS::html_values{append_customer_message}{value},
	     $CMS::html_values{append_staff_message}{value}
	    );
	
	$CMS::html_values{stage}{value} = "0";
	$CMS::html_values{othermenu}{value} = "View Outage";
	$CMS::html_values{command}{value} = "view_outage";
	
	my $recipients;
	
	if ($CMS::html_values{outageitemtypeid}{value} == 1) {
	  # router 
	  $recipients = CMS_OUTAGE_DB::get_outage_contacts_for_router
	  ($CMS::html_values{outageitem}{value},
	   $CMS::html_values{outagenotifyareaid}{value});
	} elsif ($CMS::html_values{outageitemtypeid}{value} == 2) {
	  # link
	  $recipients = CMS_OUTAGE_DB::get_outage_contacts_for_all;
	  
	} elsif ($CMS::html_values{outageitemtypeid}{value} == 3) {
	  # system
	  
	  $recipients = CMS_OUTAGE_DB::get_outage_contacts_for_all;
	  
	} 
	
	# ALL Recipients get email for now.
	foreach my $recep (@$recipients) {
	  unshift (@$recep, "email");
	}
	
	
	unless (my $return = NOTIFICATION::send
		($recipients, 
	       "Outage Notice - Update",
		 $CMS::html_values{append_customer_message}{value},
		 "Reach Outage System",
		 "outages\@TelstraInternational.com")) {
	  
	  CMS::add_error($NOTIFICATION::error);	
	}
	

	get_outage_notice;
	CMS::output_html("outage_update1");
	CMS::log("outage_updated", 
		 "$CMS::html_values{ticketno}{value}");

	
      }
    }

    ################## END UPDATE OUTAGES #######################

    ################## SOMETHING ASTRAY #########################
  } else {
    
    generate_view; #menu
  }
}

1;
