/*
 * An XML document type.
 * Localname: addCustomerRequest
 * Namespace: http://schemas.hp.com/ucmdb/management/1/params
 * Java type: com.hp.schemas.ucmdb.management._1.params.AddCustomerRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.management._1.params.impl;
/**
 * A document containing one addCustomerRequest(@http://schemas.hp.com/ucmdb/management/1/params) element.
 *
 * This is a complex type.
 */
public class AddCustomerRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.management._1.params.AddCustomerRequestDocument
{
    
    public AddCustomerRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDCUSTOMERREQUEST$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/management/1/params", "addCustomerRequest");
    
    
    /**
     * Gets the "addCustomerRequest" element
     */
    public com.hp.schemas.ucmdb.management._1.params.AddCustomerRequest getAddCustomerRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.AddCustomerRequest target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.AddCustomerRequest)get_store().find_element_user(ADDCUSTOMERREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "addCustomerRequest" element
     */
    public void setAddCustomerRequest(com.hp.schemas.ucmdb.management._1.params.AddCustomerRequest addCustomerRequest)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.AddCustomerRequest target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.AddCustomerRequest)get_store().find_element_user(ADDCUSTOMERREQUEST$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.management._1.params.AddCustomerRequest)get_store().add_element_user(ADDCUSTOMERREQUEST$0);
            }
            target.set(addCustomerRequest);
        }
    }
    
    /**
     * Appends and returns a new empty "addCustomerRequest" element
     */
    public com.hp.schemas.ucmdb.management._1.params.AddCustomerRequest addNewAddCustomerRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.management._1.params.AddCustomerRequest target = null;
            target = (com.hp.schemas.ucmdb.management._1.params.AddCustomerRequest)get_store().add_element_user(ADDCUSTOMERREQUEST$0);
            return target;
        }
    }
}
