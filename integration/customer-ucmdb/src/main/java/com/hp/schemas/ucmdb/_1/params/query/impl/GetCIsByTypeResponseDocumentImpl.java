/*
 * An XML document type.
 * Localname: getCIsByTypeResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one getCIsByTypeResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class GetCIsByTypeResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponseDocument
{
    
    public GetCIsByTypeResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCISBYTYPERESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "getCIsByTypeResponse");
    
    
    /**
     * Gets the "getCIsByTypeResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponse getGetCIsByTypeResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponse)get_store().find_element_user(GETCISBYTYPERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getCIsByTypeResponse" element
     */
    public void setGetCIsByTypeResponse(com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponse getCIsByTypeResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponse)get_store().find_element_user(GETCISBYTYPERESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponse)get_store().add_element_user(GETCISBYTYPERESPONSE$0);
            }
            target.set(getCIsByTypeResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getCIsByTypeResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponse addNewGetCIsByTypeResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.GetCIsByTypeResponse)get_store().add_element_user(GETCISBYTYPERESPONSE$0);
            return target;
        }
    }
}
