/*
 * An XML document type.
 * Localname: deleteCIsAndRelations
 * Namespace: http://schemas.hp.com/ucmdb/1/params/update
 * Java type: com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.update.impl;
/**
 * A document containing one deleteCIsAndRelations(@http://schemas.hp.com/ucmdb/1/params/update) element.
 *
 * This is a complex type.
 */
public class DeleteCIsAndRelationsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelationsDocument
{
    
    public DeleteCIsAndRelationsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DELETECISANDRELATIONS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/update", "deleteCIsAndRelations");
    
    
    /**
     * Gets the "deleteCIsAndRelations" element
     */
    public com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations getDeleteCIsAndRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations)get_store().find_element_user(DELETECISANDRELATIONS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "deleteCIsAndRelations" element
     */
    public void setDeleteCIsAndRelations(com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations deleteCIsAndRelations)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations)get_store().find_element_user(DELETECISANDRELATIONS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations)get_store().add_element_user(DELETECISANDRELATIONS$0);
            }
            target.set(deleteCIsAndRelations);
        }
    }
    
    /**
     * Appends and returns a new empty "deleteCIsAndRelations" element
     */
    public com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations addNewDeleteCIsAndRelations()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations target = null;
            target = (com.hp.schemas.ucmdb._1.params.update.DeleteCIsAndRelations)get_store().add_element_user(DELETECISANDRELATIONS$0);
            return target;
        }
    }
}
