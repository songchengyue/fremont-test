# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
#
# $Id: CMS_ROUTERS.pm,v 1.3 2016/07/07 15:19:16 d804709 Exp $

###################################################################
# File        : CMS_ROUTERS.pm
# Description : Passes parameters and calls the functions in CMS_ROUTERS_DB.pm file.
# Parameters to be passed and usage : adding,location,community,router_switch,
#                                     cos,cos_v,cos_e,det_poll,prefix_update.
# Output of the script
# Input File  : 
# Output File :
# Author      : KETAKI THOMBARE (Infosys Tech. Ltd., Bangalore )
# Started On  : 15 July 2009
# Modification History :
#  Date               Name             Change/Description
# ------------------------------------------------------------------
# 15July2009    KETAKI THOMBARE     Baseline for EVPL.
# 15July2009    KETAKI THOMBARE     Added for EVPL.
# 25OCT2010	KARUNA BALLAL	    Modified for IP Transit Ph5-auto suggest feature
####################################################################


package CMS_ROUTERS;
use Exporter;
use DBI;
use warnings;
use strict;
use CMS_ROUTERS_DB;

our @ISA = qw (Exporter);

our $error;

##Debug option added by Karuna
my $debug = 1;
#to disable debug statement, uncomment following line and vice versa
#$debug = 0;
if ($debug){open (IPT,">/data1/tmp_log/routerslog.log");}


sub start {
	if ($debug){print IPT "PM inside start function\n";}


  	if ((not defined($CMS::html_values{command}{value})) || 
      	((defined($CMS::html_values{submit}{value}) &&
	$CMS::html_values{submit}{value} =~ /cancel/i))) {
    		$CMS::html_values{command}{value} = "";
    		CMS::clear_html_values();
    		$CMS::html_values{subsystem}{value} = "routers";
    		$CMS::html_values{command}{value} = "add_router";
  	}
	## ++ IPT5
   	if (defined($CMS::html_values{command}{value})){
		if ($CMS::html_values{command}{value} eq "get_router_string") {
			##called from the front end script
        		if ($debug){print IPT "PM routers entered get_router_string\n";}
			##get result from the DB module
        	        my $retStr = CMS_ROUTERS_DB::fetch_routers();
			#if ($debug){print IPT "PM retStr:$retStr\n";}
			##print on screen for auto complete feature
        	        print "Content-type: html/text\n\n";
                	print $retStr;
		} elsif ($CMS::html_values{command}{value} eq "get_router_switch"){
			##called from the front end script
                        if ($debug){print IPT "PM routers entered get_router_switch\n";}
			#my $routerid = shift @_;
			my $routerid = $CMS::html_values{routerId}{value};
			my $retStr = CMS_ROUTERS_DB::get_router_switch($routerid);
			if ($debug){print IPT "PM get_router_switch retStr:$retStr\n";}
			print "Content-type: html/text\n\n";
                        print $retStr;
		}
	}
	## -- IPT5
  	if ($CMS::html_values{command}{value} =~ /(.+)\_(.+)/) {
    		my $command = $1;
    		my $type = $2;
  		# ++ EVPL
  		# Getting values for router/Switch Type dropdown.
  		$CMS::html_values{router_switch}{options} = CMS_ROUTERS_DB::get_routertype();
  		# -- EVPL
        	if ($type eq "router") {
          		$CMS::html_values{location}{options} =
            		CMS_ROUTERS_DB::get_pop("ALL");
        	} elsif ($type eq "pop") {
          		$CMS::html_values{location}{options} =
            		CMS_ROUTERS_DB::get_city("ALL");
        	} elsif ($type eq "city") {
          		$CMS::html_values{location}{options} =
            		CMS_ROUTERS_DB::get_country("ALL");#changed for country code
	
		#Added by Rahul for country code
        	}     elsif ($type eq "country") {
           		$CMS::html_values{location}{options} =
            		CMS_ROUTERS_DB::get_region("ALL");
          	} #End change
         	elsif ($type eq "region") {
          		$CMS::html_values{location}{options} =
            		[["world"]];
        	}

        	foreach my $location (@{$CMS::html_values{location}{options}}){
          		my $string = join (", ", @$location);
          		$string =~ s/, $//g;
          		$location = [$$location[0], $string];
        	}

    		if ($command eq "add") {
      			if (defined ($CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} =~ /add/i)) {
  				# Added for EVPL
   				# Getting values for router/Switch Type dropdown
   				$CMS::html_values{router_switch}{options} = CMS_ROUTERS_DB::get_routertype();
				# End of EVPL
				my $result;
				# ++ EVPL
				# Passing entered values.  
				if ($type eq "router") {
	  				$result = CMS_ROUTERS_DB::add_router
					($CMS::html_values{adding}{value},
	     				$CMS::html_values{location}{value},
	     				$CMS::html_values{community}{value},
	     				$CMS::html_values{router_switch}{value},
	     				$CMS::html_values{cos}{value},
	     				$CMS::html_values{cos_v}{value},	
	     				$CMS::html_values{cos_e}{value},
	     				$CMS::html_values{det_poll}{value},
             				$CMS::html_values{prefix_update}{value});
        			# -- EVPL
				} elsif ($type eq "pop") {
	  				$result = CMS_ROUTERS_DB::add_pop
	    				($CMS::html_values{adding}{value},
	     				$CMS::html_values{location}{value});
					} elsif ($type eq "city") {
	  				$result = CMS_ROUTERS_DB::add_city
	    				($CMS::html_values{adding}{value},
	     				$CMS::html_values{location}{value});
				} 
				# Added for country code update
            			elsif ($type eq "country") {
            				$result = CMS_ROUTERS_DB::add_country
              				($CMS::html_values{adding}{value},
               				$CMS::html_values{location}{value});
          			# EOC
	
				}  elsif ($type eq "region") {
	  				$result = CMS_ROUTERS_DB::add_region
	    				($CMS::html_values{adding}{value},
	     				$CMS::html_values{location}{value});
			}
	
				if ($result) {
	  				CMS::add_error("$CMS::html_values{adding}{value} added to $CMS::html_values{location}{value}.");
	  				CMS::output_html("routers_add");
				} else {
	  				CMS::add_error $CMS_ROUTERS_DB::error;
	  				CMS::output_html("routers_add");
          				# ++ EVPL
	  				# Getting values for router/Switch Type dropdown
	  				$CMS::html_values{router_switch}{options} = CMS_ROUTERS_DB::get_routertype();
	  				# -- EVPL
				}
          				# ++ EVPL
	  				# Getting values for router/Switch Type dropdown
	  				$CMS::html_values{router_switch}{options} = CMS_ROUTERS_DB::get_routertype();	
	  				# -- EVPL
      				} else {
					if ($type eq "router") {
	  				$CMS::html_values{location}{options} = 
	    				CMS_ROUTERS_DB::get_pop("ALL");
				} elsif ($type eq "pop") {
	  				$CMS::html_values{location}{options} = 
	    				CMS_ROUTERS_DB::get_city("ALL");
				} elsif ($type eq "city") {
	  				$CMS::html_values{location}{options} = 
	    				CMS_ROUTERS_DB::get_country("ALL"); #changed for country code update
				} 
				#Added by Rahul for Country Code
             			elsif ($type eq "country") {
             			$CMS::html_values{location}{options} =
               			CMS_ROUTERS_DB::get_region("ALL");
             			# EoC
           			}	
	  			elsif ($type eq "region") {
	  			$CMS::html_values{location}{options} = [["world"]];
				}
				foreach my $location (@{$CMS::html_values{location}{options}}){
	  				my $string = join (", ", @$location);
					$string =~ s/, $//g;
	  				$location = [$$location[0], $string];
				}
				CMS::output_html("routers_add");
      			}
    			} elsif ($command eq "delete") {
      				if (defined ($CMS::html_values{submit}{value}) && ($CMS::html_values{submit}{value} =~ /delete/i)) {
					# has clicked delete button
					my $result;
					if ($type eq "router") {
	  					$result = CMS_ROUTERS_DB::delete_router($CMS::html_values{deleting}{value});
					} elsif ($type eq "pop") {
	  					$result = CMS_ROUTERS_DB::delete_pop($CMS::html_values{deleting}{value});
					} elsif ($type eq "city") {
	  					$result = CMS_ROUTERS_DB::delete_city($CMS::html_values{deleting}{value});
					}
					# Added for country code
               				elsif ($type eq "country") {
               					$result = CMS_ROUTERS_DB::delete_country($CMS::html_values{deleting}{value});
             				}
             				#EoC
	  				elsif ($type eq "region") {
	  					$result = CMS_ROUTERS_DB::delete_region($CMS::html_values{deleting}{value});
					}
					if ($type eq "router") {
						if ($result == 1) {
	  						CMS::add_error("$CMS::html_values{deleting}{value} deleted successfully.");
	  						CMS::output_html("routers_delete");
					} elsif($result =~ /0/) {
	  					CMS::add_error ("Router not present in database.");
	  					CMS::output_html("routers_delete");
					}else{
						CMS::add_error ("Link record Exists for the router.");
						CMS::output_html("routers_delete");
        				}
        			}else{
					if ($result) {
          					CMS::add_error("$CMS::html_values{deleting}{value} deleted successfully.");
          					CMS::output_html("routers_delete");
        				}else{
						CMS::add_error ($CMS_ROUTERS_DB::error);
        					CMS::output_html("routers_delete");
        				}
      				}
    				}else{
					CMS::output_html("routers_delete");
        			}
    				#} elsif ($command eq "edit") {
    				} elsif ($command eq "edit") {
					if ($debug){print IPT "PM EDIT Entered Save\n";}
					my $result;
					if (defined ($CMS::html_values{submit}{value}) &&
					# ++EVPL
          					($CMS::html_values{submit}{value} =~ /save/i)) {
						print "Not yet! ($CMS::html_values{adding}{value}) ($CMS::html_values{routerid}{value}, $CMS::html_values{community}{value}, $CMS::html_values{cos}{value}, $CMS::html_values{det_poll}{value}, $CMS::html_values{prefix_update}{value}, $CMS::html_values{cos_v}{value}, $CMS::html_values{cos_e}{value},$CMS::html_values{router_switch}{value})";
						## ++ IPT5
						my $router_details = $CMS::html_values{routerid}{value};
						my @router_details = split (/\,/,$router_details);
						my $routerid = $router_details[0];
						#uncommented by Nisha
						$CMS::html_values{routerid}{value} = $routerid;
						if ($debug){print IPT "PM EDIT routerid:$routerid\n";}
						if ($debug){print IPT "PM EDIT values ($CMS::html_values{adding}{value}) ($CMS::html_values{routerid}{value}, $CMS::html_values{community}{value}, $CMS::html_values{cos}{value}, $CMS::html_values{det_poll}{value}, $CMS::html_values{prefix_update}{value}, $CMS::html_values{cos_v}{value}, $CMS::html_values{cos_e}{value},$CMS::html_values{router_switch}{value})\n";}
						$result = CMS_ROUTERS_DB::edit_router
						## Modified by Karuna for IP Transit ph5
						($routerid,
						## -- IPT5
             					$CMS::html_values{community}{value},
             					$CMS::html_values{cos}{value},
             					$CMS::html_values{det_poll}{value},
                        			$CMS::html_values{prefix_update}{value},
						$CMS::html_values{cos_v}{value},
						$CMS::html_values{cos_e}{value},
						$CMS::html_values{router_switch}{value});
					# --EVPL		

					if ($result) {
						## Modified by Karuna for IP Transit ph5
          					CMS::add_error("$routerid updated.");
						#Added by Nisha for IPTph5
						$CMS::html_values{routerid}{options} = CMS_ROUTERS_DB::get_router("ALL");
						$CMS::html_values{router_switch}{options} = CMS_ROUTERS_DB::get_routertype();
						foreach my $router (@{$CMS::html_values{routerid}{options}}){
							my $string = join (", ", @$router);
							$string =~ s/, $//g;
							$router = [$$router[0], $string];
						}
						$CMS::html_values{stage}{value}="2";
          					CMS::output_html("routers_edit");
        					} else {
          						CMS::add_error ($CMS_ROUTERS_DB::error);
          						CMS::output_html("routers_edit");
        					}
					} else {
						#Added by Nisha on 10Dec2010 for IPTph5
						$CMS::html_values{routerid}{options} = CMS_ROUTERS_DB::get_router("ALL");
						$CMS::html_values{router_switch}{options} = CMS_ROUTERS_DB::get_routertype();
						foreach my $router (@{$CMS::html_values{routerid}{options}}){
							my $string = join (", ", @$router);
							$string =~ s/, $//g;
							$router = [$$router[0], $string];
						}
						CMS::output_html("routers_edit");
					}
    					} elsif ($command eq "list") {
      						my $result;
      						if ($type eq "router") {
							$CMS::html_values{routers}{table} = 
							#IPT5
	  						CMS_ROUTERS_DB::get_routersw();
							# ++EVPL
							#Added Router/Switch Type column for IP Transit Ph4c by Nisha on 06Oct2010
							unshift (@{$CMS::html_values{routers}{table}},
		 					["Router", "POP", "Community", "City", "Region", "CoS", "Detail", "Prefix Update", "VPLS", "EVPL", "Router/Switch Type", "Router Software"]);
							# --EVPL
							$CMS::html_values{routers}{header_rows} = 1;
      						} elsif ($type eq "pop") {
							$CMS::html_values{routers}{table} = 
	  						CMS_ROUTERS_DB::get_pop("ALL");
							unshift (@{$CMS::html_values{routers}{table}},
		 					["POP", "City", "Region"]);
							$CMS::html_values{routers}{header_rows} = 1;
      						} elsif ($type eq "city") {
							$CMS::html_values{routers}{table} = 
	  						CMS_ROUTERS_DB::get_city("ALL");
							unshift (@{$CMS::html_values{routers}{table}},
		 					["City", "Region"]);
							$CMS::html_values{routers}{header_rows} = 1;
      						} 
 						# Added for country code update
             					elsif ($type eq "country") {
             						$CMS::html_values{routers}{table} =
               						CMS_ROUTERS_DB::get_country("ALL");
             						unshift(@{$CMS::html_values{routers}{table}},
                     					["Country", "Region"]);
             						$CMS::html_values{routers}{header_rows} = 1;
           					}
						elsif ($type eq "region") {
							$CMS::html_values{routers}{table} = 
	  						CMS_ROUTERS_DB::get_region("ALL");
							unshift(@{$CMS::html_values{routers}{table}},
							["Region"]);
							$CMS::html_values{routers}{header_rows} = 1;
      						}
      						CMS::output_html("routers_list");
    
				}
				}
		}
1;
