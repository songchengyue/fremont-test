#!/usr/bin/perl

=head1 Name

    OAuth2TokenCheck.pl - check the validity of some tokens

=head1 Description

    Test a list of tokens for validity. Useful for testing OAuth2 token behaviour.

=cut


BEGIN { push @INC, '.'; };

use Log::Log4perl qw(:easy);
use OAuth2Token;

#
# you can get a valid token with the get-token.bat (in dev anyway)
#
my @tokens = (
    'ff4d977f-2dca-4126-87b6-becd4961d84a', # valid
    '5e32aefd-e8c2-4e0a-afe0-8f7c975a74f0' # invalid
);

# use local test config instead of the default
$OAuth2Token::config_file_glob = "./OAuth2TokenCheckConfig.yml";

Log::Log4perl->easy_init($INFO);

# for debugging
#Log::Log4perl->easy_init($DEBUG);

our $logger = Log::Log4perl->get_logger();

foreach my $token (@tokens) {

    my $invalid = OAuth2Token::invalid($token);

    if (!$invalid) {
        $logger->info("Token $token is valid");
    } else {
        $logger->info("Token $token is invalid (reason: $invalid)");
    }
}
