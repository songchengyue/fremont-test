/*
 * An XML document type.
 * Localname: getProbesNamesResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getProbesNamesResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetProbesNamesResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponseDocument
{
    
    public GetProbesNamesResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETPROBESNAMESRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbesNamesResponse");
    
    
    /**
     * Gets the "getProbesNamesResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponse getGetProbesNamesResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponse)get_store().find_element_user(GETPROBESNAMESRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getProbesNamesResponse" element
     */
    public void setGetProbesNamesResponse(com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponse getProbesNamesResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponse)get_store().find_element_user(GETPROBESNAMESRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponse)get_store().add_element_user(GETPROBESNAMESRESPONSE$0);
            }
            target.set(getProbesNamesResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getProbesNamesResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponse addNewGetProbesNamesResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbesNamesResponse)get_store().add_element_user(GETPROBESNAMESRESPONSE$0);
            return target;
        }
    }
}
