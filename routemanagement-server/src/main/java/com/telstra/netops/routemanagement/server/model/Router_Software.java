package com.telstra.netops.routemanagement.server.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for Router_Software table
 *
 */
@Entity
@Table(name = "Router_Software")
public class Router_Software implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "Router_Software_ID")
	private String id;

	@Column(name = "Router_Software")
	private String routerSoftware;

	@Column(name = "routername")
	private String routerName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRouter_Software() {
		return routerSoftware;
	}

	public void setRouter_Software(String routerSoftware) {
		this.routerSoftware = routerSoftware;
	}

	public String getRoutername() {
		return routerName;
	}

	public void setRoutername(String routerName) {
		this.routerName = routerName;
	}

}
