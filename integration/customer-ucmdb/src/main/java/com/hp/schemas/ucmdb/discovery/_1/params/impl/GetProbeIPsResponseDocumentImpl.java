/*
 * An XML document type.
 * Localname: getProbeIPsResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * A document containing one getProbeIPsResponse(@http://schemas.hp.com/ucmdb/discovery/1/params) element.
 *
 * This is a complex type.
 */
public class GetProbeIPsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponseDocument
{
    
    public GetProbeIPsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETPROBEIPSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbeIPsResponse");
    
    
    /**
     * Gets the "getProbeIPsResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse getGetProbeIPsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse)get_store().find_element_user(GETPROBEIPSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getProbeIPsResponse" element
     */
    public void setGetProbeIPsResponse(com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse getProbeIPsResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse)get_store().find_element_user(GETPROBEIPSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse)get_store().add_element_user(GETPROBEIPSRESPONSE$0);
            }
            target.set(getProbeIPsResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "getProbeIPsResponse" element
     */
    public com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse addNewGetProbeIPsResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse target = null;
            target = (com.hp.schemas.ucmdb.discovery._1.params.GetProbeIPsResponse)get_store().add_element_user(GETPROBEIPSRESPONSE$0);
            return target;
        }
    }
}
