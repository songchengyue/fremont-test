package com.telstra.netops.dns.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientDnsRemoveRequest {
	
	private String customerId;
	
	private String ids;
	
	private String domain;



	public static ClientDnsRemoveRequest create(String customerId, String ids,String domain) {
		ClientDnsRemoveRequest request = new ClientDnsRemoveRequest();
		request.setCustomerId(customerId);
		request.setIds(ids);
		request.setDomain(domain);
		
		return request;
	}
	
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
}
