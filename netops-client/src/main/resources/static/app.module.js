(function(){
var app = angular.module('netops', ['ngRoute', 'netops.utilModule', 'netops.lookingGlassModule', 'netops.dnsModule', 'netops.routeManagementModule', 'LocalStorageModule'])

	.run(function($rootScope, $location, $anchorScroll,$templateCache) {
		//when the route is changed scroll to the proper element.
		$rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
			if($location.hash()) $anchorScroll();  
		});
		$rootScope.$on('$routeChangeStart', function(event, next, current) {
			if (typeof(current) !== 'undefined'){
				$templateCache.remove(current.templateUrl);
			}
		});
	})
	
 .config(function (localStorageServiceProvider) {
  localStorageServiceProvider
  
    //
    // Namespace our local storage variables to avoid clashes with other applications
    //
  
    .setPrefix('netops')
    
    //
    // Turn off cookie "fall back" for browsers that don't support local storage.
    //
    // This means we don't support browsers that don't support local storage.
    // We use local storage rather that cookies to store authentication details.
    // Using local storage instead of cookies means we are protected from 
    // CSRF attacks as such attacks rely on authentication details being
    // stored in cookies.
    //
    // Do not turn on cookie support without adding CSRF protection. For details
    // no CSRF see http://docs.spring.io/spring-security/site/docs/current/reference/html/csrf.html
    //
  
    .setDefaultToCookie(false)
    
    // setitem notification required (used in ProductController)
    .setNotify(true, false);
})

.constant('ERROR_TEXT', {
        'SERVER': "Error occurred on the server. Please contact your system administrator."
    })
    
.factory('Data', function ($q) {

    var data = {
        Username: '',
        Role:'',
        SAML:'',
        UserProfile:'',
        SuccessCode:'',
        ErrorFromServer:'',
        ErrorCode:'',
        ProductLine: null,
        ProductLineDeferred: $q.defer()
    };

    return {
        getSuccessCode: function () {
            return data.SuccessCode;
        },
        setSuccessCode: function (SuccessCode) {
            data.SuccessCode = SuccessCode;
            // data.SuccessCode;
        },
        getErrorFromServer: function () {
            return data.ErrorFromServer;
        },
        setErrorFromServer: function (ErrorFromServer) {
            data.ErrorFromServer = ErrorFromServer;
            // data.SuccessCode;
        },
        setErrorCode: function (ErrorCode) {
        data.ErrorCode = ErrorCode;
        //return data.ErrorCode ;
        },
        getErrorCode: function () {
        return  data.ErrorCode;
        },
        getProductLine: function () {
            return data.ProductLine;
        },
        setProductLine: function (ProductLine) {
            data.ProductLine = ProductLine;
        },		
		getRole: function () {
			return data.Role;
		},
		setRole: function (Role) {
			data.Role = Role;
		},
        setProductLineAsync: function (ProductLine) {
        	data.ProductLineDeferred.resolve(ProductLine);
        },	
		getProductLineAsync: function (ProductLine) {
			return data.ProductLineDeferred.promise;
        },
    
    };
})
 .factory('oauthTokenHttpInterceptor', function (localStorageService, moduleRegistry) {
    
	var interceptor = {
	        request: function(config) {
	        	
	        	if (moduleRegistry.includesBaseUrl(config.url)) {
	        		if (localStorageService.get('token') == null) {
	        			console.log('Error: no oauth token available!');
	        		} else {   
	        			config.headers['Authorization'] ='Bearer ' + localStorageService.get('token');
	        		}
	        	}
	        	
	            return config;
	        }
	    };

    return interceptor;
})

.factory('notAuthorizedHttpInterceptor', function ($q, $location, localStorageService) {
    
	var interceptor = {
			responseError: function(response) {
	        					
	     		if (response.status == 401) {  
	     			// typically this means our token has expired so let's force a logout
	     			console.log('token expired, removing and forcing logout');
	     			localStorageService.remove('productLine');
	     			localStorageService.remove('token');
	     			$location.path('/Logout');
	     		}
	     		
	     		return $q.reject(response);
	        }
	    };

    return interceptor;
})

 .factory('errorHttpInterceptor', function ($q) {
    
	var interceptor = {
			responseError: function(response) {
	        	
				console.log("An error was encountered " + response.status);
				
	     		if (response.status == 422) {         	
	     			response.errorText = response.data.message;
	     		}
	     			     		
	     		return $q.reject(response);
	        }
	    };

    return interceptor;
})

 .factory('baseUrlHttpInterceptor', function (localStorageService, moduleRegistry) {
    
	var interceptor = {
	        request: function(config) {
	        	
	        	var module = moduleRegistry.includesBaseUrl(config.url);
	        	
	        	if (module) {
	        		var clientBaseUrl = localStorageService.get('env.baseurl.' + module.name);
	        		if (clientBaseUrl) {
		        		config.url = clientBaseUrl + config.url;
		        	}
	        	}
	        	
	            return config;
	        }
	    };

    return interceptor;
})

 .config(['$httpProvider', function($httpProvider) {  
	    $httpProvider.interceptors.push('oauthTokenHttpInterceptor');
	    $httpProvider.interceptors.push('notAuthorizedHttpInterceptor');
	    $httpProvider.interceptors.push('errorHttpInterceptor');
	    $httpProvider.interceptors.push('baseUrlHttpInterceptor');
 }])
 
 .controller('MainController', function($scope, $route, $routeParams, $location, localStorageService) {  
     $scope.loggedIn = localStorageService.get('token') != null; 
     
  	$scope.logout = function(){
		console.log('MainController(): logout');
		localStorageService.remove('productLine');
		localStorageService.remove('token');
		$location.path('/Logout');
	}
 })
 
	.controller('ProductController', function($http,$scope, $routeParams,$rootScope,$window,$location,localStorageService,Data) {
     $scope.name = "ProductController";
     $scope.params = $routeParams;
     $scope.products;
     $scope.error;
     $scope.placeholder="Product Selection"
     $scope.sProductLine;
	 $scope.role  =localStorageService.get('role');
     //getting products from the db for the customer ID
     $scope.$on("LocalStorageModule.notification.setitem", function (event, parameters) {   	 
    	 
    	 if (parameters.key == 'token' && parameters.newvalue != null) {
	    	 
	    	 $scope.welcome=false;
	    	 $window.location.href = '#/Netops'; 
	    	 $http({
	    		 method	: 'GET',
	    		 url		: '/lookingglass-server-1.0/productline'
	    	 	}).then(function (response) {
	    	 		// this callback will be called asynchronously
	    	 		// when the response is available
	
    	 			$scope.welcome=true;
    	 			
    	 			$scope.productLines = response.data;
    	 			
    	 			var redirectProductLine = localStorageService.get('productLine');
    	 			console.log("redirectProductLine "+ redirectProductLine);
    	 			
    	 			for (var i = 0; i < $scope.productLines.length; i++) {
    	 				if ($scope.productLines[i].code == redirectProductLine) {
    	 					$scope.sProductLine = $scope.productLines[i];
    	 				}
    	 			}
    	 			
    	 			if (redirectProductLine != null && $scope.sProductLine == null) {
    	 				console.log("redirectProductLine " + redirectProductLine + " not found!");
    	 			}
    	 			
    	 			if (redirectProductLine != null && $scope.sProductLine != null) {
    	 				// we know our product line, set up our tools!
    	 				$scope.getTools();
    	 			}
    	 			
    	 			if(Object.keys(response.data).length==0){
    	 				$scope.productLines = ["No Products"];
    	 				$window.location.href = '#/Error';
    	 			}
	   		
	    	 	}, function (response) {
	    	 		//unhandled exception
	    	 		// called asynchronously if an error occurs
	    	 		// or server returns response with an error status.
	    	 		//in this case redirected to error page
	    	 		$window.location.href = '#/Error';
	    	 		$scope.productLines = ["No Products"];
	    	 		$scope.welcome=false;
	    	 	});	
    	  }
	 });
     
     
     $scope.getTools = function(){
    	 
    	 if($scope.sProductLine=="Product Selection"){
    	 	window.location.href = '#/Netops';
    	 }
    	 $rootScope.loading=false;
    	 
    	 var selectedProductLine = $scope.sProductLine.code;
    	 console.log('selected product line is ' + selectedProductLine);
    	 Data.setProductLine($scope.sProductLine); ////uday added
    	 Data.setProductLineAsync($scope.sProductLine);
    	 var dns="DNS";
    	 var lookingGlass="Looking Glass";
    	 var networkCLI="Network CLI";
    	 var routeManagement="Route Management";
    	 $scope.data = {
 	            	dns				: false,
 	            	lookingGlass	: false,
 	            	networkCLI		: false,
 	            	routeManagement	: false
 	        		}; 
    	 

    	 $http({
    	   	  method	: 'GET',
    	   	  url		: '/lookingglass-server-1.0/tools?productLine=' + selectedProductLine
    	   	}).then(function (response) {
    	   	    // this callback will be called asynchronously
    	   	    // when the response is available

      			$rootScope.loading=false;
      	    	
      	    	 if($scope.sProductLine=="No Products" || $scope.sProductLine=="Product Selection" ){
      	    		$window.location.href = '#/Error';
      	    		 $scope.data = {
      	  	            	dns				: false,
      	  	            	lookingGlass	: false,
      	  	            	networkCLI		: false,
      	  	            	routeManagement	: false
      	  	        		}; 
      	    	 } else {                             
                     $scope.data = {
                               dns                : response.data.indexOf('DNS') >= 0,
                               lookingGlass    : response.data.indexOf('Looking Glass') >= 0,
                               networkCLI        : response.data.indexOf('Network CLI') >= 0,
                               routeManagement    : response.data.indexOf('Route Management') >= 0
                               }; 
                 }
    	   		
    	   	  }, function (response) {
    	   		  //unhandled exception
    	   		// called asynchronously if an error occurs
    	     	    // or server returns response with an error status.
    	   		//in this case redirected to error page
    	     		
    	   	  });

     }
     
 })

 

 .controller('SuccessController',function($scope,Data,$location) {
	console.log('in controller');
	  
	 console.log('Data'+Data.getSuccessCode());
	$scope.successcode=Data.getSuccessCode();
	
   $scope.BackToHomePage=function(){
		$location.path('/Netops');	
	}
})
   ////for displaying err from server on validation failure from server end
   .controller('ServerErrorController',function($scope,Data,$location) {
		console.log('in controller');
		  
		 console.log('Data'+Data.getErrorFromServer());
		$scope.ErrorFromServer=Data.getErrorFromServer();
		
	   $scope.BackToHomePage=function(){
			$location.path('/Netops');	
		}
	
	   ////for displaying err from server
 })
 
  

	  //###########################################################################################################################################//

 
.directive('ipCheck',function(){
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
					//var regex = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$|([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))$/;
          				//var regex = /(^(?:(?:0?0?\d|0?[1-9]\d|1\d\d|2[0-5][0-5]|2[0-4]\d)\.){3}(?:0?0?\d|0?[1-9]\d|1\d\d|2[0-5][0-5]|2[0-4]\d)$)|(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))$)/gi;                       
					//var regex = /[^\w\s]/gi;
					var regex = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3})$|^((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]))$/;


					var validator = function(value){
							
                            ctrl.$setValidity('ipCheck', regex.test(value));
                           
							return value;
						};
						
						ctrl.$parsers.unshift(validator);
                   }
				};
			})			
.directive('validNumber',function(){
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
					//var regex = /^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*/  | /^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))$/;
					//var regex = /^((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?$|/^((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]))$/ ;
					var regex = /^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$|^((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]))$/;
					//var regex = /^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$|^((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]))$/;
					

		               var validator = function(value){
                            ctrl.$setValidity('validNumber', regex.test(value));

							return value;
						};
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})
 //////////added by chinju
			.directive('ipAddress',function(){
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
					
                      var regex=/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/;
						// alert(regex);
                       //console.log(regex_ip);
                        var validator = function(value){
                            ctrl.$setValidity('ipAddress', regex.test(value));
                     
							return value;
						};
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})
			

.directive('positiveNumber',function(){
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
						
					var regex_number = /^\d+$/;
                       
                        var validator = function(value){
                        	
                        	var abc='123';
                            ctrl.$setValidity('positiveNumber', regex_number.test(value));
                        
							return value;
						};
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})			
			
			
///directive for ipv6
.directive('specialCharacter',function(){
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
					var regex_special = /^[^~{\/\\#@!;\$%^&*<|\s(),'}+:?]+$/;
//alert(regex_special);
                        var validator = function(value){
                            ctrl.$setValidity('specialCharacter', regex_special.test(value));
                        
							return value;
						};
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})
			
			

			
 //directive for valid ipaddress
.directive('ipBlock',function(){
	//alert('hi');
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
						console.log('scope'+scope+'elm'+elm+'attrs'+attrs+'ctrl'+ctrl);
						//alert('hi');
					
					
						var validator=function(value){
							 
						var ipaddress = value.split("/");
                        	var regex_ipblock = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
                       
						var regex_prefix=/^\d+$/;
                        	var lastOctect=ipaddress[0].split(".");
                        	
                           var difference= (ipaddress[1]-24);
                        
                        	var divisor= Math.pow(2, difference);
                        	
                        	var newvalue=(256/divisor);
                        
                        	var validation=(lastOctect[3]%newvalue);
                        
                        	if(!(regex_ipblock.test(ipaddress[0]))){
                        		
                        		ctrl.$setValidity("ipadressvalidity",false);
                        		ctrl.$setValidity("noPrefix",true);
                        		ctrl.$setValidity("octectvalidation",true);
                        		ctrl.$setValidity("prefixvalidation",true);
                        		
                        		return undefined;	
                            }
                        	else if(ipaddress[1]==''|| ipaddress[1]==null){
                        		ctrl.$setValidity("ipadressvalidity",true);
                        		ctrl.$setValidity("noPrefix",false);
                        		ctrl.$setValidity("octectvalidation",true);
                        		ctrl.$setValidity("prefixvalidation",true);
                        		
                        	}
                        	else if(ipaddress[1]<24 || ipaddress[1]>32){
                        		
                        		ctrl.$setValidity("ipadressvalidity",true);
                        		ctrl.$setValidity("noPrefix",true);
                        		ctrl.$setValidity("octectvalidation",true);
                        		ctrl.$setValidity("prefixvalidation",false);
                        	}
                        	else if(validation!=0){
                        		
                        		ctrl.$setValidity("prefixvalidation",true);
                        		ctrl.$setValidity("ipadressvalidity",true);
                        		ctrl.$setValidity("noPrefix",true);
                        		ctrl.$setValidity("octectvalidation",false);
                        	}
                        	else{
                        		
                        		ctrl.$setValidity("prefixvalidation",true);
                        		ctrl.$setValidity("ipadressvalidity",true);
                        		ctrl.$setValidity("noPrefix",true);
                        		ctrl.$setValidity("octectvalidation",true);
                        		ctrl.$setValidity("ipBlock",true);
                          		
                          		return value;
                        	}
						  }
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})

			
 //directive for valid ipaddress route
	.directive('ipBlockroute',function(){
		return{
			require: "ngModel",
			link: function(scope, elm, attrs, ctrl){
				console.log('scope'+scope+'elm'+elm+'attrs'+attrs+'ctrl'+ctrl);


				var validator=function(value){

					var ipaddress = value.split("/");
					var regex_ipblock = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

					var regex_prefix=/^\d+$/;
					var lastOctect=ipaddress[0].split(".");

					var difference= (ipaddress[1]-24);

					var divisor= Math.pow(2, difference);

					var newvalue=(256/divisor);

					var validation=(lastOctect[3]%newvalue);

					if(!(regex_ipblock.test(ipaddress[0]))){

						ctrl.$setValidity("ipadressvalidity",false);
						ctrl.$setValidity("noPrefix",true);
						ctrl.$setValidity("octectvalidation",true);
						ctrl.$setValidity("prefixvalidation",true);

						return undefined;	
					}
					else if(ipaddress[1]==''|| ipaddress[1]==null){
						ctrl.$setValidity("ipadressvalidity",true);
						ctrl.$setValidity("noPrefix",false);
						ctrl.$setValidity("octectvalidation",true);
						ctrl.$setValidity("prefixvalidation",true);

					}
					else if(ipaddress[1]<8 || ipaddress[1]>24){

						ctrl.$setValidity("ipadressvalidity",true);
						ctrl.$setValidity("noPrefix",true);
						ctrl.$setValidity("octectvalidation",true);
						ctrl.$setValidity("prefixvalidation",false);
					}
					else{

						ctrl.$setValidity("prefixvalidation",true);
						ctrl.$setValidity("ipadressvalidity",true);
						ctrl.$setValidity("noPrefix",true);
						ctrl.$setValidity("ipBlock",true);

						return value;
				}

				ctrl.$parsers.unshift(validator);
			} 
			}
		};
	})	
/////directive to check email id
.directive('emailId',function(){
	//alert("emailid check");
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
					
						var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//alert (regex);
                        var validator = function(value){
                            ctrl.$setValidity('emailID', regex.test(value));
                        
							return value;
						};
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})
	
			////directive for text record to allow space in the input.
			.directive('textRecord',function(){
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
					var regex_special = /^[^~{\/\\#@!;\$%^&*<|(),'}+:?]+$/;
//alert(regex_special);
                        var validator = function(value){
                            ctrl.$setValidity('specialCharacter', regex_special.test(value));
                        
							return value;
						};
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})

			
////directive to check if IPv6 CDIR format. modified on 14-7-2016!
			.directive('ipBlockipv6',function(){
	//alert('hi');
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
						//console.log('scope'+scope+'elm'+elm+'attrs'+attrs+'ctrl'+ctrl);
						//alert('hi');
					
					
						var validator=function(value){
							 
						var ipaddress = value.split("/");
                        	var regex_ipblock = /^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/;
                       
						var regex_prefix=/^\d+$/;
						//alert(ipaddress[0]+"Prefix"+ipaddress[1]);
                        	
                        	
                        
                        	if(!(regex_ipblock.test(ipaddress[0]))){
                        		
                        		ctrl.$setValidity("ipadressvalidity",false);
                        		ctrl.$setValidity("noPrefix",true);
                        		ctrl.$setValidity("prefixtype",true);
                        		ctrl.$setValidity("invalidPrefix",true);
                        	
                        		
                        		return undefined;	
                            }
                        	else if(ipaddress[1]==''|| ipaddress[1]==null){
                        		ctrl.$setValidity("ipadressvalidity",true);
                        		ctrl.$setValidity("noPrefix",false);
                        		ctrl.$setValidity("invalidPrefix",true);
                        		ctrl.$setValidity("prefixtype",true);
                        	
                        
                        		
                        	}
                        	
                       else if(ipaddress[1]<48 || ipaddress[1]>64){
                        		
                        		ctrl.$setValidity("ipadressvalidity",true);
                        		ctrl.$setValidity("noPrefix",true);
                        		ctrl.$setValidity("invalidPrefix",false);
                        		ctrl.$setValidity("prefixtype",true);
                        		
                        	}
                       else if(!(regex_prefix.test(ipaddress[1]))){
                   		//alert('test 123');
                   		ctrl.$setValidity("invalidPrefix",true);
                   		ctrl.$setValidity("ipadressvalidity",true);
                   		ctrl.$setValidity("noPrefix",true);
                   		
                   		ctrl.$setValidity("prefixtype",false);
                   	}
                        	else{
                        		
                        		ctrl.$setValidity("prefixtype",true);
                        		ctrl.$setValidity("ipadressvalidity",true);
                        		ctrl.$setValidity("noPrefix",true);
                        		ctrl.$setValidity("invalidPrefix",true);
                        		ctrl.$setValidity("ipBlockipv6",true);
                          		
                          		return value;
                        	}
						  }
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})	   

			
	////directive to check if IPv6 route CDIR format.
			.directive('ipBlockipv6route',function(){
	//alert('hi');
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
						//console.log('scope'+scope+'elm'+elm+'attrs'+attrs+'ctrl'+ctrl);
						//alert('hi');
					
					
						var validator=function(value){
							 
						var ipaddress = value.split("/");
                        	var regex_ipblock = /^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/;
                       
						var regex_prefix=/^\d+$/;
						//alert(ipaddress[0]+"Prefix"+ipaddress[1]);
                        	
                        	
                        
                        	if(!(regex_ipblock.test(ipaddress[0]))){
                        		
                        		ctrl.$setValidity("ipadressvalidity",false);
                        		ctrl.$setValidity("noPrefix",true);
                        		
                        		return undefined;	
                            }
                        	else if(ipaddress[1]==''|| ipaddress[1]==null){
                        		ctrl.$setValidity("ipadressvalidity",true);
                        		ctrl.$setValidity("noPrefix",false);
                        
                        		
                        	}
                        	
                        	else{
                        		
                        	
                        		ctrl.$setValidity("ipadressvalidity",true);
                        		ctrl.$setValidity("noPrefix",true);
                        		
                        		ctrl.$setValidity("ipBlockipv6",true);
                          		
                          		return value;
                        	}
						  }
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})			
//directive to check if it is valid positive number.
.directive('positiveNumber',function(){
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
						console.log('scope'+scope+'elm'+elm+'attrs'+attrs+'ctrl'+ctrl);
					var regex_number = /^\d+$/;
                       
                        var validator = function(value){
                        	
                        	var abc='123';
                            ctrl.$setValidity('positiveNumber', regex_number.test(value));
                       
							return value;
						};
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})			
			
			
///directive for ipv6
.directive('ipVsix',function(){
	
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
					
						var regex = /^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/;
//alert (regex);
                        var validator = function(value){
                            ctrl.$setValidity('ipVsix', regex.test(value));
                        
							return value;
						};
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})
			
			///directive for ipv4
.directive('ipVfour',function(){
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
					var regex = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
          
                        var validator = function(value){
							
                            ctrl.$setValidity('ipCheck', regex.test(value));
                           
							return value;
						};
						
						ctrl.$parsers.unshift(validator);
                   }
				};
				
				
			})
			
///directive for orgin field Validation.
.directive('asNumber',function(){

				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
						console.log('scope'+scope+'elm'+elm+'attrs'+attrs+'ctrl'+ctrl);
					
					
					
						var validator=function(value){
							//alert('hi');
						var ASNumber = value.substring(0, 2);
						var RemainingNumber=value.substring(2);
                        	
                       
						var regex_prefix=/^\d+$/;
						if (!(regex_prefix.test(RemainingNumber))){
							//alert('hi in num testing');
							ctrl.$setValidity("asNumber",false);
						}
						else if(!(ASNumber=='AS' || ASNumber=='as')){
							//alert('hi in asnum testing');
							ctrl.$setValidity("asNumber",false);
						}
						else{
							ctrl.$setValidity("asNumber",true);	
							return value;
						}
                        	
                        
						  }
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})	  

			///////directive for domain and host name validation
			
			.directive('domainAndhost',function(){
	
	
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
					var regex_special = /^[^~{\/\\#@!;\$_%^&*<|\s(),'}+:?]+$/;
//alert(regex_special);
                        var validator = function(value){
                            ctrl.$setValidity('domainAndhost', regex_special.test(value));
                        
							return value;
						};
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})
			
    
.directive('specialCharacterhost',function(){
				return{
					require: "ngModel",
					link: function(scope, elm, attrs, ctrl){
					var regex_special = /^[^~{\/\\#@!_;\$%^&*<|\s(),'}+:?]+$/;
//alert(regex_special);
                        var validator = function(value){
                        	if (value=="" || value==null){
                        		//alert('in if');
                        		 ctrl.$setValidity('specialCharacterhost', true);
                        		 return value;
                        		 
                        	}
                        	else{
                            ctrl.$setValidity('specialCharacterhost', regex_special.test(value));
                        
							return value;
						}}
                        ;
						
						ctrl.$parsers.unshift(validator);
        
					}
				};
			})

    .directive('fileUpload', function () {
    	//alert("in fileUpload directive");
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                console.log(files);
              
                console.log(files[0].name);
            
                //iterate files since 'multiple' may be specified on the element,//Uncomment the below code if multiple files need to be uploaded.
               // for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    //scope.$emit("fileSelected", { file: files[i] });
                	scope.$emit("fileSelected", { file: files });
                   //}                            
            });
        }
    };
})
 .directive('multipleEmails', function () {
  return {
      require: 'ngModel',
      link: function (scope, element, attrs, ctrl) {
          ctrl.$parsers.unshift(function (modelvalue) {

              var emails = modelvalue.split(';');
           
              var regexPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

              // angular.foreach(emails, function() {
              var validityArr = emails.map(function (valueintheemails) {
            	  console.log('check flow'+valueintheemails);
            	  //alert('check flow'+valueintheemails)
            	  
                  if (modelvalue) {
                	  //alert('rawInput'+modelvalue);
                      return regexPattern.test(valueintheemails);

                  } else if (!modelvalue) {
                	  //alert('else');
                      return true;
                  }
              }); // sample return is [true, true, true, false, false, false]

              //console.log(emails, validityArr);
              var atLeastOneInvalid = false;

              angular.forEach(validityArr, function (valueofvalidityArr) {
            	  //alert('validityArr'+validityArr+'value'+valueofvalidityArr);
                  if (valueofvalidityArr == false){
                	  //alert('in the check');
                      atLeastOneInvalid = true;
                  }
              });
              //alert(atLeastOneInvalid+'value finally');
              //alert(!atLeastOneInvalid+'negation');
             
              if (!atLeastOneInvalid) {
            	  //alert('in fisrt ');
                  // ^ all I need is to call the angular email checker here, I think.
                  ctrl.$setValidity('multipleEmails', true);
                  return modelvalue;
              } else {
            	  //alert('in second setter ');
                  ctrl.$setValidity('multipleEmails', false);
                  return undefined;
              }

          });
      }
  };
})    

.config(function($routeProvider, $locationProvider) {
  $routeProvider
  .when('/Netops2', {
    templateUrl: 'templates/Netops2.html'
  })
  .when('/Netops', {
    templateUrl: 'templates/Netops.html'
  })
  .when('/Error', {
	templateUrl: 'templates/Error.html',
  })
   .when('/Success', {
		templateUrl: 'templates/Success.html',
	  })
 .when('/ServerError', {
		templateUrl: 'templates/ServerError.html',
	  })
  
  .when('/NoLocalStorage', {
	templateUrl: 'templates/NoLocalStorage.html',
  })
  .when('/Logout', {
	  redirectTo: function() {
	        window.location = '/nesp/jsp/ipsaappslogout.jsp';
	    }
  })
  .otherwise({ redirectTo: '/Netops' });

  // configure html5 to get links working on jsfiddle
  $locationProvider.html5Mode(false);
});

})();
