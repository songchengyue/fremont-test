/*
 * XML Type:  LongCondition
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.LongCondition
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML LongCondition(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class LongConditionImpl extends com.hp.schemas.ucmdb._1.types.impl.ConditionImpl implements com.hp.schemas.ucmdb._1.types.LongCondition
{
    
    public LongConditionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONDITION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "condition");
    private static final javax.xml.namespace.QName LONGOPERATOR$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "longOperator");
    
    
    /**
     * Gets the "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.LongProp getCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "condition" element
     */
    public void setCondition(com.hp.schemas.ucmdb._1.types.LongProp condition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().find_element_user(CONDITION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().add_element_user(CONDITION$0);
            }
            target.set(condition);
        }
    }
    
    /**
     * Appends and returns a new empty "condition" element
     */
    public com.hp.schemas.ucmdb._1.types.LongProp addNewCondition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongProp)get_store().add_element_user(CONDITION$0);
            return target;
        }
    }
    
    /**
     * Gets the "longOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator.Enum getLongOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LONGOPERATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "longOperator" element
     */
    public com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator xgetLongOperator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator)get_store().find_element_user(LONGOPERATOR$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "longOperator" element
     */
    public void setLongOperator(com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator.Enum longOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LONGOPERATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LONGOPERATOR$2);
            }
            target.setEnumValue(longOperator);
        }
    }
    
    /**
     * Sets (as xml) the "longOperator" element
     */
    public void xsetLongOperator(com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator longOperator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator target = null;
            target = (com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator)get_store().find_element_user(LONGOPERATOR$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator)get_store().add_element_user(LONGOPERATOR$2);
            }
            target.set(longOperator);
        }
    }
    /**
     * An XML longOperator(@http://schemas.hp.com/ucmdb/1/types).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.LongCondition$LongOperator.
     */
    public static class LongOperatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.hp.schemas.ucmdb._1.types.LongCondition.LongOperator
    {
        
        public LongOperatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected LongOperatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
