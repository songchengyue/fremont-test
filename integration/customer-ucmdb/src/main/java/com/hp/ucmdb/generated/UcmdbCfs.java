package com.hp.ucmdb.generated;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.hp.schemas.ucmdb._1.types.Conditions;
import com.hp.schemas.ucmdb._1.types.StrCondition;
import com.hp.schemas.ucmdb._1.types.StrCondition.StrOperator;
import com.hp.schemas.ucmdb._1.types.StrConditions;
import com.hp.schemas.ucmdb._1.types.StrProp;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByType.ConditionsLogicalOperator.Enum;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeDocument;
import com.hp.schemas.ucmdb._1.params.query.GetFilteredCIsByTypeResponseDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunks;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksDocument;
import com.hp.schemas.ucmdb._1.params.query.PullTopologyMapChunksResponseDocument;
import com.hp.schemas.ucmdb._1.types.props.CustomProperties;
import com.hp.schemas.ucmdb._1.types.props.CustomTypedProperties;
import com.hp.schemas.ucmdb._1.types.props.PropertiesList;
import com.hp.schemas.ucmdb._1.types.props.TypedProperties;
import com.hp.schemas.ucmdb._1.types.props.TypedPropertiesCollection;
import com.hp.schemas.ucmdb._1.types.*;

public class UcmdbCfs {
	static int numberOfChunks;
	static int chunkNumber;

	static String key1;
	static String key2;
	


	static String className1 ="customer_facing_service";
	static String propertyName1 = "cfs_id";
	static String propertyName2 = "telstra_service_id";
	static String propertyName3 = "as_number";
	static String conditionName1 = "product_name";
	static List<String> conditionValues1 = new ArrayList<String>();

	static String callerApplication = "netops";
	static String cndtnLgclOprtrOr = "OR";
	static String strOprtr = "EqualIgnoreCase";


	static List<String> cmdbIds = new ArrayList<String>();
	static List<String> cfsIds = new ArrayList<String>();
	static List<String> telstraServiceIds = new ArrayList<String>();
	static List<String> asNumbers = new ArrayList<String>();
	static List<Timestamp> fullLastUpdatedDate = new ArrayList<Timestamp>();
	
	static String cmdbId = "";
	static String cfsId = "";
	static String telstraServiceId = "";
	static String asNumber = "";
	static String getName;
	static String getValue;
	static String configPath;

	private static final Logger LOGGER = LoggerFactory.getLogger(UcmdbNetopsSynch.class);
	static Properties props = new Properties();
	static Connection dbConnection;
	static Statement statement;
	static UcmdbServiceStub stub = null;
	
public static void getCfsIds(UcmdbServiceStub ucmdbStub,String mainConfigPath) {
		LOGGER.info("WITHIN CFS");
		stub = ucmdbStub;
		configPath = mainConfigPath;
		
		conditionValues1.add("Global Internet Direct  ");
    	conditionValues1.add("Global Internet Direct Economy  ");
    	conditionValues1.add("IP Transit Premium ");
    	conditionValues1.add("IP Transit Standard ");
		
    	try {
    		loadParams(configPath);   
			//loadParams();
        	//Deleting previous Entries
        	dbConnection = getDBConnection();
			Statement delstmt = dbConnection.createStatement();
			delstmt.executeQuery("delete from CFS_STG");
			LOGGER.info("deleted");

        	getFilteredCIsByTypeDemo();
        	insertIntoTable();
		} catch (Exception e) {
			LOGGER.error("exception inside main", e);
		}
    	
	}
	
	/* getFilteredCIsByType Operation*/
	public static void getFilteredCIsByTypeDemo() {
		LOGGER.info("inside getFilteredCIsByTypeDemo method");

		try {
			Enum conditionsLogicalOperator = Enum.forString(cndtnLgclOprtrOr);

			StrCondition[] strConditionArray = new StrCondition[conditionValues1.size()];
			for(int j=0; j<conditionValues1.size();j++)
			{
				StrCondition strCondition =  StrCondition.Factory.newInstance();
				StrProp strProp = StrProp.Factory.newInstance();
				strProp.setName(conditionName1);
				strProp.setValue(conditionValues1.get(j));
				strCondition.setCondition(strProp);
				strCondition.setStrOperator(StrOperator.Enum.forString(strOprtr));
				strConditionArray[j]= strCondition;
				
			}
	
			Conditions conditions = Conditions.Factory.newInstance();
			StrConditions strConditions= StrConditions.Factory.newInstance();
			
			
			strConditions.setStrConditionArray(strConditionArray);
			conditions.setStrConditions(strConditions);
			
			CmdbContext cntxt = CmdbContext.Factory.newInstance();
			cntxt.setCallerApplication(callerApplication);
			
			GetFilteredCIsByType getFilteredCIsByType = GetFilteredCIsByType.Factory.newInstance();
			getFilteredCIsByType.setCmdbContext(cntxt);
			getFilteredCIsByType.setType(className1);
			getFilteredCIsByType.setConditions(conditions);
			getFilteredCIsByType.setConditionsLogicalOperator(conditionsLogicalOperator);
			

			CustomProperties properties = CustomProperties.Factory.newInstance();
			PropertiesList propertiesList= PropertiesList.Factory.newInstance();
			propertiesList.addPropertyName(propertyName1);
			propertiesList.addPropertyName(propertyName2);
			propertiesList.addPropertyName(propertyName3);
			properties.setPropertiesList(propertiesList);
			getFilteredCIsByType.setProperties(properties);

			GetFilteredCIsByTypeDocument request = GetFilteredCIsByTypeDocument.Factory.newInstance();
			request.setGetFilteredCIsByType(getFilteredCIsByType);
			
			//LOGGER.info("getFilteredCIsByType request : " + request);					
			////LOGGER.info("set inputs");
			
			HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
			basicAuthentication.setUsername("admin");
			basicAuthentication.setPassword("admin");
			int so_timeout = 480000;
			int con_timeout = 60000;
			stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
			////LOGGER.info("authentication passed");

			GetFilteredCIsByTypeResponseDocument response = stub.getFilteredCIsByType(request);
			//LOGGER.info("getFilteredCIsByType response : " + response);

			
			ChunkInfo chunkInfo = response.getGetFilteredCIsByTypeResponse().getChunkInfo();
			numberOfChunks = chunkInfo.getNumberOfChunks();
			ChunkKey chunkKey = chunkInfo.getChunksKey();
			key1 = chunkKey.getKey1();
			key2 = chunkKey.getKey2();
			
			LOGGER.info("numberOfChunks : " + numberOfChunks);
			/*LOGGER.info("key1 : " + key1);
			LOGGER.info("key2 : " + key2);*/
			if(numberOfChunks==0)
			{
				
				CI[] CI = response.getGetFilteredCIsByTypeResponse().getCIs().getCIArray();
				////LOGGER.info("Within zero chunks :CI.length : " +CI.length);
				java.util.Date date= new java.util.Date();
				for(int j=0 ; j < CI.length ; j++)
				{
					cmdbId = CI[j].getID().getStringValue();
					cmdbIds.add(cmdbId);
					fullLastUpdatedDate.add(new Timestamp(date.getTime()));
					StrProp[] strprop = CI[j].getProps().getStrProps().getStrPropArray();
					LOGGER.info("strprop.length : " + strprop.length);
					if(strprop.length==1)
					{
						for (int p = 0; p < strprop.length; p++)
						{
							getName = strprop[p].getName();
							getValue = strprop[p].getValue();
							if(getName.equalsIgnoreCase("cfs_id")){
								cfsId = getValue;
								////LOGGER.info("cfsId : " + cfsId);
								cfsIds.add(cfsId);
							}
						}
						telstraServiceIds.add("");
						asNumbers.add("");
					}
					else if(strprop.length==2)
					{
						for (int p = 0; p < strprop.length; p++)
						{
							getName = strprop[p].getName();
							getValue = strprop[p].getValue();
							if(getName.equalsIgnoreCase("cfs_id")){
								cfsId = getValue;
								////LOGGER.info("cfsId : " + cfsId);
								cfsIds.add(cfsId);
							}
							if(getName.equalsIgnoreCase("telstra_service_id")){
								telstraServiceId = getValue;
								telstraServiceIds.add(telstraServiceId);
								asNumbers.add("");	
							}
							else if(getName.equalsIgnoreCase("as_number")){
								asNumber = getValue;
								//LOGGER.info("asNumber : " + asNumber);
								asNumbers.add(asNumber);
								telstraServiceIds.add("");
							}

						}					
					}
					else if(strprop.length==3)
					{
						for (int p = 0; p < strprop.length; p++)
						{
							getName = strprop[p].getName();
							getValue = strprop[p].getValue();
							if(getName.equalsIgnoreCase("cfs_id")){
								cfsId = getValue;
								////LOGGER.info("cfsId : " + cfsId);
								cfsIds.add(cfsId);
							}
							if(getName.equalsIgnoreCase("telstra_service_id")){
								telstraServiceId = getValue;
								telstraServiceIds.add(telstraServiceId);
							}
							if(getName.equalsIgnoreCase("as_number")){
								asNumber = getValue;
								//LOGGER.info("asNumber : " + asNumber);
								asNumbers.add(asNumber);
							}
						}
					}

				}
				LOGGER.info("cmdbIdSize : "+cmdbIds.size());
				LOGGER.info("cfsIdSize : "+cfsIds.size());
				LOGGER.info("telstraServiceIdsSize : "+telstraServiceIds.size());
				LOGGER.info("asNumbersSize : "+asNumbers.size());
				LOGGER.info("ZeroChunk Loop");
			}

			else
			{
			pullTopologyMapChunksFull();
			}
		

		}	catch (RemoteException rme) {
			LOGGER.error("RemoteException inside getFilteredCIsByTypeDemo", rme);
		} catch (UcmdbFault ufe) {
			LOGGER.error("UcmdbFaultException inside getFilteredCIsByTypeDemo", ufe);
		} catch (Exception e) {
			 LOGGER.error("exception inside getFilteredCIsByTypeDemo", e);
		}

	}

	 // pullTopologyMapChunksFull Operation
	public static void pullTopologyMapChunksFull() {
		LOGGER.info("inside pullTopologyMapChunksFull method");

		try {
			
			////LOGGER.info("numberOfChunks : " + numberOfChunks);
			////LOGGER.info("key1 : " + key1);
			////LOGGER.info("key2 : " + key2);
			for(int i=1 ; i <= numberOfChunks ; i++)
			{
				chunkNumber = i;
				////LOGGER.info("chunkNumber:" +chunkNumber);
				
				CmdbContext cntxt = CmdbContext.Factory.newInstance();
				cntxt.setCallerApplication(callerApplication);
				ChunkRequest chunkRequest = ChunkRequest.Factory.newInstance();
				ChunkInfo chunkInfo = ChunkInfo.Factory.newInstance();
				
				chunkRequest.setChunkNumber(chunkNumber);
				chunkInfo.setNumberOfChunks(numberOfChunks);

				ChunkKey chunksKey = ChunkKey.Factory.newInstance();
				chunksKey.setKey1(key1);
				chunksKey.setKey2(key2);
				
				chunkInfo.setChunksKey(chunksKey);
				chunkRequest.setChunkInfo(chunkInfo );
				
				
				TypedPropertiesCollection queryTypedProperties = TypedPropertiesCollection.Factory.newInstance();
				TypedProperties typedProperties = TypedProperties.Factory.newInstance();
				typedProperties.setType(className1);
				CustomTypedProperties properties = CustomTypedProperties.Factory.newInstance();
				PropertiesList propertiesList = PropertiesList.Factory.newInstance();
				String[] propertyNameArray = {propertyName1,propertyName2,propertyName3};
				propertiesList.setPropertyNameArray(propertyNameArray );

				properties.setPropertiesList(propertiesList);
				typedProperties.setProperties(properties);
				
				TypedProperties[] typedPropertiesArray ={typedProperties};
				queryTypedProperties.setTypedPropertiesArray(typedPropertiesArray );
				
				PullTopologyMapChunks pullTopologyMapChunks = PullTopologyMapChunks.Factory.newInstance();
				
				pullTopologyMapChunks.setCmdbContext(cntxt);
				pullTopologyMapChunks.setChunkRequest(chunkRequest);
				pullTopologyMapChunks.setQueryTypedProperties(queryTypedProperties);
				PullTopologyMapChunksDocument request = PullTopologyMapChunksDocument.Factory.newInstance();
				request.setPullTopologyMapChunks(pullTopologyMapChunks);
				////LOGGER.info("pullTopologyMapChunks request" +request);					
				
				HttpTransportProperties.Authenticator basicAuthentication = new HttpTransportProperties.Authenticator();
				basicAuthentication.setUsername("admin");
				basicAuthentication.setPassword("admin");
				int so_timeout = 480000;
				int con_timeout = 60000;
				stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
				stub._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, basicAuthentication);
				stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, so_timeout);
				stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, con_timeout);
				////LOGGER.info("authentication passed");

				PullTopologyMapChunksResponseDocument response = stub.pullTopologyMapChunks(request);
				////LOGGER.info("pullTopologyMapChunks response : " + response);
				
				
				CINode[] ciNode = response.getPullTopologyMapChunksResponse().getTopologyMap().getCINodes().getCINodeArray();
				
				CI[] ci = ciNode[0].getCIs().getCIArray();
				////LOGGER.info("ci : " + ci);
				////LOGGER.info(ci.length);
				java.util.Date date= new java.util.Date();
				for(int j=0 ; j < ci.length ; j++)
				{						
					cmdbId = ci[j].getID().getStringValue();	
					
					cmdbIds.add(cmdbId);
					fullLastUpdatedDate.add(new Timestamp(date.getTime()));

						StrProp[] strprop = ci[j].getProps().getStrProps().getStrPropArray();
						LOGGER.info("strprop.length : " + strprop.length);
				
						if(strprop.length==1)
						{
							for (int p = 0; p < strprop.length; p++)
							{
								getName = strprop[p].getName();
								getValue = strprop[p].getValue();
								if(getName.equalsIgnoreCase("cfs_id")){
									cfsId = getValue;
									////LOGGER.info("cfsId : " + cfsId);
									cfsIds.add(cfsId);
								}
							}
							telstraServiceIds.add("");
							asNumbers.add("");
						}
						else if(strprop.length==2)
						{
							for (int p = 0; p < strprop.length; p++)
							{
								getName = strprop[p].getName();
								getValue = strprop[p].getValue();
								if(getName.equalsIgnoreCase("cfs_id")){
									cfsId = getValue;
									////LOGGER.info("cfsId : " + cfsId);
									cfsIds.add(cfsId);
								}
								if(getName.equalsIgnoreCase("telstra_service_id")){
									telstraServiceId = getValue;
									telstraServiceIds.add(telstraServiceId);
									asNumbers.add("");	
								}
								else if(getName.equalsIgnoreCase("as_number")){
									asNumber = getValue;
									//LOGGER.info("asNumber : " + asNumber);
									asNumbers.add(asNumber);
									telstraServiceIds.add("");
								}

							}					
						}
						else if(strprop.length==3)
						{
							for (int p = 0; p < strprop.length; p++)
							{
								getName = strprop[p].getName();
								getValue = strprop[p].getValue();
								if(getName.equalsIgnoreCase("cfs_id")){
									cfsId = getValue;
									////LOGGER.info("cfsId : " + cfsId);
									cfsIds.add(cfsId);
								}
								if(getName.equalsIgnoreCase("telstra_service_id")){
									telstraServiceId = getValue;
									telstraServiceIds.add(telstraServiceId);
								}
								if(getName.equalsIgnoreCase("as_number")){
									asNumber = getValue;
									//LOGGER.info("asNumber : " + asNumber);
									asNumbers.add(asNumber);
								}
							}
						}
					
				}


			}
		LOGGER.info("OUTSIDE CHUNK LOOP");
		LOGGER.info("cmdbIdSize : "+cmdbIds.size());
		LOGGER.info("cfsIdSize : "+cfsIds.size());
		LOGGER.info("telstraServiceIdsSize : "+telstraServiceIds.size());
		LOGGER.info("asNumbersSize : "+asNumbers.size());
		
		
		} 
		catch (RemoteException rme) {
			LOGGER.error("RemoteException inside pullTopologyMapChunksDemo", rme);
		} catch (UcmdbFault ufe) {
			LOGGER.error("UcmdbFaultException inside pullTopologyMapChunksDemo", ufe);
		}catch (Exception e) {
			 LOGGER.error("exception inside pullTopologyMapChunksFull", e);
		}

	}

	private static void insertIntoTable() {
		//Inserting Values into CFS_STG TABLE
		LOGGER.info("inserting");
		LOGGER.info("cmdbIdSize : "+cmdbIds.size());
		LOGGER.info("telstraServiceIdsSize : "+telstraServiceIds.size());
		LOGGER.info("asNumbersSize : "+asNumbers.size());
		LOGGER.info("cfsIdSize : "+cfsIds.size());
				try {
					dbConnection = getDBConnection();
					String stringSQL = "INSERT INTO CFS_STG (CMDB_ID,A_TELSTRA_SERVICE_ID,A_AS_NUMBER,A_CFS_ID,TIMESTAMP) VALUES (?,?,?,?,?)" ;
					PreparedStatement ps = dbConnection.prepareStatement(stringSQL);
					
					for (int k = 0; k < cmdbIds.size(); k++){
						ps.setString(1, cmdbIds.get(k));
						ps.setString(2, telstraServiceIds.get(k));
						ps.setString(3, asNumbers.get(k));
						ps.setString(4, cfsIds.get(k));
						ps.setTimestamp(5, fullLastUpdatedDate.get(k));									
						ps.addBatch();
					}		
					////LOGGER.info("completed batch execution");
					ps.executeBatch();
					ps.close();
					dbConnection.close();
					////LOGGER.info("connection closed");
				} 
		catch (SQLException sqle) {
			LOGGER.info(sqle.getMessage());
			LOGGER.error("SQL exception inside insertIntoTable", sqle);

		}
		catch (Exception e) {
			LOGGER.error("exception inside insertIntoTable", e);
		}	
		
	}
	
	//public static void loadParams() {
	public static void loadParams(String configPath) {
	    InputStream is = null;
	 
	    try {
	    	LOGGER.info("configPath : " +configPath);
	        File f = new File (configPath);
	    	// File f = new File("E:\\Ashwini\\workspaceAhana\\ucmdbSrv\\src\\config.txt");
	        is = new FileInputStream( f );
	    }
	    catch ( Exception e ) { is = null; }
	 
	    try {
	        props.load( is );
	    }
	    catch ( Exception e ) {
	    	LOGGER.error("exception inside loadParams", e);
	    }

	   
	}
	
	
	private static Connection getDBConnection() {
		//Using config file to remove hardcoding
		String DB_DRIVER = props.getProperty("DB_DRIVER");
		String DB_CONNECTION = props.getProperty("DB_CONNECTION");
		String DB_USER = props.getProperty("DB_USER");
		String DB_PASSWORD = props.getProperty("DB_PASSWORD");
		
		try {
			
			Class.forName(DB_DRIVER);
			
		} catch (ClassNotFoundException e) {
			LOGGER.info(e.getMessage());
		}
		
		
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
			return dbConnection;
		
		
		} catch (SQLException e) {
			LOGGER.info(e.getMessage());
		}
		return dbConnection;
	}
}
