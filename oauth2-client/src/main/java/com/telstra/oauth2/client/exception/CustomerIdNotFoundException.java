package com.telstra.oauth2.client.exception;

public class CustomerIdNotFoundException extends RuntimeException {
	
	public CustomerIdNotFoundException(String message) {
		super(message);
	}

}
