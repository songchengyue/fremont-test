package com.telstra.netops.dns.server.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.telstra.netops.dns.server.model.ClientDnsRemoveRequest;
import com.telstra.netops.dns.server.model.ClientDnsResolverRequest;
import com.telstra.netops.dns.server.model.ClientPrimaryDnsRequest;
import com.telstra.netops.dns.server.model.ClientReverseDnsRequest;
import com.telstra.netops.dns.server.model.ClientSecondaryDnsRequest;
import com.telstra.netops.dns.server.model.DnsPrimaryRecord;
import com.telstra.netops.dns.server.model.DnsResolverRecord;
import com.telstra.netops.dns.server.model.DnsReverseRecord;
import com.telstra.netops.dns.server.model.SecondaryDnsRecord;
import com.telstra.netops.dns.server.model.Success;
import com.telstra.netops.dns.server.model.cgi.DnsResultWrapper;
import com.telstra.netops.dns.server.repository.ServiceIdAccessRepository;
import com.telstra.netops.dns.server.service.PrimaryDnsService;
import com.telstra.netops.dns.server.service.ResolverService;
import com.telstra.netops.dns.server.service.ReverseDnsService;
import com.telstra.netops.dns.server.service.SecondaryDnsService;
import com.telstra.oauth2.client.CustomerIdResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
public class DnsController {

	// private Logger logger;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DnsController.class.getName());

	@Autowired
	HttpServletRequest request;

	@Autowired
	private ResolverService resolverService;

	@Autowired
	private PrimaryDnsService primaryDnsService;

	@Autowired
	private SecondaryDnsService secondaryDnsService;

	@Autowired
	private ReverseDnsService reverseDnsService;
	
	@Autowired
	private CustomerIdResolver customerIdResolver;
	
	@Autowired
	private ServiceIdAccessRepository serviceIdAccessRepository;

	//
	// DNS Resolver
	//

	@RequestMapping(value = "/resolver", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Success> resolverAdd(
			@RequestBody ClientDnsResolverRequest request, Principal principal, UriComponentsBuilder uriBuilder) {

		LOGGER.info("in dnsResolverSubmitDetails controller");
		String serviceID = request.getServiceId();
		LOGGER.debug("dnsResolverSubmitDetails IDIDIDIDDI ------->>" +serviceID);
		request.setCustomerId(customerIdResolver.resolve(principal));
		LOGGER.info("in dnsResolverSubmitDetails controller" +customerIdResolver.resolve(principal));
		Success success = resolverService.add(request);
		return createSuccessResponse(success, "/resolver", uriBuilder);
	}

	@RequestMapping(value = "/resolver", method = RequestMethod.GET)
	public @ResponseBody List<DnsResolverRecord> resolverList(
			Principal principal) {

		LOGGER.info("in dnsResolverListDetails controller");

		return  resolverService.list(customerIdResolver.resolve(principal));
	}

	@RequestMapping(value = "/resolvercancel", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Success> resolverDelete(
			@RequestBody ClientDnsResolverRequest request, Principal principal) {

		LOGGER.info("in dnsResolverCancelDetails controller");
		request.setCustomerId(customerIdResolver.resolve(principal));
		Success success = resolverService.remove(request);

		return new ResponseEntity<Success>(success, HttpStatus.OK);
	}

	//
	// Primary DNS
	//

	@RequestMapping(value = "/primary", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Success> primaryAdd(
			@RequestBody ClientPrimaryDnsRequest request,
			Principal principal,
			UriComponentsBuilder uriBuilder) {
		LOGGER.debug("in Primary DNS Controller add---->>" +request.getServiceId());
		request.setCustomerId(customerIdResolver.resolve(principal));
		Success success  = primaryDnsService.add(request);
		return createSuccessResponse(success, "/primary", uriBuilder);
	}

	@RequestMapping(value = "/primary", method = RequestMethod.GET)
	public @ResponseBody List<DnsPrimaryRecord> primaryList(Principal principal) {
        String customerId = customerIdResolver.resolve(principal);
		LOGGER.debug("in Primary DNS viewprimarydns record list");
		LOGGER.debug("resolverCustomerId: " + customerId);
		return primaryDnsService.list(customerId);
	}

	@RequestMapping(value = "/primary", method = RequestMethod.DELETE)
	public @ResponseBody Success  primaryDelete(
			@RequestParam ("ids")  String ids, Principal principal) {
		LOGGER.debug("in Primary deleteprimarydns remove");
		String customerId = customerIdResolver.resolve(principal);
		LOGGER.debug("in Primary deleteprimarydns customerId "+customerId+"and row id "+ids);
		Success success = primaryDnsService.remove(ClientDnsRemoveRequest.create(customerId, ids,null));
		return success;
	}

	//
	// Secondary DNS SecondaryDnsRecord
	//

	@RequestMapping(value = "/secondary", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Success> secondaryAdd(
			@RequestBody ClientSecondaryDnsRequest request, Principal principal, UriComponentsBuilder uriBuilder) {
		String serviceID = request.getServiceId();
		LOGGER.debug("dnsSubmitSecondartSubmitDetails IDIDIDIDDI ------->>" +serviceID);
		request.setCustomerId(customerIdResolver.resolve(principal));
		Success success = secondaryDnsService.add(request);
		return createSuccessResponse(success, "/secondary", uriBuilder);
	}

	@RequestMapping(value = "/secondary", method = RequestMethod.GET)
	public @ResponseBody List<SecondaryDnsRecord> secondaryList(Principal principal) {

		LOGGER.debug("In View Controller");
		return secondaryDnsService.list(customerIdResolver.resolve(principal));
		 
	}

	@RequestMapping(value = "/secondary", method = RequestMethod.DELETE)
	public @ResponseBody Success secondaryDelete(
			@RequestParam ("ids")  String rowIDtoRemove,@RequestParam ("domain")  String domain, Principal principal) {

		LOGGER.debug("In Cancel Controller SecondaryDNSRecordListCancel with row ID >>"+rowIDtoRemove);
		String customerId = customerIdResolver.resolve(principal);
		//request.setCustomerId(customerIdResolver.resolve(principal));
		Success success = secondaryDnsService.remove(ClientDnsRemoveRequest.create(customerId, rowIDtoRemove,domain));
		return success;
	}

	//
	// Reverse DNS
	//

	@RequestMapping(value = "/reversedelegation", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Success> reverseDelegationAdd(
			@RequestBody ClientReverseDnsRequest request, Principal principal, UriComponentsBuilder uriBuilder) {
		LOGGER.debug("reverse delegation add " + request);
		String serviceID = request.getServiceId();
		LOGGER.debug("dnsreverseDelegationSubmitDetails IDIDIDIDDI ------->>" +serviceID);
		request.setCustomerId(customerIdResolver.resolve(principal));
		Success success = reverseDnsService.addDelegation(request);
		return createSuccessResponse(success, "/reversedelegation", uriBuilder);
	}

	@RequestMapping(value = "/reversedelegationCancel", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Success> reverseDelegationRemove(
			@RequestBody ClientReverseDnsRequest request, Principal principal) {
		LOGGER.debug("reverse delegation remove " + request);
		request.setCustomerId(customerIdResolver.resolve(principal));
		Success success = reverseDnsService.removeDelegation(request);
		return new ResponseEntity<Success>(success, HttpStatus.OK);
	}
	
	 @RequestMapping(value = "/reversemapping", method=RequestMethod.POST)
	 public @ResponseBody ResponseEntity<DnsResultWrapper> reverseMappingAdd(@RequestBody
	  ClientReverseDnsRequest request, Principal principal){
		 LOGGER.debug("reverse mapping add");
			String serviceID = request.getServiceId();
			LOGGER.debug("dnsreversemappingSubmitDetails IDIDIDIDDI ------->>" +serviceID);
		 request.setCustomerId(customerIdResolver.resolve(principal));
		 DnsResultWrapper result = reverseDnsService.addMapping(request); 
		 return new ResponseEntity<DnsResultWrapper>(result, HttpStatus.CREATED);  
	 }
	  
	 
	 @RequestMapping(value = "/reversemappingCancel", method=RequestMethod.POST)
	 public @ResponseBody DnsResultWrapper reverseMappingRemove(@RequestBody
	  ClientReverseDnsRequest request, Principal principal){
		 LOGGER.debug("reverse mapping remove");
		 request.setCustomerId(customerIdResolver.resolve(principal));
		 DnsResultWrapper result = reverseDnsService.removeMapping(request); 
		 return result;	  
	 }
	 
	  @RequestMapping(value = "/reversedns", method=RequestMethod.GET)
	  public @ResponseBody List<DnsReverseRecord> reverseDnsList(Principal principal){
		  LOGGER.debug("reverse dns list"+request);
		  return reverseDnsService.list(customerIdResolver.resolve(principal));  
	  }
	  
	  
	  //
	  //Parent Site id retrieval
	  //
	  @RequestMapping(value = "/service", method=RequestMethod.GET)
      public @ResponseBody List<String> getServiceidDetails(@RequestParam ("productLine") String productLine, Principal principal){
		  LOGGER.debug("Resolver getServiceidDetails dns list " + productLine );
		  return serviceIdAccessRepository.findDistinctServiceIdByProductLineAndCustomerId(productLine, customerIdResolver.resolve(principal));            
      }
	  	  
	  private ResponseEntity<Success> createSuccessResponse(Success success, String path, UriComponentsBuilder uriBuilder) {
		  HttpHeaders headers = new HttpHeaders();
		  if (success.getId() != null) {
			  UriComponents uriComponents = 
						uriBuilder.path(path + "/{id}").buildAndExpand(success.getId());
			  headers.setLocation(uriComponents.toUri());
		  }

		  return new ResponseEntity<Success>(success, headers, HttpStatus.CREATED);
	  }
	 
}