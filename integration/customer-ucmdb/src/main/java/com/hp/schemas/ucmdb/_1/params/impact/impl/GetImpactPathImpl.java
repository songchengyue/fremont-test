/*
 * XML Type:  getImpactPath
 * Namespace: http://schemas.hp.com/ucmdb/1/params/impact
 * Java type: com.hp.schemas.ucmdb._1.params.impact.GetImpactPath
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.impact.impl;
/**
 * An XML getImpactPath(@http://schemas.hp.com/ucmdb/1/params/impact).
 *
 * This is a complex type.
 */
public class GetImpactPathImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.impact.GetImpactPath
{
    
    public GetImpactPathImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RELATION$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "relation");
    private static final javax.xml.namespace.QName IDENTIFIER$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "identifier");
    private static final javax.xml.namespace.QName CMDBCONTEXT$4 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/impact", "cmdbContext");
    
    
    /**
     * Gets the "relation" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowRelation getRelation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowRelation)get_store().find_element_user(RELATION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "relation" element
     */
    public void setRelation(com.hp.schemas.ucmdb._1.types.ShallowRelation relation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowRelation)get_store().find_element_user(RELATION$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.ShallowRelation)get_store().add_element_user(RELATION$0);
            }
            target.set(relation);
        }
    }
    
    /**
     * Appends and returns a new empty "relation" element
     */
    public com.hp.schemas.ucmdb._1.types.ShallowRelation addNewRelation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.ShallowRelation target = null;
            target = (com.hp.schemas.ucmdb._1.types.ShallowRelation)get_store().add_element_user(RELATION$0);
            return target;
        }
    }
    
    /**
     * Gets the "identifier" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.Identifier getIdentifier()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.Identifier target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.Identifier)get_store().find_element_user(IDENTIFIER$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "identifier" element
     */
    public void setIdentifier(com.hp.schemas.ucmdb._1.types.impact.Identifier identifier)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.Identifier target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.Identifier)get_store().find_element_user(IDENTIFIER$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.impact.Identifier)get_store().add_element_user(IDENTIFIER$2);
            }
            target.set(identifier);
        }
    }
    
    /**
     * Appends and returns a new empty "identifier" element
     */
    public com.hp.schemas.ucmdb._1.types.impact.Identifier addNewIdentifier()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.impact.Identifier target = null;
            target = (com.hp.schemas.ucmdb._1.types.impact.Identifier)get_store().add_element_user(IDENTIFIER$2);
            return target;
        }
    }
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$4, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$4);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$4);
            return target;
        }
    }
}
