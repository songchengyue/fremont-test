/*
 * An XML document type.
 * Localname: getClassAncestors
 * Namespace: http://schemas.hp.com/ucmdb/1/params/classmodel
 * Java type: com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.classmodel.impl;
/**
 * A document containing one getClassAncestors(@http://schemas.hp.com/ucmdb/1/params/classmodel) element.
 *
 * This is a complex type.
 */
public class GetClassAncestorsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestorsDocument
{
    
    public GetClassAncestorsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETCLASSANCESTORS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getClassAncestors");
    
    
    /**
     * Gets the "getClassAncestors" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestors getGetClassAncestors()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestors target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestors)get_store().find_element_user(GETCLASSANCESTORS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getClassAncestors" element
     */
    public void setGetClassAncestors(com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestors getClassAncestors)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestors target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestors)get_store().find_element_user(GETCLASSANCESTORS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestors)get_store().add_element_user(GETCLASSANCESTORS$0);
            }
            target.set(getClassAncestors);
        }
    }
    
    /**
     * Appends and returns a new empty "getClassAncestors" element
     */
    public com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestors addNewGetClassAncestors()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestors target = null;
            target = (com.hp.schemas.ucmdb._1.params.classmodel.GetClassAncestors)get_store().add_element_user(GETCLASSANCESTORS$0);
            return target;
        }
    }
}
