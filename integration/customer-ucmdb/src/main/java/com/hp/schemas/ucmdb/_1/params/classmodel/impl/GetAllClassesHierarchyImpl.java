/*
 * XML Type:  getAllClassesHierarchy
 * Namespace: http://schemas.hp.com/ucmdb/1/params/classmodel
 * Java type: com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.classmodel.impl;
/**
 * An XML getAllClassesHierarchy(@http://schemas.hp.com/ucmdb/1/params/classmodel).
 *
 * This is a complex type.
 */
public class GetAllClassesHierarchyImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.classmodel.GetAllClassesHierarchy
{
    
    public GetAllClassesHierarchyImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBCONTEXT$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "cmdbContext");
    
    
    /**
     * Gets the "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "cmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "cmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$0);
            return target;
        }
    }
}
