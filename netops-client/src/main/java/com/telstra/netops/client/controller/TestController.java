package com.telstra.netops.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {
	
	@Value("${client.default.userdn}")
	private String userdn;

	@Value("${client.default.authorization}")
	private String authorization;
	
	@RequestMapping(value = "/test")
	public String test(ModelMap model) {	
		model.addAttribute("username", userdn);
		model.addAttribute("authorization", authorization);
		return "test";	
	}

}
