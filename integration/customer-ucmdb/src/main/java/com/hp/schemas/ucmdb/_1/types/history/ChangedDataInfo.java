/*
 * XML Type:  ChangedDataInfo
 * Namespace: http://schemas.hp.com/ucmdb/1/types/history
 * Java type: com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.history;


/**
 * An XML ChangedDataInfo(@http://schemas.hp.com/ucmdb/1/types/history).
 *
 * This is a complex type.
 */
public interface ChangedDataInfo extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ChangedDataInfo.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("changeddatainfofcebtype");
    
    /**
     * Gets the "changedIDs" element
     */
    com.hp.schemas.ucmdb._1.types.IDs getChangedIDs();
    
    /**
     * Sets the "changedIDs" element
     */
    void setChangedIDs(com.hp.schemas.ucmdb._1.types.IDs changedIDs);
    
    /**
     * Appends and returns a new empty "changedIDs" element
     */
    com.hp.schemas.ucmdb._1.types.IDs addNewChangedIDs();
    
    /**
     * Gets the "rootID" element
     */
    com.hp.schemas.ucmdb._1.types.ID getRootID();
    
    /**
     * Sets the "rootID" element
     */
    void setRootID(com.hp.schemas.ucmdb._1.types.ID rootID);
    
    /**
     * Appends and returns a new empty "rootID" element
     */
    com.hp.schemas.ucmdb._1.types.ID addNewRootID();
    
    /**
     * Gets the "changeType" element
     */
    com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType.Enum getChangeType();
    
    /**
     * Gets (as xml) the "changeType" element
     */
    com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType xgetChangeType();
    
    /**
     * Sets the "changeType" element
     */
    void setChangeType(com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType.Enum changeType);
    
    /**
     * Sets (as xml) the "changeType" element
     */
    void xsetChangeType(com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType changeType);
    
    /**
     * An XML changeType(@http://schemas.hp.com/ucmdb/1/types/history).
     *
     * This is an atomic type that is a restriction of com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo$ChangeType.
     */
    public interface ChangeType extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ChangeType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("changetypee811elemtype");
        
        org.apache.xmlbeans.StringEnumAbstractBase enumValue();
        void set(org.apache.xmlbeans.StringEnumAbstractBase e);
        
        static final Enum NO_CHANGE = Enum.forString("NO_CHANGE");
        static final Enum ROOT_CHANGED = Enum.forString("ROOT_CHANGED");
        static final Enum RELATED_OBJECT_CHANGED = Enum.forString("RELATED_OBJECT_CHANGED");
        static final Enum ROOT_AND_OBJECT_CHANGED = Enum.forString("ROOT_AND_OBJECT_CHANGED");
        
        static final int INT_NO_CHANGE = Enum.INT_NO_CHANGE;
        static final int INT_ROOT_CHANGED = Enum.INT_ROOT_CHANGED;
        static final int INT_RELATED_OBJECT_CHANGED = Enum.INT_RELATED_OBJECT_CHANGED;
        static final int INT_ROOT_AND_OBJECT_CHANGED = Enum.INT_ROOT_AND_OBJECT_CHANGED;
        
        /**
         * Enumeration value class for com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo$ChangeType.
         * These enum values can be used as follows:
         * <pre>
         * enum.toString(); // returns the string value of the enum
         * enum.intValue(); // returns an int value, useful for switches
         * // e.g., case Enum.INT_NO_CHANGE
         * Enum.forString(s); // returns the enum value for a string
         * Enum.forInt(i); // returns the enum value for an int
         * </pre>
         * Enumeration objects are immutable singleton objects that
         * can be compared using == object equality. They have no
         * public constructor. See the constants defined within this
         * class for all the valid values.
         */
        static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
        {
            /**
             * Returns the enum value for a string, or null if none.
             */
            public static Enum forString(java.lang.String s)
                { return (Enum)table.forString(s); }
            /**
             * Returns the enum value corresponding to an int, or null if none.
             */
            public static Enum forInt(int i)
                { return (Enum)table.forInt(i); }
            
            private Enum(java.lang.String s, int i)
                { super(s, i); }
            
            static final int INT_NO_CHANGE = 1;
            static final int INT_ROOT_CHANGED = 2;
            static final int INT_RELATED_OBJECT_CHANGED = 3;
            static final int INT_ROOT_AND_OBJECT_CHANGED = 4;
            
            public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
                new org.apache.xmlbeans.StringEnumAbstractBase.Table
            (
                new Enum[]
                {
                    new Enum("NO_CHANGE", INT_NO_CHANGE),
                    new Enum("ROOT_CHANGED", INT_ROOT_CHANGED),
                    new Enum("RELATED_OBJECT_CHANGED", INT_RELATED_OBJECT_CHANGED),
                    new Enum("ROOT_AND_OBJECT_CHANGED", INT_ROOT_AND_OBJECT_CHANGED),
                }
            );
            private static final long serialVersionUID = 1L;
            private java.lang.Object readResolve() { return forInt(intValue()); } 
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType newValue(java.lang.Object obj) {
              return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType) type.newValue( obj ); }
            
            public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType newInstance() {
              return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo.ChangeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo newInstance() {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.history.ChangedDataInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
