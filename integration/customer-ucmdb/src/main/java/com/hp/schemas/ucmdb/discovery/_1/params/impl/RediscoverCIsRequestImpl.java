/*
 * XML Type:  rediscoverCIsRequest
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML rediscoverCIsRequest(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class RediscoverCIsRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.RediscoverCIsRequest
{
    
    public RediscoverCIsRequestImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CMDBIDS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "CmdbIDs");
    private static final javax.xml.namespace.QName CMDBCONTEXT$2 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "CmdbContext");
    
    
    /**
     * Gets the "CmdbIDs" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList getCmdbIDs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(CMDBIDS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CmdbIDs" element
     */
    public void setCmdbIDs(com.hp.schemas.ucmdb._1.types.StrList cmdbIDs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(CMDBIDS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(CMDBIDS$0);
            }
            target.set(cmdbIDs);
        }
    }
    
    /**
     * Appends and returns a new empty "CmdbIDs" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList addNewCmdbIDs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(CMDBIDS$0);
            return target;
        }
    }
    
    /**
     * Gets the "CmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext getCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CmdbContext" element
     */
    public void setCmdbContext(com.hp.schemas.ucmdb._1.types.CmdbContext cmdbContext)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().find_element_user(CMDBCONTEXT$2, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$2);
            }
            target.set(cmdbContext);
        }
    }
    
    /**
     * Appends and returns a new empty "CmdbContext" element
     */
    public com.hp.schemas.ucmdb._1.types.CmdbContext addNewCmdbContext()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.CmdbContext target = null;
            target = (com.hp.schemas.ucmdb._1.types.CmdbContext)get_store().add_element_user(CMDBCONTEXT$2);
            return target;
        }
    }
}
