node {

    // see https://jenkins.io/solutions/pipeline/ for pipeline help!

    stage 'Build and Deploy'
    
    def version = "${currentBuild.number}"
    
    def issues = []

    def changeLogSets = currentBuild.rawBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            echo "${entry.commitId} by ${entry.author} on ${new Date(entry.timestamp)}: ${entry.msg}"

            def matcher = entry.msg =~ "((?<!([A-Z]{1,10})-?)[A-Z]+-\\d+)"
            while (matcher.find()) {
                def issueKey = entry.msg.substring(matcher.start(), matcher.end())
                echo "issueKey: ${issueKey}"
                issues.add(issueKey)
            }
        }
    }
    
    changeLogSets = null
    
    for (int i=0; i< issues.size(); i++) {
        jiraComment(issueKey: issues[i], body: "Starting build.")
    }

    //def apps = [ 'client', 'lookingglass', 'oauth2-server' ]
    
    def apps = [ 'client' ]

    def url  = 'https://devops-poc-openshift-master1-syd5.ose.pacific.net.au:8443'
    def devProject = 'netops-dev'
    def token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImplbmtpbnMtdG9rZW4tOTRyNjIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiamVua2lucyIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6Ijc4ZDQ0YmExLTVkMDgtMTFlNi1hYmNkLTAwNTA1NjhlNTYyZiIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmplbmtpbnMifQ.L9ihG8GzNQ20fSfZV9Wz4MrJx6zPCTARc7TrvIS9VD1XOGdVyELPM8slMpOe5saJAo5nATaVEKbrHRlb7r-I4jZuD77eTUfJo0RX8oRKcK2Gjhnm1YpyQNkLc7k8ceingM0BKTIngkE9eIY3bruRsgPT2i5PWShCRRbTFkFxVd7v8RIzfcHL30XCscyeiDDsOzww1Ti4fBcmHKt5NMTo45gX_KalBCa_9S41zvBJqkOyxk3vtu4AKqc5hYl8RaWLANwmPGP3-G1aZqd3a6Z2cSuD86we3VJIls-Bb9WRECb7f4B0pa_1Wo2s5Geh7zk7VVwGWzJAD-HA6DiWyz-K7w'

    // my attempt to do things in parallel, didn't work, build just hangs
    // def builds = [:]
    // for (app in apps) {
    //    builds[app] = {
    //        openshiftBuild(buildConfig: app, apiURL: url, namespace: devProject, authToken: token, verbose: true)
    //    }
    //}
    //
    //parallel builds

    for (app in apps) {
        openshiftBuild(buildConfig: app, apiURL: url, namespace: devProject, authToken: token, verbose: true)
    }

    for (int i=0; i< issues.size(); i++) {
        jiraComment(issueKey: issues[i], body: "Please test your changes and go to ${env.BUILD_URL} to mark as Ready for SIT.")
    }

    def body = "Please test your changes and go to ${env.BUILD_URL} to mark as Ready for SIT."
    def subject = "${env.JOB_NAME} - Build # ${version} - DEV DEPLOY COMPLETED"
    emailext body: body, mimeType: 'text/plain', replyTo: 'no-reply-jenkins@team.telstra.com', subject: subject, to: 'david.donn@pacnet.com'

    stage 'Promote - Ready for SIT'
    input 'Ready for SIT?'
    
    def sitProject = 'netops-sit'

    for (app in apps) {
        openshiftTag(buildConfig: app, apiURL: url, namespace: devProject, authToken: token, verbose: true, sourceStream: app, sourceTag: 'latest', destinationNamespace: 'netops-sit', destinationStream: app, destinationTag: 'readyforsit')
    }

    for (int i=0; i< issues.size(); i++) {
        jiraComment(issueKey: issues[i], body: "Please go to ${env.BUILD_URL} to deploy the changes to SIT for testing.")
    }

    body = "Please go to ${env.BUILD_URL} to deploy the changes to SIT for testing."
    subject = "${env.JOB_NAME} - Build # ${version} - READY FOR SIT"
    emailext body: body, mimeType: 'text/plain', replyTo: 'no-reply-jenkins@team.telstra.com', subject: subject, to: 'david.donn@pacnet.com'

    stage 'Deploy - SIT'
    input 'Deploy to SIT?'

    for (app in apps) {
        openshiftTag(buildConfig: app, apiURL: url, namespace: sitProject, authToken: token, verbose: true, sourceStream: app, sourceTag: 'readyforsit', destinationStream: app, destinationTag: 'latest')
    }

    for (int i=0; i< issues.size(); i++) {
        jiraComment(issueKey: issues[i], body: "Please test your changes and go to ${env.BUILD_URL} to mark for Release.")
    }

    body = "Please test your changes and go to ${env.BUILD_URL} to mark for Release."
    subject = "${env.JOB_NAME} - Build # ${version} - SIT DEPLOY COMPLETED"
    emailext body: body, mimeType: 'text/plain', replyTo: 'no-reply-jenkins@team.telstra.com', subject: subject, to: 'david.donn@pacnet.com'

    stage 'Promote - Ready for Production'
    input 'Ready for Production?'
    
    def prodProject = 'netops-prod'
    
    for (app in apps) {
        openshiftTag(buildConfig: app, apiURL: url, namespace: sitProject, authToken: token, verbose: true, sourceStream: app, sourceTag: 'latest', destinationNamespace: 'netops-prod', destinationStream: app, destinationTag: 'readyforprod')
    }

    for (int i=0; i< issues.size(); i++) {
        jiraComment(issueKey: issues[i], body: "Please go to ${env.BUILD_URL} to deploy the changes to Production.")
    }

    body = "Please go to ${env.BUILD_URL} to deploy the changes to Production."
    subject = "${env.JOB_NAME} - Build # ${version} - READY FOR PRODUCTION"
    emailext body: body, mimeType: 'text/plain', replyTo: 'no-reply-jenkins@team.telstra.com', subject: subject, to: 'david.donn@pacnet.com'

    stage 'Deploy - Production'
    input 'Deploy to Production?'

    for (app in apps) {
        openshiftTag(buildConfig: app, apiURL: url, namespace: prodProject, authToken: token, verbose: true, sourceStream: app, sourceTag: 'readyforprod', destinationStream: app, destinationTag: 'latest')
    }

    for (int i=0; i< issues.size(); i++) {
        jiraComment(issueKey: issues[i], body: "Please check your changes in Production.")
    }

    body = "Please check your changes in Production."
    subject = "${env.JOB_NAME} - Build # ${version} - PRODUCTION DEPLOY COMPLETED"
    emailext body: body, mimeType: 'text/plain', replyTo: 'no-reply-jenkins@team.telstra.com', subject: subject, to: 'david.donn@pacnet.com'

}
