package com.telstra.oauth2.client;

import org.springframework.context.annotation.Bean;

/**
 * Any config specific to Telstra's application infrastructure.
 * 
 * @author d772392
 *
 */
public class TelstraConfig {
	
	@Bean
	public CustomerIdResolver customerIdResolver() {
		return new CustomerIdResolver();
	}
	
	@Bean
	public UserProfileResolver userProfileResolver() {
		return new UserProfileResolver();
	}
	
	@Bean
	public OAuth2TokenResolver tokenResolver() {
		return new OAuth2TokenResolver();
	}

}
