# Mock CGI

Scripts to mock various CGI scripts rather than do the actual work.

This project follows these [Development Guidelines](DevelopmentGuidelines.md)

## Installation

On Ubuntu:
```
apt install apache2
cd /etc/apache2/mods-enabled
sudo ln -s ../mods-available/cgi.load

apt install libjson-pp-perl

service apache2 reload

cp lg-mock.cgi /usr/lib/cgi-bin

```

## Configure Mocks


In the application-*.yml for your environment, set the appropriate CGI url in the appropriate project e.g.

```
lookingglass.cgi.url: http://10.200.26.206/cgi-bin/lg-mock.cgi
```
