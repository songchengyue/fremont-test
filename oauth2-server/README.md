# Telstra OAuth2 Server

## Synopsis

This project is an OAuth2 Server capable of issuing and checking OAuth2 tokens.

It supports the OAuth 2.0 SAML Bearer Assertion Flow.

It is the Authorization server in the flow shown below:

![OAuth 2.0 SAML Bearer Assertion Flow](OAuth-2_0-SAML-Bearer-Assertion-Flow.jpeg)

## Prerequisites

* Git
* Java 8 JDK
* Maven
* curl

## Run

The OAuth2 Server uses the "OAuth 2.0 SAML Bearer Assertion Flow". Only SAML v1.1 is supported as that
is the SAML version used by IDAM. We can use "curl" to simulate the OAuth2 client to test token issuing and
token checking.

* Get the code

```
git clone https://<your_username>@bitbucket.org/telstradevelopment/telstra-netops.git
```

* Build and run

```
cd telstra-netops\oauth2-server
mvn package
# activate the correct spring profiles for your environment by adding "-Dspring.profiles.active=<your_profiles>"
# to the tomcat java startup - usually you can do this by setting JAVA_OPTS=Dspring.profiles.active=<your_profiles>
# before starting tomcat
cp target\oauth2-server-0.0.1-SNAPSHOT.war <tomcat_webapps_folder>/oauth2-server.war
# now restart tomcat
```

* Test

We need to POST a SAML assertion to get an OAuth2 token.

A Base64-encoded SAML assertion [IDAM-sample-assertion-base64-encoded.xml (./sample-assertion/IDAM-sample-assertion-base64-encoded.xml)  was obtained
from IDAM. This is the encoded assertion that must be POSTed to the server to get an OAuth2 token.

The Base64-encoded SAML assertion must first be URL encoded so it can passed in an HTTP POST.
The URL encoded version is [IDAM-sample-assertion-base64-and-url-encoded.xml](./sample-assertion/IDAM-sample-assertion-base64-and-url-encoded.xml). The URL encoded version
has been copied to the file [post-data.txt](./post-data.txt) for convenience in POSTing the
data using curl.

To obtain an OAuth2 token run the following:

```
cd netops\oauth2-server
get-token.bat

{"access_token":"4da9d11f-0bf2-4adb-ba74-e54c67b99ddc","token_type":"bearer","expires_in":43199,"scope":"read"}
```

* Check the token

```
cd netops\oauth2-server
check-token.bat 4da9d11f-0bf2-4adb-ba74-e54c67b99ddc
```

## Run in other environments

To run the application in staging/production environments:

* copy the jar file
* copy the oauth2-server-start.bat and oauth2-server-start.sh
* import your IdP public cert into your truststore.jks
* set up your overrides.yml with the environment settings you need
* run the application be running the appropriate start script for your OS


## TODO

We still need to implement the following:

* persisting tokens across application restart 
* how to run in a load balancing configuration
* more tests

## Software used

This project makes use of Spring Web, [Spring OAuth](http://projects.spring.io/spring-security-oauth/) and [Spring SAML(http://projects.spring.io/spring-security-saml/).

## Viewing the SAML Assertion

The XML SAML assertion is [IDAM-sample-assertion.xml](./sample-assertion/IDAM-sample-assertion.xml).
This file was decoded with the included Base64UrlEncode program from the Base64-encoded version. 

## Getting Your Cert Trusted

To get your cert trusted:

* copy cert from XML SAML assertion in IDAM-pub.crt file (the cert can be found in the ds:X509Certificate element)
* import into keystore (password is in application.yaml)
keytool -import -trustcacerts -alias idam -file mycert-pub.crt -keystore truststore.jks
* change the keystore.alias in application.yml to "idam" and ensure the truststore name and password are correct


## References

* [SAML 2.0 Profile for OAuth 2.0 Client Authentication and Authorization
                                 Grants](http://tools.ietf.org/html/draft-ietf-oauth-saml2-bearer-17)
* [Salesforce OAuth 2.0 SAML Bearer Assertion Flow](https://help.salesforce.com/apex/HTViewHelpDoc?id=remoteaccess_oauth_SAML_bearer_flow.htm&language=en#response_token)
* [OpenSAML 2 Library reference](https://wiki.shibboleth.net/confluence/display/OpenSAML/Home)

