/*
 * XML Type:  IntListParams
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.IntListParams
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types.impl;
/**
 * An XML IntListParams(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public class IntListParamsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.types.IntListParams
{
    
    public IntListParamsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INTLISTPARAM$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/types", "intListParam");
    
    
    /**
     * Gets array of all "intListParam" elements
     */
    public com.hp.schemas.ucmdb._1.types.IntListProp[] getIntListParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INTLISTPARAM$0, targetList);
            com.hp.schemas.ucmdb._1.types.IntListProp[] result = new com.hp.schemas.ucmdb._1.types.IntListProp[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "intListParam" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListProp getIntListParamArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().find_element_user(INTLISTPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "intListParam" element
     */
    public int sizeOfIntListParamArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTLISTPARAM$0);
        }
    }
    
    /**
     * Sets array of all "intListParam" element
     */
    public void setIntListParamArray(com.hp.schemas.ucmdb._1.types.IntListProp[] intListParamArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(intListParamArray, INTLISTPARAM$0);
        }
    }
    
    /**
     * Sets ith "intListParam" element
     */
    public void setIntListParamArray(int i, com.hp.schemas.ucmdb._1.types.IntListProp intListParam)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().find_element_user(INTLISTPARAM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(intListParam);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "intListParam" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListProp insertNewIntListParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().insert_element_user(INTLISTPARAM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "intListParam" element
     */
    public com.hp.schemas.ucmdb._1.types.IntListProp addNewIntListParam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.IntListProp target = null;
            target = (com.hp.schemas.ucmdb._1.types.IntListProp)get_store().add_element_user(INTLISTPARAM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "intListParam" element
     */
    public void removeIntListParam(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTLISTPARAM$0, i);
        }
    }
}
