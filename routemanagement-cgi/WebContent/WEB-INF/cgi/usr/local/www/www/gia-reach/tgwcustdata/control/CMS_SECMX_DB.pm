# (C) Telstra 2001
#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 24 September 2001
# File: CMS_SECMX_DB.pm
#
# $Id: CMS_SECMX_DB.pm,v 1.1.1.1 2003/10/19 13:49:52 rxc Exp $

package CMS_SECMX_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;

our $error;

sub add_secmx_entry {
  my $accno = $dbh->quote(shift @_);
  my $domain_name = $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("INSERT INTO secmx
                  (accno, domain_name, request_time, status)
           VALUES ($accno, $domain_name, CURRENT_TIMESTAMP, 1)");

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;
}

sub delete_secmx_entry {
  my $accno =  $dbh->quote(shift @_);
  my $domain_name =  $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("UPDATE secmx 
      SET status = 3,
          request_time = CURRENT_TIMESTAMP
      WHERE accno = $accno 
        AND domain_name = $domain_name");

  my $result = $sth->execute;
  $error = $dbh->errstr;
  return $result;
}

sub get_secmx_entries_for_account {
  my $accno =  $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT domain_name, request_time, status
      FROM secmx
      WHERE accno = $accno");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
}

sub get_secmx_entries {
  my $accno =  $dbh->quote(shift @_);

  my $sth = $dbh->prepare
    ("SELECT domain_name
      FROM secmx
      WHERE status = 2");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
}

sub update_status {
 my $sth = $dbh->prepare
   ("UPDATE secmx
        SET status = 2
      WHERE status = 1");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

# $sth = $dbh->prepare
#   ("UPDATE secmx
#        SET status = 4
#      WHERE status = 3");
 $sth = $dbh->prepare
   ("DELETE from secmx 
      WHERE status = 3");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

}

sub get_accno_for_mx_domain {
  my $domain_name = $dbh->quote(shift @_);

  my $sth = $dbh->prepare 
    ("SELECT accno
      FROM secmx
      WHERE domain_name = $domain_name");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  if (my @result = $sth->fetchrow_array) {
    $error = $dbh->errstr;
    $sth->finish;
    return $result[0];
  } else {
    $error = $dbh->errstr;
    $sth->finish;
    return undef;
  }
}

sub get_secmx_status {
  my $sth = $dbh->prepare
    ("SELECT status, statustext
      FROM secmx_status");

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return undef;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
}

sub search_secmx_entries {
  
  my $accno = shift @_ || "";
  my $domain_name = shift @_ || "";
  my $status = shift @_ || "";
  my $timestamp_start = shift @_ || "";
  my $timestamp_end = shift @_ || "";

  my $command = "SELECT accno, domain_name,
                 statustext, request_time
                 FROM secmx 
                 NATURAL JOIN secmx_status ";
  
  my $extension = "";

  if ($timestamp_start ne "") {
    $extension .= "and request_time >= '$timestamp_start'
                   and request_time <= '$timestamp_end' ";
  }
  
  if ($accno ne "") {
    $extension .= "and accno ilike '%$accno%' ";
  }
  
  if ($domain_name ne "") {
    $extension .= "and domain_name ilike '%$domain_name%' ";
  }
  
  
  
  if ($status ne "") {
    $extension .= "and status = $status ";
  }  

  $extension =~ s/and/where/;
  
  

  $command .= "$extension order by request_time DESC ";
  
  if ($extension eq "") {
    # limit by default
    $command .= "limit 20";
  }

 
  my $sth = $dbh->prepare($command);

  unless ($sth->execute) {
    $error = $dbh->errstr;
    return 0;
  }

  my $result = $sth->fetchall_arrayref;
  $error = $dbh->errstr;
  return $result;
  
}


1;
