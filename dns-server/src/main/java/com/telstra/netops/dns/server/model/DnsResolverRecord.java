package com.telstra.netops.dns.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DnsResolverRecord {
	
	@JsonProperty("Hostname")
	private String hostName;
	
	@JsonProperty("IPBlock")
	private String ipBlock;
	
	@JsonProperty("Contact")
	private String contactEmailAddress;
	
	@JsonProperty("Account")
	private String customerId;
	
	@JsonProperty("header")
	private String header;
	
	@JsonProperty("Error")
	private String error;

	public DnsResolverRecord() {
	}
	
	public DnsResolverRecord(String hostName, String ipBlock,
			String contactEmailAddress, String customerId, String header) {
		super();
		this.hostName = hostName;
		this.ipBlock = ipBlock;
		this.contactEmailAddress = contactEmailAddress;
		this.customerId = customerId;
		this.header = header;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getIpBlock() {
		return ipBlock;
	}

	public void setIpBlock(String ipBlock) {
		this.ipBlock = ipBlock;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean notARecord() {
		return error != null;
	}	

}
