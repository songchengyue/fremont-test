/*
 * XML Type:  getCredentialsEntriesIDsResponse
 * Namespace: http://schemas.hp.com/ucmdb/discovery/1/params
 * Java type: com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb.discovery._1.params.impl;
/**
 * An XML getCredentialsEntriesIDsResponse(@http://schemas.hp.com/ucmdb/discovery/1/params).
 *
 * This is a complex type.
 */
public class GetCredentialsEntriesIDsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb.discovery._1.params.GetCredentialsEntriesIDsResponse
{
    
    public GetCredentialsEntriesIDsResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CREDENTIALSENTRYIDS$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/discovery/1/params", "credentialsEntryIDs");
    
    
    /**
     * Gets the "credentialsEntryIDs" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList getCredentialsEntryIDs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(CREDENTIALSENTRYIDS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "credentialsEntryIDs" element
     */
    public void setCredentialsEntryIDs(com.hp.schemas.ucmdb._1.types.StrList credentialsEntryIDs)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().find_element_user(CREDENTIALSENTRYIDS$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(CREDENTIALSENTRYIDS$0);
            }
            target.set(credentialsEntryIDs);
        }
    }
    
    /**
     * Appends and returns a new empty "credentialsEntryIDs" element
     */
    public com.hp.schemas.ucmdb._1.types.StrList addNewCredentialsEntryIDs()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.types.StrList target = null;
            target = (com.hp.schemas.ucmdb._1.types.StrList)get_store().add_element_user(CREDENTIALSENTRYIDS$0);
            return target;
        }
    }
}
