#
# Author: Peter Marrinon (peterm@telstra.net)
# Date: 24 September 2001
# File: CMS_BGP_DB.pm
#
# $Id: CMS_BGP_DB.pm,v 1.1.1.1 2003/10/19 13:49:52 rxc Exp $

package CMS_BGP_DB;
use Exporter;

use 5.6.1;
use strict;
use warnings;
use CMS_DB;

our $error;

sub get_as_path_by_as {
  my $as = shift @_;
  my $sth = $dbh->prepare("select distinct as_path from bgp_prefix_list where as_path like '$as%'");

  unless ($sth->execute) {
    $error = $dbh::err;
    return;
  }

  my $result = $sth->fetchall_arrayref();
  $error = $dbh::err;
  return $result;
}

sub get_as_path_communities_by_as {
  my $as = shift @_;
  my $sth = $dbh->prepare("select distinct as_path, communities from bgp_prefix_list where as_path like '$as%'");

  unless ($sth->execute) {
    $error = $dbh::err;
    return;
  }

  my $result = $sth->fetchall_arrayref();
  $error = $dbh::err;
  return $result;
}

sub get_as_path_communities_by_as_communities {
  my $as   = shift @_;
  my $comm = shift @_;
  my $sql  = "select distinct as_path, communities from bgp_prefix_list where as_path !=''";
  if ( ($as ne '') && ($as !~ /^all$/i) ) {
    $sql .= " and as_path like '$as%'";
  }
  if ( ($comm ne '') && ($comm !~ /^all$/i) ) {
    $sql .= " and communities like '%$comm%'";
  }
  my $sth = $dbh->prepare("$sql");
  # my $sth = $dbh->prepare("select distinct as_path, communities from bgp_prefix_list where as_path like '$as%'");

  unless ($sth->execute) {
    $error = $dbh::err;
    return;
  }

  my $result = $sth->fetchall_arrayref();
  $error = $dbh::err;
  return $result;
}

sub get_last_update {
  my $type = shift @_;
  my $sth = $dbh->prepare("select last_update from bgp_last_update where type = '$type'");
  $sth->execute; 
  my @result = $sth->fetchrow_array();
  $error = $dbh::err;
  return $result[0];
}

sub set_last_update {
  my $type = shift @_;
  my $last_update = shift @_;
  
  my $sth = $dbh->prepare("update bgp_last_update set last_update = '$last_update' where type = '$type'");
  $sth->execute;
  $error = $dbh::err;
  return;
}
 
sub del_outdate_route {
  my ($type, $time) = @_;
  my $sql_command = "delete from ";
  if ($type eq "mrt") {
    $sql_command .= "bgp_prefix_list ";
    if ($time ne "all") {
      $sql_command .= "where gmtime < $time";
    } 
  } elsif ($type eq "update") {
    $sql_command .= "bgp_update_list ";
    if ($time ne "all") {
      $sql_command .= "where update_time < CURRENT_TIMESTAMP - interval '$time' ";
    } 
  }
 
  my $sth = $dbh->prepare($sql_command);
  $sth->execute;
  $error = $dbh::err;
  return;
}

sub get_as_object_by_opshandle {
  my $opshandle = shift @_;
  my $sql_command = "select as_no from service";
  my %as_path = ();
  my $res;

  if ($opshandle ne "")  {
     $sql_command .= " where service_id = '$opshandle'";
  }
  my $sth = $dbh->prepare($sql_command);
  $sth->execute;

  my $result = $sth->fetchall_arrayref();
  $sth->finish;

  for my $i (0 .. $#{$result}) {
    $sth = $dbh->prepare("select distinct as_path from bgp_prefix_list where as_path like '$result->[$i][0]%'");
    $sth->execute;

    $res = $sth->fetchall_arrayref();
    for my $j (0 .. $#{$res}) {
      my @tmp = split($res->[$j][0], ' ');
      for my $k (1 .. $#tmp) {
         if (!(exists $as_path{$tmp[$k]})) {
            $as_path{$tmp[$k]} = $tmp[$k];
         }
      } 
    }
  }
  return %as_path; 
}

1;
