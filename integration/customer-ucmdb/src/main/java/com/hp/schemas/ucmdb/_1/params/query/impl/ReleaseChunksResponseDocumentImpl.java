/*
 * An XML document type.
 * Localname: releaseChunksResponse
 * Namespace: http://schemas.hp.com/ucmdb/1/params/query
 * Java type: com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.params.query.impl;
/**
 * A document containing one releaseChunksResponse(@http://schemas.hp.com/ucmdb/1/params/query) element.
 *
 * This is a complex type.
 */
public class ReleaseChunksResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponseDocument
{
    
    public ReleaseChunksResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RELEASECHUNKSRESPONSE$0 = 
        new javax.xml.namespace.QName("http://schemas.hp.com/ucmdb/1/params/query", "releaseChunksResponse");
    
    
    /**
     * Gets the "releaseChunksResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse getReleaseChunksResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse)get_store().find_element_user(RELEASECHUNKSRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "releaseChunksResponse" element
     */
    public void setReleaseChunksResponse(com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse releaseChunksResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse)get_store().find_element_user(RELEASECHUNKSRESPONSE$0, 0);
            if (target == null)
            {
                target = (com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse)get_store().add_element_user(RELEASECHUNKSRESPONSE$0);
            }
            target.set(releaseChunksResponse);
        }
    }
    
    /**
     * Appends and returns a new empty "releaseChunksResponse" element
     */
    public com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse addNewReleaseChunksResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse target = null;
            target = (com.hp.schemas.ucmdb._1.params.query.ReleaseChunksResponse)get_store().add_element_user(RELEASECHUNKSRESPONSE$0);
            return target;
        }
    }
}
