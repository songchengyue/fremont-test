package com.telstra.netops.dns.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientSecondaryDnsRequest {
	

	private String contactEmailAddress;	

	private String domain;

	private String primaryServerIpAddress;
	
	private String customerId;
	private String rowid;
	
	private String serviceId ;


	public ClientSecondaryDnsRequest() {
	}
	
	public ClientSecondaryDnsRequest(String contactEmailAddress,
			String domain, String rowid,String customerId ,String serviceId) {
		super();
		this.contactEmailAddress = contactEmailAddress;
		this.domain = domain;
		this.customerId = customerId;
		this.rowid = rowid;
		this.serviceId = serviceId ;
	}
	

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}


	public String getPrimaryServerIpAddress() {
		return primaryServerIpAddress;
	}

	public void setPrimaryServerIpAddress(String primaryServerIpAddress) {
		this.primaryServerIpAddress = primaryServerIpAddress;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getRowid() {
		return rowid;
	}

	public void setRowid(String rowid) {
		this.rowid = rowid;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
}
