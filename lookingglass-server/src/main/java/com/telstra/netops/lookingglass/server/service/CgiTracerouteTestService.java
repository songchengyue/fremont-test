package com.telstra.netops.lookingglass.server.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.telstra.netops.lookingglass.server.exception.LookingClassApplicationInternalException;
import com.telstra.netops.lookingglass.server.model.ClientNodeTestRequest;
import com.telstra.netops.lookingglass.server.model.NodeTraceTestRequest;
import com.telstra.netops.lookingglass.server.model.TracerouteHop;
import com.telstra.netops.lookingglass.server.repository.NodeRepository;
import com.telstra.netops.lookingglass.server.util.JsonSerializerDeserializer;
import com.telstra.oauth2.client.OAuth2TokenResolver;

@Service
@Profile("!local")
public class CgiTracerouteTestService implements TracerouteTestService {

	@Value("${lookingglass.cgi.url}")
	private String cgiURL;

	private static final Logger LOGGER = LoggerFactory.getLogger(CgiTracerouteTestService.class);

	@Autowired
	private NodeRepository nodeRepository;

	@Autowired
	private JsonSerializerDeserializer jsonSerializerDeserializer;
	
	@Autowired
    private OAuth2TokenResolver tokenResolver;

	public List<TracerouteHop> performTracerouteTest(ClientNodeTestRequest request) {
		if (request.getSubmit() == null) {
			LookingClassApplicationInternalException lge = new LookingClassApplicationInternalException(
					"Invalid or no Inputs!");
			LOGGER.error("can't perform node test", lge);
			throw lge;
		}

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.add(getAuthorizationHeaderName(), getAuthorizationHeaderValue());

		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();

		NodeTraceTestRequest nodeTraceTestRequest = new NodeTraceTestRequest(
				String.valueOf(request.getType().getCode()), request.getCustomerId(), request.getProduct(),
				getIpAddress(request.getSourceNode()), request.getTargetNode(), request.getSubmit(), null);

		map.add("POSTDATA", jsonSerializerDeserializer.serialize(nodeTraceTestRequest));

		HttpEntity<MultiValueMap<String, Object>> httpRequest = new HttpEntity<MultiValueMap<String, Object>>(map,
				headers);

		ResponseEntity<Map> responseEntity = restTemplate.exchange(cgiURL, HttpMethod.POST, httpRequest, Map.class);

		if (!HttpStatus.OK.equals(responseEntity.getStatusCode())) {
			LookingClassApplicationInternalException lge = new LookingClassApplicationInternalException(
					"node test failed with http status code: " + responseEntity.getStatusCode());
			LOGGER.error("can't perform node test", lge);
			throw lge;
		}

		Map<String, String> responseMap = (Map<String, String>) responseEntity.getBody();

		List<TracerouteHop> hops = new ArrayList<TracerouteHop>();

		Set<String> sortedKeys = new TreeSet<String>(responseMap.keySet());
		for (String key : sortedKeys) {
			hops.add(new TracerouteHop(responseMap.get(key)));
		}

		return hops;
	}
	
	private String getIpAddress(Long nodeID) {
		return nodeRepository.findOne(nodeID).getIpAddress();
	}

	public String getIpAddress(String nodeID) {
		return nodeRepository.findOne(Long.valueOf(nodeID)).getIpAddress();
	}
	
	private String getAuthorizationHeaderName() {
		return tokenResolver.getAuthorizationHeaderName();
	}
	
	private String getAuthorizationHeaderValue() {
		return tokenResolver.getAuthorizationHeaderValue();
	}

}
