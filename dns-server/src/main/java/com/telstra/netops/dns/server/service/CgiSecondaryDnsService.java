package com.telstra.netops.dns.server.service;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.telstra.netops.dns.server.exception.DnsApplicationGenericBusinessRuleException;
import com.telstra.netops.dns.server.model.ClientDnsRemoveRequest;
import com.telstra.netops.dns.server.model.ClientSecondaryDnsRequest;
import com.telstra.netops.dns.server.model.SecondaryDnsRecord;
import com.telstra.netops.dns.server.model.Success;
import com.telstra.netops.dns.server.model.cgi.CgiDnsRequest;
import com.telstra.netops.dns.server.model.cgi.DnsResultWrapper;
import com.telstra.netops.dns.server.model.cgi.DnsSecondaryRecordsResultWrapper;
import com.telstra.netops.dns.server.util.JsonSerializerDeserializer;
import com.telstra.oauth2.client.OAuth2TokenResolver;

@Service
@Profile("!local")
public class CgiSecondaryDnsService implements SecondaryDnsService {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CgiResolverService.class);
	
	@Value("${dns.cgi.url}")
	private String cgiURL;
	
	@Autowired
	private JsonSerializerDeserializer jsonSerializerDeserializer;
	
	@Autowired
    private OAuth2TokenResolver tokenResolver;
	
	@Override
	public List<SecondaryDnsRecord> list(String accountId) {
		
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createSecondaryDnsListRecords(accountId);
		
		CgiDnsRequester<DnsSecondaryRecordsResultWrapper> requester = new CgiDnsRequester<DnsSecondaryRecordsResultWrapper>(DnsSecondaryRecordsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsSecondaryRecordsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isEmpty()) {
			return Collections.<SecondaryDnsRecord>emptyList();
		}
		
		LOGGER.debug(wrapper.toString());

		return wrapper.getResult();	
	}

	public Success remove(ClientDnsRemoveRequest request) {
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createSecondaryDnsCancelRecords(request);
		
		CgiDnsRequester<DnsResultWrapper> requester = new CgiDnsRequester<DnsResultWrapper>(DnsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isError()) {
			throw new DnsApplicationGenericBusinessRuleException(wrapper.getErrorMessage());
		}
		
		LOGGER.debug(wrapper.toString());
		return new Success(wrapper.getSuccessMessage());
	}

	public Success add(ClientSecondaryDnsRequest request) {
		CgiDnsRequest cgiDnsRequest = CgiDnsRequest.createSecondaryDnsAddRecordRequest(request);

		CgiDnsRequester<DnsResultWrapper> requester = new CgiDnsRequester<DnsResultWrapper>(DnsResultWrapper.class, jsonSerializerDeserializer, cgiURL);

		DnsResultWrapper wrapper = requester.doRequest(cgiDnsRequest, tokenResolver);
		
		if (wrapper.isError()) {
			throw new DnsApplicationGenericBusinessRuleException(wrapper.getErrorMessage());
		}
		
		LOGGER.debug(wrapper.toString());
		return new Success(wrapper.getSuccessMessage());
	}


}