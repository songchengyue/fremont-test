/*
 * XML Type:  Parameters
 * Namespace: http://schemas.hp.com/ucmdb/1/types
 * Java type: com.hp.schemas.ucmdb._1.types.Parameters
 *
 * Automatically generated - do not modify.
 */
package com.hp.schemas.ucmdb._1.types;


/**
 * An XML Parameters(@http://schemas.hp.com/ucmdb/1/types).
 *
 * This is a complex type.
 */
public interface Parameters extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Parameters.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s4F73B35B0CD72BC1C43DDD0DB2F70A53").resolveHandle("parametersedf6type");
    
    /**
     * Gets the "dateParams" element
     */
    com.hp.schemas.ucmdb._1.types.DateParams getDateParams();
    
    /**
     * Sets the "dateParams" element
     */
    void setDateParams(com.hp.schemas.ucmdb._1.types.DateParams dateParams);
    
    /**
     * Appends and returns a new empty "dateParams" element
     */
    com.hp.schemas.ucmdb._1.types.DateParams addNewDateParams();
    
    /**
     * Gets the "doubleParams" element
     */
    com.hp.schemas.ucmdb._1.types.DoubleParams getDoubleParams();
    
    /**
     * Sets the "doubleParams" element
     */
    void setDoubleParams(com.hp.schemas.ucmdb._1.types.DoubleParams doubleParams);
    
    /**
     * Appends and returns a new empty "doubleParams" element
     */
    com.hp.schemas.ucmdb._1.types.DoubleParams addNewDoubleParams();
    
    /**
     * Gets the "floatParams" element
     */
    com.hp.schemas.ucmdb._1.types.FloatParams getFloatParams();
    
    /**
     * Sets the "floatParams" element
     */
    void setFloatParams(com.hp.schemas.ucmdb._1.types.FloatParams floatParams);
    
    /**
     * Appends and returns a new empty "floatParams" element
     */
    com.hp.schemas.ucmdb._1.types.FloatParams addNewFloatParams();
    
    /**
     * Gets the "intListParams" element
     */
    com.hp.schemas.ucmdb._1.types.IntListParams getIntListParams();
    
    /**
     * Sets the "intListParams" element
     */
    void setIntListParams(com.hp.schemas.ucmdb._1.types.IntListParams intListParams);
    
    /**
     * Appends and returns a new empty "intListParams" element
     */
    com.hp.schemas.ucmdb._1.types.IntListParams addNewIntListParams();
    
    /**
     * Gets the "intParams" element
     */
    com.hp.schemas.ucmdb._1.types.IntParams getIntParams();
    
    /**
     * Sets the "intParams" element
     */
    void setIntParams(com.hp.schemas.ucmdb._1.types.IntParams intParams);
    
    /**
     * Appends and returns a new empty "intParams" element
     */
    com.hp.schemas.ucmdb._1.types.IntParams addNewIntParams();
    
    /**
     * Gets the "strParams" element
     */
    com.hp.schemas.ucmdb._1.types.StrParams getStrParams();
    
    /**
     * Sets the "strParams" element
     */
    void setStrParams(com.hp.schemas.ucmdb._1.types.StrParams strParams);
    
    /**
     * Appends and returns a new empty "strParams" element
     */
    com.hp.schemas.ucmdb._1.types.StrParams addNewStrParams();
    
    /**
     * Gets the "strListParams" element
     */
    com.hp.schemas.ucmdb._1.types.StrListParams getStrListParams();
    
    /**
     * Sets the "strListParams" element
     */
    void setStrListParams(com.hp.schemas.ucmdb._1.types.StrListParams strListParams);
    
    /**
     * Appends and returns a new empty "strListParams" element
     */
    com.hp.schemas.ucmdb._1.types.StrListParams addNewStrListParams();
    
    /**
     * Gets the "longParams" element
     */
    com.hp.schemas.ucmdb._1.types.LongParams getLongParams();
    
    /**
     * Sets the "longParams" element
     */
    void setLongParams(com.hp.schemas.ucmdb._1.types.LongParams longParams);
    
    /**
     * Appends and returns a new empty "longParams" element
     */
    com.hp.schemas.ucmdb._1.types.LongParams addNewLongParams();
    
    /**
     * Gets the "bytesParams" element
     */
    com.hp.schemas.ucmdb._1.types.BytesParams getBytesParams();
    
    /**
     * Sets the "bytesParams" element
     */
    void setBytesParams(com.hp.schemas.ucmdb._1.types.BytesParams bytesParams);
    
    /**
     * Appends and returns a new empty "bytesParams" element
     */
    com.hp.schemas.ucmdb._1.types.BytesParams addNewBytesParams();
    
    /**
     * Gets the "xmlParams" element
     */
    com.hp.schemas.ucmdb._1.types.XmlParams getXmlParams();
    
    /**
     * Sets the "xmlParams" element
     */
    void setXmlParams(com.hp.schemas.ucmdb._1.types.XmlParams xmlParams);
    
    /**
     * Appends and returns a new empty "xmlParams" element
     */
    com.hp.schemas.ucmdb._1.types.XmlParams addNewXmlParams();
    
    /**
     * Gets the "booleanParams" element
     */
    com.hp.schemas.ucmdb._1.types.BooleanParams getBooleanParams();
    
    /**
     * Sets the "booleanParams" element
     */
    void setBooleanParams(com.hp.schemas.ucmdb._1.types.BooleanParams booleanParams);
    
    /**
     * Appends and returns a new empty "booleanParams" element
     */
    com.hp.schemas.ucmdb._1.types.BooleanParams addNewBooleanParams();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.hp.schemas.ucmdb._1.types.Parameters newInstance() {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.hp.schemas.ucmdb._1.types.Parameters parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.hp.schemas.ucmdb._1.types.Parameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
