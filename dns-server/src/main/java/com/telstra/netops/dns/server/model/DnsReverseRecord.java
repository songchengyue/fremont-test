package com.telstra.netops.dns.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DnsReverseRecord {
	
	@JsonProperty("Hostname")
	private String hostName;
	
	@JsonProperty("IPaddress")
	private String IPaddress;
	
	@JsonProperty("Contact")
	private String contactEmailAddress;
	
	@JsonProperty("Account")
	private String customerId;
	
	@JsonProperty("header")
	private String header;
	
	@JsonProperty("Error")
	private String error;
	
	@JsonProperty("NameServer")
	private String nameServer;
	
	@JsonProperty("RecordType")
	private String RecordType;

	public DnsReverseRecord() {
	}
	
	public DnsReverseRecord(String hostName, String ipBlock,
			String contactEmailAddress, String customerId, String header ,String nameServer ,String recordtype ) {
		super();
		this.hostName = hostName;
		this.IPaddress = ipBlock;
		this.contactEmailAddress = contactEmailAddress;
		this.customerId = customerId;
		this.header = header;
		this.nameServer = nameServer;
		this.RecordType = recordtype;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getIpBlock() {
		return IPaddress;
	}

	public void setIpBlock(String ipBlock) {
		this.IPaddress = ipBlock;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean notARecord() {
		return error != null;
	}

	public String getNameServer() {
		return nameServer;
	}

	public void setNameServer(String nameServer) {
		this.nameServer = nameServer;
	}

	public String getRecordtype() {
		return RecordType;
	}

	public void setRecordtype(String recordtype) {
		this.RecordType = recordtype;
	}	

}
